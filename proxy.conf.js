const proxyConfLocal = require("./proxy.conf.local.js")
const proxyConfVariable = require("./scripts/proxy.conf.variable")

const logLevel = proxyConfLocal.logLevel;

module.exports = {
    ...proxyConfVariable.shortDoi(logLevel),
    ...proxyConfVariable.oauth(logLevel, 20210),
    ...proxyConfVariable.shiblogin(logLevel, 20210),
    ...proxyConfLocal.obj(logLevel),
}
