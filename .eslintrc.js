module.exports = {
    extends: [
        "./node_modules/solidify-frontend/.eslintrc.cjs",
    ],
    ignorePatterns: [
        "node_modules/**/*",
        "scripts/**/*",
        "src/app/generated-api",
        "src/environments/environment.local.ts",
        "src/server-environments/server-environment.local.ts",
        "proxy.conf.*"
    ],
    overrides: [
        {
            files: [
                "*.ts"
            ],
            parserOptions: {
                tsconfigRootDir: __dirname,
                project: [
                    "tsconfig.json",
                    "e2e/tsconfig.json"
                ],
                createDefaultProgram: true
            },
            extends: [],
            plugins: [],
            rules: {
                "@angular-eslint/component-selector": [
                    "error",
                    {
                        "type": "element",
                        "prefix": "aou",
                        "style": "kebab-case"
                    }
                ],
                "@angular-eslint/directive-selector": [
                    "error",
                    {
                        "type": "attribute",
                        "prefix": "aou",
                        "style": "camelCase"
                    }
                ],
            }
        },
        {
            files: [
                "*.html"
            ],
            extends: [],
            rules: {}
        }
    ]
}
