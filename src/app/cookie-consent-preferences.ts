/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - cookie-consent-preferences.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CookieEnum} from "@shared/enums/cookie.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  CookiePreference,
  CookieType,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

export const cookieConsentPreferences: CookiePreference[] = [
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.seeAllDepositsLastSelection,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.seeAllDepositsLastSelection.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.seeAllDepositsLastSelection.description"),
    visible: () => true,
  },
  {
    type: CookieType.cookie,
    isEnabled: false,
    name: CookieEnum.publicationWithRestrictedAccessMasters,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.publicationWithRestrictedAccessMasters.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.publicationWithRestrictedAccessMasters.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.seeMyDepositAsContributor,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.seeMyDepositAsContributor.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.seeMyDepositAsContributor.description"),
    visible: () => true,
  },
];
