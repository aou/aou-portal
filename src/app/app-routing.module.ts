/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {
  RouterModule,
  UrlSegment,
} from "@angular/router";
import {PageNotFoundPresentational} from "@app/components/presentationals/page-not-found/page-not-found.presentational";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@app/shared/guards/application-role-guard.service";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {HomeGuardService} from "@shared/guards/home-guard.service";
import {IsUserMemberAuthorizedInstitutionsGuardService} from "@shared/guards/is-user-member-authorized-institutions-guard.service";
import {IsValidatorGuardService} from "@shared/guards/is-validator-guard.service";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  AboutRoutable,
  ChangelogRoutable,
  ColorCheckRoutable,
  CombinedGuardService,
  DevGuardService,
  EmptyRoutable,
  IconAppSummaryRoutable,
  MaintenanceModeRoutable,
  OrcidRedirectRoutable,
  PreventLeaveGuardService,
  PreventLeaveMaintenanceGuardService,
  ReleaseNotesRoutable,
  ServerOfflineModeRoutable,
  UnableToLoadAppRoutable,
  UrlQueryParamHelper,
} from "solidify-frontend";

const regexArchiveId = new RegExp(/.+:\d+$/);

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.home,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/home/home.module").then(m => m.HomeModule),
    data: {
      permission: ApplicationRolePermissionEnum.noPermission,
      guards: [ApplicationRoleGuardService, HomeGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.deposit,

    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/deposit/deposit.module").then(m => m.DepositModule),
    data: {
      permission: ApplicationRolePermissionEnum.userPermission,
      breadcrumb: LabelTranslateEnum.deposits,
      guards: [ApplicationRoleGuardService, IsUserMemberAuthorizedInstitutionsGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.depositToValidate,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/deposit/deposit.module").then(m => m.DepositModule),
    data: {
      permission: ApplicationRolePermissionEnum.userPermission,
      breadcrumb: LabelTranslateEnum.depositsToValidate,
      guards: [ApplicationRoleGuardService, IsValidatorGuardService, IsUserMemberAuthorizedInstitutionsGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.admin,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/admin/admin.module").then(m => m.AdminModule),
    data: {
      breadcrumb: LabelTranslateEnum.administration,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService, IsUserMemberAuthorizedInstitutionsGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.notification,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/notification/notification.module").then(m => m.NotificationModule),
    data: {
      breadcrumb: LabelTranslateEnum.notifications,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService, IsUserMemberAuthorizedInstitutionsGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.icons,
    component: IconAppSummaryRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.colorCheck,
    component: ColorCheckRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.maintenance,
    component: MaintenanceModeRoutable,
    canDeactivate: [PreventLeaveMaintenanceGuardService],
  },
  {
    path: AppRoutesEnum.about,
    component: AboutRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.about,
    },
  },
  {
    path: AppRoutesEnum.releaseNotes,
    component: ReleaseNotesRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.releaseNotes,
    },
  },
  {
    path: AppRoutesEnum.changelog,
    component: ChangelogRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.changelog,
    },
  },
  {
    path: AppRoutesEnum.serverOffline,
    component: ServerOfflineModeRoutable,
    canDeactivate: [PreventLeaveGuardService],
  },
  {
    path: AppRoutesEnum.unableToLoadApp,
    component: UnableToLoadAppRoutable,
    canDeactivate: [PreventLeaveGuardService],
  },
  {
    path: AppRoutesEnum.orcidRedirect,
    component: OrcidRedirectRoutable,
  },
  {
    path: AppRoutesEnum.redirect,
    component: EmptyRoutable,
  },
  {
    path: AppRoutesEnum.root,
    redirectTo: "/" + AppRoutesEnum.home + UrlQueryParamHelper.getQueryParamOAuth2(),
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.index,
    redirectTo: "/" + AppRoutesEnum.home + UrlQueryParamHelper.getQueryParamOAuth2(),
    pathMatch: "full",
  },
  {
    redirectTo: AppRoutesEnum.separator + AppRoutesEnum.home + AppRoutesEnum.separator + HomePageRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    pathMatch: "full",
    matcher: (url) => {
      const path = url[0].path;
      if (url.length === 1 && path.match(regexArchiveId)) {
        return {
          consumed: url,
          posParams: {
            [AppRoutesEnum.paramIdWithoutPrefixParam]: new UrlSegment(path, {}),
          },
        };
      }
      return null;
    },
  },
  {
    path: AppRoutesEnum.contributor + "/" + AppRoutesEnum.paramId,
    redirectTo: "/" + AppRoutesEnum.home + "/" + AppRoutesEnum.contributor + "/" + AppRoutesEnum.paramId,
    pathMatch: "full",
  },
  {
    path: "**",
    component: PageNotFoundPresentational,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: "enabledBlocking",
  })],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
