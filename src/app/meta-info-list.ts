/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - meta-info-list.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Meta} from "@angular/platform-browser";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {PublicContributorDto} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  cleanHtml,
  isArray,
  isNotNullNorUndefinedNorWhiteString,
  MappingObjectUtil,
  MetaInfo,
  MetaUtil,
} from "solidify-frontend";

const TITLE_MAX_CHAR: number = 60;
const ELLIPSIS: string = "...";

export const REGEXP_CITATION = new RegExp(/citation_*/);

export const metaInfoPublication: MetaInfo<PublishedDeposit> = {
  regex: /^(\/home\/detail)?\/unige\:\d*/,
  titleToTranslateCallback: (translate, publishedDeposit) => {
    const suffix = translate.instant(LabelTranslateEnum.metaPublicationSuffixTitle);
    const title: string = cleanHtml(publishedDeposit.title);
    const concatenation: string = title + suffix;
    if (concatenation.length <= TITLE_MAX_CHAR) {
      return concatenation;
    }
    return title.substring(0, TITLE_MAX_CHAR - (suffix.length + ELLIPSIS.length)) + ELLIPSIS + suffix;
  },
  descriptionToTranslateCallback: (translate, publishedDeposit) => translate.instant(LabelTranslateEnum.metaPublicationDescription, publishedDeposit),
  extraMetaTreatmentCallback: (document, meta: Meta, translateService, archive) => {
    MetaUtil.cleanMatchingMetaByName(meta, REGEXP_CITATION);
    MappingObjectUtil.forEach(archive.htmlMetaHeaders, (metaValue, key) => {
      if (isArray(metaValue)) {
        MetaUtil.setMetaTagNameForEachArray(meta, key, metaValue);
      } else {
        MetaUtil.setMetaTagName(meta, key, metaValue);
      }
    });
  },
  bypassAutomaticMeta: true,
};

export const metaInfoContributor: MetaInfo<PublicContributorDto> = {
  regex: /^\/home\/contributor\/*/,
  titleToTranslateCallback: (translate, contributor) => {
    const suffix = translate.instant(LabelTranslateEnum.metaContributorSuffixTitle);
    const title: string = `${contributor.lastName}, ${contributor.firstName}`;
    const concatenation: string = title + suffix;
    if (concatenation.length <= TITLE_MAX_CHAR) {
      return concatenation;
    }
    return title.substring(0, TITLE_MAX_CHAR - (suffix.length + ELLIPSIS.length)) + ELLIPSIS + suffix;
  },
  descriptionToTranslateCallback: (translate, contributor) => translate.instant(LabelTranslateEnum.metaContributorDescription, contributor),
  extraMetaTreatmentCallback: (document, meta: Meta, translateService, contributor) => {
    const orcid = "orcid";
    MetaUtil.cleanMetaByName(meta, orcid);
    if (isNotNullNorUndefinedNorWhiteString(contributor.orcid)) {
      MetaUtil.setMetaTagName(meta, orcid, contributor.orcid);
    }
  },
  bypassAutomaticMeta: true,
};

export const metaInfoAbout: MetaInfo<PublishedDeposit> = {
  regex: /^\/about*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAboutTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAboutDescription),
};

export const metaInfoAdmin: MetaInfo<void> = {
  regex: /^\/admin\/*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAdminTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralDescription),
};

export const metaInfoDefault: MetaInfo<void> = {
  regex: /^.*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralDescription),
};

export const metaInfoList: MetaInfo<any>[] = [
  metaInfoPublication,
  metaInfoContributor,
  metaInfoAbout,
  metaInfoDefault,
];
