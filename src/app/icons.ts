/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - icons.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  IconInfos,
  IconLibEnum,
} from "solidify-frontend";

export const icons: IconInfos[] = [
  {
    name: IconNameEnum.researchGroup,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "building",
  },
  {
    name: IconNameEnum.deposit,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-upload",
  },
  {
    name: IconNameEnum.depositToValidate,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "clipboard-check",
  },
  {
    name: IconNameEnum.home,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "home",
  },
  {
    name: IconNameEnum.order,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "archive",
  },
  {
    name: IconNameEnum.preservationSpace,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "users",
  },
  {
    name: IconNameEnum.preservationPlanning,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "boxes",
  },
  {
    name: IconNameEnum.administration,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cogs",
  },
  {
    name: IconNameEnum.profile,
    lib: IconLibEnum.materialIcon,
    icon: "account_circle",
  },
  {
    name: IconNameEnum.token,
    lib: IconLibEnum.materialIcon,
    icon: "vpn_key",
  },
  {
    name: IconNameEnum.logout,
    lib: IconLibEnum.materialIcon,
    icon: "power_settings_new",
  },
  {
    name: IconNameEnum.login,
    lib: IconLibEnum.materialIcon,
    icon: "account_circle",
  },
  {
    name: IconNameEnum.resId,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "key",
  },
  {
    name: IconNameEnum.history,
    lib: IconLibEnum.materialIcon,
    icon: "history",
  },
  {
    name: IconNameEnum.docDev,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "code",
  },
  {
    name: IconNameEnum.docUser,
    lib: IconLibEnum.materialIcon,
    icon: "help",
  },
  {
    name: IconNameEnum.docValidator,
    lib: IconLibEnum.materialIcon,
    icon: "task_alt",
  },
  {
    name: IconNameEnum.about,
    lib: IconLibEnum.materialIcon,
    icon: "info",
  },
  {
    name: IconNameEnum.cart,
    lib: IconLibEnum.materialIcon,
    icon: "archive",
  },
  {
    name: IconNameEnum.search,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "search",
  },
  {
    name: IconNameEnum.back,
    lib: IconLibEnum.materialIcon,
    icon: "navigate_before",
  },
  {
    name: IconNameEnum.next,
    lib: IconLibEnum.materialIcon,
    icon: "navigate_next",
  },
  {
    name: IconNameEnum.refresh,
    lib: IconLibEnum.materialIcon,
    icon: "refresh",
  },
  {
    name: IconNameEnum.delete,
    lib: IconLibEnum.materialIcon,
    icon: "delete",
  },
  {
    name: IconNameEnum.deleteAll,
    lib: IconLibEnum.materialIcon,
    icon: "delete_forever",
  },
  {
    name: IconNameEnum.create,
    lib: IconLibEnum.materialIcon,
    icon: "add",
  },
  {
    name: IconNameEnum.internalLink,
    lib: IconLibEnum.materialIcon,
    icon: "open_in_new ",
  },
  {
    name: IconNameEnum.externalLink,
    lib: IconLibEnum.materialIcon,
    icon: "language",
  },
  {
    name: IconNameEnum.edit,
    lib: IconLibEnum.materialIcon,
    icon: "edit",
  },
  {
    name: IconNameEnum.add,
    lib: IconLibEnum.materialIcon,
    icon: "add",
  },
  {
    name: IconNameEnum.addPerson,
    lib: IconLibEnum.materialIcon,
    icon: "person_add",
  },
  {
    name: IconNameEnum.addCollaboration,
    lib: IconLibEnum.materialIcon,
    icon: "group_add",
  },
  {
    name: IconNameEnum.close,
    lib: IconLibEnum.materialIcon,
    icon: "close",
  },
  {
    name: IconNameEnum.archiveBrowsing,
    lib: IconLibEnum.materialIcon,
    icon: "list",
  },
  {
    name: IconNameEnum.download,
    lib: IconLibEnum.materialIcon,
    icon: "file_download",
  },
  {
    name: IconNameEnum.downloadLoginNeeded,
    lib: IconLibEnum.materialIcon,
    icon: "account_circle",
  },
  {
    name: IconNameEnum.downloadNotAllowed,
    lib: IconLibEnum.materialIcon,
    icon: "block",
  },
  {
    name: IconNameEnum.sendRequest,
    lib: IconLibEnum.materialIcon,
    icon: "announcement",
  },
  {
    name: IconNameEnum.roleValidator,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-check",
  },
  {
    name: IconNameEnum.roleUser,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.myOrder,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.allOrder,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "users",
  },
  {
    name: IconNameEnum.metadata,
    lib: IconLibEnum.materialIcon,
    icon: "assignment",
  },
  {
    name: IconNameEnum.files,
    lib: IconLibEnum.materialIcon,
    icon: "insert_drive_file",
  },
  {
    name: IconNameEnum.associateArchive,
    lib: IconLibEnum.materialIcon,
    icon: "link",
  },
  {
    name: IconNameEnum.collection,
    lib: IconLibEnum.materialIcon,
    icon: "collections_bookmark",
  },
  {
    name: IconNameEnum.classification,
    lib: IconLibEnum.materialIcon,
    icon: "label",
  },
  {
    name: IconNameEnum.navigate,
    lib: IconLibEnum.materialIcon,
    icon: "forward",
  },
  {
    name: IconNameEnum.reserveDoi,
    lib: IconLibEnum.materialIcon,
    icon: "library_books",
  },
  {
    name: IconNameEnum.submit,
    lib: IconLibEnum.materialIcon,
    icon: "done_all",
  },
  {
    name: IconNameEnum.approve,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.unapprove,
    lib: IconLibEnum.materialIcon,
    icon: "close",
  },
  {
    name: IconNameEnum.askFeedback,
    lib: IconLibEnum.materialIcon,
    icon: "feedback",
  },
  {
    name: IconNameEnum.wait,
    lib: IconLibEnum.materialIcon,
    icon: "access_time",
  },
  {
    name: IconNameEnum.save,
    lib: IconLibEnum.materialIcon,
    icon: "save",
  },
  {
    name: IconNameEnum.uploadFile,
    lib: IconLibEnum.materialIcon,
    icon: "publish",
  },
  {
    name: IconNameEnum.uploadStructuredFiles,
    lib: IconLibEnum.materialIcon,
    icon: "unarchive",
  },
  {
    name: IconNameEnum.filesView,
    lib: IconLibEnum.materialIcon,
    icon: "list",
  },
  {
    name: IconNameEnum.foldersView,
    lib: IconLibEnum.materialIcon,
    icon: "folder_open",
  },
  {
    name: IconNameEnum.preview,
    lib: IconLibEnum.materialIcon,
    icon: "remove_red_eye",
  },
  {
    name: IconNameEnum.star,
    lib: IconLibEnum.materialIcon,
    icon: "star",
  },
  {
    name: IconNameEnum.star_empty,
    lib: IconLibEnum.materialIcon,
    icon: "star_border",
  },
  {
    name: IconNameEnum.move,
    lib: IconLibEnum.materialIcon,
    icon: "redo",
  },
  {
    name: IconNameEnum.information,
    lib: IconLibEnum.materialIcon,
    icon: "info",
  },
  {
    name: IconNameEnum.contributor,
    lib: IconLibEnum.materialIcon,
    icon: "group",
  },
  {
    name: IconNameEnum.requestInbox,
    lib: IconLibEnum.materialIcon,
    icon: "mail",
  },
  {
    name: IconNameEnum.requestSent,
    lib: IconLibEnum.materialIcon,
    icon: "send",
  },
  {
    name: IconNameEnum.sip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "paper-plane",
  },
  {
    name: IconNameEnum.dip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cart-arrow-down",
  },
  {
    name: IconNameEnum.aip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "archive",
  },
  {
    name: IconNameEnum.monitoring,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-md",
  },
  {
    name: IconNameEnum.jobs,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "robot",
  },
  {
    name: IconNameEnum.archivingStatus,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "clipboard-check",
  },
  {
    name: IconNameEnum.aipDownloaded,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cart-arrow-down",
  },
  {
    name: IconNameEnum.storagion,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "server",
  },
  {
    name: IconNameEnum.simpleChecksum,
    lib: IconLibEnum.materialIcon,
    icon: "refresh",
  },
  {
    name: IconNameEnum.doubleChecksum,
    lib: IconLibEnum.materialIcon,
    icon: "sync",
  },
  {
    name: IconNameEnum.reindex,
    lib: IconLibEnum.materialIcon,
    icon: "repeat",
  },
  {
    name: IconNameEnum.check,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.init,
    lib: IconLibEnum.materialIcon,
    icon: "power",
  },
  {
    name: IconNameEnum.run,
    lib: IconLibEnum.materialIcon,
    icon: "play_arrow",
  },
  {
    name: IconNameEnum.resume,
    lib: IconLibEnum.materialIcon,
    icon: "play_circle_filled",
  },
  {
    name: IconNameEnum.resumeAll,
    lib: IconLibEnum.materialIcon,
    icon: "play_circle_outline",
  },
  {
    name: IconNameEnum.notIgnore,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.orderReady,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.orderInProgress,
    lib: IconLibEnum.materialIcon,
    icon: "autorenew",
  },
  {
    name: IconNameEnum.orderInError,
    lib: IconLibEnum.materialIcon,
    icon: "error",
  },
  {
    name: IconNameEnum.submissionPolicies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-check",
  },
  {
    name: IconNameEnum.preservationPolicies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "history",
  },
  {
    name: IconNameEnum.disseminationPolicies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cart-arrow-down",
  },
  {
    name: IconNameEnum.licenses,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "copyright",
  },
  {
    name: IconNameEnum.globalBanners,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "exclamation-triangle",
  },
  {
    name: IconNameEnum.correct,
    lib: IconLibEnum.materialIcon,
    icon: "done",
  },
  {
    name: IconNameEnum.incorrect,
    lib: IconLibEnum.materialIcon,
    icon: "dangerous",
  },
  {
    name: IconNameEnum.institutions,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "university",
  },
  {
    name: IconNameEnum.researchDomains,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "microscope",
  },
  {
    name: IconNameEnum.users,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-circle",
  },
  {
    name: IconNameEnum.roles,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "id-badge",
  },
  {
    name: IconNameEnum.oaiSets,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "passport",
  },
  {
    name: IconNameEnum.oauth2Clients,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "passport",
  },
  {
    name: IconNameEnum.peoples,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.fundingAgencies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "hand-holding-usd",
  },
  {
    name: IconNameEnum.indexFieldAliases,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "search",
  },
  {
    name: IconNameEnum.archiveAcl,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "lock",
  },
  {
    name: IconNameEnum.languages,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "language",
  },
  {
    name: IconNameEnum.metadataTypes,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "table",
  },
  {
    name: IconNameEnum.notifications,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "envelope",
  },
  {
    name: IconNameEnum.passwordVisible,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye",
  },
  {
    name: IconNameEnum.passwordHide,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye-slash",
  },
  {
    name: IconNameEnum.testFile,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.folderEmpty,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "folder",
  },
  {
    name: IconNameEnum.folderOpened,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "folder-open",
  },
  {
    name: IconNameEnum.folderClosed,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "folder",
  },
  {
    name: IconNameEnum.expandAll,
    lib: IconLibEnum.materialIcon,
    icon: "unfold_more",
  },
  {
    name: IconNameEnum.collapseAll,
    lib: IconLibEnum.materialIcon,
    icon: "unfold_less",
  },
  {
    name: IconNameEnum.send,
    lib: IconLibEnum.materialIcon,
    icon: "send",
  },
  {
    name: IconNameEnum.clear,
    lib: IconLibEnum.materialIcon,
    icon: "clear",
  },
  {
    name: IconNameEnum.done,
    lib: IconLibEnum.materialIcon,
    icon: "done",
  },
  {
    name: IconNameEnum.update,
    lib: IconLibEnum.materialIcon,
    icon: "sync",
  },
  {
    name: IconNameEnum.emptyCart,
    lib: IconLibEnum.materialIcon,
    icon: "archive",
  },
  {
    name: IconNameEnum.copyToClipboard,
    lib: IconLibEnum.materialIcon,
    icon: "filter_none",
  },
  {
    name: IconNameEnum.notFound,
    lib: IconLibEnum.materialIcon,
    icon: "mood_bad",
  },
  {
    name: IconNameEnum.up,
    lib: IconLibEnum.materialIcon,
    icon: "keyboard_arrow_up",
  },
  {
    name: IconNameEnum.down,
    lib: IconLibEnum.materialIcon,
    icon: "keyboard_arrow_down",
  },
  {
    name: IconNameEnum.redo,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "redo-alt",
  },
  {
    name: IconNameEnum.sort,
    lib: IconLibEnum.materialIcon,
    icon: "format_list_numbered_rtl",
  },
  {
    name: IconNameEnum.clearCache,
    lib: IconLibEnum.materialIcon,
    icon: "delete_sweep",
  },
  {
    name: IconNameEnum.warning,
    lib: IconLibEnum.materialIcon,
    icon: "warning",
  },
  {
    name: IconNameEnum.autoUpdate,
    lib: IconLibEnum.materialIcon,
    icon: "autorenew",
  },
  {
    name: IconNameEnum.fingerprint,
    lib: IconLibEnum.materialIcon,
    icon: "fingerprint",
  },
  {
    name: IconNameEnum.http,
    lib: IconLibEnum.materialIcon,
    icon: "http",
  },
  {
    name: IconNameEnum.closeChip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "times-circle",
  },
  {
    name: IconNameEnum.success,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.error,
    lib: IconLibEnum.materialIcon,
    icon: "error",
  },
  {
    name: IconNameEnum.zoomOut,
    lib: IconLibEnum.materialIcon,
    icon: "zoom_out",
  },
  {
    name: IconNameEnum.zoomIn,
    lib: IconLibEnum.materialIcon,
    icon: "zoom_in",
  },
  {
    name: IconNameEnum.menuButtons,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "ellipsis-v",
  },
  {
    name: IconNameEnum.orcid,
    lib: IconLibEnum.image,
    icon: "orcid.svg",
  },
  {
    name: IconNameEnum.orcidText,
    lib: IconLibEnum.image,
    icon: "orcid-text.svg",
  },
  {
    name: IconNameEnum.doi,
    lib: IconLibEnum.image,
    icon: "doi.svg",
  },
  {
    name: IconNameEnum.archive,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "book",
  },
  {
    name: IconNameEnum.unigeBlack,
    lib: IconLibEnum.image,
    icon: "unigelogo-black.svg",
  },
  {
    name: IconNameEnum.unigeWhite,
    lib: IconLibEnum.image,
    icon: "unigelogo-white.svg",
  },
  {
    name: IconNameEnum.theme,
    lib: IconLibEnum.materialIcon,
    icon: "style",
  },
  {
    name: IconNameEnum.darkMode,
    lib: IconLibEnum.materialIcon,
    icon: "bedtime",
  },
  {
    name: IconNameEnum.lightMode,
    lib: IconLibEnum.materialIcon,
    icon: "brightness_5",
  },
  {
    name: IconNameEnum.creationDate,
    lib: IconLibEnum.materialIcon,
    icon: "event_note",
  },
  {
    name: IconNameEnum.defaultValue,
    lib: IconLibEnum.materialIcon,
    icon: "settings",
  },
  {
    name: IconNameEnum.change,
    lib: IconLibEnum.materialIcon,
    icon: "swap_horiz",
  },
  {
    name: IconNameEnum.trueValue,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle_outline",
  },
  {
    name: IconNameEnum.falseValue,
    lib: IconLibEnum.materialIcon,
    icon: "highlight_off",
  },
  {
    name: IconNameEnum.dispose,
    lib: IconLibEnum.materialIcon,
    icon: "delete_forever",
  },
  {
    name: IconNameEnum.approveDisposal,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.approveDisposalByOrgUnit,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.extendRetention,
    lib: IconLibEnum.materialIcon,
    icon: "schedule",
  },
  {
    name: IconNameEnum.listView,
    lib: IconLibEnum.materialIcon,
    icon: "view_list",
  },
  {
    name: IconNameEnum.tilesView,
    lib: IconLibEnum.materialIcon,
    icon: "view_module",
  },
  {
    name: IconNameEnum.accessLevelRestricted,
    lib: IconLibEnum.image,
    icon: "access-level-restricted.svg",
  },
  {
    name: IconNameEnum.accessLevelPrivate,
    lib: IconLibEnum.image,
    icon: "access-level-closed.svg",
  },
  {
    name: IconNameEnum.accessLevelPublic,
    lib: IconLibEnum.image,
    icon: "access-level-public.svg",
  },
  {
    name: IconNameEnum.accessLevelUndefined,
    lib: IconLibEnum.image,
    icon: "access-level-undefined.svg",
  },
  {
    name: IconNameEnum.uploadImage,
    lib: IconLibEnum.materialIcon,
    icon: "add_a_photo",
  },
  {
    name: IconNameEnum.help,
    lib: IconLibEnum.materialIcon,
    icon: "help",
  },
  {
    name: IconNameEnum.maintenance,
    lib: IconLibEnum.materialIcon,
    icon: "settings",
  },
  {
    name: IconNameEnum.offline,
    lib: IconLibEnum.materialIcon,
    icon: "power_settings_new",
  },
  {
    name: IconNameEnum.viewNumber,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye",
  },
  {
    name: IconNameEnum.downloadNumber,
    lib: IconLibEnum.materialIcon,
    icon: "get_app",
  },
  {
    name: IconNameEnum.sortUndefined,
    lib: IconLibEnum.materialIcon,
    icon: "filter_list",
  },
  {
    name: IconNameEnum.sortAscending,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "sort-amount-up-alt",
  },
  {
    name: IconNameEnum.sortDescending,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "sort-amount-down",
  },
  {
    name: IconNameEnum.noPreview,
    lib: IconLibEnum.materialIcon,
    icon: "insert_drive_file",
  },
  {
    name: IconNameEnum.fullScreenEnter,
    lib: IconLibEnum.materialIcon,
    icon: "fullscreen",
  },
  {
    name: IconNameEnum.fullScreenLeave,
    lib: IconLibEnum.materialIcon,
    icon: "fullscreen_exit",
  },
  {
    name: IconNameEnum.guidedTour,
    lib: IconLibEnum.materialIcon,
    icon: "beenhere",
  },
  {
    name: IconNameEnum.dataSensitivity,
    lib: IconLibEnum.materialIcon,
    icon: "local_offer",
  },
  {
    name: IconNameEnum.plus,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "plus-square",
  },
  {
    name: IconNameEnum.minus,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "minus-square",
  },
  {
    name: IconNameEnum.rate,
    lib: IconLibEnum.materialIcon,
    icon: "stars",
  },
  {
    name: IconNameEnum.dataSensitivityPartiallySupportedOrange,
    lib: IconLibEnum.image,
    icon: "local-offer-disabled-orange.svg",
  },
  {
    name: IconNameEnum.dataSensitivityPartiallySupportedRed,
    lib: IconLibEnum.image,
    icon: "local-offer-disabled-red.svg",
  },
  {
    name: IconNameEnum.dataSensitivityPartiallySupportedCrimson,
    lib: IconLibEnum.image,
    icon: "local-offer-disabled-crimson.svg",
  },
  {
    name: IconNameEnum.structures,
    lib: IconLibEnum.materialIcon,
    icon: "account_balance",
  },
  {
    name: IconNameEnum.privacy,
    lib: IconLibEnum.materialIcon,
    icon: "vpn_lock",
  },
  {
    name: IconNameEnum.more,
    lib: IconLibEnum.materialIcon,
    icon: "more_horiz",
  },
  {
    name: IconNameEnum.dragToSort,
    lib: IconLibEnum.materialIcon,
    icon: "drag_handle",
  },
  {
    name: IconNameEnum.transfer,
    lib: IconLibEnum.materialIcon,
    icon: "forward",
  },
  {
    name: IconNameEnum.downArrow,
    lib: IconLibEnum.materialIcon,
    icon: "arrow_drop_down",
  },
  {
    name: IconNameEnum.addAll,
    lib: IconLibEnum.materialIcon,
    icon: "library_add",
  },
  {
    name: IconNameEnum.comments,
    lib: IconLibEnum.materialIcon,
    icon: "chat_bubble",
  },
  {
    name: IconNameEnum.validatorComments,
    lib: IconLibEnum.materialIcon,
    icon: "mark_chat_read",
  },
  {
    name: IconNameEnum.resId,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "key",
  },
  {
    name: IconNameEnum.pmid,
    lib: IconLibEnum.image,
    icon: "pmid.png",
  },
  {
    name: IconNameEnum.events,
    lib: IconLibEnum.materialIcon,
    icon: "notifications",
  },
  {
    name: IconNameEnum.file,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file",
  },
  {
    name: IconNameEnum.markAsRead,
    lib: IconLibEnum.materialIcon,
    icon: "drafts",
  },
  {
    name: IconNameEnum.markAsUnread,
    lib: IconLibEnum.materialIcon,
    icon: "markunread",
  },
  {
    name: IconNameEnum.thisIsNotMyDeposit,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-times",
  },
  {
    name: IconNameEnum.fileVersionAccepted,
    lib: IconLibEnum.image,
    icon: "file-version-accepted.svg",
  },
  {
    name: IconNameEnum.fileVersionPublished,
    lib: IconLibEnum.image,
    icon: "file-version-published.svg",
  },
  {
    name: IconNameEnum.fileVersionSubmitted,
    lib: IconLibEnum.image,
    icon: "file-version-submitted.svg",
  },
  {
    name: IconNameEnum.fileSecondary,
    lib: IconLibEnum.materialIcon,
    icon: "attach_file",
  },
  {
    name: IconNameEnum.fileCorrection,
    lib: IconLibEnum.materialIcon,
    icon: "post_add",
  },
  {
    name: IconNameEnum.fileThesisMaster,
    lib: IconLibEnum.materialIcon,
    icon: "school",
  },
  {
    name: IconNameEnum.filePresentationPosterPreprint,
    lib: IconLibEnum.materialIcon,
    icon: "article",
  },
  {
    name: IconNameEnum.fileAgreements,
    lib: IconLibEnum.materialIcon,
    icon: "history_edu",
  },
  {
    name: IconNameEnum.fileOther,
    lib: IconLibEnum.materialIcon,
    icon: "attach_file",
  },
  {
    name: IconNameEnum.question,
    lib: IconLibEnum.materialIcon,
    icon: "help",
  },
  {
    name: IconNameEnum.authorized,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.notAuthorized,
    lib: IconLibEnum.materialIcon,
    icon: "cancel",
  },
  {
    name: IconNameEnum.tree,
    lib: IconLibEnum.materialIcon,
    icon: "account_tree",
  },
  {
    name: IconNameEnum.import,
    lib: IconLibEnum.materialIcon,
    icon: "import_export",
  },
  {
    name: IconNameEnum.autoFix,
    lib: IconLibEnum.materialIcon,
    icon: "auto_fix_high",
  },
  {
    name: IconNameEnum.multiImport,
    lib: IconLibEnum.materialIcon,
    icon: "dynamic_feed",
  },
  {
    name: IconNameEnum.synchronize,
    lib: IconLibEnum.materialIcon,
    icon: "sync",
  },
  {
    name: IconNameEnum.mail,
    lib: IconLibEnum.materialIcon,
    icon: "mail",
  },
  {
    name: IconNameEnum.requestCorrection,
    lib: IconLibEnum.materialIcon,
    icon: "mark_email_read",
  },
  {
    name: IconNameEnum.dashboard,
    lib: IconLibEnum.materialIcon,
    icon: "dashboard",
  },
  {
    name: IconNameEnum.phone,
    lib: IconLibEnum.materialIcon,
    icon: "phone",
  },
  {
    name: IconNameEnum.commentOrQuestion,
    lib: IconLibEnum.materialIcon,
    icon: "contact_support",
  },
  {
    name: IconNameEnum.structureWithoutChild,
    lib: IconLibEnum.materialIcon,
    icon: "arrow_right",
  },
  {
    name: IconNameEnum.structureWithChildOpened,
    lib: IconLibEnum.materialIcon,
    icon: "remove",
  },
  {
    name: IconNameEnum.structureWithChildClosed,
    lib: IconLibEnum.materialIcon,
    icon: "add",
  },
  {
    name: IconNameEnum.cloneDeposit,
    lib: IconLibEnum.materialIcon,
    icon: "file_copy",
  },
  {
    name: IconNameEnum.identifiers,
    lib: IconLibEnum.materialIcon,
    icon: "fingerprint",
  },
  {
    name: IconNameEnum.keyword,
    lib: IconLibEnum.materialIcon,
    icon: "local_offer",
  },
  {
    name: IconNameEnum.author,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.mandator,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-tag",
  },
  {
    name: IconNameEnum.director,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-tie",
  },
  {
    name: IconNameEnum.editor,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-tag",
  },
  {
    name: IconNameEnum.guest_editor,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-tag",
  },
  {
    name: IconNameEnum.photographer,
    lib: IconLibEnum.materialIcon,
    icon: "photo_camera",
  },
  {
    name: IconNameEnum.translator,
    lib: IconLibEnum.materialIcon,
    icon: "translate",
  },
  {
    name: IconNameEnum.collaboration,
    lib: IconLibEnum.materialIcon,
    icon: "groups",
  },
  {
    name: IconNameEnum.filePdf,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-pdf",
  },
  {
    name: IconNameEnum.fileDoc,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-word",
  },
  {
    name: IconNameEnum.fileZip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-zipper",
  },
  {
    name: IconNameEnum.fileLines,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-lines",
  },
  {
    name: IconNameEnum.fileImage,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-image",
  },
  {
    name: IconNameEnum.fileExcel,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-excel",
  },
  {
    name: IconNameEnum.fileCsv,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-csv",
  },
  {
    name: IconNameEnum.pinboard,
    lib: IconLibEnum.materialIcon,
    icon: "bookmarks",
  },
  {
    name: IconNameEnum.addToPinboard,
    lib: IconLibEnum.materialIcon,
    icon: "bookmark_add",
  },
  {
    name: IconNameEnum.changeToCanonical,
    lib: IconLibEnum.materialIcon,
    icon: "donut_large",
  },
  {
    name: IconNameEnum.changeToCompleted,
    lib: IconLibEnum.materialIcon,
    icon: "done_all",
  },
  {
    name: IconNameEnum.removeToPinboard,
    lib: IconLibEnum.materialIcon,
    icon: "bookmark_remove",
  },
  {
    name: IconNameEnum.generateBibliography,
    lib: IconLibEnum.materialIcon,
    icon: "ios_share",
  },
  {
    name: IconNameEnum.exportBibliography,
    lib: IconLibEnum.materialIcon,
    icon: "file_download",
  },
  {
    name: IconNameEnum.excluded,
    lib: IconLibEnum.materialIcon,
    icon: "do_disturb",
  },
  {
    name: IconNameEnum.share,
    lib: IconLibEnum.materialIcon,
    icon: "share",
  },
  {
    name: IconNameEnum.mergeUser,
    lib: IconLibEnum.materialIcon,
    icon: "merge_type",
  },
  {
    name: IconNameEnum.note,
    lib: IconLibEnum.materialIcon,
    icon: "sticky_note_2",
  },
  {
    name: IconNameEnum.datasets,
    lib: IconLibEnum.materialIcon,
    icon: "storage",
  },
  {
    name: IconNameEnum.correction,
    lib: IconLibEnum.materialIcon,
    icon: "rule",
  },
  {
    name: IconNameEnum.award,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "award",
  },
  {
    name: IconNameEnum.otherTitle,
    lib: IconLibEnum.materialIcon,
    icon: "title",
  },
  {
    name: IconNameEnum.publisher,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "book-bookmark",
  },
  {
    name: IconNameEnum.pagination,
    lib: IconLibEnum.materialIcon,
    icon: "auto_stories",
  },
  {
    name: IconNameEnum.publishedIn,
    lib: IconLibEnum.materialIcon,
    icon: "book-open",
  },
  {
    name: IconNameEnum.presentedAt,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "person-chalkboard",
  },
  {
    name: IconNameEnum.discipline,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "graduation-cap",
  },
  {
    name: IconNameEnum.edition,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "pen-nib",
  },
  {
    name: IconNameEnum.facebook,
    lib: IconLibEnum.fontAwesomeBrand,
    icon: "square-facebook",
  },
  {
    name: IconNameEnum.twitter,
    lib: IconLibEnum.fontAwesomeBrand,
    icon: "twitter",
  },
  {
    name: IconNameEnum.linkedin,
    lib: IconLibEnum.fontAwesomeBrand,
    icon: "linkedin",
  },
  {
    name: IconNameEnum.oaiMetadataPrefixes,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "passport",
  },
  {
    name: IconNameEnum.oaiSets,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "object-group",
  },
  {
    name: IconNameEnum.menu,
    lib: IconLibEnum.materialIcon,
    icon: "menu",
  },
  {
    name: IconNameEnum.excludedFacet,
    lib: IconLibEnum.materialIcon,
    icon: "do_disturb",
  },
  {
    name: IconNameEnum.logInMfa,
    lib: IconLibEnum.materialIcon,
    icon: "admin_panel_settings",
  },
];
