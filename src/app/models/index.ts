/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - index.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-restricted-imports */
import {DepositFormModel} from "@app/features/deposit/models/deposit-form-definition.model";
import {PublicationStatistic as PublicationStatisticPartial} from "@app/generated-api/model/publication-statistic.partial.model";
import {Enums} from "@enums";
import {
  ApplicationRole as SolidifyApplicationRole,
  BaseResource,
  BaseResourceWithLabels,
  Label as SolidifyLabel,
  MappingObject,
  OverrideType,
  PersonWithEmail,
  StatusModel,
  SystemPropertyPartial as SolidifySystemPropertyPartial,
  User as SolidifyUser,
  UserApplicationRoleEnum,
} from "solidify-frontend";
import {ApplicationRole as ApplicationRolePartial} from "../generated-api/model/application-role.partial.model";
import {ChangeInfo as ChangeInfoPartial} from "../generated-api/model/change-info.partial.model";
import {Comment as CommentPartial} from "../generated-api/model/comment.partial.model";
import {Contributor as ContributorPartial} from "../generated-api/model/contributor.partial.model";
import {EventType as ApplicationEventEventTypePartial} from "../generated-api/model/event-type.partial.model";
import {Event as ApplicationEventPartial} from "../generated-api/model/event.partial.model";
import {IndexFieldAlias as IndexFieldAliasPartial} from "../generated-api/model/index-field-alias.partial.model";
import {Institution as InstitutionPartial} from "../generated-api/model/institution.partial.model";
import {Language as LanguagePartial} from "../generated-api/model/language.partial.model";
import {LicenseGroup as LicenseGroupPartial} from "../generated-api/model/license-group.partial.model";
import {License as LicensePartial} from "../generated-api/model/license.partial.model";
import {NotificationType as NotificationTypePartial} from "../generated-api/model/notification-type.partial.model";
import {Notification as NotificationPartial} from "../generated-api/model/notification.partial.model";
import {OAISet as OaiSetPartial} from "../generated-api/model/oai-set.partial.model";
import {Person as PersonPartial} from "../generated-api/model/person.partial.model";
import {PublicContributorDTO as PublicContributorDTOPartial} from "../generated-api/model/public-contributor-dto.partial.model";
import {PublicationDownload as PublicationDownloadPartial} from "../generated-api/model/publication-download.partial.model";
import {PublicationSubSubtype as PublicationSubSubtypePartial} from "../generated-api/model/publication-sub-subtype.partial.model";
import {PublicationSubtype as PublicationSubtypePartial} from "../generated-api/model/publication-subtype.partial.model";
import {
  PublicationType,
  PublicationType as PublicationTypePartial,
} from "../generated-api/model/publication-type.partial.model";
import {Publication as PublicationPartial} from "../generated-api/model/publication.partial.model";
import {ResearchGroup as ResearchGroupPartial} from "../generated-api/model/research-group.partial.model";
import {Role as RolePartial} from "../generated-api/model/role.partial.model";
import {Structure as StructurePartial} from "../generated-api/model/structure.partial.model";
import {SystemProperties as SystemPropertyPartial} from "../generated-api/model/system-properties.partial.model";
import {User as UserPartial} from "../generated-api/model/user.partial.model";
/* eslint-enable no-restricted-imports */

export type BaseResourceExtended = {
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
  _links?: Link[];
} & BaseResource;

export interface Link {
  rel?: string;
  href?: string;
  hreflang?: string;
  media?: string;
  title?: string;
  type?: string;
  deprecation?: string;
  profile?: string;
  name?: string;
}

export interface AdvancedSearchCriteria {
  alias?: string;
  labels?: Label[];
  type?: Enums.Criteria.TypeEnum;
  selectableValues?: EnumValueTranslated[];
  uncheckedCriteria?: AdvancedSearchCriteria;
  checkedCriteria?: AdvancedSearchCriteria;
  isChildOf?: AdvancedSearchCriteria;
  isCombination?: boolean;
}

export type ApplicationRole = OverrideType<ApplicationRolePartial, {
  resId?: UserApplicationRoleEnum;
}> & SolidifyApplicationRole & OverrideType<BaseResourceExtended, {
  resId?: UserApplicationRoleEnum;
}>;

export type ApplicationEvent = OverrideType<ApplicationEventPartial, {
  eventType?: ApplicationEventEventType;
  publication?: Deposit;
  triggerBy?: Person;
}>;

export type ApplicationEventEventType = OverrideType<ApplicationEventEventTypePartial, {
  name?: string;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
}>;

export interface BibliographyFormat {
  name?: string;
  labels?: Label[];
}

export type ChangeInfo = OverrideType<ChangeInfoPartial>;

export type Comment = OverrideType<CommentPartial, {
  person?: Person;
  publication?: Deposit;
}> & BaseResourceExtended;

export interface ContactableContributor {
  cnIndividu?: string;
  firstName?: string;
  lastName?: string;
}

export type ContributorRockSolid = {
  cnIndividu?: string;
  firstname?: string;
  lastname?: string;
  yearOfBirth?: number;
  function?: string;
  structure?: string;
  employee?: boolean;
} & BaseResource;

export type Contributor = OverrideType<ContributorPartial, {
  fullName?: string;
  displayName?: string;
  orcid?: string;
}> & BaseResourceExtended;

export type EnumValueTranslated = {
  value?: string;
  labels?: Label[];
} & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResource;

export type Institution = OverrideType<InstitutionPartial, {
  logo?: Logo;
}> & BaseResourceExtended;

export interface JournalTitleDTO {
  mainTitle?: string;
  startingYear?: string;
  keytitle?: string;
  publisher?: string;
  issn?: string;
  issnl?: string;
  url?: string;
  countryCode?: string;
}

export type Language = OverrideType<LanguagePartial, {
  labels?: Label[];
  sortValue?: number;
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceExtended;

export type License = OverrideType<LicensePartial, {
  licenseGroup?: LicenseGroup;
  labels?: Label[];
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceExtended;

export type LicenseGroup = OverrideType<LicenseGroupPartial, {
  labels?: Label[];
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceExtended;

export interface Logo {
  resId?: string;
  fileName?: string;
  fileSize?: number;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
}

export type Notification = OverrideType<NotificationPartial, {
  event?: ApplicationEvent;
  readTime?: string;
}> & BaseResourceExtended;

export type NotificationType = OverrideType<NotificationTypePartial, {
  eventType?: ApplicationEventEventType;
  notifiedRoleStructure?: Role;
  notifiedApplicationRole?: ApplicationRole;
}> & BaseResource;

export type OaiSet = OverrideType<OaiSetPartial> & BaseResourceExtended;

export type Person = OverrideType<PersonPartial, {
  institutions?: any[];
  notificationTypes?: NotificationType[];
  researchGroups?: ResearchGroup[];
  structures?: Structure[];
  roles?: Role[];
  avatar?: Logo;
  verifiedOrcid?: boolean;
}> & BaseResourceExtended;

export type PersonNotificationType = {
  notificationFrequency?: Enums.NotificationType.FrequencyEnum;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
} & BaseResource;

export interface PublicationContactMessage {
  senderName?: string;
  senderEmail?: string;
  messageContent?: string;
  recipientCnIndividu?: string;
}

export interface AskCorrectionMessage {
  senderName?: string;
  senderEmail?: string;
  messageContent?: string;
}

export interface PublicationImportDTO {
  doi?: string;
  pmid?: string;
  creatorId?: string;
}

export type PublicContributorDto = OverrideType<PublicContributorDTOPartial, {
  structures?: Structure[];
  researchGroups?: ResearchGroup[];
}>;

export type ContributorRole = {
  value?: Enums.Deposit.RoleContributorEnum;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
} & BaseResourceExtended;

export type Creator = {
  avatar?: any;
  creation?: ChangeInfo;
  hasAuthenticatedOrcid?: boolean;
  lastUpdate?: ChangeInfo;
  orcid?: string;
  resId?: string;
} & PersonWithEmail;

export type Deposit = OverrideType<PublicationPartial, {
  targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum;
  formStep?: Enums.Deposit.StepEnum;
  depositFormMode?: DepositFormModel;
  validationStructureId?: string;
  depositFormModel?: DepositFormModel;
  firstAuthor?: string;
  publicationYear?: string;
  pmid?: string;
  doi?: string;
  creator?: Creator;
  numberOfFiles?: number;
  thumbnail?: Logo;
  metadataStructures?: Structure[] | any;
  validationStructures?: Structure[] | any;
  structures?: Structure[]; // used to store metadataStructures or validationStructures
  lastStatusUpdate?: string;
  lastEditor?: PersonWithEmail;
}> & BaseResourceExtended;

export type DepositType = OverrideType<PublicationTypePartial> & BaseResourceExtended;

export type DepositStructure = {
  linkType?: Enums.DepositStructure.LinkTypesEnum;
} & BaseResourceExtended;

export type DepositSubtype = OverrideType<PublicationSubtypePartial, {
  publicationType?: PublicationType;
  descriptions?: Label[];
}> & BaseResourceWithLabels;

export type DepositSubSubtype = OverrideType<PublicationSubSubtypePartial, {
  descriptions?: Label[];
  labels?: Label[];
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceWithLabels;

export type DocumentFile = {
  publication?: Deposit;
  documentFileType?: DocumentFileType;
  license?: License;
  label?: string;
  sourceData?: string;
  finalData?: string;
  fileName?: string;
  fileSize?: number;
  mimetype?: string;
  notSureForLicense?: boolean;
  status?: Enums.DocumentFile.StatusEnum;
  statusMessage?: string;
  accessLevel?: Enums.DocumentFile.AccessLevelEnum;
  embargoAccessLevel?: Enums.DocumentFile.AccessLevelEnum;
  embargoEndDate?: string;
  sortValue?: number;
} & BaseResourceExtended;

export type DocumentFileType = {
  level?: Enums.DocumentFileType.FileTypeLevelEnum;
  value?: Enums.Deposit.FileTypeStringEnum;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
} & BaseResourceWithLabels & BaseResourceExtended;

export type IndexFieldAlias = OverrideType<IndexFieldAliasPartial, {
  labels?: Label[];
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceExtended;

export type PublicationSubtypeContributorRoleDTO = {
  sortValue?: number;
  value?: Enums.Deposit.RoleContributorEnum;
  contributorRoleId?: string;
  publicationSubtypeId?: string;
} & BaseResourceWithLabels;

export interface PublicationSubtypeDocumentFileTypeGroup {
  level?: Enums.DocumentFileType.FileTypeLevelEnum;
  publicationSubtypeDocumentFileTypes?: PublicationSubtypeDocumentFileTypeDTO[];
}

export type PublicationSubtypeDocumentFileTypeDTO = {
  level?: Enums.DocumentFileType.FileTypeLevelEnum;
  sortValue?: number;
  value?: string;
  documentFileTypeId?: string;
  publicationSubtypeId?: string;
} & BaseResourceWithLabels;

export type Label = OverrideType<SolidifyLabel, {
  languageCode?: Enums.Language.LanguageV2Enum;
  text?: string;
}>;

export interface LargeLabel {
  text: string;
  language: Language;
}

export interface OpenAireProjectDTO {
  program?: string;
  code?: string;
  acronym?: string;
  title?: string;
  funding?: OpenAireFundingDTO;
}

export interface OpenAireFundingDTO {
  name?: string;
  shortName?: string;
  jurisdiction?: string;
  description?: string;
}

export type ResearchGroup = OverrideType<ResearchGroupPartial> & BaseResourceExtended;

export interface Romeo {
  errors: string[];
  issn?: string;
  listedInDoaj?: boolean;
  title?: string;
  journalType?: string;
  sherpaRomeoUrl?: string;
  footer?: string[];
  versions?: RomeoVersions;
}

export interface RomeoVersions {
  published?: RomeoVersion;
  submitted?: RomeoVersion;
  accepted?: RomeoVersion;
}

export interface RomeoVersion {
  version?: string;
  policies?: RomeoPolicie[];
}

export interface RomeoPolicie {
  oaDiffusion?: RomeoOaDiffusion;
  licences?: string[];
  conditions?: string[];
  embargo?: string;
}

export interface RomeoOaDiffusion {
  icon?: string;
  text?: string;
}

export type Role = OverrideType<RolePartial, {
  labels?: Label[];
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceExtended;

export type AoUStatusModel = OverrideType<StatusModel, {
  hideInFilter?: boolean;
}> & StatusModel;

export type StoredSearch = {
  creator?: Person;
  name?: string;
  criteria?: StoredSearchCriteria[];
  withRestrictedAccessMasters?: boolean;
  usedForBibliography?: boolean;
} & BaseResource;

export interface StoredSearchCriteria {
  type?: Enums.StoredSearch.Type;
  booleanClauseType?: Enums.StoredSearch.BooleanClauseType;
  field?: string;
  multiMatchFields?: string[];
  value?: string;
  terms?: string[];
  upperValue?: string;
  lowerValue?: string;
  source?: Enums.StoredSearch.CriteriaSource;
}

export type Structure = OverrideType<StructurePartial, {
  status?: Enums.Structure.StatusEnum;
  parentStructure?: Structure;
}> & BaseResourceExtended;

export type SystemProperty = OverrideType<SystemPropertyPartial, {
  defaultContributorRoles?: MappingObject<Enums.Deposit.DepositFriendlyNameSubTypeEnum, Enums.Deposit.RoleContributorEnum>;
  orcidContributorRolesToSendToProfile?: Enums.Deposit.RoleContributorEnum[];
  orcid?: OrcidConfig;
  staticPages?: StaticPage[];
  systemIndexFieldAlias?: SystemIndexFieldAlias;
}> & SolidifySystemPropertyPartial;

export interface SystemIndexFieldAlias {
  openAccess?: string;
  principalFileAccessLevel?: string;
}

export interface OrcidConfig {
  authorizeUrl?: string;
  clientId?: string;
  scope?: string;
}

export interface StaticPage {
  labels?: StaticPageLabel[];
}

export type StaticPageLabel = OverrideType<Label, {
  url?: string;
}>;

export interface UserInfo {
  resId?: string;
  firstName?: string;
  lastName?: string;
  externalUid?: string;
}

export type User = OverrideType<UserPartial, {
  person?: Person;
  accessToken?: string;
  refreshToken?: string;
  applicationRole?: ApplicationRole;
  lastLoginIpAddress?: string;
  lastLoginTime?: string;
}> & BaseResourceExtended & OverrideType<SolidifyUser, {
  applicationRole?: ApplicationRole;
}>;

export interface MetadataDifference {
  fieldName: string;
  oldValue: string;
  newValue: string;
}

export type PublicationDownload = OverrideType<PublicationDownloadPartial>;

export type PublicationStatistic = OverrideType<PublicationStatisticPartial> & BaseResource;
