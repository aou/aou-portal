/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - index.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-restricted-imports */
import {Publication as PublicationPartial} from "@app/generated-api/model/publication.partial.model";
import {Structure as StructurePartial} from "@app/generated-api/model/structure.partial.model";
import {AoUStatusModel} from "@models";
import {ColorHexaEnum} from "@shared/enums/color-hexa.enum";
/* eslint-enable no-restricted-imports */
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";

import {
  KeyValue,
  LanguagePartialEnum,
  MARK_AS_TRANSLATABLE,
  StatusModel,
} from "solidify-frontend";

export namespace Enums {
  export namespace HighlightJS {
    export type HighlightLanguageEnum =
      "xml"
      | "json";
    export const HighlightLanguageEnum = {
      xml: "xml" as HighlightLanguageEnum,
      json: "json" as HighlightLanguageEnum,
    };
  }

  export namespace Language {
    type LanguageExtendEnum =
      "en"
      | "fr"
      | "de"
      | "it"
      | "es"
      | "pt"
      | "ar"
      | "zh"
      | "ru"
      | "nl"
      | "tr"
      | "ja"
      | "cs"
      | "sq"
      | "el"
      | "hu"
      | "bg"
      | "la"
      | "ro"
      | "mis";
    const LanguageExtendEnum = {
      en: "en" as LanguageExtendEnum,
      fr: "fr" as LanguageExtendEnum,
      de: "de" as LanguageExtendEnum,
      it: "it" as LanguageExtendEnum,
      es: "es" as LanguageExtendEnum,
      pt: "pt" as LanguageExtendEnum,
      ar: "ar" as LanguageExtendEnum,
      zh: "zh" as LanguageExtendEnum,
      ru: "ru" as LanguageExtendEnum,
      nl: "nl" as LanguageExtendEnum,
      tr: "tr" as LanguageExtendEnum,
      ja: "ja" as LanguageExtendEnum,
      cs: "cs" as LanguageExtendEnum,
      sq: "sq" as LanguageExtendEnum,
      el: "el" as LanguageExtendEnum,
      hu: "hu" as LanguageExtendEnum,
      bg: "bg" as LanguageExtendEnum,
      la: "la" as LanguageExtendEnum,
      ro: "ro" as LanguageExtendEnum,
      mis: "mis" as LanguageExtendEnum,
    };
    // tslint:disable-next-line:variable-name
    export const LanguageEnum = {...LanguagePartialEnum, ...LanguageExtendEnum} as Omit<typeof LanguagePartialEnum, keyof typeof LanguageExtendEnum> & typeof LanguageExtendEnum;
    export type LanguageEnum = typeof LanguageEnum[keyof typeof LanguageEnum];
    export const LanguageEnumTranslate: KeyValue[] = [
      {
        key: LanguageEnum.en,
        value: MARK_AS_TRANSLATABLE("language.english"),
      },
      {
        key: LanguageEnum.de,
        value: MARK_AS_TRANSLATABLE("language.german"),
      },
      {
        key: LanguageEnum.fr,
        value: MARK_AS_TRANSLATABLE("language.french"),
      },
      {
        key: LanguageEnum.es,
        value: MARK_AS_TRANSLATABLE("language.spanish"),
      },
      {
        key: LanguageEnum.it,
        value: MARK_AS_TRANSLATABLE("language.italian"),
      },
      {
        key: LanguageEnum.pt,
        value: MARK_AS_TRANSLATABLE("language.portuguese"),
      },
      {
        key: LanguageEnum.ar,
        value: MARK_AS_TRANSLATABLE("language.arabic"),
      },
      {
        key: LanguageEnum.zh,
        value: MARK_AS_TRANSLATABLE("language.chinese"),
      },
      {
        key: LanguageEnum.ru,
        value: MARK_AS_TRANSLATABLE("language.russian"),
      },
      {
        key: LanguageEnum.nl,
        value: MARK_AS_TRANSLATABLE("language.dutch"),
      },
      {
        key: LanguageEnum.tr,
        value: MARK_AS_TRANSLATABLE("language.turkish"),
      },
      {
        key: LanguageEnum.ja,
        value: MARK_AS_TRANSLATABLE("language.japanese"),
      },
      {
        key: LanguageEnum.cs,
        value: MARK_AS_TRANSLATABLE("language.czech"),
      },
      {
        key: LanguageEnum.sq,
        value: MARK_AS_TRANSLATABLE("language.albanian"),
      },
      {
        key: LanguageEnum.el,
        value: MARK_AS_TRANSLATABLE("language.greek"),
      },
      {
        key: LanguageEnum.hu,
        value: MARK_AS_TRANSLATABLE("language.hungarian"),
      },
      {
        key: LanguageEnum.bg,
        value: MARK_AS_TRANSLATABLE("language.bulgarian"),
      },
      {
        key: LanguageEnum.la,
        value: MARK_AS_TRANSLATABLE("language.latin"),
      },
      {
        key: LanguageEnum.ro,
        value: MARK_AS_TRANSLATABLE("language.romanian"),
      },
      {
        key: LanguageEnum.mis,
        value: MARK_AS_TRANSLATABLE("language.miscellaneous"),
      },
    ];

    export type LanguageV2Enum =
      "eng"
      | "fre"
      | "ger"
      | "deu"
      | "ita"
      | "spa"
      | "por"
      | "ara"
      | "mis"
      | "chi"
      | "rus"
      | "dut"
      | "tur"
      | "jpn"
      | "cze"
      | "alb"
      | "gre"
      | "hun"
      | "bul"
      | "lat"
      | "rum";

    export const LanguageV2Enum = {
      eng: "eng" as LanguageV2Enum,
      fre: "fre" as LanguageV2Enum,
      ger: "ger" as LanguageV2Enum,
      deu: "deu" as LanguageV2Enum,
      ita: "ita" as LanguageV2Enum,
      spa: "spa" as LanguageV2Enum,
      por: "por" as LanguageV2Enum,
      ara: "ara" as LanguageV2Enum,
      mis: "mis" as LanguageV2Enum,
      chi: "chi" as LanguageV2Enum,
      rus: "rus" as LanguageV2Enum,
      dut: "dut" as LanguageV2Enum,
      tur: "tur" as LanguageV2Enum,
      jpn: "jpn" as LanguageV2Enum,
      cze: "cze" as LanguageV2Enum,
      alb: "alb" as LanguageV2Enum,
      gre: "gre" as LanguageV2Enum,
      hun: "hun" as LanguageV2Enum,
      bul: "bul" as LanguageV2Enum,
      lat: "lat" as LanguageV2Enum,
      rum: "rum" as LanguageV2Enum,
    };

    export const LanguageIso639V2ToV1 = (languageIso2: LanguageV2Enum): LanguageEnum => {
      switch (languageIso2) {
        case LanguageV2Enum.eng:
          return LanguageEnum.en;
        case LanguageV2Enum.fre:
          return LanguageEnum.fr;
        case LanguageV2Enum.deu:
        case LanguageV2Enum.ger:
          return LanguageEnum.de;
        case LanguageV2Enum.spa:
          return LanguageEnum.es;
        case LanguageV2Enum.ita:
          return LanguageEnum.it;
        case LanguageV2Enum.por:
          return LanguageEnum.pt;
        case LanguageV2Enum.ara:
          return LanguageEnum.ar;
        case LanguageV2Enum.chi:
          return LanguageEnum.zh;
        case LanguageV2Enum.rus:
          return LanguageEnum.ru;
        case LanguageV2Enum.dut:
          return LanguageEnum.nl;
        case LanguageV2Enum.tur:
          return LanguageEnum.tr;
        case LanguageV2Enum.jpn:
          return LanguageEnum.ja;
        case LanguageV2Enum.cze:
          return LanguageEnum.cs;
        case LanguageV2Enum.alb:
          return LanguageEnum.sq;
        case LanguageV2Enum.gre:
          return LanguageEnum.el;
        case LanguageV2Enum.hun:
          return LanguageEnum.hu;
        case LanguageV2Enum.bul:
          return LanguageEnum.bg;
        case LanguageV2Enum.lat:
          return LanguageEnum.la;
        case LanguageV2Enum.rum:
          return LanguageEnum.ro;
        case LanguageV2Enum.mis:
          return LanguageEnum.mis;
        default:
          return null;
      }
    };

    export const LanguageIso639V1ToV2 = (languageIso1: LanguageEnum): LanguageV2Enum => {
      switch (languageIso1) {
        case LanguageEnum.en:
          return LanguageV2Enum.eng;
        case LanguageEnum.fr:
          return LanguageV2Enum.fre;
        case LanguageEnum.de:
          return LanguageV2Enum.deu;
        case LanguageEnum.es:
          return LanguageV2Enum.spa;
        case LanguageEnum.it:
          return LanguageV2Enum.ita;
        case LanguageEnum.pt:
          return LanguageV2Enum.por;
        case LanguageEnum.ar:
          return LanguageV2Enum.ara;
        case LanguageEnum.zh:
          return LanguageV2Enum.chi;
        case LanguageEnum.ru:
          return LanguageV2Enum.rus;
        case LanguageEnum.nl:
          return LanguageV2Enum.dut;
        case LanguageEnum.tr:
          return LanguageV2Enum.tur;
        case LanguageEnum.ja:
          return LanguageV2Enum.jpn;
        case LanguageEnum.cs:
          return LanguageV2Enum.cze;
        case LanguageEnum.sq:
          return LanguageV2Enum.alb;
        case LanguageEnum.el:
          return LanguageV2Enum.gre;
        case LanguageEnum.hu:
          return LanguageV2Enum.hun;
        case LanguageEnum.bg:
          return LanguageV2Enum.bul;
        case LanguageEnum.la:
          return LanguageV2Enum.lat;
        case LanguageEnum.ro:
          return LanguageV2Enum.rum;
        case LanguageEnum.mis:
          return LanguageV2Enum.mis;
        default:
          return null;
      }
    };
  }

  export namespace Facet {
    export type Name =
      "subtype"
      | "globalSearch"
      | "contributors.fullName.keyword"
      | "principalFileAccessLevel"
      | "openAccess"
      | "diffusion"
      | "fulltextVersion"
      | "files.license"
      | "keywords";
    export const Name = {
      SUBTYPE: "subtype" as Name,
      GLOBAL_SEARCH: "globalSearch" as Name,
      CONTRIBUTORS: "contributors.fullName.keyword" as Name,
      PRINCIPAL_FILE_ACCESS_LEVEL: "principalFileAccessLevel" as Name,
      OPEN_ACCESS: "openAccess" as Name,
      DIFFUSION: "diffusion" as Name,
      FULLTEXT_VERSION: "fulltextVersion" as Name,
      FILES_LICENSE: "files.license" as Name,
      KEYWORDS: "keywords" as Name,
      PUBLISHED_IN: "publishedIn" as Name,
      LANGUAGE: "languages" as Name,
      IS_BEFORE_UNIGE: "isBeforeUnige" as Name,
    };

    export const NameTranslate: AoUStatusModel[] = [
      {
        key: Name.SUBTYPE,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.subtype"),
      },
      {
        key: Name.GLOBAL_SEARCH,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.subtype"),
      },
      {
        key: Name.CONTRIBUTORS,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.globalSearch"),
      },
      {
        key: Name.DIFFUSION,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.contributors"),
      },
      {
        key: Name.PRINCIPAL_FILE_ACCESS_LEVEL,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.principalFileAccessLevel"),
      },
      {
        key: Name.OPEN_ACCESS,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.openAccess"),
      },
      {
        key: Name.FULLTEXT_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.diffusion"),
      },
      {
        key: Name.FILES_LICENSE,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.fulltextVersion"),
      },
      {
        key: Name.KEYWORDS,
        value: MARK_AS_TRANSLATABLE("enum.facet.name.files"),
      },
    ];

    export namespace FacetValue {

      export type PrincipalFilesAccessLevel =
        "Public"
        | "Restricted"
        | "Private"
        | "noFulltext";
      export const PrincipalFilesAccessLevel = {
        PUBLIC: "Public" as PrincipalFilesAccessLevel,
        RESTRICTED: "Restricted" as PrincipalFilesAccessLevel,
        PRIVATE: "Private" as PrincipalFilesAccessLevel,
        NO_FULLTEXT: "noFulltext" as PrincipalFilesAccessLevel,
      };

      export const PrincipalFilesAccessLevelTranslate: StatusModel[] = [
        {
          key: PrincipalFilesAccessLevel.PUBLIC,
          value: MARK_AS_TRANSLATABLE("enum.facet.principalFilesAccessLevel.public"),
          backgroundColorHexa: ColorHexaEnum.greenOpenAccess,
        },
        {
          key: PrincipalFilesAccessLevel.RESTRICTED,
          value: MARK_AS_TRANSLATABLE("enum.facet.principalFilesAccessLevel.restricted"),
          backgroundColorHexa: ColorHexaEnum.pinkUnige,
        },
        {
          key: PrincipalFilesAccessLevel.PRIVATE,
          value: MARK_AS_TRANSLATABLE("enum.facet.principalFilesAccessLevel.private"),
          backgroundColorHexa: ColorHexaEnum.greyClosedAccess,
        },
        {
          key: PrincipalFilesAccessLevel.NO_FULLTEXT,
          value: MARK_AS_TRANSLATABLE("enum.facet.principalFilesAccessLevel.noFullText"),
          backgroundColorHexa: ColorHexaEnum.lightGrey,
        },
      ];

      export type OpenAccessValue =
        "openAccess"
        | "notOpenAccess"
        | "embargoedAccess";
      export const OpenAccessValue = {
        OPEN_ACCESS: "openAccess" as OpenAccessValue,
        NOT_OPEN_ACCESS: "notOpenAccess" as OpenAccessValue,
        EMBARGOED_ACCESS: "embargoedAccess" as OpenAccessValue,
      };

      export const OpenAccessValueTranslate: StatusModel[] = [
        {
          key: OpenAccessValue.OPEN_ACCESS,
          value: MARK_AS_TRANSLATABLE("enum.facet.openAccessValue.openAccess"),
          backgroundColorHexa: ColorHexaEnum.orangeOpenAccess,
        },
        {
          key: OpenAccessValue.EMBARGOED_ACCESS,
          value: MARK_AS_TRANSLATABLE("enum.facet.openAccessValue.embargoedAccess"),
          backgroundColorHexa: ColorHexaEnum.orangeEmbargoedOpenAccess,
        },
        {
          key: OpenAccessValue.NOT_OPEN_ACCESS,
          value: MARK_AS_TRANSLATABLE("enum.facet.openAccessValue.notOpenAccess"),
          backgroundColorHexa: ColorHexaEnum.mediumLightGrey,
        },
      ];

      export type Diffusion =
        "openAccess"
        | "embargoedAccess"
        | "restrictedAccess"
        | "closedAccess";
      export const Diffusion = {
        OPEN_ACCESS: "openAccess" as Diffusion,
        EMBARGOED_ACCESS: "embargoedAccess" as Diffusion,
        RESTRICTED_ACCESS: "restrictedAccess" as Diffusion,
        CLOSED_ACCESS: "closedAccess" as Diffusion,
      };

      export const DiffusionTranslate: StatusModel[] = [
        {
          key: Diffusion.OPEN_ACCESS,
          value: MARK_AS_TRANSLATABLE("enum.facet.diffusion.openAccess"),
          backgroundColorHexa: ColorHexaEnum.greenOpenAccess,
        },
        {
          key: Diffusion.EMBARGOED_ACCESS,
          value: LabelTranslateEnum.diffusionEmbargoedAccess,
          backgroundColorHexa: ColorHexaEnum.yellow,
        },
        {
          key: Diffusion.RESTRICTED_ACCESS,
          value: MARK_AS_TRANSLATABLE("enum.facet.diffusion.restrictedAccess"),
          backgroundColorHexa: ColorHexaEnum.pinkUnige,
        },
        {
          key: Diffusion.CLOSED_ACCESS,
          value: MARK_AS_TRANSLATABLE("enum.facet.diffusion.closedAccess"),
          backgroundColorHexa: ColorHexaEnum.greyClosedAccess,
        },
      ];

      export type FulltextVersions =
        "publishedVersion"
        | "acceptedVersion"
        | "extract"
        | "unknown"
        | "submittedVersion";
      export const FulltextVersions = {
        PUBLISHED_VERSION: "publishedVersion" as FulltextVersions,
        ACCEPTED_VERSION: "acceptedVersion" as FulltextVersions,
        EXTRACT: "extract" as FulltextVersions,
        UNKNOWN: "unknown" as FulltextVersions,
        SUBMITTED_VERSION: "submittedVersion" as FulltextVersions,
        NO_FULLTEXT: "noFulltext" as FulltextVersions,
        RECORDING: "recording" as FulltextVersions,
      };

      export const FulltextVersionsTranslate: StatusModel[] = [
        {
          key: FulltextVersions.PUBLISHED_VERSION,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.publishedVersion"),
        },
        {
          key: FulltextVersions.ACCEPTED_VERSION,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.acceptedVersion"),
        },
        {
          key: FulltextVersions.EXTRACT,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.extract"),
        },
        {
          key: FulltextVersions.UNKNOWN,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.unknown"),
        },
        {
          key: FulltextVersions.SUBMITTED_VERSION,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.submittedVersion"),
        },
        {
          key: FulltextVersions.NO_FULLTEXT,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.noFulltext"),
        },
        {
          key: FulltextVersions.RECORDING,
          value: MARK_AS_TRANSLATABLE("enum.facet.fulltextVersions.recording"),
        },
      ];

      export const IsBeforeUnigeTranslate: StatusModel[] = [
        {
          key: "true",
          value: MARK_AS_TRANSLATABLE("enum.facet.isBeforeUnige.true"),
        },
        {
          key: "false",
          value: MARK_AS_TRANSLATABLE("enum.facet.isBeforeUnige.false"),
        },
      ];

      export const LanguagesTranslate: StatusModel[] = [
        {
          key: "alb",
          value: MARK_AS_TRANSLATABLE("language.albanian"),
        },
        {
          key: "ara",
          value: MARK_AS_TRANSLATABLE("language.arabic"),
        },
        {
          key: "bul",
          value: MARK_AS_TRANSLATABLE("language.bulgarian"),
        },
        {
          key: "chi",
          value: MARK_AS_TRANSLATABLE("language.chinese"),
        },
        {
          key: "cze",
          value: MARK_AS_TRANSLATABLE("language.czech"),
        },
        {
          key: "dut",
          value: MARK_AS_TRANSLATABLE("language.dutch"),
        },
        {
          key: "eng",
          value: MARK_AS_TRANSLATABLE("language.english"),
        },
        {
          key: "fre",
          value: MARK_AS_TRANSLATABLE("language.french"),
        },
        {
          key: "ger",
          value: MARK_AS_TRANSLATABLE("language.german"),
        },
        {
          key: "gre",
          value: MARK_AS_TRANSLATABLE("language.greek"),
        },
        {
          key: "hun",
          value: MARK_AS_TRANSLATABLE("language.hungarian"),
        },
        {
          key: "ita",
          value: MARK_AS_TRANSLATABLE("language.italian"),
        },
        {
          key: "jpn",
          value: MARK_AS_TRANSLATABLE("language.japanese"),
        },
        {
          key: "lat",
          value: MARK_AS_TRANSLATABLE("language.latin"),
        },
        {
          key: "por",
          value: MARK_AS_TRANSLATABLE("language.portuguese"),
        },
        {
          key: "rum",
          value: MARK_AS_TRANSLATABLE("language.romanian"),
        },
        {
          key: "rus",
          value: MARK_AS_TRANSLATABLE("language.russian"),
        },
        {
          key: "spa",
          value: MARK_AS_TRANSLATABLE("language.spanish"),
        },
        {
          key: "tur",
          value: MARK_AS_TRANSLATABLE("language.turkish"),
        },
        {
          key: "mis",
          value: MARK_AS_TRANSLATABLE("language.miscellaneous"),
        },
      ];
    }
  }

  export namespace StoredSearch {
    export type Type =
      "MATCH"
      | "TERM"
      | "WILDCARD"
      | "RANGE"
      | "QUERY"
      | "SIMPLE_QUERY"
      | "NESTED_BOOLEAN";

    export const Type = {
      MATCH: "MATCH" as Type,
      TERM: "TERM" as Type,
      WILDCARD: "WILDCARD" as Type,
      RANGE: "RANGE" as Type,
      QUERY: "QUERY" as Type,
      SIMPLE_QUERY: "SIMPLE_QUERY" as Type,
      NESTED_BOOLEAN: "NESTED_BOOLEAN" as Type,
    };

    export type BooleanClauseType =
      "MUST"
      | "FILTER"
      | "SHOULD"
      | "MUST_NOT";

    export const BooleanClauseType = {
      MUST: "MUST" as BooleanClauseType,
      FILTER: "FILTER" as BooleanClauseType,
      SHOULD: "SHOULD" as BooleanClauseType,
      MUST_NOT: "MUST_NOT" as BooleanClauseType,
    };

    export type CriteriaSource =
      "FACET"
      | "SEARCH"
      | "ADVANCED_SEARCH";

    export const CriteriaSource = {
      FACET: "FACET" as CriteriaSource,
      SEARCH: "SEARCH" as CriteriaSource,
      ADVANCED_SEARCH: "ADVANCED_SEARCH" as CriteriaSource,
    };
  }

  export namespace Deposit {
    export type StatusEnum = PublicationPartial.StatusEnum;
    export const StatusEnum = PublicationPartial.StatusEnum;

    export const StatusEnumTranslate: AoUStatusModel[] = [
      {
        key: StatusEnum.IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.inprogress"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.FEEDBACK_REQUIRED,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.feedbackrequired"),
      },
      {
        key: StatusEnum.IN_VALIDATION,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.invalidation"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.inerror"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.COMPLETED,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.completed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.rejected"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.SUBMITTED,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.submitted"),
        backgroundColorHexa: ColorHexaEnum.blue,
        hideInFilter: true,
      },
      {
        key: StatusEnum.CHECKED,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.checked"),
        hideInFilter: true,
      },
      {
        key: StatusEnum.IN_PREPARATION,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.inPreparation"),
        hideInFilter: true,
      },
      {
        key: StatusEnum.CANONICAL,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.canonical"),
      },
      {
        key: StatusEnum.DELETED,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.deleted"),
      },
      {
        key: StatusEnum.IN_EDITION,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.inedition"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.UPDATES_VALIDATION,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.updatesvalidation"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.CANCEL_EDITION,
        value: MARK_AS_TRANSLATABLE("enum.depositStatus.canceledition"),
        backgroundColorHexa: ColorHexaEnum.blue,
        hideInFilter: true,
      },
    ];

    export enum TargetRoutingAfterSaveEnum {
      SAVE_FOR_LATER = undefined,
      NOTHING = 0,
      PREVIOUS = -1,
      NEXT = 1,
      SAVE_AND_SUBMIT = 2,
    }

    export type StepEnum = PublicationPartial.FormStepEnum;
    export const StepEnum = PublicationPartial.FormStepEnum;

    export const StepEnumTranslate: KeyValue[] = [
      {
        key: StepEnum.TYPE,
        value: MARK_AS_TRANSLATABLE("enum.stepStatus.type"),
      },
      {
        key: StepEnum.FILES,
        value: MARK_AS_TRANSLATABLE("enum.stepStatus.files"),
      },
      {
        key: StepEnum.CONTRIBUTORS,
        value: MARK_AS_TRANSLATABLE("enum.stepStatus.contributors"),
      },
      {
        key: StepEnum.DESCRIPTION,
        value: MARK_AS_TRANSLATABLE("enum.stepStatus.description"),
      },
      {
        key: StepEnum.SUMMARY,
        value: MARK_AS_TRANSLATABLE("enum.stepStatus.summary"),
      },
    ];

    export const publicationStepEnumValue = (step: number): PublicationPartial.FormStepEnum => {
      switch (step) {
        case 0:
          return StepEnum.FILES;
        case 1:
          return StepEnum.CONTRIBUTORS;
        case 2:
          return StepEnum.DESCRIPTION;
        case 3:
          return StepEnum.SUMMARY;
        default:
          return undefined;
      }
    };

    export const publicationStepEnumNumber = (step: PublicationPartial.FormStepEnum): number => {
      switch (step) {
        case StepEnum.FILES:
          return 0;
        case StepEnum.CONTRIBUTORS:
          return 1;
        case StepEnum.DESCRIPTION:
          return 2;
        case StepEnum.SUMMARY:
          return 3;
        default:
          return -1;
      }
    };

    export type ImportSourceEnum = PublicationPartial.ImportSourceEnum;
    export const ImportSourceEnum = PublicationPartial.ImportSourceEnum;

    export type RoleContributorEnum =
      "author"
      | "collaborator"
      | "director"
      | "editor"
      | "guest_editor"
      | "photographer"
      | "translator";
    export const RoleContributorEnum = {
      author: "author" as RoleContributorEnum,
      collaborator: "collaborator" as RoleContributorEnum,
      director: "director" as RoleContributorEnum,
      editor: "editor" as RoleContributorEnum,
      guest_editor: "guest_editor" as RoleContributorEnum,
      photographer: "photographer" as RoleContributorEnum,
      translator: "translator" as RoleContributorEnum,
    };

    export type AccessLevelEnum = "PUBLIC" | "RESTRICTED" | "PRIVATE";
    export const AccessLevelEnum = {
      PUBLIC: "PUBLIC" as AccessLevelEnum,
      RESTRICTED: "RESTRICTED" as AccessLevelEnum,
      PRIVATE: "PRIVATE" as AccessLevelEnum,
    };

    const accessLevelPublic: KeyValue = {
      key: AccessLevelEnum.PUBLIC,
      value: LabelTranslateEnum.accessLevelPublic,
    };
    const accessLevelRestricted: KeyValue = {
      key: AccessLevelEnum.RESTRICTED,
      value: LabelTranslateEnum.accessLevelRestricted,
    };
    const accessLevelPrivate: KeyValue = {
      key: AccessLevelEnum.PRIVATE,
      value: LabelTranslateEnum.accessLevelPrivate,
    };

    export const AccessLevelEnumTranslate: KeyValue[] = [
      accessLevelPublic,
      accessLevelRestricted,
      accessLevelPrivate,
    ];

    export type DepositSubTypeEnum = "A1"
      | "A2"
      | "A3"
      | "C1"
      | "C2"
      | "C3"
      | "C4"
      | "D1"
      | "D2"
      | "D3"
      | "D4"
      | "D5"
      | "J1"
      | "L1"
      | "L2"
      | "L3"
      | "L5"
      | "R1"
      | "R2"
      | "R3"
      | "R4";
    export const DepositSubTypeEnum = {
      A1: "A1" as DepositSubTypeEnum,
      A2: "A2" as DepositSubTypeEnum,
      A3: "A3" as DepositSubTypeEnum,
      C1: "C1" as DepositSubTypeEnum,
      C2: "C2" as DepositSubTypeEnum,
      C3: "C3" as DepositSubTypeEnum,
      C4: "C4" as DepositSubTypeEnum,
      D1: "D1" as DepositSubTypeEnum,
      D2: "D2" as DepositSubTypeEnum,
      D3: "D3" as DepositSubTypeEnum,
      D4: "D4" as DepositSubTypeEnum,
      D5: "D5" as DepositSubTypeEnum,
      J1: "J1" as DepositSubTypeEnum,
      L1: "L1" as DepositSubTypeEnum,
      L2: "L2" as DepositSubTypeEnum,
      L3: "L3" as DepositSubTypeEnum,
      L5: "L5" as DepositSubTypeEnum,
      R1: "R1" as DepositSubTypeEnum,
      R2: "R2" as DepositSubTypeEnum,
      R3: "R3" as DepositSubTypeEnum,
      R4: "R4" as DepositSubTypeEnum,
    };

    export enum DepositFriendlyNameSubTypeEnum {
      SCIENTIFIC_ARTICLE = "A1",
      PROFESSIONAL_ARTICLE = "A2",
      OTHER_ARTICLE = "A3",
      CONFERENCE_PROCEEDINGS = "C1",
      PRESENTATION_SPEECH = "C2",
      POSTER = "C3",
      CHAPTER_OF_PROCEEDINGS = "C4",
      THESIS = "D1",
      MASTER_OF_ADVANCED_STUDIES = "D2",
      MASTER_DEGREE = "D3",
      PRIVATE_DOCTOR_THESIS = "D4",
      PROFESSIONAL_THESIS = "D5", // TODO NEW TO MANAGE WHEN READY ON BACKEND
      JOURNAL_ISSUE = "J1",
      BOOK = "L1",
      COLLECTIVE_WORK = "L2",
      BOOK_CHAPTER = "L3",
      CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA = "L5",
      RESEARCH_REPORT = "R1",
      TECHNICAL_REPORT = "R2",
      WORKING_PAPER = "R3",
      PREPRINT = "R4",
    }

    export const DepositSubTypeEnumTranslate: KeyValue[] = [
      {
        key: DepositSubTypeEnum.A1,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.A1"),
      },
      {
        key: DepositSubTypeEnum.A2,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.A2"),
      },
      {
        key: DepositSubTypeEnum.A3,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.A3"),
      },
      {
        key: DepositSubTypeEnum.C1,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.C1"),
      },
      {
        key: DepositSubTypeEnum.C2,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.C2"),
      },
      {
        key: DepositSubTypeEnum.C3,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.C3"),
      },
      {
        key: DepositSubTypeEnum.C4,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.C4"),
      },
      {
        key: DepositSubTypeEnum.D1,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.D1"),
      },
      {
        key: DepositSubTypeEnum.D2,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.D2"),
      },
      {
        key: DepositSubTypeEnum.D3,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.D3"),
      },
      {
        key: DepositSubTypeEnum.D4,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.D4"),
      },
      {
        key: DepositSubTypeEnum.D5,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.D5"),
      },
      {
        key: DepositSubTypeEnum.J1,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.J1"),
      },
      {
        key: DepositSubTypeEnum.L1,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.L1"),
      },
      {
        key: DepositSubTypeEnum.L2,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.L2"),
      },
      {
        key: DepositSubTypeEnum.L3,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.L3"),
      },
      {
        key: DepositSubTypeEnum.L5,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.L5"),
      },
      {
        key: DepositSubTypeEnum.R1,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.R1"),
      },
      {
        key: DepositSubTypeEnum.R2,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.R2"),
      },
      {
        key: DepositSubTypeEnum.R3,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.R3"),
      },
      {
        key: DepositSubTypeEnum.R4,
        value: MARK_AS_TRANSLATABLE("enum.depositSubType.R4"),
      },
    ];

    export enum DepositSubSubTypeNameEnum {
      ARTICLE = "Article",
      ARTICLE_DONNEES = "Article de données",
      ARTICLE_VIDEO = "Article vidéo",
      COMMENTAIRE = "Commentaire",
      COMPTE_RENDU_LIVRE = "Compte rendu de livre",
      EDITORIAL = "Editorial",
      LETTRE = "Lettre",
      META_ANALYSIS = "Méta-analyse",
      RAPPORT_DU_CAS = "Rapport de cas",
      REVIEW_LITERATURE = "Revue de la littérature",
      BILLET_DE_BLOG = "Billet de blog",
    }

    export const DepositSubSubTypeNameEnumTranslate: KeyValue[] = [
      {
        key: DepositSubSubTypeNameEnum.ARTICLE,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Article"),
      },
      {
        key: DepositSubSubTypeNameEnum.COMPTE_RENDU_LIVRE,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Compte rendu de livre"),
      },
      {
        key: DepositSubSubTypeNameEnum.RAPPORT_DU_CAS,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Rapport de cas"),
      },
      {
        key: DepositSubSubTypeNameEnum.COMMENTAIRE,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Commentaire"),
      },
      {
        key: DepositSubSubTypeNameEnum.ARTICLE_DONNEES,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Article de données"),
      },
      {
        key: DepositSubSubTypeNameEnum.EDITORIAL,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Editorial"),
      },
      {
        key: DepositSubSubTypeNameEnum.LETTRE,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Lettre"),
      },
      {
        key: DepositSubSubTypeNameEnum.META_ANALYSIS,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Méta-analyse"),
      },
      {
        key: DepositSubSubTypeNameEnum.REVIEW_LITERATURE,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Revue de la littérature"),
      },
      {
        key: DepositSubSubTypeNameEnum.ARTICLE_VIDEO,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Article vidéo"),
      },
      {
        key: DepositSubSubTypeNameEnum.BILLET_DE_BLOG,
        value: MARK_AS_TRANSLATABLE("enum.depositSubSubType.Billet de blog"),
      },
    ];

    export enum FileTypeStringEnum {
      ARTICLE_PUBLISHED_VERSION = "Article (Published version)",
      ARTICLE_ACCEPTED_VERSION = "Article (Accepted version)",
      ARTICLE_SUBMITTED_VERSION = "Article (Submitted version)",
      ISSUE_PUBLISHED_VERSION = "Issue (Published version)",
      BOOK_PUBLISHED_VERSION = "Book (Published version)",
      BOOK_ACCEPTED_VERSION = "Book (Accepted version)",
      BOOK_SUBMITTED_VERSION = "Book (Submitted version)",
      BOOK_CHAPTER_PUBLISHED_VERSION = "Book chapter (Published version)",
      BOOK_CHAPTER_ACCEPTED_VERSION = "Book chapter (Accepted version)",
      BOOK_CHAPTER_SUBMITTED_VERSION = "Book chapter (Submitted version)",
      ENCYCLOPEDIA_ENTRY_PUBLISHED_VERSION = "Encyclopedia entry (Published version)",
      ENCYCLOPEDIA_ENTRY_ACCEPTED_VERSION = "Encyclopedia entry (Accepted version)",
      ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION = "Encyclopedia entry (Submitted version)",
      PROCEEDINGS_PUBLISHED_VERSION = "Proceedings (Published version)",
      PROCEEDINGS_ACCEPTED_VERSION = "Proceedings (Accepted version)",
      PROCEEDINGS_SUBMITTED_VERSION = "Proceedings (Submitted version)",
      PROCEEDINGS_CHAPTER_PUBLISHED_VERSION = "Proceedings chapter (Published version)",
      PROCEEDINGS_CHAPTER_ACCEPTED_VERSION = "Proceedings chapter (Accepted version)",
      PROCEEDINGS_CHAPTER_SUBMITTED_VERSION = "Proceedings chapter (Submitted version)",
      PRESENTATION = "Presentation",
      RECORDING = "Recording",
      APPENDIX = "Appendix",
      ADDENDUM = "Addendum",
      CORRIGENDUM = "Corrigendum",
      ERRATUM = "Erratum",
      RETRACTION = "Retraction",
      SUPPLEMENTAL_DATA = "Supplemental data",
      EXTRACT = "Extract",
      SUMMARY = "Summary",
      TRANSLATION = "Translation",
      POSTER = "Poster",
      THESIS = "Thesis",
      MASTER = "Master",
      PREPRINT = "Preprint",
      REPORT_PUBLISHED_VERSION = "Report (Published version)",
      REPORT_ACCEPTED_VERSION = "Report (Accepted version)",
      REPORT_SUBMITTED_VERSION = "Report (Submitted version)",
      WORKING_PAPER_PUBLISHED_VERSION = "Working paper (Published version)",
      WORKING_PAPER_ACCEPTED_VERSION = "Working paper (Accepted version)",
      WORKING_PAPER_SUBMITTED_VERSION = "Working paper (Submitted version)",
      AGREEMENT = "Agreement",
      IMPRIMATUR = "Imprimatur",
      MODE_PUBLICATION = "Publication mode",
    }

    export type FileTypeEnum =
      "ARTICLE_PUBLISHED_VERSION"
      | "ARTICLE_ACCEPTED_VERSION"
      | "ARTICLE_SUBMITTED_VERSION"
      | "ISSUE_PUBLISHED_VERSION"
      | "BOOK_PUBLISHED_VERSION"
      | "BOOK_ACCEPTED_VERSION"
      | "BOOK_SUBMITTED_VERSION"
      | "BOOK_CHAPTER_PUBLISHED_VERSION"
      | "BOOK_CHAPTER_ACCEPTED_VERSION"
      | "BOOK_CHAPTER_SUBMITTED_VERSION"
      | "ENCYCLOPEDIA_ENTRY_PUBLISHED_VERSION"
      | "ENCYCLOPEDIA_ENTRY_ACCEPTED_VERSION"
      | "ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION"
      | "PROCEEDINGS_PUBLISHED_VERSION"
      | "PROCEEDINGS_ACCEPTED_VERSION"
      | "PROCEEDINGS_SUBMITTED_VERSION"
      | "PROCEEDINGS_CHAPTER_PUBLISHED_VERSION"
      | "PROCEEDINGS_CHAPTER_ACCEPTED_VERSION"
      | "PROCEEDINGS_CHAPTER_SUBMITTED_VERSION"
      | "PRESENTATION"
      | "RECORDING"
      | "APPENDIX"
      | "ADDENDUM"
      | "SUPPLEMENTAL_DATA"
      | "EXTRACT"
      | "SUMMARY"
      | "TRANSLATION"
      | "POSTER"
      | "THESIS"
      | "MASTER"
      | "PREPRINT"
      | "REPORT_PUBLISHED_VERSION"
      | "REPORT_ACCEPTED_VERSION"
      | "REPORT_SUBMITTED_VERSION"
      | "WORKING_PAPER_PUBLISHED_VERSION"
      | "WORKING_PAPER_ACCEPTED_VERSION"
      | "WORKING_PAPER_SUBMITTED_VERSION"
      | "DOCUMENT";
    export const FileTypeEnum = {
      ARTICLE_PUBLISHED_VERSION: "ARTICLE_PUBLISHED_VERSION" as FileTypeEnum,
      ARTICLE_ACCEPTED_VERSION: "ARTICLE_ACCEPTED_VERSION" as FileTypeEnum,
      ARTICLE_SUBMITTED_VERSION: "ARTICLE_SUBMITTED_VERSION" as FileTypeEnum,
      ISSUE_PUBLISHED_VERSION: "ISSUE_PUBLISHED_VERSION" as FileTypeEnum,
      BOOK_PUBLISHED_VERSION: "BOOK_PUBLISHED_VERSION" as FileTypeEnum,
      BOOK_ACCEPTED_VERSION: "BOOK_ACCEPTED_VERSION" as FileTypeEnum,
      BOOK_SUBMITTED_VERSION: "BOOK_SUBMITTED_VERSION" as FileTypeEnum,
      BOOK_CHAPTER_PUBLISHED_VERSION: "BOOK_CHAPTER_PUBLISHED_VERSION" as FileTypeEnum,
      BOOK_CHAPTER_ACCEPTED_VERSION: "BOOK_CHAPTER_ACCEPTED_VERSION" as FileTypeEnum,
      BOOK_CHAPTER_SUBMITTED_VERSION: "BOOK_CHAPTER_SUBMITTED_VERSION" as FileTypeEnum,
      ENCYCLOPEDIA_ENTRY_PUBLISHED_VERSION: "ENCYCLOPEDIA_ENTRY_PUBLISHED_VERSION" as FileTypeEnum,
      ENCYCLOPEDIA_ENTRY_ACCEPTED_VERSION: "ENCYCLOPEDIA_ENTRY_ACCEPTED_VERSION" as FileTypeEnum,
      ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION: "ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION" as FileTypeEnum,
      PROCEEDINGS_PUBLISHED_VERSION: "PROCEEDINGS_PUBLISHED_VERSION" as FileTypeEnum,
      PROCEEDINGS_ACCEPTED_VERSION: "PROCEEDINGS_ACCEPTED_VERSION" as FileTypeEnum,
      PROCEEDINGS_SUBMITTED_VERSION: "PROCEEDINGS_SUBMITTED_VERSION" as FileTypeEnum,
      PROCEEDINGS_CHAPTER_PUBLISHED_VERSION: "PROCEEDINGS_CHAPTER_PUBLISHED_VERSION" as FileTypeEnum,
      PROCEEDINGS_CHAPTER_ACCEPTED_VERSION: "PROCEEDINGS_CHAPTER_ACCEPTED_VERSION" as FileTypeEnum,
      PROCEEDINGS_CHAPTER_SUBMITTED_VERSION: "PROCEEDINGS_CHAPTER_SUBMITTED_VERSION" as FileTypeEnum,
      PRESENTATION: "PRESENTATION" as FileTypeEnum,
      RECORDING: "RECORDING" as FileTypeEnum,
      APPENDIX: "APPENDIX" as FileTypeEnum,
      ADDENDUM: "ADDENDUM" as FileTypeEnum,
      SUPPLEMENTAL_DATA: "SUPPLEMENTAL_DATA" as FileTypeEnum,
      EXTRACT: "EXTRACT" as FileTypeEnum,
      SUMMARY: "SUMMARY" as FileTypeEnum,
      TRANSLATION: "TRANSLATION" as FileTypeEnum,
      POSTER: "POSTER" as FileTypeEnum,
      THESIS: "THESIS" as FileTypeEnum,
      MASTER: "MASTER" as FileTypeEnum,
      PREPRINT: "PREPRINT" as FileTypeEnum,
      REPORT_PUBLISHED_VERSION: "REPORT_PUBLISHED_VERSION" as FileTypeEnum,
      REPORT_ACCEPTED_VERSION: "REPORT_ACCEPTED_VERSION" as FileTypeEnum,
      REPORT_SUBMITTED_VERSION: "REPORT_SUBMITTED_VERSION" as FileTypeEnum,
      WORKING_PAPER_PUBLISHED_VERSION: "WORKING_PAPER_PUBLISHED_VERSION" as FileTypeEnum,
      WORKING_PAPER_ACCEPTED_VERSION: "WORKING_PAPER_ACCEPTED_VERSION" as FileTypeEnum,
      WORKING_PAPER_SUBMITTED_VERSION: "WORKING_PAPER_SUBMITTED_VERSION" as FileTypeEnum,
      DOCUMENT: "DOCUMENT" as FileTypeEnum,
    };

    export const FileTypeEnumTranslate: KeyValue[] = [
      {
        key: FileTypeEnum.ARTICLE_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.articlePublishedVersion"),
      },
      {
        key: FileTypeEnum.ARTICLE_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.articleAcceptedVersion"),
      },
      {
        key: FileTypeEnum.ARTICLE_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.articleSubmittedVersion"),
      },
      {
        key: FileTypeEnum.ISSUE_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.issuePublishedVersion"),
      },
      {
        key: FileTypeEnum.BOOK_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.bookPublishedVersion"),
      },
      {
        key: FileTypeEnum.BOOK_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.bookAcceptedVersion"),
      },
      {
        key: FileTypeEnum.BOOK_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.bookSubmittedVersion"),
      },
      {
        key: FileTypeEnum.BOOK_CHAPTER_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.bookChapterPublishedVersion"),
      },
      {
        key: FileTypeEnum.BOOK_CHAPTER_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.bookChapterAcceptedVersion"),
      },
      {
        key: FileTypeEnum.BOOK_CHAPTER_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.bookChapterSubmittedVersion"),
      },
      {
        key: FileTypeEnum.ENCYCLOPEDIA_ENTRY_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.encyclopediaEntryPublishedVersion"),
      },
      {
        key: FileTypeEnum.ENCYCLOPEDIA_ENTRY_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.encyclopediaEntryAcceptedVersion"),
      },
      {
        key: FileTypeEnum.ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.encyclopediaEntrySubmittedVersion"),
      },
      {
        key: FileTypeEnum.PROCEEDINGS_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.proceedingsPublishedVersion"),
      },
      {
        key: FileTypeEnum.PROCEEDINGS_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.proceedingsAcceptedVersion"),
      },
      {
        key: FileTypeEnum.PROCEEDINGS_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.proceedingsSubmittedVersion"),
      },
      {
        key: FileTypeEnum.PROCEEDINGS_CHAPTER_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.proceedingsChapterPublishedVersion"),
      },
      {
        key: FileTypeEnum.PROCEEDINGS_CHAPTER_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.proceedingsChapterAcceptedVersion"),
      },
      {
        key: FileTypeEnum.PROCEEDINGS_CHAPTER_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.proceedingsChapterSubmittedVersion"),
      },
      {
        key: FileTypeEnum.PRESENTATION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.presentation"),
      },
      {
        key: FileTypeEnum.RECORDING,
        value: MARK_AS_TRANSLATABLE("enum.fileType.recording"),
      },
      {
        key: FileTypeEnum.APPENDIX,
        value: MARK_AS_TRANSLATABLE("enum.fileType.appendix"),
      },
      {
        key: FileTypeEnum.ADDENDUM,
        value: MARK_AS_TRANSLATABLE("enum.fileType.addendum"),
      },
      {
        key: FileTypeEnum.SUPPLEMENTAL_DATA,
        value: MARK_AS_TRANSLATABLE("enum.fileType.supplementalData"),
      },
      {
        key: FileTypeEnum.EXTRACT,
        value: MARK_AS_TRANSLATABLE("enum.fileType.extract"),
      },
      {
        key: FileTypeEnum.SUMMARY,
        value: MARK_AS_TRANSLATABLE("enum.fileType.summary"),
      },
      {
        key: FileTypeEnum.TRANSLATION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.translation"),
      },
      {
        key: FileTypeEnum.POSTER,
        value: MARK_AS_TRANSLATABLE("enum.fileType.poster"),
      },
      {
        key: FileTypeEnum.THESIS,
        value: MARK_AS_TRANSLATABLE("enum.fileType.thesis"),
      },
      {
        key: FileTypeEnum.MASTER,
        value: MARK_AS_TRANSLATABLE("enum.fileType.master"),
      },
      {
        key: FileTypeEnum.PREPRINT,
        value: MARK_AS_TRANSLATABLE("enum.fileType.preprint"),
      },
      {
        key: FileTypeEnum.REPORT_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.reportPublishedVersion"),
      },
      {
        key: FileTypeEnum.REPORT_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.reportAcceptedVersion"),
      },
      {
        key: FileTypeEnum.REPORT_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.reportSubmittedVersion"),
      },
      {
        key: FileTypeEnum.WORKING_PAPER_PUBLISHED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.workingPaperPublishedVersion"),
      },
      {
        key: FileTypeEnum.WORKING_PAPER_ACCEPTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.workingPaperAcceptedVersion"),
      },
      {
        key: FileTypeEnum.WORKING_PAPER_SUBMITTED_VERSION,
        value: MARK_AS_TRANSLATABLE("enum.fileType.workingPaperSubmittedVersion"),
      },
      {
        key: FileTypeEnum.DOCUMENT,
        value: MARK_AS_TRANSLATABLE("enum.fileType.document"),
      },
    ];

    export type LinkTypesEnum =
      "internal"
      | "web";
    export const LinkTypesEnum = {
      internal: "internal" as LinkTypesEnum,
      web: "web" as LinkTypesEnum,
    };
    export const LinkTypesEnumTranslate: KeyValue[] = [
      {
        key: LinkTypesEnum.internal,
        value: MARK_AS_TRANSLATABLE("enum.linkTypes.internal"),
      },
      {
        key: LinkTypesEnum.web,
        value: MARK_AS_TRANSLATABLE("enum.linkTypes.web"),
      },
    ];

    export type DateTypesEnum =
      "PUBLICATION"
      | "FIRST_ONLINE"
      | "IMPRIMATUR"
      | "DEFENSE";
    export const DateTypesEnum = {
      PUBLICATION: "PUBLICATION" as DateTypesEnum,
      FIRST_ONLINE: "FIRST_ONLINE" as DateTypesEnum,
      IMPRIMATUR: "IMPRIMATUR" as DateTypesEnum,
      DEFENSE: "DEFENSE" as DateTypesEnum,
    };
    export const DateTypesEnumTranslate: KeyValue[] = [
      {
        key: DateTypesEnum.PUBLICATION,
        value: LabelTranslateEnum.enumDateTypesPublication,
      },
      {
        key: DateTypesEnum.FIRST_ONLINE,
        value: LabelTranslateEnum.enumDateTypesFirstOnline,
      },
      {
        key: DateTypesEnum.IMPRIMATUR,
        value: LabelTranslateEnum.enumDateTypesImprimatur,
      },
      {
        key: DateTypesEnum.DEFENSE,
        value: LabelTranslateEnum.enumDateTypesDefense,
      },
    ];

    export type UserRoleEnum = "CREATOR" | "CONTRIBUTOR" | "VALIDATOR";
    export const UserRoleEnum = {
      CREATOR: "CREATOR" as UserRoleEnum,
      CONTRIBUTOR: "CONTRIBUTOR" as UserRoleEnum,
      VALIDATOR: "VALIDATOR" as UserRoleEnum,
    };
  }

  export namespace DepositStructure {
    export type LinkTypesEnum =
      "METADATA"
      | "VALIDATION";
    export const LinkTypesEnum = {
      METADATA: "METADATA" as LinkTypesEnum,
      VALIDATION: "VALIDATION" as LinkTypesEnum,
    };
  }

  export namespace DocumentFile {
    export type StatusEnum =
      "RECEIVED"
      | "TO_PROCESS"
      | "PROCESSED"
      | "READY"
      | "CLEANING"
      | "DUPLICATE"
      | "IN_ERROR"
      | "IMPOSSIBLE_TO_DOWNLOAD"
      | "TO_BE_CONFIRMED"
      | "CONFIRMED";
    export const StatusEnum = {
      RECEIVED: "RECEIVED" as StatusEnum,
      TO_PROCESS: "TO_PROCESS" as StatusEnum,
      PROCESSED: "PROCESSED" as StatusEnum,
      READY: "READY" as StatusEnum,
      CLEANING: "CLEANING" as StatusEnum,
      DUPLICATE: "DUPLICATE" as StatusEnum,
      IN_ERROR: "IN_ERROR" as StatusEnum,
      IMPOSSIBLE_TO_DOWNLOAD: "IMPOSSIBLE_TO_DOWNLOAD" as StatusEnum,
      TO_BE_CONFIRMED: "TO_BE_CONFIRMED" as StatusEnum,
      CONFIRMED: "CONFIRMED" as StatusEnum,
    };

    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.RECEIVED,
        value: LabelTranslateEnum.received,
      },
      {
        key: StatusEnum.TO_PROCESS,
        value: LabelTranslateEnum.processing,
      },
      {
        key: StatusEnum.PROCESSED,
        value: LabelTranslateEnum.uploading,
      },
      {
        key: StatusEnum.READY,
        value: LabelTranslateEnum.fileUploaded,
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.CLEANING,
        value: LabelTranslateEnum.cleaning,
      },
      {
        key: StatusEnum.DUPLICATE,
        value: LabelTranslateEnum.duplicate,
      },
      {
        key: StatusEnum.IN_ERROR,
        value: LabelTranslateEnum.inError,
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.IMPOSSIBLE_TO_DOWNLOAD,
        value: LabelTranslateEnum.impossibleToDownload,
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.TO_BE_CONFIRMED,
        value: LabelTranslateEnum.toBeConfirmed,
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.CONFIRMED,
        value: LabelTranslateEnum.confirmed,
        backgroundColorHexa: ColorHexaEnum.green,
      },
    ];

    export type AccessLevelEnum = "PUBLIC" | "RESTRICTED" | "PRIVATE";
    export const AccessLevelEnum = {
      PUBLIC: "PUBLIC" as AccessLevelEnum,
      RESTRICTED: "RESTRICTED" as AccessLevelEnum,
      PRIVATE: "PRIVATE" as AccessLevelEnum,
    };

    const accessLevelPublic: KeyValue = {
      key: AccessLevelEnum.PUBLIC,
      value: MARK_AS_TRANSLATABLE("enum.accessLevel.public"),
    };
    const accessLevelRestricted: KeyValue = {
      key: AccessLevelEnum.RESTRICTED,
      value: MARK_AS_TRANSLATABLE("enum.accessLevel.restricted"),
    };
    const accessLevelPrivate: KeyValue = {
      key: AccessLevelEnum.PRIVATE,
      value: MARK_AS_TRANSLATABLE("enum.accessLevel.private"),
    };

    export const AccessLevelEnumTranslate: KeyValue[] = [
      accessLevelPublic,
      accessLevelRestricted,
      accessLevelPrivate,
    ];

    export const embargoAccessLevelEnumList = (access: AccessLevelEnum): KeyValue[] => {
      if (access === AccessLevelEnum.PUBLIC) {
        return [
          accessLevelRestricted,
          accessLevelPrivate,
        ];
      } else if (access === AccessLevelEnum.RESTRICTED) {
        return [
          accessLevelPrivate,
        ];
      }
      return [];
    };

    export const getExplanation = (status: StatusEnum): string => {
      switch (status) {
        case StatusEnum.RECEIVED:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.received");
        case StatusEnum.TO_PROCESS:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.toProcess");
        case StatusEnum.PROCESSED:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.processed");
        case StatusEnum.READY:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.ready");
        case StatusEnum.CLEANING:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.cleaning");
        case StatusEnum.IN_ERROR:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.inError");
        case StatusEnum.IMPOSSIBLE_TO_DOWNLOAD:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.impossibleToDownload");
        default:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.pending");
      }
    };
  }

  export namespace DocumentFileType {
    export type FileTypeLevelEnum = "PRINCIPAL" | "SECONDARY" | "CORRECTIVE | ADMINISTRATION";
    export const FileTypeLevelEnum = {
      PRINCIPAL: "PRINCIPAL" as FileTypeLevelEnum,
      SECONDARY: "SECONDARY" as FileTypeLevelEnum,
      CORRECTIVE: "CORRECTIVE" as FileTypeLevelEnum,
      ADMINISTRATION: "ADMINISTRATION" as FileTypeLevelEnum,
    };
    export const DocumentFileTypeEnumTranslate: KeyValue[] = [
      {
        key: FileTypeLevelEnum.PRINCIPAL,
        value: MARK_AS_TRANSLATABLE("enum.fileTypeLevelEnum.principal"),
      },
      {
        key: FileTypeLevelEnum.SECONDARY,
        value: MARK_AS_TRANSLATABLE("enum.fileTypeLevelEnum.secondary"),
      },
      {
        key: FileTypeLevelEnum.CORRECTIVE,
        value: MARK_AS_TRANSLATABLE("enum.fileTypeLevelEnum.corrective"),
      },
      {
        key: FileTypeLevelEnum.ADMINISTRATION,
        value: MARK_AS_TRANSLATABLE("enum.fileTypeLevelEnum.administration"),
      },
    ];

    export const GetFileTypeLevelSortValue = (fileTypeLevelEnum: FileTypeLevelEnum): number => {
      switch (fileTypeLevelEnum) {
        case FileTypeLevelEnum.PRINCIPAL:
          return 10;
        case FileTypeLevelEnum.SECONDARY:
          return 20;
        case FileTypeLevelEnum.CORRECTIVE:
          return 30;
        case FileTypeLevelEnum.ADMINISTRATION:
          return 40;
      }
      // put by default at the end
      return 100;
    };
  }

  export namespace Result {
    export type ActionStatusEnum = "EXECUTED" | "NOT_EXECUTED" | "NON_APPLICABLE";
    export const ActionStatusEnum = {
      EXECUTED: "EXECUTED" as ActionStatusEnum,
      NOT_EXECUTED: "NOT_EXECUTED" as ActionStatusEnum,
      NON_APPLICABLE: "NON_APPLICABLE" as ActionStatusEnum,
    };
  }

  export namespace Role {
    export type RoleEnum =
      "USER"
      | "VALIDATOR";
    export const RoleEnum = {
      USER: "USER" as RoleEnum,
      VALIDATOR: "VALIDATOR" as RoleEnum,
    };
    export const RoleEnumTranslate: StatusModel[] = [
      {
        key: RoleEnum.USER,
        value: MARK_AS_TRANSLATABLE("enum.role.user"),
      },
      {
        key: RoleEnum.VALIDATOR,
        value: MARK_AS_TRANSLATABLE("enum.role.validator"),
      },
    ];

    export const getRoleIcon = (role: RoleEnum): IconNameEnum | undefined => {
      switch (role) {
        case RoleEnum.USER:
          return IconNameEnum.roleUser;
        case RoleEnum.VALIDATOR:
          return IconNameEnum.roleValidator;
        default:
          return undefined;
      }
    };
  }

  export namespace Structure {
    export type StatusEnum = StructurePartial.StatusEnum;
    export const StatusEnum = StructurePartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.ACTIVE,
        value: MARK_AS_TRANSLATABLE("enum.structureStatus.active"),
      },
      {
        key: StatusEnum.OBSOLETE,
        value: MARK_AS_TRANSLATABLE("enum.structureStatus.obsolete"),
      },
      {
        key: StatusEnum.HISTORICAL,
        value: MARK_AS_TRANSLATABLE("enum.structureStatus.historical"),
      },
    ];
  }

  export enum StructureCodeEnum {
    SCIENCE_FACULTY = "1",
    MATHEMATIC_FACULTY = "11",
  }

  export namespace UserApplicationRole {
    export type UserApplicationRoleEnum =
      "ROOT"
      | "ADMIN"
      | "USER"
      | "TRUSTED_CLIENT"
      | "GUEST";
    export const UserApplicationRoleEnum = {
      root: "ROOT" as UserApplicationRoleEnum,
      admin: "ADMIN" as UserApplicationRoleEnum,
      user: "USER" as UserApplicationRoleEnum,
      trusted_client: "TRUSTED_CLIENT" as UserApplicationRoleEnum,
      guest: "GUEST" as UserApplicationRoleEnum,
    };
    export const UserApplicationRoleEnumTranslate: KeyValue[] = [
      {
        key: UserApplicationRoleEnum.admin,
        value: MARK_AS_TRANSLATABLE("admin.user.applicationRoles.admin"),
      },
      {
        key: UserApplicationRoleEnum.root,
        value: MARK_AS_TRANSLATABLE("admin.user.applicationRoles.root"),
      },
      {
        key: UserApplicationRoleEnum.user,
        value: MARK_AS_TRANSLATABLE("admin.user.applicationRoles.user"),
      },
      {
        key: UserApplicationRoleEnum.trusted_client,
        value: MARK_AS_TRANSLATABLE("admin.user.applicationRoles.trustedClient"),
      },
    ];
  }

  export namespace EventType {
    export type EventTypeEnum =
      "PUBLICATION_TO_VALIDATE"
      | "PUBLICATION_VALIDATED"
      | "PUBLICATION_CREATED"
      | "PUBLICATION_SUBMITTED"
      | "PUBLICATION_REJECTED"
      | "COMMENT_IN_PUBLICATION"
      | "COMMENT_FOR_VALIDATORS_IN_PUBLICATION"
      | "PUBLICATION_FEEDBACK_REQUIRED"
      | "PUBLICATION_DETECTED_ON_ORCID"
      | "PUBLICATION_EXPORTED_TO_ORCID"
      | "PUBLICATION_UPDATED_IN_BATCH";
    export const EventTypeEnum = {
      PUBLICATION_TO_VALIDATE: "PUBLICATION_TO_VALIDATE" as EventTypeEnum,
      PUBLICATION_VALIDATED: "PUBLICATION_VALIDATED" as EventTypeEnum,
      PUBLICATION_CREATED: "PUBLICATION_CREATED" as EventTypeEnum,
      PUBLICATION_SUBMITTED: "PUBLICATION_SUBMITTED" as EventTypeEnum,
      PUBLICATION_REJECTED: "PUBLICATION_REJECTED" as EventTypeEnum,
      COMMENT_IN_PUBLICATION: "COMMENT_IN_PUBLICATION" as EventTypeEnum,
      COMMENT_FOR_VALIDATORS_IN_PUBLICATION: "COMMENT_FOR_VALIDATORS_IN_PUBLICATION" as EventTypeEnum,
      PUBLICATION_FEEDBACK_REQUIRED: "PUBLICATION_FEEDBACK_REQUIRED" as EventTypeEnum,
      PUBLICATION_DETECTED_ON_ORCID: "PUBLICATION_DETECTED_ON_ORCID" as EventTypeEnum,
      PUBLICATION_EXPORTED_TO_ORCID: "PUBLICATION_EXPORTED_TO_ORCID" as EventTypeEnum,
      PUBLICATION_UPDATED_IN_BATCH: "PUBLICATION_UPDATED_IN_BATCH" as EventTypeEnum,
    };
    export const EventTypeEnumTranslate: KeyValue[] = [
      {
        key: EventTypeEnum.PUBLICATION_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationToValidate"),
      },
      {
        key: EventTypeEnum.PUBLICATION_VALIDATED,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationValidated"),
      },
      {
        key: EventTypeEnum.PUBLICATION_CREATED,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationCreated"),
      },
      {
        key: EventTypeEnum.PUBLICATION_SUBMITTED,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationSubmitted"),
      },
      {
        key: EventTypeEnum.PUBLICATION_REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationRejected"),
      },
      {
        key: EventTypeEnum.COMMENT_IN_PUBLICATION,
        value: MARK_AS_TRANSLATABLE("enum.eventType.commentInPublication"),
      },
      {
        key: EventTypeEnum.COMMENT_FOR_VALIDATORS_IN_PUBLICATION,
        value: MARK_AS_TRANSLATABLE("enum.eventType.commentForValidatorsInPublication"),
      },
      {
        key: EventTypeEnum.PUBLICATION_FEEDBACK_REQUIRED,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationFeedbackRequired"),
      },
      {
        key: EventTypeEnum.PUBLICATION_DETECTED_ON_ORCID,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationDetectedOnOrcid"),
      },
      {
        key: EventTypeEnum.PUBLICATION_EXPORTED_TO_ORCID,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationExportedToOrcid"),
      },
      {
        key: EventTypeEnum.PUBLICATION_UPDATED_IN_BATCH,
        value: MARK_AS_TRANSLATABLE("enum.eventType.publicationUpdatedInBatch"),
      },
    ];
  }

  export namespace GlobalBanner {
    export type TypeEnum =
      "CRITICAL"
      | "WARNING"
      | "INFO";
    export const TypeEnum = {
      CRITICAL: "CRITICAL" as TypeEnum,
      WARNING: "WARNING" as TypeEnum,
      INFO: "INFO" as TypeEnum,
    };
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.CRITICAL,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.critical"),
      },
      {
        key: TypeEnum.WARNING,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.warning"),
      },
      {
        key: TypeEnum.INFO,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.info"),
      },
    ];
  }

  export namespace NotificationType {

    export type FrequencyEnum =
      "IMMEDIATELY"
      | "DAILY"
      | "WEEKLY";
    export const FrequencyEnum = {
      IMMEDIATELY: "IMMEDIATELY" as FrequencyEnum,
      DAILY: "DAILY" as FrequencyEnum,
      WEEKLY: "WEEKLY" as FrequencyEnum,
    };
    export const FrequencyEnumTranslate: KeyValue[] = [
      {
        key: FrequencyEnum.IMMEDIATELY,
        value: MARK_AS_TRANSLATABLE("enum.frequency.immediately"),
      },
      {
        key: FrequencyEnum.DAILY,
        value: MARK_AS_TRANSLATABLE("enum.frequency.daily"),
      },
      {
        key: FrequencyEnum.WEEKLY,
        value: MARK_AS_TRANSLATABLE("enum.frequency.weekly"),
      },
    ];

    export type NotificationTypeEnum =
      "MY_PUBLICATION_COMMENTED"
      | "MY_INDIRECT_PUBLICATION_COMMENTED"
      | "MY_PUBLICATION_REJECTED"
      | "MY_PUBLICATION_VALIDATED"
      | "MY_PUBLICATION_FEEDBACK_REQUIRED"
      | "MY_INDIRECT_PUBLICATION_VALIDATED"
      | "PUBLICATION_TO_VALIDATE"
      | "PUBLICATION_TO_VALIDATE_COMMENTED"
      | "PUBLICATION_FORGOTTEN_TO_VALIDATE"
      | "PUBLICATION_FORGOTTEN_IN_PROGRESS"
      | "PUBLICATION_DETECTED_ON_ORCID"
      | "PUBLICATION_EXPORTED_TO_ORCID";
    export const NotificationTypeEnum = {
      MY_PUBLICATION_COMMENTED: "MY_PUBLICATION_COMMENTED" as NotificationTypeEnum,
      MY_INDIRECT_PUBLICATION_COMMENTED: "MY_INDIRECT_PUBLICATION_COMMENTED" as NotificationTypeEnum,
      MY_PUBLICATION_REJECTED: "MY_PUBLICATION_REJECTED" as NotificationTypeEnum,
      MY_PUBLICATION_VALIDATED: "MY_PUBLICATION_VALIDATED" as NotificationTypeEnum,
      MY_PUBLICATION_FEEDBACK_REQUIRED: "MY_PUBLICATION_FEEDBACK_REQUIRED" as NotificationTypeEnum,
      MY_INDIRECT_PUBLICATION_VALIDATED: "MY_INDIRECT_PUBLICATION_VALIDATED" as NotificationTypeEnum,
      PUBLICATION_TO_VALIDATE: "PUBLICATION_TO_VALIDATE" as NotificationTypeEnum,
      PUBLICATION_TO_VALIDATE_COMMENTED: "PUBLICATION_TO_VALIDATE_COMMENTED" as NotificationTypeEnum,
      PUBLICATION_FORGOTTEN_TO_VALIDATE: "PUBLICATION_FORGOTTEN_TO_VALIDATE" as NotificationTypeEnum,
      PUBLICATION_FORGOTTEN_IN_PROGRESS: "PUBLICATION_FORGOTTEN_IN_PROGRESS" as NotificationTypeEnum,
      PUBLICATION_DETECTED_ON_ORCID: "PUBLICATION_DETECTED_ON_ORCID" as NotificationTypeEnum,
      PUBLICATION_EXPORTED_TO_ORCID: "PUBLICATION_EXPORTED_TO_ORCID" as NotificationTypeEnum,
    };
    export const NotificationTypeEnumTranslate: KeyValue[] = [
      {
        key: NotificationTypeEnum.MY_INDIRECT_PUBLICATION_VALIDATED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myIndirectPublicationValidated"),
      },
      {
        key: NotificationTypeEnum.MY_INDIRECT_PUBLICATION_COMMENTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myIndirectPublicationCommented"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_DETECTED_ON_ORCID,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.publicationDetectedOnOrcid"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_EXPORTED_TO_ORCID,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.publicationExportedToOrcid"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_VALIDATED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myPublicationValidated"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_COMMENTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myPublicationCommented"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myPublicationRejected"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.publicationToValidate"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_TO_VALIDATE_COMMENTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.publicationToValidateCommented"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_FORGOTTEN_IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.publicationsForgottenInProgress"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_FEEDBACK_REQUIRED,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myPublicationFeedbackRequired"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_FORGOTTEN_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.publicationsForgottenToValidate"),
      },
    ];
    export const NotificationTypeShortEnumTranslate: KeyValue[] = [
      {
        key: NotificationTypeEnum.MY_INDIRECT_PUBLICATION_VALIDATED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myIndirectPublicationValidated"),
      },
      {
        key: NotificationTypeEnum.MY_INDIRECT_PUBLICATION_COMMENTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myIndirectPublicationCommented"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_DETECTED_ON_ORCID,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.publicationDetectedOnOrcid"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_EXPORTED_TO_ORCID,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.publicationExportedToOrcid"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_VALIDATED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myPublicationValidated"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_COMMENTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myPublicationCommented"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myPublicationRejected"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.publicationToValidate"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_TO_VALIDATE_COMMENTED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.publicationToValidateCommented"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_FORGOTTEN_IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.publicationsForgottenInProgress"),
      },
      {
        key: NotificationTypeEnum.MY_PUBLICATION_FEEDBACK_REQUIRED,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myPublicationFeedbackRequired"),
      },
      {
        key: NotificationTypeEnum.PUBLICATION_FORGOTTEN_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.publicationsForgottenToValidate"),
      },
    ];
  }

  export namespace Criteria {
    export type TypeEnum =
      "FREE_TEXT_TERM"
      | "FREE_TEXT_MATCH"
      | "FREE_TEXT_WILDCARD"
      | "ENUM_SINGLE"
      | "ENUM_MULTIPLE"
      | "INTERVAL_YEAR"
      | "STRUCTURE"
      | "STRUCTURE_SUB_STRUCTURES"
      | "STRUCTURE_COMBINED"
      | "RESEARCH_GROUP"
      | "CONTRIBUTOR_ALL"
      | "CONTRIBUTOR_UNIGE"
      | "CONTRIBUTOR_COMBINED";
    export const TypeEnum = {
      FREE_TEXT_TERM: "FREE_TEXT_TERM" as TypeEnum,
      FREE_TEXT_MATCH: "FREE_TEXT_MATCH" as TypeEnum,
      FREE_TEXT_WILDCARD: "FREE_TEXT_WILDCARD" as TypeEnum,
      ENUM_SINGLE: "ENUM_SINGLE" as TypeEnum,
      ENUM_MULTIPLE: "ENUM_MULTIPLE" as TypeEnum,
      INTERVAL_YEAR: "INTERVAL_YEAR" as TypeEnum,
      STRUCTURE: "STRUCTURE" as TypeEnum,
      STRUCTURE_SUB_STRUCTURES: "STRUCTURE_SUB_STRUCTURES" as TypeEnum,
      STRUCTURE_COMBINED: "STRUCTURE_COMBINED" as TypeEnum,
      RESEARCH_GROUP: "RESEARCH_GROUP" as TypeEnum,
      CONTRIBUTOR_ALL: "CONTRIBUTOR_ALL" as TypeEnum,
      CONTRIBUTOR_UNIGE: "CONTRIBUTOR_UNIGE" as TypeEnum,
      CONTRIBUTOR_COMBINED: "CONTRIBUTOR_COMBINED" as TypeEnum,
    };
  }

  export namespace DepositType {
    export type DepositTypeLevelEnum = "ARTICLE" | "CONFERENCE" | "DIPLOME | JOURNAL" | "LIVRE" | "RAPPORT";
    export const DepositTypeLevelEnum = {
      ARTICLE: "ARTICLE" as DepositTypeLevelEnum,
      CONFERENCE: "CONFERENCE" as DepositTypeLevelEnum,
      DIPLOME: "DIPLOME" as DepositTypeLevelEnum,
      JOURNAL: "JOURNAL" as DepositTypeLevelEnum,
      LIVRE: "LIVRE" as DepositTypeLevelEnum,
      RAPPORT: "RAPPORT" as DepositTypeLevelEnum,
    };
    export const DepositTypeEnumTranslate: KeyValue[] = [
      {
        key: DepositTypeLevelEnum.ARTICLE,
        value: MARK_AS_TRANSLATABLE("enum.depositTypeLevelEnum.article"),
      },
      {
        key: DepositTypeLevelEnum.CONFERENCE,
        value: MARK_AS_TRANSLATABLE("enum.depositTypeLevelEnum.conference"),
      },
      {
        key: DepositTypeLevelEnum.DIPLOME,
        value: MARK_AS_TRANSLATABLE("enum.depositTypeLevelEnum.diplome"),
      },
      {
        key: DepositTypeLevelEnum.JOURNAL,
        value: MARK_AS_TRANSLATABLE("enum.depositTypeLevelEnum.journal"),
      },
      {
        key: DepositTypeLevelEnum.LIVRE,
        value: MARK_AS_TRANSLATABLE("enum.depositTypeLevelEnum.livre"),
      },
      {
        key: DepositTypeLevelEnum.RAPPORT,
        value: MARK_AS_TRANSLATABLE("enum.depositTypeLevelEnum.rapport"),
      },
    ];
  }
  export namespace PublicationDownload {
    export type StatusEnum = "IN_PROGRESS" | "SUBMITTED" | "IN_PREPARATION" | "READY" | "IN_ERROR";
    export const StatusEnum = {
      IN_PROGRESS: "IN_PROGRESS" as StatusEnum,
      SUBMITTED: "SUBMITTED" as StatusEnum,
      IN_PREPARATION: "IN_PREPARATION" as StatusEnum,
      READY: "READY" as StatusEnum,
      IN_ERROR: "IN_ERROR" as StatusEnum,
    };
  }
}
