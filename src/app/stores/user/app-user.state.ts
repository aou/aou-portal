/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-user.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {UserProfileDialog} from "@app/components/dialogs/user-profile/user-profile.dialog";
import {AppExtendAction} from "@app/stores/app.action";
import {AppPersonAction} from "@app/stores/person/app-person.action";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonDepositAction} from "@app/stores/person/deposit/app-person-deposit.action";
import {AppPersonStructureAction} from "@app/stores/person/structure/app-person-structure.action";
import {AppPersonValidationRightStructureAction} from "@app/stores/person/validation-right/app-person-validation-right-structure.action";
import {
  AppUserAction,
  appUserActionNameSpace,
} from "@app/stores/user/app-user.action";
import {AppUserLogoAction} from "@app/stores/user/user-logo/app-user-logo.action";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {ApmService} from "@elastic/apm-rum-angular";
import {environment} from "@environments/environment";
import {User} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AppTourService} from "@shared/services/app-tour.service";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  DialogUtil,
  isEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  MemoizedUtil,
  NotificationService,
  QueryParameters,
  SolidifyAppUserState,
  SolidifyAppUserStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export const defaultAppUserStateModel: () => AppUserStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
    userAuthorizedForDepositActions: undefined,
  });

export interface AppUserStateModel extends SolidifyAppUserStateModel<User> {
  userAuthorizedForDepositActions: boolean;
}

@Injectable()
@State<AppUserStateModel>({
  name: StateEnum.application_user,
  defaults: {
    ...defaultAppUserStateModel(),
  },
  children: [
    AppUserLogoState,
  ],
})
export class AppUserState extends SolidifyAppUserState<AppUserStateModel, User> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              protected readonly _appTourService: AppTourService,
              private readonly _dialog: MatDialog,
              private readonly _securityService: SecurityService,
              protected readonly _apmService: ApmService) {
    super(_apiService, _store, _notificationService, _actions$, _httpClient, {
      nameSpace: appUserActionNameSpace,
      apiActionEnumAuthenticated: ApiActionNameEnum.AUTHENTICATED,
    }, environment, _apmService);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminUsers;
  }

  @Action(AppUserAction.GetCurrentUserSuccess)
  getCurrentUserSuccess(ctx: SolidifyStateContext<AppUserStateModel>, action: AppUserAction.GetCurrentUserSuccess): void {
    ctx.patchState({
      current: action.user,
    });

    ctx.dispatch(new AppPersonAction.AddCurrentPerson(action.user.person));
    ctx.dispatch(new AppPersonDepositAction.GetAll(MemoizedUtil.selectSnapshot(this._store, AppPersonState, state => action.user.person.resId)));

    if (isNotNullNorUndefined(action.user?.person?.avatar)) {
      ctx.dispatch(new AppUserLogoAction.GetFile(action.user.person.resId));
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new AppUserAction.IsUserMemberOfAuthorizedInstitutions(action.user.resId),
      AppUserAction.IsUserMemberOfAuthorizedInstitutionsSuccess,
      result => {
        this._initUserDependingIsUnigeOrHugMemberOrNot(ctx, result.isMember, action.user);
      }, AppUserAction.IsUserMemberOfAuthorizedInstitutionsFail,
      () => {
        this._initUserDependingIsUnigeOrHugMemberOrNot(ctx, false, action.user);
      }));
  }

  private _initUserDependingIsUnigeOrHugMemberOrNot(ctx: SolidifyStateContext<AppUserStateModel>, isUnigeOrHug: boolean, user: User): void {
    if (!isUnigeOrHug) {
      this._displayAppTourIfFirstLogin(user);
      return;
    }

    ctx.dispatch(new AppPersonValidationRightStructureAction.GetAll(user.person.resId, new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo)));
    ctx.dispatch(new AppExtendAction.UpdateNotificationInbox());

    if (this._securityService.isRoot()) {
      this._displayAppTourIfFirstLogin(user);
      return;
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(ctx, this._actions$,
      new AppPersonStructureAction.GetAll(user.person.resId, new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo)),
      AppPersonStructureAction.GetAllSuccess,
      result => {
        const personStructure = result.list._data;
        if (isEmptyArray(personStructure)) {
          DialogUtil.open(this._dialog, UserProfileDialog, user, {
            width: "90%",
            disableClose: true,
          }, dialogResult => {
            this._displayAppTourIfFirstLogin(user);
          });
        } else {
          this._displayAppTourIfFirstLogin(user);
        }
      },
      AppPersonStructureAction.GetAllFail,
      () => {
        this._displayAppTourIfFirstLogin(user);
      }));
  }

  @Action(AppUserAction.IsUserMemberOfAuthorizedInstitutions)
  isUserMemberOfInstitutionsAuthorized(ctx: SolidifyStateContext<AppUserStateModel>, action: AppUserAction.IsUserMemberOfAuthorizedInstitutions): Observable<boolean> {
    return this._apiService.get<boolean>(this._urlResource + urlSeparator + ApiActionNameEnum.IS_USER_MEMBER_OF_AUTHORIZED_INSTITUTION, null)
      .pipe(
        tap(m => {
          ctx.dispatch(new AppUserAction.IsUserMemberOfAuthorizedInstitutionsSuccess(action, m));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppUserAction.IsUserMemberOfAuthorizedInstitutionsFail(action));
          throw error;
        }),
      );
  }

  @Action(AppUserAction.IsUserMemberOfAuthorizedInstitutionsSuccess)
  isUserMemberOfInstitutionsAuthorizedSuccess(ctx: SolidifyStateContext<AppUserStateModel>, action: AppUserAction.IsUserMemberOfAuthorizedInstitutionsSuccess): void {
    ctx.patchState({
      userAuthorizedForDepositActions: action.isMember,
    });
  }

  @Action(AppUserAction.IsUserMemberOfAuthorizedInstitutionsFail)
  isUserMemberOfInstitutionsAuthorizedFail(ctx: SolidifyStateContext<AppUserStateModel>, action: AppUserAction.IsUserMemberOfAuthorizedInstitutionsFail): void {
    ctx.patchState({
      userAuthorizedForDepositActions: false,
    });
  }

  private _displayAppTourIfFirstLogin(user: User): void {
    if (isNullOrUndefined(user.lastLoginTime) && isTrue(environment.displayTour)) {
      this._appTourService.runFullTour();
    }
  }
}
