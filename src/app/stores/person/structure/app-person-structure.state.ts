/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-person-structure.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpErrorResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {
  AppPersonStructureAction,
  appPersonStructureActionNameSpace,
} from "@app/stores/person/structure/app-person-structure.action";
import {environment} from "@environments/environment";
import {Structure} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  AssociationActionHelper,
  AssociationState,
  AssociationStateModel,
  defaultAssociationStateInitValue,
  ErrorDto,
  isFunction,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export const defaultAppPersonStructureStateStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);

export const defaultAppPersonStructureStateStateModel: () => AppPersonStructureStateModel = () =>
  ({
    ...defaultAssociationStateInitValue(),
    queryParameters: defaultAppPersonStructureStateStateQueryParameters(),
  });

export interface AppPersonStructureStateModel extends AssociationStateModel<Structure> {
}

@Injectable()
@State<AppPersonStructureStateModel>({
  name: StateEnum.application_person_structure,
  defaults: {
    ...defaultAppPersonStructureStateStateModel(),
  },
})
export class AppPersonStructureState extends AssociationState<AppPersonStructureStateModel, Structure> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appPersonStructureActionNameSpace,
      resourceName: ApiResourceNameEnum.STRUCTURES,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  @OverrideDefaultAction()
  @Action(AppPersonStructureAction.Create)
  create(ctx: SolidifyStateContext<AppPersonStructureStateModel>, action: AppPersonStructureAction.Create): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    if (action.listResId.length === 0) {
      ctx.dispatch(AssociationActionHelper.createSuccess(this._nameSpace, action));
      return;
    }
    const url = isFunction(this._options.apiPathCreate) ? this._options.apiPathCreate(action.parentId) :
      this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + action.parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
    return this._apiService.post<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(new AppPersonStructureAction.CreateSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          let errorDto: ErrorDto = undefined;
          if (error instanceof HttpErrorResponse) {
            errorDto = error.error;
          }
          ctx.dispatch(new AppPersonStructureAction.CreateFail(action, errorDto));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(AppPersonStructureAction.CreateFail)
  createFail(ctx: SolidifyStateContext<AppPersonStructureStateModel>, action: AppPersonStructureAction.CreateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

}
