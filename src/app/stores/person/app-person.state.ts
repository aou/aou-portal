/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  AppPersonAction,
  appPersonActionNameSpace,
} from "@app/stores/person/app-person.action";
import {AppPersonDepositState} from "@app/stores/person/deposit/app-person-deposit.state";
import {AppPersonNotificationTypeAction} from "@app/stores/person/notification-type/app-person-notification-type.action";
import {
  AppPersonNotificationTypeState,
  AppPersonNotificationTypeStateModel,
} from "@app/stores/person/notification-type/app-person-notification-type.state";
import {AppPersonResearchGroupAction} from "@app/stores/person/research-group/app-person-research-group.action";
import {
  AppPersonResearchGroupState,
  AppPersonResearchGroupStateModel,
} from "@app/stores/person/research-group/app-person-research-group.state";
import {AppPersonStructureAction} from "@app/stores/person/structure/app-person-structure.action";
import {
  AppPersonStructureState,
  AppPersonStructureStateModel,
} from "@app/stores/person/structure/app-person-structure.state";
import {
  AppPersonValidationRightStructureState,
  AppPersonValidationRightStructureStateModel,
} from "@app/stores/person/validation-right/app-person-validation-right-structure.state";
import {environment} from "@environments/environment";
import {Person} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultRelation2TiersStateInitValue,
  defaultRelation3TiersStateInitValue,
  defaultResourceStateInitValue,
  DispatchMethodEnum,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export interface AppPersonStateModel extends ResourceStateModel<Person> {
  [StateEnum.application_person_structure]: AppPersonStructureStateModel;
  [StateEnum.application_person_validationRight]: AppPersonValidationRightStructureStateModel;
  [StateEnum.application_person_notificationType]: AppPersonNotificationTypeStateModel;
  [StateEnum.application_person_researchGroup]: AppPersonResearchGroupStateModel;
  [StateEnum.application_person_deposit]: AppPersonResearchGroupStateModel;
}

@Injectable()
@State<AppPersonStateModel>({
  name: StateEnum.application_person,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.application_person_structure]: {...defaultAssociationStateInitValue()},
    [StateEnum.application_person_validationRight]: {...defaultRelation3TiersStateInitValue()},
    [StateEnum.application_person_notificationType]: {...defaultRelation2TiersStateInitValue()},
    [StateEnum.application_person_researchGroup]: {...defaultAssociationStateInitValue()},
    [StateEnum.application_person_deposit]: {...defaultAssociationStateInitValue()},
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [
    AppPersonStructureState,
    AppPersonValidationRightStructureState,
    AppPersonNotificationTypeState,
    AppPersonDepositState,
    AppPersonResearchGroupState,
  ],
})
export class AppPersonState extends ResourceState<AppPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appPersonActionNameSpace,
      keepCurrentStateAfterUpdate: true,
      updateSubResourceDispatchMethod: DispatchMethodEnum.PARALLEL,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AppPersonStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && state[StateEnum.application_person_structure].isLoadingCounter === 0
      && state[StateEnum.application_person_notificationType].isLoadingCounter === 0
      && state[StateEnum.application_person_researchGroup].isLoadingCounter === 0
      && state[StateEnum.application_person_validationRight].isLoadingCounter === 0;
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AppPersonStateModel): boolean {
    return true;
  }

  // Override method here to form to use url override
  @OverrideDefaultAction()
  @Action(AppPersonAction.GetById)
  getById(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.GetById): Observable<Person> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: null,
    });

    return this._apiService.getById<Person>(this._urlResource, action.id)
      .pipe(
        tap((model: Person) => {
          ctx.dispatch(new AppPersonAction.GetByIdSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppPersonAction.GetByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppPersonAction.AddCurrentPerson)
  getCurrentUserSuccess(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.AddCurrentPerson): void {
    ctx.patchState({
      current: action.model,
    });
  }

  protected override _getListActionsUpdateSubResource(model: Person, action: AppPersonAction.Create | AppPersonAction.Update, ctx: SolidifyStateContext<AppPersonStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);
    const personId = model.resId;
    const newNotificationType = action.modelFormControlEvent.model.notificationTypes.map(o => o.resId);
    const newResearchGroups = action.modelFormControlEvent.model.researchGroups.map(o => o.resId);
    const newStructures = action.modelFormControlEvent.model.structures.map(o => o.resId);
    actions.push(...[
      {
        action: new AppPersonStructureAction.Update(personId, newStructures),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppPersonStructureAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppPersonStructureAction.UpdateFail)),
        ],
      },
      {
        action: new AppPersonNotificationTypeAction.Update(personId, newNotificationType),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppPersonNotificationTypeAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppPersonNotificationTypeAction.UpdateFail)),
        ],
      },
      {
        action: new AppPersonResearchGroupAction.Update(personId, newResearchGroups),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppPersonResearchGroupAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppPersonResearchGroupAction.UpdateFail)),
        ],
      },
    ]);
    return actions;
  }

  @Action(AppPersonAction.ImportAllFromOrcid)
  importAllFromOrcid(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.ImportAllFromOrcid): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<void, Result>(`${this._urlResource}/${action.personId}/${ApiActionNameEnum.IMPORT_ALL_FROM_ORCID}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new AppPersonAction.ImportAllFromOrcidSuccess(action, result));
          } else {
            ctx.dispatch(new AppPersonAction.ImportAllFromOrcidFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppPersonAction.ImportAllFromOrcidFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppPersonAction.ImportAllFromOrcidSuccess)
  importAllFromOrcidSuccess(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.ImportAllFromOrcidSuccess): void {
    this._notificationService.showSuccess(action.result?.message);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(AppPersonAction.ImportAllFromOrcidFail)
  importAllFromOrcidFail(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.ImportAllFromOrcidFail): void {
    if (isNotNullNorUndefinedNorWhiteString(action.result?.message)) {
      if (action.result.status === ResultActionStatusEnum.NON_APPLICABLE) {
        this._notificationService.showInformation(action.result?.message);
      } else if (action.result.status === ResultActionStatusEnum.NOT_EXECUTED) {
        this._notificationService.showWarning(action.result?.message);
      }
    } else {
      this._notificationService.showError(LabelTranslateEnum.notificationUnableToImportAllFromOrcid);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(AppPersonAction.ExportAllToOrcid)
  exportAllToOrcid(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.ExportAllToOrcid): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<void, Result>(`${this._urlResource}/${action.personId}/${ApiActionNameEnum.EXPORT_ALL_TO_ORCID}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new AppPersonAction.ExportAllToOrcidSuccess(action, result));
          } else {
            ctx.dispatch(new AppPersonAction.ExportAllToOrcidFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppPersonAction.ExportAllToOrcidFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppPersonAction.ExportAllToOrcidSuccess)
  exportAllToOrcidSuccess(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.ExportAllToOrcidSuccess): void {
    this._notificationService.showSuccess(action.result?.message);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(AppPersonAction.ExportAllToOrcidFail)
  exportAllToOrcidFail(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.ExportAllToOrcidFail): void {
    if (isNotNullNorUndefinedNorWhiteString(action.result?.message)) {
      if (action.result.status === ResultActionStatusEnum.NON_APPLICABLE) {
        this._notificationService.showInformation(action.result?.message);
      } else if (action.result.status === ResultActionStatusEnum.NOT_EXECUTED) {
        this._notificationService.showWarning(action.result?.message);
      }
    } else {
      this._notificationService.showError(LabelTranslateEnum.notificationUnableToExportAllToOrcid);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
