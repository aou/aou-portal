/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-person-notification-type.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {appPersonNotificationTypeActionNameSpace} from "@app/stores/person/notification-type/app-person-notification-type.action";
import {environment} from "@environments/environment";
import {
  NotificationType,
  PersonNotificationType,
} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultRelation2TiersStateInitValue,
  NotificationService,
  QueryParameters,
  Relation2TiersState,
  Relation2TiersStateModel,
} from "solidify-frontend";

export const defaultAppPersonNotificationTypeStateStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);

export const defaultAppPersonNotificationTypeStateStateModel: () => AppPersonNotificationTypeStateModel = () =>
  ({
    ...defaultRelation2TiersStateInitValue(),
    queryParameters: defaultAppPersonNotificationTypeStateStateQueryParameters(),
  });

export interface AppPersonNotificationTypeStateModel extends Relation2TiersStateModel<NotificationType> {
}

@Injectable()
@State<AppPersonNotificationTypeStateModel>({
  name: StateEnum.application_person_notificationType,
  defaults: {
    ...defaultAppPersonNotificationTypeStateStateModel(),
  },
})
export class AppPersonNotificationTypeState extends Relation2TiersState<AppPersonNotificationTypeStateModel, NotificationType, PersonNotificationType> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appPersonNotificationTypeActionNameSpace,
      resourceName: ApiResourceNameEnum.NOTIFICATION_TYPES,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }
}
