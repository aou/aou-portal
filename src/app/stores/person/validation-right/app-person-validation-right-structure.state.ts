/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-person-validation-right-structure.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  AppPersonValidationRightStructureAction,
  appPersonValidationRightStructureActionNameSpace,
} from "@app/stores/person/validation-right/app-person-validation-right-structure.action";
import {environment} from "@environments/environment";

import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {StructurePersonPublicationSubtype} from "@shared/models/business/structure-person-publication-subtype.model";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {
  ApiService,
  defaultRelation3TiersStateInitValue,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  Relation3TiersForm,
  Relation3TiersState,
  Relation3TiersStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export const defaultAppPersonValidationRightStructureStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);

export const defaultAppPersonValidationRightStructureStateModel: () => AppPersonValidationRightStructureStateModel = () =>
  ({
    ...defaultRelation3TiersStateInitValue(),
    queryParameters: defaultAppPersonValidationRightStructureStateQueryParameters(),
  });

export interface AppPersonValidationRightStructureStateModel extends Relation3TiersStateModel<ValidationRight> {
}

@Injectable()
@State<AppPersonValidationRightStructureStateModel>({
  name: StateEnum.application_person_validationRight,
  defaults: {
    ...defaultAppPersonValidationRightStructureStateModel(),
  },
})
// PersonValidationRightStructureController
export class AppPersonValidationRightStructureState extends Relation3TiersState<AppPersonValidationRightStructureStateModel, ValidationRight, StructurePersonPublicationSubtype> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appPersonValidationRightStructureActionNameSpace,
      resourceName: ApiResourceNameEnum.VALIDATION_RIGHTS_STRUCTURES,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  protected _convertResourceInForm(resource: ValidationRight): Relation3TiersForm {
    return {id: resource.resId, listId: resource.publicationSubtypes.map(r => r.resId)};
  }

  @OverrideDefaultAction()
  @Action(AppPersonValidationRightStructureAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<AppPersonValidationRightStructureStateModel>, action: AppPersonValidationRightStructureAction.UpdateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
