/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-deposit-subtype.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {StateEnum} from "@app/shared/enums/state.enum";
import {
  AppDepositSubtypeAction,
  appDepositSubtypeActionNameSpace,
} from "@app/stores/deposit-subtype/app-deposit-subtype.action";
import {environment} from "@environments/environment";
import {
  DepositSubtype,
  DepositType,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
  pipe,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isFunction,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  ResourceActionHelper,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  TransferStateService,
} from "solidify-frontend";

export interface AppDepositSubtypeStateModel extends ResourceStateModel<DepositType> {
}

@Injectable()
@State<AppDepositSubtypeStateModel>({
  name: StateEnum.application_depositSubtype,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "sortValue", order: OrderEnum.ascending}),
  },
})
export class AppDepositSubtypeState extends ResourceState<AppDepositSubtypeStateModel, DepositSubtype> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _transferState: TransferStateService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appDepositSubtypeActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublicationSubtypes;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(AppDepositSubtypeAction.GetAll)
  getAll(ctx: SolidifyStateContext<AppDepositSubtypeStateModel>, action: AppDepositSubtypeAction.GetAll): Observable<CollectionTyped<DepositSubtype>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        total: 0,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    const STATE_KEY = makeStateKey<CollectionTyped<DepositSubtype>>(`${this._stateName}-GetAll`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const collectionTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(ResourceActionHelper.getAllSuccess<DepositSubtype>(this._nameSpace, action, collectionTransferState));
      return of(collectionTransferState);
    }

    const baseUrl = isFunction(this._optionsState.apiPathGetAll) ? this._optionsState.apiPathGetAll() : this._urlResource;
    return this._apiService.getCollection<DepositSubtype>(baseUrl, ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<DepositSubtype>) => {
          this._transferState.set(STATE_KEY, collection);
          ctx.dispatch(ResourceActionHelper.getAllSuccess<DepositSubtype>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}
