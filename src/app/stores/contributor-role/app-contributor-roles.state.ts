/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-contributor-roles.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {AppContributorRolesAction} from "@app/stores/contributor-role/app-contributor-roles.action";
import {environment} from "@environments/environment";
import {PublicationSubtypeContributorRoleDTO} from "@models";
import {
  Action,
  Selector,
  State,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  defaultResourceStateInitValue,
  MappingObject,
  QueryParameters,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
  TransferStateService,
} from "solidify-frontend";

export interface AppContributorRolesStateModel extends ResourceStateModel<PublicationSubtypeContributorRoleDTO> {
  map: MappingObject<string, PublicationSubtypeContributorRoleDTO[]> | undefined;
}

@Injectable()
@State<AppContributorRolesStateModel>({
  name: StateEnum.application_contributorRoles,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
    map: {},
  },
  children: [],
})
export class AppContributorRolesState extends BasicState<AppContributorRolesStateModel> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _transferState: TransferStateService) {
    super();
  }

  protected get _urlResource(): string {
    return ApiEnum.adminContributorRoles + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameEnum.LIST_CONTRIBUTOR_ROLES;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(AppContributorRolesAction.GetAll)
  getAll(ctx: SolidifyStateContext<AppContributorRolesStateModel>, action: AppContributorRolesAction.GetAll): Observable<MappingObject<string, PublicationSubtypeContributorRoleDTO[]>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    const STATE_KEY = makeStateKey<MappingObject<string, PublicationSubtypeContributorRoleDTO[]>>(`${this._stateName}-GetAll`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const mapTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new AppContributorRolesAction.GetAllSuccess(action, mapTransferState));
      return of(mapTransferState);
    }

    return this._apiService.get<MappingObject<string, PublicationSubtypeContributorRoleDTO[]>>(this._urlResource)
      .pipe(
        tap(map => {
          this._transferState.set(STATE_KEY, map);
          ctx.dispatch(new AppContributorRolesAction.GetAllSuccess(action, map));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppContributorRolesAction.GetAllFail(action));
          throw error;
        }),
      );
  }

  @Action(AppContributorRolesAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<AppContributorRolesStateModel>, action: AppContributorRolesAction.GetAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      map: action.map,
    });
  }

  @Action(AppContributorRolesAction.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<AppContributorRolesStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      map: {},
    });
  }
}
