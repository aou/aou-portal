/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-toc.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {AppState} from "@app/stores/app.state";
import {AppTocAction} from "@app/stores/toc/app-toc.action";
import {environment} from "@environments/environment";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  defaultBaseStateInitValue,
  HeaderEnum,
  isArray,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  SOLIDIFY_CONSTANTS,
  SolidifyError,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

export interface AppTocStateModel extends BaseStateModel {
  toolsGuide: string | undefined;
  integrationGuide: string | undefined;
  userDocumentation: string | undefined;
  validatorDocumentation: string | undefined;
  apis: MappingObject<string, string> | undefined;
}

@Injectable()
@State<AppTocStateModel>({
  name: StateEnum.application_toc,
  defaults: {
    ...defaultBaseStateInitValue(),
    toolsGuide: undefined,
    integrationGuide: undefined,
    userDocumentation: undefined,
    validatorDocumentation: undefined,
    apis: undefined,
  },
})
export class AppTocState extends BasicState<AppTocStateModel> {
  constructor(protected readonly _httpClient: HttpClient,
              protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super();
  }

  protected get _urlResource(): string {
    return ApiEnum.docs;
  }

  @Action(AppTocAction.GetAllDocumentation)
  getAllDocumentation(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetAllDocumentation): Observable<boolean> {
    if (SsrUtil.isServer) {
      ctx.dispatch(new AppTocAction.GetAllDocumentationSuccess(action));
      return of(true);
    }

    this._incrementLoadingCounter(ctx);

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(
      ctx,
      [
        {action: new AppTocAction.GetIntegrationGuide()},
        {action: new AppTocAction.ComputeApis()},
        {action: new AppTocAction.GetToolsGuide()},
        {action: new AppTocAction.GetUserDocumentation()},
        {action: new AppTocAction.GetValidatorDocumentation()},
      ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AppTocAction.GetAllDocumentationSuccess(action));
        } else {
          ctx.dispatch(new AppTocAction.GetAllDocumentationFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(AppTocAction.GetAllDocumentationSuccess)
  getAllDocumentationSuccess(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetAllDocumentationSuccess): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(AppTocAction.GetAllDocumentationFail)
  getAllDocumentationFail(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetAllDocumentationFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(AppTocAction.GetToolsGuide)
  getToolsGuide(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetToolsGuide): Observable<string> {
    if (isNullOrUndefinedOrWhiteString(environment.documentationTocToolsGuide)) {
      return of(undefined);
    }
    this._incrementLoadingCounter(ctx);

    let headers = new HttpHeaders();
    headers = headers.set("Accept", ["application/xml"]);
    headers = headers.set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);

    return this._httpClient.get(this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + environment.documentationTocToolsGuide, {
      headers,
      observe: "response",
      responseType: "text",
    }).pipe(
      map((response: HttpResponse<string>) =>
        response.body),
      tap((toolsGuide: string) => {
        ctx.dispatch(new AppTocAction.GetToolsGuideSuccess(action, toolsGuide));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AppTocAction.GetToolsGuideFail(action));
        throw new SolidifyError("AppTocState GetToolsGuide", error);
      }),
    );
  }

  @Action(AppTocAction.GetToolsGuideSuccess)
  getToolsGuideSuccess(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetToolsGuideSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      toolsGuide: action.toolsGuide,
    });
  }

  @Action(AppTocAction.GetToolsGuideFail)
  getToolsGuideFail(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetToolsGuideFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(AppTocAction.GetIntegrationGuide)
  getIntegrationGuide(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetIntegrationGuide): Observable<string> {
    if (isNullOrUndefinedOrWhiteString(environment.documentationTocIntegrationGuide)) {
      return of(undefined);
    }
    this._incrementLoadingCounter(ctx);

    let headers = new HttpHeaders();
    headers = headers.set("Accept", ["application/xml"]);
    headers = headers.set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);

    return this._httpClient.get(this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + environment.documentationTocIntegrationGuide, {
      headers,
      observe: "response",
      responseType: "text",
    }).pipe(
      map((response: HttpResponse<string>) =>
        response.body),
      tap((integrationGuide: string) => {
        ctx.dispatch(new AppTocAction.GetIntegrationGuideSuccess(action, integrationGuide));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AppTocAction.GetIntegrationGuideFail(action));
        throw new SolidifyError("AppTocState GetIntegrationGuide", error);
      }),
    );
  }

  @Action(AppTocAction.GetIntegrationGuideSuccess)
  getIntegrationGuideSuccess(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetIntegrationGuideSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      integrationGuide: action.integrationGuide,
    });
  }

  @Action(AppTocAction.GetIntegrationGuideFail)
  getIntegrationGuideFail(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetIntegrationGuideFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(AppTocAction.ComputeApis)
  computeApis(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.ComputeApis): void {
    const backendModulesUrl = MemoizedUtil.selectSnapshot(this._store, environment.appState, state => state.backendModulesUrl);
    const mappingObject = {} as MappingObject<string, string>;
    MappingObjectUtil.forEach(backendModulesUrl, (value, key) => {
      if (isNullOrUndefinedOrWhiteString(value) || environment.swaggerModulesToIgnore.includes(key)) {
        return;
      }
      if (isArray(value)) {
        value.forEach((v, index) => {
          MappingObjectUtil.set(mappingObject, key, AppState.getModuleSwaggerUrl(key + "-" + (index + 1), v));
        });
      } else {
        MappingObjectUtil.set(mappingObject, key, AppState.getModuleSwaggerUrl(key, value));
      }
    });
    ctx.patchState({
      apis: mappingObject,
    });
  }

  @Action(AppTocAction.GetUserDocumentation)
  getUserDocumentation(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetUserDocumentation): Observable<string> | Observable<undefined> {
    if (isNullOrUndefinedOrWhiteString(environment.documentationTocUserGuide)) {
      return of(undefined);
    }
    this._incrementLoadingCounter(ctx);

    let headers = new HttpHeaders();
    headers = headers.set("Accept", ["application/xml"]);
    headers = headers.set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);

    return this._httpClient.get((isNotNullNorUndefined(environment.documentationTocUserGuidePath) ? environment.documentationTocUserGuidePath : this._urlResource) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + environment.documentationTocUserGuide, {
      headers,
      observe: "response",
      responseType: "text",
    }).pipe(
      map((response: HttpResponse<string>) =>
        response.body),
      tap((userDocumentation: string) => {
        ctx.dispatch(new AppTocAction.GetUserDocumentationSuccess(action, userDocumentation));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AppTocAction.GetUserDocumentationFail(action));
        throw new SolidifyError("AppTocState GetUserDocumentation", error);
      }),
    );
  }

  @Action(AppTocAction.GetUserDocumentationSuccess)
  getUserDocumentationSuccess(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetUserDocumentationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      userDocumentation: action.userDocumentation,
    });
  }

  @Action(AppTocAction.GetUserDocumentationFail)
  getUserDocumentationFail(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetUserDocumentationFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(AppTocAction.GetValidatorDocumentation)
  getValidatorDocumentation(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetValidatorDocumentation): Observable<string> | Observable<undefined> {
    if (isNullOrUndefinedOrWhiteString(environment.documentationTocValidatorGuide)) {
      return of(undefined);
    }
    this._incrementLoadingCounter(ctx);

    let headers = new HttpHeaders();
    headers = headers.set("Accept", ["application/xml"]);
    headers = headers.set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);

    return this._httpClient.get((isNotNullNorUndefined(environment.documentationTocValidatorGuidePath) ? environment.documentationTocValidatorGuidePath : this._urlResource) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + environment.documentationTocValidatorGuide, {
      headers,
      observe: "response",
      responseType: "text",
    }).pipe(
      map((response: HttpResponse<string>) =>
        response.body),
      tap((validatorDocumentation: string) => {
        ctx.dispatch(new AppTocAction.GetValidatorDocumentationSuccess(action, validatorDocumentation));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AppTocAction.GetValidatorDocumentationFail(action));
        throw new SolidifyError("AppTocState GetValidatorDocumentation", error);
      }),
    );
  }

  @Action(AppTocAction.GetValidatorDocumentationSuccess)
  getValidatorDocumentationSuccess(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetValidatorDocumentationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      validatorDocumentation: action.validatorDocumentation,
    });
  }

  @Action(AppTocAction.GetValidatorDocumentationFail)
  getValidatorDocumentationFail(ctx: SolidifyStateContext<AppTocStateModel>, action: AppTocAction.GetValidatorDocumentationFail): void {
    this._decrementLoadingCounter(ctx);
  }
}
