/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  Inject,
  Injectable,
  LOCALE_ID,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppExtendAction} from "@app/stores/app.action";
import {AppContributorRolesState} from "@app/stores/contributor-role/app-contributor-roles.state";
import {AppDepositSubtypeState} from "@app/stores/deposit-subtype/app-deposit-subtype.state";
import {AppNotificationAction} from "@app/stores/notification/app-notification.action";
import {AppNotificationState} from "@app/stores/notification/app-notification.state";
import {
  AppPersonState,
  AppPersonStateModel,
} from "@app/stores/person/app-person.state";
import {AppPinboardState} from "@app/stores/pinboard/app-pinboard.state";
import {
  AppSearchState,
  AppSearchStateModel,
} from "@app/stores/search/app-search.state";
import {AppStoredSearchesState} from "@app/stores/stored-searches/app-stored-searches.state";
import {
  AppSystemPropertyState,
  AppSystemPropertyStateModel,
} from "@app/stores/system-property/app-system-property.state";
import {
  AppTocState,
  AppTocStateModel,
} from "@app/stores/toc/app-toc.state";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {
  AppUserState,
  AppUserStateModel,
  defaultAppUserStateModel,
} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  ApplicationRole,
  Person,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SessionStorageEnum} from "@shared/enums/session-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Token} from "@shared/models/token.model";
import {SecurityService} from "@shared/services/security.service";
import {HighlightLoader} from "ngx-highlightjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  APP_OPTIONS,
  AppBannerState,
  AppCarouselState,
  AppConfigService,
  defaultSolidifyApplicationAppStateInitValue,
  GlobalBanner,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NativeNotificationService,
  NotificationService,
  OAuth2Service,
  ofSolidifyActionCompleted,
  PollingHelper,
  PrivacyPolicyTermsOfUseApprovalDialog,
  QueryParameters,
  SessionStorageHelper,
  SOLIDIFY_CONSTANTS,
  SolidifyAppAction,
  SolidifyApplicationAppOptions,
  SolidifyApplicationAppState,
  SolidifyApplicationAppStateModel,
  SolidifyStateContext,
  StoreUtil,
  TransferStateService,
  VisualizationState,
} from "solidify-frontend";

export interface AppStateModel extends SolidifyApplicationAppStateModel {
  userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[];
  application_person: AppPersonStateModel | undefined;
  application_user: AppUserStateModel | undefined;
  application_toc: AppTocStateModel | undefined;
  globalBanner: GlobalBanner;
  isOpenedSidebarValidatorGuide: boolean;
  [StateEnum.application_systemProperty]: AppSystemPropertyStateModel;
  [StateEnum.application_search]: AppSearchStateModel;
}

@Injectable()
@State<AppStateModel>({
  name: StateEnum.application,
  defaults: {
    ...defaultSolidifyApplicationAppStateInitValue(),
    userRoles: [Enums.UserApplicationRole.UserApplicationRoleEnum.guest],
    theme: environment.theme,
    application_person: undefined,
    application_toc: undefined,
    globalBanner: undefined,
    isOpenedSidebarValidatorGuide: false,
    [StateEnum.application_systemProperty]: undefined,
    [StateEnum.application_search]: undefined,
    [StateEnum.application_user]: {...defaultAppUserStateModel()},
  },
  children: [
    AppPinboardState,
    AppUserState,
    AppPersonState,
    AppTocState,
    AppSystemPropertyState,
    AppBannerState,
    AppCarouselState,
    AppNotificationState,
    AppSearchState,
    AppStoredSearchesState,
    AppContributorRolesState,
    AppDepositSubtypeState,
    VisualizationState,
  ],
})
export class AppState extends SolidifyApplicationAppState<AppStateModel> {
  static readonly KEY_ONLY_WITH_NULL_READ_TIME: string = "onlyWithNullReadTime";

  constructor(@Inject(LOCALE_ID) protected readonly _locale: string,
              protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _oauthService: OAuth2Service,
              protected readonly _actions$: Actions,
              protected readonly _apiService: ApiService,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              @Inject(APP_OPTIONS) protected readonly _optionsState: SolidifyApplicationAppOptions,
              protected readonly _hljsLoader: HighlightLoader,
              protected readonly _nativeNotificationService: NativeNotificationService,
              protected readonly _securityService: SecurityService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _dialog: MatDialog,
              protected readonly _transferState: TransferStateService,
  ) {
    super(
      _locale,
      _store,
      _translate,
      _oauthService,
      _actions$,
      _apiService,
      _httpClient,
      _notificationService,
      _optionsState,
      environment,
      _hljsLoader,
      _nativeNotificationService,
      _appConfigService,
      _dialog,
      PrivacyPolicyTermsOfUseApprovalDialog,
      _securityService,
      _transferState,
    );
  }

  @Selector()
  static currentPerson(state: AppStateModel): Person {
    return state.application_person.current;
  }

  @Selector()
  static currentUser(state: AppStateModel): User {
    return state.application_user.current;
  }

  @Selector()
  static currentUserApplicationRole(state: AppStateModel): ApplicationRole {
    const currentPersonRole = this.currentUser(state);
    if (isNullOrUndefined(currentPersonRole)) {
      return undefined;
    }
    return currentPersonRole.applicationRole;
  }

  @Selector()
  static currentUserApplicationRoleResId(state: AppStateModel): string | undefined {
    const applicationRole = this.currentUserApplicationRole(state);
    if (isNullOrUndefined(applicationRole)) {
      return undefined;
    }
    return applicationRole.resId;
  }

  override doLoginSuccess(ctx: SolidifyStateContext<AppStateModel>, action: SolidifyAppAction.LoginSuccess): Token {
    const tokenDecoded = super.doLoginSuccess(ctx, action) as Token;

    ctx.patchState({
      userRoles: tokenDecoded.authorities,
    });
    this.subscribe(StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new AppUserAction.GetCurrentUser(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppUserAction.GetCurrentUserSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppUserAction.GetCurrentUserFail)),
        ],
      },
    ]));
    return tokenDecoded;
  }

  override _treatmentAfterRevokeToken(ctx: SolidifyStateContext<AppStateModel>): void {
    super._treatmentAfterRevokeToken(ctx);
    ctx.patchState({
      userRoles: [Enums.UserApplicationRole.UserApplicationRoleEnum.guest],
      application_user: {...defaultAppUserStateModel()},
    });
  }

  @Action(AppExtendAction.StartPollingNotification)
  startPollingNotification(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.StartPollingNotification): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshNotificationsAvailableIntervalInSecond,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      maximumIntervalRefreshInSecond: environment.refreshNotificationsAvailableIntervalInSecond * 10,
      filter: () => isNotNullNorUndefinedNorWhiteString(this._oauthService.getAccessToken())
        && isNotNullNorUndefined(MemoizedUtil.currentSnapshot(this._store, AppPersonState)
          && MemoizedUtil.selectSnapshot(this._store, AppUserState, state => state.userAuthorizedForDepositActions)),
      actionToDo: () => {
        this._store.dispatch(new AppExtendAction.UpdateNotificationInbox());
      },
    }));
  }

  @Action(AppExtendAction.UpdateNotificationInbox)
  updateNotificationInbox(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.UpdateNotificationInbox): void {
    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
    const searchItems = queryParameters.search.searchItems;
    MappingObjectUtil.set(searchItems, AppState.KEY_ONLY_WITH_NULL_READ_TIME, SOLIDIFY_CONSTANTS.STRING_TRUE);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(AppNotificationAction.GetAllSuccess),
      take(1),
      tap(result => {
        if (!result.result.successful) {
          return;
        }
        const actionSuccess = (result.action as AppNotificationAction.GetAllSuccess);
        const listNewNotificationInboxPending = actionSuccess.list._data.map(newNotification => newNotification.resId);
        const listOldNotificationInboxPending = SessionStorageHelper.getListItem(SessionStorageEnum.notificationInboxPending);
        let counterNewNotification = 0;
        listNewNotificationInboxPending.forEach(newNotificationId => {
          if (!listOldNotificationInboxPending.includes(newNotificationId)) {
            counterNewNotification++;
          }
        });
        if (counterNewNotification > 0) {
          const actionNotification = {
            text: LabelTranslateEnum.see,
            callback: () => this._store.dispatch(new Navigate([RoutesEnum.notification])),
          };
          this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("app.notification.notification.newIncoming"), {count: counterNewNotification}, actionNotification);
        }
        SessionStorageHelper.initItemInList(SessionStorageEnum.notificationInboxPending, listNewNotificationInboxPending);
      }),
    ));

    this._store.dispatch(new AppNotificationAction.GetAll(queryParameters));
  }

  @Action(AppExtendAction.ChangeDisplaySidebarValidatorGuide)
  changeDisplaySidebarValidatorGuide(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.ChangeDisplaySidebarValidatorGuide): void {
    ctx.patchState({
      isOpenedSidebarValidatorGuide: action.isOpen,
    });
  }
}
