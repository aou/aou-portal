/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-notification.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {appNotificationActionNameSpace} from "@app/stores/notification/app-notification.action";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {environment} from "@environments/environment";
import {Notification} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  MemoizedUtil,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
} from "solidify-frontend";

export interface AppNotificationStateModel extends ResourceStateModel<Notification> {
}

@Injectable()
@State<AppNotificationStateModel>({
  name: StateEnum.application_notification,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class AppNotificationState extends ResourceState<AppNotificationStateModel, Notification> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appNotificationActionNameSpace,
      keepCurrentStateAfterUpdate: true,
    });
  }

  protected get _urlResource(): string {
    const currentPerson = MemoizedUtil.currentSnapshot(this._store, AppPersonState);
    return ApiEnum.adminPeople + urlSeparator + currentPerson?.resId + urlSeparator + ApiResourceNameEnum.NOTIFICATIONS;
  }
}
