/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-pinboard.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonDepositAction} from "@app/stores/person/deposit/app-person-deposit.action";
import {AppPinboardAction} from "@app/stores/pinboard/app-pinboard.action";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  CookieConsentService,
  MemoizedUtil,
  NotificationService,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AppPinboardStateModel extends BaseStateModel {
}

@Injectable()
@State<AppPinboardStateModel>({
  name: StateEnum.application_pinboard,
  defaults: {
    isLoadingCounter: 0,
  },
  children: [],
})
export class AppPinboardState extends BasicState<AppPinboardStateModel> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _cookieConsentService: CookieConsentService) {
    super();
  }

  private get _getCurrentUserId(): string {
    return MemoizedUtil.selectSnapshot(this._store, AppPersonState, state => state.current?.resId);
  }

  @Action(AppPinboardAction.Add)
  add(ctx: SolidifyStateContext<AppPinboardStateModel>, action: AppPinboardAction.Add): void {
    const createAction = new AppPersonDepositAction.Create(this._getCurrentUserId, [action.resId]);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, createAction,
      AppPersonDepositAction.CreateSuccess,
      result => {
        ctx.dispatch(new AppPersonDepositAction.GetAll(this._getCurrentUserId));
        this._notificationService.showSuccess(LabelTranslateEnum.notificationAddToPinboardSuccess);
      },
      AppPersonDepositAction.CreateFail,
      result => {
        this._notificationService.showError(LabelTranslateEnum.notificationAddToPinboardFail);
      }));
  }

  @Action(AppPinboardAction.Remove)
  remove(ctx: SolidifyStateContext<AppPinboardStateModel>, action: AppPinboardAction.Remove): void {
    const deleteAction = new AppPersonDepositAction.Delete(this._getCurrentUserId, action.resId);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, deleteAction,
      AppPersonDepositAction.DeleteSuccess,
      result => {
        ctx.dispatch(new AppPersonDepositAction.GetAll(this._getCurrentUserId));
        this._notificationService.showSuccess(LabelTranslateEnum.notificationRemoveToPinboardSuccess);
      },
      AppPersonDepositAction.DeleteFail,
      result => {
        this._notificationService.showError(LabelTranslateEnum.notificationRemoveToPinboardFail);
      }));
  }
}
