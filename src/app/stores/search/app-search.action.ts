/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-search.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdvancedSearchCriteria} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "solidify-frontend";

const state = StateEnum.application_search;

export namespace AppSearchAction {
  export class GetAllSearchCriteria extends BaseAction {
    static readonly type: string = `[${state}] Get All Search Criteria`;
  }

  export class GetAllSearchCriteriaSuccess extends BaseSubActionSuccess<GetAllSearchCriteria> {
    static readonly type: string = `[${state}] Get All Search Criteria Success`;

    constructor(public parentAction: GetAllSearchCriteria, public list?: AdvancedSearchCriteria[]) {
      super(parentAction);
    }
  }

  export class GetAllSearchCriteriaFail extends BaseSubActionFail<GetAllSearchCriteria> {
    static readonly type: string = `[${state}] Get All Search Criteria Fail`;
  }

}

export const appSearchActionNameSpace = AppSearchAction;
