/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-search.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {AppSearchAction} from "@app/stores/search/app-search.action";
import {Enums} from "@enums";
import {AdvancedSearchCriteria} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  delay,
  Observable,
  of,
  switchMap,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  isNotNullNorUndefined,
  NotificationService,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  TransferStateService,
} from "solidify-frontend";

export interface AppSearchStateModel extends BaseStateModel {
  listAdvancedSearchCriteria: AdvancedSearchCriteria[];
  allContributorCriteria: AdvancedSearchCriteria;
  unigeContributorCriteria: AdvancedSearchCriteria;
  unigeDirectorCriteria: AdvancedSearchCriteria;
  structureCriteria: AdvancedSearchCriteria;
  structureWithChildCriteria: AdvancedSearchCriteria;
  researchGroupCriteria: AdvancedSearchCriteria;
}

@Injectable()
@State<AppSearchStateModel>({
  name: StateEnum.application_search,
  defaults: {
    isLoadingCounter: 0,
    listAdvancedSearchCriteria: undefined,
    allContributorCriteria: undefined,
    unigeContributorCriteria: undefined,
    unigeDirectorCriteria: undefined,
    structureCriteria: undefined,
    structureWithChildCriteria: undefined,
    researchGroupCriteria: undefined,
  },
  children: [],
})
export class AppSearchState extends BasicState<AppSearchStateModel> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _transferState: TransferStateService) {
    super();
  }

  @Action(AppSearchAction.GetAllSearchCriteria)
  getAllSearchCriteria(ctx: SolidifyStateContext<AppSearchStateModel>, action: AppSearchAction.GetAllSearchCriteria): Observable<AdvancedSearchCriteria[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    let listAdvancedSearchCriteria: AdvancedSearchCriteria[] = undefined;

    const STATE_KEY = makeStateKey<AdvancedSearchCriteria[]>("AppState-GetAllSearchCriteria");
    if (this._transferState.hasKey(STATE_KEY)) {
      listAdvancedSearchCriteria = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
    } else {
      listAdvancedSearchCriteria = ctx.getState().listAdvancedSearchCriteria;
    }

    if (isNotNullNorUndefined(listAdvancedSearchCriteria)) {
      return of(undefined).pipe(
        delay(0),
        switchMap(() => ctx.dispatch(new AppSearchAction.GetAllSearchCriteriaSuccess(action, listAdvancedSearchCriteria))),
        map(() => listAdvancedSearchCriteria),
      );
    }

    return this._apiService.getList<AdvancedSearchCriteria>(ApiEnum.accessSettings + urlSeparator + ApiActionNameEnum.LIST_ADVANCED_SEARCH_CRITERIA, undefined)
      .pipe(
        tap((list: AdvancedSearchCriteria[]) => {
          this._transferState.set(STATE_KEY, list);
          ctx.dispatch(new AppSearchAction.GetAllSearchCriteriaSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppSearchAction.GetAllSearchCriteriaFail(action));
          throw error;
        }),
      );
  }

  @Action(AppSearchAction.GetAllSearchCriteriaSuccess)
  getAllSearchCriteriaSuccess(ctx: SolidifyStateContext<AppSearchStateModel>, action: AppSearchAction.GetAllSearchCriteriaSuccess): void {
    const allContributorCriteria = action.list?.find(c => c.type === Enums.Criteria.TypeEnum.CONTRIBUTOR_ALL);
    const unigeContributorCriteria = action.list?.find(c => c.type === Enums.Criteria.TypeEnum.CONTRIBUTOR_UNIGE && c.alias === "cnIndividu");
    const unigeDirectorCriteria = action.list?.find(c => c.type === Enums.Criteria.TypeEnum.CONTRIBUTOR_UNIGE && c.alias === "unigeDirector");
    const structureCriteria = action.list?.find(c => c.type === Enums.Criteria.TypeEnum.STRUCTURE);
    const structureWithChildCriteria = action.list?.find(c => c.type === Enums.Criteria.TypeEnum.STRUCTURE_SUB_STRUCTURES);
    const researchGroupCriteria = action.list?.find(c => c.type === Enums.Criteria.TypeEnum.RESEARCH_GROUP);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listAdvancedSearchCriteria: action.list,
      allContributorCriteria: allContributorCriteria,
      unigeContributorCriteria: unigeContributorCriteria,
      unigeDirectorCriteria: unigeDirectorCriteria,
      structureCriteria: structureCriteria,
      structureWithChildCriteria: structureWithChildCriteria,
      researchGroupCriteria: researchGroupCriteria,
    });
  }

  @Action(AppSearchAction.GetAllSearchCriteriaFail)
  getAllSearchCriteriaFail(ctx: SolidifyStateContext<AppSearchStateModel>, action: AppSearchAction.GetAllSearchCriteriaFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
