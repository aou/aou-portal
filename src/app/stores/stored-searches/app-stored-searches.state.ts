/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-stored-searches.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  appSearchesActionNameSpace,
  AppStoredSearchesAction,
} from "@app/stores/stored-searches/app-stored-searches.action";
import {environment} from "@environments/environment";
import {StoredSearch} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  QueryParameters,
  ResourceActionHelper,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface AppStoredSearchesStateModel extends ResourceStateModel<StoredSearch> {
}

@Injectable()
@State<AppStoredSearchesStateModel>({
  name: StateEnum.application_storedSearches,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class AppStoredSearchesState extends ResourceState<AppStoredSearchesStateModel, StoredSearch> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appSearchesActionNameSpace,
      keepCurrentStateAfterUpdate: true,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("app.searches.notification.resource.create"),
      notificationResourceCreateFailTextToTranslate: MARK_AS_TRANSLATABLE("app.searches.notification.resource.createFail"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("app.searches.notification.resource.update"),
      notificationResourceUpdateFailTextToTranslate: MARK_AS_TRANSLATABLE("app.searches.notification.resource.updateFail"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("app.searches.notification.resource.delete"),
      notificationResourceDeleteFailTextToTranslate: MARK_AS_TRANSLATABLE("app.searches.notification.resource.deleteFail"),
      apiPathGetAll: () => ApiEnum.adminStoredSearches + urlSeparator + ApiActionNameEnum.LIST_MY_STORED_SEARCHES,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminStoredSearches;
  }

  @Action(AppStoredSearchesAction.GetById)
  getById(ctx: SolidifyStateContext<AppStoredSearchesStateModel>, action: AppStoredSearchesAction.GetById): Observable<StoredSearch> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    return this._apiService.getById<StoredSearch>(this._urlResource, action.id)
      .pipe(
        tap((model: StoredSearch) => {
          ctx.dispatch(ResourceActionHelper.getByIdSuccess(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceActionHelper.getByIdFail(this._nameSpace, action));
          throw environment.errorToSkipInErrorHandler;
        }),
      );
  }
}
