/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - contributor.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {
  ChartItemModel,
  ChartModel,
} from "@app/features/contributor/components/presentationals/abstract-chart/abstract-contributor-chart.presentational";
import {metaInfoContributor} from "@app/meta-info-list";
import {AppState} from "@app/stores/app.state";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SearchContextEnum} from "@home/enums/search-context.enum";
import {HomeHelper} from "@home/helpers/home.helper";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {HomeService} from "@home/services/home.service";
import {HomeAction} from "@home/stores/home.action";
import {HomeState} from "@home/stores/home.state";
import {
  PublicationStatistic,
  PublicContributorDto,
  SystemIndexFieldAlias,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ContributorIdentifierTypeEnum} from "@shared/enums/contributor-identifier-type.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SharedPublicContributorAction} from "@shared/stores/public-contributor/shared-public-contributor.action";
import {SharedPublicContributorState} from "@shared/stores/public-contributor/shared-public-contributor.state";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  CollectionTyped,
  DataTableColumns,
  DataTableFieldTypeEnum,
  Facet,
  FacetValue,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  KeyValue,
  LoggingService,
  MappingObjectUtil,
  MemoizedUtil,
  MetaService,
  NotificationService,
  ofSolidifyActionCompleted,
  OrderEnum,
  QueryParameters,
  ScrollService,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-contributor-routable",
  templateUrl: "./contributor.routable.html",
  styleUrls: ["./contributor.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContributorRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  private readonly _NUMBER_YEAR_TO_DISPLAY_IN_CHART: number = 10;
  private _YEAR_TO_DISPLAY: number[];
  private _YEAR_KEY: keyof PublicationStatistic = "publicationYear";

  private _cnIndividu: string;
  contributor: PublicContributorDto;

  darkModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.darkMode);
  isLoadingChartObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  private _facets: Facet[];
  listPublicationStatisticsObs: Observable<PublicationStatistic[]> = MemoizedUtil.select(this._store, SharedPublicContributorState, state => state.list);
  isLoadingPublicationStatisticsObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedPublicContributorState);
  queryParametersPublicationStatisticsObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, SharedPublicContributorState, state => state.queryParameters);

  publicationNumbers: number;
  totalNumberOfViews: number;
  totalNumberOfDownloads: number;
  supervisedWork: number;

  columns: DataTableColumns<PublicationStatistic>[] = [];
  diffusionChart: ChartModel;
  diffusionChartByYear: ChartModel;
  openAccessChart: ChartModel;

  searchContributorsPublicationsLink: Navigate;
  searchDirectorLink: Navigate;

  get systemIndexFieldAlias(): SystemIndexFieldAlias {
    return MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current?.systemIndexFieldAlias);
  }

  get environment(): typeof environment {
    return environment;
  }

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _route: ActivatedRoute,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _scrollService: ScrollService,
              private readonly _translateService: TranslateService,
              private readonly _metaService: MetaService,
              private readonly _notificationService: NotificationService,
              private readonly _homeService: HomeService,
              private readonly _loggingService: LoggingService,
  ) {
    super();
    this._computeYearToDisplayOnChartDiffusionByYear();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new SharedPublicContributorAction.Clean());
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._scrollService.scrollToTop();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._translateService.onLangChange.pipe(
        filter(() => isNotNullNorUndefined(this.contributor)),
        tap(() => {
          this._refreshMeta();
          this._retrieveChartData();
          this._changeDetector.detectChanges();
        }),
      ));
    }

    this.subscribe(this._route.paramMap.pipe(
      map(paramMap => paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam)),
      distinctUntilChanged(),
      filter(cnIndividu => isNotNullNorUndefinedNorWhiteString(cnIndividu)),
      SsrUtil.isServer ? take(1) : pipe(),
      tap(identifier => {
        this._cnIndividu = undefined;
        this.publicationNumbers = 0;
        this.totalNumberOfDownloads = 0;
        this.totalNumberOfViews = 0;

        this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
          new SharedPublicContributorAction.GetContributor(identifier, this._getIdentifierType(identifier)),
          SharedPublicContributorAction.GetContributorSuccess,
          result => {
            this._cnIndividu = result.contributor.cnIndividu;
            this._rewriteUrl(this._cnIndividu);

            this.contributor = {
              ...result.contributor,
              cnIndividu: this._cnIndividu,
            };
            this._refreshMeta();
            this.searchContributorsPublicationsLink = HomeHelper.getUnigeContributorsPublicationsSearchPageLink(this._store, this._cnIndividu);
            this.searchDirectorLink = HomeHelper.getUnigeDirectorPublicationsSearchPageLink(this._store, this._cnIndividu);
            this._retrieveChartData();
            const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
              field: this._YEAR_KEY as string,
              order: OrderEnum.descending,
            });

            this._dispatchActionDirectorPublication(collection => {
              this.supervisedWork = collection._page.totalItems;
              this._changeDetector.detectChanges();
            });

            this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
              new SharedPublicContributorAction.GetPublicationsByCnIndividu(this._cnIndividu, queryParameters),
              SharedPublicContributorAction.GetPublicationsByCnIndividuSuccess,
              action => {
                if (isNullOrUndefined(action?.list?._data)) {
                  return;
                }
                this.publicationNumbers = action.list._data.length;
                this.totalNumberOfDownloads = action.list._data.map(item => item.downloadNumber).reduce((acc, v) => acc + v);
                this.totalNumberOfViews = action.list._data.map(item => item.viewNumber).reduce((acc, v) => acc + v);
                this._changeDetector.detectChanges();
              }));
            this._changeDetector.detectChanges();
          },
          SharedPublicContributorAction.GetContributorFail,
          result => {
            this._notificationService.showError(LabelTranslateEnum.noContributorIsKnownWithIdentifierX, {identifier: identifier});
            this._store.dispatch(new Navigate([RoutesEnum.index]));
          },
        ));
      }),
    ));
    this._defineColumns();
  }

  private _getIdentifierType(identifier: string): ContributorIdentifierTypeEnum {
    return AouRegexUtil.isOrcid.test(identifier) ? ContributorIdentifierTypeEnum.orcid : ContributorIdentifierTypeEnum.cnIndividu;
  }

  private _refreshMeta(): void {
    this._metaService.setMetaFromInfo(metaInfoContributor, this.contributor);
  }

  private _retrieveChartData(): void {
    this.diffusionChart = undefined;
    this.diffusionChartByYear = undefined;
    this.openAccessChart = undefined;
    this.supervisedWork = undefined;
    this._facets = undefined;

    this.subscribe(this._computeFullChartAccessLevelByYear().pipe(
      tap(() => {
        this._dispatchActionSearch(collection => {
          this._facets = collection._facets;
          this._computeChartAccessLebel(this._facets);
          this._computeChartOpenAccess(this._facets);
          this._changeDetector.detectChanges();
        }, {}, isNotNullNorUndefined(this.systemIndexFieldAlias) ? [this.systemIndexFieldAlias.principalFileAccessLevel, this.systemIndexFieldAlias.openAccess] : []);
      }),
    ));

  }

  private _rewriteUrl(cnIndividu: string): void {
    if (SsrUtil.window?.history.replaceState && isNotNullNorUndefined(cnIndividu)) {
      SsrUtil.window?.history.replaceState({}, null, `${environment.baseHref}${RoutesEnum.contributor}/${cnIndividu}`);
    }
  }

  private _defineColumns(): void {
    this.columns = [
      {
        field: "title",
        header: LabelTranslateEnum.title,
        type: DataTableFieldTypeEnum.innerHtml,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        breakWord: true,
      },
      {
        field: "containerTitle",
        header: LabelTranslateEnum.publishedIn,
        type: DataTableFieldTypeEnum.innerHtml,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
        breakWord: true,
      },
      {
        field: "accessLevel",
        header: LabelTranslateEnum.accessLevel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.principalFileAccessLevel),
        width: "50px",
      },
      {
        field: "openAccess",
        header: LabelTranslateEnum.openAccess,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.openAccess),
        width: "50px",
      },
      {
        field: this._YEAR_KEY,
        header: LabelTranslateEnum.year,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.descending,
        isSortable: true,
        isFilterable: false,
        width: "50px",
      },
      {
        field: "viewNumber",
        header: LabelTranslateEnum.viewNumber,
        type: DataTableFieldTypeEnum.number,
        alignment: "center",
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
        width: "75px",
      },
      {
        field: "downloadNumber",
        header: LabelTranslateEnum.downloadNumber,
        type: DataTableFieldTypeEnum.number,
        alignment: "center",
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
        width: "75px",
      },
    ];
  }

  onQueryParametersEvent($event: QueryParameters): void {
    this._store.dispatch(new SharedPublicContributorAction.ChangeQueryParameters(this._cnIndividu, $event));
    this._changeDetector.detectChanges();
  }

  showDetail(publication: PublicationStatistic): void {
    this._store.dispatch(new Navigate([RoutesEnum.homeDetail, publication.archiveId]));
  }

  private _computeYearToDisplayOnChartDiffusionByYear(): void {
    const currentYear = new Date().getFullYear();
    let year = currentYear;
    const listYearToDisplay: number[] = [];
    for (let i = this._NUMBER_YEAR_TO_DISPLAY_IN_CHART; i > 0; i--) {
      listYearToDisplay.push(year);
      year--;
    }
    this._YEAR_TO_DISPLAY = listYearToDisplay;
  }

  private _computeChartAccessLebel(facets: Facet[]): void {
    this.diffusionChart = undefined;
    const diffusionFacets = facets.find(f => f.name === this.systemIndexFieldAlias?.principalFileAccessLevel);
    if (isNullOrUndefined(diffusionFacets)) {
      this._loggingService.logError(`Missing system facets '${this.systemIndexFieldAlias?.principalFileAccessLevel}' in search result to generate DiffusionChart`);
      return;
    }
    this.diffusionChart = {
      chartItem: [],
      keyValue: Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate,
      isGrouped: false,
      title: LabelTranslateEnum.breakdownByDiffusionLevel,
      displayValueOnChart: true,
      formatValueOnChartInPercent: true,
    };
    this._sortFacetValues(diffusionFacets, Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate).forEach(facetValue => {
      this.diffusionChart.chartItem.push({
        facetValue: facetValue,
      });
    });
  }

  private _sortFacetValues(facet: Facet, enumTranslate: KeyValue[]): FacetValue[] {
    const desiredOrder = enumTranslate.map(f => f.key);
    return [...facet.values].sort((a, b) => this._sortByArray(desiredOrder, a.value, b.value));
  }

  private _sortByArray(desiredOrder: string[], a: string, b: string): number {
    const indexOfA = desiredOrder.indexOf(a);
    const indexOfB = desiredOrder.indexOf(b);
    return indexOfA - indexOfB;
  }

  private _computeChartOpenAccess(facets: Facet[]): void {
    this.openAccessChart = undefined;
    const openAccessFacet = facets.find(f => f.name === this.systemIndexFieldAlias?.openAccess);
    if (isNullOrUndefined(openAccessFacet)) {
      this._loggingService.logError(`Missing system facets '${this.systemIndexFieldAlias?.openAccess}' in search result to generate OpenAccessChart`);
      return;
    }
    this.openAccessChart = {
      groupedChartItem: {},
      keyValue: Enums.Facet.FacetValue.OpenAccessValueTranslate,
      isGrouped: true,
      title: LabelTranslateEnum.distributionAccordingToUnigeOAPolicy,
      yLabelToTranslate: LabelTranslateEnum.publicationNumbers,
      displayValueOnChart: true,
      formatValueOnChartInPercent: true,
    };
    MappingObjectUtil.set(this.openAccessChart.groupedChartItem, "",
      this._sortFacetValues(openAccessFacet, Enums.Facet.FacetValue.OpenAccessValueTranslate).map(v => ({facetValue: v})));
  }

  private _computeFullChartAccessLevelByYear(): Observable<boolean> {
    this.diffusionChartByYear = undefined;

    const listActionWrapper: ActionSubActionCompletionsWrapper[] = [];

    this._YEAR_TO_DISPLAY.forEach(year => {
      listActionWrapper.push({
        action: this._getActionSearch({
          ["2_year"]: [`${year}-${year}`],
        }, this.systemIndexFieldAlias?.principalFileAccessLevel ? [this.systemIndexFieldAlias.principalFileAccessLevel] : []),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(HomeAction.SearchSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(HomeAction.SearchFail)),
        ],
      });
    });

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, listActionWrapper).pipe(
      map(result => {
        if (!result.success) {
          return false;
        }
        const listFacets = result.listActionSuccess.map(r => (r as HomeAction.SearchSuccess)?.collection?._facets?.find(f => f.name === this.systemIndexFieldAlias?.principalFileAccessLevel));
        this.diffusionChartByYear = {
          groupedChartItem: {},
          isGrouped: true,
          keyValue: Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate,
          title: LabelTranslateEnum.levelOfDiffusionByYear,
          titleParams: {numberYear: this._NUMBER_YEAR_TO_DISPLAY_IN_CHART},
        };
        listFacets.forEach((facet, index) => {
          const year = this._YEAR_TO_DISPLAY[index];
          if (isNullOrUndefined(facet)) {
            return;
          }
          this._computeChartAccessLevelByYear(year, facet);
        });
        if (MappingObjectUtil.size(this.diffusionChartByYear.groupedChartItem) === 0) {
          this._loggingService.logError(`Missing system facets '${this.systemIndexFieldAlias?.principalFileAccessLevel}' in search result to generate DiffusionChartByYear`);
          this.diffusionChartByYear = undefined;
          return false;
        }
        this._changeDetector.detectChanges();
        return true;
      }),
    );
  }

  private _computeChartAccessLevelByYear(year: number, facet: Facet): void {
    const list: ChartItemModel[] = this._sortFacetValues(facet, Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate).map(f => ({
      facetValue: f,
    }));
    MappingObjectUtil.set(this.diffusionChartByYear.groupedChartItem, year + "", list);
  }

  private _dispatchActionSearch(callback: (collection: CollectionTyped<PublishedDeposit>) => void, extraAdvancedSearch: any = {}, withFacets: string[] = undefined): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      this._getActionSearch(extraAdvancedSearch, withFacets),
      HomeAction.SearchSuccess,
      result => callback(result.collection),
    ));
  }

  private _getActionSearch(extraAdvancedSearch: any = {}, withFacets: string[] = undefined): HomeAction.Search {
    const unigeContributorCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.unigeContributorCriteria);
    const unigeDirectorCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.unigeDirectorCriteria);
    const basicSearch = {
      [`0_${unigeContributorCriteria.alias}`]: [this._cnIndividu],
      [`1_${unigeDirectorCriteria.alias}!`]: [this._cnIndividu],
    };
    return new HomeAction.Search(true, {
      search: SOLIDIFY_CONSTANTS.STRING_EMPTY,
      searchWithFulltext: false,
      withRestrictedAccessMasters: true,
      facetsSelected: {} as any,
      advancedSearch: {
        ...basicSearch,
        ...extraAdvancedSearch,
      },
      withFacets: withFacets,
    }, undefined, SearchContextEnum.EXECUTE_ONLY, false);
  }

  private _dispatchActionDirectorPublication(callback: (collection: CollectionTyped<PublishedDeposit>) => void, extraAdvancedSearch: any = {}, withFacets: string[] = undefined): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      this._getActionDirectorPublication(extraAdvancedSearch, withFacets),
      HomeAction.SearchSuccess,
      result => callback(result.collection),
    ));
  }

  private _getActionDirectorPublication(extraAdvancedSearch: any = {}, withFacets: string[] = undefined): HomeAction.Search {
    const unigeDirectorCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.unigeDirectorCriteria);
    const basicSearch = {
      [`0_${unigeDirectorCriteria.alias}`]: [this._cnIndividu],
    };
    return new HomeAction.Search(false, {
      search: SOLIDIFY_CONSTANTS.STRING_EMPTY,
      searchWithFulltext: false,
      withRestrictedAccessMasters: true,
      facetsSelected: {} as any,
      advancedSearch: {
        ...basicSearch,
        ...extraAdvancedSearch,
      },
      withFacets: withFacets,
    }, new QueryParameters(environment.minimalPageSizeForPageInfos), SearchContextEnum.EXECUTE_ONLY, false);
  }

  contactContributor(contributor: PublicContributorDto): void {
    if (isNullOrUndefined(contributor)) {
      return;
    }
    this._homeService.contactContributors([contributor]);
  }
}
