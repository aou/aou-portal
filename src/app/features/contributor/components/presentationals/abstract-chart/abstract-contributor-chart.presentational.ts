/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - abstract-contributor-chart.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Input,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  ChartConfiguration,
  ChartData,
  ChartDataset,
  ChartType,
  LegendItem,
} from "chart.js";
import {LegendOptions} from "chart.js/dist/types";
import {LabelOptions} from "chartjs-plugin-datalabels/types/options";
import {
  ColorHexaEnum,
  FacetValue,
  isNonEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  StatusModel,
} from "solidify-frontend";

@Directive()
export abstract class AbstractContributorChartPresentational<TChartType extends ChartType> extends SharedAbstractPresentational {
  private readonly _TRANSPARENCY: string = "75";

  abstract get chartOptions(): ChartConfiguration<TChartType>["options"];

  abstract chartType: ChartType;

  display: boolean = true;

  get defaultLegend(): Partial<LegendOptions<TChartType | any>> {
    return {
      position: "bottom",
      align: "center",
      labels: {
        padding: 15,
        usePointStyle: true,
        filter: (item: LegendItem, data: ChartData) => {
          let labels = data.labels as string[];
          if (this.chart.isGrouped) {
            labels = data.datasets.filter(d => isNonEmptyArray(d.data)).map(d => d.label);
          }
          return labels.some((label: string) => label.indexOf(item.text) > -1);
        },
      } as Partial<LegendOptions<TChartType | any>> | any,
    };
  };

  get defaultAxisXTitle(): any {
    return {
      display: isNotNullNorUndefinedNorWhiteString(this.chart?.xLabelToTranslate),
      text: isNotNullNorUndefinedNorWhiteString(this.chart?.xLabelToTranslate) ? this._translateService?.instant(this.chart?.xLabelToTranslate) : undefined,
      font: {
        size: 15,
      },
    };
  }

  get defaultAxisYTitle(): any {
    return {
      display: isNotNullNorUndefinedNorWhiteString(this.chart?.yLabelToTranslate),
      text: isNotNullNorUndefinedNorWhiteString(this.chart?.yLabelToTranslate) ? this._translateService?.instant(this.chart?.yLabelToTranslate) : undefined,
      font: {
        size: 15,
      },
    };
  }

  get defaultDatalabels(): Partial<LabelOptions> {
    if (this.chart.formatValueOnChartInPercent) {
      return {
        formatter: (value, ctx) => {
          let listValue = [];
          if (this.chart.isGrouped) {
            listValue = ctx.chart.data.datasets.map(d => d.data);
          } else {
            listValue = ctx.chart.data.datasets[0].data;
          }
          const sum = listValue.reduce((partialSum, a) => partialSum + +a, 0);
          const percentage = (value * 100 / sum).toFixed(0) + "%";
          return percentage;
        },
        color: this.textColor,
      } as LabelOptions;
    }
    return {};
  }

  get textColor(): string {
    return this.darkMode ? "#fff" : "#323232";
  }

  chartDatasets: ChartDataset<TChartType | any> = [];
  chartLegend: boolean = true;

  get chartPlugins(): any[] {
    return [];
  }

  chartLabels: string[] = [];

  private _chart: ChartModel;

  @Input()
  set chart(value: ChartModel) {
    this._chart = value;
    this.chartLabels = [];
    this.chartDatasets = [];

    if (value.isGrouped) {
      this.chartLabels = MappingObjectUtil.keys(value.groupedChartItem);

      const datasetsMap = {} as Map<string, number[]>; // key enum, list value where index = year
      value.keyValue.forEach(s => {
        // Init data for all years
        const dataForAllYears = Array(MappingObjectUtil.size(this.chartLabels.length)).fill(0);
        MappingObjectUtil.set(datasetsMap, s.key, dataForAllYears);
      });

      // Generate pivot format
      let i = 0;
      MappingObjectUtil.forEach(value.groupedChartItem, (listChartItem, year) => {
        listChartItem.forEach(item => {
          const key = item.facetValue.value;
          const list = MappingObjectUtil.get(datasetsMap, key);
          list[i] = item.facetValue.count;
          MappingObjectUtil.set(datasetsMap, key, list);
        });
        i++;
      });

      // Generate final chart.js object
      let index = 0;
      MappingObjectUtil.forEach(datasetsMap, (listChartItem, key) => {
        const keyValue = value.keyValue?.find(f => f.key === key);
        const color = isNullOrUndefined(keyValue?.backgroundColorHexa) ? ColorHexaEnum.mediumLightGrey : keyValue.backgroundColorHexa;
        this.chartDatasets[index] = {
          data: listChartItem,
          label: this._translateService.instant(isNullOrUndefined(keyValue) ? LabelTranslateEnum.undefinedLabel : keyValue.value),
          borderColor: color,
          backgroundColor: color + this._TRANSPARENCY,
        };
        index++;
      });
    } else {
      this.chartDatasets[0] = {
        data: [],
        backgroundColor: [],
        borderColor: [],
      };
      value.chartItem.forEach((item, index) => {
        const keyValue = value.keyValue?.find(f => f.key === item.facetValue.value);
        this.chartLabels.push(this._translateService.instant(isNullOrUndefined(keyValue) ? LabelTranslateEnum.undefinedLabel : keyValue.value));
        this.chartDatasets[0].data.push(item.facetValue.count);
        const color = isNullOrUndefined(keyValue?.backgroundColorHexa) ? ColorHexaEnum.mediumLightGrey : keyValue.backgroundColorHexa;
        this.chartDatasets[0].borderColor.push(color);
        this.chartDatasets[0].backgroundColor.push(color + this._TRANSPARENCY);
      });
    }
  }

  get chart(): ChartModel {
    return this._chart;
  }

  private _darkMode: boolean;

  @Input()
  set darkMode(value: boolean) {
    this._darkMode = value;
    this._refresh();
  }

  get darkMode(): boolean {
    return this._darkMode;
  }

  constructor(protected readonly _translateService: TranslateService,
              protected readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _refresh(): void {
    this.display = false;
    this._changeDetector.detectChanges();
    this.display = true;
  }
}

export interface ChartModel {
  chartItem?: ChartItemModel[];
  groupedChartItem?: MappingObject<string, ChartItemModel[]>;
  // label?: string[];
  isGrouped?: boolean;
  keyValue: StatusModel[];
  title: string;
  titleParams?: any;
  xLabelToTranslate?: string;
  yLabelToTranslate?: string;
  displayValueOnChart?: boolean;
  formatValueOnChartInPercent?: boolean;
}

export interface ChartItemModel {
  facetValue?: FacetValue;
}
