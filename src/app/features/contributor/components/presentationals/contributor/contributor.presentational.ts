/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - contributor.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {environment} from "@environments/environment";
import {HomeHelper} from "@home/helpers/home.helper";
import {SearchHelper} from "@home/helpers/search.helper";
import {PublicContributorDto} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  MappingObject,
  ObservableUtil,
  OrcidService,
} from "solidify-frontend";

@Component({
  selector: "aou-contributor",
  templateUrl: "./contributor.presentational.html",
  styleUrls: ["./contributor.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContributorPresentational extends SharedAbstractRoutable {
  avatarUrl: string;
  generateBibliographyLink: Navigate;
  searchContributorsPublicationsLink: Navigate;

  private _contributor: PublicContributorDto;

  @Input()
  set contributor(value: PublicContributorDto) {
    this._contributor = value;
    this.avatarUrl = null;
    if (value.hasAvatar) {
      this.avatarUrl = `${ApiEnum.accessContributor}/${this._contributor.cnIndividu}/${ApiActionNameEnum.DOWNLOAD_AVATAR}`;
    }
    const queryParam = {
      [SearchHelper.CONTRIBUTOR_QUERY_PARAM]: this._contributor.cnIndividu,
    } as MappingObject<string, string>;
    this.generateBibliographyLink = new Navigate([RoutesEnum.homeBibliography], queryParam);
    this.searchContributorsPublicationsLink = HomeHelper.getContributorLink(this._store, false, this.contributor.cnIndividu, this.contributor.firstName, this.contributor.lastName);
  }

  get contributor(): PublicContributorDto {
    return this._contributor;
  }

  private readonly _contactBS: BehaviorSubject<PublicContributorDto | undefined> = new BehaviorSubject<PublicContributorDto | undefined>(undefined);
  @Output("contactChange")
  readonly contactObs: Observable<PublicContributorDto | undefined> = ObservableUtil.asObservable(this._contactBS);

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get environment(): typeof environment {
    return environment;
  }

  constructor(private readonly _orcidService: OrcidService,
              private readonly _store: Store) {
    super();
  }

  goToOrcid(orcid: string): void {
    this._orcidService.openOrcidPage(orcid);
  }

  contact(): void {
    this._contactBS.next(this._contributor);
  }
}
