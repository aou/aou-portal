/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - contributor.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {ContributorBarChartPresentational} from "@app/features/contributor/components/presentationals/contributor-bar-chart/contributor-bar-chart.presentational";
import {ContributorPieChartPresentational} from "@app/features/contributor/components/presentationals/contributor-pie-chart/contributor-pie-chart.presentational";
import {ContributorVerticalBarChartPresentational} from "@app/features/contributor/components/presentationals/contributor-vertical-bar-chart/contributor-vertical-bar-chart.presentational";
import {ContributorPresentational} from "@app/features/contributor/components/presentationals/contributor/contributor.presentational";
import {ContributorRoutable} from "@app/features/contributor/components/routables/contributor/contributor.routable";
import {ContributorRoutingModule} from "@app/features/contributor/contributor-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {HomeModule} from "@home/home.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {
  BaseChartDirective,
  provideCharts,
  withDefaultRegisterables,
} from "ng2-charts";

const routables = [
  ContributorRoutable,
];
const dialogs = [];
const containers = [];
const presentationals = [
  ContributorPresentational,
  ContributorBarChartPresentational,
  ContributorPieChartPresentational,
  ContributorVerticalBarChartPresentational,
];
const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...presentationals,
    ...dialogs,
  ],
  imports: [
    SharedModule,
    HomeModule,
    ContributorRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([]),
    BaseChartDirective,
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
    // provideCharts({registerables: [PieController, ArcElement]}),
    provideCharts(withDefaultRegisterables()),
  ],
})
export class ContributorModule {
}
