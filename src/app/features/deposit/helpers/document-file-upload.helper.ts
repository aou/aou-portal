/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - document-file-upload.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {FileUploadStatusEnum} from "@app/features/deposit/enums/file-upload-status.enum";
import {
  DepositDocumentFileState,
  DepositDocumentFileStateModel,
} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {AouFileUploadWrapper} from "@deposit/models/aou-file-upload-wrapper.model";
import {AouUploadFileStatus} from "@deposit/models/aou-upload-file-status.model";
import {Enums} from "@enums";
import {DocumentFile} from "@models";
import {Store} from "@ngxs/store";
import {
  Guid,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  SolidifyStateContext,
  UploadEventModel,
} from "solidify-frontend";

export class DocumentFileUploadHelper {
  private static readonly _LIST_PENDING_STATUS: Enums.DocumentFile.StatusEnum[] = [
    Enums.DocumentFile.StatusEnum.RECEIVED,
    Enums.DocumentFile.StatusEnum.TO_PROCESS,
    Enums.DocumentFile.StatusEnum.PROCESSED,
  ];

  static addToUploadStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, fileUploadWrapper: AouFileUploadWrapper, isArchive: boolean): AouUploadFileStatus {
    const uploadFileStatus = this.createNewUploadFileStatus(fileUploadWrapper, isArchive);
    ctx.patchState({
      uploadStatus: [
        ...ctx.getState().uploadStatus,
        uploadFileStatus,
      ],
    });
    return uploadFileStatus;
  }

  private static createNewUploadFileStatus(fileUploadWrapper: AouFileUploadWrapper, isArchive: boolean): AouUploadFileStatus {
    return {
      guid: Guid.MakeNew(),
      fileUploadWrapper: fileUploadWrapper,
      status: FileUploadStatusEnum.started,
      progressPercentage: 0,
      isArchive: isArchive,
    } as AouUploadFileStatus;
  }

  static updateInProgressUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, uploadFileStatus: AouUploadFileStatus, event: UploadEventModel): void {
    uploadFileStatus.status = FileUploadStatusEnum.inProgress;
    uploadFileStatus.progressPercentage = this.getUploadProgress(event);
    this.updateUploadFileStatus(ctx, uploadFileStatus);
  }

  static getUploadProgress(event: UploadEventModel): number {
    return Math.round(100 * event.loaded / event.total);
  }

  static updateCompletedUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, uploadFileStatus: AouUploadFileStatus, depositDocumentFile: DocumentFile): void {
    uploadFileStatus.progressPercentage = 100;
    uploadFileStatus.status = FileUploadStatusEnum.completed;
    uploadFileStatus.result = depositDocumentFile;
    this.updateUploadFileStatus(ctx, uploadFileStatus);
    setTimeout(() => {
      this.removeToUploadStatus(ctx, uploadFileStatus);
    }, 5000);
  }

  static updateErrorUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, uploadFileStatus: AouUploadFileStatus, messageToTranslate: string): void {
    uploadFileStatus.status = FileUploadStatusEnum.inError;
    uploadFileStatus.errorMessageToTranslate = messageToTranslate;
    this.updateUploadFileStatus(ctx, uploadFileStatus);
  }

  static updateCancelUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, uploadFileStatus: AouUploadFileStatus): void {
    uploadFileStatus.status = FileUploadStatusEnum.canceled;
    this.updateUploadFileStatus(ctx, uploadFileStatus);
  }

  static updateUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, uploadFileStatus: AouUploadFileStatus): void {
    const uploadStatus = [...ctx.getState().uploadStatus];
    const index = this.getIndexCurrentUploadFileStatus(ctx, uploadFileStatus.guid);
    if (index !== -1 && index < uploadStatus.length) {
      uploadStatus[index] = uploadFileStatus;
      ctx.patchState({
        uploadStatus,
      });
    }
  }

  static removeToUploadStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, uploadFileStatus: AouUploadFileStatus): void {
    const uploadStatus = [...ctx.getState().uploadStatus];
    const index = this.getIndexCurrentUploadFileStatus(ctx, uploadFileStatus.guid);
    if (index !== -1 && index < uploadStatus.length) {
      uploadStatus.splice(index, 1);
      ctx.patchState({
        uploadStatus,
      });
    }
  }

  static getCurrentUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, guid: Guid): AouUploadFileStatus | null {
    const index = this.getIndexCurrentUploadFileStatus(ctx, guid);
    if (index !== -1) {
      return Object.assign({}, ctx.getState().uploadStatus[index]);
    }
    return null;
  }

  private static getIndexCurrentUploadFileStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, guid: Guid): number {
    const uploadStatus = ctx.getState().uploadStatus;
    return uploadStatus.findIndex(u => u.guid === guid);
  }

  static numberFiles(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    let pendingCounter = 0;
    MappingObjectUtil.forEach(listCurrentStatus, (value, key: Enums.DocumentFile.StatusEnum) => {
      pendingCounter += value;
    });
    return pendingCounter;
  }

  static numberFilesInReady(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    return MappingObjectUtil.get(listCurrentStatus, Enums.DocumentFile.StatusEnum.READY) ?? 0;
  }

  static numberFilesInError(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    return MappingObjectUtil.get(listCurrentStatus, Enums.DocumentFile.StatusEnum.IN_ERROR) ?? 0;
  }

  static numberFilesConfirmed(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    return MappingObjectUtil.get(listCurrentStatus, Enums.DocumentFile.StatusEnum.CONFIRMED) ?? 0;
  }

  static numberFilesToBeConfirmed(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    return MappingObjectUtil.get(listCurrentStatus, Enums.DocumentFile.StatusEnum.TO_BE_CONFIRMED) ?? 0;
  }

  static numberFilesImpossibleToDownload(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    return MappingObjectUtil.get(listCurrentStatus, Enums.DocumentFile.StatusEnum.IMPOSSIBLE_TO_DOWNLOAD) ?? 0;
  }

  static hasFilesDuplicate(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): boolean {
    return MappingObjectUtil.has(listCurrentStatus, Enums.DocumentFile.StatusEnum.DUPLICATE);
  }

  static numberFilesPending(listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>): number {
    let pendingCounter = 0;
    MappingObjectUtil.forEach(listCurrentStatus, (value, key: Enums.DocumentFile.StatusEnum) => {
      if (this._LIST_PENDING_STATUS.indexOf(key) !== -1) {
        pendingCounter += value;
      }
    });
    return pendingCounter;
  }

  static listFilesPending(store: Store): DocumentFile[] {
    return this.listFilesByStatus(store, ...this._LIST_PENDING_STATUS);
  }

  static listFilesImpossibleToDownload(store: Store): DocumentFile[] {
    return this.listFilesByStatus(store, Enums.DocumentFile.StatusEnum.IMPOSSIBLE_TO_DOWNLOAD);
  }

  static listFilesByStatus(store: Store, ...status: Enums.DocumentFile.StatusEnum[]): DocumentFile[] {
    const listDocumentFile: DocumentFile[] = MemoizedUtil.listSnapshot(store, DepositDocumentFileState);
    return listDocumentFile?.filter(d => status.includes(d.status)) ?? [];
  }
}
