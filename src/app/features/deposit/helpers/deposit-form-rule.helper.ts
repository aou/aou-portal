/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-rule.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {FormGroup} from "@angular/forms";
import {
  DepositFormModel,
  FormComponentFormDefinitionDepositFormClassification,
  FormComponentFormDefinitionDepositFormCollection,
  FormComponentFormDefinitionDepositFormContainer,
  FormComponentFormDefinitionDepositFormContributor,
  FormComponentFormDefinitionDepositFormCorrection,
  FormComponentFormDefinitionDepositFormDataset,
  FormComponentFormDefinitionDepositFormDates,
  FormComponentFormDefinitionDepositFormFunding,
  FormComponentFormDefinitionDepositFormLanguage,
  FormComponentFormDefinitionDepositFormLink,
  FormComponentFormDefinitionDepositFormPage,
  FormComponentFormDefinitionDepositFormPublicationIdentifiers,
  FormComponentFormDefinitionDepositFormPublisher,
  FormComponentFormDefinitionDepositFormTextLanguage,
  FormComponentFormDefinitionFirstStepType,
  FormComponentFormDefinitionFourthStepDescription,
  FormComponentFormDefinitionMain,
  FormComponentFormDefinitionThirdStepContributors,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DateHelper} from "@deposit/helpers/date.helper";
import {Enums} from "@enums";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {PublicationHelper} from "@shared/helpers/publication.helper";
import {
  isArray,
  isEmptyArray,
  isEmptyString,
  isFalse,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isObject,
  isString,
  isTrue,
  KeyValue,
  MappingObjectUtil,
  ObjectUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const dot = SOLIDIFY_CONSTANTS.DOT;

export class DepositFormRuleHelper {
  static _fdMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  private static _fdFirstStepType: FormComponentFormDefinitionFirstStepType = new FormComponentFormDefinitionFirstStepType();
  private static _fdThirdStepContributors: FormComponentFormDefinitionThirdStepContributors = new FormComponentFormDefinitionThirdStepContributors();
  private static _fdFourthStepDescription: FormComponentFormDefinitionFourthStepDescription = new FormComponentFormDefinitionFourthStepDescription();
  private static _fdTextLanguage: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();
  private static _fdLanguage: FormComponentFormDefinitionDepositFormLanguage = new FormComponentFormDefinitionDepositFormLanguage();
  private static _fdPublisher: FormComponentFormDefinitionDepositFormPublisher = new FormComponentFormDefinitionDepositFormPublisher();
  private static _fdDataset: FormComponentFormDefinitionDepositFormDataset = new FormComponentFormDefinitionDepositFormDataset();
  private static _fdContainer: FormComponentFormDefinitionDepositFormContainer = new FormComponentFormDefinitionDepositFormContainer();
  private static _fdLink: FormComponentFormDefinitionDepositFormLink = new FormComponentFormDefinitionDepositFormLink();
  private static _fdFunding: FormComponentFormDefinitionDepositFormFunding = new FormComponentFormDefinitionDepositFormFunding();
  private static _fdCollection: FormComponentFormDefinitionDepositFormCollection = new FormComponentFormDefinitionDepositFormCollection();
  private static _fdClassification: FormComponentFormDefinitionDepositFormClassification = new FormComponentFormDefinitionDepositFormClassification();
  private static _fdPage: FormComponentFormDefinitionDepositFormPage = new FormComponentFormDefinitionDepositFormPage();
  private static _fdDate: FormComponentFormDefinitionDepositFormDates = new FormComponentFormDefinitionDepositFormDates();
  private static _fdPublicationIdentifiers: FormComponentFormDefinitionDepositFormPublicationIdentifiers = new FormComponentFormDefinitionDepositFormPublicationIdentifiers();
  private static _fdContributor: FormComponentFormDefinitionDepositFormContributor = new FormComponentFormDefinitionDepositFormContributor();
  private static _fdCorrection: FormComponentFormDefinitionDepositFormCorrection = new FormComponentFormDefinitionDepositFormCorrection();

  static pathType: string = DepositFormRuleHelper._fdMain.type + dot + DepositFormRuleHelper._fdFirstStepType.type;
  static pathContributorsContributors: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.contributors;
  static pathContributorsContributorMembersFrontend: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.contributorMembersFrontend;
  static pathContributorsCollaborationMembersFrontend: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.collaborationMembersFrontend;
  static pathContributorsIsCollaboration: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.isCollaboration;
  static pathContributorsIsBeforeUnige: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.isBeforeUnige;
  static pathStructure: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.academicStructures;
  static pathGroup: string = DepositFormRuleHelper._fdMain.contributors + dot + DepositFormRuleHelper._fdThirdStepContributors.groups;
  static pathSubType: string = DepositFormRuleHelper._fdMain.type + dot + DepositFormRuleHelper._fdFirstStepType.subtype;
  static pathSubsubtype: string = DepositFormRuleHelper._fdMain.type + dot + DepositFormRuleHelper._fdFirstStepType.subsubtype;
  static pathLanguages: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.languages;
  static pathLanguagesLanguage: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.languages + dot + DepositFormRuleHelper._fdLanguage.language;
  static pathTitle: string = DepositFormRuleHelper._fdMain.type + dot + DepositFormRuleHelper._fdFirstStepType.title;
  static pathTitleText: string = DepositFormRuleHelper._fdMain.type + dot + DepositFormRuleHelper._fdFirstStepType.title + dot + DepositFormRuleHelper._fdTextLanguage.text;
  static pathTitleLanguage: string = DepositFormRuleHelper._fdMain.type + dot + DepositFormRuleHelper._fdFirstStepType.title + dot + DepositFormRuleHelper._fdTextLanguage.lang;
  static pathOriginalTitle: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.originalTitle;
  static pathOriginalTitleText: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.originalTitle + dot + DepositFormRuleHelper._fdTextLanguage.text;
  static pathOriginalTitleLanguage: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.originalTitle + dot + DepositFormRuleHelper._fdTextLanguage.lang;
  static pathContainerTitle: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.title;
  static pathIdentifierIssn: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.issn;
  static pathPublisher: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.publisher;
  static pathPublisherPlacePublication: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.publisher + dot + DepositFormRuleHelper._fdPublisher.place;
  static pathPublisherPublishingHouse: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.publisher + dot + DepositFormRuleHelper._fdPublisher.name;
  static pathDates: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.dates;
  static pathDatesFirstOnline: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.dates + dot + DepositFormRuleHelper._fdDate.first_online;
  static pathDatesPublication: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.dates + dot + DepositFormRuleHelper._fdDate.publication;
  static pathDatesDefense: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.dates + dot + DepositFormRuleHelper._fdDate.defense;
  static pathDatesImprimatur: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.dates + dot + DepositFormRuleHelper._fdDate.imprimatur;
  static pathContainer: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container;
  static pathContainerVolume: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.volume;
  static pathContainerIssue: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.issue;
  static pathContainerSpecialIssue: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.specialIssue;
  static pathContainerEditor: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.editor;
  static pathContainerConferenceSubtitle: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.conferenceSubtitle;
  static pathContainerConferencePlace: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.conferencePlace;
  static pathContainerConferenceDate: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.container + dot + DepositFormRuleHelper._fdContainer.conferenceDate;
  static pathPages: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.pages;
  static pathPagesPaging: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.pages + dot + DepositFormRuleHelper._fdPage.paging;
  static pathPagesNumberPagesArticleNumber: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.pages + dot + DepositFormRuleHelper._fdPage.other;
  static pathEdition: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.edition;
  static pathIdentifierIsbn: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.isbn;
  static pathIdentifierDoi: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.doi;
  static pathIdentifier: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers;
  static pathIdentifierPmid: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.pmid;
  static pathIdentifierPmcid: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.pmcid;
  static pathIdentifierArXiv: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.arxiv;
  static pathIdentifierDblp: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.dblp;
  static pathIdentifierRepEc: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.repec;
  static pathIdentifierMmsid: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.mmsid;
  static pathIdentifierUrn: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.urn;
  static pathCommercialUrl: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.publisherVersionUrl;
  static pathAbstract: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.abstracts;
  static pathAbstractText: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.abstracts + dot + DepositFormRuleHelper._fdTextLanguage.text;
  static pathAbstractLanguage: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.abstracts + dot + DepositFormRuleHelper._fdTextLanguage.lang;
  static pathKeywords: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.keywords;
  static pathNote: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.note;
  static pathJel: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.jel;
  static pathJelCode: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.jel + dot + DepositFormRuleHelper._fdClassification.code;
  static pathJelItem: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.jel + dot + DepositFormRuleHelper._fdClassification.item;
  static pathCollection: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.collections;
  static pathCollectionName: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.collections + dot + DepositFormRuleHelper._fdCollection.name;
  static pathCollectionNumber: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.collections + dot + DepositFormRuleHelper._fdCollection.number;
  static pathIdentifierLocalNumber: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.identifiers + dot + DepositFormRuleHelper._fdPublicationIdentifiers.localNumber;
  static pathDiscipline: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.discipline;
  static pathMandatedBy: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.mandator;
  static pathFunder: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings;
  static pathFunderFunder: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings + dot + DepositFormRuleHelper._fdFunding.funder;
  static pathFunderProjectName: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings + dot + DepositFormRuleHelper._fdFunding.name;
  static pathFunderProjectCode: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings + dot + DepositFormRuleHelper._fdFunding.code;
  static pathFunderProjectAcronym: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings + dot + DepositFormRuleHelper._fdFunding.acronym;
  static pathFunderProjectJurisdiction: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings + dot + DepositFormRuleHelper._fdFunding.jurisdiction;
  static pathFunderProjectProgram: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.fundings + dot + DepositFormRuleHelper._fdFunding.program;
  static pathDatasets: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.datasets;
  static pathDatasetsUrl: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.datasets + dot + DepositFormRuleHelper._fdDataset.url;
  static pathLinks: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.links;
  static pathLinksTarget: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.links + dot + DepositFormRuleHelper._fdLink.target;
  static pathLinksDescription: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.links + dot + DepositFormRuleHelper._fdLink.description;
  static pathLinksDescriptionText: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.links + dot + DepositFormRuleHelper._fdLink.description + dot + DepositFormRuleHelper._fdTextLanguage.text;
  static pathLinksDescriptionLanguage: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.links + dot + DepositFormRuleHelper._fdLink.description + dot + DepositFormRuleHelper._fdTextLanguage.lang;
  static pathLinksType: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.links + dot + DepositFormRuleHelper._fdLink.type;
  static pathAouInternalCollection: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.aouCollection;
  static pathCorrections: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.corrections;
  static pathCorrectionsNote: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.corrections + dot + DepositFormRuleHelper._fdCorrection.note;
  static pathCorrectionsDoi: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.corrections + dot + DepositFormRuleHelper._fdCorrection.doi;
  static pathCorrectionsPmid: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.corrections + dot + DepositFormRuleHelper._fdCorrection.pmid;
  static pathDoctorEmail: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.doctorEmail;
  static pathDoctorAddress: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.doctorAddress;
  static pathAward: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.award;
  static pathClassifications: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.classifications;
  static pathClassificationsCode: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.classifications + dot + DepositFormRuleHelper._fdClassification.code;
  static pathClassificationsItem: string = DepositFormRuleHelper._fdMain.description + dot + DepositFormRuleHelper._fdFourthStepDescription.classifications + dot + DepositFormRuleHelper._fdClassification.item;

  static shouldDisplayField(form: FormGroup, fieldPath: string): boolean {
    const subType = form?.get(this._fdMain.type + "." + this._fdFirstStepType.subtype)?.value;
    return this._shouldDisplayFieldDependingOfSubType(subType, fieldPath);
  }

  static isAuthorBeforeUnige(form: FormGroup): boolean {
    return isTrue(form.get(DepositFormRuleHelper.pathContributorsIsBeforeUnige)?.value);
  }

  private static _isThesisBefore2010(form: FormGroup): boolean {
    return MappingObjectUtil.values(form.get(DepositFormRuleHelper.pathDates).value)
      .filter((date: string) => this.isDateBefore2010(date)).length > 0;
  }

  static isDateBefore2010(date: string | undefined): boolean {
    if (isNullOrUndefinedOrWhiteString(date)) {
      return false;
    }
    return Number(DateHelper.getYearFromDate(date)) <= 2010;
  }

  static isThesisBefore2010OrAuthorBeforeUnige(form: FormGroup): boolean {
    const isThesisBefore2010: boolean = this._isThesisBefore2010(form);
    const authorBeforeUnige = this.isAuthorBeforeUnige(form);
    return isTrue(isThesisBefore2010) || isTrue(authorBeforeUnige);
  }

  static isThesisOrDoctoralThesis(form: FormGroup): boolean {
    const subType = form?.get(this.pathSubType)?.value;
    return subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS;

  }

  private static _shouldDisplayFieldDependingOfSubType(subType: string, fieldPath: string): boolean {
    switch (subType) {
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.SCIENTIFIC_ARTICLE:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_ARTICLE:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          ...(subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE ? [] : [this.pathSubsubtype]),
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          this.pathContainer,
          this.pathContainerVolume,
          this.pathContainerIssue,
          this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          this.pathIdentifierArXiv,
          this.pathIdentifierDblp,
          this.pathIdentifierRepEc,
          this.pathIdentifierMmsid,
          this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          this.pathCorrections,
          this.pathCorrectionsNote,
          this.pathCorrectionsDoi,
          this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          this.pathContainer,
          this.pathContainerVolume,
          this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          // this.pathIdentifierPmid,
          // this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          // this.pathDatasets,
          // this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.COLLECTIVE_WORK:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          //this.pathPagesNumberPagesArticleNumber,
          this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          // this.pathDatasets,
          // this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          // this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          this.pathContainerEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          this.pathCorrections,
          this.pathCorrectionsNote,
          this.pathCorrectionsDoi,
          this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONFERENCE_PROCEEDINGS:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          // this.pathDatasets,
          // this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          // this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          this.pathContainerEditor,
          this.pathContainerConferenceSubtitle,
          this.pathContainerConferencePlace,
          this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRESENTATION_SPEECH:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          // this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          // this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          this.pathContainerConferenceSubtitle,
          this.pathContainerConferencePlace,
          this.pathContainerConferenceDate,
          // this.pathPages,
          // this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.POSTER:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          // this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          // this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          this.pathContainerConferenceSubtitle,
          this.pathContainerConferencePlace,
          this.pathContainerConferenceDate,
          // this.pathPages,
          // this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          this.pathDoctorEmail,
          this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          // this.pathDatesFirstOnline,
          // this.pathDatesPublication,
          this.pathDatesDefense,
          this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          // this.pathIdentifierDoi,
          this.pathIdentifier,
          // this.pathIdentifierPmid,
          // this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          // this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRIVATE_DOCTOR_THESIS:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          // this.pathDatesFirstOnline,
          // this.pathDatesPublication,
          this.pathDatesDefense,
          this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          // this.pathIdentifierDoi,
          // this.pathIdentifier,
          // this.pathIdentifierPmid,
          // this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          // this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          // this.pathFunder,
          // this.pathFunderFunder,
          // this.pathFunderProjectName,
          // this.pathFunderProjectCode,
          // this.pathFunderProjectAcronym,
          // this.pathFunderProjectJurisdiction,
          // this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_OF_ADVANCED_STUDIES:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_DEGREE:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          // this.pathDatesFirstOnline,
          // this.pathDatesPublication,
          this.pathDatesDefense,
          this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          // this.pathIdentifierDoi,
          // this.pathIdentifier,
          // this.pathIdentifierPmid,
          // this.pathIdentifierPmcid,
          // this.pathIdentifierArXiv,
          // this.pathIdentifierDblp,
          // this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          // this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          this.pathDiscipline,
          // this.pathMandatedBy,
          // this.pathFunder,
          // this.pathFunderFunder,
          // this.pathFunderProjectName,
          // this.pathFunderProjectCode,
          // this.pathFunderProjectAcronym,
          // this.pathFunderProjectJurisdiction,
          // this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.RESEARCH_REPORT:
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.TECHNICAL_REPORT:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          this.pathIdentifierArXiv,
          this.pathIdentifierDblp,
          this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.WORKING_PAPER:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          this.pathIdentifierArXiv,
          this.pathIdentifierDblp,
          this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          this.pathJel,
          this.pathJelCode,
          this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
          this.pathClassifications,
          this.pathClassificationsCode,
          this.pathClassificationsItem,
        ].indexOf(fieldPath) !== -1;
      case Enums.Deposit.DepositFriendlyNameSubTypeEnum.PREPRINT:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          // this.pathContainerTitle,
          // this.pathIdentifierIssn,
          // this.pathPublisher,
          // this.pathPublisherPlacePublication,
          // this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          // this.pathDatesDefense,
          // this.pathDatesImprimatur,
          // this.pathContainer,
          // this.pathContainerVolume,
          // this.pathContainerIssue,
          // this.pathContainerSpecialIssue,
          // this.pathContainerConferenceEditor,
          // this.pathContainerConferenceSubtitle,
          // this.pathContainerConferencePlace,
          // this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          // this.pathPagesNumberPagesArticleNumber,
          // this.pathEdition,
          // this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          this.pathIdentifierArXiv,
          this.pathIdentifierDblp,
          this.pathIdentifierRepEc,
          // this.pathIdentifierMmsid,
          // this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          // this.pathCollection,
          // this.pathCollectionName,
          // this.pathCollectionNumber,
          // this.pathIdentifierLocalNumber,
          // this.pathDiscipline,
          // this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          // this.pathCorrections,
          // this.pathCorrectionsNote,
          // this.pathCorrectionsDoi,
          // this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          // this.pathAward,
        ].indexOf(fieldPath) !== -1;
      default:
        return [
          this.pathContributorsContributors,
          this.pathContributorsIsCollaboration,
          this.pathContributorsIsBeforeUnige,
          this.pathStructure,
          this.pathGroup,
          this.pathType,
          this.pathSubType,
          this.pathSubsubtype,
          this.pathLanguages,
          this.pathLanguagesLanguage,
          // this.pathDoctorEmail,
          // this.pathDoctorAddress,
          this.pathTitle,
          this.pathTitleText,
          // this.pathTitleLanguage,
          this.pathOriginalTitle,
          this.pathOriginalTitleText,
          this.pathOriginalTitleLanguage,
          this.pathContainerTitle,
          this.pathIdentifierIssn,
          this.pathPublisher,
          this.pathPublisherPlacePublication,
          this.pathPublisherPublishingHouse,
          this.pathDates,
          this.pathDatesFirstOnline,
          this.pathDatesPublication,
          this.pathDatesDefense,
          this.pathDatesImprimatur,
          this.pathContainer,
          this.pathContainerVolume,
          this.pathContainerIssue,
          this.pathContainerSpecialIssue,
          this.pathContainerEditor,
          this.pathContainerConferenceSubtitle,
          this.pathContainerConferencePlace,
          this.pathContainerConferenceDate,
          this.pathPages,
          this.pathPagesPaging,
          this.pathPagesNumberPagesArticleNumber,
          this.pathEdition,
          this.pathIdentifierIsbn,
          this.pathIdentifierDoi,
          this.pathIdentifier,
          this.pathIdentifierPmid,
          this.pathIdentifierPmcid,
          this.pathIdentifierArXiv,
          this.pathIdentifierDblp,
          this.pathIdentifierRepEc,
          this.pathIdentifierMmsid,
          this.pathIdentifierUrn,
          this.pathCommercialUrl,
          this.pathAbstract,
          this.pathAbstractText,
          this.pathAbstractLanguage,
          this.pathKeywords,
          this.pathNote,
          // this.pathJel,
          // this.pathJelCode,
          // this.pathJelItem,
          this.pathCollection,
          this.pathCollectionName,
          this.pathCollectionNumber,
          this.pathIdentifierLocalNumber,
          this.pathDiscipline,
          this.pathMandatedBy,
          this.pathFunder,
          this.pathFunderFunder,
          this.pathFunderProjectName,
          this.pathFunderProjectCode,
          this.pathFunderProjectAcronym,
          this.pathFunderProjectJurisdiction,
          this.pathFunderProjectProgram,
          this.pathDatasets,
          this.pathDatasetsUrl,
          // this.pathLinks,
          // this.pathLinksTarget,
          // this.pathLinksDescription,
          // this.pathLinksDescriptionText,
          // this.pathLinksDescriptionLanguage,
          // this.pathLinksType,
          this.pathCorrections,
          this.pathCorrectionsNote,
          this.pathCorrectionsDoi,
          this.pathCorrectionsPmid,
          // this.pathAouInternalCollection,
          this.pathAward,
          this.pathClassifications,
          this.pathClassificationsCode,
          this.pathClassificationsItem,
        ].indexOf(fieldPath) !== -1;
    }
  }

  private static _shouldNotCleanField(fieldPath: string): boolean {
    return [
      this.pathType,
      this.pathStructure,
      this.pathDates,
      this.pathContributorsContributors,
      this.pathContributorsIsCollaboration,
      this.pathContributorsIsBeforeUnige,
    ].indexOf(fieldPath) !== -1;
  }

  static applyRuleToCleanUndesiredDataDependingOfType(depositFormModelRaw: DepositFormModel, cleanedAttributes: KeyValue[]): DepositFormModel {
    const subType = depositFormModelRaw?.type?.subtype;
    if (isNullOrUndefined(subType)) {
      return depositFormModelRaw;
    }
    const depositFormModelCleaned = {} as DepositFormModel;
    this._recursivelyCleanObject(depositFormModelCleaned, subType, depositFormModelRaw, undefined, undefined, cleanedAttributes);

    return depositFormModelCleaned;
  }

  private static _recursivelyPopulateCleanedAttribute(object: any, path: string | undefined, cleanedAttributes: KeyValue[]): void {
    if (isNullOrUndefined(object)) {
      return;
    } else if (isArray(object)) {
      if (isEmptyArray(object)) {
        return;
      }
      object.forEach(item => {
        this._recursivelyPopulateCleanedAttribute(item, path, cleanedAttributes);
      });
      return;
    } else if (isObject(object)) {
      if (MappingObjectUtil.size(object as any) === 0) {
        return;
      }
      MappingObjectUtil.forEach(object as any, (v, k) => {
        this._recursivelyPopulateCleanedAttribute(v, path + SOLIDIFY_CONSTANTS.DOT + k, cleanedAttributes);
      });
      return;
    } else if (isEmptyString(object)) {
      return;
    }
    cleanedAttributes.push({
      key: path,
      value: object,
    });
  }

  private static _recursivelyCleanObject(newObject: any, subType: string, object: any, key: string | undefined, path: string | undefined, cleanedAttributes: KeyValue[]): void {
    if (isNotNullNorUndefined(path) && isNonEmptyString(key)) {
      path = path + SOLIDIFY_CONSTANTS.DOT + key;
      if (this._recursivelyCheckIfNeedToStop(newObject, subType, object, key, path, cleanedAttributes)) {
        return;
      }
    } else if (isNullOrUndefined(key)) {
      // do nothing
    } else {
      path = key;
    }
    if (isNullOrUndefined(object)) {
      newObject[key] = object;
    } else if (isArray(object)) {
      this._recursivelyCleanObjectArray(newObject, subType, object, key, path, cleanedAttributes);
    } else if (isObject(object)) {
      this._recursivelyCleanObjectChild(newObject, subType, object, key, path, cleanedAttributes);
    } else {
      newObject[key] = object;
    }
  }

  private static _recursivelyCheckIfNeedToStop(newObject: any, subType: string, object: any, key: string | undefined, path: string | undefined, cleanedAttributes: KeyValue[]): boolean {
    const shouldBypassClean = this._shouldNotCleanField(path);
    if (shouldBypassClean) {
      if (isNullOrUndefined(object)) {
        newObject[key] = object;
      } else if (isArray(object)) {
        newObject[key] = [...object];
      } else if (isObject(object)) {
        newObject[key] = ObjectUtil.clone(object);
      } else {
        newObject[key] = object;
      }
      return true;
    }
    const shouldDisplay = this._shouldDisplayFieldDependingOfSubType(subType, path);
    if (isFalse(shouldDisplay)) {
      this._recursivelyPopulateCleanedAttribute(object, path, cleanedAttributes);
      return true;
    }
    return false;
  }

  private static _recursivelyCleanObjectArray(newObject: any, subType: string, object: any, key: string | undefined, path: string | undefined, cleanedAttributes: KeyValue[]): void {
    const array = object;
    newObject[key] = [];
    array.forEach((v, i) => {
      if (isString(v) || isArray(v) || isNullOrUndefined(v)) {
        newObject[key][i] = v;
      } else {
        newObject[key][i] = {};
        this._recursivelyCleanObject(newObject[key][i], subType, v, undefined, path, cleanedAttributes);
      }
    });
  }

  private static _recursivelyCleanObjectChild(newObject: any, subType: string, object: any, key: string | undefined, path: string | undefined, cleanedAttributes: KeyValue[]): void {
    let obj = newObject;
    if (isNotNullNorUndefined(key)) {
      newObject[key] = {};
      obj = newObject[key];
    }
    MappingObjectUtil.forEach(object as any, (v, k, mappingObject) => {
      this._recursivelyCleanObject(obj, subType, v, k, path, cleanedAttributes);
    });
  }

  static getLabel(form: FormGroup, fieldPath: string): string {
    const subType = form?.get(this._fdMain.type + "." + this._fdFirstStepType.subtype)?.value;
    return this.getLabelDependingOfSubType(subType, fieldPath);
  }

  static getPlaceholder(form: FormGroup, fieldPath: string): string {
    const subType = form?.get(this._fdMain.type + "." + this._fdFirstStepType.subtype)?.value;
    return this.getPlaceholderDependingOfSubType(subType, fieldPath);
  }

  static getLabelDependingOfSubType(subType: string, fieldPath: string): string {
    switch (fieldPath) {
      case this.pathSubsubtype:
        return LabelTranslateEnum.documentSubtype;
      case this.pathSubType:
        return LabelTranslateEnum.documentType;
      case this.pathLanguages:
        return LabelTranslateEnum.languages;
      case this.pathLanguagesLanguage:
        return LabelTranslateEnum.publicationLanguage;
      case this.pathTitle:
        return LabelTranslateEnum.publicationTitle;
      case this.pathTitleText:
        return LabelTranslateEnum.publicationTitle;
      case this.pathTitleLanguage:
        return LabelTranslateEnum.languageOfTitle;
      case this.pathOriginalTitle:
        return LabelTranslateEnum.otherTitle;
      case this.pathOriginalTitleText:
        return LabelTranslateEnum.otherTitle;
      case this.pathOriginalTitleLanguage:
        return LabelTranslateEnum.languageOfOriginalTitle;
      case this.pathStructure:
        return LabelTranslateEnum.structures;
      case this.pathGroup:
        return LabelTranslateEnum.groups;
      case this.pathIdentifier:
        return LabelTranslateEnum.identifiers;
      case this.pathIdentifierDoi:
        return LabelTranslateEnum.doi;
      case this.pathIdentifierPmid:
        return LabelTranslateEnum.pmid;
      case this.pathIdentifierPmcid:
        return LabelTranslateEnum.pmcid;
      case this.pathIdentifierIsbn:
        return LabelTranslateEnum.isbn;
      case this.pathIdentifierArXiv:
        return LabelTranslateEnum.arxiv;
      case this.pathIdentifierDblp:
        return LabelTranslateEnum.dblp;
      case this.pathIdentifierRepEc:
        return LabelTranslateEnum.repec;
      case this.pathIdentifierLocalNumber:
        if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.RESEARCH_REPORT ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.TECHNICAL_REPORT ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.WORKING_PAPER) {
          return LabelTranslateEnum.reportNumber;
        }
        if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS) {
          return LabelTranslateEnum.thesisNumber;
        }
        return LabelTranslateEnum.localNumber;
      case this.pathIdentifierIssn:
        return LabelTranslateEnum.issn;
      case this.pathIdentifierMmsid:
        return LabelTranslateEnum.mmsId;
      case this.pathIdentifierUrn:
        return LabelTranslateEnum.urn;
      case this.pathCommercialUrl:
        return LabelTranslateEnum.commercialUrl;
      case this.pathContainer:
        return LabelTranslateEnum.container;
      case this.pathContainerTitle:
        if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.SCIENTIFIC_ARTICLE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_ARTICLE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE) {
          return LabelTranslateEnum.titleOfTheMagazine;
        } else if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER) {
          return LabelTranslateEnum.titleOfTheBook;
        } else if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONFERENCE_PROCEEDINGS ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS) {
          return LabelTranslateEnum.titleOfTheProceedings;
        } else if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.POSTER ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRESENTATION_SPEECH) {
          return LabelTranslateEnum.conferenceTitle;
        }
        return LabelTranslateEnum.containerTitle;
      case this.pathContainerConferenceSubtitle:
        return LabelTranslateEnum.conferenceSubtitle;
      case this.pathContainerEditor:
        return LabelTranslateEnum.editor;
      case this.pathContainerConferencePlace:
        return LabelTranslateEnum.conferencePlace;
      case this.pathContainerConferenceDate:
        return LabelTranslateEnum.conferenceDate;
      case this.pathContainerVolume:
        return LabelTranslateEnum.volume;
      case this.pathContainerIssue:
        return LabelTranslateEnum.issue;
      case this.pathContainerSpecialIssue:
        return LabelTranslateEnum.specialIssue;
      case this.pathMandatedBy:
        return LabelTranslateEnum.mandator;
      case this.pathEdition:
        return LabelTranslateEnum.edition;
      case this.pathCollection:
        return LabelTranslateEnum.collections;
      case this.pathCollectionName:
        return LabelTranslateEnum.collection;
      case this.pathCollectionNumber:
        return LabelTranslateEnum.numberInTheCollection;
      case this.pathPublisher:
        return LabelTranslateEnum.publisher;
      case this.pathPublisherPlacePublication:
        return LabelTranslateEnum.publisherPlacePublication;
      case this.pathPublisherPublishingHouse:
        return LabelTranslateEnum.publisherName;
      case this.pathDates:
        return LabelTranslateEnum.dates;
      case this.pathDatesFirstOnline:
        return PublicationHelper.getDateLabel(Enums.Deposit.DateTypesEnum.FIRST_ONLINE, subType);
      case this.pathDatesPublication:
        return PublicationHelper.getDateLabel(Enums.Deposit.DateTypesEnum.PUBLICATION, subType);
      case this.pathDatesDefense:
        return PublicationHelper.getDateLabel(Enums.Deposit.DateTypesEnum.DEFENSE, subType);
      case this.pathDatesImprimatur:
        return PublicationHelper.getDateLabel(Enums.Deposit.DateTypesEnum.IMPRIMATUR, subType);
      case this.pathDiscipline:
        return LabelTranslateEnum.discipline;
      case this.pathPages:
        return LabelTranslateEnum.pages;
      case this.pathPagesPaging:
        if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRIVATE_DOCTOR_THESIS ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_OF_ADVANCED_STUDIES ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_DEGREE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.RESEARCH_REPORT ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.TECHNICAL_REPORT ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.WORKING_PAPER ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PREPRINT ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONFERENCE_PROCEEDINGS ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.COLLECTIVE_WORK) {
          return LabelTranslateEnum.numberOfPages;
        } else {
          return LabelTranslateEnum.paging;
        }
      case this.pathPagesNumberPagesArticleNumber:
        if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.SCIENTIFIC_ARTICLE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_ARTICLE ||
          subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE) {
          return LabelTranslateEnum.articleNumber;
        }
        return LabelTranslateEnum.other;
      case this.pathAbstract:
        return LabelTranslateEnum.abstracts;
      case this.pathAbstractText:
        return LabelTranslateEnum.abstract;
      case this.pathAbstractLanguage:
        return LabelTranslateEnum.languageOfAbstract;
      case this.pathKeywords:
        return LabelTranslateEnum.keywords;
      case this.pathNote:
        return LabelTranslateEnum.note;
      case this.pathJel:
        return LabelTranslateEnum.jel;
      case this.pathJelCode:
        return LabelTranslateEnum.code;
      case this.pathJelItem:
        return LabelTranslateEnum.item;
      case this.pathAward:
        return LabelTranslateEnum.award;
      case this.pathFunder:
        return LabelTranslateEnum.funding;
      case this.pathFunderFunder:
        return LabelTranslateEnum.funder;
      case this.pathFunderProjectName:
        return LabelTranslateEnum.projectName;
      case this.pathFunderProjectCode:
        return LabelTranslateEnum.projectGrantNumber;
      case this.pathFunderProjectAcronym:
        return LabelTranslateEnum.projectAcronym;
      case this.pathFunderProjectJurisdiction:
        return LabelTranslateEnum.jurisdiction;
      case this.pathFunderProjectProgram:
        return LabelTranslateEnum.program;
      case this.pathDatasets:
        return LabelTranslateEnum.datasets;
      case this.pathDatasetsUrl:
        return LabelTranslateEnum.datasetUrl;
      case this.pathLinks:
        return LabelTranslateEnum.otherLinks;
      case this.pathLinksTarget:
        return LabelTranslateEnum.target;
      case this.pathLinksDescription:
        return LabelTranslateEnum.description;
      case this.pathLinksDescriptionText:
        return LabelTranslateEnum.description;
      case this.pathLinksDescriptionLanguage:
        return LabelTranslateEnum.language;
      case this.pathLinksType:
        return LabelTranslateEnum.type;
      case this.pathAouInternalCollection:
        return LabelTranslateEnum.collectionUnige;
      case this.pathCorrections:
        return LabelTranslateEnum.corrections;
      case this.pathCorrectionsNote:
        return LabelTranslateEnum.explanation;
      case this.pathCorrectionsDoi:
        return LabelTranslateEnum.correctionOfTheDoi;
      case this.pathCorrectionsPmid:
        return LabelTranslateEnum.correctionOfThePmid;
      case this.pathDoctorEmail:
        return LabelTranslateEnum.doctorEmail;
      case this.pathDoctorAddress:
        return LabelTranslateEnum.doctorAddress;
      case this.pathClassifications:
        return LabelTranslateEnum.classifications;
      case this.pathClassificationsCode:
        return LabelTranslateEnum.code;
      case this.pathClassificationsItem:
        return LabelTranslateEnum.item;
      default:
        return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
  }

  static getPlaceholderDependingOfSubType(subType: string, fieldPath: string): string {
    switch (fieldPath) {
      case this.pathIdentifierLocalNumber:
        if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS) {
          return LabelTranslateEnum.thesisNumberPlaceholder;
        }
        return SOLIDIFY_CONSTANTS.STRING_EMPTY;
      default:
        return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
  }

  static ignoreContainerTitleIssnSearch(form: FormGroup): boolean {
    const subType = form?.get(DepositFormRuleHelper.pathSubType)?.value;
    return DepositFormRuleHelper.ignoreContainerTitleIssnSearchOfSubType(subType);
  }

  static ignoreContainerTitleIssnSearchOfSubType(subType: string): boolean {
    return subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.POSTER
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRESENTATION_SPEECH
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA;
  }
}
