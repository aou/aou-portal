/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MatDialog} from "@angular/material/dialog";
import {DepositValidationStructureDialog} from "@app/features/deposit/components/dialogs/deposit-validation-structure/deposit-validation-structure.dialog";
import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {DepositDocumentFileState} from "@deposit/stores/document-file/deposit-document-file.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DocumentFile,
  Structure,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {Observable} from "rxjs/index";
import {
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  isEmptyArray,
  isFalse,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export class DepositHelper {
  static askValidationStructureForSubmitDialogObs(dialog: MatDialog, callback: (structureId: string) => void): Observable<string> {
    return DialogUtil.open(dialog, DepositValidationStructureDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.dialog.validationStructure.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.dialog.validationStructure.message"),
      colorConfirm: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, structureId => {
      if (isNonEmptyString(structureId)) {
        callback(structureId);
      }
    });
  }

  static confirmSubmitDialogObs(dialog: MatDialog, callback: () => void): Observable<boolean> {
    return DialogUtil.open(dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.submit.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.submit.message"),
      confirmButtonToTranslate: LabelTranslateEnum.confirm,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.primary,
      colorCancel: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      if (isFalse(isConfirmed)) {
        return;
      }
      callback();
    }) as Observable<boolean>;
  }

  static isPublicationHost(subType: string): boolean {
    return subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.SCIENTIFIC_ARTICLE ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_ARTICLE ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRESENTATION_SPEECH ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.POSTER ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA;
  }

  static isDeleteAvailable(isValidationMode: boolean, isRootOrAdmin: boolean, depositStatus: Enums.Deposit.StatusEnum): boolean {
    if (isNullOrUndefined(depositStatus)) {
      return false;
    }
    if (isValidationMode && !isRootOrAdmin) {
      return false;
    }
    return [
      Enums.Deposit.StatusEnum.IN_PROGRESS,
      Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED,
      Enums.Deposit.StatusEnum.IN_ERROR,
    ].includes(depositStatus);
  }

  static isEditAvailable(isValidationMode: boolean, isRootOrAdmin: boolean, depositStatus: Enums.Deposit.StatusEnum): boolean {
    if (isNullOrUndefined(depositStatus)) {
      return false;
    }
    if (isValidationMode) {
      if (isRootOrAdmin && [
        Enums.Deposit.StatusEnum.IN_PROGRESS,
        Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED,
      ].includes(depositStatus)) {
        return true;
      }
      return [
        Enums.Deposit.StatusEnum.IN_VALIDATION,
        Enums.Deposit.StatusEnum.IN_EDITION,
        Enums.Deposit.StatusEnum.UPDATES_VALIDATION,
      ].includes(depositStatus);
    } else {
      return [
        Enums.Deposit.StatusEnum.IN_PROGRESS,
        Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED,
        Enums.Deposit.StatusEnum.IN_ERROR,
        Enums.Deposit.StatusEnum.IN_EDITION,
      ].includes(depositStatus);
    }
  }

  static isReEditAvailable(isValidationMode: boolean, isRootOrAdmin: boolean, depositStatus: Enums.Deposit.StatusEnum): boolean {
    if (isNullOrUndefined(depositStatus)) {
      return false;
    }
    if (isValidationMode) {
      return [
        Enums.Deposit.StatusEnum.COMPLETED,
      ].includes(depositStatus);
    } else {
      return [
        Enums.Deposit.StatusEnum.COMPLETED,
        Enums.Deposit.StatusEnum.IN_ERROR,
      ].includes(depositStatus);
    }
  }

  static isThesisAndScienceFacultyAndMissingFiles(store: Store, deposit: Deposit, listStructureId: string[], isBeforeUnige: boolean): boolean {
    if (deposit.subtype.code !== Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS) {
      return false;
    }

    const structures: Structure[] = MemoizedUtil.listSnapshot(store, SharedStructureState);
    if (isNullOrUndefined(structures) || isEmptyArray(structures)) {
      return false;
    }

    const isStructureChildOfSciencePresent = listStructureId.some(structureId => {
      const foundStructure: Structure = structures.find(s => s.resId === structureId);
      return isNotNullNorUndefined(foundStructure) && DepositHelper._isStructureFromScienceOrChild(foundStructure);
    });

    return !DepositHelper.isNbFilesCompatibleWithThesis(store, isStructureChildOfSciencePresent, isFalse(isBeforeUnige));
  }

  static isNbFilesCompatibleWithThesis(store: Store, isPublicationNeeded: boolean = false, isImprimaturNeeded: boolean = false): boolean {
    const listDocumentFiles: DocumentFile[] = MemoizedUtil.listSnapshot(store, DepositDocumentFileState);
    if (isNullOrUndefined(listDocumentFiles)) {
      return true;
    }
    let numberFileRequired = 1;
    if (isImprimaturNeeded) {
      const imprimaturFile = listDocumentFiles.find(document => document.documentFileType.value === Enums.Deposit.FileTypeStringEnum.IMPRIMATUR);
      if (isNullOrUndefined(imprimaturFile)) {
        return false;
      }
      numberFileRequired++;
    }
    if (isPublicationNeeded) {
      const publicationFile = listDocumentFiles.find(document => document.documentFileType.value === Enums.Deposit.FileTypeStringEnum.MODE_PUBLICATION);
      if (isNullOrUndefined(publicationFile)) {
        return false;
      }
      numberFileRequired++;
    }
    return listDocumentFiles.length >= numberFileRequired;
  }

  private static _isStructureFromScienceOrChild(structure: Structure): boolean {
    if (isNotNullNorUndefined(structure)) {
      if (structure.cStruct === Enums.StructureCodeEnum.SCIENCE_FACULTY) {
        return true;
      } else if (isNotNullNorUndefined(structure.parentStructure)) {
        if (structure.parentStructure.cStruct === Enums.StructureCodeEnum.SCIENCE_FACULTY) {
          return true;
        } else {
          return this._isStructureFromScienceOrChild(structure.parentStructure);
        }
      }
    }
    return false;
  }

  static getNavigateToFieldPath(fieldPath: string, isValidationMode: boolean, depositId: string): Navigate | undefined {
    const listTerms = fieldPath.split(SOLIDIFY_CONSTANTS.DOT);
    const firstTerm = listTerms[0];
    let step: Enums.Deposit.StepEnum = null;
    if (firstTerm === DepositFormRuleHelper._fdMain.description || firstTerm === DepositFormRuleHelper._fdMain.type) {
      step = Enums.Deposit.StepEnum.DESCRIPTION;
    } else if (firstTerm === DepositFormRuleHelper._fdMain.files) {
      step = Enums.Deposit.StepEnum.FILES;
    } else if (firstTerm === DepositFormRuleHelper._fdMain.contributors) {
      step = Enums.Deposit.StepEnum.CONTRIBUTORS;
    } else {
      return;
    }
    return new Navigate([(isValidationMode ? RoutesEnum.depositToValidateDetail : RoutesEnum.depositDetail), depositId, DepositRoutesEnum.edit, step], {
      [environment.fieldPathQueryParam]: fieldPath,
    });
  }
}
