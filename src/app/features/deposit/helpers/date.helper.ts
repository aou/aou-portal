/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - date.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "solidify-frontend";

export class DateHelper {

  static getYearFromDate(dateString: string | number): string | null {
    dateString = dateString + "";
    if (dateString.indexOf(SOLIDIFY_CONSTANTS.DOT) === -1) {
      return dateString;
    } else {
      const dateStringSplit = dateString.split(SOLIDIFY_CONSTANTS.DOT);
      if (dateStringSplit.length === 2) {
        // Format mm.yyyy
        return dateStringSplit[1];
      } else {
        // Format dd.mm.yyyy
        return dateStringSplit[2];
      }
    }
  }
}
