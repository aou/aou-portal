/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-definition.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositTableContributorTypeEnum} from "@app/features/deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {Enums} from "@enums";
import {
  BaseFormDefinition,
  PropertyName,
} from "solidify-frontend";

export class FormComponentFormDefinitionMain extends BaseFormDefinition {
  @PropertyName() type: string;
  @PropertyName() contributors: string;
  @PropertyName() files: string;
  @PropertyName() description: string;
  @PropertyName() summary: string;
}

export class FormComponentFormDefinitionFirstStepType extends BaseFormDefinition {
  @PropertyName() title: string;
  @PropertyName() type: string;
  @PropertyName() subtype: string;
  @PropertyName() subsubtype: string;
}

export class FormComponentFormDefinitionThirdStepContributors extends BaseFormDefinition {
  @PropertyName() contributors: string;
  @PropertyName() contributorMembersFrontend: string;
  @PropertyName() collaborationMembersFrontend: string;
  @PropertyName() isCollaboration: string;
  @PropertyName() isBeforeUnige: string;
  @PropertyName() academicStructures: string;
  @PropertyName() groups: string;
}

export class FormComponentFormDefinitionFourthStepDescription extends BaseFormDefinition {
  @PropertyName() languages: string;
  @PropertyName() dates: string;
  @PropertyName() originalTitle: string;
  @PropertyName() pages: string;
  @PropertyName() identifiers: string;
  @PropertyName() publisherVersionUrl: string;
  @PropertyName() abstracts: string;
  @PropertyName() note: string;
  @PropertyName() keywords: string;
  @PropertyName() jel: string;
  @PropertyName() classifications: string;
  @PropertyName() discipline: string;
  @PropertyName() mandator: string;
  @PropertyName() publisher: string;
  @PropertyName() collections: string;
  @PropertyName() edition: string;
  @PropertyName() award: string;
  @PropertyName() datasets: string;
  @PropertyName() container: string;
  @PropertyName() links: string;
  @PropertyName() aouCollection: string;
  @PropertyName() corrections: string;
  @PropertyName() doctorEmail: string;
  @PropertyName() doctorAddress: string;
  @PropertyName() fundings: string;
}

export class FormComponentFormDefinitionRetrieveReferences extends BaseFormDefinition {
  @PropertyName() importMode: string;
  @PropertyName() service: string;
  @PropertyName() identifier: string;
}

export interface DepositDefaultData {
  academicStructures?: DepositFormAcademicStructure[];
  groups?: DepositFormGroup[];
}

export interface DepositFormModel {
  type: DepositFormStepType;
  contributors: DepositFormStepContributors;
  description: DepositFormStepDescription;
}

export interface DepositFormStepType {
  title?: DepositFormTextLanguage;
  type: string; // enum from backend
  subtype: string; // enum from backend
  subsubtype?: string; // enum from backend
}

export interface DepositFormStepContributors {
  contributors: DepositFormAbstractContributor[];
  contributorMembersFrontend: DepositFormContributor[]; // Only on frontend
  collaborationMembersFrontend: DepositFormContributor[]; // Only on frontend
  isCollaboration: boolean;
  academicStructures?: string[];
  groups?: string[];
  isBeforeUnige?: boolean;
}

export interface DepositFormStepDescription {
  languages: DepositFormLanguage[];
  originalTitle?: DepositFormTextLanguage;
  dates: DepositFormDate[];
  pages: DepositFormPage;
  identifiers?: DepositFormPublicationIdentifiers;
  publisherVersionUrl?: string;
  abstracts?: DepositFormText[];
  note?: string;
  keywords?: string[];
  classifications?: DepositFormClassification[];
  discipline?: string;
  mandator?: string;
  publisher?: DepositFormPublisher;
  collections?: DepositFormCollection[];
  edition?: string;
  award?: string;
  datasets?: DepositFormDataset[];
  container?: DepositFormContainer;
  links?: DepositFormLink[];
  aouCollection?: string;
  corrections?: any[];
  doctorEmail?: string;
  doctorAddress?: string;
  fundings: DepositFormFunding[];
}

export class FormComponentFormDefinitionDepositFormAbstractContributor extends BaseFormDefinition {
  @PropertyName() contributor: string;
  @PropertyName() collaboration: string;
}

export interface DepositFormAbstractContributor {
  contributor?: DepositFormContributor;
  collaboration?: DepositFormCollaboration;
}

export class FormComponentFormDefinitionDepositFormCollaboration extends BaseFormDefinition {
  @PropertyName() name: string;
}

export interface DepositFormCollaboration {
  name: string;
}

export class FormComponentFormDefinitionDepositFormContributor extends BaseFormDefinition {
  @PropertyName() guid: string;
  @PropertyName() role: string;
  @PropertyName() firstname: string;
  @PropertyName() lastname: string;
  @PropertyName() orcid: string;
  @PropertyName() email: string;
  @PropertyName() institution: string;
  @PropertyName() cnIndividu: string;
  @PropertyName() otherNames: string;
  @PropertyName() structure: string;
  @PropertyName() type: string;
  @PropertyName() potentiallyUnige: string;
}

export interface DepositFormContributor {
  guid?: string;
  role: Enums.Deposit.RoleContributorEnum;
  firstname: string;
  lastname: string;
  orcid: string;
  email: string;
  institution: string;
  cnIndividu: string;
  otherNames: any;
  structure: string;
  type: DepositTableContributorTypeEnum;
}

export interface DepositFormText {
  text: string;
  lang: string; // "en", "fr"
}

export class FormComponentFormDefinitionDepositFormLanguage extends BaseFormDefinition {
  @PropertyName() language: string;
}

export interface DepositFormLanguage {
  language: string; // "en", "fr"
}

export class FormComponentFormDefinitionDepositFormTextLanguage extends BaseFormDefinition {
  @PropertyName() text: string;
  @PropertyName() lang: string;
}

export interface DepositFormTextLanguage {
  text: string;
  lang: string; // "en", "fr"
}

export class FormComponentFormDefinitionDepositFormDates extends BaseFormDefinition {
  @PropertyName() first_online: string;
  @PropertyName() publication: string;
  @PropertyName() defense: string;
  @PropertyName() imprimatur: string;
}

export interface DepositFormDate {
  date: string;
  type: Enums.Deposit.DateTypesEnum;
}

export interface DepositFormDates {
  first_online?: string;
  publication?: string;
  defense?: string;
  imprimatur?: string;
}

export interface DepositFormOtherName {
  firstname: string;
  lastname: string;
}

export class FormComponentFormDefinitionDepositFormPublicationIdentifiers extends BaseFormDefinition {
  @PropertyName() doi: string;
  @PropertyName() pmid: string;
  @PropertyName() pmcid: string;
  @PropertyName() isbn: string;
  @PropertyName() issn: string;
  @PropertyName() arxiv: string;
  @PropertyName() mmsid: string;
  @PropertyName() repec: string;
  @PropertyName() dblp: string;
  @PropertyName() urn: string;
  @PropertyName() localNumber: string;
}

export interface DepositFormPublicationIdentifiers {
  doi?: string;
  pmid?: string;
  pmcid?: string;
  isbn?: string;
  issn?: string;
  arxiv?: string;
  mmsid?: string;
  repec?: string;
  dblp?: string;
  urn?: string;
  localNumber?: string;
}

export interface DepositFormEmbargo {
  accessLevel: Enums.Deposit.AccessLevelEnum;
  endDate?: Date;
}

export interface DepositFormFile {
  type: Enums.Deposit.FileTypeEnum;
  accessLevel: Enums.Deposit.AccessLevelEnum;
  name: string;
  size?: number;
  mimetype?: string;
  license?: string;
  embargo?: DepositFormEmbargo;
  label?: string;
}

export class FormComponentFormDefinitionDepositFormLink extends BaseFormDefinition {
  @PropertyName() target: string;
  @PropertyName() description: string;
  @PropertyName() type: string;
}

export interface DepositFormLink {
  target: string;
  description?: DepositFormText;
  type: Enums.Deposit.LinkTypesEnum;
}

export class FormComponentFormDefinitionDepositFormCorrection extends BaseFormDefinition {
  @PropertyName() note: string;
  @PropertyName() doi: string;
  @PropertyName() pmid: string;
}

export interface DepositFormCorrection {
  note: string;
  doi?: string;
  pmid?: string;
}

export class FormComponentFormDefinitionDepositFormContainer extends BaseFormDefinition {
  @PropertyName() title: string;
  @PropertyName() identifiers: string;
  @PropertyName() editor: string;
  @PropertyName() conferenceSubtitle: string;
  @PropertyName() conferencePlace: string;
  @PropertyName() volume: string;
  @PropertyName() issue: string;
  @PropertyName() conferenceDate: string;
  @PropertyName() specialIssue: string;
}

export interface DepositFormContainer {
  title: string;
  shortTitle?: string;
  editor?: string;
  conferenceSubtitle?: string;
  conferencePlace?: string;
  volume?: string;
  issue?: string;
  conferenceDate?: string;
  specialIssue?: string;
}

export class FormComponentFormDefinitionDepositFormPage extends BaseFormDefinition {
  @PropertyName() paging: string;
  @PropertyName() other: string;
}

export interface DepositFormPage {
  paging?: string;
  other?: string;
}

export class FormComponentFormDefinitionDepositFormClassification extends BaseFormDefinition {
  @PropertyName() code: string;
  @PropertyName() item: string;
}

export interface DepositFormClassification {
  code: string;
  item: string;
}

export class FormComponentFormDefinitionDepositFormPublisher extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() place: string;
}

export interface DepositFormPublisher {
  name: string;
  place: string;
}

export class FormComponentFormDefinitionDepositFormCollection extends BaseFormDefinition {
  @PropertyName() name: string;
  // eslint-disable-next-line id-blacklist
  @PropertyName() number: string;
}

export interface DepositFormCollection {
  name: string;
  // eslint-disable-next-line id-blacklist
  number: number;
}

export class FormComponentFormDefinitionDepositFormFunding extends BaseFormDefinition {
  @PropertyName() funder: string;
  @PropertyName() name: string;
  @PropertyName() code: string;
  @PropertyName() acronym: string;
  @PropertyName() jurisdiction: string;
  @PropertyName() program: string;
}

export interface DepositFormFunding {
  funder: string;
  name: string;
  code?: number;
  acronym?: string;
  jurisdiction?: string;
  program?: string;
}

export interface DepositFormAcademicStructure {
  name: string;
  code: string;
  resId: string;
}

export interface DepositFormGroup {
  name: string;
  code: string;
  resId: string;
}

export class FormComponentFormDefinitionDepositFormDataset extends BaseFormDefinition {
  @PropertyName() url: string;
}

export interface DepositFormDataset {
  url: string;
}

export class FormComponentFormDate extends BaseFormDefinition {
  @PropertyName() date: string;
}
