/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {DepositCreateEditRoutable} from "@app/features/deposit/components/routables/deposit-create-edit/deposit-create-edit.routable";
import {DepositDetailRoutable} from "@app/features/deposit/components/routables/deposit-detail/deposit-detail.routable";
import {DepositListRoutable} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {DepositMultipleImportRoutable} from "@app/features/deposit/components/routables/deposit-multiple-import/deposit-multiple-import.routable";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {RedirectToDepositTabGuardService} from "@deposit/guards/redirect-to-deposit-tab-guard.service";
import {Enums} from "@enums";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
} from "@shared/enums/routes.enum";
import {DepositDetailGuardService} from "@shared/guards/deposit-detail-guard.service";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CombinedGuardService,
  EmptyRoutable,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const separator: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: EmptyRoutable,
    data: {
      guards: [RedirectToDepositTabGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: DepositRoutesEnum.create,
    component: DepositCreateEditRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
  },
  {
    path: DepositRoutesEnum.importMultiple,
    component: DepositMultipleImportRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.importMultipleDeposits,
    },
  },
  {
    path: DepositRoutesEnum.detail + separator + AppRoutesEnum.paramId,
    component: DepositDetailRoutable,
    data: {
      breadcrumbMemoizedSelector: DepositState.currentTitle,
      guards: [DepositDetailGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: DepositRoutesEnum.edit,
        redirectTo: DepositRoutesEnum.edit + separator + Enums.Deposit.StepEnum[Enums.Deposit.StepEnum.FILES],
        pathMatch: "full",
      },
      {
        path: DepositRoutesEnum.edit + separator + AppRoutesEnum.paramStep,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        component: DepositCreateEditRoutable,
      },
    ],
  },
  {
    path: AppRoutesEnum.paramIdStatus,
    component: DepositListRoutable,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepositRoutingModule {
}
