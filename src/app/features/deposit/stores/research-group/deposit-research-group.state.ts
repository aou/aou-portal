/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-research-group.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositResearchGroupAction,
  depositResearchGroupActionNameSpace,
} from "@app/features/deposit/stores/research-group/deposit-research-group.action";
import {environment} from "@environments/environment";
import {ResearchGroup} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedResearchGroupAction} from "@shared/stores/research-group/shared-research-group.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface DepositResearchGroupStateModel extends ResourceStateModel<ResearchGroup> {
}

@Injectable()
@State<DepositResearchGroupStateModel>({
  name: StateEnum.deposit_researchGroup,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class DepositResearchGroupState extends ResourceState<DepositResearchGroupStateModel, ResearchGroup> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositResearchGroupActionNameSpace,
      notificationResourceUpdateFailTextToTranslate: MARK_AS_TRANSLATABLE("deposit.researchGroup.notification.resource.updateFail"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminResearchGroups;
  }

  @Selector()
  static isLoading(state: DepositResearchGroupStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: DepositResearchGroupStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentName(state: DepositResearchGroupStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: DepositResearchGroupStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: DepositResearchGroupStateModel): boolean {
    return true;
  }

  @Action(DepositResearchGroupAction.UpdateThenValidate)
  updateThenValidate(ctx: SolidifyStateContext<DepositResearchGroupStateModel>, action: DepositResearchGroupAction.UpdateThenValidate): Observable<DepositResearchGroupAction.UpdateSuccess> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(ctx, this._actions$, new DepositResearchGroupAction.Update(action.modelFormControlEvent),
      DepositResearchGroupAction.UpdateSuccess,
      researchGroup => {
        this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(ctx, this._actions$, new DepositResearchGroupAction.Validate(researchGroup.model.resId),
          DepositResearchGroupAction.ValidateSuccess,
          actionValidateSuccess => {
            ctx.dispatch(new DepositResearchGroupAction.UpdateThenValidateSuccess(action, actionValidateSuccess.researchGroup));
          }));
      });
  }

  @Action(DepositResearchGroupAction.UpdateThenValidateSuccess)
  updateThenValidateSuccess(ctx: SolidifyStateContext<DepositResearchGroupStateModel>, action: DepositResearchGroupAction.UpdateThenValidateSuccess): void {
    ctx.dispatch(new SharedResearchGroupAction.ReplaceInList(action.researchGroup));
  }

  @Action(DepositResearchGroupAction.Validate)
  validate(ctx: SolidifyStateContext<DepositResearchGroupStateModel>, action: DepositResearchGroupAction.Validate): Observable<ResearchGroup> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<void, ResearchGroup>(this._urlResource + urlSeparator + action.researchGroupId + urlSeparator + ApiActionNameEnum.VALIDATE)
      .pipe(
        tap(researchGroup => {
          ctx.dispatch(new DepositResearchGroupAction.ValidateSuccess(action, researchGroup));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositResearchGroupAction.ValidateFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositResearchGroupAction.ValidateSuccess)
  validateSuccess(ctx: SolidifyStateContext<DepositResearchGroupStateModel>, action: DepositResearchGroupAction.ValidateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.researchGroup,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.researchGroupValidated);
  }

  @Action(DepositResearchGroupAction.ValidateFail)
  validateFail(ctx: SolidifyStateContext<DepositResearchGroupStateModel>, action: DepositResearchGroupAction.ValidateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToValidateTheResearchGroup);
  }
}
