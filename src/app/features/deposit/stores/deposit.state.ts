/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpStatusCode} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {FormArray} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {DepositListTabEnum} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {DepositRedirectTargetEnum} from "@app/features/deposit/enums/deposit-redirect-target.enum";
import {
  DepositDefaultData,
  FormComponentFormDefinitionMain,
  FormComponentFormDefinitionThirdStepContributors,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositImportResultModel} from "@app/features/deposit/models/deposit-import-result.model";
import {ModelFormControlEventDeposit} from "@app/features/deposit/models/model-form-control-event-deposit.model";
import {DepositCommentValidatorAction} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.action";
import {
  defaultDepositCommentValidatorStateModel,
  defaultDepositCommentValidatorStateQueryParameters,
  DepositCommentValidatorState,
  DepositCommentValidatorStateModel,
} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.state";
import {DepositCommentAction} from "@app/features/deposit/stores/comment/deposit-comment.action";
import {
  defaultDepositCommentStateModel,
  defaultDepositCommentStateQueryParameters,
  DepositCommentState,
  DepositCommentStateModel,
} from "@app/features/deposit/stores/comment/deposit-comment.state";
import {
  DepositAction,
  depositActionNameSpace,
} from "@app/features/deposit/stores/deposit.action";
import {DepositDocumentFileAction} from "@app/features/deposit/stores/document-file/deposit-document-file.action";
import {
  defaultDepositDocumentFileStateQueryParameters,
  defaultDepositDocumentFileValue,
  DepositDocumentFileState,
  DepositDocumentFileStateModel,
} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {DepositPersonResearchGroupState} from "@app/features/deposit/stores/person/research-group/deposit-person-research-group.state";
import {DepositPersonStructureState} from "@app/features/deposit/stores/person/structure/deposit-person-structure.state";
import {DepositResearchGroupState} from "@app/features/deposit/stores/research-group/deposit-research-group.state";
import {
  DepositStatusHistoryState,
  DepositStatusHistoryStateModel,
} from "@app/features/deposit/stores/status-history/deposit-status-history.state";
import {DepositStructureAction} from "@app/features/deposit/stores/structure/deposit-structure.action";
import {DepositStructureState} from "@app/features/deposit/stores/structure/deposit-structure.state";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {
  DepositUnableImportFileDialog,
  DepositUnableImportFileDialogData,
} from "@deposit/components/dialogs/deposit-unable-import-file/deposit-unable-import-file.dialog";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  MetadataDifference,
  PublicationImportDTO,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SharedDepositSubtypeAction} from "@shared/stores/deposit-subtype/shared-deposit-subtype.action";
import {SharedDepositTypeAction} from "@shared/stores/deposit-type/shared-deposit-type.action";
import {SharedLanguageAction} from "@shared/stores/language/shared-language.action";
import {ErrorDtoUtil} from "@shared/utils/error-dto.util";
import {
  MonoTypeOperatorFunction,
  Observable,
  pipe,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  ButtonColorEnum,
  CollectionTyped,
  ConfirmDialog,
  defaultResourceFileStateInitValue,
  defaultStatusHistoryInitValue,
  DialogUtil,
  DownloadService,
  EnumUtil,
  ErrorDtoMessage,
  ErrorHelper,
  isFalse,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceActionHelper,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  Result,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  SolidifyError,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  ValidationErrorDto,
} from "solidify-frontend";

export const defaultDepositStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(50, {
    field: "lastUpdate.when",
    order: OrderEnum.descending,
  });

export interface DepositStateModel extends ResourceFileStateModel<Deposit> {
  currentTab: DepositListTabEnum;
  tabListCounters: MappingObject<DepositListTabEnum, number>;
  [StateEnum.deposit_comment]: DepositCommentStateModel;
  [StateEnum.deposit_commentValidator]: DepositCommentValidatorStateModel;
  defaultData: DepositDefaultData | undefined;
  publicationSubTypeId: string;
  [StateEnum.deposit_documentFile]: DepositDocumentFileStateModel;
  [StateEnum.deposit_statusHistory]: DepositStatusHistoryStateModel;
  alreadyDisplayDialogFileImportedAutomatically: boolean;
  alreadyDisplayDialogContributorsPartiallyImported: boolean;
  bulkImportsResult: MappingObject<string, DepositImportResultModel>;
  errorValidation: ValidationErrorDto[];
  errorDuplicate: ErrorDtoMessage;
  metadataDifferences: MappingObject<string, MappingObject<string, MetadataDifference[]>>;
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[];
}

export const defaultDepositInitValue: () => DepositStateModel = () =>
  ({
    currentTab: undefined,
    defaultData: undefined,
    tabListCounters: {} as MappingObject<DepositListTabEnum, number>,
    [StateEnum.deposit_comment]: {...defaultDepositCommentStateModel()},
    [StateEnum.deposit_commentValidator]: {...defaultDepositCommentValidatorStateModel()},
    publicationSubTypeId: undefined,
    alreadyDisplayDialogFileImportedAutomatically: false,
    alreadyDisplayDialogContributorsPartiallyImported: false,
    [StateEnum.deposit_documentFile]: {...defaultDepositDocumentFileValue()},
    [StateEnum.deposit_statusHistory]: {...defaultStatusHistoryInitValue()},
    bulkImportsResult: {} as MappingObject<string, DepositImportResultModel>,
    errorValidation: undefined,
    errorDuplicate: undefined,
    metadataDifferences: undefined,
    ...defaultResourceFileStateInitValue(),
    queryParameters: defaultDepositStateQueryParameters(),
    listUserRoleEnum: [],
  });

@Injectable()
@State<DepositStateModel>({
  name: StateEnum.deposit,
  defaults: {
    ...defaultDepositInitValue(),
  },
  children: [
    DepositPersonStructureState,
    DepositPersonResearchGroupState,
    DepositCommentState,
    DepositCommentValidatorState,
    DepositDocumentFileState,
    DepositStructureState,
    DepositStatusHistoryState,
    DepositResearchGroupState,
  ],
})
export class DepositState extends ResourceFileState<DepositStateModel, Deposit> {
  private readonly _STATUS_KEY: keyof Deposit = "status";
  static readonly _KEY_SHOW_ALL_DEPOSITS: string = "showAllDeposits";
  static readonly _KEY_SHOW_AS_CONTRIBUTOR: string = "asContributor";
  private readonly _STATUS_LIST_KEY: string = "statusList";
  private readonly _LIST_TAB_WITH_COUNTER: DepositListTabEnum[] = [DepositListTabEnum.in_progress, DepositListTabEnum.in_validation, DepositListTabEnum.feedback_required, DepositListTabEnum.in_error, DepositListTabEnum.in_edition, DepositListTabEnum.updates_validation, DepositListTabEnum.canonical];
  private readonly _KEY_QUERY_PARAM_REASON: string = "reason";
  private readonly _KEY_QUERY_PARAM_STRUCTURE_ID: string = "structureId";
  private readonly _KEY_QUERY_PARAM_LANGUAGE_ID: string = "language_id";
  private readonly _KEY_QUERY_PARAM_RESEARCH_GROUP_ID: string = "researchGroupId";
  private readonly _ERROR_VALIDATION_FIELD_NAME_DOCUMENT_FILES: string = "documentFiles";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _route: ActivatedRoute,
              private readonly _router: Router,
              private readonly _dialog: MatDialog,
              protected readonly _downloadService: DownloadService,
              private readonly _translateService: TranslateService,
              protected readonly _routerExt: RouterExtensionService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: depositActionNameSpace,
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("deposit.notification.resource.delete"),
      keepCurrentStateAfterCreate: true,
      keepCurrentStateAfterUpdate: true,
      keepCurrentStateAfterDeleteList: true,
    }, _downloadService, ResourceFileStateModeEnum.thumbnail, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublications;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: DepositStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: DepositStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: DepositStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Selector()
  static isReadyToBeDisplayed(state: DepositStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: DepositStateModel): boolean {
    return true;
  }

  @Action(DepositAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const minimalAction = [
      {
        action: new SharedDepositTypeAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDepositTypeAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDepositTypeAction.GetAllFail)),
        ],
      },
      {
        action: new SharedDepositSubtypeAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDepositSubtypeAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDepositSubtypeAction.GetAllFail)),
        ],
      },
      {
        action: new SharedLanguageAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllFail)),
        ],
      },
    ];

    const otherAction = [
      // {
      //   action: new SharedStructureAction.GetAll(new QueryParameters(environment.defaultEnumValuePageSizeOption), false, false),
      //   subActionCompletions: [
      //     this._actions$.pipe(ofSolidifyActionCompleted(SharedStructureAction.GetAllSuccess)),
      //     this._actions$.pipe(ofSolidifyActionCompleted(SharedStructureAction.GetAllFail)),
      //   ],
      // },
    ];

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, action.onlyMinimalForCreate ? minimalAction : [...minimalAction, ...otherAction])
      .pipe(
        map(result => {
          if (result.success) {
            ctx.dispatch(new DepositAction.LoadResourceSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.LoadResourceFail(action));
          }
          return result.success;
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(DepositAction.GetAll)
  getAll(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetAll): Observable<CollectionTyped<Deposit>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        total: 0,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });

    return this._apiService.getCollection<Deposit>(this._getUrlForList(), ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<Deposit>) => {
          ctx.dispatch(ResourceActionHelper.getAllSuccess<Deposit>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  private _getUrlForList(): string {
    const routingUrl = this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url);
    return this._urlResource + SOLIDIFY_CONSTANTS.SEPARATOR + (routingUrl.startsWith(urlSeparator + AppRoutesEnum.depositToValidate) ? ApiActionNameEnum.LIST_VALIDABLE_PUBLICATIONS : ApiActionNameEnum.LIST_MY_PUBLICATIONS);
  }

  @Action(DepositAction.ChangeCurrentTab)
  changeCurrentTab(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeCurrentTab): void {
    ctx.patchState({
      currentTab: action.tabEnum,
    });
    const previousUrl = this._routerExt.getPreviousUrl();
    let queryParameters = defaultDepositStateQueryParameters();
    if (isNotNullNorUndefined(previousUrl) && previousUrl.includes(DepositRoutesEnum.detail)) {
      queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    }
    queryParameters = this._generateQueryParametersWithStatus(queryParameters, action.tabEnum, ctx);
    ctx.dispatch(new DepositAction.ChangeQueryParameters(queryParameters));
    ctx.dispatch(new DepositAction.RefreshAllCounterStatusTab());
  }

  private _generateQueryParametersWithStatus(queryParameters: QueryParameters, tab: DepositListTabEnum, ctx: SolidifyStateContext<DepositStateModel>): QueryParameters {
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);

    MappingObjectUtil.delete(searchItems, this._STATUS_KEY as string);
    MappingObjectUtil.delete(searchItems, this._STATUS_LIST_KEY);
    let newStatus: Enums.Deposit.StatusEnum[];
    switch (tab) {
      case DepositListTabEnum.in_error:
        newStatus = [Enums.Deposit.StatusEnum.IN_ERROR];
        break;
      case DepositListTabEnum.in_validation:
        newStatus = [Enums.Deposit.StatusEnum.IN_VALIDATION];
        break;
      case DepositListTabEnum.feedback_required:
        newStatus = [Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED];
        break;
      case DepositListTabEnum.in_progress:
        newStatus = [Enums.Deposit.StatusEnum.IN_PROGRESS];
        break;
      case DepositListTabEnum.in_edition:
        newStatus = [Enums.Deposit.StatusEnum.IN_EDITION];
        break;
      case DepositListTabEnum.updates_validation:
        newStatus = [Enums.Deposit.StatusEnum.UPDATES_VALIDATION];
        break;
      case DepositListTabEnum.canonical:
        newStatus = [Enums.Deposit.StatusEnum.CANONICAL];
        break;
      case DepositListTabEnum.completed:
        newStatus = [Enums.Deposit.StatusEnum.COMPLETED];
        break;
      case DepositListTabEnum.all:
      default:
        newStatus = [];
    }
    if (newStatus.length > 1) {
      MappingObjectUtil.set(searchItems, this._STATUS_LIST_KEY, newStatus.join(SOLIDIFY_CONSTANTS.COMMA));
    } else if (newStatus.length === 1) {
      MappingObjectUtil.set(searchItems, this._STATUS_KEY as string, newStatus.join(SOLIDIFY_CONSTANTS.COMMA));
    }
    if (this._showAllDeposit(ctx)) {
      MappingObjectUtil.set(searchItems, DepositState._KEY_SHOW_ALL_DEPOSITS, SOLIDIFY_CONSTANTS.STRING_TRUE);
    }
    if (this._showAsContributor(ctx)) {
      MappingObjectUtil.set(searchItems, DepositState._KEY_SHOW_AS_CONTRIBUTOR, SOLIDIFY_CONSTANTS.STRING_TRUE);
    }
    return queryParameters;
  }

  private _showAllDeposit(ctx: SolidifyStateContext<DepositStateModel>): boolean {
    const currentQueryParameters = ctx.getState().queryParameters;
    const searchItems = QueryParametersUtil.getSearchItems(currentQueryParameters);
    return MappingObjectUtil.get(searchItems, DepositState._KEY_SHOW_ALL_DEPOSITS) === SOLIDIFY_CONSTANTS.STRING_TRUE;
  }

  private _showAsContributor(ctx: SolidifyStateContext<DepositStateModel>): boolean {
    const currentQueryParameters = ctx.getState().queryParameters;
    const searchItems = QueryParametersUtil.getSearchItems(currentQueryParameters);
    return MappingObjectUtil.get(searchItems, DepositState._KEY_SHOW_AS_CONTRIBUTOR) === SOLIDIFY_CONSTANTS.STRING_TRUE;
  }

  @Action(DepositAction.RefreshAllCounterStatusTab)
  refreshAllCounterStatusTab(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshAllCounterStatusTab): Observable<boolean> {
    const listActionsWrappper = [];
    this._LIST_TAB_WITH_COUNTER.forEach(tabEnum => {
      listActionsWrappper.push({
        action: new DepositAction.RefreshCounterStatusTab(tabEnum),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.RefreshCounterStatusTabSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.RefreshCounterStatusTabFail)),
        ],
      });
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, listActionsWrappper)
      .pipe(
        map(result => {
          if (result.success) {
            ctx.dispatch(new DepositAction.RefreshAllCounterStatusTabSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.RefreshAllCounterStatusTabFail(action));
          }
          return result.success;
        }),
      );
  }

  @Action(DepositAction.RefreshCounterStatusTab)
  refreshCounterStatusTab(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshCounterStatusTab): Observable<number> {
    let queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    queryParameters = this._generateQueryParametersWithStatus(queryParameters, action.tabEnum, ctx);
    return this._apiService.getCollection<Deposit>(this._getUrlForList(), queryParameters)
      .pipe(
        map((collection: CollectionTyped<Deposit>) => {
          const totalItem = collection._page.totalItems;
          ctx.dispatch(new DepositAction.RefreshCounterStatusTabSuccess(action, action.tabEnum, totalItem));
          return totalItem;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.RefreshCounterStatusTabFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.RefreshCounterStatusTabSuccess)
  refreshCounterStatusTabSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshCounterStatusTabSuccess): void {
    const tabListCounters = MappingObjectUtil.copy(ctx.getState().tabListCounters);
    MappingObjectUtil.set(tabListCounters, action.tabEnum, action.counter);
    ctx.patchState({
      tabListCounters: tabListCounters,
    });
  }

  @OverrideDefaultAction()
  @Action(DepositAction.Create)
  create(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Create): Observable<Deposit> {
    return super.create(ctx, action).pipe(
      this._catchValidationErrorsForMarkStepAsInError(ctx, action.modelFormControlEvent as ModelFormControlEventDeposit),
    );
  }

  @OverrideDefaultAction()
  @Action(DepositAction.Update)
  update(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Update): Observable<Deposit> {
    return super.update(ctx, action).pipe(
      this._catchValidationErrorsForMarkStepAsInError(ctx, action.modelFormControlEvent as ModelFormControlEventDeposit),
    );
  }

  @OverrideDefaultAction()
  @Action(DepositAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateSuccess): void {
    this.createOrUpdateSuccess(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(DepositAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateSuccess): void {
    this.createOrUpdateSuccess(ctx, action);
  }

  createOrUpdateSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateSuccess | DepositAction.UpdateSuccess): void {
    const deposit = action.parentAction.modelFormControlEvent.model;
    const targetRoutingAfterSave = deposit.targetRoutingAfterSave;
    delete deposit.targetRoutingAfterSave;

    if (action instanceof DepositAction.CreateSuccess) {
      super.createSuccess(ctx, action);
    } else if (action instanceof DepositAction.UpdateSuccess) {
      super.updateSuccess(ctx, action);
    } else {
      return;
    }

    if (targetRoutingAfterSave === Enums.Deposit.TargetRoutingAfterSaveEnum.NOTHING
      || targetRoutingAfterSave === Enums.Deposit.TargetRoutingAfterSaveEnum.SAVE_AND_SUBMIT) {
      return;
    }

    const isValidationMode = this._router.url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + AppRoutesEnum.depositToValidate + SOLIDIFY_CONSTANTS.URL_SEPARATOR);

    DepositState._redirectAfterSave(ctx, action, targetRoutingAfterSave, isValidationMode);
  }

  private static _redirectAfterSave(ctx: SolidifyStateContext<DepositStateModel>,
                                    action: DepositAction.UpdateSuccess | DepositAction.CreateSuccess,
                                    targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum,
                                    isValidationMode: boolean): void {

    const resId = action.model.resId;
    let baseRoute = RoutesEnum.deposit;
    if (isValidationMode) {
      baseRoute = RoutesEnum.depositToValidate;
    }

    if (targetRoutingAfterSave === Enums.Deposit.TargetRoutingAfterSaveEnum.SAVE_FOR_LATER) {
      ctx.dispatch(new Navigate(isValidationMode ? [baseRoute, DepositRoutesEnum.detail, resId] : [baseRoute]));
      return;
    }

    baseRoute = baseRoute + urlSeparator + DepositRoutesEnum.detail;

    const currentStep = action.parentAction.modelFormControlEvent.model.formStep;
    const currentStepInNumber = Enums.Deposit.publicationStepEnumNumber(currentStep);

    const nextStepName = Enums.Deposit.publicationStepEnumValue(currentStepInNumber + targetRoutingAfterSave);
    ctx.dispatch(new Navigate([baseRoute, resId, DepositRoutesEnum.edit, nextStepName]));
  }

  private _catchValidationErrorsForMarkStepAsInError<T>(ctx: SolidifyStateContext<DepositStateModel>, modelFormControlEvent: ModelFormControlEventDeposit): MonoTypeOperatorFunction<Deposit> {
    return catchError((error: SolidifyHttpErrorResponseModel | SolidifyError) => {
      if (error instanceof SolidifyError) {
        error = error.nativeError;
      }
      const validationErrors = ErrorHelper.extractAllValidationErrorsFromError(error);

      this._treatSpecificValidationErrorDuplicateFile(ctx, validationErrors);

      if (validationErrors.length > 0) {
        this._manageContributorErrors(modelFormControlEvent, validationErrors);
        const listFormNameToUpdateValueAndValidity = [];
        validationErrors.forEach(validationError => {
          const firstIndexOfDot = validationError.fieldName.indexOf(SOLIDIFY_CONSTANTS.DOT);
          let formName: string = validationError.fieldName;
          if (firstIndexOfDot !== -1) {
            formName = validationError.fieldName.substring(0, firstIndexOfDot);
          }
          if (listFormNameToUpdateValueAndValidity.indexOf(formName) === -1) {
            listFormNameToUpdateValueAndValidity.push(formName);
          }
        });

        listFormNameToUpdateValueAndValidity.forEach(formName => {
          const form = modelFormControlEvent.formControl.get(formName);
          if (isNotNullNorUndefined(form)) {
            form.updateValueAndValidity();
          }
        });

      }
      modelFormControlEvent.changeDetectorRef.detectChanges();
      modelFormControlEvent.subFormPartDetectChanges();
      throw error;
    });
  }

  private _treatSpecificValidationErrorDuplicateFile(ctx: SolidifyStateContext<DepositStateModel>, validationErrors: ValidationErrorDto[]): void {
    if (validationErrors?.findIndex(v => v.fieldName === this._ERROR_VALIDATION_FIELD_NAME_DOCUMENT_FILES) === -1) {
      return;
    }
    const depositId = ctx.getState().current?.resId;
    if (isNotNullNorUndefinedNorWhiteString(depositId)) {
      ctx.dispatch(new DepositDocumentFileAction.Refresh(depositId));
    }
  }

  private _manageContributorErrors(modelFormControlEvent: ModelFormControlEventDeposit, validationErrors: ValidationErrorDto[]): void {
    const fdMain = new FormComponentFormDefinitionMain();
    const fdContributors = new FormComponentFormDefinitionThirdStepContributors();
    const pathToContributor = fdMain.contributors + "." + fdContributors.contributors;
    const listErrors = validationErrors.filter(f => f.fieldName.startsWith(pathToContributor));
    if (listErrors.length === 0) {
      return;
    }

    const pathToContributorMembersFrontend = fdMain.contributors + "." + fdContributors.contributorMembersFrontend;
    const pathToCollaborationMembersFrontend = fdMain.contributors + "." + fdContributors.collaborationMembersFrontend;
    const ERROR_INDEX = 2;

    const fcContributor = modelFormControlEvent.formControl.get(pathToContributorMembersFrontend) as FormArray;
    if (isNullOrUndefined(fcContributor)) {
      return;
    }

    listErrors.forEach(error => {
      const pathSplitted = error.fieldName.split(SOLIDIFY_CONSTANTS.DOT);
      let errorIndex = +pathSplitted[ERROR_INDEX];

      let newPath = pathToContributorMembersFrontend;
      if (errorIndex >= fcContributor.length) {
        newPath = pathToCollaborationMembersFrontend;
        errorIndex = errorIndex - fcContributor.length;
      }
      error.fieldName = newPath + SOLIDIFY_CONSTANTS.DOT + errorIndex + SOLIDIFY_CONSTANTS.DOT + pathSplitted.slice(ERROR_INDEX + 1).join(".");
    });

    StoreUtil.iterateOverValidationErrorToBindIntoFormControlIfFoundIt(modelFormControlEvent, validationErrors, this._optionsState.autoScrollToFirstValidationError);
  }

  @Action(DepositAction.UpdatePublicationSubTypeDocumentId)
  updatePublicationSubTypeDocumentId(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdatePublicationSubTypeDocumentId): void {
    ctx.patchState({
      publicationSubTypeId: action.publicationSubTypeId,
    });
  }

  @OverrideDefaultAction()
  @Action(DepositAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    ctx.patchState({
      publicationSubTypeId: action.model.subtype.resId,
    });
    if (isNotNullNorUndefined(action.model.thumbnail)) {
      ctx.dispatch(new DepositAction.GetFile(action.model.resId));
    }
  }

  @OverrideDefaultAction()
  @Action(DepositAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DeleteSuccess): void {
    super.deleteSuccess(ctx, action);
    const url = this._router.url;
    const splittedUrl = url.split(SOLIDIFY_CONSTANTS.URL_SEPARATOR).filter(s => isNonEmptyString(s));
    if (splittedUrl.length === 0) {
      return;
    }
    if (splittedUrl.length === 2
      && (splittedUrl[0] === AppRoutesEnum.deposit || splittedUrl[0] === AppRoutesEnum.depositToValidate)
      && EnumUtil.convertToArray(DepositListTabEnum).indexOf(splittedUrl[1]) !== -1) {
      return;
    }
    ctx.dispatch(new Navigate([splittedUrl[0]]));
  }

  @Selector()
  static getPublicationSubTypeId(state: DepositStateModel): string {
    return state.publicationSubTypeId;
  }

  @Action(DepositAction.SubmitForValidation)
  submitForValidation(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitForValidation): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    let validationStructure;
    if (isNonEmptyString(action.validationStructureId)) {
      validationStructure = [action.validationStructureId];
    }
    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.SUBMIT_FOR_VALIDATION, validationStructure)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.SubmitForValidationSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.SubmitForValidationFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.SubmitForValidationFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.SubmitForValidationSuccess)
  submitForValidationSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitForValidationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.depositSubmittedInValidation);

    const redirectToList = action.parentAction.redirectToListWhenSuccess;

    if (isTrue(redirectToList)) {
      ctx.dispatch(new Navigate([RoutesEnum.deposit]));
    }
  }

  @Action(DepositAction.SubmitForValidationFail)
  submitForValidationFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitForValidationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToSubmitDeposit);
  }

  @Action(DepositAction.Submit)
  submit(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Submit): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.SUBMIT)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.SubmitSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.SubmitFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.SubmitFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.SubmitSuccess)
  submitSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositAction.CleanAndSubmitSuccess(action as any));
  }

  @Action(DepositAction.SubmitFail)
  submitFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositAction.CleanAndSubmitFail(action as any));
  }

  @Action(DepositAction.Clone)
  clone(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Clone): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<Deposit>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.CLONE, action.deposit)
      .pipe(
        tap(deposit => {
          ctx.dispatch(new DepositAction.CloneSuccess(action, deposit));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.CloneFail(action as any));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.CloneSuccess)
  cloneSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CloneSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.deposit,
    });

    this._notificationService.showSuccess(LabelTranslateEnum.clonedDeposit);
    ctx.dispatch([
      new DepositAction.Clean(false),
      new DepositAction.GetById(action.deposit.resId),
      new DepositDocumentFileAction.GetAll(action.deposit.resId, defaultDepositDocumentFileStateQueryParameters()),
      new DepositDocumentFileAction.GetListCurrentStatus(action.deposit.resId),
      new Navigate([RoutesEnum.depositDetail, action.deposit.resId, DepositRoutesEnum.edit, Enums.Deposit.StepEnum.FILES]),
    ]);
  }

  @Action(DepositAction.CloneFail)
  cloneFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CloneFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(LabelTranslateEnum.unableToCloneDeposit);
  }

  @Action(DepositAction.ChangeToCanonical)
  changeToCanonical(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToCanonical): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<Deposit>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.SEND_TO_CANONICAL, action.deposit)
      .pipe(
        tap(deposit => {
          ctx.dispatch(new DepositAction.ChangeToCanonicalSuccess(action, deposit));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ChangeToCanonicalFail(action as any));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ChangeToCanonicalSuccess)
  changeToCanonicalSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToCanonicalSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showSuccess(LabelTranslateEnum.changedToCanonical);
    ctx.dispatch(new DepositAction.GetById(action.deposit.resId));
  }

  @Action(DepositAction.ChangeToCanonicalFail)
  changeToCanonicalFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToCanonicalFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(LabelTranslateEnum.unableToChangeToCanonical);
  }

  @Action(DepositAction.ChangeToCompleted)
  changeToCompleted(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToCompleted): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<Deposit>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.SEND_BACK_TO_COMPLETED, action.deposit)
      .pipe(
        tap(deposit => {
          ctx.dispatch(new DepositAction.ChangeToCompletedSuccess(action, deposit));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ChangeToCompletedFail(action as any));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ChangeToCompletedSuccess)
  changeToCompletedSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToCompletedSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showSuccess(LabelTranslateEnum.changeToCompleted);
    ctx.dispatch(new DepositAction.GetById(action.deposit.resId));

  }

  @Action(DepositAction.ChangeToCompletedFail)
  changeToCompletedFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToCompletedFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(LabelTranslateEnum.unableToChangeToCompleted);
  }

  @Action(DepositAction.ChangeToDeleted)
  changeToDeleted(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToDeleted): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<Deposit>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.DELETE)
      .pipe(
        tap(deposit => {
          ctx.dispatch(new DepositAction.ChangeToDeletedSuccess(action, deposit));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ChangeToDeletedFail(action as any));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ChangeToDeletedSuccess)
  changeToDeletedSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToDeletedSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showSuccess(LabelTranslateEnum.changeToDeleted);
    ctx.dispatch(new DepositAction.GetById(action.deposit.resId));

  }

  @Action(DepositAction.ChangeToDeletedFail)
  changeToDeletedFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeToDeletedFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(LabelTranslateEnum.unableToChangeToDeleted);
  }

  @Action(DepositAction.CleanAndSubmit)
  cleanAndSubmit(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CleanAndSubmit): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.patchById<Deposit>(this._urlResource, action.deposit.resId, action.deposit)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositAction.Submit(action.deposit.resId, DepositRedirectTargetEnum.LIST));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.CleanAndSubmitFail(action as any));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.CleanAndSubmitSuccess)
  cleanAndSubmitSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CleanAndSubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showSuccess(LabelTranslateEnum.depositSubmitted);

    const redirectTarget = (action.parentAction.parentAction as DepositAction.Submit).redirectToDetailPageWhenSuccess;
    const depositId = (action.parentAction.parentAction as DepositAction.Submit).depositId;

    if (redirectTarget === DepositRedirectTargetEnum.DETAIL) {
      ctx.dispatch([
        new DepositAction.GetById(depositId),
        new Navigate([RoutesEnum.depositToValidateDetail, depositId]),
      ]);
    } else if (redirectTarget === DepositRedirectTargetEnum.LIST) {
      ctx.dispatch(new Navigate([RoutesEnum.depositToValidate]));
    }
  }

  @Action(DepositAction.CleanAndSubmitFail)
  cleanAndSubmitFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CleanAndSubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(LabelTranslateEnum.unableToSubmitDeposit);
  }

  @Action(DepositAction.Transfer)
  transfer(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Transfer): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._updateSubResourceDepositStructure(action, ctx).pipe(
      tap(result => {
        if (isTrue(result)) {
          ctx.dispatch(new DepositAction.TransferSuccess(action));
        } else {
          ctx.dispatch(new DepositAction.TransferFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new DepositAction.TransferFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(DepositAction.TransferSuccess)
  transferSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.TransferSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showSuccess(LabelTranslateEnum.depositTransferred);
    ctx.dispatch(new Navigate([RoutesEnum.depositToValidate]));
  }

  @Action(DepositAction.TransferFail)
  transferFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.TransferFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(LabelTranslateEnum.unableToTransferDeposit);
  }

  @Action(DepositAction.SendBackForValidation)
  sendBackForValidation(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SendBackForValidation): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.SEND_BACK_FOR_VALIDATION, null)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.SendBackForValidationSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.SendBackForValidationFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.SendBackForValidationFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.SendBackForValidationSuccess)
  sendBackForValidationSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SendBackForValidationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    if (isFalse(action.parentAction.fromList)) {
      this._notificationService.showSuccess(LabelTranslateEnum.depositSentBackForValidation);

      const depositId = action.parentAction.depositId;
      ctx.dispatch([
        new DepositAction.GetById(depositId),
        new DepositCommentAction.GetAll(depositId, defaultDepositCommentStateQueryParameters()),
        new DepositCommentValidatorAction.GetAll(depositId, defaultDepositCommentValidatorStateQueryParameters()),
        new Navigate([RoutesEnum.depositToValidateDetail, depositId]),
      ]);
    }
  }

  @Action(DepositAction.SendBackForValidationFail)
  sendBackForValidationFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SendBackForValidationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    if (isFalse(action.parentAction.fromList)) {
      this._notificationService.showError(LabelTranslateEnum.unableToSendBackDepositForValidation);
    }
  }

  @Action(DepositAction.ExportDepositsInProgress)
  exportDepositsInProgress(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ExportDepositsInProgress): Observable<Blob> {
    ctx.patchState({
      isLoadingFile: true,
    });

    return this._downloadService.downloadInMemory(action.url, "metadata.csv", true, null, true, action.ids)
      .pipe(
        tap((data: Blob) => {
          ctx.dispatch(new DepositAction.ExportDepositsInProgressSuccess(action, data));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ExportDepositsInProgressFail(action));
          throw error;
        }),
      );
  }

  @Action(DepositAction.ExportDepositsInProgressSuccess)
  exportDepositsInProgressSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ExportDepositsInProgressSuccess): void {
    ctx.patchState({
      isLoadingFile: false,
      file: isNullOrUndefined(action.blob) ? undefined : URL.createObjectURL(action.blob),
    });
  }

  @Action(DepositAction.ExportDepositsInProgressFail)
  exportDepositsInProgressFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ExportDepositsInProgressFail): void {
    ctx.patchState({
      isLoadingFile: false,
      file: undefined,
    });
  }

  @Action(DepositAction.Reject)
  reject(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Reject): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const customParams = {
      [this._KEY_QUERY_PARAM_REASON]: action.reason,
    };

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.REJECT, null, customParams)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.RejectSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.RejectFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.RejectFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.RejectSuccess)
  rejectSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      metadataDifferences: undefined,
    });

    if (isFalse(action.parentAction.fromList)) {
      this._notificationService.showSuccess(LabelTranslateEnum.depositRejected);

      const depositId = action.parentAction.depositId;
      ctx.dispatch([
        new DepositAction.GetById(depositId),
        new DepositCommentAction.GetAll(depositId, defaultDepositCommentStateQueryParameters()),
        new DepositCommentValidatorAction.GetAll(depositId, defaultDepositCommentValidatorStateQueryParameters()),
        new Navigate([RoutesEnum.depositToValidateDetail, depositId]),
      ]);
    }
  }

  @Action(DepositAction.RejectFail)
  rejectFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      metadataDifferences: undefined,
    });

    if (isFalse(action.parentAction.fromList)) {
      this._notificationService.showError(LabelTranslateEnum.unableToRejectDeposit);
    }
  }

  @Action(DepositAction.RejectList)
  rejectList(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectList): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const listActionWrapper: ActionSubActionCompletionsWrapper[] = [];

    action.listDepositId.forEach(depositId => {
      listActionWrapper.push({
        action: new DepositAction.Reject(depositId, action.reason, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.RejectSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.RejectFail)),
        ],
      });
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, listActionWrapper).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositAction.RejectListSuccess(action));
        } else {
          ctx.dispatch(new DepositAction.RejectListFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositAction.RejectListSuccess)
  rejectListSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectListSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    ctx.dispatch(new DepositAction.GetAll());
    this._notificationService.showSuccess(LabelTranslateEnum.depositsRejected);
  }

  @Action(DepositAction.RejectListFail)
  rejectListFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectListFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    ctx.dispatch(new DepositAction.GetAll());
    this._notificationService.showError(LabelTranslateEnum.notAllDepositsCouldBeRejected);
  }

  @Action(DepositAction.AskFeedbackList)
  askFeedbackList(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.AskFeedbackList): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const listActionWrapper: ActionSubActionCompletionsWrapper[] = [];

    action.listDepositId.forEach(depositId => {
      listActionWrapper.push({
        action: new DepositAction.AskFeedback(depositId, action.reason, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.AskFeedbackSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.AskFeedbackFail)),
        ],
      });
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, listActionWrapper).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositAction.AskFeedbackListSuccess(action));
        } else {
          ctx.dispatch(new DepositAction.AskFeedbackListFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositAction.AskFeedbackListSuccess)
  askFeedbackListSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.AskFeedbackListSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositAction.GetAll());
    this._notificationService.showSuccess(LabelTranslateEnum.depositsAskFeedback);
  }

  @Action(DepositAction.AskFeedbackListFail)
  askFeedbackListFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.AskFeedbackListFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositAction.GetAll());
    this._notificationService.showError(LabelTranslateEnum.theFeedbackRequestCannotBeMadeForAllDeposits);
  }

  @Action(DepositAction.Resume)
  resume(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.RESUME)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ResumeFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.depositResumed);

    const depositId = action.parentAction.depositId;
    ctx.dispatch([
      new DepositAction.GetById(depositId),
      new DepositCommentAction.GetAll(depositId, defaultDepositCommentStateQueryParameters()),
      new DepositCommentValidatorAction.GetAll(depositId, defaultDepositCommentValidatorStateQueryParameters()),
      new Navigate([RoutesEnum.depositToValidateDetail, depositId]),
    ]);
  }

  @Action(DepositAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToResumeDeposit);
  }

  @Action(DepositAction.EnableRevision)
  enableRevision(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.EnableRevision): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.ENABLE_REVISION)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.EnableRevisionSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.EnableRevisionFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.EnableRevisionFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.EnableRevisionSuccess)
  enableRevisionSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.EnableRevisionSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    const depositId = action.parentAction.depositId;
    ctx.dispatch(new Navigate([RoutesEnum.depositDetail, depositId, DepositRoutesEnum.edit, Enums.Deposit.StepEnum.FILES]));
  }

  @Action(DepositAction.EnableRevisionFail)
  enableRevisionFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.EnableRevisionFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToEnableRevisionOnDeposit);
  }

  @Action(DepositAction.AskFeedback)
  askFeedback(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.AskFeedback): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const customParams = {
      [this._KEY_QUERY_PARAM_REASON]: action.reason,
    };

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.ASK_FEEDBACK, null, customParams)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.AskFeedbackSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.AskFeedbackFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.AskFeedbackFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.AskFeedbackSuccess)
  askFeedbackSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.AskFeedbackSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (isFalse(action.parentAction.fromList)) {
      this._notificationService.showSuccess(LabelTranslateEnum.askFeedbackSuccess);

      const depositId = action.parentAction.depositId;
      ctx.dispatch([
        new DepositAction.GetById(depositId),
        new DepositCommentAction.GetAll(depositId, defaultDepositCommentStateQueryParameters()),
        new DepositCommentValidatorAction.GetAll(depositId, defaultDepositCommentValidatorStateQueryParameters()),
        new Navigate([RoutesEnum.depositToValidateDetail, depositId]),
      ]);
    }
  }

  @Action(DepositAction.AskFeedbackFail)
  askFeedbackFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.AskFeedbackFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToAskFeedback);
  }

  private _updateSubResourceDepositStructure(action: DepositAction.SubmitForValidation | DepositAction.Transfer, ctx: SolidifyStateContext<DepositStateModel>): Observable<boolean> {
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new DepositStructureAction.Update(action.depositId, [action.validationStructureId]),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositStructureAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositStructureAction.UpdateFail)),
        ],
      },
    ]).pipe(
      map(result => result.success),
    );
  }

  @Action(DepositAction.GetMyDefaultProfileData)
  getMyDefaultProfileData(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetMyDefaultProfileData): Observable<DepositDefaultData> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.get<DepositDefaultData>(this._urlResource + urlSeparator + ApiActionNameEnum.GET_MY_DEFAULT_PROFILE_DATA)
      .pipe(
        tap(defaultData => {
          ctx.dispatch(new DepositAction.GetMyDefaultProfileDataSuccess(action, defaultData));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.GetMyDefaultProfileDataFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.GetMyDefaultProfileDataSuccess)
  getMyDefaultProfileDataSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetMyDefaultProfileDataSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      defaultData: action.defaultData,
    });
  }

  @Action(DepositAction.GetMyDefaultProfileDataFail)
  getMyDefaultProfileDataFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetMyDefaultProfileDataFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.NotMyDeposit)
  notMyDeposit(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.NotMyDeposit): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.UNLINK_CONTRIBUTOR)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.NotMyDepositSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.NotMyDepositFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.NotMyDepositFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.NotMyDepositSuccess)
  notMyDepositSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.NotMyDepositSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.weHaveRecordedThatThisIsNotYourDeposit);
    ctx.dispatch(new Navigate([RoutesEnum.deposit]));
  }

  @Action(DepositAction.NotMyDepositFail)
  notMyDepositFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.NotMyDepositFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToIndicateThatIsNotMyDeposit);
  }

  @Action(DepositAction.DisplayDialogFileImportedAutomatically)
  displayDialogFileImportedAutomatically(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DisplayDialogFileImportedAutomatically): void {
    if (isTrue(ctx.getState().alreadyDisplayDialogFileImportedAutomatically)) {
      return;
    }
    ctx.patchState({
      alreadyDisplayDialogFileImportedAutomatically: true,
    });

    DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.pdfAutomaticallyImported.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.pdfAutomaticallyImported.message"),
      cancelButtonToTranslate: LabelTranslateEnum.okayIUnderstand,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "90%",
    });
  }

  @Action(DepositAction.DisplayDialogContributorsPartiallyImported)
  displayDialogContributorsPartiallyImported(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DisplayDialogContributorsPartiallyImported): void {
    if (isTrue(ctx.getState().alreadyDisplayDialogContributorsPartiallyImported)) {
      return;
    }
    ctx.patchState({
      alreadyDisplayDialogContributorsPartiallyImported: true,
    });

    const limitMaxContributor = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current.limitMaxContributors);
    DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.contributorsPartiallyImported.title"),
      messageToTranslate: LabelTranslateEnum.onlyTheFirstXContributorsHaveBeenImported,
      paramMessage: {
        ["number"]: limitMaxContributor,
      },
      cancelButtonToTranslate: LabelTranslateEnum.okayIUnderstand,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "90%",
    });
  }

  @Action(DepositAction.DisplayDialogUnableToImportFile)
  displayDialogUnableToImportFile(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DisplayDialogUnableToImportFile): void {
    if (isTrue(ctx.getState().alreadyDisplayDialogFileImportedAutomatically)) {
      return;
    }
    ctx.patchState({
      alreadyDisplayDialogFileImportedAutomatically: true,
    });

    DialogUtil.open(this._dialog, DepositUnableImportFileDialog, {
      listFileUnableToImport: action.listFileUnableToImport,
    } as DepositUnableImportFileDialogData, {
      width: "90%",
    });
  }

  @Action(DepositAction.UpdateResearchGroup)
  updateResearchGroup(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateResearchGroup): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParam = {
      [this._KEY_QUERY_PARAM_RESEARCH_GROUP_ID]: action.researchGroupId,
    };

    return this._apiService.patch<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.UPDATE_PUBLICATIONS_RESEARCH_GROUP, action.listDepositId, queryParam)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.UpdateResearchGroupSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.UpdateResearchGroupFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.UpdateResearchGroupFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.UpdateResearchGroupSuccess)
  updateResearchGroupSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateResearchGroupSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.researchGroupHaveBeenAddedToTheDeposit);
    ctx.dispatch(new DepositAction.GetAll());
  }

  @Action(DepositAction.UpdateResearchGroupFail)
  updateResearchGroupFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateResearchGroupFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.aProblemOccurredWhileAddingTheResearchGroupToTheDeposits);
  }

  @Action(DepositAction.UpdateStructure)
  updateStructure(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateStructure): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParam = {
      [this._KEY_QUERY_PARAM_STRUCTURE_ID]: action.structureId,
    };

    return this._apiService.patch<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.UPDATE_PUBLICATIONS_STRUCTURE, action.listDepositId, queryParam)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.UpdateStructureSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.UpdateStructureFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.UpdateStructureFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.UpdateStructureSuccess)
  updateStructureSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateStructureSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.structureHaveBeenAddedToTheDeposit);
    ctx.dispatch(new DepositAction.GetAll());
  }

  @Action(DepositAction.UpdateStructureFail)
  updateStructureFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateStructureFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.aProblemOccurredWhileAddingTheStructureToTheDeposits);
  }

  @Action(DepositAction.UpdateLanguage)
  updateLanguage(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateLanguage): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParam = {
      [this._KEY_QUERY_PARAM_LANGUAGE_ID]: action.languageId,
    };

    return this._apiService.patch<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.UPDATE_PUBLICATIONS_LANGUAGE, action.listDepositId, queryParam)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.UpdateLanguageSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.UpdateLanguageFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.UpdateLanguageFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.UpdateLanguageSuccess)
  updateLanguageSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateLanguageSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.languageHaveBeenAddedToTheDeposit);
    ctx.dispatch(new DepositAction.GetAll());
  }

  @Action(DepositAction.UpdateLanguageFail)
  updateLanguageFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateLanguageFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.aProblemOccurredWhileAddingTheLanguageToTheDeposits);
  }

  @Action(DepositAction.CreateBulkFromIdentifier)
  createBulkFromIdentifier(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateBulkFromIdentifier): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      bulkImportsResult: {},
    });

    const listActionSubActionCompletionsWrapper: ActionSubActionCompletionsWrapper[] = [];

    action.listPublicationImport.forEach(publicationImport => {
      listActionSubActionCompletionsWrapper.push({
        action: new DepositAction.CreateFromIdentifier(publicationImport),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.CreateFromIdentifierSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.CreateFromIdentifierFail)),
        ],
      });
    });

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, listActionSubActionCompletionsWrapper, 0, false).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositAction.CreateBulkFromIdentifierSuccess(action));
        } else {
          ctx.dispatch(new DepositAction.CreateBulkFromIdentifierFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositAction.CreateBulkFromIdentifierSuccess)
  createBulkFromIdentifierSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateBulkFromIdentifierSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.CreateBulkFromIdentifierFail)
  createBulkFromIdentifierFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateBulkFromIdentifierFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.CreateFromIdentifier)
  createFromIdentifier(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateFromIdentifier): Observable<Deposit> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<PublicationImportDTO, Deposit>(this._urlResource + urlSeparator + ApiActionNameEnum.CREATE_FROM_IDENTIFIER, action.publicationImportDTO)
      .pipe(
        tap(deposit => {
          ctx.dispatch(new DepositAction.CreateFromIdentifierSuccess(action, {
            deposit: deposit,
          }));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.CreateFromIdentifierFail(action, {
            error: error.error,
          }));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.CreateFromIdentifierSuccess)
  createFromIdentifierSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateFromIdentifierSuccess): void {
    this._updateImportResult(ctx, action);
  }

  @Action(DepositAction.CreateFromIdentifierFail)
  createFromIdentifierFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateFromIdentifierFail): void {
    this._updateImportResult(ctx, action);
  }

  private _updateImportResult(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateFromIdentifierSuccess | DepositAction.CreateFromIdentifierFail): void {
    const id = isNonEmptyString(action.parentAction.publicationImportDTO.doi) ? action.parentAction.publicationImportDTO.doi : action.parentAction.publicationImportDTO.pmid;
    const bulkImportsResult = MappingObjectUtil.copy(ctx.getState().bulkImportsResult);
    MappingObjectUtil.set(bulkImportsResult, id, action.result);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      bulkImportsResult: bulkImportsResult,
    });
  }

  @Action(DepositAction.ValidateMetadata)
  validateMetadata(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ValidateMetadata): Observable<string> {
    const searchMap = {} as MappingObject<string, boolean>;
    if (action.isForSubmit) {
      MappingObjectUtil.set(searchMap, "forSubmit", true);
    }
    // return this._httpClient.post<T>(`${path}`,
    //   body,
    //   {
    //     params: queryParametersHttp,
    //     withCredentials: this.configuration.withCredentials,
    //     headers,
    //     observe,
    //     reportProgress,
    //   },
    // ) as Observable<HttpEvent<U>>;
    // return this._httpClient.post<any, string>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.VALIDATE_METADATA, undefined, MappingObjectUtil.toObject(searchMap) as any)
    //   .pipe(
    //     tap(result => {
    //       ctx.dispatch(new DepositAction.ValidateMetadataSuccess(action));
    //     }),
    //     catchError((error: SolidifyHttpErrorResponseModel) => {
    //       ctx.dispatch(new DepositAction.ValidateMetadataFail(action, error.error));
    //       throw new SolidifyStateError(this, error);
    //     }),
    //   );
    return this._apiService.post<any, string>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.VALIDATE_METADATA, undefined, MappingObjectUtil.toObject(searchMap) as any)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositAction.ValidateMetadataSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          if (error.status === HttpStatusCode.Ok) {
            // "Publication is valid"
            ctx.dispatch(new DepositAction.ValidateMetadataSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.ValidateMetadataFail(action, error.error));
          }
          throw environment.errorToSkipInErrorHandler;
        }),
      );
  }

  @Action(DepositAction.ValidateMetadataSuccess)
  validateMetadataSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ValidateMetadataSuccess): void {
    ctx.patchState({
      errorValidation: undefined,
    });
  }

  @Action(DepositAction.ValidateMetadataFail)
  validateMetadataFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ValidateMetadataFail): void {
    if (action.parentAction.depositId === ctx.getState().current?.resId) {
      ctx.patchState({
        errorValidation: action.error?.validationErrors,
      });
    }
  }

  @Action(DepositAction.CheckDuplicates)
  checkDuplicates(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckDuplicates): Observable<string> {
    return this._apiService.post<void, string>(`${this._urlResource}/${action.depositId}/${ApiActionNameEnum.CHECK_DUPLICATES}`)
      .pipe(
        tap(() => ctx.dispatch(new DepositAction.CheckDuplicatesSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          if (error.status === HttpStatusCode.Ok) {
            // "Publication is valid"
            ctx.dispatch(new DepositAction.CheckDuplicatesSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.CheckDuplicatesFail(action, error.error));
          }
          throw environment.errorToSkipInErrorHandler;
        }),
      );
  }

  @Action(DepositAction.CheckDuplicatesSuccess)
  checkDuplicatesSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckDuplicatesSuccess): void {
    ctx.patchState({
      errorDuplicate: undefined,
    });
  }

  @Action(DepositAction.CheckDuplicatesFail)
  checkDuplicatesFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckDuplicatesFail): void {
    if (action.parentAction.depositId === ctx.getState().current?.resId) {
      const errorDtoMessage = ErrorDtoUtil.buildMessageFromAdditionalParameters(action.error);
      ctx.patchState({
        errorDuplicate: errorDtoMessage,
      });
    }
  }

  @OverrideDefaultAction()
  @Action(DepositAction.Clean)
  clean(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Clean): void {
    super.clean(ctx, action);
    ctx.patchState({
      errorValidation: undefined,
      errorDuplicate: undefined,
      listUserRoleEnum: [],
      alreadyDisplayDialogFileImportedAutomatically: false,
      alreadyDisplayDialogContributorsPartiallyImported: false,
    });
  }

  @Action(DepositAction.ListPublicationUserRoles)
  listPublicationUserRoles(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ListPublicationUserRoles): Observable<Enums.Deposit.UserRoleEnum[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.get<Enums.Deposit.UserRoleEnum[]>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.LIST_PUBLICATION_USER_ROLES)
      .pipe(
        tap(listUserRoleEnum => {
          ctx.dispatch(new DepositAction.ListPublicationUserRolesSuccess(action, listUserRoleEnum));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ListPublicationUserRolesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ListPublicationUserRolesSuccess)
  listPublicationUserRolesSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ListPublicationUserRolesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listUserRoleEnum: action.listUserRoleEnum,
    });
  }

  @Action(DepositAction.ListPublicationUserRolesFail)
  listPublicationUserRolesFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ListPublicationUserRolesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.StartEditingPublication)
  startEditingMetadata(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.StartEditingPublication): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.START_METADATA_EDITING)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.StartEditingPublicationSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.StartEditingPublicationFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.StartEditingPublicationFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.StartEditingPublicationSuccess)
  startEditingPublicationSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.StartEditingPublicationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.StartEditingPublicationFail)
  startEditingPublicationFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.StartEditingPublicationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToStartEditingMetadataOnDeposit);
  }

  @Action(DepositAction.CancelEditingPublication)
  cancelEditingPublication(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CancelEditingPublication): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.CANCEL_METADATA_EDITING)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.CancelEditingPublicationSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.CancelEditingPublicationFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.StartEditingPublicationFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.CancelEditingPublicationSuccess)
  cancelEditingPublicationSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CancelEditingPublicationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.CancelEditingPublicationFail)
  cancelEditingPublicationFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CancelEditingPublicationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToCancelUpdatePublication);

  }

  @Action(DepositAction.CheckMetadataDifferences)
  checkMetadataDifferences(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckMetadataDifferences): Observable<MappingObject<string, MappingObject<string, MetadataDifference[]>>> {
    return this._apiService.get<MappingObject<string, MappingObject<string, MetadataDifference[]>>>(this._urlResource + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.GET_PENDING_METADATA_UPDATES, null)
      .pipe(
        tap(m => {
          ctx.dispatch(new DepositAction.CheckMetadataDifferencesSuccess(action, m));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.CheckMetadataDifferencesFail(action));
          throw error;
        }),
      );
  }

  @Action(DepositAction.CheckMetadataDifferencesSuccess)
  checkMetadataDifferencesSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckMetadataDifferencesSuccess): void {
    ctx.patchState({
      metadataDifferences: action.differences,
    });
  }

  @Action(DepositAction.CheckMetadataDifferencesFail)
  checkMetadataDifferencesFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckMetadataDifferencesFail): void {
    ctx.patchState({
      metadataDifferences: undefined,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToCheckMetadataDifference);
  }
}
