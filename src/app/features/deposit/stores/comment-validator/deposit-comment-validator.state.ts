/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-comment-validator.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositCommentValidatorAction,
  depositCommentValidatorActionNameSpace,
} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.action";
import {environment} from "@environments/environment";
import {Comment} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs/index";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export const defaultDepositCommentValidatorStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
    field: "creation.when",
    order: OrderEnum.ascending,
  });

export const defaultDepositCommentValidatorStateModel: () => DepositCommentValidatorStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    queryParameters: defaultDepositCommentValidatorStateQueryParameters(),
  });

export interface DepositCommentValidatorStateModel extends CompositionStateModel<Comment> {
}

@Injectable()
@State<DepositCommentValidatorStateModel>({
  name: StateEnum.deposit_commentValidator,
  defaults: {
    ...defaultDepositCommentValidatorStateModel(),
  },
})
export class DepositCommentValidatorState extends CompositionState<DepositCommentValidatorStateModel, Comment> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositCommentValidatorActionNameSpace,
      resourceName: ApiResourceNameEnum.COMMENTS,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublications;
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.GetAll)
  getAll(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.GetAll): Observable<CollectionTyped<any>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    const url = this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + action.parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiResourceNameEnum.COMMENTS + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameEnum.LIST_VALIDATOR_COMMENTS;
    return this._apiService.getCollection<Comment>(url, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<Comment>) => {
          ctx.dispatch(new DepositCommentValidatorAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositCommentValidatorAction.GetAllFail(action));
          throw error;
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.CreateSuccess): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.comment.notification.success.create"));
    DepositCommentValidatorState._refreshComments(ctx, action.parentId);
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.CreateFail)
  createFail(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.CreateFail): void {
    super.createFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.comment.notification.fail.create"));
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.UpdateSuccess): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.comment.notification.success.update"));
    DepositCommentValidatorState._refreshComments(ctx, action.parentId);
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.UpdateFail)
  updateFail(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.UpdateFail): void {
    super.updateFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.comment.notification.fail.update"));
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.DeleteSuccess): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.comment.notification.success.delete"));
    DepositCommentValidatorState._refreshComments(ctx, action.parentId);
  }

  @OverrideDefaultAction()
  @Action(DepositCommentValidatorAction.DeleteFail)
  deleteFail(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, action: DepositCommentValidatorAction.DeleteFail): void {
    super.deleteFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.comment.notification.fail.delete"));
  }

  private static _refreshComments(ctx: SolidifyStateContext<DepositCommentValidatorStateModel>, depositId: string): void {
    ctx.dispatch(new DepositCommentValidatorAction.GetAll(depositId, defaultDepositCommentValidatorStateQueryParameters(), true));
  }
}
