/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-comment.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositCommentAction,
  depositCommentActionNameSpace,
} from "@app/features/deposit/stores/comment/deposit-comment.action";
import {environment} from "@environments/environment";
import {Comment} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  SolidifyStateContext,
} from "solidify-frontend";

export const defaultDepositCommentStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
    field: "creation.when",
    order: OrderEnum.ascending,
  });

export const defaultDepositCommentStateModel: () => DepositCommentStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    queryParameters: defaultDepositCommentStateQueryParameters(),
  });

export interface DepositCommentStateModel extends CompositionStateModel<Comment> {
}

@Injectable()
@State<DepositCommentStateModel>({
  name: StateEnum.deposit_comment,
  defaults: {
    ...defaultDepositCommentStateModel(),
  },
})
export class DepositCommentState extends CompositionState<DepositCommentStateModel, Comment> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositCommentActionNameSpace,
      resourceName: ApiResourceNameEnum.COMMENTS,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublications;
  }

  @OverrideDefaultAction()
  @Action(DepositCommentAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<DepositCommentStateModel>, action: DepositCommentAction.CreateSuccess): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.comment.notification.success.create"));
    DepositCommentState._refreshComments(ctx, action.parentId);
  }

  @OverrideDefaultAction()
  @Action(DepositCommentAction.CreateFail)
  createFail(ctx: SolidifyStateContext<DepositCommentStateModel>, action: DepositCommentAction.CreateFail): void {
    super.createFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.comment.notification.fail.create"));
  }

  @OverrideDefaultAction()
  @Action(DepositCommentAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<DepositCommentStateModel>, action: DepositCommentAction.UpdateSuccess): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.comment.notification.success.update"));
    DepositCommentState._refreshComments(ctx, action.parentId);
  }

  @OverrideDefaultAction()
  @Action(DepositCommentAction.UpdateFail)
  updateFail(ctx: SolidifyStateContext<DepositCommentStateModel>, action: DepositCommentAction.UpdateFail): void {
    super.updateFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.comment.notification.fail.update"));
  }

  @OverrideDefaultAction()
  @Action(DepositCommentAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<DepositCommentStateModel>, action: DepositCommentAction.DeleteSuccess): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.comment.notification.success.delete"));
    DepositCommentState._refreshComments(ctx, action.parentId);
  }

  @OverrideDefaultAction()
  @Action(DepositCommentAction.DeleteFail)
  deleteFail(ctx: SolidifyStateContext<DepositCommentStateModel>, action: DepositCommentAction.DeleteFail): void {
    super.deleteFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.comment.notification.fail.delete"));
  }

  private static _refreshComments(ctx: SolidifyStateContext<DepositCommentStateModel>, depositId: string): void {
    ctx.dispatch(new DepositCommentAction.GetAll(depositId, defaultDepositCommentStateQueryParameters(), true));
  }
}
