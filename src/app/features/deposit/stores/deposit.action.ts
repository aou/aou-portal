/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositListTabEnum} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {DepositRedirectTargetEnum} from "@app/features/deposit/enums/deposit-redirect-target.enum";
import {DepositDefaultData} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositImportResultModel} from "@app/features/deposit/models/deposit-import-result.model";
import {Enums} from "@enums";
import {
  Deposit,
  DocumentFile,
  MetadataDifference,
  PublicationImportDTO,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  ErrorDto,
  MappingObject,
  ResourceAction,
  ResourceFileAction,
  ResourceFileNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.deposit;

export namespace DepositAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
    constructor(public onlyMinimalForCreate: boolean) {
      super();
    }
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<Deposit> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<Deposit> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<Deposit> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  export class UpdatePublicationSubTypeDocumentId extends BaseAction {
    static readonly type: string = `[${state}] Update PublicationSubTypeDocument ID`;

    constructor(public publicationSubTypeId: string) {
      super();
    }
  }

  export class ChangeCurrentTab extends BaseAction {
    static readonly type: string = `[${state}] Change Current Tab`;

    constructor(public tabEnum: DepositListTabEnum) {
      super();
    }
  }

  export class RefreshAllCounterStatusTab extends BaseAction {
    static readonly type: string = `[${state}] Refresh All Counter Status Tab`;
  }

  export class RefreshAllCounterStatusTabSuccess extends BaseSubActionSuccess<RefreshAllCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh All Counter Status Tab Success`;
  }

  export class RefreshAllCounterStatusTabFail extends BaseSubActionFail<RefreshAllCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh All Counter Status Tab Fail`;
  }

  export class RefreshCounterStatusTab extends BaseAction {
    static readonly type: string = `[${state}] Refresh Counter Status Tab`;

    constructor(public tabEnum: DepositListTabEnum) {
      super();
    }
  }

  export class RefreshCounterStatusTabSuccess extends BaseSubActionSuccess<RefreshCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh Counter Status Tab Success`;

    constructor(public parentAction: RefreshCounterStatusTab, public tabEnum: DepositListTabEnum, public counter: number) {
      super(parentAction);
    }
  }

  export class RefreshCounterStatusTabFail extends BaseSubActionFail<RefreshCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh Counter Status Tab Fail`;
  }

  export class SubmitForValidation extends BaseAction {
    static readonly type: string = `[${state}] Submit For Validation`;

    constructor(public depositId: string, public validationStructureId?: string | undefined, public redirectToListWhenSuccess: boolean = true) {
      super();
    }
  }

  export class SubmitForValidationSuccess extends BaseSubActionSuccess<SubmitForValidation> {
    static readonly type: string = `[${state}] Submit For Validation Success`;
  }

  export class SubmitForValidationFail extends BaseSubActionFail<SubmitForValidation> {
    static readonly type: string = `[${state}] Submit For Validation Fail`;
  }

  export class SendBackForValidation extends BaseAction {
    static readonly type: string = `[${state}] Send Back For Validation`;

    constructor(public depositId: string, public fromList: boolean = false) {
      super();
    }
  }

  export class SendBackForValidationSuccess extends BaseSubActionSuccess<SendBackForValidation> {
    static readonly type: string = `[${state}] Send Back For Validation Success`;
  }

  export class SendBackForValidationFail extends BaseSubActionFail<SendBackForValidation> {
    static readonly type: string = `[${state}] Send Back For Validation Fail`;
  }

  export class CleanAndSubmit extends BaseAction {
    static readonly type: string = `[${state}] Clean And Submit`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class CleanAndSubmitSuccess extends BaseSubActionSuccess<CleanAndSubmit> {
    static readonly type: string = `[${state}] Clean And Submit Success`;
  }

  export class CleanAndSubmitFail extends BaseSubActionFail<CleanAndSubmit> {
    static readonly type: string = `[${state}] Clean And Submit Fail`;
  }

  export class Submit extends BaseAction {
    static readonly type: string = `[${state}] Submit`;

    constructor(public depositId: string, public redirectToDetailPageWhenSuccess: DepositRedirectTargetEnum) {
      super();
    }
  }

  export class SubmitSuccess extends BaseSubActionSuccess<Submit> {
    static readonly type: string = `[${state}] Submit Success`;
  }

  export class SubmitFail extends BaseSubActionFail<Submit> {
    static readonly type: string = `[${state}] Submit Fail`;
  }

  export class Transfer extends BaseAction {
    static readonly type: string = `[${state}] Transfer`;

    constructor(public depositId: string, public validationStructureId: string) {
      super();
    }
  }

  export class TransferSuccess extends BaseSubActionSuccess<Transfer> {
    static readonly type: string = `[${state}] Transfer Success`;
  }

  export class TransferFail extends BaseSubActionFail<Transfer> {
    static readonly type: string = `[${state}] Transfer Fail`;
  }

  export class Reject extends BaseAction {
    static readonly type: string = `[${state}] Reject`;

    constructor(public depositId: string, public reason: string, public fromList: boolean = false) {
      super();
    }
  }

  export class RejectSuccess extends BaseSubActionSuccess<Reject> {
    static readonly type: string = `[${state}] Reject Success`;
  }

  export class RejectFail extends BaseSubActionFail<Reject> {
    static readonly type: string = `[${state}] Reject Fail`;
  }

  export class RejectList extends BaseAction {
    static readonly type: string = `[${state}] Reject List`;

    constructor(public listDepositId: string[], public reason: string) {
      super();
    }
  }

  export class RejectListSuccess extends BaseSubActionSuccess<RejectList> {
    static readonly type: string = `[${state}] Reject List Success`;
  }

  export class RejectListFail extends BaseSubActionFail<RejectList> {
    static readonly type: string = `[${state}] Reject List Fail`;
  }

  export class AskFeedbackList extends BaseAction {
    static readonly type: string = `[${state}] Ask Feedback List`;

    constructor(public listDepositId: string[], public reason: string) {
      super();
    }
  }

  export class AskFeedbackListSuccess extends BaseSubActionSuccess<AskFeedbackList> {
    static readonly type: string = `[${state}] Ask Feedback List Success`;
  }

  export class AskFeedbackListFail extends BaseSubActionFail<AskFeedbackList> {
    static readonly type: string = `[${state}] Ask Feedback List Fail`;
  }

  export class Resume extends BaseAction {
    static readonly type: string = `[${state}] Resume`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class ResumeSuccess extends BaseSubActionSuccess<Resume> {
    static readonly type: string = `[${state}] Resume Success`;
  }

  export class ResumeFail extends BaseSubActionFail<Resume> {
    static readonly type: string = `[${state}] Resume Fail`;
  }

  export class ExportDepositsInProgress extends BaseAction {
    static readonly type: string = `[${state}] Export Deposits In Progress`;

    constructor(public url: string, public ids: string[]) {
      super();
    }
  }

  export class ExportDepositsInProgressSuccess extends BaseSubActionSuccess<ExportDepositsInProgress> {
    static readonly type: string = `[${state}] Export Deposits In Progress Success`;

    constructor(public parentAction: ExportDepositsInProgress, public blob: Blob) {
      super(parentAction);
    }
  }

  export class ExportDepositsInProgressFail extends BaseSubActionFail<ExportDepositsInProgress> {
    static readonly type: string = `[${state}] Export Deposits In Progress Fail`;
  }

  export class EnableRevision extends BaseAction {
    static readonly type: string = `[${state}] Enable Revision`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class EnableRevisionSuccess extends BaseSubActionSuccess<EnableRevision> {
    static readonly type: string = `[${state}] Enable Revision Success`;
  }

  export class EnableRevisionFail extends BaseSubActionFail<EnableRevision> {
    static readonly type: string = `[${state}] Enable Revision Fail`;
  }

  export class AskFeedback extends BaseAction {
    static readonly type: string = `[${state}] Ask Feedback`;

    constructor(public depositId: string, public reason: string, public fromList: boolean = false) {
      super();
    }
  }

  export class AskFeedbackSuccess extends BaseSubActionSuccess<AskFeedback> {
    static readonly type: string = `[${state}] Ask Feedback Success`;
  }

  export class AskFeedbackFail extends BaseSubActionFail<AskFeedback> {
    static readonly type: string = `[${state}] Ask Feedback Fail`;
  }

  export class GetMyDefaultProfileData extends BaseAction {
    static readonly type: string = `[${state}] Get My Default Profile Data`;
  }

  export class GetMyDefaultProfileDataSuccess extends BaseSubActionSuccess<GetMyDefaultProfileData> {
    static readonly type: string = `[${state}] Get My Default Profile Data Success`;

    constructor(public parentAction: GetMyDefaultProfileData, public defaultData: DepositDefaultData) {
      super(parentAction);
    }
  }

  export class GetMyDefaultProfileDataFail extends BaseSubActionFail<GetMyDefaultProfileData> {
    static readonly type: string = `[${state}] Get My Default Profile Data Fail`;
  }

  export class NotMyDeposit extends BaseAction {
    static readonly type: string = `[${state}] Not My Deposit`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class NotMyDepositSuccess extends BaseSubActionSuccess<NotMyDeposit> {
    static readonly type: string = `[${state}] Not My Deposit Success`;
  }

  export class NotMyDepositFail extends BaseSubActionFail<NotMyDeposit> {
    static readonly type: string = `[${state}] Not My Deposit Fail`;
  }

  export class DisplayDialogFileImportedAutomatically extends BaseAction {
    static readonly type: string = `[${state}] Display Dialog File Imported Automatically`;
  }

  export class DisplayDialogContributorsPartiallyImported extends BaseAction {
    static readonly type: string = `[${state}] Display Dialog Contributors Partially Imported`;
  }

  export class DisplayDialogUnableToImportFile extends BaseAction {
    static readonly type: string = `[${state}] Display Dialog Unable To Import File`;

    constructor(public listFileUnableToImport: DocumentFile[]) {
      super();
    }
  }

  export class UpdateResearchGroup extends BaseAction {
    static readonly type: string = `[${state}] Update Research Group`;

    constructor(public listDepositId: string[], public researchGroupId: string) {
      super();
    }
  }

  export class UpdateResearchGroupSuccess extends BaseSubActionSuccess<UpdateResearchGroup> {
    static readonly type: string = `[${state}] Update Research Group Success`;
  }

  export class UpdateResearchGroupFail extends BaseSubActionFail<UpdateResearchGroup> {
    static readonly type: string = `[${state}] Update Research Group Fail`;
  }

  export class UpdateStructure extends BaseAction {
    static readonly type: string = `[${state}] Structure`;

    constructor(public listDepositId: string[], public structureId: string) {
      super();
    }
  }

  export class UpdateStructureSuccess extends BaseSubActionSuccess<UpdateStructure> {
    static readonly type: string = `[${state}] Structure Success`;
  }

  export class UpdateStructureFail extends BaseSubActionFail<UpdateStructure> {
    static readonly type: string = `[${state}] Structure Fail`;
  }

  export class UpdateLanguage extends BaseAction {
    static readonly type: string = `[${state}] Language`;

    constructor(public listDepositId: string[], public languageId: string) {
      super();
    }
  }

  export class UpdateLanguageSuccess extends BaseSubActionSuccess<UpdateLanguage> {
    static readonly type: string = `[${state}] Language Success`;
  }

  export class UpdateLanguageFail extends BaseSubActionFail<UpdateLanguage> {
    static readonly type: string = `[${state}] Language Fail`;
  }

  export class CreateBulkFromIdentifier extends BaseAction {
    static readonly type: string = `[${state}] Create Bulk From Identifier`;

    constructor(public listPublicationImport: PublicationImportDTO[]) {
      super();
    }
  }

  export class CreateBulkFromIdentifierSuccess extends BaseSubActionSuccess<CreateBulkFromIdentifier> {
    static readonly type: string = `[${state}] Create Bulk From Identifier Success`;
  }

  export class CreateBulkFromIdentifierFail extends BaseSubActionFail<CreateBulkFromIdentifier> {
    static readonly type: string = `[${state}] Create Bulk From Identifier Fail`;
  }

  export class CreateFromIdentifier extends BaseAction {
    static readonly type: string = `[${state}] Create From Identifier`;

    constructor(public publicationImportDTO: PublicationImportDTO) {
      super();
    }
  }

  export class CreateFromIdentifierSuccess extends BaseSubActionSuccess<CreateFromIdentifier> {
    static readonly type: string = `[${state}] Create From Identifier Success`;

    constructor(public parentAction: CreateFromIdentifier, public result: DepositImportResultModel) {
      super(parentAction);
    }
  }

  export class CreateFromIdentifierFail extends BaseSubActionFail<CreateFromIdentifier> {
    static readonly type: string = `[${state}] Create From Identifier Fail`;

    constructor(public parentAction: CreateFromIdentifier, public result: DepositImportResultModel) {
      super(parentAction);
    }
  }

  export class ValidateMetadata extends BaseAction {
    static readonly type: string = `[${state}] Validate Metadata`;

    constructor(public depositId: string, public isForSubmit: boolean) {
      super();
    }
  }

  export class ValidateMetadataSuccess extends BaseSubActionSuccess<ValidateMetadata> {
    static readonly type: string = `[${state}] Validate Metadata Success`;

    constructor(public parentAction: ValidateMetadata) {
      super(parentAction);
    }
  }

  export class ValidateMetadataFail extends BaseSubActionFail<ValidateMetadata> {
    static readonly type: string = `[${state}] Validate Metadata Fail`;

    constructor(public parentAction: ValidateMetadata, public error: ErrorDto) {
      super(parentAction);
    }
  }

  export class CheckDuplicates extends BaseAction {
    static readonly type: string = `[${state}] Check Duplicates`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class CheckDuplicatesSuccess extends BaseSubActionSuccess<CheckDuplicates> {
    static readonly type: string = `[${state}] Check Duplicates Success`;
  }

  export class CheckDuplicatesFail extends BaseSubActionFail<CheckDuplicates> {
    static readonly type: string = `[${state}] Check Duplicates Fail`;

    constructor(public parentAction: CheckDuplicates, public error: ErrorDto) {
      super(parentAction);
    }
  }

  export class ListPublicationUserRoles extends BaseAction {
    static readonly type: string = `[${state}] List Publication User Roles`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class ListPublicationUserRolesSuccess extends BaseSubActionSuccess<ListPublicationUserRoles> {
    static readonly type: string = `[${state}] List Publication User Roles Success`;

    constructor(public parentAction: ListPublicationUserRoles, public listUserRoleEnum: Enums.Deposit.UserRoleEnum[]) {
      super(parentAction);
    }
  }

  export class ListPublicationUserRolesFail extends BaseSubActionFail<ListPublicationUserRoles> {
    static readonly type: string = `[${state}] List Publication User Roles Fail`;
  }

  export class Clone extends BaseAction {
    static readonly type: string = `[${state}] Clone`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class CloneSuccess extends BaseSubActionSuccess<Clone> {
    static readonly type: string = `[${state}] Clone Success`;

    constructor(public parentAction: Clone, public deposit: Deposit) {
      super(parentAction);
    }
  }

  export class CloneFail extends BaseSubActionFail<Clone> {
    static readonly type: string = `[${state}] Clone Fail`;
  }

  export class ChangeToCanonical extends BaseAction {
    static readonly type: string = `[${state}] Change To Canonical`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class ChangeToCanonicalSuccess extends BaseSubActionSuccess<ChangeToCanonical> {
    static readonly type: string = `[${state}] Change To Canonical Success`;

    constructor(public parentAction: ChangeToCanonical, public deposit: Deposit) {
      super(parentAction);
    }
  }

  export class ChangeToCanonicalFail extends BaseSubActionFail<ChangeToCanonical> {
    static readonly type: string = `[${state}] Change To Canonical Fail`;
  }

  export class ChangeToCompleted extends BaseAction {
    static readonly type: string = `[${state}] Change To Completed`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class ChangeToCompletedSuccess extends BaseSubActionSuccess<ChangeToCompleted> {
    static readonly type: string = `[${state}] Change To Completed Success`;

    constructor(public parentAction: ChangeToCompleted, public deposit: Deposit) {
      super(parentAction);
    }
  }

  export class ChangeToCompletedFail extends BaseSubActionFail<ChangeToCompleted> {
    static readonly type: string = `[${state}] Change To Completed Fail`;
  }

  export class ChangeToDeleted extends BaseAction {
    static readonly type: string = `[${state}] Change To Deleted`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class ChangeToDeletedSuccess extends BaseSubActionSuccess<ChangeToDeleted> {
    static readonly type: string = `[${state}] Change To Deleted Success`;

    constructor(public parentAction: ChangeToDeleted, public deposit: Deposit) {
      super(parentAction);
    }
  }

  export class ChangeToDeletedFail extends BaseSubActionFail<ChangeToDeleted> {
    static readonly type: string = `[${state}] Change To Deleted Fail`;
  }

  export class StartEditingPublication extends BaseAction {
    static readonly type: string = `[${state}] Start Editing publication`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class StartEditingPublicationSuccess extends BaseSubActionSuccess<StartEditingPublication> {
    static readonly type: string = `[${state}] Start Editing publication Success`;
  }

  export class StartEditingPublicationFail extends BaseSubActionFail<StartEditingPublication> {
    static readonly type: string = `[${state}] Start Editing publication Fail`;
  }

  export class CancelEditingPublication extends BaseAction {
    static readonly type: string = `[${state}] Cancel Editing Publication`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class CancelEditingPublicationSuccess extends BaseSubActionSuccess<CancelEditingPublication> {
    static readonly type: string = `[${state}] Cancel Editing Publication Success`;
  }

  export class CancelEditingPublicationFail extends BaseSubActionFail<CancelEditingPublication> {
    static readonly type: string = `[${state}] Cancel Editing Publication Fail`;
  }

  export class CheckMetadataDifferences extends BaseAction {
    static readonly type: string = `[${state}] Check Metadata Differences`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class CheckMetadataDifferencesSuccess extends BaseSubActionSuccess<CheckMetadataDifferences> {
    static readonly type: string = `[${state}] Check Metadata Differences Success`;

    constructor(public parentAction: CheckMetadataDifferences, public differences?: MappingObject<string, MappingObject<string, MetadataDifference[]>> | null | undefined) {
      super(parentAction);
    }
  }

  export class CheckMetadataDifferencesFail extends BaseSubActionFail<CheckMetadataDifferences> {
    static readonly type: string = `[${state}] Check Metadata Differences Fail`;

    constructor(public parentAction: CheckMetadataDifferences) {
      super(parentAction);
    }
  }

}

export const depositActionNameSpace: ResourceFileNameSpace = DepositAction;
