/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-person-research-group.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositPersonResearchGroupAction,
  depositPersonResearchGroupActionNameSpace,
} from "@app/features/deposit/stores/person/research-group/deposit-person-research-group.action";
import {environment} from "@environments/environment";
import {ResearchGroup} from "@models";
import {
  Action,
  Actions,
  ofActionCompleted,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  ArrayUtil,
  AssociationState,
  AssociationStateModel,
  defaultAssociationStateInitValue,
  isEmptyArray,
  isNullOrUndefined,
  NotificationService,
  OverrideDefaultAction,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface DepositPersonResearchGroupStateModel extends AssociationStateModel<ResearchGroup> {
}

@Injectable()
@State<DepositPersonResearchGroupStateModel>({
  name: StateEnum.deposit_person_researchGroup,
  defaults: {
    ...defaultAssociationStateInitValue(),
  },
})
export class DepositPersonResearchGroupState extends AssociationState<DepositPersonResearchGroupStateModel, ResearchGroup> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositPersonResearchGroupActionNameSpace,
      resourceName: ApiResourceNameEnum.RESEARCH_GROUPS,
      apiPathGetAll: parentId => this._urlResource + urlSeparator + ApiActionNameEnum.BY_EXTERNAL_UID,
      apiCustomParamGetAll: parentId => ({["external-uid"]: parentId}),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminResearchGroups;
  }

  @Action(DepositPersonResearchGroupAction.RetrieveAllByExternalUid)
  retrieveAllByExternalUid(ctx: SolidifyStateContext<DepositPersonResearchGroupStateModel>, action: DepositPersonResearchGroupAction.RetrieveAllByExternalUid): Observable<boolean> {

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      selected: [],
    });

    if (isNullOrUndefined(action.listExternalUid) || isEmptyArray(action.listExternalUid)) {
      return ctx.dispatch(new DepositPersonResearchGroupAction.RetrieveAllByExternalUidSuccess(action, [])).pipe(map(result => true));
    }

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = [];

    action.listExternalUid.forEach(externalUid => {
      actionSubActionCompletionsWrappers.push({
        action: new DepositPersonResearchGroupAction.GetAll(externalUid, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofActionCompleted(DepositPersonResearchGroupAction.GetAllSuccess)),
          this._actions$.pipe(ofActionCompleted(DepositPersonResearchGroupAction.GetAllFail)),
        ],
      });
    });

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(
      ctx,
      actionSubActionCompletionsWrappers,
    ).pipe(
      map(result => {
        if (result.success) {
          const list = ArrayUtil.distinct(ctx.getState().selected, "resId");
          ctx.dispatch(new DepositPersonResearchGroupAction.RetrieveAllByExternalUidSuccess(action, list));
        } else {
          ctx.dispatch(new DepositPersonResearchGroupAction.RetrieveAllByExternalUidFail(action));
        }
        return result.success;
      }),
    );
  }

  @OverrideDefaultAction()
  @Action(DepositPersonResearchGroupAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<DepositPersonResearchGroupStateModel>, action: DepositPersonResearchGroupAction.GetAllSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      selected: [...ctx.getState().selected, ...action.list._data],
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @Action(DepositPersonResearchGroupAction.RetrieveAllByExternalUidSuccess)
  retrieveAllByExternalUidSuccess(ctx: SolidifyStateContext<DepositPersonResearchGroupStateModel>, action: DepositPersonResearchGroupAction.RetrieveAllByExternalUidSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositPersonResearchGroupAction.RetrieveAllByExternalUidFail)
  retrieveAllByExternalUidFail(ctx: SolidifyStateContext<DepositPersonResearchGroupStateModel>, action: DepositPersonResearchGroupAction.RetrieveAllByExternalUidFail): void {
    this._notificationService.showError(LabelTranslateEnum.unableToApplyContributorResearchGroups, undefined);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
