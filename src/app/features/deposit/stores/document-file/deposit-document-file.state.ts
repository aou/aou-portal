/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-document-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositDocumentFileAction,
  depositDocumentFileActionNameSpace,
} from "@app/features/deposit/stores/document-file/deposit-document-file.action";
import {
  DepositDocumentFileStatusHistoryState,
  DepositDocumentFileStatusHistoryStateModel,
} from "@app/features/deposit/stores/document-file/status-history/deposit-document-file-status-history.state";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {AouUploadFileStatus} from "@deposit/models/aou-upload-file-status.model";
import {
  DepositDocumentFileUploadState,
  DepositDocumentFileUploadStateModel,
} from "@deposit/stores/document-file/upload/deposit-document-file-upload.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {DocumentFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  defaultStatusHistoryInitValue,
  defaultUploadStateInitValue,
  DownloadService,
  isNotNullNorUndefined,
  MappingObject,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  Result,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultDepositDocumentFileStateQueryParameters: () => QueryParameters = () =>
  new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
    field: "sortValue",
    order: OrderEnum.ascending,
  });

export const defaultDepositDocumentFileValue: () => DepositDocumentFileStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    [StateEnum.deposit_documentFile_statusHistory]: defaultStatusHistoryInitValue(),
    [StateEnum.deposit_documentFile_upload]: defaultUploadStateInitValue(),
    uploadStatus: [],
    listCurrentStatus: {} as MappingObject<Enums.DocumentFile.StatusEnum, number>,
    isLoadingCurrentStatus: 0,
    queryParameters: defaultDepositDocumentFileStateQueryParameters(),
  });

export interface DepositDocumentFileStateModel extends CompositionStateModel<DocumentFile> {
  [StateEnum.deposit_documentFile_statusHistory]: DepositDocumentFileStatusHistoryStateModel;
  [StateEnum.deposit_documentFile_upload]: DepositDocumentFileUploadStateModel;
  uploadStatus: AouUploadFileStatus[];
  listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>;
  isLoadingCurrentStatus: number;
}

@Injectable()
@State<DepositDocumentFileStateModel>({
  name: StateEnum.deposit_documentFile,
  defaults: {
    ...defaultDepositDocumentFileValue(),
  },
  children: [
    DepositDocumentFileUploadState,
    DepositDocumentFileStatusHistoryState,
  ],
})
export class DepositDocumentFileState extends CompositionState<DepositDocumentFileStateModel, DocumentFile> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositDocumentFileActionNameSpace,
      resourceName: ApiResourceNameEnum.DOCUMENT_FILES,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublications;
  }

  @Action(DepositDocumentFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.Refresh): Observable<boolean> {
    this._incrementLoadingCounter(ctx);

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new DepositDocumentFileAction.GetAll(action.parentId, defaultDepositDocumentFileStateQueryParameters(), true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDocumentFileAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDocumentFileAction.GetAllFail)),
        ],
      },
      {
        action: new DepositDocumentFileAction.GetListCurrentStatus(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDocumentFileAction.GetListCurrentStatusSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDocumentFileAction.GetListCurrentStatusFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositDocumentFileAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new DepositDocumentFileAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositDocumentFileAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.RefreshSuccess): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(DepositDocumentFileAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.RefreshFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @OverrideDefaultAction()
  @Action(DepositDocumentFileAction.Delete)
  override delete(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.Delete): Observable<string | any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.deleteById<string>(url, action.resId)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositDocumentFileAction.DeleteSuccess(action, action.parentId));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDocumentFileAction.DeleteFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(DepositDocumentFileAction.DeleteSuccess)
  override deleteSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.DeleteSuccess): void {
    this._decrementLoadingCounter(ctx);
    ctx.dispatch(new DepositDocumentFileAction.Refresh(action.parentId));
  }

  @OverrideDefaultAction()
  @Action(DepositDocumentFileAction.DeleteFail)
  override deleteFail(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.DeleteFail): void {
    this._decrementLoadingCounter(ctx);
    if (action?.result === "UNAUTHORIZED") {
      this._notificationService.showWarning(LabelTranslateEnum.notificationYouAreNotAllowedToDeleteThisFile);
    }
  }

  @Action(DepositDocumentFileAction.DeleteAll)
  deleteAll(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.DeleteAll): Observable<string[]> {
    this._incrementLoadingCounter(ctx);

    return this._apiService.delete<string[]>(`${this._urlResource}/${action.parentId}/${this._resourceName}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new DepositDocumentFileAction.DeleteAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDocumentFileAction.DeleteAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDocumentFileAction.DeleteAllSuccess)
  deleteAllSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.DeleteAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new DepositDocumentFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(DepositDocumentFileAction.DeleteAllFail)
  deleteAllFail(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.DeleteAllFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @OverrideDefaultAction()
  @Action(DepositDocumentFileAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.UpdateSuccess): void {
    this._decrementLoadingCounter(ctx);
    ctx.dispatch(new DepositDocumentFileAction.Refresh(action.parentId));
  }

  @Action(DepositDocumentFileAction.ConfirmDocumentFile)
  confirm(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.ConfirmDocumentFile): Observable<Result> {
    this._incrementLoadingCounter(ctx);

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.parentId + urlSeparator + ApiResourceNameEnum.DOCUMENT_FILES + urlSeparator + action.resId + urlSeparator + ApiActionNameEnum.CONFIRM)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositDocumentFileAction.ConfirmDocumentFileSuccess(action));
          } else {
            ctx.dispatch(new DepositDocumentFileAction.ConfirmDocumentFileFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDocumentFileAction.ConfirmDocumentFileFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDocumentFileAction.ConfirmDocumentFileSuccess)
  confirmDocumentFileSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.ConfirmDocumentFileSuccess): void {
    this._decrementLoadingCounter(ctx);
    ctx.dispatch(new DepositDocumentFileAction.Refresh(action.parentAction.parentId));
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.documentFile.notification.confirm.success"));
  }

  @Action(DepositDocumentFileAction.ConfirmDocumentFileFail)
  resumeFail(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.ConfirmDocumentFileFail): void {
    this._decrementLoadingCounter(ctx);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.documentFile.notification.confirm.fail"));
  }

  @Action(DepositDocumentFileAction.Download)
  download(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.Download): void {
    const url = `${this._urlResource}/${action.parentId}/${ApiResourceNameEnum.DOCUMENT_FILES}/${action.documentFile.resId}/${ApiActionNameEnum.DOWNLOAD}`;
    this._downloadService.download(url, action.documentFile.fileName, action.documentFile.fileSize, false);
  }

  @Action(DepositDocumentFileAction.GetListCurrentStatus)
  getListCurrentStatus(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.GetListCurrentStatus): Observable<MappingObject<Enums.DocumentFile.StatusEnum, number>> {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus + 1,
    });

    return this._apiService.get<MappingObject<Enums.DocumentFile.StatusEnum, number>>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.LIST_CURRENT_STATUS}`)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositDocumentFileAction.GetListCurrentStatusSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDocumentFileAction.GetListCurrentStatusFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDocumentFileAction.GetListCurrentStatusSuccess)
  getListCurrentStatusSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.GetListCurrentStatusSuccess): void {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus - 1,
      listCurrentStatus: action.listCurrentStatus,
    });
  }

  @Action(DepositDocumentFileAction.GetListCurrentStatusFail)
  getListCurrentStatusFail(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.GetListCurrentStatusFail): void {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus - 1,
    });
  }

  @Action(DepositDocumentFileAction.SendSortOrderDocumentFiles)
  sendSortOrderFiles(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.SendSortOrderDocumentFiles): Observable<string[]> {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus + 1,
    });
    return this._apiService.post<string[]>(`${this._urlResource}/${action.parentId}/${ApiResourceNameEnum.DOCUMENT_FILES}/${ApiActionNameEnum.UPDATE_SORT_VALUES}`, action.documentIds)
      .pipe(
        tap(() => {
          ctx.dispatch(new DepositDocumentFileAction.SendSortOrderDocumentFilesSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDocumentFileAction.SendSortOrderDocumentFilesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDocumentFileAction.SendSortOrderDocumentFilesSuccess)
  sendSortOrderFilesSuccess(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.SendSortOrderDocumentFilesSuccess): void {
    const list = [];
    action.parentAction.documentIds?.forEach(id => {
      const file = ctx.getState().list?.find(c => c.resId === id);
      if (isNotNullNorUndefined(file)) {
        list.push(file);
      }
    });

    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus - 1,
      list: list,
    });
  }

  @Action(DepositDocumentFileAction.SendSortOrderDocumentFilesFail)
  sendSortOrderFilesFail(ctx: SolidifyStateContext<DepositDocumentFileStateModel>, action: DepositDocumentFileAction.SendSortOrderDocumentFilesFail): void {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus - 1,
    });
  }
}
