/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-document-file-upload.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {AouFileUploadWrapper} from "@deposit/models/aou-file-upload-wrapper.model";
import {DepositDocumentFileAction} from "@deposit/stores/document-file/deposit-document-file.action";
import {
  DepositDocumentFileUploadAction,
  depositDocumentFileUploadActionNameSpace,
} from "@deposit/stores/document-file/upload/deposit-document-file-upload.action";
import {AouEnvironment} from "@environments/environment.defaults.model";
import {
  DocumentFile,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ErrorBackendKeyEnum} from "@shared/enums/error-backend-key.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AppSystemPropertyState,
  defaultUploadStateInitValue,
  ENVIRONMENT,
  isNonWhiteString,
  isNotNullNorUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  OverrideDefaultAction,
  SolidifyStateContext,
  UploadAction,
  UploadState,
  UploadStateModel,
} from "solidify-frontend";

export interface DepositDocumentFileUploadStateModel extends UploadStateModel<DocumentFile, AouFileUploadWrapper> {
}

@Injectable()
@State<DepositDocumentFileUploadStateModel>({
  name: StateEnum.deposit_documentFile_upload,
  defaults: {
    ...defaultUploadStateInitValue(),
  },
  children: [],
})
export class DepositDocumentFileUploadState extends UploadState<DepositDocumentFileUploadStateModel, DocumentFile> {
  private readonly _FILE_KEY: string = "file";
  private readonly _DOCUMENT_FILE_TYPE_ID: string = "documentFileTypeId";
  private readonly _ACCESS_LEVEL: string = "accessLevel";
  private readonly _LABEL: string = "label";
  private readonly _EMBARGO_ACCESS_LEVEL: string = "embargoAccessLevel";
  private readonly _EMBARGO_END_DATE: string = "embargoEndDate";
  private readonly _LICENSE_ID: string = "licenseId";
  private readonly _NOT_SURE_FOR_LICENSE: string = "notSureForLicense";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: AouEnvironment,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _translate: TranslateService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: depositDocumentFileUploadActionNameSpace,
      customUploadUrlSuffix: action => `${(action.fileUploadWrapper as AouFileUploadWrapper).depositId}/${ApiResourceNameEnum.DOCUMENT_FILES}/${ApiActionNameEnum.UPLOAD}`,
      callbackAfterAllUploadFinished: action => _store.dispatch(new DepositDocumentFileAction.Refresh(action.parentId)),
      fileSizeLimit: () => +MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current.fileSizeLimit),
    }, _environment, _labelTranslateInterface, _translate);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublications;
  }

  protected override _generateUploadFormData(action: DepositDocumentFileUploadAction.UploadFile): FormData {
    const formData = new FormData();
    const fileUploadWrapper = action.fileUploadWrapper as AouFileUploadWrapper;

    formData.append(this._FILE_KEY, fileUploadWrapper.file, fileUploadWrapper.file.name);
    formData.append(this._DOCUMENT_FILE_TYPE_ID, fileUploadWrapper.documentFile.documentFileType.resId);
    if (isNotNullNorUndefined(fileUploadWrapper.documentFile.label) && isNonWhiteString(fileUploadWrapper.documentFile.label)) {
      formData.append(this._LABEL, fileUploadWrapper.documentFile.label);
    }
    formData.append(this._ACCESS_LEVEL, fileUploadWrapper.documentFile.accessLevel);
    if (isNotNullNorUndefined(fileUploadWrapper.documentFile.embargoAccessLevel) && isNotNullNorUndefined(fileUploadWrapper.documentFile.embargoEndDate)) {
      formData.append(this._EMBARGO_ACCESS_LEVEL, fileUploadWrapper.documentFile.embargoAccessLevel);
      formData.append(this._EMBARGO_END_DATE, fileUploadWrapper.documentFile.embargoEndDate);
    }
    if (isNotNullNorUndefined(fileUploadWrapper.documentFile.license)) {
      formData.append(this._LICENSE_ID, fileUploadWrapper.documentFile.license.resId);
    }
    formData.append(this._NOT_SURE_FOR_LICENSE, fileUploadWrapper.documentFile.notSureForLicense + "");
    return formData;
  }

  @OverrideDefaultAction()
  @Action(DepositDocumentFileUploadAction.UploadFileFail)
  override uploadFileFail(ctx: SolidifyStateContext<DepositDocumentFileUploadStateModel>, action: UploadAction.UploadFileFail<DocumentFile>): void {
    let errorMessage = MARK_AS_TRANSLATABLE("deposit.file.upload.notification.error");
    if (action.errorDto.message === ErrorBackendKeyEnum.UPLOAD_DUPLICATE_DATA_FILES) {
      errorMessage = MARK_AS_TRANSLATABLE("error.upload.duplicateDocumentFiles");
    }
    this._notificationService.showError(errorMessage);
    return super.uploadFileFail(ctx, action);
  }
}
