/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-document-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {DocumentFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CompositionAction,
  CompositionNameSpace,
  MappingObject,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.deposit_documentFile;

export namespace DepositDocumentFileAction {
  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<DocumentFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
    constructor(public parentAction: Delete, public result?: string) {
      super(parentAction);
    }
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  export class ChangeCurrentFolder {
    static readonly type: string = `[${state}] Change Current Folder`;

    constructor(public newCurrentFolder: string, public refreshNewFolderContent: boolean = false) {
    }
  }

  export class ChangeCurrentCategory {
    static readonly type: string = `[${state}] Change Current Category`;

    constructor(public newCategory: string, public newType: string, public refreshNewCategory: boolean = false) {
    }
  }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public parentId: string, public documentFile: DocumentFile) {
    }
  }

  export class DeleteAll extends BaseAction {
    static readonly type: string = `[${state}] Delete All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class DeleteAllSuccess extends BaseSubActionSuccess<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Success`;
  }

  export class DeleteAllFail extends BaseSubActionFail<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Fail`;
  }

  // export class ResumeAll extends BaseAction {
  //   static readonly type: string = `[${state}] Resume All`;
  //
  //   constructor(public parentId: string, public listResId: string[]) {
  //     super();
  //   }
  // }
  //
  // export class ResumeAllSuccess extends BaseSubActionSuccess<ResumeAll> {
  //   static readonly type: string = `[${state}] Resume All Success`;
  // }
  //
  // export class ResumeAllFail extends BaseSubActionFail<ResumeAll> {
  //   static readonly type: string = `[${state}] Resume All Fail`;
  // }

  export class ConfirmDocumentFile extends BaseAction {
    static readonly type: string = `[${state}] Confirm Document File`;

    constructor(public parentId: string, public resId: string) {
      super();
    }
  }

  export class ConfirmDocumentFileSuccess extends BaseSubActionSuccess<ConfirmDocumentFile> {
    static readonly type: string = `[${state}] Confirm Document File Success`;
  }

  export class ConfirmDocumentFileFail extends BaseSubActionFail<ConfirmDocumentFile> {
    static readonly type: string = `[${state}] Confirm Document File Fail`;
  }

  export class GetListCurrentStatus extends BaseAction {
    static readonly type: string = `[${state}] Get List Current Status`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class GetListCurrentStatusSuccess extends BaseSubActionSuccess<GetListCurrentStatus> {
    static readonly type: string = `[${state}] Get List Current Status Folder Success`;

    constructor(public parentAction: GetListCurrentStatus, public listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>) {
      super(parentAction);
    }
  }

  export class GetListCurrentStatusFail extends BaseSubActionFail<GetListCurrentStatus> {
    static readonly type: string = `[${state}] Get List Current Status Folder Fail`;
  }

  export class SendSortOrderDocumentFiles extends BaseAction {
    static readonly type: string = `[${state}] Send Sort Order Document Files`;

    constructor(public parentId: string, public documentIds: string[]) {
      super();
    }
  }

  export class SendSortOrderDocumentFilesSuccess extends BaseSubActionSuccess<SendSortOrderDocumentFiles> {
    static readonly type: string = `[${state}] Send Sort Order Document Files Success`;
  }

  export class SendSortOrderDocumentFilesFail extends BaseSubActionFail<SendSortOrderDocumentFiles> {
    static readonly type: string = `[${state}] Send Sort Order Document Files Fail`;
  }
}

export const depositDocumentFileActionNameSpace: CompositionNameSpace = DepositDocumentFileAction;
