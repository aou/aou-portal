/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {DepositDocumentFileContainer} from "@app/features/deposit/components/containers/deposit-document-file/deposit-document-file.container";
import {DepositTableContributorContainer} from "@app/features/deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {DepositBulkResearchGroupDialog} from "@app/features/deposit/components/dialogs/deposit-bulk-research-group/deposit-bulk-research-group.dialog";
import {DepositBulkStructureDialog} from "@app/features/deposit/components/dialogs/deposit-bulk-structure/deposit-bulk-structure.dialog";
import {DepositCommentDialog} from "@app/features/deposit/components/dialogs/deposit-comment/deposit-comment.dialog";
import {DepositCreateFundingDialog} from "@app/features/deposit/components/dialogs/deposit-create-funding/deposit-create-funding.dialog";
import {DepositCreateResearchGroupAlternativeDialog} from "@app/features/deposit/components/dialogs/deposit-create-research-group-alternative/deposit-create-research-group-alternative.dialog";
import {DepositCreateResearchGroupDialog} from "@app/features/deposit/components/dialogs/deposit-create-research-group/deposit-create-research-group.dialog";
import {DepositDocumentFileUploadDialog} from "@app/features/deposit/components/dialogs/deposit-document-file-upload/deposit-document-file-upload.dialog";
import {DepositMultipleImportTypeDialog} from "@app/features/deposit/components/dialogs/deposit-multiple-import-type/deposit-multiple-import-type.dialog";
import {DepositValidateResearchGroupDialog} from "@app/features/deposit/components/dialogs/deposit-validate-research-group/deposit-validate-research-group.dialog";
import {DepositValidationStructureDialog} from "@app/features/deposit/components/dialogs/deposit-validation-structure/deposit-validation-structure.dialog";
import {DepositButtonApplyDefaultValuePresentational} from "@app/features/deposit/components/presentationals/deposit-button-apply-default-value/deposit-button-apply-default-value.presentational";
import {DepositCommentFormPresentational} from "@app/features/deposit/components/presentationals/deposit-comment-form/deposit-comment-form.presentational";
import {DepositContainerPresentational} from "@app/features/deposit/components/presentationals/deposit-container/deposit-container.presentational";
import {DepositFormDescriptionClassificationPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-classification/deposit-form-description-classification.presentational";
import {DepositFormDescriptionCollectionPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-collection/deposit-form-description-collection.presentational";
import {DepositFormDescriptionContainerPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-container/deposit-form-description-container.presentational";
import {DepositFormDescriptionCorrectionPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-correction/deposit-form-description-correction.presentational";
import {DepositFormDescriptionDatasetPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-dataset/deposit-form-description-dataset.presentational";
import {DepositFormDescriptionDatePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-date/deposit-form-description-date.presentational";
import {DepositFormDescriptionFundingPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-funding/deposit-form-description-funding.presentational";
import {DepositFormDescriptionLanguagePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-language/deposit-form-description-language.presentational";
import {DepositFormDescriptionLinkPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-link/deposit-form-description-link.presentational";
import {DepositFormDescriptionPagePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-page/deposit-form-description-page.presentational";
import {DepositFormDescriptionPublicationIdentifierPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-publication-identifier/deposit-form-description-publication-identifier.presentational";
import {DepositFormDescriptionPublisherPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-publisher/deposit-form-description-publisher.presentational";
import {DepositFormDescriptionTextLanguageListPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-text-language-list/deposit-form-description-text-language-list.presentational";
import {DepositFormDescriptionTextLanguagePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-text-language/deposit-form-description-text-language.presentational";
import {DepositFormFirstStepTypePresentational} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {DepositFormFourthStepDescriptionPresentational} from "@app/features/deposit/components/presentationals/deposit-form-fourth-step-description/deposit-form-fourth-step-description.presentational";
import {DepositFormRomeoPresentational} from "@app/features/deposit/components/presentationals/deposit-form-romeo/deposit-form-romeo.presentational";
import {DepositFormSecondStepFilesPresentational} from "@app/features/deposit/components/presentationals/deposit-form-second-step-files/deposit-form-second-step-files.presentational";
import {DepositFormThirdStepContributorsPresentational} from "@app/features/deposit/components/presentationals/deposit-form-third-step-contributors/deposit-form-third-step-contributors.presentational";
import {DepositFormTypeSubtypePresentational} from "@app/features/deposit/components/presentationals/deposit-form-type-subtype/deposit-form-type-subtype.presentational";
import {DepositFormPresentational} from "@app/features/deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositImportRowPresentational} from "@app/features/deposit/components/presentationals/deposit-import-row/deposit-import-row.presentational";
import {DepositMultipleImportPresentational} from "@app/features/deposit/components/presentationals/deposit-multiple-import/deposit-multiple-import.presentational";
import {DepositRepeatableFieldsPresentational} from "@app/features/deposit/components/presentationals/deposit-repeatable-fields/deposit-repeatable-fields.presentational";
import {DepositSummaryPresentational} from "@app/features/deposit/components/presentationals/deposit-summary/deposit-summary.presentational";
import {DepositValidationPanelPresentational} from "@app/features/deposit/components/presentationals/deposit-validation-panel/deposit-validation-panel.presentational";
import {DepositCreateEditRoutable} from "@app/features/deposit/components/routables/deposit-create-edit/deposit-create-edit.routable";
import {DepositDetailRoutable} from "@app/features/deposit/components/routables/deposit-detail/deposit-detail.routable";
import {DepositListRoutable} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {DepositMultipleImportRoutable} from "@app/features/deposit/components/routables/deposit-multiple-import/deposit-multiple-import.routable";
import {DepositRoutingModule} from "@app/features/deposit/deposit-routing.module";
import {DepositCommentState} from "@app/features/deposit/stores/comment/deposit-comment.state";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {DepositDocumentFileState} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {DepositDocumentFileStatusHistoryState} from "@app/features/deposit/stores/document-file/status-history/deposit-document-file-status-history.state";
import {DepositPersonResearchGroupState} from "@app/features/deposit/stores/person/research-group/deposit-person-research-group.state";
import {DepositPersonStructureState} from "@app/features/deposit/stores/person/structure/deposit-person-structure.state";
import {DepositResearchGroupState} from "@app/features/deposit/stores/research-group/deposit-research-group.state";
import {DepositStatusHistoryState} from "@app/features/deposit/stores/status-history/deposit-status-history.state";
import {DepositStructureState} from "@app/features/deposit/stores/structure/deposit-structure.state";
import {DepositCommentsContainer} from "@deposit/components/containers/deposit-comments/deposit-comments.container";
import {DepositTableContributorSearchContainer} from "@deposit/components/containers/deposit-table-contributor-search/deposit-table-contributor-search.container";
import {DepositBulkLanguageDialog} from "@deposit/components/dialogs/deposit-bulk-language/deposit-bulk-language.dialog";
import {DepositUnableImportFileDialog} from "@deposit/components/dialogs/deposit-unable-import-file/deposit-unable-import-file.dialog";
import {DepositMetadataDifferencesPanelPresentational} from "@deposit/components/presentationals/deposit-metadata-differences-panel/deposit-metadata-differences-panel.presentational";
import {DepositPotentialDuplicatesPanelPresentational} from "@deposit/components/presentationals/deposit-potential-duplicates-panel/deposit-potential-duplicates-panel.presentational";
import {DepositDocumentFileUploadState} from "@deposit/stores/document-file/upload/deposit-document-file-upload.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedModule} from "@shared/shared.module";
import {DepositCommentValidatorState} from "./stores/comment-validator/deposit-comment-validator.state";

const routables = [
  DepositCreateEditRoutable,
  DepositDetailRoutable,
  DepositListRoutable,
  DepositMultipleImportRoutable,
];
const containers = [
  DepositDocumentFileContainer,
  DepositTableContributorContainer,
  DepositTableContributorSearchContainer,
  DepositCommentsContainer,
];
const dialogs = [
  DepositDocumentFileUploadDialog,
  DepositValidationStructureDialog,
  DepositValidateResearchGroupDialog,
  DepositCommentDialog,
  DepositBulkResearchGroupDialog,
  DepositBulkStructureDialog,
  DepositBulkLanguageDialog,
  DepositMultipleImportTypeDialog,
  DepositCreateFundingDialog,
  DepositCreateResearchGroupDialog,
  DepositCreateResearchGroupAlternativeDialog,
  DepositUnableImportFileDialog,
];
const presentationals = [
  DepositSummaryPresentational,
  DepositFormPresentational,
  DepositImportRowPresentational,
  DepositMultipleImportPresentational,
  DepositCommentFormPresentational,
  DepositFormFirstStepTypePresentational,
  DepositFormSecondStepFilesPresentational,
  DepositFormThirdStepContributorsPresentational,
  DepositFormFourthStepDescriptionPresentational,
  DepositFormDescriptionPublisherPresentational,
  DepositFormDescriptionCollectionPresentational,
  DepositFormDescriptionTextLanguagePresentational,
  DepositFormDescriptionPublicationIdentifierPresentational,
  DepositFormDescriptionDatasetPresentational,
  DepositFormDescriptionContainerPresentational,
  DepositFormDescriptionLinkPresentational,
  DepositFormDescriptionCorrectionPresentational,
  DepositFormDescriptionTextLanguageListPresentational,
  DepositFormDescriptionFundingPresentational,
  DepositFormDescriptionClassificationPresentational,
  DepositFormDescriptionPagePresentational,
  DepositFormDescriptionDatePresentational,
  DepositFormDescriptionLanguagePresentational,
  DepositContainerPresentational,
  DepositButtonApplyDefaultValuePresentational,
  DepositRepeatableFieldsPresentational,
  DepositFormTypeSubtypePresentational,
  DepositFormRomeoPresentational,
  DepositValidationPanelPresentational,
  DepositMetadataDifferencesPanelPresentational,
  DepositPotentialDuplicatesPanelPresentational,
];
const directives = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
    ...directives,
  ],
  imports: [
    SharedModule,
    DepositRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      DepositState,
      DepositCommentState,
      DepositCommentValidatorState,
      DepositStructureState,
      DepositStatusHistoryState,
      DepositDocumentFileState,
      DepositDocumentFileUploadState,
      DepositDocumentFileStatusHistoryState,
      DepositPersonStructureState,
      DepositPersonResearchGroupState,
      DepositResearchGroupState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class DepositModule {
}
