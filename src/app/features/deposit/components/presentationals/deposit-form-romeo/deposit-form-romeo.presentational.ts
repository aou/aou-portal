/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-romeo.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from "@angular/core";
import {
  Romeo,
  RomeoVersion,
} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  RegexUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-romeo",
  templateUrl: "./deposit-form-romeo.presentational.html",
  styleUrls: ["deposit-form-romeo.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormRomeoPresentational extends SharedAbstractPresentational {
  readonly KEY_VERSION_PUBLISHED: string = "published version";
  readonly KEY_VERSION_ACCEPTED: string = "accepted version";
  readonly KEY_VERSION_SUBMITTED: string = "submitted version";

  readonly KEY_ICON_AUTHORIZED: string = "authorized";
  readonly KEY_ICON_QUESTION: string = "question";
  readonly KEY_ICON_NOT_AUTHORIZED: string = "not-authorized";

  readonly KEY_JOURNAL_TYPE_SUBSCRIPTION: string = "Subscription. This journal's articles are only available to subscribers.";
  readonly KEY_JOURNAL_TYPE_HYBRID: string = "Hybrid. This journal contains both subscribers-only and Open Access articles.";
  readonly KEY_JOURNAL_TYPE_GOLD: string = "Gold. This journal only publishes Open Access articles.";
  readonly KEY_JOURNAL_TYPE_DEFAULT: string = "Undetermined, due to insufficient information in Sherpa Romeo";

  readonly KEY_DIFFUSION_FREE_ACCESS_AUTHORIZED: string = "free access authorized";
  readonly KEY_DIFFUSION_FREE_ACCESS_NOT_AUTHORIZED: string = "free access not authorized";
  readonly KEY_DIFFUSION_UNDETERMINED_ACCESS_LEVEL: string = "undetermined access level";
  readonly KEY_DIFFUSION_FREE_ACCESS_IF_APC_PAID: string = "Free access authorized, as long as an APC fee was paid for the article or the document includes an <em>Open Access</em> or <em>Creative Commons</em> mention";

  readonly KEY_FOOTER_INFORMATION: string = "Whole set of conditions for this journal can be found in Sherpa Romeo: {sherpaRomeoUrl}.";
  readonly KEY_FOOTER_NO_INFORMATION: string = "Sherpa Romeo does not provide information for this journal.";
  readonly KEY_FOOTER_IF_FUNDER: string = "If your research project was supported by a funder, more advantageous conditions can apply (see link below).";

  readonly KEY_ERROR_NO_INFORMATION: string = "Sherpa Romeo does not provide information for this journal.";

  private _romeo: Romeo;

  isErrorNoInformation: boolean = false;

  data: RomeoVersion[];

  @Input()
  set romeo(value: Romeo) {
    this._romeo = value;
    this._defineData();
    this._defineErrors();
  }

  get romeo(): Romeo {
    return this._romeo;
  }

  @Input()
  @HostBinding("class.darker-pyjama")
  darkerPyjama: boolean = false;

  constructor() {
    super();
  }

  private _defineErrors(): void {
    this.isErrorNoInformation = false;

    if (isNonEmptyArray(this.romeo.errors)) {
      if (this.romeo.errors.indexOf(this.KEY_ERROR_NO_INFORMATION) !== -1) {
        this.isErrorNoInformation = true;
      }
    }
  }

  private _defineData(): void {
    this.data = [];
    if (isNotNullNorUndefined(this.romeo?.versions?.published)) {
      this.data.push(this.romeo.versions.published);
    }
    if (isNotNullNorUndefined(this.romeo?.versions?.accepted)) {
      this.data.push(this.romeo.versions.accepted);
    }
    if (isNotNullNorUndefined(this.romeo?.versions?.submitted)) {
      this.data.push(this.romeo.versions.submitted);
    }
  }

  getDistributionIcon(icon: string | undefined): IconNameEnum | undefined {
    if (isNonEmptyString(icon)) {
      if (icon === this.KEY_ICON_AUTHORIZED) {
        return IconNameEnum.authorized;
      }
      if (icon === this.KEY_ICON_QUESTION) {
        return IconNameEnum.question;
      }
      if (icon === this.KEY_ICON_NOT_AUTHORIZED) {
        return IconNameEnum.notAuthorized;
      }
    }
    return undefined;
  }

  getDistributionText(text: string | undefined): string | undefined {
    if (isNonEmptyString(text)) {
      switch (text) {
        case this.KEY_DIFFUSION_FREE_ACCESS_AUTHORIZED:
          return MARK_AS_TRANSLATABLE("romeo.diffusion.freeAccessAuthorized");
        case this.KEY_DIFFUSION_FREE_ACCESS_NOT_AUTHORIZED:
          return MARK_AS_TRANSLATABLE("romeo.diffusion.freeAccessNotAuthorized");
        case this.KEY_DIFFUSION_UNDETERMINED_ACCESS_LEVEL:
          return MARK_AS_TRANSLATABLE("romeo.diffusion.undeterminedAccessLevel");
        case this.KEY_DIFFUSION_FREE_ACCESS_IF_APC_PAID:
          return MARK_AS_TRANSLATABLE("romeo.diffusion.freeAccessIfApcPaid");
      }
    }
    return text;
  }

  getJournalType(text: string | undefined): string | undefined {
    if (isNonEmptyString(text)) {
      switch (text) {
        case this.KEY_JOURNAL_TYPE_SUBSCRIPTION:
          return MARK_AS_TRANSLATABLE("romeo.journalType.subscription");
        case this.KEY_JOURNAL_TYPE_HYBRID:
          return MARK_AS_TRANSLATABLE("romeo.journalType.hybrid");
        case this.KEY_JOURNAL_TYPE_GOLD:
          return MARK_AS_TRANSLATABLE("romeo.journalType.gold");
        case this.KEY_JOURNAL_TYPE_DEFAULT:
          return MARK_AS_TRANSLATABLE("romeo.journalType.default");
      }
    }
    return text;
  }

  getFooter(text: string | undefined): string | undefined {
    if (isNonEmptyString(text)) {
      switch (text) {
        case this.KEY_FOOTER_INFORMATION:
          return MARK_AS_TRANSLATABLE("romeo.footer.information");
        case this.KEY_FOOTER_NO_INFORMATION:
          return MARK_AS_TRANSLATABLE("romeo.footer.noInformation");
        case this.KEY_FOOTER_IF_FUNDER:
          return MARK_AS_TRANSLATABLE("romeo.footer.ifFunder");
      }
    }
    return text;
  }

  getVersionToTranslate(version: string): string | undefined {
    if (isNonEmptyString(version)) {
      if (version === this.KEY_VERSION_PUBLISHED) {
        return LabelTranslateEnum.publishedVersion;
      }
      if (version === this.KEY_VERSION_ACCEPTED) {
        return LabelTranslateEnum.acceptedVersion;
      }
      if (version === this.KEY_VERSION_SUBMITTED) {
        return LabelTranslateEnum.submittedVersion;
      }
    }
    return version;
  }

  computeLink(text: string): string {
    if (isNullOrUndefined(text)) {
      return text;
    }
    return text.replace(RegexUtil.urlInComplexText, match => `<a href="${match}" target="_blank" class="no-hover-animation">${match}</a>`);
  }
}
