/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-text-language.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {FormComponentFormDefinitionDepositFormTextLanguage} from "@app/features/deposit/models/deposit-form-definition.model";
import {Language} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BehaviorSubject,
  Observable,
  of,
} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  ResourceState,
  RichTextEditionOption,
  SOLIDIFY_CONSTANTS,
  Type,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-text-language",
  templateUrl: "./deposit-form-description-text-language.presentational.html",
  styleUrls: ["./deposit-form-description-text-language.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionTextLanguagePresentational,
    },
  ],
})
export class DepositFormDescriptionTextLanguagePresentational<T> extends DepositAbstractFormPresentational implements ControlValueAccessor, OnInit {
  formDefinition: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();

  @Input()
  path: string;

  @Input()
  resourceNameSpace: ResourceNameSpace;

  @Input()
  state: Type<ResourceState<any, T>>;

  @Input()
  filterOptionsValueKey: string;

  @Input()
  queryParametersSearchKey: string;

  @Input()
    // tslint:disable-next-line
  filteredOptionsCallbackLabel: (option: T) => string = option => option[this.filterOptionsValueKey];

  @Input()
  listLanguages: Language[];

  @Input()
  @HostBinding("class.hide-language")
  hideLanguage: boolean = false;

  @Input()
  labelToTranslate: string = LabelTranslateEnum.title;

  @Input()
  isTextarea: boolean = false;

  @Input()
  isRichText: boolean = false;

  @Input()
  isLinkAllowed: boolean = false;

  @Input()
  isBoldAllowed: boolean = false;

  @Input()
  textareaMinRows: number = 5;

  @Input()
  textareaMaxRows: number = 10;

  @Input()
  textareaMaxSize: number | undefined = undefined;

  @Input()
  isMandatoryLanguage: boolean = false;

  @Input()
  tooltipText: string | undefined;

  @Input()
  tooltipLanguage: string | undefined;

  @Input()
  hintTextToTranslate: string | undefined;

  @Input()
  displayInitialText: boolean = true;

  @Input()
  initialText: string;

  initialTextInitialized: boolean;

  @Input()
  withWordCounter: boolean = false;

  private readonly _autocompleteOptionBS: BehaviorSubject<T | undefined> = new BehaviorSubject<T | undefined>(undefined);
  @Output("autocompleteOptionSelected")
  readonly autocompleteOptionObs: Observable<T | undefined> = ObservableUtil.asObservable(this._autocompleteOptionBS);

  @ViewChild("inputElement", {static: false})
  private _inputElementRef: ElementRef;

  @ViewChild("textAreaElement", {static: false})
  private _textAreaElement: ElementRef;

  getPath(fieldName: string): string {
    return this.path + (isNonEmptyString(fieldName) ? (SOLIDIFY_CONSTANTS.DOT + fieldName) : SOLIDIFY_CONSTANTS.STRING_EMPTY);
  }

  filteredOptionsObs: Observable<T[]> = of([]);
  filteredOptionsIsLoadingObs: Observable<boolean> = of(false);

  get numberWords(): number {
    const text = this.getFormControlInCurrentFormGroup(this.formDefinition.text)?.value as string;
    if (isNullOrUndefinedOrWhiteString(text)) {
      return 0;
    }
    return text.split(" ").filter(term => isNonEmptyString(term)).length;
  }

  richTextOptions: RichTextEditionOption;

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService) {
    super(_changeDetector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._computeRichTextOptions();

    if (isNotNullNorUndefined(this.resourceNameSpace)) {
      this.filteredOptionsObs = MemoizedUtil.list(this._store, this.state);
      this.filteredOptionsIsLoadingObs = MemoizedUtil.isLoading(this._store, this.state);

      this.subscribe(this.formGroup.get(this.formDefinition.text).valueChanges.pipe(
        distinctUntilChanged(),
        filter(value => isNonEmptyString(value)),
        debounceTime(250),
        tap(value => {
          const queryParameters = new QueryParameters();
          const search = QueryParametersUtil.getSearchItems(queryParameters);
          MappingObjectUtil.set(search, this.queryParametersSearchKey, value);
          queryParameters.paging.pageSize = 10;
          this._store.dispatch(new this.resourceNameSpace.GetAll(queryParameters));
        }),
      ));
    }

    this.subscribe(this.formGroup.get(this.formDefinition.text).valueChanges.pipe(
      tap(value => {
        if (isNullOrUndefinedOrWhiteString(value)) {
          this.formGroup.get(this.formDefinition.lang).setValue(null);
        }
      }),
    ));

  }

  private _computeRichTextOptions(): void {
    if (!isTrue(this.isRichText)) {
      return;
    }
    this.richTextOptions = {
      bold: isTrue(this.isBoldAllowed),
      italic: true,
      underline: false,
      strike: false,
      sub: true,
      super: true,
      clean: true,
      link: isTrue(this.isLinkAllowed),
      image: false,
      video: false,
    };
  }

  autocompleteOptionSelected($event: MatAutocompleteSelectedEvent): void {
    const option = $event.option.value as T;
    this.formGroup.get(this.formDefinition.text).setValue(option[this.filterOptionsValueKey]);
    this._autocompleteOptionBS.next(option);
  }

  loadNextChunk(): void {
    this._store.dispatch(new this.resourceNameSpace.LoadNextChunkList());
  }

  setInitialText(value: string): void {
    if (this.initialTextInitialized) {
      return;
    }
    this.initialTextInitialized = true;
    this.initialText = value;
  }

  focus(): void {
    if (this._inputElementRef) {
      setTimeout(() => this._inputElementRef.nativeElement.focus(), 0);
    } else if (this._textAreaElement) {
      setTimeout(() => this._textAreaElement.nativeElement.focus(), 0);
    }
  }
}
