/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-abstract-form-list.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Input,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BaseFormDefinition,
  NotificationService,
} from "solidify-frontend";

@Directive()
export abstract class DepositAbstractFormListPresentational extends DepositAbstractFormPresentational implements OnInit, ControlValueAccessor {
  abstract listFormDefinitions: BaseFormDefinition[];

  @Input()
  labelSectionToTranslate: string;

  @Input()
  formArray: FormArray;

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService) {
    super(_changeDetector);
  }

  abstract formGroupFactory(fb: FormBuilder, ...formDefinition: BaseFormDefinition[]): FormGroup;

  ngOnInit(): void {
    super.ngOnInit();
    this._detectChangesWhenStatusChanges(this.formArray);
  }

}
