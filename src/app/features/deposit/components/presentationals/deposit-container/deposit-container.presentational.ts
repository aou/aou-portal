/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-container.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {
  DepositSummaryLinkInfo,
  DepositSummaryValueLanguage,
} from "@app/features/deposit/components/presentationals/deposit-summary/deposit-summary.presentational";
import {DepositFormText} from "@app/features/deposit/models/deposit-form-definition.model";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Store} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  AbstractPresentational,
  isEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-container",
  templateUrl: "./deposit-container.presentational.html",
  styleUrls: ["deposit-container.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositContainerPresentational extends AbstractPresentational {
  private _depositFormText: DepositText[];

  @Input()
  withUrl: boolean = false;

  @Input()
  isInnerHtml: boolean = false;

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  @Input()
  set depositFormText(value: DepositText[]) {
    this._depositFormText = value;

    if (isNullOrUndefined(value) || isEmptyArray(value)) {
      return;
    }

    this._initCurrentWithLanguage();
  }

  get depositFormText(): DepositText[] {
    return this._depositFormText;
  }

  @Input()
  titleToTranslate: string;

  listAllLanguages: string[] = [];
  listOtherLanguages: string[] = [];
  current: DepositText;

  constructor(private readonly _store: Store) {
    super();
  }

  private _initCurrentWithLanguage(): void {
    const currentLanguage = Enums.Language.LanguageIso639V1ToV2(MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage));
    let indexCurrentLanguage = this._depositFormText.filter(t => isNotNullNorUndefined(t)).findIndex(t => t?.lang?.toLowerCase() === currentLanguage);
    if (indexCurrentLanguage === -1) {
      const defaultLanguage = Enums.Language.LanguageIso639V1ToV2(environment.defaultLanguage);
      indexCurrentLanguage = this._depositFormText.filter(t => isNotNullNorUndefined(t)).findIndex(t => t?.lang?.toLowerCase() === defaultLanguage);
      if (indexCurrentLanguage === -1) {
        indexCurrentLanguage = 0;
      }
    }

    this.current = this._depositFormText[indexCurrentLanguage];
    this._initListLanguage(this.current);
  }

  switchToLanguage(language: string): void {
    this.current = this.depositFormText.find(t => t.lang === language);
    this._initListLanguage(this.current);
  }

  private _initListLanguage(currentLang: DepositText | undefined): void {
    this.listAllLanguages = this.depositFormText.filter(t => isNotNullNorUndefined(t) && isNonEmptyString(t.lang)).map(t => t?.lang);
    this.listOtherLanguages = this.listAllLanguages.filter(t => t !== currentLang?.lang);
  }
}

type DepositText = DepositFormText | DepositSummaryLinkInfo | DepositSummaryValueLanguage;
