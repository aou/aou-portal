/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-correction.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  AbstractControlOptions,
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {
  DepositFormCorrection,
  FormComponentFormDefinitionDepositFormCorrection,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {environment} from "@environments/environment";
import {Language} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {CorrigendumValidator} from "@shared/validators/corrigendum.validator";
import {DoiCleaner} from "@shared/validators/doi.validator";
import {TrimValidator} from "@shared/validators/trim.validator";
import {
  BaseFormDefinition,
  isNotNullNorUndefined,
  NotificationService,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-correction",
  templateUrl: "./deposit-form-description-correction.presentational.html",
  styleUrls: ["./deposit-form-description-correction.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionCorrectionPresentational,
    },
  ],
})
export class DepositFormDescriptionCorrectionPresentational extends DepositAbstractFormListPresentational implements ControlValueAccessor {
  formDefinition: FormComponentFormDefinitionDepositFormCorrection = new FormComponentFormDefinitionDepositFormCorrection();
  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition];

  @Input()
  listLanguages: Language[];

  @Input()
  isPanelOpenByDefault: boolean = true;

  prefixDoi: string = environment.doiWebsite;
  prefixPmid: string = environment.pmidWebsiteId;

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
  }

  static createCorrection(_fb: FormBuilder,
                          formDefinition: FormComponentFormDefinitionDepositFormCorrection,
                          correction: DepositFormCorrection | undefined = undefined): FormGroup {
    const form = _fb.group({
      [formDefinition.note]: [undefined, [SolidifyValidator]],
      [formDefinition.doi]: [undefined, {
        updateOn: "blur",
        validators: [TrimValidator, SolidifyValidator, DoiCleaner, Validators.pattern(AouRegexUtil.isDoi)],
      } as AbstractControlOptions],
      [formDefinition.pmid]: [undefined, {
        updateOn: "blur",
        validators: [TrimValidator, SolidifyValidator, Validators.pattern(AouRegexUtil.isPmid)],
      } as AbstractControlOptions],
    });

    form.setValidators(CorrigendumValidator);

    if (isNotNullNorUndefined(correction)) {
      form.get(formDefinition.note).setValue(correction.note);
      form.get(formDefinition.doi).setValue(correction.doi);
      form.get(formDefinition.pmid).setValue(correction.pmid);
    }

    return form;
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositFormCorrection): FormGroup {
    return DepositFormDescriptionCorrectionPresentational.createCorrection(fb, formDefinition);
  }
}
