/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-metadata-differences-panel.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";
import {MetadataDifference} from "@models";

@Component({
  selector: "aou-deposit-metadata-differences-panel",
  templateUrl: "./deposit-metadata-differences-panel.presentational.html",
  styleUrls: ["deposit-metadata-differences-panel.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositMetadataDifferencesPanelPresentational extends SharedAbstractPresentational {

  private _listDifferences: MappingObject<string, MappingObject<string, MetadataDifference[]>>;

  @Input()
  set listDifferences(value: MappingObject<string, MappingObject<string, MetadataDifference[]>>) {
    this._listDifferences = value;
  }

  get listDifferences(): MappingObject<string, MappingObject<string, MetadataDifference[]>> {
    return this._listDifferences;
  }

  isKeyEqualsInBothMaps(index: number, key: string): boolean {
    const subMap: MappingObject<string, MetadataDifference[]> = MappingObjectUtil.values(this._listDifferences)[index];
    if (MappingObjectUtil.size(subMap) === 1 && MappingObjectUtil.keys(subMap)[0] === key) {
      return true;
    }
    return false;
  }

  isFieldNameNull(index: number, key: string): boolean {
    const subMap: MappingObject<string, MetadataDifference[]> = MappingObjectUtil.values(this._listDifferences)[index];
    if (MappingObjectUtil.size(subMap) === 1 && MappingObjectUtil.values(subMap)[0] === null) {
      return true;
    }
    return false;
  }

  get mappingObjectUtil(): typeof MappingObjectUtil {
    return MappingObjectUtil;
  }

  getKey(index: number): string {
    return MappingObjectUtil.keys(this._listDifferences)[index];
  }

  getSubMapMetadataDifference(key: string): MappingObject<string, MetadataDifference[]> {
    return MappingObjectUtil.get(this._listDifferences, key);
  }

  getNoValueIfPropertyIsNullOrEmpty(value: string): string {
    if (value === null || value === "") {
      return MARK_AS_TRANSLATABLE("deposit.differences.noValue");
    }
    return value;
  }
}
