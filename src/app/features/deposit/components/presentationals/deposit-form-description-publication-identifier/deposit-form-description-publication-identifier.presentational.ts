/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-publication-identifier.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {
  AbstractControl,
  AbstractControlOptions,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {MatSelectChange} from "@angular/material/select";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {
  DepositFormPublicationIdentifiers,
  FormComponentFormDefinitionDepositFormPublicationIdentifiers,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {environment} from "@environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {TooltipTranslateEnum} from "@shared/enums/tooltip-translate.enum";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {DoiCleaner} from "@shared/validators/doi.validator";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
} from "solidify-frontend";
import {TrimValidator} from "@shared/validators/trim.validator";

@Component({
  selector: "aou-deposit-form-description-publication-identifier",
  templateUrl: "./deposit-form-description-publication-identifier.presentational.html",
  styleUrls: ["./deposit-form-description-publication-identifier.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionPublicationIdentifierPresentational,
    },
  ],
})
export class DepositFormDescriptionPublicationIdentifierPresentational extends DepositAbstractFormPresentational implements OnInit {
  readonly PATH_PENDING: string = "PENDING_VALUE_IDENTIFIER";
  formDefinition: FormComponentFormDefinitionDepositFormPublicationIdentifiers = new FormComponentFormDefinitionDepositFormPublicationIdentifiers();

  listMandatoryIdentifiers: string[] = [
    DepositFormRuleHelper.pathIdentifierDoi,
    DepositFormRuleHelper.pathIdentifierPmid,
  ];

  listIdentifiers: string[] = [
    DepositFormRuleHelper.pathIdentifierDoi,
    DepositFormRuleHelper.pathIdentifierPmid,
    DepositFormRuleHelper.pathIdentifierPmcid,
    // DepositFormRuleHelper.pathIdentifierIsbn,
    // DepositFormRuleHelper.pathIdentifierIssn,
    DepositFormRuleHelper.pathIdentifierArXiv,
    DepositFormRuleHelper.pathIdentifierDblp,
    DepositFormRuleHelper.pathIdentifierRepEc,
    // DepositFormRuleHelper.pathIdentifierLocalNumber,
    DepositFormRuleHelper.pathIdentifierMmsid,
    DepositFormRuleHelper.pathIdentifierUrn,
  ];

  formControlTempValue: FormControl = new FormControl(undefined);
  listIdentifiersSelected: MappingObject<string, FormControl> = {}; // DepositFormRuleHelper.pathIdentifierPmcid, FormControl of identifier

  get listIdentifiersAvailable(): string[] {
    return this.listIdentifiers.filter(i => (MappingObjectUtil.keys(this.listIdentifiersSelected) as string[]).indexOf(i) === -1);
  }

  get listIdentifiersOptional(): string[] {
    return this.listIdentifiersSelectedKey.filter(f => this.listMandatoryIdentifiers.indexOf(f) === -1);
  }

  get listIdentifiersSelectedKey(): string[] {
    return MappingObjectUtil.keys(this.listIdentifiersSelected) as string[];
  }

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService) {
    super(_changeDetector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const listIdentifiersAuthorizedWithThisSubtype = this.listIdentifiers.filter(fieldPath => DepositFormRuleHelper.shouldDisplayField(this.mainFormGroup, fieldPath));
    this.listIdentifiers = listIdentifiersAuthorizedWithThisSubtype;

    this.listMandatoryIdentifiers.forEach(identifier => {
      const formControlIdentifierType = this._fb.control({value: identifier, disabled: true});
      MappingObjectUtil.set(this.listIdentifiersSelected, identifier, formControlIdentifierType);
    });

    MappingObjectUtil.forEach(this.formGroup.value, (value, key) => {
      if (isNullOrUndefinedOrWhiteString(value)) {
        return;
      }
      const fieldPath = this._getFieldPathFromKey(key);
      if (isNullOrUndefined(fieldPath)) {
        return;
      }
      let formControlIdentifierType = this._fb.control(fieldPath);
      if (MappingObjectUtil.has(this.listIdentifiersSelected, fieldPath)) {
        formControlIdentifierType = MappingObjectUtil.get(this.listIdentifiersSelected, fieldPath);
      }
      MappingObjectUtil.set(this.listIdentifiersSelected, fieldPath, formControlIdentifierType);
    });

    if (this.listIdentifiersOptional.length === 0) {
      this.addIdentifier();
    }

    // this.listIdentifiers.forEach(fieldPath => {
    //   const formControl = this.mainFormGroup.get(fieldPath);
    //   if (isNonEmptyString(formControl?.value)) {
    //     this.listIdentifiersSelected.push(fieldPath);
    //   }
    // });
  }

  static createOrBindPublicationIdentifier(_fb: FormBuilder,
                                           formDefinition: FormComponentFormDefinitionDepositFormPublicationIdentifiers,
                                           form: FormGroup | undefined = undefined,
                                           publicationIdentifier: DepositFormPublicationIdentifiers | undefined = undefined): FormGroup {
    if (isNullOrUndefined(form)) {
      form = _fb.group({
        [formDefinition.doi]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator, DoiCleaner, Validators.pattern(AouRegexUtil.isDoi)],
        } as AbstractControlOptions],
        [formDefinition.pmid]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator, Validators.pattern(AouRegexUtil.isPmid)],
        } as AbstractControlOptions],
        [formDefinition.pmcid]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator, Validators.pattern(AouRegexUtil.isPmcid)],
        } as AbstractControlOptions],
        [formDefinition.isbn]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator, Validators.pattern(AouRegexUtil.isIsbn)],
        } as AbstractControlOptions],
        [formDefinition.issn]: [undefined, [TrimValidator, SolidifyValidator, Validators.maxLength(9)]],
        [formDefinition.arxiv]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator],
        } as AbstractControlOptions],
        [formDefinition.mmsid]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator],
        } as AbstractControlOptions],
        [formDefinition.repec]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator],
        } as AbstractControlOptions],
        [formDefinition.dblp]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator],
        } as AbstractControlOptions],
        [formDefinition.urn]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator],
        } as AbstractControlOptions],
        [formDefinition.localNumber]: [undefined, {
          updateOn: "blur",
          validators: [TrimValidator, SolidifyValidator],
        } as AbstractControlOptions],
      });
    }

    if (isNotNullNorUndefined(publicationIdentifier)) {
      form.get(formDefinition.doi).setValue(publicationIdentifier.doi);
      form.get(formDefinition.pmid).setValue(publicationIdentifier.pmid);
      form.get(formDefinition.pmcid).setValue(publicationIdentifier.pmcid);
      form.get(formDefinition.isbn).setValue(publicationIdentifier.isbn);
      form.get(formDefinition.issn).setValue(publicationIdentifier.issn);
      form.get(formDefinition.arxiv).setValue(publicationIdentifier.arxiv);
      form.get(formDefinition.mmsid).setValue(publicationIdentifier.mmsid);
      form.get(formDefinition.repec).setValue(publicationIdentifier.repec);
      form.get(formDefinition.dblp).setValue(publicationIdentifier.dblp);
      form.get(formDefinition.urn).setValue(publicationIdentifier.urn);
      form.get(formDefinition.localNumber).setValue(publicationIdentifier.localNumber);
    }

    return form;
  }

  getListIdentifiersAvailableWithCurrent(current: string): string[] {
    if (current !== this.PATH_PENDING) {
      return [current, ...this.listIdentifiersAvailable];
    }
    return this.listIdentifiersAvailable;
  }

  getFormControlIdentifierType(key: string): FormControl {
    return MappingObjectUtil.get(this.listIdentifiersSelected, key);
  }

  isRequired(formControl: AbstractControl, key: string): boolean {
    const errors = formControl.get(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  addIdentifier(): void {
    const formControlIdentifierType = this._fb.control(undefined);
    MappingObjectUtil.set(this.listIdentifiersSelected, this.PATH_PENDING, formControlIdentifierType);
  }

  remove(fieldPathKey: string): void {
    const formControlIdentifierValue = this.getFormControlIdentifierValue(fieldPathKey);
    formControlIdentifierValue.setValue(null);
    MappingObjectUtil.delete(this.listIdentifiersSelected, fieldPathKey);

    if (this.listIdentifiersOptional.length === 0) {
      this.addIdentifier();
    }
  }

  getTooltip(identifier: string): string {
    switch (identifier) {
      case DepositFormRuleHelper.pathIdentifierDoi:
        return TooltipTranslateEnum.depositIdentifierDoi;
      case DepositFormRuleHelper.pathIdentifierPmid:
        return TooltipTranslateEnum.depositIdentifierPmid;
      case DepositFormRuleHelper.pathIdentifierPmcid:
        return TooltipTranslateEnum.depositIdentifierPmcid;
      case DepositFormRuleHelper.pathIdentifierIsbn:
        return TooltipTranslateEnum.depositIdentifierIsbn;
      case DepositFormRuleHelper.pathIdentifierIssn:
        return TooltipTranslateEnum.depositIdentifierIssn;
      case DepositFormRuleHelper.pathIdentifierArXiv:
        return TooltipTranslateEnum.depositIdentifierArXiv;
      case DepositFormRuleHelper.pathIdentifierDblp:
        return TooltipTranslateEnum.depositIdentifierDblp;
      case DepositFormRuleHelper.pathIdentifierRepEc:
        return TooltipTranslateEnum.depositIdentifierRepEc;
      case DepositFormRuleHelper.pathIdentifierLocalNumber:
        return TooltipTranslateEnum.depositIdentifierLocalNumber;
      case DepositFormRuleHelper.pathIdentifierMmsid:
        return TooltipTranslateEnum.depositIdentifierMmsid;
      case DepositFormRuleHelper.pathIdentifierUrn:
        return TooltipTranslateEnum.depositIdentifierUrn;
      default:
        return undefined;
    }
  }

  getFormControlIdentifierValue(fieldPathKey: string): FormControl {
    if (fieldPathKey === this.PATH_PENDING) {
      return this.formControlTempValue;
    }
    return this.getFormControl(fieldPathKey);
  }

  changeTypeOfIdentifier(oldFieldPath: string, $event: MatSelectChange, index: number): void {
    const newFieldPath = $event.value as string;
    const typeIdentifierFormControl = MappingObjectUtil.get(this.listIdentifiersSelected, oldFieldPath);
    MappingObjectUtil.delete(this.listIdentifiersSelected, oldFieldPath);
    const oldFormControlIdentifierValue = this.getFormControlIdentifierValue(oldFieldPath);
    const newFormControlIdentifierValue = this.getFormControlIdentifierValue(newFieldPath);
    newFormControlIdentifierValue.setValue(oldFormControlIdentifierValue.value);
    oldFormControlIdentifierValue.setValue(undefined);
    MappingObjectUtil.set(this.listIdentifiersSelected, newFieldPath, typeIdentifierFormControl);
  }

  private _getFieldPathFromKey(key: string): string {
    return this.listIdentifiers.find(identifier => identifier.endsWith(key));
  }

  getPrefixUrl(key: string): string | undefined {
    if (key === DepositFormRuleHelper.pathIdentifierDoi) {
      return environment.doiWebsite;
    }
    if (key === DepositFormRuleHelper.pathIdentifierPmid) {
      return environment.pmidWebsiteId;
    }
    if (key === DepositFormRuleHelper.pathIdentifierPmcid) {
      return environment.pmcidWebsiteId;
    }
    if (key === DepositFormRuleHelper.pathIdentifierArXiv) {
      return environment.arxivWebsiteId;
    }
    return SOLIDIFY_CONSTANTS.STRING_EMPTY;
  }
}
