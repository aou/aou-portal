/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-second-step-files.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {
  DepositFormDates,
  FormComponentFormDefinitionFirstStepType,
  FormComponentFormDefinitionFourthStepDescription,
  FormComponentFormDefinitionMain,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {Enums} from "@enums";
import {Romeo} from "@models";
import {
  BaseFormDefinition,
  DateUtil,
  isEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-second-step-files",
  templateUrl: "./deposit-form-second-step-files.presentational.html",
  styleUrls: ["deposit-form-second-step-files.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormSecondStepFilesPresentational extends DepositAbstractFormPresentational {
  formDefinitionMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  formDefinition: BaseFormDefinition = undefined;
  formDefinitionType: FormComponentFormDefinitionFirstStepType = new FormComponentFormDefinitionFirstStepType();
  formDefinitionDescription: FormComponentFormDefinitionFourthStepDescription = new FormComponentFormDefinitionFourthStepDescription();

  protected _mainFormGroup: FormGroup;

  @Input()
  depositId: string;

  @Input()
  listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>;

  @Input()
  notifyFilesFromExternalSource: boolean;

  @Input()
  hasDuplicateFile: boolean;

  @Input()
  isValidationMode: boolean;

  @Input()
  romeo: Romeo;

  @Input()
  set mainFormGroup(value: FormGroup) {
    if (isNullOrUndefined(value)) {
      return;
    }
    this._mainFormGroup = value;
    this._computePublicationDate();
  }

  get mainFormGroup(): FormGroup {
    return this._mainFormGroup;
  }

  isEditable: boolean = true;
  publicationDate: Date | undefined;

  constructor(protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
  ) {
    super(_changeDetector);
  }

  externalDetectChanges(): void {
    this._changeDetector.detectChanges();
  }

  private _computePublicationDate(): void {
    if (isNullOrUndefined(this.mainFormGroup)) {
      return;
    }
    const subType = this.mainFormGroup.get(DepositFormRuleHelper.pathSubType)?.value;
    const isDiploma = subType === Enums.Deposit.DepositSubTypeEnum.D1 || subType === Enums.Deposit.DepositSubTypeEnum.D2 || subType === Enums.Deposit.DepositSubTypeEnum.D3 || subType === Enums.Deposit.DepositSubTypeEnum.D4;
    const dates: DepositFormDates = this.mainFormGroup.get(DepositFormRuleHelper.pathDates).value;

    const listDates: string[] = [];
    if (isDiploma) {
      listDates.push(dates.defense, dates.imprimatur);
    } else {
      listDates.push(dates.publication, dates.first_online);
    }
    const filteredDate = listDates.filter(d => isNotNullNorUndefinedNorWhiteString(d));
    if (isEmptyArray(filteredDate)) {
      return;
    }
    const matchingDate = filteredDate[0];

    let publicationDate: Date;
    if (matchingDate.indexOf(SOLIDIFY_CONSTANTS.DOT) === -1) {
      // Format yyyy
      publicationDate = new Date(matchingDate);
      publicationDate.setMonth(11);
      publicationDate.setDate(31);
    } else {
      const matchingDateSplit = matchingDate.split(SOLIDIFY_CONSTANTS.DOT);
      if (matchingDateSplit.length === 2) {
        // Format mm.yyyy
        const year = +matchingDateSplit[1];
        const month = +matchingDateSplit[0];
        publicationDate = new Date(year, month, 0);
      } else {
        // Format dd.mm.yyyy
        publicationDate = DateUtil.convertSwissDateLongStringToDate(matchingDate);
      }
    }

    if (isNaN(publicationDate.getTime())) {
      publicationDate = undefined;
    }

    this.publicationDate = publicationDate;
  }
}
