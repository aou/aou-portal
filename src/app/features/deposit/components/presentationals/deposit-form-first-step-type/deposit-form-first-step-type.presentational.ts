/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-first-step-type.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  AbstractControlOptions,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {
  FormComponentFormDate,
  FormComponentFormDefinitionDepositFormTextLanguage,
  FormComponentFormDefinitionFirstStepType,
  FormComponentFormDefinitionFourthStepDescription,
  FormComponentFormDefinitionMain,
  FormComponentFormDefinitionRetrieveReferences,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositFormDescriptionTextLanguagePresentational} from "@deposit/components/presentationals/deposit-form-description-text-language/deposit-form-description-text-language.presentational";
import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  DepositSubSubtype,
  DepositSubtype,
  Language,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {DoiCleaner} from "@shared/validators/doi.validator";
import {BehaviorSubject} from "rxjs";
import {Observable} from "rxjs/index";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  isEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isTrue,
  MappingObjectUtil,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
  SsrUtil,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-first-step-type",
  templateUrl: "./deposit-form-first-step-type.presentational.html",
  styleUrls: ["deposit-form-first-step-type.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormFirstStepTypePresentational extends DepositAbstractFormPresentational implements OnInit {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  formDefinitionMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  formDefinition: FormComponentFormDefinitionFirstStepType = new FormComponentFormDefinitionFirstStepType();
  formDefinitionFourthStep: FormComponentFormDefinitionFourthStepDescription = new FormComponentFormDefinitionFourthStepDescription();
  formDefinitionTextLanguage: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();
  formDefinitionRetrieveReferences: FormComponentFormDefinitionRetrieveReferences = new FormComponentFormDefinitionRetrieveReferences();

  formGroupRetrieveReferences: FormGroup;

  protected _mainFormGroup: FormGroup;

  @Input()
  isCreation: boolean = false;

  @Input()
  isImported: boolean = false;

  @Input()
  formDefinitionDateThesis: FormComponentFormDate = new FormComponentFormDate();

  @Input()
  set mainFormGroup(value: FormGroup) {
    if (isNullOrUndefined(value)) {
      return;
    }
    this.formGroup = value.controls[this.formDefinitionMain.type] as FormGroup;
    this._detectChangesWhenStatusChanges(this.formGroup);
    this._mainFormGroup = value;
  }

  get mainFormGroup(): FormGroup {
    return this._mainFormGroup;
  }

  get importMethodEnum(): typeof ImportMethodEnum {
    return ImportMethodEnum;
  }

  isEditable: boolean = true;

  @Input()
  listDepositSubtype: DepositSubtype[];

  @Input()
  listDepositSubSubtype: DepositSubSubtype[];

  @Input()
  listLanguages: Language[];

  @Input()
  minYearThesis: number;

  @Input()
  maxYearThesis: number;

  private readonly _subtypeBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("subtypeChange")
  readonly subtypeObs: Observable<string | undefined> = ObservableUtil.asObservable(this._subtypeBS);

  private readonly _getMetadataFromServiceBS: BehaviorSubject<GetMetadataFromServiceInfo | undefined> = new BehaviorSubject<GetMetadataFromServiceInfo | undefined>(undefined);
  @Output("getMetadataFromServiceChange")
  readonly getMetadataFromServiceObs: Observable<GetMetadataFromServiceInfo | undefined> = ObservableUtil.asObservable(this._getMetadataFromServiceBS);

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _dateThesisChangeBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("dateThesisChange")
  readonly dateThesisChangeObs: Observable<string | undefined> = ObservableUtil.asObservable(this._dateThesisChangeBS);

  @ViewChild("originalTitleTextLanguage")
  readonly originalTitleTextLanguage: DepositFormDescriptionTextLanguagePresentational<any>;

  displayOriginalTitle: boolean = false;

  listRetrieveReferencesServices: RetrieveReferenceService[] = [
    {
      name: ImportMethodEnum.doi,
      endpoint: ApiActionNameEnum.GET_METADATA_FROM_DOI,
      exampleIdentifier: "10.1016/j.cis.2010.02.006",
      isDisabled: false,
      isGroupedInOtherMethod: false,
      labelRadioOption: LabelTranslateEnum.importADepositFromDoi,
      identifierName: "doi",
      importSource: Enums.Deposit.ImportSourceEnum.DOI,
    },
    {
      name: ImportMethodEnum.pubmed,
      endpoint: ApiActionNameEnum.GET_METADATA_FROM_PMID,
      exampleIdentifier: "18951202",
      exampleIdentifierSuffix: "PMID",
      isDisabled: false,
      isGroupedInOtherMethod: false,
      labelRadioOption: LabelTranslateEnum.importADepositFromPmid,
      identifierName: "pmid",
      importSource: Enums.Deposit.ImportSourceEnum.PMID,
    },
    {
      name: "arXiv",
      endpoint: ApiActionNameEnum.GET_METADATA_FROM_ARXIV,
      exampleIdentifier: "0902.2365",
      isDisabled: false,
      isGroupedInOtherMethod: false,
      labelRadioOption: LabelTranslateEnum.importADepositFromArxiv,
      identifierName: "arxivId",
      importSource: Enums.Deposit.ImportSourceEnum.ARXIV,
    },
    {
      name: ImportMethodEnum.isbn,
      endpoint: ApiActionNameEnum.GET_METADATA_FROM_ISBN,
      exampleIdentifier: "978-3-030-67837-1 or 2-84177-148-2",
      isDisabled: false,
      isGroupedInOtherMethod: false,
      labelRadioOption: LabelTranslateEnum.importADepositFromIsbn,
      identifierName: "isbn",
      importSource: Enums.Deposit.ImportSourceEnum.ISBN,
    },
    {
      name: "DBLP",
      endpoint: undefined,
      exampleIdentifier: "journals/tcs/HucJLR12",
      isDisabled: true,
      isGroupedInOtherMethod: false,
      labelRadioOption: LabelTranslateEnum.importADepositFromDblp,
      identifierName: "dblp",
      // importSource: Enums.Deposit.ImportSourceEnum.ISBN,
    },
  ];

  get listGroupedService(): RetrieveReferenceService[] {
    if (isNullOrUndefined(this.listRetrieveReferencesServices) || isEmptyArray(this.listRetrieveReferencesServices)) {
      return [];
    }
    return this.listRetrieveReferencesServices.filter(r => r.isGroupedInOtherMethod && !r.isDisabled);
  }

  get listUngroupedService(): RetrieveReferenceService[] {
    if (isNullOrUndefined(this.listRetrieveReferencesServices) || isEmptyArray(this.listRetrieveReferencesServices)) {
      return [];
    }
    return this.listRetrieveReferencesServices.filter(r => !r.isGroupedInOtherMethod && !r.isDisabled);
  }

  get selectedService(): RetrieveReferenceService | undefined {
    const importMode = this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.importMode)?.value;
    if (isNullOrUndefined(importMode) || importMode === ImportMethodEnum.manual) {
      return undefined;
    }

    if (importMode === ImportMethodEnum.other) {
      const service = this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.service)?.value;
      if (isNullOrUndefined(service)) {
        return undefined;
      }
      return this.listRetrieveReferencesServices.find(r => r.isGroupedInOtherMethod && r.name === service);
    }

    return this.listRetrieveReferencesServices.find(r => !r.isGroupedInOtherMethod && r.name === importMode);
  }

  get displayForm(): boolean {
    return !this.isCreation || this.isImported || this.formValidationHelper.getFormControl(this.formGroupRetrieveReferences, this.formDefinitionRetrieveReferences.importMode)?.value ===
      ImportMethodEnum.manual;
  }

  constructor(protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
  ) {
    super(_changeDetector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const urlQueryParameters = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
    let importMethod = ImportMethodEnum.manual;
    let identifier = SOLIDIFY_CONSTANTS.STRING_EMPTY;
    if (MappingObjectUtil.has(urlQueryParameters, importMethodQueryParam)) {
      importMethod = MappingObjectUtil.get(urlQueryParameters, importMethodQueryParam) as ImportMethodEnum;
    }
    if (MappingObjectUtil.has(urlQueryParameters, importIdentifierQueryParam)) {
      identifier = MappingObjectUtil.get(urlQueryParameters, importIdentifierQueryParam);
      identifier = SsrUtil.window?.decodeURIComponent(identifier);
      if (importMethod === ImportMethodEnum.manual) {
        importMethod = undefined;
      }
    }

    this.formGroupRetrieveReferences = this._fb.group({
      [this.formDefinitionRetrieveReferences.importMode]: [importMethod, [SolidifyValidator]],
      [this.formDefinitionRetrieveReferences.service]: [undefined, [SolidifyValidator]],
      [this.formDefinitionRetrieveReferences.identifier]: [identifier, {updateOn: "blur", validators: [SolidifyValidator]} as AbstractControlOptions],
    });

    const fcImportMode = this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.importMode) as FormControl<ImportMethodEnum>;
    const fcService = this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.service);
    const fcIdentifier = this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.identifier);

    if (isNotNullNorUndefinedNorWhiteString(identifier) && isNotNullNorUndefined(importMethod) && importMethod !== ImportMethodEnum.manual) {
      this.getMetadataFromService();
    }

    this.subscribe(fcImportMode.valueChanges.pipe(
      distinctUntilChanged(),
      tap(im => {
        const identifierValidators = [];
        const serviceValidators = [];
        if (isNotNullNorUndefinedNorWhiteString(im) && im !== ImportMethodEnum.manual) {
          identifierValidators.push(Validators.required);
          serviceValidators.push(Validators.required);
          if (im === ImportMethodEnum.doi) {
            identifierValidators.push(Validators.pattern(AouRegexUtil.isDoi), DoiCleaner);
          } else if (im === ImportMethodEnum.isbn) {
            identifierValidators.push(Validators.pattern(AouRegexUtil.isIsbn));
          } else if (im === ImportMethodEnum.pubmed) {
            identifierValidators.push(Validators.pattern(AouRegexUtil.isPmid || AouRegexUtil.isPmcid));
          }
        }
        fcIdentifier.setValidators(identifierValidators);
        fcService.setValidators(serviceValidators);
        fcService.updateValueAndValidity();
        fcIdentifier.updateValueAndValidity();
      }),
    ));
  }

  getMetadataFromService(): void {
    const identifier: string = this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.identifier).value;
    this._getMetadataFromServiceBS.next({
      friendlyName: this.selectedService.name,
      endpoint: this.selectedService.endpoint,
      identifierName: this.selectedService.identifierName,
      identifier: identifier?.trim(),
      importSource: this.selectedService.importSource,
    });
  }

  subtypeChange(subtype: string): void {
    this._subtypeBS.next(subtype);
  }

  openMultipleImport(): void {
    this._navigateBS.next(new Navigate([RoutesEnum.depositImportMultiple]));
  }

  trimWhenPasteIdentifier($event: ClipboardEvent): void {
    const pastedString: string = ($event.clipboardData || SsrUtil.window?.["clipboardData"]).getData("text");
    if (!isNonEmptyString(pastedString)) {
      return;
    }
    if (isNotNullNorUndefinedNorWhiteString(pastedString)) {
      this.formGroupRetrieveReferences.get(this.formDefinitionRetrieveReferences.identifier).setValue(pastedString.trim());
    }

    $event.preventDefault();
    $event.stopPropagation();
  }

  addOriginalTitle(): void {
    this.displayOriginalTitle = true;
    setTimeout(() => this.originalTitleTextLanguage?.focus(), 0);
  }

  verifyDate(date: string): void {
    this._dateThesisChangeBS.next(date);
  }

  get isThesisBefore2010OrAuthorBeforeUnige(): boolean {
    const date = this.mainFormGroup.get(this.formDefinitionDateThesis.date)?.value;
    const isDateBefore2010 = DepositFormRuleHelper.isDateBefore2010(date);
    const isAuthorBeforeUnige = DepositFormRuleHelper.isAuthorBeforeUnige(this.mainFormGroup);
    return isTrue(isDateBefore2010) || isTrue(isAuthorBeforeUnige);
  }
}

interface RetrieveReferenceService {
  name: string;
  endpoint?: ApiActionNameEnum;
  exampleIdentifier: string;
  exampleIdentifierSuffix?: string;
  isDisabled?: boolean;
  isGroupedInOtherMethod: boolean;
  labelRadioOption?: string;
  identifierName: string;
  importSource?: Enums.Deposit.ImportSourceEnum;
}

export enum ImportMethodEnum {
  manual = "manual",
  other = "other",
  pubmed = "Pubmed",
  isbn = "ISBN",
  doi = "DOI"
}

export const importMethodQueryParam: string = "method";
export const importIdentifierQueryParam: string = "identifier";

export interface GetMetadataFromServiceInfo {
  friendlyName: string;
  endpoint: ApiActionNameEnum;
  identifier: string;
  identifierName: string;
  importSource?: Enums.Deposit.ImportSourceEnum;
}
