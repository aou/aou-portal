/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-fourth-step-description.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {DepositFormDescriptionDatePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-date/deposit-form-description-date.presentational";
import {DepositFormDescriptionLanguagePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-language/deposit-form-description-language.presentational";
import {DepositFormDescriptionTextLanguagePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-text-language/deposit-form-description-text-language.presentational";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {DepositHelper} from "@app/features/deposit/helpers/deposit.helper";
import {
  DepositFormLanguage,
  FormComponentFormDefinitionDepositFormLanguage,
  FormComponentFormDefinitionDepositFormTextLanguage,
  FormComponentFormDefinitionFourthStepDescription,
  FormComponentFormDefinitionMain,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {depositActionNameSpace} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {SharedExternalDataJournalTitleAction} from "@app/shared/stores/external-data/external-data-journal/shared-external-data-journal-title.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DepositSubSubtype,
  DepositSubtype,
  JournalTitleDTO,
  Language,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedExternalDataJournalTitleState} from "@shared/stores/external-data/external-data-journal/shared-external-data-journal-title.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  ArrayUtil,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  QueryParameters,
  QueryParametersUtil,
  ResourceFileNameSpace,
  SOLIDIFY_CONSTANTS,
  SolidifyEmailValidator,
  SolidifyValidator,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-fourth-step-description",
  templateUrl: "./deposit-form-fourth-step-description.presentational.html",
  styleUrls: ["deposit-form-fourth-step-description.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormFourthStepDescriptionPresentational extends DepositAbstractFormPresentational implements OnInit {
  readonly KEY_CONTAINER_TITLE: string = "title";
  readonly KEY_CONTAINER_ISSN: string = "issn";

  formDefinitionMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  formDefinition: FormComponentFormDefinitionFourthStepDescription = new FormComponentFormDefinitionFourthStepDescription();
  formDefinitionText: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();
  formDefinitionLanguage: FormComponentFormDefinitionDepositFormLanguage = new FormComponentFormDefinitionDepositFormLanguage();

  typeFormGroup: FormGroup;

  displayMainInformationEditable: boolean = false;

  get isPublicationHost(): boolean {
    const subType = this.mainFormGroup.get(DepositFormRuleHelper.pathSubType).value;
    return DepositHelper.isPublicationHost(subType);
  }

  @Input()
  isValidationMode: boolean;

  @Input()
  isDepositNotBeingUpdated: boolean;

  @Input()
  isCorrigendumFilePresent: boolean;

  @Input()
  model: Deposit;

  @Input()
  listLanguages: Language[];

  @Input()
  listDepositSubtype: DepositSubtype[];

  @Input()
  listDepositSubSubtype: DepositSubSubtype[];

  protected _mainFormGroup: FormGroup;

  @Input()
  set mainFormGroup(value: FormGroup) {
    if (isNullOrUndefined(value)) {
      return;
    }
    this.formGroup = value.controls[this.formDefinitionMain.description] as FormGroup;
    this.typeFormGroup = value.controls[this.formDefinitionMain.type] as FormGroup;
    this._mainFormGroup = value;
  }

  get mainFormGroup(): FormGroup {
    return this._mainFormGroup;
  }

  private readonly _subtypeBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("subtypeChange")
  readonly subtypeObs: Observable<string | undefined> = ObservableUtil.asObservable(this._subtypeBS);

  @ViewChild("depositDescriptionLanguagePresentational")
  readonly depositDescriptionLanguagePresentational: DepositFormDescriptionLanguagePresentational;

  @ViewChild("depositFormDescriptionDatePresentational")
  readonly depositFormDescriptionDatePresentational: DepositFormDescriptionDatePresentational;

  @ViewChild("inputContainerTitle")
  readonly inputContainerTitle: ElementRef;

  @ViewChild("inputContainerIdentifierIssn")
  readonly inputContainerIdentifierIssn: ElementRef;

  @ViewChild("originalTitleTextLanguage")
  readonly originalTitleTextLanguage: DepositFormDescriptionTextLanguagePresentational<any>;

  displayOriginalTitle: boolean = false;

  display: boolean = true;

  containerTitleFilteredOptionsObs: Observable<JournalTitleDTO[]> = MemoizedUtil.list(this._store, SharedExternalDataJournalTitleState);
  containerTitleFilteredOptionsIsLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedExternalDataJournalTitleState);
  containerTitleFilteredOptionsIsLoadingChunkObs: Observable<boolean> = MemoizedUtil.select(this._store, SharedExternalDataJournalTitleState, state => state.isLoadingChunk);

  depositActionNameSpace: ResourceFileNameSpace = depositActionNameSpace;
  depositState: typeof DepositState = DepositState;

  containerTitleFilteredOptionsCallback: (option: JournalTitleDTO) => string = option => {
    if (isNonEmptyString(option.issn)) {
      return option.mainTitle + " - " + option.issn;
    }
    return option.mainTitle;
    // tslint:disable-next-line:semicolon
  };

  separatorKeys: string[] = ["Enter", SOLIDIFY_CONSTANTS.SEMICOLON, SOLIDIFY_CONSTANTS.SEPARATOR];
  formControlContainerTitleExtendedSearch: FormControl = new FormControl(false);
  defaultImage: string = "assets/images/deposit-thumbnail-upload-default-picture.png";

  get textAreaMaxSize(): number {
    return environment.textareaMaxSize;
  }

  constructor(protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService,
  ) {
    super(_changeDetector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._expandMainInformationEditableWhenValidator();
    this._expandMainInformationEditableWhenNoLanguage();

    this.subscribe(this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle).valueChanges.pipe(
      distinctUntilChanged(),
      filter(value => isNonEmptyString(value)),
      debounceTime(250),
      tap(value => {
        this.searchDataJournal(this.KEY_CONTAINER_TITLE, value);
      }),
    ));

    this.subscribe(this.mainFormGroup.get(DepositFormRuleHelper.pathIdentifierIssn).valueChanges.pipe(
      distinctUntilChanged(),
      filter(value => isNonEmptyString(value)),
      debounceTime(250),
      tap(value => {
        this.searchDataJournal(this.KEY_CONTAINER_ISSN, value);
      }),
    ));

    this.subscribe(this.formControlContainerTitleExtendedSearch.valueChanges.pipe(
      distinctUntilChanged(),
      tap(value => {
        const containerTitle = this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle)?.value;
        if (isNonEmptyString(containerTitle)) {
          this.searchDataJournal(this.KEY_CONTAINER_TITLE, this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle).value);
        }
      }),
    ));

    this.subscribe(this.mainFormGroup.get(DepositFormRuleHelper.pathOriginalTitleText).valueChanges.pipe(
      distinctUntilChanged(),
      tap(value => {
        this._setOriginalTitleLanguageRequiredIfTitleSet();
      }),
    ));
    this._setOriginalTitleLanguageRequiredIfTitleSet();

    const firstAbstractFormGroup = (this.mainFormGroup.get(DepositFormRuleHelper.pathAbstract) as FormArray).controls[0] as FormGroup;
    const depositMainLanguagesFormControl = (this.mainFormGroup.get(DepositFormRuleHelper.pathLanguages) as FormArray).controls[0].get(this.formDefinitionLanguage.language);
    this.subscribe(firstAbstractFormGroup.get(this.formDefinitionText.text).valueChanges.pipe(
      distinctUntilChanged(),
      filter(value => isNonEmptyString(value)),
      tap(value => {
        const langFormControl = firstAbstractFormGroup.get(this.formDefinitionText.lang);
        if (isNullOrUndefinedOrWhiteString(langFormControl.value)) {
          langFormControl.setValue(depositMainLanguagesFormControl.value);
        }
      }),
    ));
    this._computeValidationDoctorInfosAndLocalNumber();

    this.subscribe(this.mainFormGroup.get(DepositFormRuleHelper.pathSubType).valueChanges.pipe(
      distinctUntilChanged(),
      filter(value => isNonEmptyString(value)),
      tap(value => {
        this._computeValidationDoctorInfosAndLocalNumber();
      }),
    ));
  }

  private _expandMainInformationEditableWhenValidator(): void {
    if (isTrue(this.displayMainInformationEditable)) {
      return;
    }
    if (isTrue(this.isValidationMode)) {
      this.displayMainInformationInEditionMode();
    }
  }

  private _expandMainInformationEditableWhenNoLanguage(): void {
    if (isTrue(this.displayMainInformationEditable)) {
      return;
    }
    const depositLanguages = this.mainFormGroup.get(DepositFormRuleHelper.pathLanguages)?.value as string[];
    const originalTitleText = this.mainFormGroup.get(DepositFormRuleHelper.pathOriginalTitleText)?.value as string;
    const originalTitleLanguage = this.mainFormGroup.get(DepositFormRuleHelper.pathOriginalTitleLanguage)?.value as string;
    if (isNullOrUndefinedOrEmptyArray(depositLanguages) || (isNotNullNorUndefinedNorWhiteString(originalTitleText) && isNullOrUndefinedOrWhiteString(originalTitleLanguage))) {
      this.displayMainInformationInEditionMode();
    }
  }

  private _setOriginalTitleLanguageRequiredIfTitleSet(): void {
    const originalTitleText = this.mainFormGroup.get(DepositFormRuleHelper.pathOriginalTitleText)?.value;
    const formControlOriginalTitleLanguage = this.mainFormGroup.get(DepositFormRuleHelper.pathOriginalTitleLanguage);
    if (isNotNullNorUndefinedNorWhiteString(originalTitleText)) {
      formControlOriginalTitleLanguage.setValidators([Validators.required]);
    } else {
      formControlOriginalTitleLanguage.setValidators([]);
    }
  }

  displayMainInformationInEditionMode(): void {
    this.displayMainInformationEditable = true;
  }

  searchDataJournal(key: string, value: string): void {
    this.cleanContainerTitle();

    if (!isNonEmptyString(value)) {
      return;
    }

    if (DepositFormRuleHelper.ignoreContainerTitleIssnSearch(this.mainFormGroup)) {
      return;
    }
    const queryParameters = new QueryParameters();
    const search = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(search, key, value);
    MappingObjectUtil.set(search, "extendedSearch", this.formControlContainerTitleExtendedSearch.value);
    queryParameters.paging.pageSize = 10;
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(
      this._store, this._actions$,
      new SharedExternalDataJournalTitleAction.GetAll(queryParameters),
      SharedExternalDataJournalTitleAction.GetAllSuccess,
      result => {
        if (isNullOrUndefinedOrEmptyArray(result?.list?._data)) {
          this._notificationService.showInformation(LabelTranslateEnum.notificationNoResultsFound);
        }
      },
    ));
  }

  journalTitleDataSelected($event: MatAutocompleteSelectedEvent): void {
    this.cleanContainerTitle();
    const journalTitle = $event.option.value as JournalTitleDTO;
    this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle).setValue(journalTitle.mainTitle);

    const fcIssn = this.mainFormGroup.get(DepositFormRuleHelper.pathIdentifierIssn);
    if (isNotNullNorUndefined(fcIssn)) {
      fcIssn.setValue(journalTitle.issn);
    }
  }

  issnDataSelected($event: MatAutocompleteSelectedEvent): void {
    this.cleanContainerTitle();
    const journalTitle = $event.option.value as JournalTitleDTO;
    this.mainFormGroup.get(DepositFormRuleHelper.pathIdentifierIssn).setValue(journalTitle.issn);

    const fcTitle = this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle);
    if (isNotNullNorUndefined(fcTitle)) {
      fcTitle.setValue(journalTitle.mainTitle);
    }
  }

  formatLanguages(languages: DepositFormLanguage[]): string {
    return languages.map(l => l.language).join(" | ");
  }

  override externalDetectChanges(): void {
    this._changeDetector.detectChanges();
    this.depositFormDescriptionDatePresentational.externalDetectChanges();
  }

  subtypeChange(subtype: string): void {
    this._subtypeBS.next(subtype);

    if (!this.displayExtendedSearchCheckbox && isTrue(this.formControlContainerTitleExtendedSearch?.value)) {
      this.formControlContainerTitleExtendedSearch.setValue(false);
    }

    // Allow to force to redisplay all field that depend of subtype selected
    this.display = false;
    this._changeDetector.detectChanges();
    this.display = true;
  }

  addOriginalTitle(): void {
    this.displayOriginalTitle = true;
    setTimeout(() => this.originalTitleTextLanguage?.focus(), 0);
  }

  loadNextChunkContainerTitle(): void {
    this._store.dispatch(new SharedExternalDataJournalTitleAction.LoadNextChunkList());
  }

  cleanContainerTitle(): void {
    this._store.dispatch(new SharedExternalDataJournalTitleAction.Clean(false));
  }

  fixTitle(formControlTextLanguage: AbstractControl): void {
    const formControl = formControlTextLanguage?.get(this.formDefinitionText.text);
    let title: string = formControl?.value;
    if (isNullOrUndefinedOrWhiteString(title)) {
      return;
    }
    title = title.trim();
    if (title.lastIndexOf(SOLIDIFY_CONSTANTS.DOT) === title.length - 1) {
      title = title.substring(0, title.length - 1);
    }
    title = StringUtil.capitalize(title.toLowerCase());
    formControl.setValue(title);
  }

  fixDescription(formControlTextLanguage: FormGroup): void {
    const formControl = formControlTextLanguage?.get(this.formDefinitionText.text);
    let description: string = formControl?.value;
    if (isNullOrUndefinedOrWhiteString(description)) {
      return;
    }
    description = description.replace(/<p>|<\/p>/g, SOLIDIFY_CONSTANTS.LINE_BREAK);
    description = description.replace(/<br>/g, SOLIDIFY_CONSTANTS.LINE_BREAK);
    description = description.replace(/&nbsp;/g, SOLIDIFY_CONSTANTS.SPACE);
    description = description.trim();
    description = description.split(SOLIDIFY_CONSTANTS.LINE_BREAK)
      .map(text => text.trim())
      .filter(d => {
        const text = d.toLowerCase();
        return !["abstract", "résumé", "summary"].includes(text) && isNonEmptyString(text);
      })
      .join(SOLIDIFY_CONSTANTS.SPACE);
    description = description.replace(/\s+/g, SOLIDIFY_CONSTANTS.SPACE);
    formControl.setValue(description);
  }

  fixKeyword(formControl: AbstractControl): void {
    let keywords: string[] = formControl?.value;
    if (isNullOrUndefinedOrEmptyArray(keywords)) {
      return;
    }
    keywords = keywords.map(k => {
      if (isNonEmptyString(k)) {
        k = StringUtil.replaceAll(k, "*", "");
        k = StringUtil.capitalize(k.trim());
        const lastChar = k[k.length - 1];
        if (lastChar === SOLIDIFY_CONSTANTS.DOT) {
          k = k.substring(0, k.length - 1);
        }
      }
      return k;
    });
    keywords = ArrayUtil.distinct(keywords);

    formControl.setValue(keywords);
  }

  get displayButtonSearchContainerTitleOrIdentifier(): boolean {
    const subType = this.mainFormGroup.get(DepositFormRuleHelper.pathSubType)?.value;
    if (DepositFormRuleHelper.ignoreContainerTitleIssnSearchOfSubType(subType)) {
      return false;
    }

    const formControlTitle = this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle);
    const formControlIdentifier = this.mainFormGroup.get(DepositFormRuleHelper.pathIdentifierIssn);
    if (isNullOrUndefinedOrWhiteString(formControlTitle?.value) && isNotNullNorUndefinedNorWhiteString(formControlIdentifier?.value)) {
      return true;
    }

    return isNullOrUndefinedOrWhiteString(formControlIdentifier?.value) && isNotNullNorUndefinedNorWhiteString(formControlTitle?.value);
  }

  focusContainerTitleOrIdentifierIssn(): void {
    const formControlTitle = this.mainFormGroup.get(DepositFormRuleHelper.pathContainerTitle);
    const formControlIdentifier = this.mainFormGroup.get(DepositFormRuleHelper.pathIdentifierIssn);
    const field = isNullOrUndefinedOrWhiteString(formControlTitle?.value) && isNotNullNorUndefinedNorWhiteString(formControlIdentifier?.value) ? this.inputContainerIdentifierIssn : this.inputContainerTitle;
    setTimeout(() => field?.nativeElement?.focus(), 0);
  }

  get displayExtendedSearchCheckbox(): boolean {
    const subType = this.mainFormGroup.get(DepositFormRuleHelper.pathSubType)?.value;
    return DepositFormRuleHelper.ignoreContainerTitleIssnSearchOfSubType(subType) === false &&
      subType !== Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA;
  }

  isUpdatingDeposit(model: Deposit): boolean {
    return model.status === Enums.Deposit.StatusEnum.UPDATES_VALIDATION || this.model.status === Enums.Deposit.StatusEnum.IN_EDITION;
  }

  private _computeValidationDoctorInfosAndLocalNumber(): void {
    const subType = this.mainFormGroup.get(DepositFormRuleHelper.pathSubType).value;
    const formControlDoctorEmail = this.mainFormGroup.get(DepositFormRuleHelper.pathDoctorEmail) as FormControl;
    const formControlDoctorAddress = this.mainFormGroup.get(DepositFormRuleHelper.pathDoctorAddress) as FormControl;
    const fcLocalNumber = this.mainFormGroup.get(DepositFormRuleHelper.pathIdentifierLocalNumber) as FormArray;

    const isThesisBefore2010OrAuthorBeforeUnige: boolean = DepositFormRuleHelper.isThesisBefore2010OrAuthorBeforeUnige(this.mainFormGroup);
    if (!this.isUpdatingDeposit(this.model) &&
      (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS)
      && !isThesisBefore2010OrAuthorBeforeUnige) {
      fcLocalNumber?.setValidators([SolidifyValidator, Validators.required]);
      formControlDoctorEmail?.setValidators([SolidifyValidator, Validators.required, SolidifyEmailValidator]);
      formControlDoctorAddress?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      fcLocalNumber?.setValidators([SolidifyValidator]);
      formControlDoctorEmail?.setValidators([SolidifyValidator, SolidifyEmailValidator]);
      formControlDoctorAddress?.setValidators([SolidifyValidator]);
    }
    fcLocalNumber?.updateValueAndValidity();
    formControlDoctorEmail?.updateValueAndValidity();
    formControlDoctorAddress?.updateValueAndValidity();
  }

  datesChange(): void {
    this._computeValidationDoctorInfosAndLocalNumber();
  }
}
