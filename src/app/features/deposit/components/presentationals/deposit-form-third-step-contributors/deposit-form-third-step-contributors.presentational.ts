/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-third-step-contributors.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {
  DepositTableContributorContainer,
  DepositTableContributorModeEnum,
  DepositTableContributorTypeEnum,
} from "@app/features/deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {DepositCreateResearchGroupDialog} from "@app/features/deposit/components/dialogs/deposit-create-research-group/deposit-create-research-group.dialog";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {
  DepositDefaultData,
  DepositFormContributor,
  DepositFormStepContributors,
  FormComponentFormDefinitionDepositFormContributor,
  FormComponentFormDefinitionMain,
  FormComponentFormDefinitionThirdStepContributors,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositPersonResearchGroupAction} from "@app/features/deposit/stores/person/research-group/deposit-person-research-group.action";
import {DepositPersonStructureAction} from "@app/features/deposit/stores/person/structure/deposit-person-structure.action";
import {sharedResearchGroupActionNameSpace} from "@app/shared/stores/research-group/shared-research-group.action";
import {Enums} from "@enums";
import {
  ResearchGroup,
  Structure,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  ofActionCompleted,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  DialogUtil,
  isEmptyArray,
  isNonEmptyArray,
  isNonEmptyString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  NotificationService,
  OrderEnum,
  OverlayPositionEnum,
  ResourceNameSpace,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-third-step-contributors",
  templateUrl: "./deposit-form-third-step-contributors.presentational.html",
  styleUrls: ["deposit-form-third-step-contributors.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormThirdStepContributorsPresentational extends DepositAbstractFormPresentational implements OnInit {
  formDefinitionMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  formDefinition: FormComponentFormDefinitionThirdStepContributors = new FormComponentFormDefinitionThirdStepContributors();
  isThesisOrMasterDegreeOrOfAdvancedStudies: boolean = false;
  isEditable: boolean = true;

  readonly KEY_RESEARCH_GROUP_SEARCH_NAME: string = SharedResearchGroupState.FIELD_SEARCH_NAME;
  sharedResearchGroupSort: Sort<ResearchGroup> = {
    field: SharedResearchGroupState.FIELD_SORT_VALUE,
    order: OrderEnum.ascending,
  };
  sharedResearchGroupActionNameSpace: ResourceNameSpace = sharedResearchGroupActionNameSpace;
  sharedResearchGroupState: typeof SharedResearchGroupState = SharedResearchGroupState;
  researchGroupLabel: (value: ResearchGroup) => string = value => SharedResearchGroupState.labelCallback(value, this._translateService);
  researchGroupPreTreatmentHighlightText: (result: string) => string = SharedResearchGroupState.preTreatmentHighlightText;
  researchGroupPostTreatmentHighlightText: (result: string, resultBeforePreTreatment: string) => string = SharedResearchGroupState.postTreatmentHighlightText;

  formDefinitionContributor: FormComponentFormDefinitionDepositFormContributor = new FormComponentFormDefinitionDepositFormContributor();

  displayResearchGroup: boolean = true;
  displayStructure: boolean = true;

  protected _mainFormGroup: FormGroup;

  @Input()
  listStructure: Structure[];

  @Input()
  isLoadingStructure: boolean;

  @Input()
  defaultData: DepositDefaultData;

  @Input()
  subModel: DepositFormStepContributors;

  @Input()
  isValidationMode: boolean;

  @Input()
  defaultContributorRole: Enums.Deposit.RoleContributorEnum;

  @Input()
  userConnected: User;

  @Input()
  notifyContributorsFromExternalSource: boolean;

  structureLabel: (value: Structure) => string = value => SharedStructureState.labelCallback(value, this._translateService);

  @Input()
  set mainFormGroup(value: FormGroup) {
    if (isNullOrUndefined(value)) {
      return;
    }
    const subtype = value.get(DepositFormRuleHelper.pathSubType)?.value;
    const isThesisBefore2010OrAuthorBeforeUnige: boolean = DepositFormRuleHelper.isThesisBefore2010OrAuthorBeforeUnige(value);

    this.isThesisOrMasterDegreeOrOfAdvancedStudies =
      (
        (
          subtype === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS ||
          subtype === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS
        )
        && !isThesisBefore2010OrAuthorBeforeUnige
      ) ||
      subtype === Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_OF_ADVANCED_STUDIES ||
      subtype === Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_DEGREE;

    this.formGroup = value.controls[this.formDefinitionMain.contributors] as FormGroup;
    this._mainFormGroup = value;

    this.subscribe(this.formGroup.get(this.formDefinition.isCollaboration).valueChanges.pipe(
      distinctUntilChanged(),
      tap(v => {
        const formArray = this.formGroup.get(this.formDefinition.collaborationMembersFrontend) as FormArray;
        if (!v) {
          while (formArray.length !== 0) {
            formArray.removeAt(0);
          }
        }
      }),
    ));

    if (isNonEmptyArray(this.formGroup.get(this.formDefinition.collaborationMembersFrontend).value)) {
      this.formGroup.get(this.formDefinition.isCollaboration).setValue(true);
    }
  }

  get mainFormGroup(): FormGroup {
    return this._mainFormGroup;
  }

  @ViewChild("contributorMembersFrontendContainer")
  readonly contributorMembersFrontendContainer: DepositTableContributorContainer;

  @ViewChild("collaborationMembersFrontendContainer")
  readonly collaborationMembersFrontendContainer: DepositTableContributorContainer;

  get overlayPositionEnum(): typeof OverlayPositionEnum {
    return OverlayPositionEnum;
  }

  constructor(protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService,
              private readonly _dialog: MatDialog,
              private readonly _translateService: TranslateService,
  ) {
    super(_changeDetector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this._observeIsBeforeUnigeFormControl());
  }

  private _observeIsBeforeUnigeFormControl(): Observable<boolean> {
    this._computeIsBeforeUnigeFormControl();
    return this.mainFormGroup.get(DepositFormRuleHelper.pathContributorsIsBeforeUnige).valueChanges.pipe(
      tap(isChecked => {
        this._computeIsBeforeUnigeFormControl();
      }),
    );
  }

  private _computeIsBeforeUnigeFormControl(): void {
    const isBeforeUnige = isTrue(this.mainFormGroup.get(DepositFormRuleHelper.pathContributorsIsBeforeUnige).value);
    const formArray = this.mainFormGroup.get(DepositFormRuleHelper.pathStructure) as FormArray;
    if (isTrue(isBeforeUnige)) {
      formArray.setValue([]);
      formArray.disable();
      this._refreshStructureComponent();
    } else if (isNullOrUndefined(formArray.value) || isEmptyArray(formArray.value)) {
      this.autocompleteStructure();
    }
  }

  override externalDetectChanges(): void {
    this._changeDetector.detectChanges();
    this.contributorMembersFrontendContainer?.externalDetectChanges();
    this.collaborationMembersFrontendContainer?.externalDetectChanges();
  }

  get listDefaultGroup(): string[] {
    return this.isValidationMode ? [] : this.defaultData?.groups?.map(g => g.resId);
  }

  get listDefaultStructure(): string[] {
    return this.isValidationMode ? [] : this.defaultData?.academicStructures?.map(g => g.resId);
  }

  get modeTableContributorEnum(): typeof DepositTableContributorModeEnum {
    return DepositTableContributorModeEnum;
  }

  get disableStructure(): boolean {
    return isTrue(this.mainFormGroup.get(DepositFormRuleHelper.pathContributorsIsBeforeUnige)?.value);
  }

  autocompleteStructure(): void {
    const listExternalUid = this._getListExternalUid();
    if (isEmptyArray(listExternalUid)) {
      this._notifyStructureAutoUpdate();
      return;
    }

    const action = new DepositPersonStructureAction.RetrieveAllByExternalUid(listExternalUid);
    this.subscribe(this._actions$.pipe(
      ofActionCompleted(DepositPersonStructureAction.RetrieveAllByExternalUidSuccess),
      filter(result => result.action.parentAction === action),
      take(1),
      filter(result => result.result.successful),
      tap(result => {
        const listStructure = (result.action as DepositPersonStructureAction.RetrieveAllByExternalUidSuccess).list ?? [];

        const formArray = this.mainFormGroup.get(DepositFormRuleHelper.pathStructure) as FormArray;
        let counter = 0;
        listStructure.forEach(academicStructure => {
          const currentListId: string[] = formArray.value ?? [];
          if (currentListId.indexOf(academicStructure.resId) !== -1) {
            return;
          }
          counter++;
          formArray.setValue([...currentListId, academicStructure.resId]);
        });
        if (counter > 0) {
          this._refreshStructureComponent();
        }
        this._notifyStructureAutoUpdate(counter);
      }),
    ));

    this._store.dispatch(action);
  }

  addResearchGroup(): void {
    const formArray = this.mainFormGroup.get(DepositFormRuleHelper.pathGroup) as FormArray;
    if (isNullOrUndefined(formArray)) {
      return;
    }
    this.subscribe(DialogUtil.open(this._dialog, DepositCreateResearchGroupDialog, {}, {
      width: "900px",
    }, researchGroup => {
      const list = formArray.value ?? [];
      formArray.setValue([...list, researchGroup.resId]);
      this._refreshGroupComponent();
    }));
  }

  autocompleteResearchGroup(): void {
    const listExternalUid = this._getListExternalUid();
    if (isEmptyArray(listExternalUid)) {
      this._notifyResearchGroupAutoUpdate();
      return;
    }

    const action = new DepositPersonResearchGroupAction.RetrieveAllByExternalUid(listExternalUid);
    this.subscribe(this._actions$.pipe(
      ofActionCompleted(DepositPersonResearchGroupAction.RetrieveAllByExternalUidSuccess),
      filter(result => result.action.parentAction === action),
      take(1),
      filter(result => result.result.successful),
      tap(result => {
        const listResearchGroup = (result.action as DepositPersonResearchGroupAction.RetrieveAllByExternalUidSuccess).list ?? [];

        const formArray = this.mainFormGroup.get(DepositFormRuleHelper.pathGroup) as FormArray;
        let counter = 0;
        listResearchGroup.forEach(academicResearchGroup => {
          const currentList: string[] = formArray.value ?? [];
          if (currentList.indexOf(academicResearchGroup.resId) !== -1) {
            return;
          }
          counter++;
          formArray.setValue([...currentList, academicResearchGroup.resId]);
        });
        if (counter > 0) {
          this._refreshGroupComponent();
        }
        this._notifyResearchGroupAutoUpdate(counter);
      }),
    ));

    this._store.dispatch(action);
  }

  private _notifyStructureAutoUpdate(counter: number = 0): void {
    if (counter > 0) {
      this._notificationService.showSuccess(LabelTranslateEnum.contributorStructuresApplied, {counter: counter});
    } else {
      this._notificationService.showInformation(LabelTranslateEnum.noStructureRetrievedFromTheContributorsProfile);
    }
  }

  private _notifyResearchGroupAutoUpdate(counter: number = 0): void {
    if (counter > 0) {
      this._notificationService.showSuccess(LabelTranslateEnum.contributorResearchGroupsApplied, {counter: counter});
    } else {
      this._notificationService.showInformation(LabelTranslateEnum.noResearchGroupRetrievedFromTheContributorsProfile);
    }
  }

  private _getListExternalUid(): string[] {
    const formArrayContributor = this.mainFormGroup.get(DepositFormRuleHelper.pathContributorsContributorMembersFrontend) as FormArray;
    const listContributor: DepositFormContributor[] = formArrayContributor?.value;
    if (isNullOrUndefined(listContributor)) {
      return [];
    }

    return listContributor.filter(c => isNonEmptyString(c.cnIndividu)).map(c => c.cnIndividu + "@unige.ch");
  }

  private _refreshGroupComponent(): void {
    this.displayResearchGroup = false;
    this._changeDetector.detectChanges();
    setTimeout(() => {
      this.displayResearchGroup = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  private _refreshStructureComponent(): void {
    this.displayStructure = false;
    this._changeDetector.detectChanges();
    setTimeout(() => {
      this.displayStructure = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  changeAllContributorIntoCollaborator(): void {
    const formArrayContributor = this.getFormArrayInCurrentFormGroup(this.formDefinition.contributorMembersFrontend);
    const formArrayCollaborationMembers = this.getFormArrayInCurrentFormGroup(this.formDefinition.collaborationMembersFrontend);
    this.formGroup.get(this.formDefinition.isCollaboration).setValue(true);

    const listIndexContributorToRemoves = [];
    formArrayContributor.controls.forEach((formGroup: FormGroup, index) => {
      if (formGroup.get(this.formDefinitionContributor.type).value !== DepositTableContributorTypeEnum.collaboration) {
        listIndexContributorToRemoves.push(index);
      }
      if (isNullOrUndefinedOrWhiteString(formGroup.get(this.formDefinitionContributor.cnIndividu).value)) {
        return;
      }
      formGroup.get(this.formDefinitionContributor.role).setValue(Enums.Deposit.RoleContributorEnum.collaborator);
      formArrayCollaborationMembers.push(formGroup);
    });

    listIndexContributorToRemoves.reverse().forEach(index => {
      formArrayContributor.removeAt(index);
    });
    this.collaborationMembersFrontendContainer?.externalDetectChanges();
  }
}
