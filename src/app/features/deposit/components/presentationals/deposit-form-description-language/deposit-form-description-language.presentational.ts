/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-language.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {
  DepositFormLanguage,
  FormComponentFormDefinitionDepositFormLanguage,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {environment} from "@environments/environment";
import {Language} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {AouLabelUtil} from "@shared/utils/aou-label.util";
import {tap} from "rxjs/operators";
import {
  BackTranslatePipe,
  BaseFormDefinition,
  FormValidationHelper,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
  NotificationService,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-language",
  templateUrl: "./deposit-form-description-language.presentational.html",
  styleUrls: ["./deposit-form-description-language.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionLanguagePresentational,
    },
  ],
})
export class DepositFormDescriptionLanguagePresentational extends DepositAbstractFormListPresentational implements ControlValueAccessor, OnInit {
  backTranslatePipe: BackTranslatePipe;
  formDefinition: FormComponentFormDefinitionDepositFormLanguage = new FormComponentFormDefinitionDepositFormLanguage();

  formControl: FormControl = new FormControl();

  @Input()
  listLanguages: Language[];

  languageCallback: (value: Language) => string = (value: Language) => {
    if (isNonEmptyArray(value.labels)) {
      return AouLabelUtil.getTranslationFromListLabelsWithStore(this._store, value.labels);
    }
    return value.code;
    // tslint:disable-next-line:semicolon
  };

  @Input()
  tooltipText: string | undefined;

  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition];

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
    this.backTranslatePipe = new BackTranslatePipe(this._store, environment);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (FormValidationHelper.hasRequiredField(this.formArray)) {
      this.formControl.setValidators([Validators.required]);
    }
    this.formControl.setValue(this.formArray.value.map(v => v[this.formDefinition.language]).filter(l => isNotNullNorUndefinedNorWhiteString(l)));

    this.subscribe(this.formControl.valueChanges.pipe(
      tap((value: string[]) => {
        this.formArray.clear();
        if (isNonEmptyArray(value)) {
          value.forEach(lang => {
            if (isNullOrUndefinedOrWhiteString(lang)) {
              return;
            }
            const formGroup = DepositFormDescriptionLanguagePresentational.createLanguage(this._fb, this.formDefinition, {
              language: lang,
            } as DepositFormLanguage);
            this.formArray.push(formGroup);
          });
        }
      }),
    ));
  }

  static createLanguage(_fb: FormBuilder,
                        formDefinition: FormComponentFormDefinitionDepositFormLanguage,
                        dataset: DepositFormLanguage | undefined = undefined): FormGroup {
    const form = _fb.group({
      [formDefinition.language]: [undefined, [Validators.required, SolidifyValidator]],
    });

    if (isNotNullNorUndefined(dataset)) {
      form.setValue(dataset as any);
    }

    return form;
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositFormLanguage): FormGroup {
    return DepositFormDescriptionLanguagePresentational.createLanguage(fb, formDefinition);
  }
}
