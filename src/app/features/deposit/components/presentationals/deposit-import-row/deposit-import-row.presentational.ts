/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-import-row.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {ImportMethodEnum} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {DepositImportResultModel} from "@app/features/deposit/models/deposit-import-result.model";
import {Enums} from "@enums";
import {Deposit} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  BaseFormDefinition,
  FormValidationHelper,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  ObservableUtil,
  PropertyName,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-import-row",
  templateUrl: "./deposit-import-row.presentational.html",
  styleUrls: ["./deposit-import-row.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositImportRowPresentational,
    },
  ],
})
export class DepositImportRowPresentational extends SharedAbstractPresentational implements OnInit, ControlValueAccessor {
  formDefinition: FormComponentFormDefinitionDepositImportRow = new FormComponentFormDefinitionDepositImportRow();

  @Input()
  formGroup: FormGroup;

  @Input()
  result: DepositImportResultModel;

  private _readonly: boolean;

  @HostBinding("class.is-readonly")
  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    if (isTrue(value)) {
      this.formGroup.disable();
    } else {
      this.formGroup.enable();
    }
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private readonly _openDepositBS: BehaviorSubject<Deposit | undefined> = new BehaviorSubject<Deposit | undefined>(undefined);
  @Output("openDeposit")
  readonly openDepositObs: Observable<Deposit | undefined> = ObservableUtil.asObservable(this._openDepositBS);

  listImportService: ImportService[] = [
    {
      name: ImportMethodEnum.doi,
      exampleIdentifier: "10.1016/j.cis.2010.02.006",
      labelRadioOption: LabelTranslateEnum.importADepositFromDoi,
      identifierName: "doi",
      importSource: Enums.Deposit.ImportSourceEnum.DOI,
    },
    {
      name: ImportMethodEnum.pubmed,
      exampleIdentifier: "18951202",
      exampleIdentifierSuffix: "PMID",
      labelRadioOption: LabelTranslateEnum.importADepositFromPmid,
      identifierName: "pmid",
      importSource: Enums.Deposit.ImportSourceEnum.PMID,
    },
  ];

  @HostBinding("class.success")
  get resultSuccess(): boolean {
    return isNotNullNorUndefined(this.result?.deposit);
  }

  @HostBinding("class.error")
  get resultError(): boolean {
    return isNotNullNorUndefined(this.result) && (isNonEmptyString(this.result?.error) || isNullOrUndefined(this.result?.deposit));
  }

  @HostBinding("class.pending")
  get resultPending(): boolean {
    return isNullOrUndefined(this.result);
  }

  get resultIcon(): IconNameEnum {
    if (this.resultSuccess) {
      return IconNameEnum.success;
    } else {
      return IconNameEnum.error;
    }
  }

  constructor(protected readonly _fb: FormBuilder) {
    super();
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.formGroup, key);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  getSelectedService(serviceId: string): ImportService {
    return this.listImportService.find(s => s.name === serviceId);
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }

  seeDeposit(): void {
    if (this.resultSuccess) {
      this._openDepositBS.next(this.result.deposit);
    }
  }
}

interface ImportService {
  name: string;
  exampleIdentifier: string;
  exampleIdentifierSuffix?: string;
  labelRadioOption?: string;
  identifierName: string;
  importSource?: Enums.Deposit.ImportSourceEnum;
}

export class FormComponentFormDefinitionDepositImportRow extends BaseFormDefinition {
  @PropertyName() service: string;
  @PropertyName() id: string;
}

export interface DepositImportRowModel {
  id: string;
  service: ImportMethodEnum;
}
