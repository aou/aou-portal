/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-publisher.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";
import {
  FormBuilder,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {FormComponentFormDefinitionDepositFormPublisher} from "@app/features/deposit/models/deposit-form-definition.model";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {NotificationService} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-publisher",
  templateUrl: "./deposit-form-description-publisher.presentational.html",
  styleUrls: ["./deposit-form-description-publisher.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionPublisherPresentational,
    },
  ],
})
export class DepositFormDescriptionPublisherPresentational extends DepositAbstractFormPresentational {
  formDefinition: FormComponentFormDefinitionDepositFormPublisher = new FormComponentFormDefinitionDepositFormPublisher();

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService) {
    super(_changeDetector);
  }
}
