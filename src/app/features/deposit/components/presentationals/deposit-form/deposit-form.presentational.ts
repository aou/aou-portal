/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {STEPPER_GLOBAL_OPTIONS} from "@angular/cdk/stepper";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Injector,
  Input,
  NgZone,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  DepositTableContributorContainer,
  DepositTableContributorTypeEnum,
} from "@app/features/deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {DepositFormDescriptionClassificationPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-classification/deposit-form-description-classification.presentational";
import {DepositFormDescriptionCollectionPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-collection/deposit-form-description-collection.presentational";
import {DepositFormDescriptionContainerPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-container/deposit-form-description-container.presentational";
import {DepositFormDescriptionCorrectionPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-correction/deposit-form-description-correction.presentational";
import {DepositFormDescriptionDatasetPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-dataset/deposit-form-description-dataset.presentational";
import {DepositFormDescriptionDatePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-date/deposit-form-description-date.presentational";
import {DepositFormDescriptionFundingPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-funding/deposit-form-description-funding.presentational";
import {DepositFormDescriptionLanguagePresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-language/deposit-form-description-language.presentational";
import {DepositFormDescriptionLinkPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-link/deposit-form-description-link.presentational";
import {DepositFormDescriptionPublicationIdentifierPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-publication-identifier/deposit-form-description-publication-identifier.presentational";
import {DepositFormDescriptionTextLanguageListPresentational} from "@app/features/deposit/components/presentationals/deposit-form-description-text-language-list/deposit-form-description-text-language-list.presentational";
import {
  DepositFormFirstStepTypePresentational,
  GetMetadataFromServiceInfo,
} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {DepositFormFourthStepDescriptionPresentational} from "@app/features/deposit/components/presentationals/deposit-form-fourth-step-description/deposit-form-fourth-step-description.presentational";
import {DepositFormSecondStepFilesPresentational} from "@app/features/deposit/components/presentationals/deposit-form-second-step-files/deposit-form-second-step-files.presentational";
import {DepositFormThirdStepContributorsPresentational} from "@app/features/deposit/components/presentationals/deposit-form-third-step-contributors/deposit-form-third-step-contributors.presentational";
import {DepositModeEnum} from "@app/features/deposit/components/routables/deposit-create-edit/deposit-create-edit.routable";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {DepositHelper} from "@app/features/deposit/helpers/deposit.helper";
import {DocumentFileUploadHelper} from "@app/features/deposit/helpers/document-file-upload.helper";
import {
  DepositDefaultData,
  DepositFormAbstractContributor,
  DepositFormContributor,
  DepositFormDates,
  DepositFormModel,
  DepositFormStepContributors,
  DepositFormStepDescription,
  FormComponentFormDate,
  FormComponentFormDefinitionDepositFormClassification,
  FormComponentFormDefinitionDepositFormCollection,
  FormComponentFormDefinitionDepositFormContainer,
  FormComponentFormDefinitionDepositFormContributor,
  FormComponentFormDefinitionDepositFormCorrection,
  FormComponentFormDefinitionDepositFormDataset,
  FormComponentFormDefinitionDepositFormDates,
  FormComponentFormDefinitionDepositFormFunding,
  FormComponentFormDefinitionDepositFormLanguage,
  FormComponentFormDefinitionDepositFormLink,
  FormComponentFormDefinitionDepositFormPage,
  FormComponentFormDefinitionDepositFormPublicationIdentifiers,
  FormComponentFormDefinitionDepositFormPublisher,
  FormComponentFormDefinitionDepositFormTextLanguage,
  FormComponentFormDefinitionFirstStepType,
  FormComponentFormDefinitionFourthStepDescription,
  FormComponentFormDefinitionMain,
  FormComponentFormDefinitionThirdStepContributors,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {ModelFormControlEventDeposit} from "@app/features/deposit/models/model-form-control-event-deposit.model";
import {DepositDocumentFileState} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DepositSubSubtype,
  DepositSubtype,
  Language,
  MetadataDifference,
  Romeo,
  Structure,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedStepperPresentational} from "@shared/components/presentationals/shared-stepper/shared-stepper.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {Step} from "@shared/models/step.model";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  tap,
} from "rxjs/operators";
import {
  AbstractPresentational,
  ArrayUtil,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  isArray,
  isEmptyString,
  isFalse,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorEmptyObject,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  isNumber,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ObservableUtil,
  ScrollService,
  SOLIDIFY_CONSTANTS,
  SolidifyEmailValidator,
  SolidifyValidator,
  ValidationUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form",
  templateUrl: "./deposit-form.presentational.html",
  styleUrls: ["deposit-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false},
  }],
})
export class DepositFormPresentational extends AbstractPresentational implements OnInit {
  formDefinitionMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  formDefinitionFirstStepType: FormComponentFormDefinitionFirstStepType = new FormComponentFormDefinitionFirstStepType();
  formDefinitionThirdStepContributors: FormComponentFormDefinitionThirdStepContributors = new FormComponentFormDefinitionThirdStepContributors();
  formDefinitionFourthStepDescription: FormComponentFormDefinitionFourthStepDescription = new FormComponentFormDefinitionFourthStepDescription();
  formDefinitionDepositFormTextLanguage: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();
  formDefinitionDepositFormPublisher: FormComponentFormDefinitionDepositFormPublisher = new FormComponentFormDefinitionDepositFormPublisher();
  formDefinitionDepositFormDataset: FormComponentFormDefinitionDepositFormDataset = new FormComponentFormDefinitionDepositFormDataset();
  formDefinitionDepositFormContainer: FormComponentFormDefinitionDepositFormContainer = new FormComponentFormDefinitionDepositFormContainer();
  formDefinitionDepositFormLink: FormComponentFormDefinitionDepositFormLink = new FormComponentFormDefinitionDepositFormLink();
  formDefinitionDepositFormCorrection: FormComponentFormDefinitionDepositFormCorrection = new FormComponentFormDefinitionDepositFormCorrection();
  formDefinitionDepositFormFunding: FormComponentFormDefinitionDepositFormFunding = new FormComponentFormDefinitionDepositFormFunding();
  formDefinitionDepositFormCollection: FormComponentFormDefinitionDepositFormCollection = new FormComponentFormDefinitionDepositFormCollection();
  formDefinitionDepositFormClassification: FormComponentFormDefinitionDepositFormClassification = new FormComponentFormDefinitionDepositFormClassification();
  formDefinitionDepositFormPage: FormComponentFormDefinitionDepositFormPage = new FormComponentFormDefinitionDepositFormPage();
  formDefinitionDepositFormDate: FormComponentFormDefinitionDepositFormDates = new FormComponentFormDefinitionDepositFormDates();
  formDefinitionDepositFormPublicationIdentifiers: FormComponentFormDefinitionDepositFormPublicationIdentifiers = new FormComponentFormDefinitionDepositFormPublicationIdentifiers();
  formDefinitionDepositFormLanguage: FormComponentFormDefinitionDepositFormLanguage = new FormComponentFormDefinitionDepositFormLanguage();
  formDefinitionDepositFormContributor: FormComponentFormDefinitionDepositFormContributor = new FormComponentFormDefinitionDepositFormContributor();

  formDefinitionDateThesis: FormComponentFormDate = new FormComponentFormDate();

  form: FormGroup;
  formGroupFirstStepType: FormGroup;
  formGroupSecondStepFiles: FormGroup;
  formGroupThirdStepContributors: FormGroup;
  formGroupFourthStepDescription: FormGroup;
  convertedFormToDepositFormModelForSummary: DepositFormModel;

  linear: boolean = false;

  readonly MIN_YEAR_THESIS: number = 1873;

  get MAX_YEAR_THESIS(): number {
    return new Date().getFullYear() + 1;
  }

  private readonly _JEL: string = "JEL";

  private get rootUrl(): string[] {
    return [RoutesEnum.depositDetail, this.model.resId, DepositRoutesEnum.edit];
  }

  errorFirstStepType: number = 0;
  errorSecondStepFiles: number = 0;
  errorThirdStepContributors: number = 0;
  errorFourthStepDescription: number = 0;

  get documentFileUploadHelper(): typeof DocumentFileUploadHelper {
    return DocumentFileUploadHelper;
  }

  get depositStatusEnum(): typeof Enums.Deposit.StatusEnum {
    return Enums.Deposit.StatusEnum;
  }

  suffixUrlMatcher: (route: ActivatedRoute) => string = (route: ActivatedRoute) => route.snapshot.paramMap.get(AppRoutesEnum.paramStepWithoutPrefixParam);

  steps: Step[];

  @Input()
  depositMode: DepositModeEnum | undefined;

  @Input()
  isFilePresent: boolean;

  @Input()
  isValidationMode: boolean = false;

  private _listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>;

  @Input()
  set listCurrentStatus(value: MappingObject<Enums.DocumentFile.StatusEnum, number>) {
    this._listCurrentStatus = value;
    this.errorSecondStepFiles = DocumentFileUploadHelper.numberFilesInError(this.listCurrentStatus) + (DocumentFileUploadHelper.hasFilesDuplicate(this.listCurrentStatus) ? 1 : 0);
    this.stepper?.triggerDetectChanges();
  }

  get listCurrentStatus(): MappingObject<Enums.DocumentFile.StatusEnum, number> {
    return this._listCurrentStatus;
  }

  @Input()
  defaultData: DepositDefaultData;

  @Input()
  listDepositSubtype: DepositSubtype[];

  @Input()
  listDepositSubSubtype: DepositSubSubtype[];

  @Input()
  listStructure: Structure[];

  @Input()
  listLanguages: Language[];

  @Input()
  listFiles: Language[];

  get isCorrigendumFilePresent(): boolean {
    const listFiles = MemoizedUtil.selectSnapshot(this._store, DepositDocumentFileState, state => state.list);
    return listFiles?.some(f => f.documentFileType?.value === environment.corrigendumValue);
  }

  @Input()
  isLoadingStructure: boolean;

  @Input()
  listDifferences: MappingObject<string, MappingObject<string, MetadataDifference[]>>;

  isDepositNotBeingUpdated: boolean;

  private _model: Deposit;

  @Input()
  @HostBinding("class.is-edit")
  set model(value: Deposit) {
    this._model = value;
    this._computeIsDepositNotBeingUpdated();
  }

  get model(): Deposit {
    return this._model;
  }

  private _computeIsDepositNotBeingUpdated(): void {
    this.isDepositNotBeingUpdated = true;
    if (this.model?.status) {
      this.isDepositNotBeingUpdated = this.model.status !== Enums.Deposit.StatusEnum.UPDATES_VALIDATION && this.model.status !== Enums.Deposit.StatusEnum.IN_EDITION;
    }
  }

  get hasMetadataUpdate(): boolean {
    return isNotNullNorUndefined(this.listDifferences) && MappingObjectUtil.size(this.listDifferences) > 0;
  }

  isLoadingOnCreate: boolean = false;

  @Input()
  romeo: Romeo;

  @Input()
  defaultContributorRole: Enums.Deposit.RoleContributorEnum;

  @Input()
  userConnected: User;

  depositFormModel: DepositFormModel;

  get isImported(): boolean {
    return isNotNullNorUndefined(this.importedSource);
  }

  get depositModeEnum(): typeof DepositModeEnum {
    return DepositModeEnum;
  }

  importedSource: Enums.Deposit.ImportSourceEnum | undefined = undefined;

  @Input()
  step: Enums.Deposit.StepEnum;

  private readonly _titleBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("titleChange")
  readonly titleObs: Observable<string | undefined> = ObservableUtil.asObservable(this._titleBS);

  private readonly _subtypeBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("subtypeChange")
  readonly subtypeObs: Observable<string | undefined> = ObservableUtil.asObservable(this._subtypeBS);

  private readonly _getMetadataFromServiceBS: BehaviorSubject<GetMetadataFromServiceInfo | undefined> = new BehaviorSubject<GetMetadataFromServiceInfo | undefined>(undefined);
  @Output("getMetadataFromServiceChange")
  readonly getMetadataFromServiceObs: Observable<GetMetadataFromServiceInfo | undefined> = ObservableUtil.asObservable(this._getMetadataFromServiceBS);

  private readonly _navigateToStepBS: BehaviorSubject<Enums.Deposit.StepEnum> = new BehaviorSubject<Enums.Deposit.StepEnum>(undefined);
  @Output("navigateToStepChange")
  readonly navigateToStepObs: Observable<Enums.Deposit.StepEnum> = ObservableUtil.asObservable(this._navigateToStepBS);

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _refreshValidationErrorBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("refreshValidationErrorChange")
  readonly refreshValidationErrorObs: Observable<void> = ObservableUtil.asObservable(this._refreshValidationErrorBS);

  protected readonly _sortValuesBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("sendSortValues")
  readonly sortValuesObs: Observable<string[]> = ObservableUtil.asObservable(this._sortValuesBS);

  protected readonly _formLoadedBS: BehaviorSubject<FormGroup> = new BehaviorSubject<FormGroup>(undefined);
  @Output("formLoaded")
  readonly formLoadedObs: Observable<FormGroup> = ObservableUtil.asObservable(this._formLoadedBS);

  @ViewChild("depositTypePresentational")
  readonly depositTypePresentational: DepositFormFirstStepTypePresentational;

  @ViewChild("depositFilesPresentational")
  readonly depositFilesPresentational: DepositFormSecondStepFilesPresentational;

  @ViewChild("depositContributorsPresentational")
  readonly depositContributorsPresentational: DepositFormThirdStepContributorsPresentational;

  @ViewChild("depositDescriptionPresentational")
  readonly depositDescriptionPresentational: DepositFormFourthStepDescriptionPresentational;

  @ViewChild("stepper")
  readonly stepper: SharedStepperPresentational;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get depositStepEnum(): typeof Enums.Deposit.StepEnum {
    return Enums.Deposit.StepEnum;
  }

  get targetRoutingAfterSaveEnum(): typeof Enums.Deposit.TargetRoutingAfterSaveEnum {
    return Enums.Deposit.TargetRoutingAfterSaveEnum;
  }

  protected readonly _createBS: BehaviorSubject<ModelFormControlEventDeposit | undefined> = new BehaviorSubject<ModelFormControlEventDeposit | undefined>(undefined);
  @Output("createChange")
  readonly createObs: Observable<ModelFormControlEventDeposit | undefined> = ObservableUtil.asObservable(this._createBS);

  protected readonly _updateBS: BehaviorSubject<ModelFormControlEventDeposit | undefined> = new BehaviorSubject<ModelFormControlEventDeposit | undefined>(undefined);
  @Output("updateChange")
  readonly updateObs: Observable<ModelFormControlEventDeposit | undefined> = ObservableUtil.asObservable(this._updateBS);

  protected readonly _submitBS: BehaviorSubject<ModelFormControlEventDeposit | undefined> = new BehaviorSubject<ModelFormControlEventDeposit | undefined>(undefined);
  @Output("submitChange")
  readonly submitObs: Observable<ModelFormControlEventDeposit | undefined> = ObservableUtil.asObservable(this._submitBS);

  protected readonly _exitBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("exitChange")
  readonly exitObs: Observable<void> = ObservableUtil.asObservable(this._exitBS);

  protected readonly _issnBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("issnChange")
  readonly issnObs: Observable<string | undefined> = ObservableUtil.asObservable(this._issnBS);

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _scrollService: ScrollService,
              private readonly _zone: NgZone,
              protected readonly _dialog: MatDialog,
              private readonly _store: Store,
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._initNewForm();
    this._defineStep();
    if (isNotNullNorUndefined(this.model)) {
      this._bindFormTo(this.model);
    } else {
      this.subtypeChange(undefined);
    }
    this._addLogical();
    this._addStepErrorWatcher();
    if (this.step === Enums.Deposit.StepEnum.SUMMARY) {
      this.convertedFormToDepositFormModelForSummary = this.convertFormToDepositFormModel();
    }
    setTimeout(() => {
      this._formLoadedBS.next(this.form);
    }, 0);
  }

  externalDetectChanges(): void {
    this._changeDetectorRef.detectChanges();
  }

  private _defineStep(): void {
    this.steps = [
      {
        id: Enums.Deposit.StepEnum.FILES,
        titleToTranslate: LabelTranslateEnum.filesAndDistributionConditions,
        titleShortToTranslate: LabelTranslateEnum.files,
        suffixUrl: Enums.Deposit.StepEnum.FILES,
        numberErrors: () => this.errorSecondStepFiles,
        route: () => [...this.rootUrl, Enums.Deposit.StepEnum.FILES],
      },
      {
        id: Enums.Deposit.StepEnum.CONTRIBUTORS,
        titleToTranslate: LabelTranslateEnum.contributorsAndAffiliations,
        titleShortToTranslate: LabelTranslateEnum.contributors,
        suffixUrl: Enums.Deposit.StepEnum.CONTRIBUTORS,
        numberErrors: () => this.errorThirdStepContributors,
        route: () => [...this.rootUrl, Enums.Deposit.StepEnum.CONTRIBUTORS],
        stepFormAbstractControl: this.formGroupThirdStepContributors,
      },
      {
        id: Enums.Deposit.StepEnum.DESCRIPTION,
        titleToTranslate: LabelTranslateEnum.descriptionOfTheDocument,
        titleShortToTranslate: LabelTranslateEnum.description,
        suffixUrl: Enums.Deposit.StepEnum.DESCRIPTION,
        numberErrors: () => this.errorFourthStepDescription,
        route: () => [...this.rootUrl, Enums.Deposit.StepEnum.DESCRIPTION],
        stepFormAbstractControl: this.formGroupFourthStepDescription,
      },
      {
        id: Enums.Deposit.StepEnum.SUMMARY,
        titleToTranslate: LabelTranslateEnum.finalVerificationAndSubmission,
        titleShortToTranslate: LabelTranslateEnum.confirm,
        suffixUrl: Enums.Deposit.StepEnum.SUMMARY,
        route: () => [...this.rootUrl, Enums.Deposit.StepEnum.SUMMARY],
      },
    ];
  }

  protected _initNewForm(): void {
    this.formGroupFirstStepType = this._fb.group({
      [this.formDefinitionFirstStepType.title]: this._fb.group({
        [this.formDefinitionDepositFormTextLanguage.text]: ["", [Validators.required, SolidifyValidator]],
        [this.formDefinitionDepositFormTextLanguage.lang]: ["", [SolidifyValidator]],
      }),
      [this.formDefinitionFirstStepType.type]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinitionFirstStepType.subtype]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinitionFirstStepType.subsubtype]: ["", [SolidifyValidator]],
    });
    this.formGroupSecondStepFiles = this._fb.group({});
    this.formGroupThirdStepContributors = this._fb.group({
      [this.formDefinitionThirdStepContributors.contributorMembersFrontend]: this._fb.array([], [SolidifyValidator]),
      [this.formDefinitionThirdStepContributors.collaborationMembersFrontend]: this._fb.array([], [SolidifyValidator]),
      [this.formDefinitionThirdStepContributors.isCollaboration]: [false, [SolidifyValidator]],
      [this.formDefinitionThirdStepContributors.isBeforeUnige]: [false, [SolidifyValidator]],
      [this.formDefinitionThirdStepContributors.academicStructures]: [],
      [this.formDefinitionThirdStepContributors.groups]: [],
    });
    this.formGroupFourthStepDescription = this._fb.group({
      [this.formDefinitionFourthStepDescription.languages]: this._fb.array([
        DepositFormDescriptionLanguagePresentational.createLanguage(this._fb, this.formDefinitionDepositFormLanguage),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.originalTitle]: this._fb.group({
        [this.formDefinitionDepositFormTextLanguage.text]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormTextLanguage.lang]: ["", [SolidifyValidator]],
      }),
      [this.formDefinitionFourthStepDescription.dates]: DepositFormDescriptionDatePresentational.createOrBindDate(this._fb, this.formDefinitionDepositFormDate),
      [this.formDefinitionFourthStepDescription.pages]: this._fb.group({
        [this.formDefinitionDepositFormPage.paging]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormPage.other]: ["", [SolidifyValidator]],
      }),
      [this.formDefinitionFourthStepDescription.identifiers]: DepositFormDescriptionPublicationIdentifierPresentational.createOrBindPublicationIdentifier(this._fb, this.formDefinitionDepositFormPublicationIdentifiers),
      [this.formDefinitionFourthStepDescription.publisherVersionUrl]: ["", [SolidifyValidator, Validators.pattern(AouRegexUtil.url)]],
      [this.formDefinitionFourthStepDescription.abstracts]: this._fb.array([
        DepositFormDescriptionTextLanguageListPresentational.createTextLanguage(this._fb, this.formDefinitionDepositFormTextLanguage),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.note]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.jel]: [[], [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.keywords]: [[], [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.classifications]: this._fb.array([
        DepositFormDescriptionClassificationPresentational.createClassification(this._fb, this.formDefinitionDepositFormClassification),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.discipline]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.mandator]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.publisher]: this._fb.group({
        [this.formDefinitionDepositFormPublisher.name]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormPublisher.place]: ["", [SolidifyValidator]],
      }),
      [this.formDefinitionFourthStepDescription.collections]: this._fb.array([
        DepositFormDescriptionCollectionPresentational.createCollection(this._fb, this.formDefinitionDepositFormCollection),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.edition]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.award]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.datasets]: this._fb.array([
        DepositFormDescriptionDatasetPresentational.createDataset(this._fb, this.formDefinitionDepositFormDataset),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.container]: this._fb.group({
        [this.formDefinitionDepositFormContainer.title]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.editor]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.conferenceSubtitle]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.conferencePlace]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.volume]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.issue]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.conferenceDate]: ["", [SolidifyValidator]],
        [this.formDefinitionDepositFormContainer.specialIssue]: ["", [SolidifyValidator]],
      }),
      [this.formDefinitionFourthStepDescription.links]: this._fb.array([
        DepositFormDescriptionLinkPresentational.createLink(this._fb, this.formDefinitionDepositFormLink, this.formDefinitionDepositFormTextLanguage),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.aouCollection]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.corrections]: this._fb.array([
        DepositFormDescriptionCorrectionPresentational.createCorrection(this._fb, this.formDefinitionDepositFormCorrection),
      ], [SolidifyValidator]),
      [this.formDefinitionFourthStepDescription.doctorEmail]: ["", [SolidifyValidator, SolidifyEmailValidator]],
      [this.formDefinitionFourthStepDescription.doctorAddress]: ["", [SolidifyValidator]],
      [this.formDefinitionFourthStepDescription.fundings]: this._fb.array([], [/*CustomFormValidator.minLengthArray(1), */SolidifyValidator]),
    });

    this.form = this._fb.group({
      [this.formDefinitionMain.type]: this.formGroupFirstStepType,
      [this.formDefinitionMain.contributors]: this.formGroupThirdStepContributors,
      [this.formDefinitionMain.description]: this.formGroupFourthStepDescription,
      [this.formDefinitionDateThesis.date]: ["", [SolidifyValidator, Validators.pattern("^(2|1){1}[0-9]{3}")]],
    });
  }

  static patchForm(formGroup: FormGroup, value: any): void {
    if (formGroup instanceof FormArray) {
      // eslint-disable-next-line no-console
      console.warn("UNABLE TO PATCH A FORM ARRAY, USE PUSH INSTEAD");
      return;
    }
    if (isNullOrUndefined(value)) {
      return;
    }
    formGroup.patchValue(value);
  }

  static patchFormWithKey(formGroup: FormGroup, key: string, value: any): void {
    this.patchForm(formGroup.get(key) as FormGroup, value);
  }

  protected _bindFormTo(deposit: Deposit): void {
    this.importedSource = deposit.importSource;
    const depositFormModel: DepositFormModel = JSON.parse(deposit.formData);
    this._mapDepositFormModelToForm(depositFormModel);
  }

  applyMetadataFromService(depositFormModel: DepositFormModel, importSource: Enums.Deposit.ImportSourceEnum | undefined): void {
    this.importedSource = importSource;
    this._mapDepositFormModelToForm(depositFormModel);
  }

  private _mapDepositFormModelToForm(depositFormModel: DepositFormModel): void {
    this.subtypeChange(depositFormModel?.type?.subtype);
    this.depositFormModel = depositFormModel;
    this._mapDepositFormModelToFormOnType(depositFormModel);
    this._mapDepositFormModelToFormOnContributors(depositFormModel);
    this._mapDepositFormModelToFormOnDescription(depositFormModel);
  }

  private _mapDepositFormModelToFormOnType(depositFormModel: DepositFormModel): void {
    if (isNotNullNorUndefined(depositFormModel.type)) {
      DepositFormPresentational.patchFormWithKey(this.formGroupFirstStepType, this.formDefinitionFirstStepType.title, depositFormModel.type?.title);
      DepositFormPresentational.patchForm(this.formGroupFirstStepType, depositFormModel.type);
    }
  }

  private _mapDepositFormModelToFormOnContributors(depositFormModel: DepositFormModel): void {
    if (isNotNullNorUndefined(depositFormModel.contributors)) {
      this._mapDepositFormModelToFormOnContributorsContributors(depositFormModel);
      this._mapDepositFormModelToFormOnContributorsStructure(depositFormModel);
      this._mapDepositFormModelToFormOnContributorsResearchGroups(depositFormModel);
      depositFormModel.contributors.isBeforeUnige = isTrue(depositFormModel.contributors.isBeforeUnige);
      this.form.get(DepositFormRuleHelper.pathContributorsIsBeforeUnige).setValue(depositFormModel.contributors.isBeforeUnige);
    }
  }

  private _mapDepositFormModelToFormOnContributorsContributors(depositFormModel: DepositFormModel): void {
    if (isNullOrUndefined(depositFormModel.contributors.isCollaboration)) {
      depositFormModel.contributors.isCollaboration = false;
      this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.contributorMembersFrontend)
        .setValue(depositFormModel.contributors.contributors);
    } else {
      this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.isCollaboration)
        .setValue(depositFormModel.contributors.isCollaboration);
      const indexOfFirstCollaborator = depositFormModel?.contributors?.contributors?.findIndex(c => c.contributor?.role === Enums.Deposit.RoleContributorEnum.collaborator);
      if (isNullOrUndefined(indexOfFirstCollaborator) || indexOfFirstCollaborator === -1) {
        this._restoreContributor(depositFormModel?.contributors?.contributors, this.formDefinitionThirdStepContributors.contributorMembersFrontend);
      } else {
        const listContributors = depositFormModel?.contributors?.contributors.slice(0, indexOfFirstCollaborator);
        const listCollaborationMembers = depositFormModel?.contributors?.contributors.slice(indexOfFirstCollaborator, depositFormModel?.contributors?.contributors.length);
        const listCollaboration = ArrayUtil.extract(listCollaborationMembers, c => isNotNullNorUndefinedNorEmptyObject(c.collaboration));
        this._restoreContributor([...listContributors, ...listCollaboration], this.formDefinitionThirdStepContributors.contributorMembersFrontend);
        this._restoreContributor(listCollaborationMembers, this.formDefinitionThirdStepContributors.collaborationMembersFrontend);
      }
    }
  }

  private _restoreContributor(listContributors: DepositFormAbstractContributor[], formArrayKey: string): void {
    if (!isArray(listContributors)) {
      return;
    }

    const formArray = this.formGroupThirdStepContributors.get(formArrayKey) as FormArray;
    formArray.clear();

    listContributors.forEach(contributor => {
      const contributorFormGroup = DepositTableContributorContainer.restoreContributorFromAbstractContributor(this._fb,
        this.formDefinitionDepositFormContributor,
        contributor,
        DepositTableContributorTypeEnum.external,
        this.defaultContributorRole);
      formArray.push(contributorFormGroup);
    });
  }

  private _mapDepositFormModelToFormOnContributorsStructure(depositFormModel: DepositFormModel): void {
    if (depositFormModel.contributors?.academicStructures?.length > 0) {
      const formControlGroups = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.academicStructures) as FormControl;
      formControlGroups.setValue(depositFormModel.contributors.academicStructures);
    }
  }

  private _mapDepositFormModelToFormOnContributorsResearchGroups(depositFormModel: DepositFormModel): void {
    if (depositFormModel.contributors?.groups?.length > 0) {
      const formControlGroups = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.groups) as FormControl;
      formControlGroups.setValue(depositFormModel.contributors.groups);
    }
  }

  private _mapDepositFormModelToFormOnDescription(depositFormModel: DepositFormModel): void {
    if (isNotNullNorUndefined(depositFormModel.description)) {
      this._mapDepositFormModelToFormOnDescriptionLanguages(depositFormModel);
      DepositFormPresentational.patchFormWithKey(this.formGroupFourthStepDescription, this.formDefinitionFourthStepDescription.originalTitle, depositFormModel.description?.originalTitle);
      this._computeDateValues(this.formGroupFirstStepType.get(this.formDefinitionFirstStepType.subtype).value);
      this._mapDepositFormModelToFormOnDescriptionDates(depositFormModel);
      DepositFormPresentational.patchFormWithKey(this.formGroupFourthStepDescription, this.formDefinitionFourthStepDescription.pages, depositFormModel.description?.pages);
      this._mapDepositFormModelToFormOnDescriptionIdentifiers(depositFormModel);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.publisherVersionUrl)
        .setValue(depositFormModel.description?.publisherVersionUrl);
      this._mapDepositFormModelToFormOnDescriptionAbstracts(depositFormModel);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.note).setValue(depositFormModel.description?.note);
      DepositFormPresentational.patchFormWithKey(this.formGroupFourthStepDescription, this.formDefinitionFourthStepDescription.jel,
        depositFormModel.description?.classifications?.filter(e => e.code === this._JEL)?.map(e => e.item));
      DepositFormPresentational.patchFormWithKey(this.formGroupFourthStepDescription, this.formDefinitionFourthStepDescription.keywords, depositFormModel.description?.keywords);
      this._mapDepositFormModelToFormOnDescriptionClassifications(depositFormModel);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.discipline).setValue(depositFormModel.description?.discipline);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.mandator).setValue(depositFormModel.description?.mandator);
      DepositFormPresentational.patchFormWithKey(this.formGroupFourthStepDescription, this.formDefinitionFourthStepDescription.publisher, depositFormModel.description?.publisher);
      this._mapDepositFormModelToFormOnDescriptionCollections(depositFormModel);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.edition).setValue(depositFormModel.description?.edition);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.award).setValue(depositFormModel.description?.award);
      this._mapDepositFormModelToFormOnDescriptionDatasets(depositFormModel);
      this._mapDepositFormModelToFormOnDescriptionContainer(depositFormModel);
      this._mapDepositFormModelToFormOnDescriptionLinks(depositFormModel);
      this._mapDepositFormModelToFormOnDescriptionCorrections(depositFormModel);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.aouCollection)
        .setValue(depositFormModel.description?.aouCollection);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.doctorEmail)
        .setValue(depositFormModel.description?.doctorEmail);
      this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.doctorAddress)
        .setValue(depositFormModel.description?.doctorAddress);
      this._mapDepositFormModelToFormOnDescriptionFundings(depositFormModel);
    }
  }

  private _mapDepositFormModelToFormOnDescriptionLanguages(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.languages?.length > 0) {
      const formArrayLanguages = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.languages) as FormArray;
      formArrayLanguages.clear();
      depositFormModel.description.languages.forEach(language => {
        formArrayLanguages.push(DepositFormDescriptionLanguagePresentational.createLanguage(this._fb, this.formDefinitionDepositFormLanguage, language));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionDates(depositFormModel: DepositFormModel): void {
    if (isNotNullNorUndefined(depositFormModel.description?.dates)) {
      const formControlDates = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.dates) as FormGroup;
      DepositFormDescriptionDatePresentational.createOrBindDate(
        this._fb,
        this.formDefinitionDepositFormDate,
        formControlDates,
        depositFormModel.description?.dates);
    }
  }

  private _mapDepositFormModelToFormOnDescriptionIdentifiers(depositFormModel: DepositFormModel): void {
    if (isNotNullNorUndefined(depositFormModel.description?.identifiers)) {
      const formControlIdentifiers = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.identifiers) as FormGroup;
      DepositFormDescriptionPublicationIdentifierPresentational.createOrBindPublicationIdentifier(
        this._fb,
        this.formDefinitionDepositFormPublicationIdentifiers,
        formControlIdentifiers,
        depositFormModel.description?.identifiers);
    }
  }

  private _mapDepositFormModelToFormOnDescriptionAbstracts(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.abstracts?.length > 0) {
      const formArrayDescription = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.abstracts) as FormArray;
      formArrayDescription.clear();
      depositFormModel.description.abstracts.forEach(abstract => {
        formArrayDescription.push(DepositFormDescriptionTextLanguageListPresentational.createTextLanguage(this._fb, this.formDefinitionDepositFormTextLanguage, abstract));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionClassifications(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.classifications?.length > 0) {
      const formArrayClassifications = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.classifications) as FormArray;
      formArrayClassifications.clear();
      depositFormModel.description.classifications.filter(e => e.code !== this._JEL).forEach(classification => {
        formArrayClassifications.push(DepositFormDescriptionClassificationPresentational.createClassification(this._fb, this.formDefinitionDepositFormClassification, classification));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionCollections(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.collections?.length > 0) {
      const formArrayCollections = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.collections) as FormArray;
      formArrayCollections.clear();
      depositFormModel.description.collections.forEach(collection => {
        formArrayCollections.push(DepositFormDescriptionCollectionPresentational.createCollection(this._fb, this.formDefinitionDepositFormCollection, collection));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionDatasets(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.datasets?.length > 0) {
      const formArrayDatasets = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.datasets) as FormArray;
      formArrayDatasets.clear();
      depositFormModel.description.datasets.forEach(dataset => {
        formArrayDatasets.push(DepositFormDescriptionDatasetPresentational.createDataset(this._fb, this.formDefinitionDepositFormDataset, dataset));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionContainer(depositFormModel: DepositFormModel): void {
    if (isNotNullNorUndefined(depositFormModel.description?.container)) {
      const formControlContainer = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.container) as FormGroup;
      DepositFormDescriptionContainerPresentational.bindContainer(
        this._fb,
        this.formDefinitionDepositFormContainer,
        formControlContainer,
        depositFormModel.description.container);
    }
  }

  private _mapDepositFormModelToFormOnDescriptionLinks(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.links?.length > 0) {
      const formArrayLinks = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.links) as FormArray;
      formArrayLinks.clear();
      depositFormModel.description.links.forEach(link => {
        formArrayLinks.push(DepositFormDescriptionLinkPresentational.createLink(this._fb, this.formDefinitionDepositFormLink, this.formDefinitionDepositFormTextLanguage, link));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionCorrections(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.corrections?.length > 0) {
      const formArrayLinks = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.corrections) as FormArray;
      formArrayLinks.clear();
      depositFormModel.description.corrections.forEach(correction => {
        formArrayLinks.push(DepositFormDescriptionCorrectionPresentational.createCorrection(this._fb, this.formDefinitionDepositFormCorrection, correction));
      });
    }
  }

  private _mapDepositFormModelToFormOnDescriptionFundings(depositFormModel: DepositFormModel): void {
    if (depositFormModel.description?.fundings?.length > 0) {
      const formArrayFunding = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.fundings) as FormArray;
      formArrayFunding.clear();
      depositFormModel.description.fundings.forEach(funding => {
        formArrayFunding.push(DepositFormDescriptionFundingPresentational.createFunding(this._fb, this.formDefinitionDepositFormFunding, funding));
      });
    }
  }

  private _addLogical(): void {
    this._subtypeLogical();
    this._issnLogical();

    this.subscribe(this.form.get(DepositFormRuleHelper.pathTitleText).valueChanges.pipe(
      tap(title => {
        this._titleBS.next(title);
      }),
    ));
  }

  private _subtypeLogical(): void {
    this.subscribe(this.formGroupFirstStepType.get(this.formDefinitionFirstStepType.subtype).valueChanges.pipe(
      distinctUntilChanged(),
      tap((subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum) => {
        this._computeDateValues(subType);
        this._subtypeLogicalApplyRules(subType);
      }),
    ));

    this._subtypeLogicalApplyRules(this.formGroupFirstStepType.get(this.formDefinitionFirstStepType.subtype)?.value);
  }

  private _subtypeLogicalApplyRules(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    this._computeValidationLanguageAndTitleLanguage(subType);
    this._computeValidationAbstract(subType);
    this._computeValidationContainerTitle(subType);
    this._computeValidationDoctorInfos(subType);
    this._computeValidationLocalNumber(subType);
    this._computeValidationDates(subType);
  }

  private _computeDateValues(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const formControlDates = this.form.get(DepositFormRuleHelper.pathDates) as FormGroup;
    const oldValueDates: DepositFormDates = formControlDates.value;
    formControlDates.reset();
    if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_OF_ADVANCED_STUDIES
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_DEGREE
      || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRIVATE_DOCTOR_THESIS) {
      this.form.get(DepositFormRuleHelper.pathDatesDefense).setValue((oldValueDates.defense || oldValueDates.first_online) ?? "");
      this.form.get(DepositFormRuleHelper.pathDatesImprimatur).setValue((oldValueDates.imprimatur || oldValueDates.publication) ?? "");
    } else {
      this.form.get(DepositFormRuleHelper.pathDatesFirstOnline).setValue((oldValueDates.first_online || oldValueDates.defense) ?? "");
      this.form.get(DepositFormRuleHelper.pathDatesPublication).setValue((oldValueDates.publication || oldValueDates.imprimatur) ?? "");
    }
  }

  private _computeValidationAbstract(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const formArrayAbstract = this.form.get(DepositFormRuleHelper.pathAbstract) as FormArray;
    let fcAbstract = undefined;
    if (formArrayAbstract?.controls?.length > 0) {
      fcAbstract = formArrayAbstract.controls[0];
    }
    const fcText = fcAbstract?.get(this.formDefinitionDepositFormTextLanguage.text);
    const fcLang = fcAbstract?.get(this.formDefinitionDepositFormTextLanguage.lang);
    if (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS ||
      subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRIVATE_DOCTOR_THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS
    ) {
      fcText?.setValidators([SolidifyValidator, Validators.required]);
      fcLang?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      fcText?.setValidators([SolidifyValidator]);
      fcLang?.setValidators([SolidifyValidator]);
    }
    fcText?.updateValueAndValidity();
    fcLang?.updateValueAndValidity();
  }

  private _computeValidationContainerTitle(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const formControlContainerTitle = this.form.get(DepositFormRuleHelper.pathContainerTitle) as FormControl;
    if ([
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.SCIENTIFIC_ARTICLE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_ARTICLE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRESENTATION_SPEECH,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.POSTER,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA,
    ].indexOf(subType) !== -1) {
      formControlContainerTitle?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      formControlContainerTitle?.setValidators([SolidifyValidator]);
    }
    formControlContainerTitle?.updateValueAndValidity();
  }

  private _computeValidationLocalNumber(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const isThesisBefore2010OrAuthorBeforeUnige: boolean = DepositFormRuleHelper.isThesisBefore2010OrAuthorBeforeUnige(this.form);
    const fcLocalNumber = this.form.get(DepositFormRuleHelper.pathIdentifierLocalNumber) as FormArray;
    if ((subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS) && !isThesisBefore2010OrAuthorBeforeUnige) {
      fcLocalNumber?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      fcLocalNumber?.setValidators([SolidifyValidator]);
    }
  }

  private _computeValidationDoctorInfos(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const formControlDate = this.form.get(this.formDefinitionDateThesis.date) as FormControl;
    const formControlDoctorEmail = this.form.get(DepositFormRuleHelper.pathDoctorEmail) as FormControl;
    const formControlDoctorAddress = this.form.get(DepositFormRuleHelper.pathDoctorAddress) as FormControl;

    const authorBeforeUnige = DepositFormRuleHelper.isAuthorBeforeUnige(this.form);
    const isThesisBefore2010 = DepositFormRuleHelper.isDateBefore2010(formControlDate.value);
    const isThesisBefore2010OrAuthorBeforeUnige: boolean = isTrue(isThesisBefore2010) || isTrue(authorBeforeUnige);

    if (this.isDepositNotBeingUpdated &&
      (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS)
      && !isThesisBefore2010OrAuthorBeforeUnige) {
      formControlDate?.setValidators([SolidifyValidator, Validators.required, Validators.min(this.MIN_YEAR_THESIS), Validators.max(this.MAX_YEAR_THESIS)]);
      formControlDoctorEmail?.setValidators([SolidifyValidator, Validators.required, SolidifyEmailValidator]);
      formControlDoctorAddress?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      formControlDate?.setValidators([SolidifyValidator, Validators.min(this.MIN_YEAR_THESIS), Validators.max(this.MAX_YEAR_THESIS)]);
      formControlDoctorEmail?.setValidators([SolidifyValidator, SolidifyEmailValidator]);
      formControlDoctorAddress?.setValidators([SolidifyValidator]);
    }
    formControlDoctorEmail?.updateValueAndValidity();
    formControlDoctorAddress?.updateValueAndValidity();
    formControlDate?.updateValueAndValidity();
  }

  private _computeValidationLanguageAndTitleLanguage(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const formControlTitleLanguage = this.form.get(DepositFormRuleHelper.pathTitleLanguage) as FormControl;
    const formControlLanguages = this.form.get(DepositFormRuleHelper.pathLanguages) as FormArray;
    const defaultLanguage = isNonEmptyArray(formControlLanguages?.value) && isNonEmptyString(formControlLanguages.value[0].language) ? formControlLanguages.value[0].language : undefined;
    if ([
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_OF_ADVANCED_STUDIES,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_DEGREE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRIVATE_DOCTOR_THESIS,
    ].indexOf(subType) !== -1) {
      if (isNonEmptyString(defaultLanguage) && (isNullOrUndefined(formControlTitleLanguage.value) || isEmptyString(formControlTitleLanguage.value))) {
        formControlTitleLanguage?.setValue(defaultLanguage);
      }
      formControlTitleLanguage?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      formControlTitleLanguage?.setValidators([SolidifyValidator]);
    }
    formControlTitleLanguage?.updateValueAndValidity();
  }

  private _computeValidationDates(subType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const formControlDate = this.form.get(this.formDefinitionDateThesis.date) as FormControl;

    if (this.isDepositNotBeingUpdated &&
      (subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || subType === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS)) {
      formControlDate?.setValidators([SolidifyValidator, Validators.required]);
    } else {
      formControlDate?.setValidators([SolidifyValidator]);
    }
    formControlDate?.updateValueAndValidity();
  }

  private _issnLogical(): void {
    this.subscribe(this.form.get(DepositFormRuleHelper.pathIdentifierIssn).valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      tap(issn => {
        this._issnBS.next(issn);
      }),
    ));
    this._issnBS.next(this.form.get(DepositFormRuleHelper.pathIdentifierIssn)?.value);
  }

  protected convertFormToDepositFormModel(): DepositFormModel {
    const contributors = {} as DepositFormStepContributors;
    contributors.isCollaboration = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.isCollaboration)?.value;
    contributors.isBeforeUnige = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.isBeforeUnige)?.value;
    contributors.academicStructures = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.academicStructures)?.value;
    contributors.groups = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.groups)?.value;
    const listContributors = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.contributorMembersFrontend)?.value;
    const listCollaborationMembers = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.collaborationMembersFrontend)?.value;
    const listMerged = [...listContributors, ...listCollaborationMembers];
    const listContributorsBackend: DepositFormAbstractContributor[] = [];

    listMerged.forEach(c => {
      const contributorBackend: DepositFormAbstractContributor = {};
      if (c.type === DepositTableContributorTypeEnum.collaboration) {
        contributorBackend.collaboration = {
          name: c.lastname,
        };
      } else {
        contributorBackend.contributor = c;
        delete c.guid;
        delete c.type;
        delete c.potentiallyUnige;
      }
      listContributorsBackend.push(contributorBackend);
    });

    contributors.contributors = listContributorsBackend;

    const description: DepositFormStepDescription = this.formGroupFourthStepDescription.value;
    description.dates = [];
    const dates: DepositFormDates = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.dates)?.value;
    if (isNotNullNorUndefinedNorWhiteString(dates.first_online)) {
      description.dates.push({
        date: dates.first_online,
        type: Enums.Deposit.DateTypesEnum.FIRST_ONLINE,
      });
    }
    if (isNotNullNorUndefinedNorWhiteString(dates.publication)) {
      description.dates.push({
        date: dates.publication,
        type: Enums.Deposit.DateTypesEnum.PUBLICATION,
      });
    }
    if (isNotNullNorUndefinedNorWhiteString(dates.defense)) {
      description.dates.push({
        date: dates.defense,
        type: Enums.Deposit.DateTypesEnum.DEFENSE,
      });
    }
    if (isNotNullNorUndefinedNorWhiteString(dates.imprimatur)) {
      description.dates.push({
        date: dates.imprimatur,
        type: Enums.Deposit.DateTypesEnum.IMPRIMATUR,
      });
    }
    description.fundings = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.fundings)?.value;
    description.classifications = this.formGroupFourthStepDescription.get(this.formDefinitionFourthStepDescription.jel)
      ?.value?.map(item => ({code: this._JEL, item: item}));

    return {
      type: this.formGroupFirstStepType.value,
      contributors: contributors,
      description: description,
    };
  }

  private _cleanDoctorsInfoIfNeeded(depositFormModel: DepositFormModel): void {
    let cleanDoctorInfos: boolean = false;

    const address: string = depositFormModel.description.doctorAddress;
    const email: string = depositFormModel.description.doctorAddress;

    // check if a clean may be useful
    // Note: it also ensures that empty strings are not posted for deposits that are not thesis
    if (isNotNullNorUndefinedNorWhiteString(address) || isNotNullNorUndefinedNorWhiteString(email)) {
      depositFormModel.description.dates.forEach(d => {
        if (DepositFormRuleHelper.isDateBefore2010(d.date)) {
          cleanDoctorInfos = true;
        }
      });
      if (depositFormModel.contributors.isBeforeUnige) {
        cleanDoctorInfos = true;
      }

      if (cleanDoctorInfos) {
        if (isNotNullNorUndefinedNorWhiteString(address)) {
          depositFormModel.description.doctorAddress = "";
        }
        if (isNotNullNorUndefinedNorWhiteString(email)) {
          depositFormModel.description.doctorEmail = "";
        }
      }
    }
  }

  protected convertFormToDeposit(): Deposit {
    const deposit = {} as Deposit;
    const depositForm = this.convertFormToDepositFormModel();
    this._cleanDepositFormBeforeSendToBackend(depositForm);
    this.applyDefaultDataOnCreation(depositForm);
    deposit.title = depositForm.type.title.text;
    deposit.subtype = {
      resId: depositForm.type.subtype,
    };
    // TODO Check if we need to clean data to backend side when change of subtype and conditional field are not relevant
    deposit.formData = JSON.stringify(depositForm);
    return deposit;
  }

  private _cleanDepositFormBeforeSendToBackend(depositForm: DepositFormModel): void {
    this._cleanDoctorsInfoIfNeeded(depositForm);
  }

  applyDefaultDataOnCreation(depositForm: DepositFormModel): void {
    if (isNotNullNorUndefined(this.model)) {
      return;
    }
    if (isNullOrUndefined(depositForm.contributors)) {
      depositForm.contributors = {} as any;
    }
    if (isNonEmptyArray(this.defaultData?.academicStructures)) {
      depositForm.contributors.academicStructures = this.defaultData.academicStructures?.map((g => g.resId));
    }
    if (isNonEmptyArray(this.defaultData?.groups)) {
      depositForm.contributors.groups = this.defaultData.groups?.map(g => g.resId);
    }
  }

  subtypeChange(subtypeId: string | undefined): void {
    this._subtypeBS.next(subtypeId);
  }

  dateThesisChange(date: string | undefined): void {
    //set this date into pathDates used in summary
    if (isNotNullNorUndefined(date)) {
      this.form.get(DepositFormRuleHelper.pathDatesDefense)?.setValue(date);
      this.form.get(DepositFormRuleHelper.pathDatesImprimatur)?.setValue(date);
    }
    this._computeValidationDoctorInfos(this.form.get(DepositFormRuleHelper.pathSubType)?.value);
  }

  getMetadataFromServiceChange(getMetadataFromServiceInfo: GetMetadataFromServiceInfo): void {
    this._getMetadataFromServiceBS.next(getMetadataFromServiceInfo);
  }

  private _convertToStepValue(step: number | Enums.Deposit.StepEnum): Enums.Deposit.StepEnum {
    if (isNumber(step)) {
      return Enums.Deposit.publicationStepEnumValue(step);
    }
    return step;
  }

  move(step: number | Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum, listPermissions: MovePermissionEnum[] = []): void {
    const stepValue = this._convertToStepValue(step);

    if (isNullOrUndefined(stepValue) && isNullOrUndefined(this.model)) {
      this.isLoadingOnCreate = true;
      this.create(targetRoutingAfterSave);
      return;
    }

    if (this._isBlockByValidationRule(stepValue, targetRoutingAfterSave, listPermissions)) {
      return;
    }

    if (this._isBlockByValidationRule(stepValue, targetRoutingAfterSave, listPermissions)) {
      return;
    }

    if (stepValue === Enums.Deposit.StepEnum.DESCRIPTION) {
      this.convertedFormToDepositFormModelForSummary = this.convertFormToDepositFormModel();
    }

    this.update(stepValue, targetRoutingAfterSave);
  }

  private _isBlockByValidationRule(stepValue: Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum, listPermissions: MovePermissionEnum[]): boolean {
    // Add validation rules only on NEXT step
    if (isNullOrUndefined(targetRoutingAfterSave) || targetRoutingAfterSave <= 0) {
      return false;
    }

    this._addPermissionsForValidation(listPermissions);

    if (this._isStepFilesBlockByValidationRule(stepValue, targetRoutingAfterSave, listPermissions)) {
      return true;
    }

    if (this._isStepContributorsBlockByValidationRule(stepValue, targetRoutingAfterSave, listPermissions)) {
      return true;
    }

    if (this._isStepDescriptionBlockByValidationRule(stepValue, targetRoutingAfterSave, listPermissions)) {
      return true;
    }

    return false;
  }

  private _addPermissionsForValidation(listPermissions: MovePermissionEnum[]): void {
    if (!this.isValidationMode) {
      return;
    }
    if (!listPermissions.includes(MovePermissionEnum.ALLOW_NO_STRUCTURE)) {
      listPermissions.push(MovePermissionEnum.ALLOW_NO_STRUCTURE);
    }
    if (!listPermissions.includes(MovePermissionEnum.ALLOW_NO_FILE) && isFalse(environment.depositRequestToValidatorAtLeastOnPdfFile)) {
      listPermissions.push(MovePermissionEnum.ALLOW_NO_FILE);
    }
  }

  private _isStepFilesBlockByValidationRule(stepValue: Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum, listPermissions: MovePermissionEnum[]): boolean {
    if (stepValue !== Enums.Deposit.StepEnum.FILES) {
      return false;
    }

    if (this.isDepositNotBeingUpdated) {
      if (!listPermissions.includes(MovePermissionEnum.ALLOW_NO_FILE)
        && isFalse(this.isFilePresent)) {
        return this._displayNoFileDialog(listPermissions, !this.isValidationMode, targetRoutingAfterSave);
      }

      const isThesisBefore2010OrAuthorBeforeUnige: boolean = DepositFormRuleHelper.isThesisBefore2010OrAuthorBeforeUnige(this.form);
      if (!listPermissions.includes(MovePermissionEnum.ALLOW_THESIS_MISSING_FILES)
        && DepositHelper.isThesisAndScienceFacultyAndMissingFiles(this._store, this.model, this.model.metadataStructures.map(s => s.resId), isThesisBefore2010OrAuthorBeforeUnige)) {
        return this._displayThesisFilesNeededDialog(listPermissions, !this.isValidationMode, targetRoutingAfterSave);
      }
    }

    return false;
  }

  private _isStepContributorsBlockByValidationRule(stepValue: Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum, listPermissions: MovePermissionEnum[]): boolean {
    if (stepValue !== Enums.Deposit.StepEnum.CONTRIBUTORS) {
      return false;
    }

    if (!listPermissions.includes(MovePermissionEnum.ALLOW_NO_UNIGE_CONTRIBUTOR)) {
      const check = this.getCheckContributorCondition();
      if (check !== ContributorUnigeCheck.OK) {
        return this._displayConfirmationNoContributorsDialog(listPermissions, check, targetRoutingAfterSave);
      }
    }

    if (!listPermissions.includes(MovePermissionEnum.ALLOW_DUPLICATE_CONTRIBUTOR)) {
      const listDuplicateContributor = this._getDuplicateContributor();
      if (isNonEmptyArray(listDuplicateContributor)) {
        return this._displayWarningDuplicateContributorDialog(listDuplicateContributor, listPermissions, targetRoutingAfterSave);
      }
    }

    if (!listPermissions.includes(MovePermissionEnum.ALLOW_PERSON_WITHOUT_FIRST_NAME)) {
      if (this._isThereContributorWithoutFirstName()) {
        return this._displayContributorWithoutFirstNamedDialog(listPermissions, targetRoutingAfterSave);
      }
    }

    if (!listPermissions.includes(MovePermissionEnum.ALLOW_NO_STRUCTURE)) {
      const structures: string[] = this.form.get(DepositFormRuleHelper.pathStructure).value;
      const isBeforeUnige = isTrue(this.form.get(DepositFormRuleHelper.pathContributorsIsBeforeUnige)?.value);
      if (isNullOrUndefinedOrEmptyArray(structures) && isFalse(isBeforeUnige)) {
        return this._displayNoStructureSelectedDialog(listPermissions, targetRoutingAfterSave);
      }
    }

    return false;
  }

  private _isStepDescriptionBlockByValidationRule(stepValue: Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum, listPermissions: MovePermissionEnum[]): boolean {
    if (stepValue !== Enums.Deposit.StepEnum.DESCRIPTION) {
      return false;
    }

    if (!listPermissions.includes(MovePermissionEnum.ALLOW_DUPLICATE_FUNDING)) {
      if (this.isDuplicateFunding()) {
        return this._displayWarningDuplicateFundingDialog(listPermissions, targetRoutingAfterSave);
      }
    }

    return false;
  }

  private _displayThesisFilesNeededDialog(listPermissions: MovePermissionEnum[], allowToContinue: boolean, targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.thesisNeededFiles.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.thesisNeededFiles.depositor.message"),
      cancelButtonToTranslate: allowToContinue ? LabelTranslateEnum.submitAFile : undefined,
      confirmButtonToTranslate: allowToContinue ? LabelTranslateEnum.continue : LabelTranslateEnum.submitAFile,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "450px",
    }, isConfirmed => {
      if (allowToContinue) {
        if (isNullOrUndefined(targetRoutingAfterSave)) {
          return;
        }
        listPermissions.push(MovePermissionEnum.ALLOW_THESIS_MISSING_FILES);
        this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.FILES), targetRoutingAfterSave, listPermissions);
      } else {
        this.navigateToStep(Enums.Deposit.StepEnum.FILES);
      }
    }));
    return true;
  }

  private _displayNoFileDialog(listPermissions: MovePermissionEnum[], allowToContinue: boolean, targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.noPdfFile.title"),
      messageToTranslate: allowToContinue ? MARK_AS_TRANSLATABLE("deposit.file.warning.noPdfFile.depositor.message") : MARK_AS_TRANSLATABLE("deposit.file.warning.noPdfFile.validator.message"),
      cancelButtonToTranslate: allowToContinue ? LabelTranslateEnum.submitAFile : undefined,
      confirmButtonToTranslate: allowToContinue ? LabelTranslateEnum.continue : LabelTranslateEnum.submitAFile,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "90vw",
    }, isConfirmed => {
      if (allowToContinue) {
        if (isNotNullNorUndefined(targetRoutingAfterSave)) {
          listPermissions.push(MovePermissionEnum.ALLOW_NO_FILE);
          this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.FILES), targetRoutingAfterSave, listPermissions);
        }
      } else {
        this.navigateToStep(Enums.Deposit.StepEnum.FILES);
      }
    }));
    return true;
  }

  private _displayNoStructureSelectedDialog(listPermissions: MovePermissionEnum[], targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.noAffiliationStructure.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.noAffiliationStructure.message"),
      cancelButtonToTranslate: LabelTranslateEnum.addStructure,
      confirmButtonToTranslate: LabelTranslateEnum.continue,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "90vw",
    }, isConfirmed => {
      listPermissions.push(MovePermissionEnum.ALLOW_NO_STRUCTURE);
      this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.CONTRIBUTORS), targetRoutingAfterSave, listPermissions);
    }, () => {

    }));
    return true;
  }

  private _displayConfirmationNoContributorsDialog(listPermissions: MovePermissionEnum[], check: ContributorUnigeCheck, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    let title = SOLIDIFY_CONSTANTS.STRING_EMPTY;
    let message = SOLIDIFY_CONSTANTS.STRING_EMPTY;
    let confirmButton = LabelTranslateEnum.yesImSure;

    switch (check) {
      case ContributorUnigeCheck.NOT_AFFILIATE_AND_NOBODY_UNIGE:
        title = MARK_AS_TRANSLATABLE("deposit.contributor.warning.notAffiliateAndNobodyUnige.title");
        message = MARK_AS_TRANSLATABLE("deposit.contributor.warning.notAffiliateAndNobodyUnige.message");
        break;
      case ContributorUnigeCheck.AFFILIATE_BUT_NOBODY_UNIGE:
        title = MARK_AS_TRANSLATABLE("deposit.contributor.warning.affiliateButNobodyUnige.title");
        message = MARK_AS_TRANSLATABLE("deposit.contributor.warning.affiliateButNobodyUnige.message");
        break;
      case ContributorUnigeCheck.NO_CONTRIBUTOR:
        title = MARK_AS_TRANSLATABLE("deposit.contributor.warning.noContributor.title");
        message = MARK_AS_TRANSLATABLE("deposit.contributor.warning.noContributor.message");
        confirmButton = undefined;
        break;
      default:
        return;
    }
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: title,
      messageToTranslate: message,
      confirmButtonToTranslate: confirmButton,
      cancelButtonToTranslate: LabelTranslateEnum.correct,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "90vw",
    }, isConfirmed => {
      const form = this.formGroupThirdStepContributors;
      const structures: string[] = form.get(this.formDefinitionThirdStepContributors.academicStructures).value;
      listPermissions.push(MovePermissionEnum.ALLOW_NO_UNIGE_CONTRIBUTOR);

      if (!this.isValidationMode && isNullOrUndefinedOrEmptyArray(structures)) {
        this._displayNoStructureSelectedDialog(listPermissions, targetRoutingAfterSave);
        return;
      }
      this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.CONTRIBUTORS), targetRoutingAfterSave, listPermissions);
    }));
    return true;
  }

  private _displayWarningDuplicateFundingDialog(listPermissions: MovePermissionEnum[], targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.description.warning.duplicateFunding.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.description.warning.duplicateFunding.message"),
      cancelButtonToTranslate: LabelTranslateEnum.correct,
      confirmButtonToTranslate: LabelTranslateEnum.continue,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "90vw",
    }, isConfirmed => {
      listPermissions.push(MovePermissionEnum.ALLOW_DUPLICATE_FUNDING);
      this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.DESCRIPTION), targetRoutingAfterSave, listPermissions);
    }));
    return true;
  }

  private _displayWarningDuplicateContributorDialog(listDuplicateContributor: DepositFormContributor[], listPermissions: MovePermissionEnum[], targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.contributor.warning.duplicate.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.contributor.warning.duplicate.message"),
      paramMessage: {
        contributors: "\n- " + listDuplicateContributor.map(c => (isNotNullNorUndefinedNorWhiteString(c.firstname) ? c.firstname + " " : "") + c.lastname)
          .join("\n- "),
      },
      displayLineReturnOnMessage: true,
      cancelButtonToTranslate: LabelTranslateEnum.correct,
      confirmButtonToTranslate: LabelTranslateEnum.continue,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "90vw",
    }, isConfirmed => {
      listPermissions.push(MovePermissionEnum.ALLOW_DUPLICATE_CONTRIBUTOR);
      this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.CONTRIBUTORS), targetRoutingAfterSave, listPermissions);
    }));
    return true;
  }

  private _displayContributorWithoutFirstNamedDialog(listPermissions: MovePermissionEnum[], targetRoutingAfterSave?: Enums.Deposit.TargetRoutingAfterSaveEnum): true {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.contributor.warning.noFirstName.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.contributor.warning.noFirstName.message"),
      cancelButtonToTranslate: LabelTranslateEnum.correct,
      confirmButtonToTranslate: LabelTranslateEnum.continue,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "90vw",
    }, isConfirmed => {
      listPermissions.push(MovePermissionEnum.ALLOW_PERSON_WITHOUT_FIRST_NAME);
      this.move(Enums.Deposit.publicationStepEnumNumber(Enums.Deposit.StepEnum.CONTRIBUTORS), targetRoutingAfterSave, listPermissions);
    }));
    return true;
  }

  private _isThereContributorWithoutFirstName(): boolean {
    const form = this.formGroupThirdStepContributors;
    const contributors: DepositFormContributor[] = form.get(this.formDefinitionThirdStepContributors.contributorMembersFrontend).value;
    const collaborationsMembers: DepositFormContributor[] = form.get(this.formDefinitionThirdStepContributors.collaborationMembersFrontend).value;
    for (const contributor of contributors) {
      if ((contributor.type !== DepositTableContributorTypeEnum.collaboration) && isNullOrUndefinedOrWhiteString(contributor.firstname)) {
        return true;
      }
    }
    for (const contributor of collaborationsMembers) {
      if (isNullOrUndefinedOrWhiteString(contributor.firstname)) {
        return true;
      }
    }
    return false;
  }

  isDuplicateFunding(): boolean {
    const listFunding = this.form.get(DepositFormRuleHelper.pathFunder)
      ?.value
      ?.filter(f => isNotNullNorUndefinedNorWhiteString(f[this.formDefinitionDepositFormFunding.code])) ?? [];
    return listFunding.length > ArrayUtil.distinct(listFunding, this.formDefinitionDepositFormFunding.code).length;
  }

  getCheckContributorCondition(): ContributorUnigeCheck {
    const form = this.formGroupThirdStepContributors;
    const contributors: DepositFormContributor[] = form.get(this.formDefinitionThirdStepContributors.contributorMembersFrontend).value;
    const collaborationsMembers: DepositFormContributor[] = form.get(this.formDefinitionThirdStepContributors.collaborationMembersFrontend).value;
    if (contributors.length === 0 && collaborationsMembers.length === 0) {
      return ContributorUnigeCheck.NO_CONTRIBUTOR;
    }
    const isDepositorAffiliateToUnige = true; // TODO SET THIS VALUE WHEN AVAILABLE (HUG / UNIGE)

    const isUnigeContributor = contributors.findIndex(c => isNonEmptyString(c.cnIndividu)) !== -1;
    const isUnigeCollaborationMember = collaborationsMembers.findIndex(c => isNonEmptyString(c.cnIndividu)) !== -1;

    if (!isUnigeContributor && !isUnigeCollaborationMember) {
      return isDepositorAffiliateToUnige ? ContributorUnigeCheck.AFFILIATE_BUT_NOBODY_UNIGE : ContributorUnigeCheck.NOT_AFFILIATE_AND_NOBODY_UNIGE;
    }

    return ContributorUnigeCheck.OK;
  }

  private _getDuplicateContributor(): DepositFormContributor[] {
    const contributors: DepositFormContributor[] = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.contributorMembersFrontend).value ?? [];
    const collaborationsMembers: DepositFormContributor[] = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.collaborationMembersFrontend).value ?? [];
    const allContributors = [...contributors, ...collaborationsMembers];

    const fullNameKey = "fullName";
    const listExternalContributor = allContributors.filter(c => isNullOrUndefinedOrWhiteString(c.cnIndividu))
      .map(c => {
        c[fullNameKey] = `${c.firstname}|${c.lastname}`;
        return c;
      });

    const listDuplicated = [];
    ArrayUtil.distinct(listExternalContributor, fullNameKey).forEach(externalContributor => {
      const duplicateContributor = allContributors.filter(c => c.guid !== externalContributor.guid)
        .find(c => c.firstname?.toLowerCase() === externalContributor.firstname?.toLowerCase() && c.lastname?.toLowerCase() === externalContributor.lastname?.toLowerCase());
      if (isNotNullNorUndefined(duplicateContributor)) {
        listDuplicated.push(duplicateContributor);
      }
    });
    return listDuplicated;
  }

  create(targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum): void {
    const modelFormControlEvent = this._generateModelFormControlEvent(Enums.Deposit.StepEnum.TYPE, targetRoutingAfterSave);
    if (isNonEmptyString(this.importedSource)) {
      modelFormControlEvent.model.importSource = this.importedSource;
    }
    this._createBS.next(modelFormControlEvent);
  }

  update(step: Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum): void {
    this._updateBS.next(this._generateModelFormControlEvent(step, targetRoutingAfterSave));
  }

  saveAndSubmit(): void {
    this.move(this.stepper.currentStepIndex, Enums.Deposit.TargetRoutingAfterSaveEnum.SAVE_AND_SUBMIT);
  }

  submit(): void {
    if (isFalse(this.isFilePresent) && isTrue(environment.depositRequestToDepositorAtLeastOnPdfFileForSendToValidation) && this.isDepositNotBeingUpdated) {
      this._displayNoFileDialog([], false);
      return;
    }

    const isThesisBefore2010OrAuthorBeforeUnige: boolean = DepositFormRuleHelper.isThesisBefore2010OrAuthorBeforeUnige(this.form);

    if (DepositHelper.isThesisAndScienceFacultyAndMissingFiles(this._store, this.model, this.model.metadataStructures.map(s => s.resId), isThesisBefore2010OrAuthorBeforeUnige) && this.isDepositNotBeingUpdated) {
      this._displayThesisFilesNeededDialog([], false);
      return;
    }

    const depositModelFormControlEvent = this._generateModelFormControlEvent(Enums.Deposit.StepEnum.SUMMARY, Enums.Deposit.TargetRoutingAfterSaveEnum.SAVE_FOR_LATER);
    const listStructure: string[] | undefined = this.formGroupThirdStepContributors.get(this.formDefinitionThirdStepContributors.academicStructures).value;
    if (isNonEmptyArray(listStructure)) {
      this.subscribe(DepositHelper.confirmSubmitDialogObs(this._dialog, () => {
        this._submitBS.next(depositModelFormControlEvent);
      }));
    } else {
      this.subscribe(DepositHelper.askValidationStructureForSubmitDialogObs(this._dialog, structureId => {
        if (DepositHelper.isThesisAndScienceFacultyAndMissingFiles(this._store, this.model, [structureId], isThesisBefore2010OrAuthorBeforeUnige) && this.isDepositNotBeingUpdated) {
          this._displayThesisFilesNeededDialog([], false);
          return;
        }
        depositModelFormControlEvent.model.validationStructureId = structureId;
        this._submitBS.next(depositModelFormControlEvent);
      }));
    }
  }

  exit(): void {
    this._exitBS.next();
  }

  private _generateModelFormControlEvent(formStep: Enums.Deposit.StepEnum, targetRoutingAfterSave: Enums.Deposit.TargetRoutingAfterSaveEnum): ModelFormControlEventDeposit {
    const listPanelExpandablePresentational = [];
    if (formStep === Enums.Deposit.StepEnum.FILES) {
      listPanelExpandablePresentational.push(...this.depositFilesPresentational.listPanelExpandablePresentational.toArray());
    }
    if (formStep === Enums.Deposit.StepEnum.CONTRIBUTORS) {
      listPanelExpandablePresentational.push(...this.depositContributorsPresentational.listPanelExpandablePresentational.toArray());
    }
    if (formStep === Enums.Deposit.StepEnum.DESCRIPTION) {
      listPanelExpandablePresentational.push(...this.depositDescriptionPresentational.listPanelExpandablePresentational.toArray());
    }
    const model = this.convertFormToDeposit();
    model.formStep = formStep;
    model.resId = this.model?.resId;
    model.targetRoutingAfterSave = targetRoutingAfterSave;
    return {
      model: model,
      formControl: this.form,
      changeDetectorRef: this._changeDetectorRef,
      listFieldNameToDisplayErrorInToast: [],
      listPanelExpandablePresentational: listPanelExpandablePresentational,
      subFormPartDetectChanges: () => {
        this.depositTypePresentational?.externalDetectChanges();
        this.depositFilesPresentational?.externalDetectChanges();
        this.depositContributorsPresentational?.externalDetectChanges();
        this.depositDescriptionPresentational?.externalDetectChanges();
      },
    } as ModelFormControlEventDeposit;
  }

  navigateToStep(step: Enums.Deposit.StepEnum): void {
    this._navigateToStepBS.next(step);
  }

  private _addStepErrorWatcher(): void {
    this.subscribe(this._observeValueChangeForUpdateErrorNumber(this.formGroupFirstStepType).pipe(
      distinctUntilChanged(),
      tap(numberError => {
        this.errorFirstStepType = numberError;
        this.stepper.triggerDetectChanges();
        this._changeDetectorRef.detectChanges();
      }),
    ));

    this.subscribe(this._observeValueChangeForUpdateErrorNumber(this.formGroupSecondStepFiles).pipe(
      distinctUntilChanged(),
      filter(numberError => isNotNullNorUndefined(this.model)), // allow to avoid to display error on creation step due to import
      tap(numberError => {
        this.errorSecondStepFiles = numberError;
        this.stepper.triggerDetectChanges();
        this._changeDetectorRef.detectChanges();
      }),
    ));

    this.subscribe(this._observeValueChangeForUpdateErrorNumber(this.formGroupThirdStepContributors).pipe(
      distinctUntilChanged(),
      filter(numberError => isNotNullNorUndefined(this.model)), // allow to avoid to display error on creation step due to import
      tap(numberError => {
        this.errorThirdStepContributors = numberError;
        this.stepper.triggerDetectChanges();
        this._changeDetectorRef.detectChanges();
      }),
    ));

    this.subscribe(this._observeValueChangeForUpdateErrorNumber(this.formGroupFourthStepDescription).pipe(
      distinctUntilChanged(),
      filter(numberError => isNotNullNorUndefined(this.model)), // allow to avoid to display error on creation step due to import
      tap(numberError => {
        this.errorFourthStepDescription = numberError;
        this.stepper.triggerDetectChanges();
        this._changeDetectorRef.detectChanges();
      }),
    ));
  }

  private _observeValueChangeForUpdateErrorNumber(formGroup: FormGroup): Observable<number> {
    return formGroup.valueChanges.pipe(
      debounceTime(1000),
      map(value => this._getNumberFormError(formGroup)),
    );
  }

  private _getNumberFormError(formGroupFourthStepDescription: FormGroup, stopOnFirstArray: boolean = true): number {
    const errors = ValidationUtil.collectErrorsInArray(formGroupFourthStepDescription, stopOnFirstArray);
    if (isNullOrUndefined(errors)) {
      return 0;
    }
    return Object.keys(errors).length;
  }

  disableNavigation(stepper: SharedStepperPresentational): boolean {
    const isCreationFirstStep = isNullOrUndefined(stepper.currentStep?.stepFormAbstractControl) && this.isDepositNotBeingUpdated;
    let form;
    if (isCreationFirstStep) {
      form = this.formGroupFirstStepType as AbstractControl;
    } else {
      form = stepper.currentStep?.stepFormAbstractControl;
    }

    let isInvalid = form?.invalid;

    if (!isInvalid) {
      if (isCreationFirstStep) {
        const formControlLanguages = this.form.get(DepositFormRuleHelper.pathLanguages);
        const formControlType = this.form.get(DepositFormRuleHelper.pathSubType);
        const formControlDateThesis = this.form.get(this.formDefinitionDateThesis.date);
        const formControlDoctorAddress = this.form.get(DepositFormRuleHelper.pathDoctorAddress);
        const formControlDoctorEmail = this.form.get(DepositFormRuleHelper.pathDoctorEmail);

        if (isNotNullNorUndefined(formControlLanguages)) {
          isInvalid = formControlLanguages.invalid;
        }
        if (stepper.currentStep?.id === undefined
          && (formControlType.value === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS || formControlType.value === Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS)) {
          isInvalid = formControlDoctorAddress.invalid || formControlDoctorEmail.invalid || formControlDateThesis.invalid;
        }
      } else if (stepper.currentStep?.id === Enums.Deposit.StepEnum.FILES) {
        const listDocumentFile = MemoizedUtil.selectSnapshot(this._store, DepositDocumentFileState, state => state.list);
        const listDocumentFilesNotReady = listDocumentFile?.filter(u => u.status !== Enums.DocumentFile.StatusEnum.READY && u.status !== Enums.DocumentFile.StatusEnum.CONFIRMED);
        if (listDocumentFilesNotReady?.length > 0) {
          isInvalid = true;
        }
      }
    }

    return isInvalid;
  }

  navigate($event: Navigate): void {
    this._navigateBS.next($event);
  }

  refreshValidationError(): void {
    this._refreshValidationErrorBS.next();
  }
}

enum ContributorUnigeCheck {
  OK,
  NOT_AFFILIATE_AND_NOBODY_UNIGE,
  AFFILIATE_BUT_NOBODY_UNIGE,
  NO_CONTRIBUTOR,
}

export type MovePermissionEnum =
  "ALLOW_PERSON_WITHOUT_FIRST_NAME"
  | "ALLOW_THESIS_MISSING_FILES"
  | "ALLOW_NO_FILE"
  | "ALLOW_NO_UNIGE_CONTRIBUTOR"
  | "ALLOW_DUPLICATE_CONTRIBUTOR"
  | "ALLOW_NO_STRUCTURE";
export const MovePermissionEnum = {
  ALLOW_PERSON_WITHOUT_FIRST_NAME: "ALLOW_PERSON_WITHOUT_FIRST_NAME" as MovePermissionEnum,
  ALLOW_THESIS_MISSING_FILES: "ALLOW_THESIS_MISSING_FILES" as MovePermissionEnum,
  ALLOW_NO_FILE: "ALLOW_NO_FILE" as MovePermissionEnum,
  ALLOW_NO_UNIGE_CONTRIBUTOR: "ALLOW_NO_UNIGE_CONTRIBUTOR" as MovePermissionEnum,
  ALLOW_DUPLICATE_CONTRIBUTOR: "ALLOW_DUPLICATE_CONTRIBUTOR" as MovePermissionEnum,
  ALLOW_NO_STRUCTURE: "ALLOW_NO_STRUCTURE" as MovePermissionEnum,
  ALLOW_DUPLICATE_FUNDING: "ALLOW_DUPLICATE_FUNDING" as MovePermissionEnum,
};
