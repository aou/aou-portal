/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-type-subtype.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {
  FormComponentFormDefinitionFirstStepType,
  FormComponentFormDefinitionMain,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {
  DepositSubSubtype,
  DepositSubtype,
  DepositType,
} from "@models";
import {
  AouObjectUtil,
  KeyValuesObject,
} from "@shared/utils/aou-object.util";
import {BehaviorSubject} from "rxjs";
import {Observable} from "rxjs/index";
import {
  distinctUntilChanged,
  filter,
} from "rxjs/operators";
import {
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-type-subtype",
  templateUrl: "./deposit-form-type-subtype.presentational.html",
  styleUrls: ["deposit-form-type-subtype.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormTypeSubtypePresentational extends DepositAbstractFormPresentational implements OnInit {
  formDefinitionMain: FormComponentFormDefinitionMain = new FormComponentFormDefinitionMain();
  formDefinition: FormComponentFormDefinitionFirstStepType = new FormComponentFormDefinitionFirstStepType();

  depositTypeEnumValues: KeyValue[] = Enums.DepositType.DepositTypeEnumTranslate;
  path: string = this.formDefinitionMain.type;

  protected _mainFormGroup: FormGroup;

  @Input()
  set mainFormGroup(value: FormGroup) {
    if (isNullOrUndefined(value)) {
      return;
    }
    this.formGroup = value.controls[this.formDefinitionMain.type] as FormGroup;
    this._mainFormGroup = value;
  }

  get mainFormGroup(): FormGroup {
    return this._mainFormGroup;
  }

  isEditable: boolean = true;

  @Input()
  listDepositSubtype: DepositSubtype[];

  private _groupBasedOnDepositSubtypeList: DepositSubtype[];
  private _groupSubTypeByType: KeyValuesObject<DepositSubtype, DepositType>[];

  get groupSubTypeByType(): KeyValuesObject<DepositSubtype, DepositType>[] {
    let listResult: KeyValuesObject<DepositSubtype, DepositType>[] = [];
    if (isNullOrUndefined(this.listDepositSubtype)) {
      return listResult;
    }

    if (isNotNullNorUndefined(this._groupSubTypeByType) && this._groupBasedOnDepositSubtypeList === this.listDepositSubtype) {
      return this._groupSubTypeByType;
    }
    listResult = AouObjectUtil.groupBy(this.listDepositSubtype, key => this.listDepositSubtype.find(s => s.publicationType?.resId === key)?.publicationType, "publicationType", "resId") as any;
    listResult = listResult.sort((a, b) => {
      const orderA = a.key.sortValue || 0;
      const orderB = b.key.sortValue || 0;
      if (orderA === orderB) {
        return 0;
      } else if (orderA < orderB) {
        return -1;
      }
      return 1;
    });
    this._groupSubTypeByType = listResult;
    this._groupBasedOnDepositSubtypeList = this.listDepositSubtype;
    return listResult;
  }

  private _listDepositSubSubtype: DepositSubSubtype[];

  @Input()
  set listDepositSubSubtype(value: DepositSubSubtype[]) {
    this._listDepositSubSubtype = value;
    if (this._listDepositSubSubtype?.length > 0) {
      const subsubtypeFormControl = this.formGroup.get(this.formDefinition.subsubtype);
      if (isNotNullNorUndefined(subsubtypeFormControl)) {
        const subsubtype = subsubtypeFormControl.value;
        if (this._listDepositSubSubtype.findIndex(s => s.resId === subsubtype) === -1) {
          subsubtypeFormControl.setValue(this._listDepositSubSubtype[0].resId);
        }
      }
    }
  }

  get listDepositSubSubtype(): DepositSubSubtype[] {
    return this._listDepositSubSubtype;
  }

  private readonly _subtypeBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("subtypeChange")
  readonly subtypeObs: Observable<string | undefined> = ObservableUtil.asObservable(this._subtypeBS);

  constructor(protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
  ) {
    super(_changeDetector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.formGroup.get(this.formDefinition.subtype).valueChanges.pipe(
        distinctUntilChanged(),
        filter(value => isNotNullNorUndefined(value) && isNonEmptyString(value)),
      ),
      subTypeId => {
        this._subtypeBS.next(subTypeId);

        this.formGroup.get(this.formDefinition.subsubtype).setValue(undefined);
        const subType = this.listDepositSubtype.find(s => s.resId === subTypeId);
        this.formGroup.get(this.formDefinition.type).setValue(subType?.publicationType?.resId);
      },
    );
  }
}
