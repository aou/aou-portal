/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-classification.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {
  DepositFormClassification,
  FormComponentFormDefinitionDepositFormClassification,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BaseFormDefinition,
  isNotNullNorUndefined,
  NotificationService,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-classification",
  templateUrl: "./deposit-form-description-classification.presentational.html",
  styleUrls: ["./deposit-form-description-classification.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionClassificationPresentational,
    },
  ],
})
export class DepositFormDescriptionClassificationPresentational extends DepositAbstractFormListPresentational implements ControlValueAccessor {
  formDefinition: FormComponentFormDefinitionDepositFormClassification = new FormComponentFormDefinitionDepositFormClassification();

  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition];

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
  }

  static createClassification(_fb: FormBuilder,
                              formDefinition: FormComponentFormDefinitionDepositFormClassification,
                              classification: DepositFormClassification | undefined = undefined): FormGroup {
    const form = _fb.group({
      [formDefinition.code]: [undefined, [SolidifyValidator]],
      [formDefinition.item]: [undefined, [SolidifyValidator]],
    });

    if (isNotNullNorUndefined(classification)) {
      form.setValue(classification as any);
    }

    return form;
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositFormClassification): FormGroup {
    return DepositFormDescriptionClassificationPresentational.createClassification(fb, formDefinition);
  }
}
