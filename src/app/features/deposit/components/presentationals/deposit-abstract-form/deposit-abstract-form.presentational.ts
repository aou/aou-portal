/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-abstract-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  HostBinding,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormArray,
  FormControl,
  FormGroup,
} from "@angular/forms";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {DepositFormPublisher} from "@app/features/deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  BaseFormDefinition,
  FormValidationHelper,
  isNotNullNorUndefined,
  isNullOrUndefined,
  ObservableUtil,
  PanelExpandablePresentational,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Directive()
export abstract class DepositAbstractFormPresentational extends SharedAbstractPresentational implements ControlValueAccessor, OnInit {
  @ViewChildren(PanelExpandablePresentational) listPanelExpandablePresentational: QueryList<PanelExpandablePresentational>;

  abstract formDefinition: BaseFormDefinition;

  @Input()
  formGroup: FormGroup;

  protected _mainFormGroup: FormGroup;

  @Input()
  set mainFormGroup(value: FormGroup) {
    this._mainFormGroup = value;
  }

  get mainFormGroup(): FormGroup {
    return this._mainFormGroup;
  }

  _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private readonly _valueBS: BehaviorSubject<any[] | undefined> = new BehaviorSubject<any[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<any[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  @Input()
  @HostBinding("class.disabled")
  disabled: boolean = false;

  get depositFormRuleHelper(): typeof DepositFormRuleHelper {
    return DepositFormRuleHelper;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get depositSubTypeEnum(): typeof Enums.Deposit.DepositSubTypeEnum {
    return Enums.Deposit.DepositSubTypeEnum;
  }

  constructor(protected readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._detectChangesWhenStatusChanges(this.formGroup);
  }

  getFormControlInCurrentFormGroup(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.formGroup, key);
  }

  getFormGroupInCurrentFormGroup(key: string): FormGroup {
    return FormValidationHelper.getFormGroup(this.formGroup, key);
  }

  getFormArrayInCurrentFormGroup(key: string): FormArray {
    return FormValidationHelper.getFormArray(this.formGroup, key);
  }

  getAbstractControl(key: string): AbstractControl {
    return FormValidationHelper.getAbstractControl(this.mainFormGroup, key);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.mainFormGroup, key);
  }

  getFormGroup(key: string): FormGroup {
    return FormValidationHelper.getFormGroup(this.mainFormGroup, key);
  }

  getFormArray(key: string): FormArray {
    return FormValidationHelper.getFormArray(this.mainFormGroup, key);
  }

  isRequired(formControl: AbstractControl, key: string): boolean {
    const errors = formControl.get(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  externalDetectChanges(): void {
    this._changeDetector.detectChanges();
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: DepositFormPublisher[]): void {
  }

  protected _detectChangesWhenStatusChanges(abstractControl: AbstractControl): void {
    if (isNotNullNorUndefined(abstractControl)) {
      this.subscribe(abstractControl.statusChanges.pipe(
        distinctUntilChanged(),
        filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
        tap(status => {
          setTimeout(() => {
            this._changeDetector.detectChanges();
          });
        }),
      ));
    }
  }
}
