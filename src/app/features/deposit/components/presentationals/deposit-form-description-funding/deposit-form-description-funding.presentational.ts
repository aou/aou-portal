/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-funding.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {MatDialog} from "@angular/material/dialog";
import {DepositCreateFundingDialog} from "@app/features/deposit/components/dialogs/deposit-create-funding/deposit-create-funding.dialog";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {
  DepositFormFunding,
  FormComponentFormDefinitionDepositFormFunding,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {environment} from "@environments/environment";
import {AouEnvironment} from "@environments/environment.defaults.model";
import {OpenAireProjectDTO} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedExternalDataOpenAireAction} from "@shared/stores/external-data/external-data-open-aire/shared-external-data-open-aire.action";
import {SharedExternalDataOpenAireState} from "@shared/stores/external-data/external-data-open-aire/shared-external-data-open-aire.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  BaseFormDefinition,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  QueryParameters,
  QueryParametersUtil,
  SolidifyValidator,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-funding",
  templateUrl: "./deposit-form-description-funding.presentational.html",
  styleUrls: ["./deposit-form-description-funding.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionFundingPresentational,
    },
  ],
})
export class DepositFormDescriptionFundingPresentational extends DepositAbstractFormListPresentational implements ControlValueAccessor, OnInit {
  KEY_SEARCH_BY_ACRONYM: string = "acronym";
  KEY_SEARCH_BY_CODE: string = "grantID";
  KEY_SEARCH_BY_NAME: string = "name";

  INPUT_MAX_LENGTH_CODE: number = 20;

  formDefinition: FormComponentFormDefinitionDepositFormFunding = new FormComponentFormDefinitionDepositFormFunding();

  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition];

  formControlOpenAirSearch: FormControl = new FormControl();
  formControlOpenAirSearchBy: FormControl = new FormControl(this.KEY_SEARCH_BY_CODE);

  openAirProjectObs: Observable<OpenAireProjectDTO[]> = MemoizedUtil.list(this._store, SharedExternalDataOpenAireState);
  openAirProjectIsLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedExternalDataOpenAireState);
  openAirProjectIsLoadingChunkObs: Observable<boolean> = MemoizedUtil.select(this._store, SharedExternalDataOpenAireState, state => state.isLoadingChunk);

  @ViewChild("inputElementRef", {static: false})
  inputElementRef: ElementRef;

  get environment(): AouEnvironment {
    return environment;
  }

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService,
              protected readonly _dialog: MatDialog) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.formControlOpenAirSearch.valueChanges.pipe(
      distinctUntilChanged(),
      tap(value => {
        this._store.dispatch(new SharedExternalDataOpenAireAction.Clean(false));
      }),
    ));
  }

  searchOpenAirJournal(): void {
    setTimeout(() => this.inputElementRef?.nativeElement?.focus(), 0);
    this._searchOpenAirJournal(this.formControlOpenAirSearchBy.value, this.formControlOpenAirSearch.value);
  }

  private _searchOpenAirJournal(key: string, value: string): void {
    const queryParameters = new QueryParameters();
    const search = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(search, "type", key);
    MappingObjectUtil.set(search, "value", value);
    queryParameters.paging.pageSize = 10;
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new SharedExternalDataOpenAireAction.GetAll(queryParameters),
      SharedExternalDataOpenAireAction.GetAllSuccess,
      result => {
        if (result.list._data.length > 0) {
          return;
        }
        this._openDialogNotFoundAndAskIfNeedToCreate();
      }));
  }

  private _openDialogNotFoundAndAskIfNeedToCreate(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.funding.modal.notFound.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.funding.modal.notFound.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yes,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.primary,
      colorCancel: ButtonColorEnum.primary,
    }, {takeOne: true}, isConfirmed => this._openDialogFormFunding()));
  }

  private _openDialogFormFunding(): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositCreateFundingDialog, {
      preSetName: this.formControlOpenAirSearchBy.value === this.KEY_SEARCH_BY_ACRONYM ? this.formControlOpenAirSearch?.value : undefined,
      preSetCode: this.formControlOpenAirSearchBy.value === this.KEY_SEARCH_BY_CODE ? this.formControlOpenAirSearch?.value : undefined,
    }, {takeOne: true}, funding => {
      const formGroup = DepositFormDescriptionFundingPresentational.createFunding(this._fb, this.formDefinition, funding);
      this.formArray.push(formGroup);
      this.formControlOpenAirSearch.setValue(undefined);
    }));
  }

  loadNextChunkOpenAir(): void {
    this._store.dispatch(new SharedExternalDataOpenAireAction.LoadNextChunkList());
  }

  openAirProjectSelected($event: MatAutocompleteSelectedEvent): void {
    this.formControlOpenAirSearch.setValue(undefined);
    this._store.dispatch(new SharedExternalDataOpenAireAction.Clean(false));

    const openAireProject = $event.option.value as OpenAireProjectDTO;

    const formGroup = DepositFormDescriptionFundingPresentational.createFunding(this._fb, this.formDefinition, {
      funder: openAireProject.funding?.name,
      name: openAireProject.title,
      code: openAireProject.code as any,
      acronym: openAireProject.acronym,
      jurisdiction: openAireProject.funding?.jurisdiction,
      program: openAireProject.program,
    });

    this.formArray.push(formGroup);
  }

  static createFunding(_fb: FormBuilder,
                       formDefinition: FormComponentFormDefinitionDepositFormFunding,
                       founding: DepositFormFunding | undefined = undefined): FormGroup {
    const form = _fb.group({
      [formDefinition.funder]: [undefined, [SolidifyValidator]],
      [formDefinition.name]: [undefined, [SolidifyValidator]],
      [formDefinition.code]: [undefined, [SolidifyValidator]],
      [formDefinition.acronym]: [undefined, [SolidifyValidator]],
      [formDefinition.jurisdiction]: [undefined, [SolidifyValidator]],
      [formDefinition.program]: [undefined, [SolidifyValidator]],
    });

    if (isNotNullNorUndefined(founding)) {
      form.get(formDefinition.funder).setValue(founding.funder);
      form.get(formDefinition.name).setValue(founding.name);
      form.get(formDefinition.code).setValue(founding.code);
      form.get(formDefinition.acronym).setValue(founding.acronym);
      form.get(formDefinition.jurisdiction).setValue(founding.jurisdiction);
      form.get(formDefinition.program).setValue(founding.program);
    }

    return form;
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositFormFunding): FormGroup {
    return DepositFormDescriptionFundingPresentational.createFunding(fb, formDefinition);
  }

  navigateToSnsf(formGroup: FormGroup): void {
    const code = formGroup.get(this.formDefinition.code)?.value;
    if (isNullOrUndefinedOrWhiteString(code)) {
      return;
    }
    SsrUtil.window?.open(environment.snsfWebsiteId + code);
  }
}
