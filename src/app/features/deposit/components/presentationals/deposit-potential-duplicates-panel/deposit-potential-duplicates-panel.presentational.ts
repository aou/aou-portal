/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-potential-duplicates-panel.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  ErrorDtoMessage,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-potential-duplicates",
  templateUrl: "./deposit-potential-duplicates-panel.presentational.html",
  styleUrls: ["./deposit-potential-duplicates-panel.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositPotentialDuplicatesPanelPresentational extends SharedAbstractPresentational {
  @Input()
  errorMessage: ErrorDtoMessage;

  get isInternalLink(): boolean {
    return this.errorMessage.url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
  }

  constructor(protected readonly _router: RouterExtensionService) {
    super();
  }

  navigate(): void {
    this._router.navigate([this.errorMessage.url], undefined, true);
  }
}
