/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-validation-panel.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {DepositHelper} from "@deposit/helpers/deposit.helper";
import {Navigate} from "@ngxs/router-plugin";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isEmptyArray,
  isNonEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
  StringUtil,
  ValidationErrorDto,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-validation-panel",
  templateUrl: "./deposit-validation-panel.presentational.html",
  styleUrls: ["deposit-validation-panel.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositValidationPanelPresentational extends SharedAbstractPresentational {
  readonly SEPARATOR: string = " > ";

  @Input()
  depositId: string;

  @Input()
  isValidationMode: boolean = false;

  @Input()
  isEditAvailable: boolean = false;

  private _subType: string;

  @Input()
  set subType(value: string) {
    this._subType = value;
    this._computeErrors();
  }

  get subType(): string {
    return this._subType;
  }

  listErrors: ValidationError[];

  private _listValidationError: ValidationErrorDto[];

  @Input()
  set listValidationError(value: ValidationErrorDto[]) {
    this._listValidationError = value;
    this._computeErrors();
  }

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private _computeErrors(): void {
    this.listErrors = [];
    if (isEmptyArray(this._listValidationError)) {
      return;
    }
    this._listValidationError.forEach(validationError => {

      this.listErrors.push({
        listTermTranslatable: this._getPathName(validationError.fieldName),
        errorMessages: validationError.errorMessages,
        nativeValidationErrorDto: validationError,
      });
    });
  }

  private _getPathName(fieldPath: string): string[] {
    if (isNullOrUndefinedOrWhiteString(fieldPath)) {
      return [fieldPath];
    }
    const translatedPath = DepositValidationPanelPresentational._getTranslationForFieldPath(fieldPath);
    if (isNonEmptyArray(translatedPath)) {
      return translatedPath;
    }

    const listTerms = fieldPath.split(SOLIDIFY_CONSTANTS.DOT);
    return listTerms.length === 1 ?
      [DepositValidationPanelPresentational._getTranslationForTerm(listTerms[0], this.subType, fieldPath)] :
      listTerms.map(t => DepositValidationPanelPresentational._getTranslationForTerm(t, this.subType, fieldPath));
  }

  private static _getTranslationForTerm(term: string, subType: string, fieldPath: string): string {
    if (term === DepositFormRuleHelper._fdMain.description || term === DepositFormRuleHelper._fdMain.type) {
      return LabelTranslateEnum.descriptionOfTheDocument;
    }
    if (term === DepositFormRuleHelper._fdMain.contributors) {
      return LabelTranslateEnum.contributors;
    }
    const translatedTerm = DepositFormRuleHelper.getLabelDependingOfSubType(subType, fieldPath);
    if (isNotNullNorUndefinedNorWhiteString(translatedTerm)) {
      return translatedTerm;
    }
    return StringUtil.capitalize(term);
  }

  private static _getTranslationForFieldPath(fieldPath: string): string[] | undefined {
    // ALLOW TO DEFINE SPECIFIC NAMING FOR SPECIFIC PATH : Example :
    // if (fieldPath === DepositFormRuleHelper.pathAward) {
    //   return [LabelTranslateEnum.description, LabelTranslateEnum.award];
    // }
    return undefined;
  }

  navigateToForm(error: ValidationError): void {
    if (!this.isEditAvailable) {
      return;
    }
    const fieldPath = error.nativeValidationErrorDto.fieldName;
    const navigation = DepositHelper.getNavigateToFieldPath(fieldPath, this.isValidationMode, this.depositId);
    if (isNullOrUndefined(navigation)) {
      return;
    }
    this._navigateBS.next(navigation);
  }
}

interface ValidationError {
  listTermTranslatable: string[];
  errorMessages: string[];
  nativeValidationErrorDto: ValidationErrorDto;
}
