/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-multiple-import.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DepositMultipleImportTypeDialog} from "@app/features/deposit/components/dialogs/deposit-multiple-import-type/deposit-multiple-import-type.dialog";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {ImportMethodEnum} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {
  DepositImportRowModel,
  FormComponentFormDefinitionDepositImportRow,
} from "@app/features/deposit/components/presentationals/deposit-import-row/deposit-import-row.presentational";
import {DepositRepeatableFieldsPresentational} from "@app/features/deposit/components/presentationals/deposit-repeatable-fields/deposit-repeatable-fields.presentational";
import {DepositImportResultModel} from "@app/features/deposit/models/deposit-import-result.model";
import {environment} from "@environments/environment";
import {
  Deposit,
  PublicationImportDTO,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DoiCleaner} from "@shared/validators/doi.validator";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  ArrayUtil,
  BaseFormDefinition,
  DialogUtil,
  FormValidationHelper,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-multiple-import",
  templateUrl: "./deposit-multiple-import.presentational.html",
  styleUrls: ["./deposit-multiple-import.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositMultipleImportPresentational extends DepositAbstractFormListPresentational implements OnInit {
  @Input()
  creatorId: string;

  numberPublicationFinishedInError: number = 0;

  private _listResult: MappingObject<string, DepositImportResultModel>;

  @Input()
  set listResult(value: MappingObject<string, DepositImportResultModel>) {
    this._listResult = value;

    this.numberPublicationFinishedInError = 0;
    MappingObjectUtil.forEach(this._listResult, result => {
      if (isNotNullNorUndefinedNorWhiteString(result.error) || isNullOrUndefined(result.deposit)) {
        this.numberPublicationFinishedInError++;
      }
    });
  }

  get listResult(): MappingObject<string, DepositImportResultModel> {
    return this._listResult;
  }

  @Input()
  isImportLaunched: boolean;

  get numberTotalPublication(): number {
    return this.listPublicationImport?.length;
  }

  get numberPublicationFinished(): number {
    return MappingObjectUtil.size(this.listResult);
  }

  private readonly _LIST_SEPARATOR_PASTER_IDENTIFIER: string[] = [SOLIDIFY_CONSTANTS.COMMA, SOLIDIFY_CONSTANTS.SPACE, environment.doiWebsite, "http://doi.org/", "http://dx.doi.org/", "https://dx.doi.org/"];
  private readonly _LAST_SEPARATOR_PASTER_IDENTIFIER: string = SOLIDIFY_CONSTANTS.LINE_BREAK;

  private readonly _openDepositBS: BehaviorSubject<Deposit | undefined> = new BehaviorSubject<Deposit | undefined>(undefined);
  @Output("openDeposit")
  readonly openDepositObs: Observable<Deposit | undefined> = ObservableUtil.asObservable(this._openDepositBS);

  formDefinition: FormComponentFormDefinitionDepositImportRow = new FormComponentFormDefinitionDepositImportRow();

  listPublicationImport: PublicationImportDTO[];

  generateListPublicationImport(): PublicationImportDTO[] {
    const listToImport: PublicationImportDTO[] = [];
    const listIndexArrayToRemove: number[] = [];
    this.formArray.controls.forEach((importRowFromGroup, index) => {
      const service = importRowFromGroup.get(this.formDefinition.service)?.value;
      const id = importRowFromGroup.get(this.formDefinition.id)?.value;

      if ((service !== ImportMethodEnum.doi && service !== ImportMethodEnum.pubmed) || !isNonEmptyString(id)) {
        return;
      }
      if (listToImport.findIndex(i => (service === ImportMethodEnum.doi && i.doi === id) || (service === ImportMethodEnum.pubmed && i.pmid === id))) {
        listToImport.push({
          doi: service === ImportMethodEnum.doi ? id : undefined,
          pmid: service === ImportMethodEnum.pubmed ? id : undefined,
          creatorId: this.creatorId,
        });
      } else {
        listIndexArrayToRemove.push(index);
      }
    });
    listIndexArrayToRemove.reverse().forEach(index => {
      this.formArray.removeAt(index);
    });
    this.listPublicationImport = listToImport;
    return this.listPublicationImport;
  }

  @ViewChild("depositRepeatableFieldsPresentational")
  depositRepeatableFieldsPresentational: DepositRepeatableFieldsPresentational;

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService,
              public readonly _dialog: MatDialog,
  ) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.formArray = this._fb.array([]);
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositImportRow, model?: DepositImportRowModel): FormGroup {
    const formGroup = fb.group({
      [formDefinition.id]: [undefined, [SolidifyValidator, Validators.required, DoiCleaner]],
      [formDefinition.service]: [undefined, [SolidifyValidator, Validators.required]],
    });

    if (isNotNullNorUndefined(model)) {
      formGroup.patchValue(model as any);
    }

    return formGroup;
  }

  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition];

  get percentageImportCompletion(): number {
    return (this.numberPublicationFinished / this.numberTotalPublication) * 100;
  }

  get percentageImportErrorCompletion(): number {
    return (this.numberPublicationFinishedInError / this.numberTotalPublication) * 100;
  }

  getResult(formGroup: FormGroup): DepositImportResultModel | undefined {
    if (!this.isImportLaunched) {
      return;
    }
    const id = formGroup.get(this.formDefinition.id)?.value;
    return MappingObjectUtil.get(this.listResult, id);
  }

  openDeposit(deposit: Deposit): void {
    this._openDepositBS.next(deposit);
  }

  paste($event: ClipboardEvent): void {
    let pastedString: string = ($event.clipboardData || SsrUtil.window?.["clipboardData"]).getData("text");
    if (!isNonEmptyString(pastedString)) {
      return;
    }

    this._LIST_SEPARATOR_PASTER_IDENTIFIER.forEach(separator => {
      let listTerm = pastedString.split(separator);
      listTerm = listTerm.map(s => s.trim()).filter(s => isNonEmptyString(s));
      pastedString = listTerm.join(this._LAST_SEPARATOR_PASTER_IDENTIFIER);
    });
    let listIdentifier: string[] = pastedString.split(this._LAST_SEPARATOR_PASTER_IDENTIFIER).map(value => value.toLowerCase().trim());
    listIdentifier = ArrayUtil.distinct(listIdentifier);

    if (listIdentifier.length === 0) {
      return;
    }

    this.subscribe(
      DialogUtil.open(this._dialog, DepositMultipleImportTypeDialog, {
        listPastedIdentifier: listIdentifier,
      }, undefined, importMethod => {
        listIdentifier.forEach(value => {
          const newImportRow: DepositImportRowModel = {
            id: value,
            service: importMethod,
          };
          if (this.formArray.value.findIndex((row: DepositImportRowModel) => row.id === newImportRow.id && row.service === newImportRow.service) === -1) {
            this.formArray.push(this.formGroupFactory(this._fb, this.formDefinition, newImportRow));
          }
        });

        this.depositRepeatableFieldsPresentational.externalChangeDetector();
      }),
    );

    $event.preventDefault();
    $event.stopPropagation();
  }

  keypress($event: KeyboardEvent): void {
    this._notificationService.showInformation(LabelTranslateEnum.pasteTheListOfIdentifiersHere);
    $event.preventDefault();
    $event.stopPropagation();
  }

  add(): void {
    this.depositRepeatableFieldsPresentational.add();
    this.depositRepeatableFieldsPresentational.externalChangeDetector();
  }
}
