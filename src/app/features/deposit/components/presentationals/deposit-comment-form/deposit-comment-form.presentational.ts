/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-comment-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Injector,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {AppState} from "@app/stores/app.state";
import {
  Comment,
  User,
} from "@models";
import {Store} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BehaviorSubject} from "rxjs";
import {Observable} from "rxjs/index";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  isNonEmptyArray,
  MARK_AS_TRANSLATABLE,
  ObservableUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-comment-form",
  templateUrl: "./deposit-comment-form.presentational.html",
  styleUrls: ["deposit-comment-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCommentFormPresentational extends AbstractFormPresentational<Comment> {
  readonly MAX_NUMBER_CHARACTER: number = 4000;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  showCommentForm: boolean = false;
  idCommentEdited: string | undefined = undefined;
  isOpen: boolean = false;

  @ViewChild("textAreaElement")
  private _textAreaElement: ElementRef;

  @Input()
  @HostBinding("class.internal-comment")
  isValidator: boolean = false;

  private _listComments: Comment[];

  @Input()
  set listComments(value: Comment[]) {
    this._listComments = value;
    if (isNonEmptyArray(this._listComments)) {
      this.isOpen = true;
    }
  }

  get listComments(): Comment[] {
    return this._listComments;
  }

  @HostBinding("class.without-panel")
  @Input()
  withoutPanel: boolean = false;

  protected override _readonly: boolean = true;

  @Input()
  override set readonly(value: boolean) {
    this._readonly = value;
  }

  override get readonly(): boolean {
    return this._readonly;
  }

  private readonly _createCommentBS: BehaviorSubject<Comment | undefined> = new BehaviorSubject<Comment | undefined>(undefined);
  @Output("commentCreated")
  readonly commentCreatedObs: Observable<Comment | undefined> = ObservableUtil.asObservable(this._createCommentBS);

  private readonly _deleteCommentBS: BehaviorSubject<Comment | undefined> = new BehaviorSubject<Comment | undefined>(undefined);
  @Output("commentDeleted")
  readonly commentDeletedObs: Observable<Comment | undefined> = ObservableUtil.asObservable(this._deleteCommentBS);

  trackByFn(index: number, comment: Comment): string {
    return comment.resId;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              protected readonly _dialog: MatDialog,
              private readonly _fb: FormBuilder,
              private readonly _store: Store,
  ) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.text]: ["", [Validators.required, Validators.maxLength(this.MAX_NUMBER_CHARACTER), SolidifyValidator]],
    });
  }

  protected _bindFormTo(comment: Comment): void {
    this.form = this._fb.group({
      [this.formDefinition.text]: [comment.text, [Validators.required, Validators.maxLength(this.MAX_NUMBER_CHARACTER), SolidifyValidator]],
      [this.formDefinition.resId]: [comment.resId, []],
    });
  }

  protected _treatmentBeforeSubmit(comment: Comment): Comment {
    return comment;
  }

  submit(): void {
    const model = this._treatmentBeforeSubmit(this.form.value as Comment);
    model.onlyForValidators = this.isValidator;
    this._createCommentBS.next(model);
    this.showCommentForm = false;
    this.idCommentEdited = undefined;
  }

  cancel(): void {
    this.showCommentForm = false;
    this.idCommentEdited = undefined;
  }

  openCommentForm(): void {
    this._initNewForm();
    this.isOpen = true;
    this.showCommentForm = true;
    setTimeout(() => this._textAreaElement?.nativeElement?.focus(), 0);
  }

  editComment(comment: Comment): void {
    this._bindFormTo(comment);
    this.showCommentForm = true;
    this.idCommentEdited = comment.resId;
    setTimeout(() => this._textAreaElement?.nativeElement?.focus(), 0);
  }

  deleteComment(comment: Comment): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.bulkDelete.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.comment.deleteAction.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, undefined, isConfirmed => this._deleteCommentBS.next(comment)));
  }

  isCurrentUserComment(comment: Comment): boolean {
    const currentUser: User = this._store.selectSnapshot(AppState.currentUser);
    return comment.person.resId === currentUser.person.resId;
  }

  externalChangeDetector(): void {
    this._changeDetectorRef.detectChanges();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() text: string;
  @PropertyName() resId: string;
}
