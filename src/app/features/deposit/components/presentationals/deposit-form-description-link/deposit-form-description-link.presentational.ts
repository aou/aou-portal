/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-link.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  AbstractControlOptions,
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {DepositFormPresentational} from "@app/features/deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {
  DepositFormLink,
  FormComponentFormDefinitionDepositFormLink,
  FormComponentFormDefinitionDepositFormTextLanguage,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {Language} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BaseFormDefinition,
  isNotNullNorUndefined,
  KeyValue,
  NotificationService,
  SolidifyValidator,
} from "solidify-frontend";
import {TrimValidator} from "@shared/validators/trim.validator";

@Component({
  selector: "aou-deposit-form-description-link",
  templateUrl: "./deposit-form-description-link.presentational.html",
  styleUrls: ["./deposit-form-description-link.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionLinkPresentational,
    },
  ],
})
export class DepositFormDescriptionLinkPresentational extends DepositAbstractFormListPresentational implements ControlValueAccessor {
  formDefinition: FormComponentFormDefinitionDepositFormLink = new FormComponentFormDefinitionDepositFormLink();
  formDefinitionDepositFormTextLanguage: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();

  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition, this.formDefinitionDepositFormTextLanguage];

  linkTypesEnumValues: KeyValue[] = Enums.Deposit.LinkTypesEnumTranslate;

  @Input()
  listLanguages: Language[];

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
  }

  static createLink(_fb: FormBuilder,
                    formDefinition: FormComponentFormDefinitionDepositFormLink,
                    formDefinitionDepositFormTextLanguage: FormComponentFormDefinitionDepositFormTextLanguage,
                    link: DepositFormLink | undefined = undefined): FormGroup {
    const form = _fb.group({
      [formDefinition.target]: [undefined, {
        updateOn: "blur",
        validators: [TrimValidator, SolidifyValidator],
      } as AbstractControlOptions],
      [formDefinition.description]: _fb.group({
        [formDefinitionDepositFormTextLanguage.text]: ["", [SolidifyValidator]],
        [formDefinitionDepositFormTextLanguage.lang]: ["", [SolidifyValidator]],
      }),
      [formDefinition.type]: [undefined, [SolidifyValidator]],
    });

    if (isNotNullNorUndefined(link)) {
      form.get(formDefinition.target).setValue(link.target);
      DepositFormPresentational.patchFormWithKey(form, formDefinition.description, link.description);
      form.get(formDefinition.type).setValue(link.type);
    }

    return form;
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositFormLink, formDefinitionDepositFormTextLanguage: FormComponentFormDefinitionDepositFormTextLanguage): FormGroup {
    return DepositFormDescriptionLinkPresentational.createLink(fb, formDefinition, formDefinitionDepositFormTextLanguage);
  }
}
