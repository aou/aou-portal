/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-repeatable-fields.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {AOU_CONSTANTS} from "@app/constants";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  BaseFormDefinition,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-repeatable-fields",
  templateUrl: "./deposit-repeatable-fields.presentational.html",
  styleUrls: ["./deposit-repeatable-fields.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositRepeatableFieldsPresentational extends SharedAbstractPresentational implements OnInit {
  readonly ERROR_FROM_BACKEND: string = AOU_CONSTANTS.ERROR_FROM_BACKEND;

  @Input()
  labelSectionToTranslate: string;

  @Input()
  fieldsTemplate: TemplateRef<any>;

  @Input()
  headerTemplate: TemplateRef<any>;

  @Input()
  formArray: FormArray;

  @Input()
  formGroupFactory: (fb: FormBuilder, ...formDefinitions: BaseFormDefinition[]) => FormGroup;

  @Input()
  readonly: boolean;

  @Input()
  listFormDefinitions: BaseFormDefinition[];

  @Input()
  fixAlignmentDeleteButton: boolean = true;

  @Input()
  fixAlignmentAddButton: boolean = true;

  @Input()
  canAdd: boolean = true;

  @Input()
  canDeleteFirst: boolean = false;

  @Input()
  canSort: boolean = true;

  @Input()
  withoutPanel: boolean = false;

  @Input()
  isPanelOpenByDefault: boolean = true;

  @Input()
  isPanelCollapsable: boolean = false;

  @Input()
  withFixButton: boolean = false;

  @Input()
  isValidationMode: boolean = false;

  @Input()
  typePanel: "main" | "secondary";

  @Input()
  dragCssClass: string;

  isDragging: boolean = false;

  private readonly _fixBS: BehaviorSubject<FormGroup | undefined> = new BehaviorSubject<FormGroup | undefined>(undefined);
  @Output("fixChange")
  readonly fixObs: Observable<FormGroup | undefined> = ObservableUtil.asObservable(this._fixBS);

  get cssClass(): string {
    const list = [];
    list.push("panel");
    if (this.typePanel === "main") {
      list.push("main-panel");
    } else if (this.typePanel === "secondary") {
      list.push("secondary-panel");
    }
    return list.join(" ");
  }

  get canDrag(): boolean {
    return !this.readonly && this.getFormArrayControl?.length > 1 && this.canSort;
  }

  constructor(private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.formArray.statusChanges.pipe(
      distinctUntilChanged(),
      filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
      tap(status => {
        setTimeout(() => {
          this._changeDetector.detectChanges();
        });
      }),
    ));

    this.subscribe(this.formArray.valueChanges.pipe(
      distinctUntilChanged(),
      filter(value => this.formArray.invalid),
      tap(value => {
        this.formArray.updateValueAndValidity();
      }),
    ));
  }

  get getFormArrayControl(): FormGroup[] {
    return this.formArray.controls as FormGroup[];
  }

  remove(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  fix(index: number): void {
    this._fixBS.next(this.getFormArrayControl[index]);
  }

  add(): void {
    this.formArray.push(this.formGroupFactory(this._fb, ...this.listFormDefinitions));
  }

  dropToSort(event: CdkDragDrop<any>): void {
    const movedLine = this.formArray.at(event.previousIndex);
    this.formArray.removeAt(event.previousIndex);
    this.formArray.insert(event.currentIndex, movedLine);
  }

  startDragging(): void {
    this.isDragging = true;
  }

  endDragging(): void {
    this.isDragging = false;
  }

  externalChangeDetector(): void {
    this._changeDetector.detectChanges();
  }

  trackByFn(index: number, item: FormGroup): string {
    return index + "";
  }
}
