/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-container.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {
  DepositFormContainer,
  FormComponentFormDefinitionDepositFormContainer,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {Language} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BreakpointService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  NotificationService,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-container",
  templateUrl: "./deposit-form-description-container.presentational.html",
  styleUrls: ["./deposit-form-description-container.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionContainerPresentational,
    },
  ],
})
export class DepositFormDescriptionContainerPresentational extends DepositAbstractFormPresentational {
  formDefinition: FormComponentFormDefinitionDepositFormContainer = new FormComponentFormDefinitionDepositFormContainer();

  @Input()
  listLanguages: Language[];

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService,
              public readonly breakpointService: BreakpointService) {
    super(_changeDetector);
  }

  static bindContainer(_fb: FormBuilder,
                       formDefinition: FormComponentFormDefinitionDepositFormContainer,
                       form: FormGroup | undefined = undefined,
                       containerContainer: DepositFormContainer | undefined = undefined): FormGroup {
    if (isNullOrUndefined(form)) {
      return form;
    }

    if (isNotNullNorUndefined(containerContainer)) {
      form.get(formDefinition.title).setValue(containerContainer.title);
      form.get(formDefinition.editor).setValue(containerContainer.editor);
      form.get(formDefinition.conferenceSubtitle).setValue(containerContainer.conferenceSubtitle);
      form.get(formDefinition.conferencePlace).setValue(containerContainer.conferencePlace);
      form.get(formDefinition.volume).setValue(containerContainer.volume);
      form.get(formDefinition.issue).setValue(containerContainer.issue);
      form.get(formDefinition.conferenceDate).setValue(containerContainer.conferenceDate);
      form.get(formDefinition.specialIssue).setValue(containerContainer.specialIssue);
    }

    return form;
  }
}
