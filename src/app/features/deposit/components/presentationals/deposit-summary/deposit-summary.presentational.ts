/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-summary.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnChanges,
  Output,
  Renderer2,
  SimpleChanges,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DepositValidateResearchGroupDialog} from "@app/features/deposit/components/dialogs/deposit-validate-research-group/deposit-validate-research-group.dialog";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {DepositHelper} from "@app/features/deposit/helpers/deposit.helper";
import {DocumentFileUploadHelper} from "@app/features/deposit/helpers/document-file-upload.helper";
import {
  DepositFormAbstractContributor,
  DepositFormClassification,
  DepositFormContainer,
  DepositFormCorrection,
  DepositFormDate,
  DepositFormFunding,
  DepositFormLanguage,
  DepositFormModel,
  DepositFormStepDescription,
  DepositFormText,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {depositActionNameSpace} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {AppContributorRolesState} from "@app/stores/contributor-role/app-contributor-roles.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Creator,
  PublicationSubtypeContributorRoleDTO,
  ResearchGroup,
  Romeo,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedDepositSubtypeActionNameSpace} from "@shared/stores/deposit-subtype/shared-deposit-subtype.action";
import {SharedDepositSubtypeState} from "@shared/stores/deposit-subtype/shared-deposit-subtype.state";
import {sharedDepositTypeActionNameSpace} from "@shared/stores/deposit-type/shared-deposit-type.action";
import {SharedDepositTypeState} from "@shared/stores/deposit-type/shared-deposit-type.state";
import {sharedDepositSubSubtypeActionNameSpace} from "@shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.action";
import {SharedDepositSubSubtypeState} from "@shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.state";
import {sharedResearchGroupActionNameSpace} from "@shared/stores/research-group/shared-research-group.action";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {sharedStructureActionNameSpace} from "@shared/stores/structure/shared-structure.action";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {AouObjectUtil} from "@shared/utils/aou-object.util";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {isDate} from "moment";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractPresentational,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  EnumUtil,
  isEmptyArray,
  isEmptyString,
  isFalse,
  isNonEmptyArray,
  isNonEmptyString,
  isNonWhiteString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isNumber,
  isTrue,
  KeyValue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ObjectUtil,
  ObservableUtil,
  OrcidService,
  PersonWithEmail,
  ResourceFileNameSpace,
  ResourceNameSpace,
  ResourceState,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-summary",
  templateUrl: "./deposit-summary.presentational.html",
  styleUrls: ["deposit-summary.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositSummaryPresentational extends AbstractPresentational implements OnChanges {
  private _KEYWORD_LENGTH_TO_CHECK: number = 50;
  private readonly _SHORT_TITLE_MAX_SIZE: number = 35;
  private readonly _MAIL_TO: string = "mailto:";
  private readonly _MAIL_SUBJECT: string = "subject";
  private readonly _MAIL_BODY: string = "body";
  private readonly _JEL: string = "JEL";

  private readonly _LIST_CLEANED_ATTRIBUTES_KEY_TO_HIDE: string[] = [
    "description.classifications.code",
    "description.classifications.item",
    "files.files.resId",
    "files.files.type",
    "files.files.accessLevel",
    "files.files.name",
    "files.files.size",
    "files.files.mimetype",
  ];

  private readonly _RESEARCH_GROUP_ID: string = "researchGroup";

  private _depositId: string;

  @Input()
  set depositId(value: string) {
    this._depositId = value;
    this._computeMailToCreatorUrl();
    this._computeMailToLastEditorUrl();
  }

  get depositId(): string {
    return this._depositId;
  }

  @Input()
  isValidationMode: boolean = false;

  @Input()
  depositStatus: Enums.Deposit.StatusEnum | undefined;

  depositActionNameSpace: ResourceFileNameSpace = depositActionNameSpace;
  depositState: typeof DepositState = DepositState;

  cleanedAttributes: KeyValue[] = [];

  private _depositFormModel: DepositFormModel;

  depositFormModelCleanToSubmit: DepositFormModel;

  mailToCreatorUrl: string;
  mailToLastEditorUrl: string;

  @Input()
  set depositFormModel(value: DepositFormModel) {
    if (isNullOrUndefined(value)) {
      return;
    }
    this.depositFormModelCleanToSubmit = DepositFormRuleHelper.applyRuleToCleanUndesiredDataDependingOfType(value, this.cleanedAttributes);
    this.hideSpecificCleanedAttributes(this.cleanedAttributes);
    this._depositFormModel = ObjectUtil.clone(this.depositFormModelCleanToSubmit, true);
    this._depositFormModel = AouObjectUtil.cleanEmptyAttributes(this._depositFormModel);
    this._computeMailToCreatorUrl();
    this._computeMailToLastEditorUrl();
  }

  get depositFormModel(): DepositFormModel {
    return this._depositFormModel;
  }

  private hideSpecificCleanedAttributes(cleanedAttributes: KeyValue[]): void {
    this.cleanedAttributes = this.cleanedAttributes.filter(c => this._LIST_CLEANED_ATTRIBUTES_KEY_TO_HIDE.indexOf(c.key) === -1);
  }

  @Input()
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[];

  private _creator: Creator;

  @Input()
  set creator(value: Creator) {
    this._creator = value;
    this._computeMailToCreatorUrl();
  }

  get creator(): Creator {
    return this._creator;
  }

  private _lastEditor: PersonWithEmail;

  @Input()
  set lastEditor(value: Creator) {
    this._lastEditor = value;
    this._computeMailToLastEditorUrl();
  }

  get lastEditor(): PersonWithEmail {
    return this._lastEditor;
  }

  @Input()
  listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>;

  @Input()
  romeo: Romeo;

  private readonly _navigateToStepBS: BehaviorSubject<Enums.Deposit.StepEnum> = new BehaviorSubject<Enums.Deposit.StepEnum>(undefined);
  @Output("navigateToStepChange")
  readonly navigateToStepObs: Observable<Enums.Deposit.StepEnum> = ObservableUtil.asObservable(this._navigateToStepBS);

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _refreshValidationErrorBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("refreshValidationErrorChange")
  readonly refreshValidationErrorObs: Observable<void> = ObservableUtil.asObservable(this._refreshValidationErrorBS);

  isWithPhotoObs: Observable<boolean> = MemoizedUtil.select(this._store, DepositState, state => isNotNullNorUndefinedNorWhiteString(state.file));

  listInfoMains: Info[];

  private _infoType: Info;
  private _infoSubType: Info | undefined;
  private _infoSubSubType: Info | undefined;

  contributors: DepositSummaryValuePerson[] | undefined;
  collaborationMember: DepositSummaryValuePerson[] | undefined;
  longKeywordDetected: boolean = false;
  abstracts: DepositFormText[] | undefined;

  keywordCharLimitForWarning: number = environment.keywordCharLimitForWarning;

  get getMostPreciseType(): Info {
    if (isNotNullNorUndefined(this._infoSubSubType)) {
      return this._infoSubSubType;
    }
    if (isNotNullNorUndefined(this._infoSubType)) {
      return this._infoSubType;
    }
    if (isNotNullNorUndefined(this._infoType)) {
      return this._infoType;
    }
    return undefined;
  }

  get depositFormRuleHelper(): typeof DepositFormRuleHelper {
    return DepositFormRuleHelper;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get depositStepEnum(): typeof Enums.Deposit.StepEnum {
    return Enums.Deposit.StepEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get typeInfoEnum(): typeof TypeInfoEnum {
    return TypeInfoEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get documentFileUploadHelper(): typeof DocumentFileUploadHelper {
    return DocumentFileUploadHelper;
  }

  trackByFn(index: number, item: Info): string {
    return item.label;
  }

  typeIntoInfo(value: any): Info {
    return value;
  }

  get isPublicationHost(): boolean {
    const subType = this.depositFormModel.type?.subtype;
    return DepositHelper.isPublicationHost(subType);
  }

  private _getLabel(fieldPath: string): string {
    const subtype = this.depositFormModel.type?.subtype;
    if (isNullOrUndefined(subtype)) {
      return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
    return DepositFormRuleHelper.getLabelDependingOfSubType(subtype, fieldPath);
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _orcidService: OrcidService,
              private readonly _store: Store,
              private readonly _renderer: Renderer2,
              private readonly _dialog: MatDialog,
              private readonly _securityService: SecurityService,
              private readonly _translate: TranslateService,
  ) {
    super();
    this.subscribe(this._translate.onLangChange, () => {
      this._computeMailToCreatorUrl();
      this._computeMailToLastEditorUrl();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
    if (isNotNullNorUndefined(this.depositFormModel)) {
      this._resetListInfo();
      this._defineListInfo();
    }
  }

  private _refreshResearchGroup(): void {
    const index = this.listInfoMains.findIndex(i => i.id === this._RESEARCH_GROUP_ID);
    this.listInfoMains = this.listInfoMains.filter(i => i.id !== this._RESEARCH_GROUP_ID);
    this.listInfoMains.splice(index, 0, ...this._getGroups());
    this.listInfoMains = [...this.listInfoMains];
    this._changeDetectorRef.detectChanges();
  }

  private _resetListInfo(): void {
    this.listInfoMains = [];
  }

  private _defineListInfo(): void {
    this._infoType = {
      label: LabelTranslateEnum.type,
      value: this.depositFormModel.type?.type,
      state: SharedDepositTypeState,
      action: sharedDepositTypeActionNameSpace,
      labelKey: "name",
      type: TypeInfoEnum.translate,
      fieldPath: DepositFormRuleHelper.pathType,
    };

    if (isNonEmptyString(this.depositFormModel.type?.subtype)) {
      this._infoSubType = {
        label: LabelTranslateEnum.subtype,
        value: this.depositFormModel.type?.subtype,
        state: SharedDepositSubtypeState as any,
        action: sharedDepositSubtypeActionNameSpace as any,
        labelKey: "labels",
        type: TypeInfoEnum.translate,
        isBackTranslate: true,
        fieldPath: DepositFormRuleHelper.pathSubType,
      };
    }

    if (isNonEmptyString(this.depositFormModel.type?.subsubtype)) {
      this._infoSubSubType = {
        label: LabelTranslateEnum.subsubtype,
        value: this.depositFormModel.type?.subsubtype,
        state: SharedDepositSubSubtypeState as any,
        action: sharedDepositSubSubtypeActionNameSpace as any,
        labelKey: "labels",
        type: TypeInfoEnum.translate,
        isBackTranslate: true,
        fieldPath: DepositFormRuleHelper.pathSubsubtype,
      };
    }

    if (isNotNullNorUndefined(this.depositFormModel?.description?.abstracts)) {
      this.abstracts = this.depositFormModel.description.abstracts.map(abstract => AouObjectUtil.cleanEmptyAttributes(abstract));
    }

    this.contributors = this._getAllContributors(this.depositFormModel?.contributors?.contributors);
    this.collaborationMember = this._getCollaborationMembers(this.depositFormModel?.contributors?.contributors);

    this.longKeywordDetected = this.depositFormModel?.description?.keywords?.findIndex(k => k?.length > this.keywordCharLimitForWarning) > -1;

    this.listInfoMains = [
      ...this._getSectionPhdStudentContact(),
      ...this._getSectionAffiliations(),
      ...this._getSectionHostDepositOrBibliographicInformation(this.depositFormModel?.description),
      ...this._getSectionIdentifier(),
      ...this._getSectionComplementaryDescription(this.depositFormModel?.description),
      ...this._getSectionClassifications(this.depositFormModel?.description?.classifications),
      ...this._getSectionFundings(this.depositFormModel?.description?.fundings),
      ...this._getSectionLinks(),
      ...this._getSectionCorrections(this.depositFormModel?.description?.corrections),
    ];
  }

  private _getSectionPhdStudentContact(): Info[] {
    return [{
      label: LabelTranslateEnum.phDStudentContactDetails,
      type: TypeInfoEnum.listInfo,
      valuesInfo: [
        ...isNullOrUndefined(this.depositFormModel?.description?.doctorEmail) ? [] : [{
          label: this._getLabel(DepositFormRuleHelper.pathDoctorEmail),
          value: this.depositFormModel?.description?.doctorEmail,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathDoctorEmail,
        }],
        ...isNullOrUndefined(this.depositFormModel?.description?.doctorAddress) ? [] : [{
          label: this._getLabel(DepositFormRuleHelper.pathDoctorAddress),
          value: this.depositFormModel?.description?.doctorAddress,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathDoctorAddress,
        }],
      ],
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
    }];
  }

  private _getSectionAffiliations(): Info[] {
    return [{
      label: LabelTranslateEnum.affiliations,
      type: TypeInfoEnum.listInfo,
      valuesInfo: [
        ...this._getStructures(),
        ...this._getGroups(),
        {
          label: LabelTranslateEnum.publicationIsBeforeUnige,
          type: TypeInfoEnum.translate,
          value: isTrue(this.depositFormModel?.contributors?.isBeforeUnige) ? LabelTranslateEnum.yes : LabelTranslateEnum.no,
          shortcutTo: Enums.Deposit.StepEnum.CONTRIBUTORS,
          fieldPath: DepositFormRuleHelper.pathContributorsIsBeforeUnige,
        } as Info,
      ],
      shortcutTo: Enums.Deposit.StepEnum.CONTRIBUTORS,
    }];
  }

  private _getSectionHostDepositOrBibliographicInformation(description: DepositFormStepDescription): Info[] {
    const publisher = description?.publisher;
    const identifiers = description?.identifiers;
    const container: DepositFormContainer = description?.container;
    return [
      {
        label: this.isPublicationHost ? LabelTranslateEnum.hostDeposit : LabelTranslateEnum.bibliographicInformation,
        valuesInfo: [
          ...this._getInfoIfValueDefined({
            label: this._getLabel(DepositFormRuleHelper.pathContainerTitle),
            value: container?.title,
            needCheck: this.isValidationMode && isNullOrUndefinedOrWhiteString(description?.identifiers?.issn) && !DepositFormRuleHelper.ignoreContainerTitleIssnSearchOfSubType(this.depositFormModel?.type?.subtype),
            shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
            fieldPath: DepositFormRuleHelper.pathContainerTitle,
          }),
          ...this._getSimpleValue(DepositFormRuleHelper.pathIdentifierIssn, identifiers?.issn, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathPublisherPlacePublication, publisher?.place, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathPublisherPublishingHouse, publisher?.name, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getDates(description?.dates),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerEditor, container?.editor, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerConferenceSubtitle, container?.conferenceSubtitle, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerConferencePlace, container?.conferencePlace, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerConferenceDate, container?.conferenceDate, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerVolume, container?.volume, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerIssue, container?.issue, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathContainerSpecialIssue, container?.specialIssue, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getPages(),
          ...this._getSimpleValue(DepositFormRuleHelper.pathMandatedBy, description?.mandator, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathEdition, description?.edition, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathIdentifierIsbn, identifiers?.isbn, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getInfoIfValueDefined({
            label: this._getLabel(DepositFormRuleHelper.pathCollection),
            values: description?.collections?.map(c => (isNonEmptyString(c.name) ? c.name + " " : "") + (isNotNullNorUndefined(c.number) ? "(" + c.number + ")" : SOLIDIFY_CONSTANTS.STRING_EMPTY)),
            type: TypeInfoEnum.list,
            shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
            fieldPath: DepositFormRuleHelper.pathCollection,
            needCheckByElement: (index: number) => {
              const collectionNumber = description?.collections[index]?.number + "";
              return this.isValidationMode && isNotNullNorUndefinedNorWhiteString(collectionNumber) && !AouRegexUtil.onlyDigit.test(collectionNumber);
            },
          }),
        ],
        type: TypeInfoEnum.listInfo,
        shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
      },
    ];
  }

  private _getSectionIdentifier(): Info[] {
    const identifiers = this.depositFormModel?.description?.identifiers;
    return this._getInfoIfValueDefined({
      label: this._getLabel(DepositFormRuleHelper.pathIdentifier),
      valuesInfo: [
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathIdentifierDoi),
          link: {
            text: identifiers?.doi,
            url: environment.doiWebsite + identifiers?.doi,
          } as DepositSummaryLinkInfo,
          type: TypeInfoEnum.link,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathIdentifierDoi,
        }),
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathIdentifierPmid),
          link: {
            text: identifiers?.pmid,
            url: environment.pmidWebsiteId + identifiers?.pmid,
          } as DepositSummaryLinkInfo,
          type: TypeInfoEnum.link,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathIdentifierPmid,
        }),
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathIdentifierPmcid),
          link: {
            text: identifiers?.pmcid,
            url: environment.pmcidWebsiteId + identifiers?.pmcid,
          } as DepositSummaryLinkInfo,
          type: TypeInfoEnum.link,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathIdentifierPmcid,
        }),
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathIdentifierArXiv),
          link: {
            text: identifiers?.arxiv,
            url: environment.arxivWebsiteId + identifiers?.arxiv,
          } as DepositSummaryLinkInfo,
          type: TypeInfoEnum.link,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathIdentifierArXiv,
        }),
        ...this._getSimpleValue(DepositFormRuleHelper.pathIdentifierMmsid, identifiers?.mmsid, Enums.Deposit.StepEnum.DESCRIPTION),
        ...this._getSimpleValue(DepositFormRuleHelper.pathIdentifierRepEc, identifiers?.repec, Enums.Deposit.StepEnum.DESCRIPTION),
        ...this._getSimpleValue(DepositFormRuleHelper.pathIdentifierDblp, identifiers?.dblp, Enums.Deposit.StepEnum.DESCRIPTION),
        ...this._getSimpleValue(DepositFormRuleHelper.pathIdentifierUrn, identifiers?.urn, Enums.Deposit.StepEnum.DESCRIPTION),
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathIdentifierLocalNumber),
          value: identifiers?.localNumber,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathIdentifierLocalNumber,
          needCheckByElement: input => this.isValidationMode && this.depositFormModel.type?.subtype === Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS && this.isValidationMode && isNotNullNorUndefinedNorWhiteString(input) && AouRegexUtil.onlyDigit.test(input),
        }),
        ...this._getSimpleValue(DepositFormRuleHelper.pathDiscipline, this.depositFormModel?.description?.discipline, Enums.Deposit.StepEnum.DESCRIPTION),
      ],
      type: TypeInfoEnum.listInfo,
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
      fieldPath: DepositFormRuleHelper.pathIdentifier,
    });
  }

  private _getSectionComplementaryDescription(description: DepositFormStepDescription): Info[] {
    return [{
      label: LabelTranslateEnum.complementaryDescription,
      valuesInfo: [
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathKeywords),
          needCheckByElement: (index: number) => this.needToCheckKeyword(description?.keywords[index]),
          isListInline: true,
          values: description?.keywords,
          type: TypeInfoEnum.list,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathKeywords,
        }),
        ...this._getInfoIfValueDefined({
          label: LabelTranslateEnum.note,
          value: description?.note,
          needCheck: this.isValidationMode && isNotNullNorUndefinedNorWhiteString(description?.note),
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathNote,
        }),
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathJel),
          isListInline: true,
          values: description?.classifications?.filter(e => e.code === this._JEL)?.map(e => e.item),
          type: TypeInfoEnum.list,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathJel,
        }),
      ],
      type: TypeInfoEnum.listInfo,
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
    }];
  }

  private _getSectionClassifications(classifications: DepositFormClassification[]): Info[] {
    const listInfos: Info[] = [];
    if (isNullOrUndefined(classifications)) {
      return listInfos;
    }

    return [{
      label: this._getLabel(DepositFormRuleHelper.pathClassifications),
      valuesInfo: [
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathClassifications),
          values: classifications?.filter(c => c.code !== this._JEL)
            .map(c => (isNonEmptyString(c.item) ? c.item + SOLIDIFY_CONSTANTS.SPACE : SOLIDIFY_CONSTANTS.STRING_EMPTY) + (isNonEmptyString(c.code) ? "(" + c.code + ")" : SOLIDIFY_CONSTANTS.STRING_EMPTY)),
          type: TypeInfoEnum.list,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathClassifications,
        }),
      ],
      type: TypeInfoEnum.listInfo,
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
      fieldPath: DepositFormRuleHelper.pathClassifications,
    }];
  }

  private _getSectionFundings(fundings: DepositFormFunding[]): Info[] {
    const listInfos: Info[] = [];
    if (isNullOrUndefined(fundings)) {
      return listInfos;
    }
    fundings.forEach(f => {
      listInfos.push({
        label: LabelTranslateEnum.financing,
        valuesInfo: [
          ...this._getSimpleValue(DepositFormRuleHelper.pathFunderFunder, f?.funder, Enums.Deposit.StepEnum.DESCRIPTION),
          ...(f.funder === environment.fundingSnsfName && isNotNullNorUndefinedNorWhiteString(f.code) ?
              this._getInfoIfValueDefined({
                label: this._getLabel(DepositFormRuleHelper.pathFunderProjectName),
                link: {
                  text: f.name,
                  url: environment.snsfWebsiteId + f.code,
                },
                shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
                fieldPath: DepositFormRuleHelper.pathFunder,
                type: TypeInfoEnum.link,
              }) :
              this._getSimpleValue(DepositFormRuleHelper.pathFunderProjectName, f?.name, Enums.Deposit.StepEnum.DESCRIPTION)
          ),
          ...this._getSimpleValue(DepositFormRuleHelper.pathFunderProjectCode, f?.code as any, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathFunderProjectAcronym, f?.acronym, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathFunderProjectJurisdiction, f?.jurisdiction, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathFunderProjectProgram, f?.program, Enums.Deposit.StepEnum.DESCRIPTION),
        ],
        type: TypeInfoEnum.listInfo,
        shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
        fieldPath: DepositFormRuleHelper.pathFunder,
        needCheck: this.isValidationMode && isNullOrUndefinedOrWhiteString(f.jurisdiction),
      });
    });
    return listInfos;
  }

  private _getSectionLinks(): Info[] {
    const description = this.depositFormModel?.description;
    return this._getInfoIfValueDefined({
      label: LabelTranslateEnum.links,
      valuesInfo: [
        ...this._getInfoIfValueDefined({
          label: this._getLabel(DepositFormRuleHelper.pathCommercialUrl),
          link: {
            text: description?.publisherVersionUrl,
            url: this._addHttpSuffixIfMissing(description?.publisherVersionUrl),
          },
          type: TypeInfoEnum.link,
          shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
          fieldPath: DepositFormRuleHelper.pathPublisher,
        }),
        ...this._getDatasetsUrl(),
        ...this._getOtherLinks(),
        ...this._getSimpleValue(DepositFormRuleHelper.pathAward, description?.award, Enums.Deposit.StepEnum.DESCRIPTION),
        ...this._getSimpleValue(DepositFormRuleHelper.pathCollection, description?.aouCollection, Enums.Deposit.StepEnum.DESCRIPTION),
      ],
      type: TypeInfoEnum.listInfo,
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
      fieldPath: DepositFormRuleHelper.pathLinks,
    });
  }

  private _getSectionCorrections(corrections: DepositFormCorrection[]): Info[] {
    const listInfos: Info[] = [];
    if (isNullOrUndefined(corrections)) {
      return listInfos;
    }
    corrections.forEach(f => {
      listInfos.push({
        label: this._getLabel(DepositFormRuleHelper.pathCorrections),
        valuesInfo: [
          ...this._getSimpleValue(DepositFormRuleHelper.pathCorrectionsNote, f?.note, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathCorrectionsDoi, f?.doi, Enums.Deposit.StepEnum.DESCRIPTION),
          ...this._getSimpleValue(DepositFormRuleHelper.pathCorrectionsPmid, f?.pmid, Enums.Deposit.StepEnum.DESCRIPTION),
        ],
        type: TypeInfoEnum.listInfo,
        shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
        fieldPath: DepositFormRuleHelper.pathCorrections,
      });
    });
    return listInfos;
  }

  private _getGroups(): Info[] {
    const inactiveLabel = this._translate.instant(LabelTranslateEnum.inactive);
    const listInfos = [];
    if (isNullOrUndefined(this.depositFormModel?.contributors?.groups)) {
      return listInfos;
    }
    this.depositFormModel?.contributors?.groups.forEach((resId, index) => {
      listInfos.push({
        id: this._RESEARCH_GROUP_ID,
        label: index === 0 ? this._getLabel(DepositFormRuleHelper.pathGroup) : SOLIDIFY_CONSTANTS.STRING_EMPTY,
        type: TypeInfoEnum.translate,
        value: resId,
        state: SharedResearchGroupState,
        action: sharedResearchGroupActionNameSpace,
        labelCallback: (researchGroup: ResearchGroup) => {
          let result = (isFalse(researchGroup.active) ? `[${inactiveLabel}] ` : SOLIDIFY_CONSTANTS.STRING_EMPTY);
          result = result + researchGroup.name;
          if (isNonWhiteString(researchGroup.code)) {
            result = result + ` (${researchGroup.code})`;
          }
          if (isFalse(researchGroup.active)) {
            result = `<span class="${this.needToCheckResearchGroup(researchGroup) ? "need-check " : SOLIDIFY_CONSTANTS.STRING_EMPTY} inactive">${result}</span>`;
          }
          return result;
        },
        callbackOnRetrieveTranslateBack: (elementRef: ElementRef, resource: ResearchGroup) => {
          if (isNullOrUndefined(this.isValidationMode) || isFalse(this.isValidationMode)) {
            return;
          }
          if (isNullOrUndefined(resource.validated) || isFalse(resource.validated)) {
            this._renderer.addClass(elementRef.nativeElement, "need-check");
          } else {
            this._renderer.removeClass(elementRef.nativeElement, "need-check");
          }
        },
        callbackOnClickTranslateBack: (resource: ResearchGroup) => {
          if (isNullOrUndefined(this.isValidationMode) || isFalse(this.isValidationMode)) {
            return false;
          }
          if (isTrue(resource.validated)) {
            return false;
          }
          if (this._securityService.isRootOrAdmin()) {
            this.subscribe(DialogUtil.open(this._dialog, DepositValidateResearchGroupDialog, {
              researchGroup: resource,
            }, {
              width: "900px",
            }), researchGroup => {
              this._refreshValidationErrorBS.next();
              this._refreshResearchGroup();
            });
          } else {
            DialogUtil.open(this._dialog, ConfirmDialog, {
              titleToTranslate: MARK_AS_TRANSLATABLE("deposit.modal.contactAdministratorToConfirmResearchGroup.title"),
              messageToTranslate: MARK_AS_TRANSLATABLE("deposit.modal.contactAdministratorToConfirmResearchGroup.message"),
              paramTitle: {
                name: resource.name,
              },
              cancelButtonToTranslate: LabelTranslateEnum.cancel,
              colorCancel: ButtonColorEnum.primary,
            });
          }
        },
        shortcutTo: Enums.Deposit.StepEnum.CONTRIBUTORS,
        fieldPath: DepositFormRuleHelper.pathGroup,
      } as Info);
    });
    return listInfos;
  }

  private _getStructures(): Info[] {
    const listInfos = [];
    if (isNullOrUndefined(this.depositFormModel?.contributors?.academicStructures)) {
      return listInfos;
    }
    this.depositFormModel?.contributors?.academicStructures.forEach((resId, index) => {
      listInfos.push({
        label: index === 0 ? this._getLabel(DepositFormRuleHelper.pathStructure) : SOLIDIFY_CONSTANTS.STRING_EMPTY,
        type: TypeInfoEnum.translate,
        value: resId,
        state: SharedStructureState,
        action: sharedStructureActionNameSpace,
        labelKey: "name",
        shortcutTo: Enums.Deposit.StepEnum.CONTRIBUTORS,
        fieldPath: DepositFormRuleHelper.pathStructure,
      } as Info);
    });
    return listInfos;
  }

  private _getPages(): Info[] {
    const pages = this.depositFormModel?.description?.pages;
    return [
      ...this._getSimpleValue(DepositFormRuleHelper.pathPagesPaging, pages?.paging, Enums.Deposit.StepEnum.DESCRIPTION),
      ...this._getSimpleValue(DepositFormRuleHelper.pathPagesNumberPagesArticleNumber, pages?.other, Enums.Deposit.StepEnum.DESCRIPTION),
    ];
  }

  private _getDatasetsUrl(): Info[] {
    return this._getInfoIfValueDefined({
      label: this._getLabel(DepositFormRuleHelper.pathDatasetsUrl),
      // tslint:disable-next-line:arrow-return-shorthand
      links: this.depositFormModel?.description?.datasets?.map(datasetsUrl => ({
        url: this._addHttpSuffixIfMissing(datasetsUrl?.url),
        text: datasetsUrl?.url,
      } as DepositSummaryLinkInfo)),
      type: TypeInfoEnum.listLink,
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
      fieldPath: DepositFormRuleHelper.pathDatasets,
    });
  }

  private _getOtherLinks(): Info[] {
    return this._getInfoIfValueDefined({
      label: this._getLabel(DepositFormRuleHelper.pathLinks),
      // tslint:disable-next-line:arrow-return-shorthand
      links: this.depositFormModel?.description?.links?.map(linkGroup => ({
        url: this._addHttpSuffixIfMissing(linkGroup?.target),
        text: linkGroup?.description?.text,
        lang: linkGroup?.description?.lang,
      } as DepositSummaryLinkInfo)),
      type: TypeInfoEnum.listLink,
      shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
      fieldPath: DepositFormRuleHelper.pathLinks,
    });
  }

  private _getSimpleValue(fieldPath: string, value: string | Date, shortcut?: Enums.Deposit.StepEnum): Info[] {
    return this._getInfoIfValueDefined({
      label: this._getLabel(fieldPath),
      value: value,
      shortcutTo: shortcut,
      fieldPath: fieldPath,
    });
  }

  private _getInfoIfValueDefined(info: Info): Info[] {
    const type = info?.type;
    if ((type === TypeInfoEnum.value || isNullOrUndefined(type)) && (isNotNullNorUndefinedNorWhiteString(info.value) || isNumber(info.value) || isDate(info.value))) {
      return [info];
    } else if (type === TypeInfoEnum.translate && isNotNullNorUndefinedNorWhiteString(info.value)) {
      return [info];
    } else if (type === TypeInfoEnum.list && isNonEmptyArray(info.values)) {
      return [info];
    } else if (type === TypeInfoEnum.listPerson && isNonEmptyArray(info.valuesPerson)) {
      return [info];
    } else if (type === TypeInfoEnum.listLanguage && isNonEmptyArray(info.valuesLanguage)) {
      return [info];
    } else if (type === TypeInfoEnum.listInfo && isNonEmptyArray(info.valuesInfo)) {
      return [info];
    } else if (type === TypeInfoEnum.link && isNotNullNorUndefined(info.link) && isNotNullNorUndefined(info.link.url) && isNotNullNorUndefined(info.link.text)) {
      return [info];
    } else if (type === TypeInfoEnum.listLink && isNonEmptyArray(info.links)) {
      return [info];
    }
    return [];
  }

  private _getAllContributors(contributors: DepositFormAbstractContributor[]): DepositSummaryValuePerson[] {
    return this._extractPersonOrCollaboration(contributors,
      true,
      Enums.Deposit.RoleContributorEnum.author,
      Enums.Deposit.RoleContributorEnum.director,
      Enums.Deposit.RoleContributorEnum.editor,
      Enums.Deposit.RoleContributorEnum.guest_editor,
      Enums.Deposit.RoleContributorEnum.photographer,
      Enums.Deposit.RoleContributorEnum.translator,
    );
  }

  private _getCollaborationMembers(contributors: DepositFormAbstractContributor[]): DepositSummaryValuePerson[] {
    return this._extractPersonOrCollaboration(contributors, false, Enums.Deposit.RoleContributorEnum.collaborator);
  }

  private _extractPersonOrCollaboration(contributors: DepositFormAbstractContributor[], includeCollaboration: boolean, ...role: Enums.Deposit.RoleContributorEnum[]): DepositSummaryValuePerson[] | undefined {
    contributors = contributors?.filter(c => (includeCollaboration && isNotNullNorUndefined(c.collaboration)) || role.indexOf(c.contributor?.role) !== -1);
    const contributorRoles = MemoizedUtil.selectSnapshot(this._store, AppContributorRolesState, state => state.map);
    if (isNullOrUndefined(contributors) || isEmptyArray(contributors)) {
      return undefined;
    }
    const listPerson = [];
    contributors.forEach(d => {
      let person: DepositSummaryValuePerson;
      if (isNotNullNorUndefined(d.collaboration)) {
        person = {
          lastname: d.collaboration.name,
        };
      } else {
        const contributorRole = MappingObjectUtil.get(contributorRoles, this.depositFormModel.type.subtype).find(e => e.value === d.contributor.role);
        person = {
          firstname: d.contributor.firstname,
          lastname: d.contributor.lastname,
          isUnige: isNonEmptyString(d.contributor.cnIndividu),
          roleToTranslate: contributorRole,
          displayRole: d.contributor.role !== Enums.Deposit.RoleContributorEnum.author,
        };
      }
      listPerson.push(person);
    });
    return listPerson;
  }

  private _getDates(dates: DepositFormDate[]): Info[] {
    const listInfos: Info[] = [];
    if (isNullOrUndefined(dates)) {
      return listInfos;
    }
    dates.forEach(d => {
      const fieldPath = this._getDatePathFromType(d.type);
      listInfos.push({
        label: this._getLabel(fieldPath),
        value: d.date,
        type: TypeInfoEnum.value,
        shortcutTo: Enums.Deposit.StepEnum.DESCRIPTION,
        fieldPath: fieldPath,
      });
    });
    return listInfos;
  }

  private _getDatePathFromType(type: Enums.Deposit.DateTypesEnum): string {
    switch (type) {
      case Enums.Deposit.DateTypesEnum.PUBLICATION:
        return DepositFormRuleHelper.pathDatesPublication;
      case Enums.Deposit.DateTypesEnum.FIRST_ONLINE:
        return DepositFormRuleHelper.pathDatesFirstOnline;
      case Enums.Deposit.DateTypesEnum.IMPRIMATUR:
        return DepositFormRuleHelper.pathDatesImprimatur;
      case Enums.Deposit.DateTypesEnum.DEFENSE:
        return DepositFormRuleHelper.pathDatesDefense;
    }
    return DepositFormRuleHelper.pathDates;
  }

  private _addHttpSuffixIfMissing(url: string): string | undefined {
    if (isNullOrUndefined(url) || isEmptyString(url)) {
      return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
    return url.startsWith("https://") || url.startsWith("http://") ? url : "http://" + url;
  }

  goToOrcid(orcid: string): void {
    this._orcidService.openOrcidPage(orcid);
  }

  navigateToStep(step: Enums.Deposit.StepEnum): void {
    this._navigateToStepBS.next(step);
  }

  formatLanguages(languages: DepositFormLanguage[]): string {
    return languages.map(l => l.language).join(" | ");
  }

  needToCheckResearchGroup(researchGroup: ResearchGroup): boolean {
    return this.isValidationMode && isFalse(researchGroup.active);
  }

  needToCheckKeyword(keyword: string): boolean {
    return this.isValidationMode
      && (keyword.length > this._KEYWORD_LENGTH_TO_CHECK
        || keyword[0] !== keyword[0].toUpperCase()
        || keyword[keyword.length - 1] === SOLIDIFY_CONSTANTS.DOT
        || keyword.indexOf("*") !== -1);
  }

  needToCheckTitle(text: string | undefined): boolean {
    return this.isValidationMode && isNotNullNorUndefinedNorWhiteString(text) && text === text.toUpperCase();
  }

  redirectToShortcut(info: Info): void {
    if (this._shouldPreventShortcutToEnterEditView(info.shortcutTo)) {
      return;
    }
    this._navigateToShortcut(info.shortcutTo, info.fieldPath);
  }

  redirectToShortcutStep(step: Enums.Deposit.StepEnum, fieldPath?: string, mouseEvent?: MouseEvent): void {
    if (this._shouldPreventShortcutToEnterEditView(step)) {
      return;
    }
    if (isNotNullNorUndefined(mouseEvent) && mouseEvent.target["tagName"]?.toUpperCase() === "A") {
      return;
    }
    this._navigateToShortcut(step, fieldPath);
  }

  private _navigateToShortcut(step: Enums.Deposit.StepEnum, fieldPath?: string): void {
    if (isNullOrUndefinedOrWhiteString(fieldPath)) {
      this._navigateToStepBS.next(step);
      return;
    }
    const navigation = DepositHelper.getNavigateToFieldPath(fieldPath, this.isValidationMode, this.depositId);
    if (isNullOrUndefined(navigation)) {
      this._navigateToStepBS.next(step);
      return;
    }
    this._navigateBS.next(navigation);
  }

  private _shouldPreventShortcutToEnterEditView(step: Enums.Deposit.StepEnum): boolean {
    return isFalse(this.isValidationMode) || isNullOrUndefined(this.isValidationMode) || isNullOrUndefined(step) || !DepositHelper.isEditAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), this.depositStatus);
  }

  shortcutIsClickable(info?: Info): boolean {
    return isTrue(this.isValidationMode) && (isNullOrUndefined(info) || isNotNullNorUndefinedNorWhiteString(info.shortcutTo)) && DepositHelper.isEditAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), this.depositStatus);
  }

  private _computeMailToCreatorUrl(): void {
    this.mailToCreatorUrl = this._getEmailUrl(this.creator?.email, MARK_AS_TRANSLATABLE("deposit.mailToCreator.subject"), MARK_AS_TRANSLATABLE("deposit.mailToCreator.body"));
  }

  private _computeMailToLastEditorUrl(): void {
    this.mailToLastEditorUrl = this._getEmailUrl(this.lastEditor?.email, MARK_AS_TRANSLATABLE("deposit.mailToLastEditor.subject"), MARK_AS_TRANSLATABLE("deposit.mailToLastEditor.body"));
  }

  private _getEmailUrl(email: string, i18nSubject: string, i18nBody: string): string | undefined {
    if (isNullOrUndefined(email) || isNullOrUndefined(this.depositFormModel) || isNullOrUndefined(this.depositId)) {
      return undefined;
    }
    const title = this.depositFormModel.type?.title?.text;
    const shortTitle = title.length > this._SHORT_TITLE_MAX_SIZE ? title.substring(0, this._SHORT_TITLE_MAX_SIZE) + "..." : title;
    const url = SsrUtil.window?.location.origin + SOLIDIFY_CONSTANTS.URL_SEPARATOR + SOLIDIFY_CONSTANTS.HASHTAG + SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.depositDetail + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.depositId;
    const subject = this._translate.instant(i18nSubject, {shortTitle: shortTitle});
    const body = this._translate.instant(i18nBody, {title: title, url: url});

    return `${this._MAIL_TO}${email}?${this._MAIL_SUBJECT}=${subject}&${this._MAIL_BODY}=${body}`;
  }
}

interface Info {
  id?: string;
  label: string;
  translateLabel?: boolean;
  value?: string | Date;
  values?: string[];
  valuesLanguage?: DepositSummaryValueLanguage[];
  valuesPerson?: DepositSummaryValuePerson[];
  valuesInfo?: Info[];
  type?: TypeInfoEnum;
  link?: DepositSummaryLinkInfo;
  links?: DepositSummaryLinkInfo[];
  labelListEnumTranslation?: KeyValue[];
  lang?: string;
  state?: Type<ResourceState<any, any>>;
  action?: ResourceNameSpace;
  labelKey?: string;
  labelCallback?: (resource: any) => string;
  callbackOnRetrieveTranslateBack?: (elementRef: ElementRef, resource: any) => void;
  callbackOnClickTranslateBack?: (resource: any) => boolean;
  isBackTranslate?: boolean;
  isTextarea?: boolean;
  isListInline?: boolean;
  needCheck?: boolean;
  needCheckByElement?: (element: any) => boolean;
  wrap?: boolean;
  shortcutTo?: Enums.Deposit.StepEnum;
  fieldPath?: string;
}

enum TypeInfoEnum {
  value = 0,
  translate = 1,
  list,
  listPerson,
  listLanguage,
  listInfo,
  link,
  listLink,
}

export interface DepositSummaryLinkInfo {
  text: string;
  url: string;
  lang?: string;
}

export interface DepositSummaryValueLanguage {
  text: string;
  lang: string;
}

export interface DepositSummaryValuePerson {
  firstname?: string;
  lastname?: string;
  roleToTranslate?: PublicationSubtypeContributorRoleDTO;
  displayRole?: boolean;
  isUnige?: boolean;
}
