/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-text-language-list.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  Output,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormListPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form-list/deposit-abstract-form-list.presentational";
import {
  DepositFormTextLanguage,
  FormComponentFormDefinitionDepositFormTextLanguage,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {Language} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  BaseFormDefinition,
  isNonEmptyString,
  isNotNullNorUndefined,
  NotificationService,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-text-language-list",
  templateUrl: "./deposit-form-description-text-language-list.presentational.html",
  styleUrls: ["./deposit-form-description-text-language-list.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionTextLanguageListPresentational,
    },
  ],
})
export class DepositFormDescriptionTextLanguageListPresentational extends DepositAbstractFormListPresentational implements ControlValueAccessor {
  formDefinition: FormComponentFormDefinitionDepositFormTextLanguage = new FormComponentFormDefinitionDepositFormTextLanguage();

  listFormDefinitions: BaseFormDefinition[] = [this.formDefinition];

  @Input()
  path: string;

  @Input()
  listLanguages: Language[];

  @Input()
  labelTextToTranslate: string;

  @Input()
  isTextarea: boolean = false;

  @Input()
  textareaMaxSize: number | undefined = undefined;

  @Input()
  isRichText: boolean = false;

  @Input()
  isLinkAllowed: boolean = false;

  @Input()
  isBoldAllowed: boolean = false;

  @Input()
  tooltipText: string | undefined;

  @Input()
  hintTextToTranslate: string | undefined;

  @Input()
  tooltipLanguage: string | undefined;

  @Input()
  withoutPanel: boolean = false;

  @Input()
  withWordCounter: boolean = false;

  @Input()
  withFixButton: boolean = false;

  @Input()
  isValidationMode: boolean = false;

  private readonly _fixBS: BehaviorSubject<FormGroup | undefined> = new BehaviorSubject<FormGroup | undefined>(undefined);
  @Output("fixChange")
  readonly fixObs: Observable<FormGroup | undefined> = ObservableUtil.asObservable(this._fixBS);

  getPath(fieldName: string): string {
    return this.path + (isNonEmptyString(fieldName) ? (SOLIDIFY_CONSTANTS.DOT + fieldName) : SOLIDIFY_CONSTANTS.STRING_EMPTY);
  }

  constructor(protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _notificationService: NotificationService) {
    super(_translate,
      _store,
      _fb,
      _changeDetector,
      _actions$,
      _notificationService);
  }

  static createTextLanguage(_fb: FormBuilder,
                            formDefinition: FormComponentFormDefinitionDepositFormTextLanguage,
                            link: DepositFormTextLanguage | undefined = undefined): FormGroup {
    const form = _fb.group({
      [formDefinition.text]: ["", [SolidifyValidator]],
      [formDefinition.lang]: ["", [SolidifyValidator]],
    });

    if (isNotNullNorUndefined(link)) {
      form.setValue(link as any);
    }

    return form;
  }

  formGroupFactory(fb: FormBuilder, formDefinition: FormComponentFormDefinitionDepositFormTextLanguage): FormGroup {
    return DepositFormDescriptionTextLanguageListPresentational.createTextLanguage(fb, formDefinition);
  }

  fix(formGroup: FormGroup): void {
    this._fixBS.next(formGroup);
  }
}
