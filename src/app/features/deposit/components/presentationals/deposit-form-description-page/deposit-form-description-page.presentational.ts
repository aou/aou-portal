/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-page.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {FormComponentFormDefinitionDepositFormPage} from "@app/features/deposit/models/deposit-form-definition.model";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  isNullOrUndefinedOrWhiteString,
  isNumberReal,
  NotificationService,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-page",
  templateUrl: "./deposit-form-description-page.presentational.html",
  styleUrls: ["./deposit-form-description-page.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionPagePresentational,
    },
  ],
})
export class DepositFormDescriptionPagePresentational extends DepositAbstractFormPresentational {
  formDefinition: FormComponentFormDefinitionDepositFormPage = new FormComponentFormDefinitionDepositFormPage();

  @Input()
  isValidationMode: boolean;

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService) {
    super(_changeDetector);
  }

  completePageNumber(formControl: AbstractControl): void {
    let paging: string = formControl?.value;
    if (isNullOrUndefinedOrWhiteString(paging)) {
      return;
    }
    paging = paging.trim();
    let listPaging = paging.split("-").map(p => p.trim());
    if (listPaging.length === 2) {
      const numberStartString = listPaging[0];
      let numberEndString = listPaging[1];
      const numberStart = +numberStartString;
      const numberEnd = +numberEndString;

      if (isNumberReal(numberStart) && isNumberReal(numberEnd) && numberStart > numberEnd && numberStartString.length > numberEndString.length) {
        const indexSeparator = numberStartString.length - numberEndString.length;
        const numberStartFirstPart = +numberStartString.substring(0, indexSeparator);
        const numberStartSecondPart = +numberStartString.substring(indexSeparator);
        if (numberStartSecondPart < numberEnd) {
          numberEndString = numberStartFirstPart + numberEndString;
          listPaging = [numberStartString, numberEndString];
        }
      }
    }
    paging = listPaging.join("-");
    formControl.setValue(paging);
  }
}
