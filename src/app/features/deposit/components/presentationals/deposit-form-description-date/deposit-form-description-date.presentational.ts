/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-form-description-date.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {DepositAbstractFormPresentational} from "@app/features/deposit/components/presentationals/deposit-abstract-form/deposit-abstract-form.presentational";
import {
  DepositFormDate,
  FormComponentFormDefinitionDepositFormDates,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {Enums} from "@enums";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  ObservableUtil,
  SolidifyValidator,
  ValidationErrorHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-form-description-date",
  templateUrl: "./deposit-form-description-date.presentational.html",
  styleUrls: ["./deposit-form-description-date.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositFormDescriptionDatePresentational,
    },
  ],
})
export class DepositFormDescriptionDatePresentational extends DepositAbstractFormPresentational {
  formDefinition: FormComponentFormDefinitionDepositFormDates = new FormComponentFormDefinitionDepositFormDates();

  readonly PANEL_LABEL_SUFFIX = MARK_AS_TRANSLATABLE("deposit.dates.panel.suffix");
  readonly PANEL_LABEL_SUFFIX_FIRST_ONLINE_PRIVILEGED = MARK_AS_TRANSLATABLE("deposit.dates.panel.suffixFirstOnlinePrivileged");

  private readonly _datesChangeBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("datesChange")
  readonly dateChangeObs: Observable<string | undefined> = ObservableUtil.asObservable(this._datesChangeBS);

  listDatePath: string[] = [
    DepositFormRuleHelper.pathDatesFirstOnline,
    DepositFormRuleHelper.pathDatesPublication,
    DepositFormRuleHelper.pathDatesDefense,
    DepositFormRuleHelper.pathDatesImprimatur,
  ];

  externalDetectChanges(): void {
    this._changeDetector.detectChanges();
  }

  get isMoreThanOneDate(): boolean {
    let datesToDisplay = 0;
    return this.listDatePath.some(path => {
      if (DepositFormRuleHelper.shouldDisplayField(this.mainFormGroup, path)) {
        datesToDisplay++;
      }
      if (datesToDisplay >= 2) {
        return true;
      }
    });
  }

  get isFirstOnlinePresent(): boolean {
    return DepositFormRuleHelper.shouldDisplayField(this.mainFormGroup, DepositFormRuleHelper.pathDatesFirstOnline);
  }

  @HostBinding("class.is-invalid")
  get isInvalid(): boolean {
    return this.formGroup?.invalid;
  }

  constructor(protected readonly _changeDetector: ChangeDetectorRef) {
    super(_changeDetector);
  }

  removeErrorInSecondDateIfNecessary(formControl: FormControl): void {
    if (!this.formGroup.invalid) {
      return;
    }
    const value = formControl.value;
    if (isNonEmptyString(value)) {
      MappingObjectUtil.forEach(this.formGroup.controls, (fc, key) => {
        if (fc.valid) {
          return;
        }
        if (isNotNullNorUndefined(fc.errors)) {
          ValidationErrorHelper.removeAllErrors(fc);
          fc.updateValueAndValidity();
        }
      });
    }
  }

  verifyDate(date: string): void {
    this._datesChangeBS.next(date);
  }

  static createOrBindDate(_fb: FormBuilder,
                          formDefinition: FormComponentFormDefinitionDepositFormDates,
                          form: FormGroup | undefined = undefined,
                          publicationDates: DepositFormDate[] | undefined = undefined): FormGroup {
    if (isNullOrUndefined(form)) {
      form = _fb.group({
        [formDefinition.first_online]: [undefined, [SolidifyValidator]],
        [formDefinition.publication]: [undefined, [SolidifyValidator]],
        [formDefinition.defense]: [undefined, [SolidifyValidator]],
        [formDefinition.imprimatur]: [undefined, [SolidifyValidator]],
      });
    }

    if (isNotNullNorUndefined(publicationDates)) {
      form.get(formDefinition.first_online)?.setValue(publicationDates?.find(d => d.type === Enums.Deposit.DateTypesEnum.FIRST_ONLINE)?.date);
      form.get(formDefinition.publication)?.setValue(publicationDates?.find(d => d.type === Enums.Deposit.DateTypesEnum.PUBLICATION)?.date);
      form.get(formDefinition.defense)?.setValue(publicationDates?.find(d => d.type === Enums.Deposit.DateTypesEnum.DEFENSE)?.date);
      form.get(formDefinition.imprimatur)?.setValue(publicationDates?.find(d => d.type === Enums.Deposit.DateTypesEnum.IMPRIMATUR)?.date);
    }

    return form;
  }
}
