/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-comment.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositCommentValidatorAction} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.action";
import {DepositCommentValidatorState} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.state";
import {DepositCommentAction} from "@app/features/deposit/stores/comment/deposit-comment.action";
import {DepositCommentState} from "@app/features/deposit/stores/comment/deposit-comment.state";
import {environment} from "@environments/environment";
import {
  Comment,
  Deposit,
} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {Observable} from "rxjs";
import {
  BreakpointService,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-comment-dialog",
  templateUrl: "./deposit-comment.dialog.html",
  styleUrls: ["./deposit-comment.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCommentDialog extends SharedAbstractDialog<DepositCommentDialogData> implements OnInit {
  listCommentsObs: Observable<Comment[]>;
  isLoadingObs: Observable<boolean>;

  constructor(protected readonly _dialogRef: MatDialogRef<DepositCommentDialog>,
              protected readonly _fb: FormBuilder,
              private readonly _store: Store,
              public readonly breakpointService: BreakpointService,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositCommentDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
      field: "creation.when",
      order: OrderEnum.descending,
    });
    if (this.data.isValidatorComments) {
      this._store.dispatch(new DepositCommentValidatorAction.GetAll(this.data.deposit.resId, queryParameters));
      this.listCommentsObs = MemoizedUtil.list(this._store, DepositCommentValidatorState);
      this.isLoadingObs = MemoizedUtil.isLoading(this._store, DepositCommentValidatorState);
    } else {
      this._store.dispatch(new DepositCommentAction.GetAll(this.data.deposit.resId, queryParameters));
      this.listCommentsObs = MemoizedUtil.list(this._store, DepositCommentState);
      this.isLoadingObs = MemoizedUtil.isLoading(this._store, DepositCommentState);
    }
  }
}

export interface DepositCommentDialogData {
  deposit: Deposit;
  isValidatorComments: boolean;
}
