/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-bulk-language.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {SharedLanguageAction} from "@app/shared/stores/language/shared-language.action";
import {environment} from "@environments/environment";
import {Language} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {Observable} from "rxjs";
import {
  BackTranslatePipe,
  BaseFormDefinition,
  FormValidationHelper,
  MemoizedUtil,
  OrderEnum,
  PropertyName,
  QueryParameters,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-bulk-language-dialog",
  templateUrl: "./deposit-bulk-language.dialog.html",
  styleUrls: ["./deposit-bulk-language.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositBulkLanguageDialog extends SharedAbstractDialog<DepositBulkLanguageDialogData, string> implements OnInit {
  backTranslatePipe: BackTranslatePipe;

  listLanguageObs: Observable<Language[]> = MemoizedUtil.list(this._store, SharedLanguageState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(protected readonly _dialogRef: MatDialogRef<DepositBulkLanguageDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositBulkLanguageDialogData,
              protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _fb: FormBuilder) {
    super(_dialogRef, data);
    this.backTranslatePipe = new BackTranslatePipe(this._store, environment);
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedLanguageAction.GetAll(new QueryParameters(environment.defaultEnumValuePageSizeOption, {
      field: "sortValue",
      order: OrderEnum.ascending,
    })));
    this.form = this._fb.group({
      [this.formDefinition.languageId]: [undefined, [SolidifyValidator, Validators.required]],
    });
  }

  confirm(): void {
    const languageId = this.form.get(this.formDefinition.languageId).value;
    this.submit(languageId);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }
}

export interface DepositBulkLanguageDialogData {
  titleToTranslate: string;
  messageToTranslate: string;
  numberDeposit: number;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() languageId: string;
}
