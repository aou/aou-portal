/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-create-research-group.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositCreateResearchGroupAlternativeDialog} from "@app/features/deposit/components/dialogs/deposit-create-research-group-alternative/deposit-create-research-group-alternative.dialog";
import {SharedResearchGroupAction} from "@app/shared/stores/research-group/shared-research-group.action";
import {environment} from "@environments/environment";
import {ResearchGroup} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {Observable} from "rxjs";
import {
  BaseFormDefinition,
  DialogUtil,
  FormValidationHelper,
  isNonEmptyArray,
  isNotNullNorUndefined,
  MappingObjectUtil,
  MemoizedUtil,
  ModelFormControlEvent,
  NotificationService,
  PropertyName,
  QueryParameters,
  QueryParametersUtil,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-create-research-group-dialog",
  templateUrl: "./deposit-create-research-group.dialog.html",
  styleUrls: ["./deposit-create-research-group.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCreateResearchGroupDialog extends SharedAbstractDialog<DepositCreateResearchGroupDialogData, ResearchGroup> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  form: FormGroup;
  isLoadingResearchGroupObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedResearchGroupState);

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<DepositCreateResearchGroupDialog>,
              private readonly _notificationService: NotificationService,
              private readonly _dialog: MatDialog,
              private readonly _actions$: Actions,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositCreateResearchGroupDialogData,
              private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef,
  ) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.code]: ["", [SolidifyValidator]],
      [this.formDefinition.acronym]: ["", [SolidifyValidator]],
    });
  }

  saveResearchGroup(): void {
    this.checkSimilarResearchGroup({
      formControl: this.form,
      changeDetectorRef: this._changeDetector,
      model: this.form.value,
    });
  }

  private checkSimilarResearchGroup($event: ModelFormControlEvent<ResearchGroup>): void {
    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    const name = (this.form.get(this.formDefinition.name).value as string)?.trim().toLowerCase();
    const acronym = (this.form.get(this.formDefinition.acronym).value as string)?.trim().toLowerCase();
    const code = (this.form.get(this.formDefinition.code).value as string)?.trim().toLowerCase();
    MappingObjectUtil.set(searchItems, SharedResearchGroupState.FIELD_SEARCH_NAME, name);
    MappingObjectUtil.set(searchItems, SharedResearchGroupState.FIELD_SEARCH_ACRONYM, acronym);
    MappingObjectUtil.set(searchItems, SharedResearchGroupState.FIELD_SEARCH_CODE, code);
    MappingObjectUtil.set(searchItems, SharedResearchGroupState.FIELD_SEARCH_BY_NAME_STRICT, SOLIDIFY_CONSTANTS.STRING_TRUE);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new SharedResearchGroupAction.GetAll(queryParameters), SharedResearchGroupAction.GetAllSuccess,
      result => {
        const listResearchGroupMatching = result.list._data;
        const alreadyExist = listResearchGroupMatching.find(r => r.name.toLowerCase() === name);
        if (isNotNullNorUndefined(alreadyExist)) {
          this.submit(alreadyExist);
          return;
        }

        if (isNonEmptyArray(listResearchGroupMatching)) {
          this.openAlternativeDialog($event, listResearchGroupMatching);
        } else {
          this.createResearchGroup($event);
        }
      }),
    );
  }

  private createResearchGroup($event: ModelFormControlEvent<ResearchGroup>): void {
    // TODO SET STATUS TO CHECK ON RESEARCH GROUP !
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedResearchGroupAction.Create($event),
      SharedResearchGroupAction.CreateSuccess,
      resultAction => {
        this.submit(resultAction.model);
      }));
  }

  private openAlternativeDialog(newResearchGroup: ModelFormControlEvent<ResearchGroup>, listMatchingResearchGroup: ResearchGroup[]): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositCreateResearchGroupAlternativeDialog, {
        listExistingResearchGroup: listMatchingResearchGroup,
        newResearchGroup: newResearchGroup.model,
      }, undefined,
      depositResearchGroupSelected => {
        if (depositResearchGroupSelected.isNew) {
          this.createResearchGroup(newResearchGroup);
        } else {
          this.submit(depositResearchGroupSelected.selected);
        }
      }));
  }
}

export interface DepositCreateResearchGroupDialogData {
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() code: string;
  @PropertyName() acronym: string;
}
