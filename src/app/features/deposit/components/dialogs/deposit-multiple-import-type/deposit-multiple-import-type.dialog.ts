/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-multiple-import-type.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {ImportMethodEnum} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {KeyValue} from "solidify-frontend";

@Component({
  selector: "aou-deposit-multiple-import-type-dialog",
  templateUrl: "./deposit-multiple-import-type.dialog.html",
  styleUrls: ["./deposit-multiple-import-type.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositMultipleImportTypeDialog extends SharedAbstractDialog<DepositMultipleImportTypeDialogData, ImportMethodEnum> implements OnInit {
  formControl: FormControl = new FormControl(undefined, [Validators.required]);

  listImportMethods: KeyValue[] = [
    {
      key: ImportMethodEnum.doi,
      value: "DOI",
    },
    {
      key: ImportMethodEnum.pubmed,
      value: "PMID",
    },
  ];

  constructor(protected readonly _dialogRef: MatDialogRef<DepositMultipleImportTypeDialog>,
              protected readonly _fb: FormBuilder,
              private readonly _store: Store,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositMultipleImportTypeDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  confirm(): void {
    this.submit(this.formControl.value);
  }
}

export interface DepositMultipleImportTypeDialogData {
  listPastedIdentifier: string[];
}
