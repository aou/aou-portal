/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-create-research-group-alternative.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {ResearchGroup} from "@models";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {isNullOrUndefined} from "solidify-frontend";

@Component({
  selector: "aou-deposit-create-research-group-alternative-dialog",
  templateUrl: "./deposit-create-research-group-alternative.dialog.html",
  styleUrls: ["./deposit-create-research-group-alternative.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCreateResearchGroupAlternativeDialog extends SharedAbstractDialog<DepositCreateResearchGroupAlternativeDialogData, AlternativeDepositResearchGroupSelected> {
  selectedDepositResearchGroup: AlternativeDepositResearchGroupSelected;

  typeIntoResearchGroup(value: any): ResearchGroup {
    return value;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<DepositCreateResearchGroupAlternativeDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositCreateResearchGroupAlternativeDialogData) {
    super(_dialogRef, data);
    this.select(this.data.newResearchGroup, true);
  }

  select(selected: ResearchGroup, isNew: boolean): void {
    this.selectedDepositResearchGroup = {
      selected: selected,
      isNew: isNew,
    };
  }

  isSelected(current: ResearchGroup): boolean {
    if (isNullOrUndefined(this.selectedDepositResearchGroup)) {
      return false;
    }
    return current === this.selectedDepositResearchGroup.selected;
  }
}

export interface DepositCreateResearchGroupAlternativeDialogData {
  newResearchGroup: ResearchGroup;
  listExistingResearchGroup: ResearchGroup[];
}

export interface AlternativeDepositResearchGroupSelected {
  selected: ResearchGroup;
  isNew: boolean;
}
