/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-validate-research-group.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositResearchGroupAction} from "@app/features/deposit/stores/research-group/deposit-research-group.action";
import {ResearchGroup} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {Observable} from "rxjs";
import {
  BaseFormDefinition,
  FormValidationHelper,
  MemoizedUtil,
  NotificationService,
  PropertyName,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-validate-research-group-dialog",
  templateUrl: "./deposit-validate-research-group.dialog.html",
  styleUrls: ["./deposit-validate-research-group.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositValidateResearchGroupDialog extends SharedAbstractDialog<DepositValidateResearchGroupDialogData, ResearchGroup> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  form: FormGroup;
  isLoadingResearchGroupObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedResearchGroupState);

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<DepositValidateResearchGroupDialog>,
              private readonly _notificationService: NotificationService,
              private readonly _dialog: MatDialog,
              private readonly _actions$: Actions,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositValidateResearchGroupDialogData,
              private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef,
  ) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.form = this._fb.group({
      [this.formDefinition.name]: [this.data.researchGroup.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.code]: [this.data.researchGroup.code, [SolidifyValidator]],
      [this.formDefinition.acronym]: [this.data.researchGroup.acronym, [SolidifyValidator]],
    });
  }

  validateResearchGroup(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositResearchGroupAction.UpdateThenValidate({
        formControl: this.form,
        changeDetectorRef: this._changeDetector,
        model: {
          ...this.form.value,
          resId: this.data.researchGroup.resId,
        },
      }), DepositResearchGroupAction.UpdateThenValidateSuccess,
      result => {
        this.submit(result.parentAction.modelFormControlEvent.model);
      }),
    );
  }
}

export interface DepositValidateResearchGroupDialogData {
  researchGroup: ResearchGroup;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() code: string;
  @PropertyName() acronym: string;
}
