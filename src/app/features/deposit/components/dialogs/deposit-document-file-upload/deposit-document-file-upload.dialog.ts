/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-document-file-upload.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {AouFileUploadWrapper} from "@app/features/deposit/models/aou-file-upload-wrapper.model";
import {SharedDocumentFileTypeAction} from "@app/shared/stores/document-file-type/shared-document-file-type.action";
import {sharedLicenseGroupActionNameSpace} from "@app/shared/stores/license-group/shared-license-group.action";
import {SharedLicenseAction} from "@app/shared/stores/license/shared-license.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  DocumentFile,
  DocumentFileType,
  License,
  LicenseGroup,
  PublicationSubtypeDocumentFileTypeDTO,
  PublicationSubtypeDocumentFileTypeGroup,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedDocumentFileTypeState} from "@shared/stores/document-file-type/shared-document-file-type.state";
import {SharedLicenseGroupState} from "@shared/stores/license-group/shared-license-group.state";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {AouLabelUtil} from "@shared/utils/aou-label.util";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  BackTranslatePipe,
  BaseFormDefinition,
  BreakpointService,
  DateUtil,
  EnumUtil,
  FormValidationHelper,
  isEmptyString,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isNumberReal,
  KeyValue,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  PropertyName,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  Sort,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-document-file-upload-dialog",
  templateUrl: "./deposit-document-file-upload.dialog.html",
  styleUrls: ["./deposit-document-file-upload.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositDocumentFileUploadDialog extends SharedAbstractDialog<DepositDocumentFileUploadDialogData, AouFileUploadWrapper | DocumentFile> implements OnInit, AfterViewInit {
  backTranslatePipe: BackTranslatePipe;
  form: FormGroup;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  file: File = undefined;
  isInError: boolean = false;

  private readonly _GROUP_CC_PREFIX_KEY: string = "GROUP-CC";
  private readonly _AGREEMENT_VALUE: string = "Agreement";
  private readonly _IMPRIMATUR_VALUE: string = "Imprimatur";
  private readonly _PUBLICATION_MODE_VALUE: string = "Publication mode";
  private readonly _UNDEFINED_LABEL_KEY: string = "discardUndefinedLabels";

  readonly REGEX_INVALID: RegExp = new RegExp("[~!@#$%^&*`;?,\\\\]");

  readonly EMBARGO_DURATION_MIN: number = 1;
  readonly EMBARGO_DURATION_MAX: number = 1200;

  restrictionMode: RestrictedModeEnum = RestrictedModeEnum.noRestriction;
  listLicensesObs: Observable<License[]> = MemoizedUtil.list(this._store, SharedLicenseState);
  isLoadingLicensesObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedLicenseState);

  sharedLicenseGroupSort: Sort<LicenseGroup> = {
    field: SharedLicenseGroupState.FIELD_SORT_VALUE,
    order: OrderEnum.ascending,
  };
  sharedLicenseGroupActionNameSpace: ResourceNameSpace = sharedLicenseGroupActionNameSpace;
  sharedLicenseGroupState: typeof SharedLicenseGroupState = SharedLicenseGroupState;
  licenseGroupCallback: (value: LicenseGroup) => string = (value: LicenseGroup) => {
    if (isNonEmptyArray(value.labels)) {
      return AouLabelUtil.getTranslationFromListLabelsWithStore(this._store, value.labels);
    }
    return value[SharedLicenseGroupState.FIELD_SEARCH_TITLE] + "";
    // tslint:disable-next-line:semicolon
  };

  sharedDocumentFileTypeSort: Sort<DocumentFileType> = {
    field: "value",
    order: OrderEnum.ascending,
  };
  sharedDocumentFileTypeState: typeof SharedDocumentFileTypeState = SharedDocumentFileTypeState;

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  getFormGroup(key: string): FormGroup {
    return FormValidationHelper.getFormGroup(this.form, key);
  }

  listPublicationSubtypeDocFileObs: Observable<PublicationSubtypeDocumentFileTypeGroup[]> = MemoizedUtil.select(this._store, SharedDocumentFileTypeState, state => state.listPublicationSubtypeDocumentFileTypeGroup);

  documentFileTypeEnumValues: KeyValue[] = Enums.DocumentFileType.DocumentFileTypeEnumTranslate;

  get accessLevelEnumValues(): KeyValue[] {
    if (this.restrictionMode === RestrictedModeEnum.closedOnlyWithoutLicense) {
      return Enums.DocumentFile.AccessLevelEnumTranslate.filter(a => a.key === Enums.DocumentFile.AccessLevelEnum.PRIVATE);
    } else if (this.restrictionMode === RestrictedModeEnum.publicOnlyWithoutLicense) {
      return Enums.DocumentFile.AccessLevelEnumTranslate.filter(a => a.key === Enums.DocumentFile.AccessLevelEnum.PUBLIC);
    }
    return Enums.DocumentFile.AccessLevelEnumTranslate;
  }

  embargoEnumValues: KeyValue[] = [];

  get accessLevelEnum(): typeof Enums.DocumentFile.AccessLevelEnum {
    return Enums.DocumentFile.AccessLevelEnum;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get restrictedModeEnum(): typeof RestrictedModeEnum {
    return RestrictedModeEnum;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<DepositDocumentFileUploadDialog>,
              protected readonly _fb: FormBuilder,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              public readonly breakpointService: BreakpointService,
              private readonly _changeDetector: ChangeDetectorRef,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositDocumentFileUploadDialogData) {
    super(_dialogRef, data);
    this.backTranslatePipe = new BackTranslatePipe(this._store, environment);
  }

  get isCreate(): boolean {
    return this.data.mode === "create";
  }

  get isDetail(): boolean {
    return this.data.mode === "detail";
  }

  get isEdit(): boolean {
    return this.data.mode === "edit";
  }

  get isDetailOrEdit(): boolean {
    return this.isDetail || this.isEdit;
  }

  get shouldDisplayEmbargoDuration(): boolean {
    return (this.isCreate || this.isEdit) && isNotNullNorUndefined(this.data.depositPublicationDate) && !isEmptyString(this.data.depositPublicationDate);
  }

  get isAtLeastOneFileInfoPresent(): boolean {
    return isNotNullNorUndefined(this.file?.type) || isNotNullNorUndefined(this.file?.size) || isNotNullNorUndefined(this.file?.lastModified);
  }

  ngOnInit(): void {
    super.ngOnInit();
    const queryParameters = new QueryParameters(environment.defaultEnumValuePageSizeOption);
    MappingObjectUtil.set(queryParameters.search.searchItems, this._UNDEFINED_LABEL_KEY, true);
    this._store.dispatch(new SharedDocumentFileTypeAction.GetAll(queryParameters));

    if (this.isCreate) {
      this.file = this.data.file;
      this.isInError = this.isFileNameInvalid(this.file);
    }

    this.form = this._fb.group({
      [this.formDefinition.documentFileTypeId]: [undefined, [Validators.required]],
      [this.formDefinition.documentFileTypeValue]: [undefined, []],
      [this.formDefinition.isLabel]: [false, []],
      [this.formDefinition.label]: [undefined, []],
      [this.formDefinition.notSureForLicense]: [false, []],
      [this.formDefinition.licenseId]: [undefined, []],
      [this.formDefinition.licenseGroupId]: [undefined, []],
      [this.formDefinition.accessLevel]: [Enums.DocumentFile.AccessLevelEnum.PUBLIC, [Validators.required]],
      [this.formDefinition.hasEmbargo]: [false],
      [this.formDefinition.embargoAccessLevel]: [undefined],
      [this.formDefinition.embargoDuration]: [undefined],
      [this.formDefinition.embargoEndDate]: [undefined],
    });

    if (this.isDetailOrEdit) {
      this.form.get(this.formDefinition.documentFileTypeId).setValue(this.data.documentFile.documentFileType.resId);
      this.form.get(this.formDefinition.documentFileTypeValue).setValue(this.data.documentFile.documentFileType.value);
      this.form.get(this.formDefinition.isLabel).setValue(isNotNullNorUndefinedNorWhiteString(this.data.documentFile.label));
      this.form.get(this.formDefinition.label).setValue(this.data.documentFile.label);
      this.form.get(this.formDefinition.notSureForLicense).setValue(this.data.documentFile.notSureForLicense);
      this.form.get(this.formDefinition.licenseId).setValue(this.data.documentFile.license?.resId);
      this.form.get(this.formDefinition.licenseGroupId).setValue(this.data.documentFile.license?.licenseGroup?.resId);
      this.form.get(this.formDefinition.accessLevel).setValue(this.data.documentFile.accessLevel);
      this.form.get(this.formDefinition.hasEmbargo).setValue(isNotNullNorUndefined(this.data.documentFile.embargoAccessLevel));
      this.form.get(this.formDefinition.embargoAccessLevel).setValue(this.data.documentFile.embargoAccessLevel);
      this.form.get(this.formDefinition.embargoEndDate).setValue(this.data.documentFile.embargoEndDate);

      this.file = {
        name: this.data.documentFile.fileName,
        size: this.data.documentFile.fileSize,
        type: this.data.documentFile.mimetype,
      } as any;
      this.isInError = false;

      this.initEmbargoAccessLevel(this.form.get(this.formDefinition.accessLevel).value);
    }

    this._computeSpecificTypeSelected();

    this.subscribe(this.form.get(this.formDefinition.licenseGroupId).valueChanges.pipe(
      distinctUntilChanged(),
      tap(licenseGroupId => {
        this.form.get(this.formDefinition.licenseId).setValue(undefined);
        this._retrieveListLicensesVersions(licenseGroupId);
      }),
    ));
    this._retrieveListLicensesVersions(this.form.get(this.formDefinition.licenseGroupId).value);

    if (this.shouldDisplayEmbargoDuration) {
      this.subscribe(this.form.get(this.formDefinition.embargoDuration).valueChanges.pipe(
        distinctUntilChanged(),
        filter(numberMonth => isNumberReal(numberMonth) && numberMonth >= this.EMBARGO_DURATION_MIN && numberMonth <= this.EMBARGO_DURATION_MAX),
        tap(numberMonth => {
          const publicationDate = new Date(this.data.depositPublicationDate);
          const day = publicationDate.getDate();
          const newDate = new Date(publicationDate.getFullYear(), publicationDate.getMonth() + numberMonth + 1, 0);
          if (day < newDate.getDate()) {
            newDate.setDate(day);
          }
          newDate.setDate(newDate.getDate() + 1);
          this.form.get(this.formDefinition.embargoEndDate).setValue(newDate);
        }),
      ));
    }

    this.subscribe(this.form.get(this.formDefinition.documentFileTypeId).valueChanges.pipe(
      tap(documentFileTypeId => {
        const listDocumentFileType = MemoizedUtil.listSnapshot(this._store, SharedDocumentFileTypeState);
        if (isNonEmptyArray(listDocumentFileType)) {
          const documentFileTypeValue = listDocumentFileType.find(d => d.documentFileTypeId === documentFileTypeId);
          this.form.get(this.formDefinition.documentFileTypeValue).setValue(documentFileTypeValue?.value);
          this._computeRestrictionMode(documentFileTypeValue);
          if (this.restrictionMode === RestrictedModeEnum.closedOnlyWithoutLicense) {
            this.form.get(this.formDefinition.accessLevel).setValue(Enums.Deposit.AccessLevelEnum.PRIVATE);
            this._resetOtherThanAccessLevelAndTypeFormParameters();
          } else if (this.restrictionMode === RestrictedModeEnum.publicOnlyWithoutLicense) {
            this.form.get(this.formDefinition.accessLevel).setValue(Enums.Deposit.AccessLevelEnum.PUBLIC);
            this._resetOtherThanAccessLevelAndTypeFormParameters();
          }
        }
      }),
    ));

    this.validationEmbargo();

    if (this.isDetail) {
      this.form.disable();
    }
  }

  private _resetOtherThanAccessLevelAndTypeFormParameters(): void {
    this.form.get(this.formDefinition.isLabel).setValue(false);
    this.form.get(this.formDefinition.label).setValue(undefined);
    this.form.get(this.formDefinition.licenseGroupId).setValue(undefined);
    this.form.get(this.formDefinition.licenseId).setValue(undefined);
    this.form.get(this.formDefinition.notSureForLicense).setValue(false);
    this.form.get(this.formDefinition.hasEmbargo).setValue(false);
    this.form.get(this.formDefinition.embargoAccessLevel).setValue(undefined);
    this.form.get(this.formDefinition.embargoDuration).setValue(undefined);
    this.form.get(this.formDefinition.embargoEndDate).setValue(undefined);
  }

  private _computeSpecificTypeSelected(): void {
    const documentFileTypeId = this.form.get(this.formDefinition.documentFileTypeId)?.value;
    const listDocumentFileType = MemoizedUtil.listSnapshot(this._store, SharedDocumentFileTypeState);
    const documentFileTypeValue = listDocumentFileType?.find(d => d.documentFileTypeId === documentFileTypeId);
    if (isNullOrUndefined(documentFileTypeValue)) {
      return;
    }
    this._computeRestrictionMode(documentFileTypeValue);
  }

  private _isClosedOnlyTypeSelected(documentFileTypeValue: PublicationSubtypeDocumentFileTypeDTO): boolean {
    return (documentFileTypeValue.level === Enums.DocumentFileType.FileTypeLevelEnum.ADMINISTRATION && documentFileTypeValue.value === this._AGREEMENT_VALUE)
      ||
      (documentFileTypeValue.level === Enums.DocumentFileType.FileTypeLevelEnum.PRINCIPAL && documentFileTypeValue.value === this._PUBLICATION_MODE_VALUE);
  }

  private _isPublicOnlyTypeSelected(documentFileTypeValue: PublicationSubtypeDocumentFileTypeDTO): boolean {
    return documentFileTypeValue.level === Enums.DocumentFileType.FileTypeLevelEnum.PRINCIPAL && documentFileTypeValue.value === this._IMPRIMATUR_VALUE;
  }

  private _computeRestrictionMode(documentFileTypeValue: PublicationSubtypeDocumentFileTypeDTO): void {
    if (this._isClosedOnlyTypeSelected(documentFileTypeValue)) {
      this.restrictionMode = RestrictedModeEnum.closedOnlyWithoutLicense;
    } else if (this._isPublicOnlyTypeSelected(documentFileTypeValue)) {
      this.restrictionMode = RestrictedModeEnum.publicOnlyWithoutLicense;
    } else {
      this.restrictionMode = RestrictedModeEnum.noRestriction;
    }
  }

  private _retrieveListLicensesVersions(licenseGroupId: string): void {
    if (isNullOrUndefinedOrWhiteString(licenseGroupId)) {
      return;
    }
    const queryParameters = new QueryParameters();
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(searchItems, SharedLicenseState.FIELD_SEARCH_RESEARCH_GROUP, licenseGroupId);
    queryParameters.sort = {
      field: SharedLicenseState.FIELD_SORT_VALUE,
      order: OrderEnum.ascending,
    };
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new SharedLicenseAction.GetAll(queryParameters),
      SharedLicenseAction.GetAllSuccess,
      result => {
        const fcLicenseId = this.form.get(this.formDefinition.licenseId);
        const listLicenseVersion = result.list._data;
        if (isNonEmptyArray(listLicenseVersion) && isNullOrUndefinedOrWhiteString(fcLicenseId?.value)) {
          let licenseId = listLicenseVersion[0].resId;
          if (licenseGroupId.startsWith(this._GROUP_CC_PREFIX_KEY)) {
            const listLicenseOnlyVersion = listLicenseVersion.filter(l => isNotNullNorUndefined(l.version));
            if (isNonEmptyArray(listLicenseOnlyVersion)) {
              licenseId = listLicenseOnlyVersion[0].resId;
            }
          }
          fcLicenseId.setValue(licenseId);
        }
      }));
  }

  getDate(lastModified: number): Date {
    return new Date(lastModified);
  }

  onSubmit(): void {
    const documentFile = {
      documentFileType: {
        resId: this.form.get(this.formDefinition.documentFileTypeId).value,
      },
      accessLevel: this.form.get(this.formDefinition.accessLevel).value,
      label: this.form.get(this.formDefinition.label).value,
    } as DocumentFile;

    const licenseId = this.form.get(this.formDefinition.licenseId).value;
    if (isNotNullNorUndefined(licenseId) && isNonEmptyString(licenseId)) {
      documentFile.license = {
        resId: licenseId,
      };
    } else {
      documentFile.license = null;
    }
    if (this.form.get(this.formDefinition.hasEmbargo).value && documentFile.accessLevel !== Enums.Deposit.AccessLevelEnum.PRIVATE) {
      documentFile.embargoAccessLevel = this.form.get(this.formDefinition.embargoAccessLevel).value;
      documentFile.embargoEndDate = DateUtil.convertToLocalDateDateSimple(this.form.get(this.formDefinition.embargoEndDate).value);
    } else {
      documentFile.embargoAccessLevel = null;
      documentFile.embargoEndDate = null;
    }

    documentFile.notSureForLicense = this.form.get(this.formDefinition.notSureForLicense).value;

    if (documentFile.accessLevel !== Enums.DocumentFile.AccessLevelEnum.PUBLIC) {
      documentFile.notSureForLicense = false;
      documentFile.license = null;
    }

    if (this.isCreate) {
      const fileUploadWrapper = {
        documentFile: documentFile,
        depositId: this.data.depositId,
        file: this.file,
      } as AouFileUploadWrapper;
      this.submit(fileUploadWrapper);
    } else if (this.isEdit) {
      documentFile.resId = this.data.documentFile.resId;
      this.submit(documentFile);
    } else {
      this.close();
    }
  }

  delete(): void {
    this.file = undefined;
    this.isInError = undefined;
  }

  isFileNameInvalid(file: File): boolean {
    const fileName = file.name;
    return this.REGEX_INVALID.test(fileName);
  }

  adaptEmbargoAccessLevel(value: Enums.DocumentFile.AccessLevelEnum): void {
    this.initEmbargoAccessLevel(value);
    const formControlEmbargoAccessLevel = this.form.get(this.formDefinition.embargoAccessLevel);
    if (isNotNullNorUndefined(formControlEmbargoAccessLevel?.value) && this.embargoEnumValues.findIndex(e => e.key === formControlEmbargoAccessLevel.value) === -1) {
      formControlEmbargoAccessLevel.setValue(undefined);
    }
    const accessLevel = this.form.get(this.formDefinition.accessLevel)?.value;
    const embargoAccessLevelFormControl = this.form.get(this.formDefinition.embargoAccessLevel);

    if (accessLevel === Enums.DocumentFile.AccessLevelEnum.RESTRICTED) {
      embargoAccessLevelFormControl.setValue(Enums.DocumentFile.AccessLevelEnum.PRIVATE);
    }
  }

  initEmbargoAccessLevel(value: Enums.DocumentFile.AccessLevelEnum): void {
    this.embargoEnumValues = Enums.DocumentFile.embargoAccessLevelEnumList(value);
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  private validationEmbargo(): void {
    const embargoEndDateFormControl = this.form.get(this.formDefinition.embargoEndDate);
    const embargoAccessLevelFormControl = this.form.get(this.formDefinition.embargoAccessLevel);
    this.subscribe(this.form.get(this.formDefinition.hasEmbargo).valueChanges.pipe(
      distinctUntilChanged(),
      tap(hasEmbargo => {
        if (hasEmbargo === true) {
          embargoEndDateFormControl.setValidators([Validators.required]);
          embargoAccessLevelFormControl.setValidators([Validators.required]);
        } else {
          embargoEndDateFormControl.setValidators([]);
          embargoAccessLevelFormControl.setValidators([]);
        }
        embargoEndDateFormControl.updateValueAndValidity();
        embargoAccessLevelFormControl.updateValueAndValidity();
        this._changeDetector.detectChanges();
      }),
    ));
  }

  cleanEmbargoDuration(): void {
    this.form.get(this.formDefinition.embargoDuration).setValue(undefined);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() documentFileTypeId: string;
  @PropertyName() documentFileTypeValue: string;
  @PropertyName() isLabel: string;
  @PropertyName() notSureForLicense: string;
  @PropertyName() label: string;
  @PropertyName() licenseGroupId: string;
  @PropertyName() licenseId: string;
  @PropertyName() accessLevel: string;
  @PropertyName() hasEmbargo: string;
  @PropertyName() embargoAccessLevel: string;
  @PropertyName() embargoEndDate: string;
  @PropertyName() embargoDuration: string;
}

export interface DepositDocumentFileUploadDialogData {
  mode: "edit" | "create" | "detail";
  file: File;
  documentFile: DocumentFile;
  depositId: string;
  depositPublicationDate: Date | undefined;
}

type RestrictedModeEnum =
  "closedOnlyWithoutLicense"
  | "publicOnlyWithoutLicense"
  | "noRestriction";
const RestrictedModeEnum = {
  closedOnlyWithoutLicense: "closedOnlyWithoutLicense" as RestrictedModeEnum,
  publicOnlyWithoutLicense: "publicOnlyWithoutLicense" as RestrictedModeEnum,
  noRestriction: "noRestriction" as RestrictedModeEnum,
};
