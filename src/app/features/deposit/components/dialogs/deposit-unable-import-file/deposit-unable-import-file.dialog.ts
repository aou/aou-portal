/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-unable-import-file.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DocumentFile} from "@models";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";

@Component({
  selector: "aou-deposit-unable-import-file-dialog",
  templateUrl: "./deposit-unable-import-file.dialog.html",
  styleUrls: ["./deposit-unable-import-file.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositUnableImportFileDialog extends SharedAbstractDialog<DepositUnableImportFileDialogData> implements OnInit {
  constructor(protected readonly _dialogRef: MatDialogRef<DepositUnableImportFileDialog>,
              protected readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositUnableImportFileDialogData) {
    super(_dialogRef, data);
  }

  confirm(): void {
    this.close();
  }
}

export interface DepositUnableImportFileDialogData {
  listFileUnableToImport: DocumentFile[];
}
