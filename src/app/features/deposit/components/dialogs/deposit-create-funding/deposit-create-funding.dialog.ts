/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-create-funding.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  DepositFormFunding,
  FormComponentFormDefinitionDepositFormFunding,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  FormValidationHelper,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-create-funding-dialog",
  templateUrl: "./deposit-create-funding.dialog.html",
  styleUrls: ["./deposit-create-funding.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCreateFundingDialog extends SharedAbstractDialog<DepositCreateFundingDialogData, DepositFormFunding> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinitionDepositFormFunding = new FormComponentFormDefinitionDepositFormFunding();

  constructor(protected readonly _dialogRef: MatDialogRef<DepositCreateFundingDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositCreateFundingDialogData,
              protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _fb: FormBuilder) {
    super(_dialogRef, data);
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.funder]: [undefined, [SolidifyValidator, Validators.required]],
      [this.formDefinition.name]: [this.data?.preSetName, [SolidifyValidator]],
      [this.formDefinition.code]: [this.data?.preSetCode, [SolidifyValidator]],
    });
  }

  confirm(): void {
    this.submit(this.form.value);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }
}

export interface DepositCreateFundingDialogData {
  preSetName: string;
  preSetCode: string;
}
