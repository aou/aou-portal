/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-bulk-structure.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Structure} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {sharedStructureActionNameSpace} from "@shared/stores/structure/shared-structure.action";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {
  BaseFormDefinition,
  FormValidationHelper,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-bulk-structure-dialog",
  templateUrl: "./deposit-bulk-structure.dialog.html",
  styleUrls: ["./deposit-bulk-structure.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositBulkStructureDialog extends SharedAbstractDialog<DepositBulkStructureDialogData, string> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  sharedStructureSort: Sort<Structure> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedStructureActionNameSpace: ResourceNameSpace = sharedStructureActionNameSpace;
  sharedStructureState: typeof SharedStructureState = SharedStructureState;
  structureLabel: (value: Structure) => string = value => SharedStructureState.labelCallback(value, this._translateService);

  constructor(protected readonly _dialogRef: MatDialogRef<DepositBulkStructureDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositBulkStructureDialogData,
              protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _translateService: TranslateService) {
    super(_dialogRef, data);
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.structureId]: [undefined, [SolidifyValidator, Validators.required]],
    });
  }

  confirm(): void {
    const structureId = this.form.get(this.formDefinition.structureId).value;
    this.submit(structureId);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }
}

export interface DepositBulkStructureDialogData {
  titleToTranslate: string;
  messageToTranslate: string;
  numberDeposit: number;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() structureId: string;
}
