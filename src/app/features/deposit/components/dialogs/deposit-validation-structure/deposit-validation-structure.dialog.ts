/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-validation-structure.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {environment} from "@environments/environment";
import {Structure} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedStructureAction} from "@shared/stores/structure/shared-structure.action";
import {
  BaseFormDefinition,
  ButtonColorEnum,
  FormValidationHelper,
  isNullOrUndefined,
  OrderEnum,
  PropertyName,
  QueryParameters,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-validation-structure-dialog",
  templateUrl: "./deposit-validation-structure.dialog.html",
  styleUrls: ["./deposit-validation-structure.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositValidationStructureDialog extends SharedAbstractDialog<DepositValidationStructureDialogData, string> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  isLoading: boolean = true;
  listStructure: Structure[] = [];

  constructor(protected readonly _dialogRef: MatDialogRef<DepositValidationStructureDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositValidationStructureDialogData,
              protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super(_dialogRef, data);
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.structureId]: [undefined, [SolidifyValidator, Validators.required]],
    });
    this.subscribe(
      StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedStructureAction.GetAll(new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
          field: "sortValue",
          order: OrderEnum.ascending,
        })),
        SharedStructureAction.GetAllSuccess,
        resultAction => {
          this.listStructure = resultAction.list._data;
          this.isLoading = false;
          this._changeDetector.detectChanges();
        }),
    );
  }

  confirm(): void {
    const structureId = this.form.get(this.formDefinition.structureId).value;
    this.submit(structureId);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  selectStructure(structure: Structure): void {
    this.form.get(this.formDefinition.structureId).setValue(structure.resId);
  }

  get colorConfirm(): string {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.primary : this.data.colorConfirm;
  }
}

export interface DepositValidationStructureDialogData {
  titleToTranslate: string;
  messageToTranslate: string;
  colorConfirm?: string;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() structureId: string;
}
