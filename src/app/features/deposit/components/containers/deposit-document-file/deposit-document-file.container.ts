/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-document-file.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositDocumentFileUploadDialog} from "@app/features/deposit/components/dialogs/deposit-document-file-upload/deposit-document-file-upload.dialog";
import {FileUploadStatusEnum} from "@app/features/deposit/enums/file-upload-status.enum";
import {DocumentFileUploadHelper} from "@app/features/deposit/helpers/document-file-upload.helper";
import {DepositAction} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {DepositDocumentFileAction} from "@app/features/deposit/stores/document-file/deposit-document-file.action";
import {DepositDocumentFileState} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {DepositDocumentFileStatusHistoryAction} from "@app/features/deposit/stores/document-file/status-history/deposit-document-file-status-history.action";
import {DepositDocumentFileStatusHistoryState} from "@app/features/deposit/stores/document-file/status-history/deposit-document-file-status-history.state";
import {AouFileUploadWrapper} from "@deposit/models/aou-file-upload-wrapper.model";
import {AouUploadFileStatus} from "@deposit/models/aou-upload-file-status.model";
import {DepositDocumentFileUploadAction} from "@deposit/stores/document-file/upload/deposit-document-file-upload.action";
import {DepositDocumentFileUploadState} from "@deposit/stores/document-file/upload/deposit-document-file-upload.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DocumentFile,
  DocumentFileType,
  License,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {DepositRoleHelper} from "@shared/helpers/deposit-role.helper";
import {SecurityService} from "@shared/services/security.service";
import {sharedDocumentFileTypeActionNameSpace} from "@shared/stores/document-file-type/shared-document-file-type.action";
import {SharedDocumentFileTypeState} from "@shared/stores/document-file-type/shared-document-file-type.state";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {Observable} from "rxjs";
import {
  map,
  tap,
} from "rxjs/operators";
import {
  BackTranslatePipe,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  DialogUtil,
  FilePreviewDialog,
  FileVisualizerHelper,
  FileVisualizerService,
  Guid,
  HTMLInputEvent,
  isEmptyArray,
  isEmptyString,
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  PollingHelper,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  SOLIDIFY_CONSTANTS,
  SolidifyDataFileModel,
  Sort,
  StatusHistoryDialog,
  UploadFileStatus,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-document-file",
  templateUrl: "./deposit-document-file.container.html",
  styleUrls: ["./deposit-document-file.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositDocumentFileContainer extends SharedAbstractRoutable implements OnInit {
  backTranslatePipe: BackTranslatePipe;

  private readonly _KEY_QUERY_PARAMETERS: keyof DocumentFile = "status";
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositState);
  isLoadingDocumentFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositDocumentFileState);
  listDocumentFileObs: Observable<DocumentFile[]> = MemoizedUtil.list(this._store, DepositDocumentFileState);
  totalDocumentFileObs: Observable<number> = MemoizedUtil.total(this._store, DepositDocumentFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, DepositDocumentFileState).pipe(
    tap(queryParameters => this.isInErrorStatusFilter = MappingObjectUtil.get(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_QUERY_PARAMETERS) === Enums.DocumentFile.StatusEnum.IN_ERROR),
  );
  uploadStatusObs: Observable<AouUploadFileStatus[]> = MemoizedUtil.select(this._store, DepositDocumentFileUploadState, state => state.uploadStatus);
  numberUploadInProgressObs: Observable<number> = this.uploadStatusObs.pipe(
    map(uploadStatus => uploadStatus?.filter(u => u.status === FileUploadStatusEnum.inProgress)?.length),
  );
  uploadStatusFilteredByCurrentContextObs: Observable<AouUploadFileStatus[]>;

  private _depositId: string;

  @Input()
  set depositId(value: string) {
    const valueChange = value !== this._depositId;
    this._depositId = value;
    if (valueChange) {
      this._getSubResourceWithParentId(this.depositId);
    }
  }

  get depositId(): string {
    return this._depositId;
  }

  @Input()
  isReadonly: boolean = false;

  @Input()
  notifyFilesFromExternalSource: boolean;

  get alreadyDisplayDialogFileImportedAutomatically(): boolean {
    return MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.alreadyDisplayDialogFileImportedAutomatically);
  }

  @Input()
  depositPublicationDate: Date;

  @Input()
  canEdit: boolean;

  @Input()
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[] = [Enums.Deposit.UserRoleEnum.CREATOR, Enums.Deposit.UserRoleEnum.CONTRIBUTOR];

  @Input()
  isValidationMode: boolean = false;

  @Input()
  listCurrentStatus: MappingObject<Enums.DocumentFile.StatusEnum, number>;

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<DocumentFile>;

  columns: DataTableColumns<DocumentFile>[];
  actions: DataTableActions<DocumentFile>[] = [
    {
      logo: IconNameEnum.done,
      callback: (documentFile: DocumentFile) => this.confirm(this.depositId, documentFile),
      placeholder: current => LabelTranslateEnum.confirm,
      displayOnCondition: (documentFile: DocumentFile) => documentFile.status === Enums.DocumentFile.StatusEnum.TO_BE_CONFIRMED,
    },
    // {
    //   logo: IconNameEnum.resume,
    //   callback: (documentFile: DocumentFile) => this.resumeDocumentFile(this.depositId, documentFile),
    //   placeholder: current => LabelTranslateEnum.resume,
    //   displayOnCondition: (documentFile: DocumentFile) => documentFile.status === Enums.DocumentFile.StatusEnum.IN_ERROR,
    // },
    {
      logo: IconNameEnum.edit,
      callback: (documentFile: DocumentFile) => this.openDetailFile(documentFile),
      placeholder: current => LabelTranslateEnum.edit,
      displayOnCondition: (documentFile: DocumentFile) => this.canEdit && documentFile.status !== Enums.DocumentFile.StatusEnum.IN_ERROR
        && documentFile.status !== Enums.DocumentFile.StatusEnum.IMPOSSIBLE_TO_DOWNLOAD,
    },
    {
      logo: IconNameEnum.delete,
      callback: (documentFile: DocumentFile) => this._deleteDocumentFile(this.depositId, documentFile),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: (documentFile: DocumentFile) => this.canEdit,
    },
    {
      logo: IconNameEnum.download,
      callback: (documentFile: DocumentFile) => this.downloadDocumentFile(this.depositId, documentFile),
      placeholder: current => LabelTranslateEnum.download,
      displayOnCondition: (documentFile: DocumentFile) => this._canDisplayDownloadAndPreviewAction(documentFile),
      isWrapped: false,
      isVisible: true,
    },
    {
      logo: IconNameEnum.preview,
      callback: (documentFile: DocumentFile) => this._showPreview(documentFile),
      placeholder: current => LabelTranslateEnum.showPreview,
      displayOnCondition: (documentFile: DocumentFile) => this._canDisplayDownloadAndPreviewAction(documentFile) && FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
        dataFile: DepositDocumentFileContainer._dataFileAdapter(documentFile),
        fileExtension: FileVisualizerHelper.getFileExtension(documentFile.fileName),
      }),
      isWrapped: false,
      isVisible: true,
    },
    {
      logo: IconNameEnum.history,
      callback: (documentFile: DocumentFile) => this.showHistory(this.depositId, documentFile),
      placeholder: current => LabelTranslateEnum.showHistoryStatus,
    },
  ];

  readonly KEY_PARAM_NAME: keyof Deposit & string = undefined;
  isInErrorStatusFilter: boolean = false;

  sharedLicenseSort: Sort<License> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;

  sharedDocumentFileTypeSort: Sort<DocumentFileType> = {
    field: "value",
    order: OrderEnum.ascending,
  };
  sharedDocumentFileTypeActionNameSpace: ResourceNameSpace = sharedDocumentFileTypeActionNameSpace;
  sharedDocumentFileTypeState: typeof SharedDocumentFileTypeState = SharedDocumentFileTypeState;

  private _shouldContinuePolling: boolean = false;
  highlightLicenseNotSure: (documentFile: DocumentFile) => boolean = documentFile => this.isValidationMode && documentFile.notSureForLicense;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _securityService: SecurityService,
              private readonly _fileVisualizerService: FileVisualizerService) {
    super();
    this._defineColumns();
    this.backTranslatePipe = new BackTranslatePipe(this._store, environment);
    this.uploadStatusFilteredByCurrentContextObs = this.uploadStatusObs.pipe(map(listUploadStatus => listUploadStatus.filter(uploadStatus => uploadStatus.fileUploadWrapper.depositId === this.depositId)));
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._createPollingListenerForFileReady();
  }

  lineTooltipCallback(documentFile: DocumentFile): string | undefined {
    if (documentFile.notSureForLicense) {
      return LabelTranslateEnum.licenseToBeCheckedUponValidation;
    }
  }

  private _defineColumns(): void {
    this.columns = [
      {
        field: "documentFileType",
        header: LabelTranslateEnum.publicLink,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        resourceNameSpace: this.sharedDocumentFileTypeActionNameSpace,
        resourceState: this.sharedDocumentFileTypeState as any,
        searchableSingleSelectSort: this.sharedDocumentFileTypeSort,
        filterableField: "documentFileType" as any,
        sortableField: "documentFileType.value" as any,
        resourceValueKey: "documentFileTypeId",
        isSortable: false,
        isFilterable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.fileType),
      },
      {
        field: "accessLevel",
        header: LabelTranslateEnum.accessLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        filterableField: "accessLevel",
        sortableField: "accessLevel",
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.DocumentFile.AccessLevelEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.accessLevelWithEmbargo),
      },
      {
        field: "license",
        header: LabelTranslateEnum.license,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        resourceNameSpace: this.sharedLicenseActionNameSpace,
        resourceState: this.sharedLicenseState as any,
        searchableSingleSelectSort: this.sharedLicenseSort,
        filterableField: "license" as any,
        sortableField: "license.title" as any,
        resourceLabelKey: "resId",
        resourceLabelCallback: (value: License) => this._licenseLabel(value),
        useResourceLabelCallback: true,
        isSortable: false,
        isFilterable: false,
        conditionallyDisplay: data => data.accessLevel === Enums.Deposit.AccessLevelEnum.PUBLIC,
      },
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        filterableField: "fileName",
        sortableField: "sourceData",
        isSortable: false,
        isFilterable: false,
      },
      {
        field: "fileSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.size,
        order: OrderEnum.none,
        sortableField: "fileSize",
        filterableField: "fileSize",
        isSortable: false,
        isFilterable: false,
        translate: true,
        width: "100px",
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.uploaded,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "status",
        isSortable: false,
        isFilterable: false,
        translate: true,
        tooltip: (value) => Enums.DocumentFile.getExplanation(value as Enums.DocumentFile.StatusEnum),
        filterEnum: Enums.DocumentFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
      },
    ];
  }

  private _licenseLabel(license: License): string {
    if (isNullOrUndefined(license) || isEmptyString(license)) {
      return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
    let version = undefined;
    if (isNotNullNorUndefinedNorWhiteString(license.version)) {
      version = license.version;
    }
    let title = undefined;
    const licenseGroup = license.licenseGroup;
    if (isNotNullNorUndefined(licenseGroup)) {
      if (isNonEmptyArray(licenseGroup.labels)) {
        title = this.backTranslatePipe.transform(licenseGroup.labels);
      } else {
        title = licenseGroup.name;
      }
    } else {
      if (isNonEmptyArray(license.labels)) {
        title = this.backTranslatePipe.transform(license.labels);
      } else {
        title = license.title;
      }
    }
    return title + (isNotNullNorUndefinedNorWhiteString(version) ? SOLIDIFY_CONSTANTS.SPACE + version : SOLIDIFY_CONSTANTS.STRING_EMPTY);
  }

  private _createPollingListenerForFileReady(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositDataFileIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      doBeforeFilter: () => {
        if (this.notifyFilesFromExternalSource && isFalse(this.alreadyDisplayDialogFileImportedAutomatically)) {
          const listFilePendingExternal = this._filterExternalPublication(DocumentFileUploadHelper.listFilesPending(this._store));
          if (isEmptyArray(listFilePendingExternal)) {
            const listFileImpossibleToImport = this._filterExternalPublication(DocumentFileUploadHelper.listFilesImpossibleToDownload(this._store));
            if (isNonEmptyArray(listFileImpossibleToImport)) {
              this._store.dispatch(new DepositAction.DisplayDialogUnableToImportFile(listFileImpossibleToImport));
              listFileImpossibleToImport.forEach(documentImpossibleToDownload => {
                this._deleteDocumentFile(this.depositId, documentImpossibleToDownload);
              });
            } else if (this._filterExternalPublication(DocumentFileUploadHelper.listFilesByStatus(this._store, Enums.DocumentFile.StatusEnum.READY, Enums.DocumentFile.StatusEnum.CONFIRMED)).length > 0) {
              this._store.dispatch(new DepositAction.DisplayDialogFileImportedAutomatically());
            }
          }
        }
        this.computedShouldContinuePolling();
      },
      filter: () => this._shouldContinuePolling,
      actionToDo: () => {
        this._store.dispatch(new DepositDocumentFileAction.Refresh(this.depositId));
      },
    }));
  }

  private _filterExternalPublication(list: DocumentFile[]): DocumentFile[] {
    return list?.filter(f => isNotNullNorUndefinedNorWhiteString(f.sourceData) && (f.sourceData.startsWith("http://") || f.sourceData.startsWith("https://")));
  }

  private computedShouldContinuePolling(): void {
    this._shouldContinuePolling = DocumentFileUploadHelper.numberFilesPending(this.listCurrentStatus) > 0 || DocumentFileUploadHelper.listFilesPending(this._store).length > 0;
  }

  _getSubResourceWithParentId(id: string): void {
  }

  onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    this._store.dispatch(new DepositDocumentFileAction.ChangeQueryParameters(this.depositId, queryParameters, true, withRefresh));
  }

  refresh(): void {
    this._store.dispatch(new DepositDocumentFileAction.Refresh(this.depositId));
  }

  downloadDocumentFile(parentId: string, $event: DocumentFile): void {
    this._store.dispatch(new DepositDocumentFileAction.Download(parentId, $event));
  }

  openDetailFile(documentFile: DocumentFile): void {
    if (this.isReadonly) {
      return;
    }
    this.subscribe(DialogUtil.open(this._dialog, DepositDocumentFileUploadDialog, {
        mode: this.canEdit && documentFile.status !== Enums.DocumentFile.StatusEnum.IN_ERROR ? "edit" : "detail",
        file: undefined,
        documentFile: documentFile,
        depositPublicationDate: this.depositPublicationDate,
      }, {
        width: "700px",
      }, documentFileUpdated => this._update(documentFileUpdated as DocumentFile),
    ));
  }

  private _deleteDocumentFile(parentId: string, documentFile: DocumentFile): void {
    this._store.dispatch(new DepositDocumentFileAction.Delete(parentId, documentFile.resId));
  }

  openModalUpload(file: File): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositDocumentFileUploadDialog, {
      mode: "create",
      file: file,
      documentFile: undefined,
      depositId: this._depositId,
      depositPublicationDate: this.depositPublicationDate,
    }, {
      width: "700px",
    }, filesUploadWrapper => this._upload(filesUploadWrapper as AouFileUploadWrapper)));
  }

  private _upload($event: AouFileUploadWrapper): void {
    this._store.dispatch(new DepositDocumentFileUploadAction.UploadFile(this.depositId, $event));
  }

  private _update(documentFile: DocumentFile): void {
    this._store.dispatch(new DepositDocumentFileAction.Update(this.depositId, documentFile));
  }

  cancel(uploadStatus: UploadFileStatus<DocumentFile>): void {
    this._store.dispatch(new DepositDocumentFileUploadAction.CancelFileSending(this.depositId, uploadStatus));
  }

  retry(uploadStatus: UploadFileStatus<DocumentFile>): void {
    this._store.dispatch(new DepositDocumentFileUploadAction.RetrySendFile(this.depositId, uploadStatus));
  }

  trackByFn(index: number, item: AouUploadFileStatus): Guid {
    return item.guid;
  }

  showHistory(resId: string, documentFile: DocumentFile): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: resId,
      resourceResId: documentFile.resId,
      name: documentFile.fileName,
      statusHistory: MemoizedUtil.select(this._store, DepositDocumentFileStatusHistoryState, state => state.history),
      isLoading: MemoizedUtil.isLoading(this._store, DepositDocumentFileStatusHistoryState),
      queryParametersObs: MemoizedUtil.select(this._store, DepositDocumentFileStatusHistoryState, state => state.queryParameters),
      state: DepositDocumentFileStatusHistoryAction,
      statusEnums: Enums.DocumentFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  confirm(resId: string, documentFile: DocumentFile): void {
    this._store.dispatch(new DepositDocumentFileAction.ConfirmDocumentFile(this.depositId, documentFile.resId));
  }

  onFileChange(event: HTMLInputEvent): void {
    this.openModalForFirstFile(Array.from(event.target.files));
  }

  openModalForFirstFile(files: File[]): void {
    if (files.length > 0) {
      const file = files[0];
      this.openModalUpload(file);
    }
  }

  sendSortedData(documentFiles: DocumentFile[]): void {
    this._store.dispatch(new DepositDocumentFileAction.SendSortOrderDocumentFiles(this.depositId, documentFiles.map(d => d.resId)));
  }

  private _showPreview(documentFile: DocumentFile): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
      fileInput: {
        dataFile: DepositDocumentFileContainer._dataFileAdapter(documentFile),
      },
      fileDownloadUrl: `${ApiEnum.adminPublications}/${documentFile.publication.resId}/${ApiResourceNameEnum.DOCUMENT_FILES}/${documentFile.resId}/${ApiActionNameEnum.DOWNLOAD}`,
    }, {
      width: "max-content",
      maxWidth: "90vw",
      height: "min-content",
    });
  }

  private static _dataFileAdapter(documentFile: DocumentFile): SolidifyDataFileModel {
    return {
      fileName: documentFile.fileName,
      fileSize: documentFile.fileSize,
      status: documentFile.status as any,
      mimetype: documentFile.mimetype,
    };
  }

  private _canDisplayDownloadAndPreviewAction(documentFile: DocumentFile): boolean {
    const validStatus = documentFile.status === Enums.DocumentFile.StatusEnum.PROCESSED
      || documentFile.status === Enums.DocumentFile.StatusEnum.READY
      || documentFile.status === Enums.DocumentFile.StatusEnum.CONFIRMED
      || documentFile.status === Enums.DocumentFile.StatusEnum.TO_BE_CONFIRMED
      || documentFile.status === Enums.DocumentFile.StatusEnum.DUPLICATE;
    if (isFalse(validStatus)) {
      return false;
    }
    if (DepositRoleHelper.isCreatorOrContributorOrValidator(this.listUserRoleEnum) || this.isValidationMode || this._securityService.isRootOrAdmin()) {
      return true;
    }
    if (documentFile.accessLevel === Enums.DocumentFile.AccessLevelEnum.PRIVATE) {
      // final access private --> never allowed
      return false;
    }
    if (isNullOrUndefined(documentFile.embargoAccessLevel) || new Date(documentFile.embargoEndDate) < new Date()) {
      // no embargo or embargo is over --> allowed
      return true;
    }
    // embargo running --> allowed if embargo level is not private
    return documentFile.embargoAccessLevel !== Enums.DocumentFile.AccessLevelEnum.PRIVATE;
  }
}
