/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-table-contributor-search.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import {ContributorRockSolid} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedExternalDataPersonAction} from "@shared/stores/external-data/external-data-person/shared-external-data-person.action";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  CollectionTyped,
  isEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObjectUtil,
  ObjectUtil,
  ObservableUtil,
  QueryParameters,
  QueryParametersUtil,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-table-contributor-search-container",
  templateUrl: "./deposit-table-contributor-search.container.html",
  styleUrls: ["./deposit-table-contributor-search.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositTableContributorSearchContainer extends SharedAbstractContainer implements OnInit, OnChanges {
  @Input()
  firstName: string;

  @Input()
  lastName: string;

  @Input()
  cnIndividu: string;

  result: CollectionTyped<ContributorRockSolid>;

  get totalItems(): number {
    if (isNullOrUndefined(this.result?._page)) {
      return 0;
    }
    return this.result._page.totalItems;
  }

  get isLoading(): boolean {
    return this.isLoadingCounter > 0;
  }

  get isLoadingNextChunk(): boolean {
    return this.isLoadingNextChunkCounter > 0;
  }

  isLoadingCounter: number = 0;
  isLoadingNextChunkCounter: number = 0;

  get list(): ContributorRockSolid[] {
    if (isNullOrUndefined(this.result)) {
      return [];
    }
    return this.result._data;
  }

  selectedContributor: ContributorRockSolid;

  searchIndex: number = 0;

  private readonly _PAGE_SIZE: number = 10;

  queryParameters: QueryParameters;

  private readonly _valueBS: BehaviorSubject<ContributorRockSolid | undefined> = new BehaviorSubject<ContributorRockSolid | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<ContributorRockSolid | undefined> = ObservableUtil.asObservable(this._valueBS);

  private readonly _initBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("init")
  readonly initObs: Observable<void> = ObservableUtil.asObservable(this._initBS);

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._initBS.next();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);

    if (isNotNullNorUndefined(changes.firstName) || isNotNullNorUndefined(changes.lastName)) {
      this._search();
    }
  }

  private _search(): void {
    if (isNullOrUndefined(this.firstName)
      || isNullOrUndefined(this.lastName)
      || this.firstName.length <= 1
      || this.lastName.length <= 1) {
      return;
    }
    this.searchIndex++;
    const currentSearchIndex = this.searchIndex;
    this.isLoadingCounter++;
    this.isLoadingNextChunkCounter = 0;
    this.result = undefined;
    this._initBS.next();

    this.queryParameters = new QueryParameters(this._PAGE_SIZE);
    const search = QueryParametersUtil.getSearchItems(this.queryParameters);
    MappingObjectUtil.set(search, "firstname", this.firstName);
    MappingObjectUtil.set(search, "lastname", this.lastName);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedExternalDataPersonAction.GetAll(this.queryParameters, false, false),
      SharedExternalDataPersonAction.GetAllSuccess,
      result => {
        if (currentSearchIndex !== this.searchIndex) {
          this.isLoadingCounter--;
          this._changeDetector.detectChanges();
          return;
        }

        this.result = result.list;
        this.updateQueryParameters();

        if (isNotNullNorUndefined(this.selectedContributor)) {
          this.selectedContributor = this.list.find(c => c.cnIndividu === this.selectedContributor.cnIndividu);
        }
        this.isLoadingCounter--;
        this._changeDetector.detectChanges();
      },
      SharedExternalDataPersonAction.GetAllFail,
      result => {
        if (currentSearchIndex !== this.searchIndex) {
          return;
        }
        this.isLoadingCounter--;
        this._changeDetector.detectChanges();
      },
    ));
  }

  onScroll(scrollEvent: Event, elementRef: HTMLElement): void {
    if (this.isLoading || this.isLoadingNextChunk || isEmptyArray(this.list) || this.totalItems === this.list.length) {
      return;
    }
    const currentPosition = elementRef.scrollTop + elementRef.offsetHeight;
    const maxPosition = elementRef.scrollHeight - 1;

    if (currentPosition >= maxPosition) {
      this._loadNextChunk();
    }
  }

  preventScrollIfLoadingNextChunk(wheelEvent: Event): void {
    if (this.isLoadingNextChunk) {
      wheelEvent.preventDefault();
      wheelEvent.stopPropagation();
    }
  }

  private _loadNextChunk(): void {
    if (!StoreUtil.isNextChunkAvailable(this.queryParameters)) {
      return;
    }

    this.queryParameters.paging.pageIndex = this.queryParameters.paging.pageIndex + 1;
    const currentSearchIndex = this.searchIndex;
    this.isLoadingNextChunkCounter++;

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedExternalDataPersonAction.GetAll(this.queryParameters, false, false),
      SharedExternalDataPersonAction.GetAllSuccess,
      result => {
        if (currentSearchIndex !== this.searchIndex) {
          this.isLoadingNextChunkCounter--;
          this._changeDetector.detectChanges();
          return;
        }
        this.result._data = [...this.result._data, ...result.list._data];
        this.result._page = result.list._page;
        this.updateQueryParameters();
        this.isLoadingNextChunkCounter--;
        this._changeDetector.detectChanges();
      },
      SharedExternalDataPersonAction.GetAllFail,
      result => {
        if (currentSearchIndex !== this.searchIndex) {
          return;
        }
        this.isLoadingNextChunkCounter--;
        this._changeDetector.detectChanges();
      },
    ));
  }

  select(cnIndividu: string): void {
    this.selectedContributor = this.list.find(c => c.cnIndividu === cnIndividu);
  }

  cancel(): void {
    this.selectedContributor = null;
    this._valueBS.next(this.selectedContributor);
  }

  confirm(): void {
    this._valueBS.next(this.selectedContributor);
  }

  updateQueryParameters(): void {
    const queryParameters = ObjectUtil.clone(this.queryParameters);
    const paging = ObjectUtil.clone(queryParameters.paging);
    paging.length = isNullOrUndefined(this.result) ? 0 : this.result._page.totalItems;
    queryParameters.paging = paging;
    this.queryParameters = queryParameters;
  }
}
