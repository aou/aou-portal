/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-table-contributor.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {AOU_CONSTANTS} from "@app/constants";
import {
  DepositFormAbstractContributor,
  FormComponentFormDefinitionDepositFormContributor,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {UserUtil} from "@app/shared/utils/user.util";
import {AppContributorRolesState} from "@app/stores/contributor-role/app-contributor-roles.state";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {DepositTableContributorSearchContainer} from "@deposit/components/containers/deposit-table-contributor-search/deposit-table-contributor-search.container";
import {DepositAction} from "@deposit/stores/deposit.action";
import {DepositState} from "@deposit/stores/deposit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  ContributorRockSolid,
  PublicationSubtypeContributorRoleDTO,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BehaviorSubject,
  Observable,
  Subscription,
} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  FormError,
  FormValidationHelper,
  Guid,
  isFalse,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  OrcidService,
  ValidationErrorHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-table-contributor-container",
  templateUrl: "./deposit-table-contributor.container.html",
  styleUrls: ["./deposit-table-contributor.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: DepositTableContributorContainer,
    },
  ],
})
export class DepositTableContributorContainer extends SharedAbstractContainer implements ControlValueAccessor, OnInit {
  static probablyUnigeInstitutionRegex: RegExp = new RegExp("(Geneva|Genéve|Genève|Unige|HUG)", "gi");

  private readonly _INIT_WITH_ONE_PERSON: boolean = true;
  private readonly _ERROR_DUPLICATE_CN_INDIVIDU: string = "This contributor appears more than once";
  readonly MIN_CONTRIBUTOR_TO_DISPLAY_BUTTON_MOVE_UNIGE_MEMBER_TO_COLLABORATORS: number = environment.minContributorToDisplayButtonMoveUnigeMemberToCollaborators;
  readonly ERROR_FROM_BACKEND: string = AOU_CONSTANTS.ERROR_FROM_BACKEND;
  contributorRoleDisplayMode: "radio" | "dropdown" = environment.contributorRoleDisplayMode;

  searchContributorInfosMap: MappingObject<string, DepositTableContributorSearchContainer>;

  displayLastNameFirstName: boolean = true;

  @Input()
  mode: DepositTableContributorModeEnum;

  @Input()
  checkAtLeastOneDirector: boolean = false;

  @Input()
  checkAtLeastOneAuthor: boolean = false;

  @Input()
  defaultContributorRole: Enums.Deposit.RoleContributorEnum;

  @Input()
  userConnected: User;

  @Input()
  notifyContributorsFromExternalSource: boolean;

  formDefinition: FormComponentFormDefinitionDepositFormContributor = new FormComponentFormDefinitionDepositFormContributor();

  @HostBinding("class.disabled") disabled: boolean = false;

  get roleContributorEnum(): typeof Enums.Deposit.RoleContributorEnum {
    return Enums.Deposit.RoleContributorEnum;
  }

  get modeEnum(): typeof DepositTableContributorModeEnum {
    return DepositTableContributorModeEnum;
  }

  @Input()
  formArray: FormArray;

  private _listContributors: DepositFormAbstractContributor[];

  trackByFn(index: number, item: FormGroup): string {
    return index + item?.get(this.formDefinition?.guid)?.value;
  }

  @Input()
  set listContributors(value: DepositFormAbstractContributor[]) {
    this.readonly = true;
    this._listContributors = value;
    this.mode = DepositTableContributorModeEnum.contributors;
    this._initiateFormArrayWithContributors(this.listContributors);
    this._displayWarningContributorImportedPartially();
    this.formArray?.disable();
  }

  get listContributors(): DepositFormAbstractContributor[] {
    return this._listContributors;
  }

  get listContributorRoles(): PublicationSubtypeContributorRoleDTO[] {
    const contributorRoles = MemoizedUtil.selectSnapshot(this._store, AppContributorRolesState, state => state.map);
    const publicationSubTypeId = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.publicationSubTypeId);
    return MappingObjectUtil.get(contributorRoles, publicationSubTypeId);
  }

  _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private readonly _valueBS: BehaviorSubject<any[] | undefined> = new BehaviorSubject<any[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<any[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  protected readonly _moveToCollaboratorsBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("moveToCollaborators")
  readonly moveToCollaboratorsObs: Observable<void> = ObservableUtil.asObservable(this._moveToCollaboratorsBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(formControl: AbstractControl, key: string): AbstractControl {
    return formControl.get(key);
  }

  isRequired(formControl: AbstractControl, key: string): boolean {
    const errors = formControl.get(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService,
              private readonly _orcidService: OrcidService,
              private readonly _dialog: MatDialog) {
    super();
  }

  private _displayWarningContributorImportedPartially(): void {
    const limitMaxContributor = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current.limitMaxContributors);
    if (this.notifyContributorsFromExternalSource && this.formArray?.controls?.length >= limitMaxContributor) {
      this._store.dispatch(new DepositAction.DisplayDialogContributorsPartiallyImported());
    }
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.searchContributorInfosMap = {} as MappingObject<string, DepositTableContributorSearchContainer>;

    if (isNullOrUndefined(this.formArray)) {
      this.readonly = true;
      this.disabled = true;
      this._initiateFormArrayWithContributors(this.listContributors);
      setTimeout(() => {
        this.formArray.disable();
        this.formArray.controls.forEach(c => c.disable());
      });
    } else {
      this.formArray.controls.forEach(control => {
        this._addSubscriptionOnContributor(control as FormGroup);
      });
    }

    if (this._INIT_WITH_ONE_PERSON && this.formArray.length === 0) {
      this.formArray.push(this.createContributor(DepositTableContributorTypeEnum.external));
    }
    this._displayWarningContributorImportedPartially();
  }

  private _initiateFormArrayWithContributors(listContributors: DepositFormAbstractContributor[]): void {
    this.formArray = this._fb.array([]);
    listContributors.forEach(contributor => {
      const contributorForm = DepositTableContributorContainer.restoreContributorFromAbstractContributor(this._fb, this.formDefinition, contributor);
      this.formArray.push(contributorForm);
    });
  }

  externalDetectChanges(): void {
    this._changeDetector.detectChanges();
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }

  getGuid(form: FormGroup): string {
    return form.get(this.formDefinition.guid).value;
  }

  getType(form: FormGroup): string {
    return form.get(this.formDefinition.type).value;
  }

  getFirstName(form: FormGroup): string {
    return form.get(this.formDefinition.firstname).value;
  }

  getLastName(form: FormGroup): string {
    return form.get(this.formDefinition.lastname).value;
  }

  getCnIndividu(form: FormGroup): string {
    return form.get(this.formDefinition.cnIndividu).value;
  }

  getInstitution(form: FormGroup): string {
    return form.get(this.formDefinition.institution).value;
  }

  getStructure(form: FormGroup): string {
    return form.get(this.formDefinition.structure).value;
  }

  getPotentiallyUnige(form: FormGroup): string {
    return form.get(this.formDefinition.potentiallyUnige).value;
  }

  displaySearchLine(form: FormGroup): boolean {
    return this.isUnigeMember(form) && isNullOrUndefined(this.getCnIndividu(form));
  }

  displayAdditionalPersonInfo(f: FormGroup): boolean {
    const isPotentiallyUnige = isTrue(this.getPotentiallyUnige(f));
    const structure = this.getStructure(f);
    return (isPotentiallyUnige || isNonEmptyString(structure)) && this.getType(f) !== DepositTableContributorTypeEnum.collaboration;
  }

  getDepositTableContributorSearchContainer(form: FormGroup): DepositTableContributorSearchContainer {
    return MappingObjectUtil.get(this.searchContributorInfosMap, this.getGuid(form));
  }

  isUnigeMember(form: FormGroup): boolean {
    return this.getType(form) === DepositTableContributorTypeEnum.unige;
  }

  isCollaboration(form: FormGroup): boolean {
    return this.getType(form) === DepositTableContributorTypeEnum.collaboration;
  }

  get typeEnum(): typeof DepositTableContributorTypeEnum {
    return DepositTableContributorTypeEnum;
  }

  static getFormGroupContributor(_fb: FormBuilder,
                                 formDefinition: FormComponentFormDefinitionDepositFormContributor,
                                 defaultType: DepositTableContributorTypeEnum): FormGroup {
    return _fb.group({
      [formDefinition.guid]: [undefined, [Validators.required]],
      [formDefinition.institution]: [undefined, []],
      [formDefinition.firstname]: [undefined],
      [formDefinition.lastname]: [undefined, [Validators.required]],
      [formDefinition.role]: [undefined, [Validators.required]],
      [formDefinition.orcid]: [undefined, []],
      [formDefinition.email]: [undefined, []],
      [formDefinition.cnIndividu]: [undefined, []],
      [formDefinition.structure]: [undefined, []],
      [formDefinition.type]: [defaultType, [DepositTableContributorContainer._validatorValidType]],
      [formDefinition.potentiallyUnige]: [undefined, []],
    });
  }

  private static _validatorValidType(control: FormControl): { [key: string]: any } | null {
    const value = control.value;
    if (value === DepositTableContributorTypeEnum.undefined) {
      return {invalid: value};
    }
    return null;
  }

  static restoreContributorFromAbstractContributor(_fb: FormBuilder,
                                                   formDefinition: FormComponentFormDefinitionDepositFormContributor,
                                                   abstractContributor: DepositFormAbstractContributor,
                                                   defaultType: DepositTableContributorTypeEnum = DepositTableContributorTypeEnum.external,
                                                   defaultRole: Enums.Deposit.RoleContributorEnum = Enums.Deposit.RoleContributorEnum.author): FormGroup {
    const form = this.getFormGroupContributor(_fb, formDefinition, defaultType);

    if (isNotNullNorUndefined(abstractContributor.collaboration)) {
      form.get(formDefinition.lastname).setValue(abstractContributor.collaboration.name);
      form.get(formDefinition.type).setValue(DepositTableContributorTypeEnum.collaboration);
    } else if (isNotNullNorUndefined(abstractContributor.contributor)) {
      const contributor = abstractContributor.contributor;

      form.get(formDefinition.type).setValue(isNonEmptyString(contributor.cnIndividu) ? DepositTableContributorTypeEnum.unige : defaultType);
      form.get(formDefinition.firstname).setValue(contributor.firstname);
      form.get(formDefinition.lastname).setValue(contributor.lastname);
      form.get(formDefinition.institution).setValue(contributor.institution);
      form.get(formDefinition.structure).setValue(contributor.structure);
      form.get(formDefinition.role).setValue(isNonEmptyString(contributor.role) ? contributor.role : defaultRole);
      form.get(formDefinition.orcid).setValue(contributor.orcid);
      form.get(formDefinition.email).setValue(contributor.email);
      form.get(formDefinition.cnIndividu).setValue(contributor.cnIndividu);
      if (isNonEmptyString(contributor.institution) && contributor.institution.match(this.probablyUnigeInstitutionRegex)) {
        form.get(formDefinition.potentiallyUnige).setValue(true);
      }
    }

    return form;
  }

  createContributor(type: DepositTableContributorTypeEnum): FormGroup {
    this._removeFirstEntryIfEmpty();

    const form = DepositTableContributorContainer.getFormGroupContributor(this._fb, this.formDefinition, type);

    this._addSubscriptionOnContributor(form);

    if (this.mode === DepositTableContributorModeEnum.collaborationMembers) {
      form.get(this.formDefinition.role).setValue(Enums.Deposit.RoleContributorEnum.collaborator);
      return form;
    }

    let defaultRole = this.defaultContributorRole;
    if (isNullOrUndefined(defaultRole)) {
      const listRole = this.listContributorRoles;
      if (isNonEmptyArray(listRole)) {
        defaultRole = listRole[0].value;
      } else {
        defaultRole = Enums.Deposit.RoleContributorEnum.author;
      }
    }
    form.get(this.formDefinition.role).setValue(defaultRole);
    return form;
  }

  private _removeFirstEntryIfEmpty(): void {
    // check if there is member empty before adding a new one
    if (this.formArray.length === 1
      && isNullOrUndefinedOrWhiteString(this.formArray.at(0).get(this.formDefinition.firstname)?.value)
      && isNullOrUndefinedOrWhiteString(this.formArray.at(0).get(this.formDefinition.lastname)?.value)) {
      this.formArray.removeAt(0);
    }
  }

  private _addSubscriptionOnContributor(form: FormGroup): void {
    form.get(this.formDefinition.guid).setValue(Guid.MakeNew().guid);

    const formControlType = form.get(this.formDefinition.type);
    const formControlFirstname = form.get(this.formDefinition.firstname);
    const formControlLastname = form.get(this.formDefinition.lastname);
    const formControlStructure = form.get(this.formDefinition.structure);
    const formControlCnIndividu = form.get(this.formDefinition.cnIndividu);
    const formControlOrcid = form.get(this.formDefinition.orcid);
    const formControlRole = form.get(this.formDefinition.role);

    if (this.mode === DepositTableContributorModeEnum.collaborationMembers) {
      formControlRole.setValue(Enums.Deposit.RoleContributorEnum.collaborator);
    }

    this.subscribe(formControlType.valueChanges.pipe(
      distinctUntilChanged(),
      tap(type => {
        if (type === DepositTableContributorTypeEnum.unige) {
          formControlCnIndividu.setValidators([Validators.required]);
          formControlRole.setValidators([Validators.required]);
        } else {
          formControlCnIndividu.setValue(undefined);
          formControlStructure.setValue(undefined);
          formControlFirstname.enable();
          formControlLastname.enable();
          formControlCnIndividu.setValidators([]);

          if (type === DepositTableContributorTypeEnum.collaboration) {
            formControlFirstname.setValue(undefined);
            formControlRole.setValue(undefined);
            formControlOrcid.setValue(undefined);
            formControlStructure.setValue(undefined);
            formControlFirstname.setValidators([]);
            formControlRole.setValidators([]);
          } else {
            formControlRole.setValidators([Validators.required]);
          }
        }
        formControlFirstname.updateValueAndValidity();
        formControlRole.updateValueAndValidity();
        formControlCnIndividu.updateValueAndValidity();
      }),
    ));

    // HACK TO ALLOW TO HAVE COMBINE LATEST WORK FIRST TIME
    formControlFirstname.setValue(formControlFirstname.value);
    formControlLastname.setValue(formControlLastname.value);
    formControlType.setValue(formControlType.value);
  }

  private static _cancelSubscriptionOnSearchResult(subscriptionSuccess: Subscription, subscriptionFail: Subscription): void {
    if (isNotNullNorUndefined(subscriptionSuccess) && !subscriptionSuccess.closed) {
      subscriptionSuccess.unsubscribe();
    }
    if (isNotNullNorUndefined(subscriptionFail) && !subscriptionFail.closed) {
      subscriptionFail.unsubscribe();
    }
  }

  addContributor(): void {
    const contributorFormGroup = this.createContributor(DepositTableContributorTypeEnum.external);
    this.formArray.push(contributorFormGroup);
  }

  addCollaboration(): void {
    const contributorFormGroup = this.createContributor(DepositTableContributorTypeEnum.collaboration);
    this.formArray.push(contributorFormGroup);
  }

  addMeAsAuthor(): void {
    if (isNullOrUndefined(this.userConnected.person)) {
      throw Error("UserConnected person not provided");
    }
    const formGroup = this.createContributor(DepositTableContributorTypeEnum.external);
    this.formArray.push(formGroup);
    formGroup.get(this.formDefinition.firstname).setValue(this.userConnected.person?.firstName);
    formGroup.get(this.formDefinition.lastname).setValue(this.userConnected.person?.lastName);
    if (UserUtil.isUnigeUser(this.userConnected)) {
      const cnIndividu = UserUtil.getCnIndividu(this.userConnected);
      formGroup.get(this.formDefinition.cnIndividu).setValue(cnIndividu);
      formGroup.get(this.formDefinition.type).setValue(DepositTableContributorTypeEnum.unige);
    } else {
      formGroup.get(this.formDefinition.type).setValue(DepositTableContributorTypeEnum.external);
    }
  }

  isPersonConnectedAlreadyAdded(): boolean {
    return this.formArray.controls.findIndex(c => c.get(this.formDefinition.firstname)?.value === this.userConnected.firstName &&
      c.get(this.formDefinition.lastname)?.value === this.userConnected.lastName) !== -1;
  }

  delete(index: number): void {
    this._removeDuplicateCnIndividuErrorIfNotMoreApplicable(index);
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  private _removeDuplicateCnIndividuErrorIfNotMoreApplicable(index: number): void {
    if (this.formArray.invalid) {
      const currentCnIndividuDeleted = this.formArray.controls[index].get(this.formDefinition.cnIndividu)?.value;
      if (isNotNullNorUndefinedNorWhiteString(currentCnIndividuDeleted)) {
        const listControlsWithDuplicateCnIndividu = this.formArray.controls.filter((control, i) =>
          i !== index
          && control.invalid
          && control.get(this.formDefinition.cnIndividu)?.value === currentCnIndividuDeleted
          && (control.get(this.formDefinition.cnIndividu)?.errors as FormError)?.errorsFromBackend.indexOf(this._ERROR_DUPLICATE_CN_INDIVIDU) >= 0);
        listControlsWithDuplicateCnIndividu.forEach(control => {
          const formError = control.get(this.formDefinition.cnIndividu).errors as FormError;
          const indexOf = formError.errorsFromBackend.indexOf(this._ERROR_DUPLICATE_CN_INDIVIDU);
          formError.errorsFromBackend.splice(indexOf, 1);
          if (formError.errorsFromBackend.length === 0 && MappingObjectUtil.size(formError as any) === 1) {
            control.get(this.formDefinition.cnIndividu).setErrors(undefined);
          }
        });
      }
    }
  }

  contributorUnigeValueSelected(formGroup: FormGroup, selectedContributor: ContributorRockSolid): void {
    if (isNotNullNorUndefined(selectedContributor)) {
      this.confirmCollaboratorUnige(formGroup, selectedContributor);
    } else {
      this.cancelCollaboratorUnige(formGroup);
    }
  }

  confirmCollaboratorUnige(formGroup: FormGroup, selectedContributor: ContributorRockSolid): void {
    formGroup.get(this.formDefinition.cnIndividu).setValue(selectedContributor.cnIndividu);
    const formControlFirstname = formGroup.get(this.formDefinition.firstname);
    const formControlLastname = formGroup.get(this.formDefinition.lastname);
    const formControlStructure = formGroup.get(this.formDefinition.structure);
    formControlFirstname.setValue(selectedContributor.firstname);
    formControlLastname.setValue(selectedContributor.lastname);
    formControlStructure.setValue(selectedContributor.structure);
  }

  cancelCollaboratorUnige(formGroup: FormGroup): void {
    formGroup.get(this.formDefinition.type).setValue(null);
  }

  get getFormArrayControl(): FormGroup[] {
    return this.formArray.controls as FormGroup[];
  }

  goToOrcid(orcid: string): void {
    this._orcidService.openOrcidPage(orcid);
  }

  dropToSort(event: CdkDragDrop<any>): void {
    const item = this.formArray.at(event.previousIndex);
    let indexRemove = event.previousIndex;
    let indexAdd = event.currentIndex;
    if (indexRemove > event.currentIndex) {
      indexRemove++;
    } else {
      indexAdd++;
    }
    this.formArray.insert(indexAdd, item);
    this.formArray.removeAt(indexRemove);
  }

  allUnigeMember(): void {
    this.formArray.controls
      .filter(formGroup => formGroup.get(this.formDefinition.type).value !== DepositTableContributorTypeEnum.collaboration)
      .forEach(formGroup => {
        formGroup.get(this.formDefinition.type).setValue(DepositTableContributorTypeEnum.unige);
      });
  }

  nobodyUnigeMember(): void {
    this.formArray.controls
      .filter(formGroup => formGroup.get(this.formDefinition.type).value !== DepositTableContributorTypeEnum.collaboration)
      .forEach(formGroup => {
        formGroup.get(this.formDefinition.type).setValue(DepositTableContributorTypeEnum.external);
      });
  }

  changeAllContributorIntoCollaborator(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: LabelTranslateEnum.moveUnigeMemberToCollaborators,
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.contributors.moveToCollaborators.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.primary,
      colorCancel: ButtonColorEnum.primary,
    }, undefined, isConfirmed => this._moveToCollaboratorsBS.next()));
  }

  getNobodySelectedMessage(): string {
    if (this.mode === DepositTableContributorModeEnum.contributors) {
      return LabelTranslateEnum.noContributorsEntered;
    }
    if (this.mode === DepositTableContributorModeEnum.contributors) {
      return LabelTranslateEnum.noCollaborationMembersEntered;
    }
    return "";
  }

  refreshLastnameFirstnameInput(): void {
    // Hack that allow problem input cross update after dragging
    this.displayLastNameFirstName = false;
    setTimeout(() => {
      this.displayLastNameFirstName = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  atLeastTheRole(roleRequired: Enums.Deposit.RoleContributorEnum): boolean {
    const atLeastTheRole = this.formArray.controls.findIndex(c => c.get(this.formDefinition.role)?.value === roleRequired) !== -1;
    const key = "missing_role_" + roleRequired;
    if (isFalse(atLeastTheRole)) {
      ValidationErrorHelper.addError(this.formArray, key);
    } else {
      ValidationErrorHelper.removeError(this.formArray, key);
    }
    return atLeastTheRole;
  }

  toggleUnigeMember($event: MatSlideToggleChange, formControl: FormControl, formGroup: FormGroup): void {
    formControl.setValue($event.checked ? DepositTableContributorTypeEnum.unige : DepositTableContributorTypeEnum.external);
    const formControlFirstname = formGroup.get(this.formDefinition.firstname);
    if ($event.checked) {
      formControlFirstname.setValidators([Validators.required]);
    } else {
      formControlFirstname.setValidators([]);
    }
    formControlFirstname.updateValueAndValidity();
  }

  isDragging: boolean;

  startDragging(): void {
    this.isDragging = true;
  }

  endDragging(): void {
    this.isDragging = false;
  }

  registerDepositTableContributorSearchContainer(form: FormGroup, depositTableContributorSearchContainer: DepositTableContributorSearchContainer): void {
    MappingObjectUtil.set(this.searchContributorInfosMap, this.getGuid(form), depositTableContributorSearchContainer);
    this._changeDetector.detectChanges();
  }
}

export type DepositTableContributorModeEnum = "collaborationMembers" | "contributors";
export const DepositTableContributorModeEnum = {
  collaborationMembers: "collaborationMembers" as DepositTableContributorModeEnum,
  contributors: "contributors" as DepositTableContributorModeEnum,
};

export type DepositTableContributorTypeEnum = "unige" | "collaboration" | "undefined" | "external";
export const DepositTableContributorTypeEnum = {
  unige: "unige" as DepositTableContributorTypeEnum,
  collaboration: "collaboration" as DepositTableContributorTypeEnum,
  // eslint-disable-next-line id-blacklist
  undefined: "undefined" as DepositTableContributorTypeEnum,
  external: "external" as DepositTableContributorTypeEnum,
};
