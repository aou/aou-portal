/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-comments.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
} from "@angular/core";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositCommentFormPresentational} from "@deposit/components/presentationals/deposit-comment-form/deposit-comment-form.presentational";
import {DepositCommentValidatorAction} from "@deposit/stores/comment-validator/deposit-comment-validator.action";
import {DepositCommentValidatorState} from "@deposit/stores/comment-validator/deposit-comment-validator.state";
import {DepositCommentAction} from "@deposit/stores/comment/deposit-comment.action";
import {DepositCommentState} from "@deposit/stores/comment/deposit-comment.state";
import {Comment} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {Observable} from "rxjs/index";
import {
  isNonEmptyArray,
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-comments-container",
  templateUrl: "./deposit-comments.container.html",
  styleUrls: ["deposit-comments.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCommentsContainer extends SharedAbstractContainer {
  @Input()
  isValidationMode: boolean = false;

  @Input()
  depositResId: string;

  @ViewChild("depositCommentFormPresentational")
  depositCommentFormPresentational: DepositCommentFormPresentational;

  @ViewChild("depositCommentValidatorFormPresentational")
  depositCommentValidatorFormPresentational: DepositCommentFormPresentational;

  listCommentObs: Observable<Comment[]> = MemoizedUtil.list(this._store, DepositCommentState);
  isLoadingCommentObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositCommentState);
  listCommentValidatorObs: Observable<Comment[]> = MemoizedUtil.list(this._store, DepositCommentValidatorState);
  isLoadingCommentValidatorObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositCommentValidatorState);

  constructor(private readonly _store: Store) {
    super();
  }

  createUpdateComment(modelFormControlEvent: Comment): void {
    modelFormControlEvent.publication = {
      resId: this.depositResId,
    };
    if (isNullOrUndefined(modelFormControlEvent.resId)) {
      this._store.dispatch(new DepositCommentAction.Create(this.depositResId, modelFormControlEvent));
    } else {
      this._store.dispatch(new DepositCommentAction.Update(this.depositResId, modelFormControlEvent));
    }
  }

  deleteComment(modelFormControlEvent: Comment): void {
    this._store.dispatch(new DepositCommentAction.Delete(this.depositResId, modelFormControlEvent.resId));
  }

  createUpdateCommentValidator(modelFormControlEvent: Comment): void {
    modelFormControlEvent.publication = {
      resId: this.depositResId,
    };
    if (isNullOrUndefined(modelFormControlEvent.resId)) {
      this._store.dispatch(new DepositCommentValidatorAction.Create(this.depositResId, modelFormControlEvent));
    } else {
      this._store.dispatch(new DepositCommentValidatorAction.Update(this.depositResId, modelFormControlEvent));
    }
  }

  deleteCommentValidator(modelFormControlEvent: Comment): void {
    this._store.dispatch(new DepositCommentValidatorAction.Delete(this.depositResId, modelFormControlEvent.resId));
  }

  needToCheckComment(comments: Comment[]): boolean {
    const currentUserResId = MemoizedUtil.selectSnapshot(this._store, AppUserState, state => state.current?.resId);
    return isNonEmptyArray(comments) && this.isValidationMode && comments.findIndex(c => isNullOrUndefined(c.person) || c.person.resId !== currentUserResId) !== 0;
  }
}
