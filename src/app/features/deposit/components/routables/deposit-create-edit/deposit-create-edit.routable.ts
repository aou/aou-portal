/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-create-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {
  ActivatedRoute,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import {GetMetadataFromServiceInfo} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {DepositFormPresentational} from "@app/features/deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositFormRuleHelper} from "@app/features/deposit/helpers/deposit-form-rule.helper";
import {DepositDefaultData} from "@app/features/deposit/models/deposit-form-definition.model";
import {ModelFormControlEventDeposit} from "@app/features/deposit/models/model-form-control-event-deposit.model";
import {DepositAction} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {DepositDocumentFileState} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {SharedDepositSubSubtypeState} from "@app/shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.state";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DepositSubSubtype,
  DepositSubtype,
  Language,
  MetadataDifference,
  Romeo,
  Structure,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {SharedDepositSubtypeState} from "@shared/stores/deposit-subtype/shared-deposit-subtype.state";
import {SharedDepositSubSubtypeAction} from "@shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.action";
import {SharedExternalDataAction} from "@shared/stores/external-data/shared-external-data.action";
import {SharedExternalDataState} from "@shared/stores/external-data/shared-external-data.state";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AppBannerAction,
  BackTranslatePipe,
  BannerColorEnum,
  cleanHtml,
  FORM_CONTROL_ELEMENT_REF,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  SOLIDIFY_CONSTANTS,
  StateModel,
  StoreUtil,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-create-edit-routable",
  templateUrl: "./deposit-create-edit.routable.html",
  styleUrls: ["./deposit-create-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCreateEditRoutable extends SharedAbstractRoutable implements OnInit, AfterViewInit {
  @Select(DepositState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(DepositState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  @Select((state: StateModel) => state.router.state) urlStateObs: Observable<RouterStateSnapshot>;
  isLoadingGetMetadataServiceObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedExternalDataState);

  currentStep: Enums.Deposit.StepEnum;

  currentObs: Observable<Deposit> = MemoizedUtil.current(this._store, DepositState);
  listDepositSubtypeObs: Observable<DepositSubtype[]> = MemoizedUtil.list(this._store, SharedDepositSubtypeState);
  listDepositSubSubtypeObs: Observable<DepositSubSubtype[]> = MemoizedUtil.list(this._store, SharedDepositSubSubtypeState);
  listStructureObs: Observable<Structure[]> = MemoizedUtil.list(this._store, SharedStructureState);
  listLanguagesObs: Observable<Language[]> = MemoizedUtil.list(this._store, SharedLanguageState);
  isLoadingStructureObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedStructureState);
  defaultDataObs: Observable<DepositDefaultData> = MemoizedUtil.select(this._store, DepositState, state => state.defaultData);
  isFilePresentObs: Observable<boolean> = MemoizedUtil.select(this._store, DepositDocumentFileState, state => isNonEmptyArray(state.list));
  listCurrentStatusObs: Observable<MappingObject<Enums.DocumentFile.StatusEnum, number>> = MemoizedUtil.select(this._store, DepositDocumentFileState, state => state.listCurrentStatus);
  romeoObs: Observable<Romeo> = MemoizedUtil.select(this._store, SharedExternalDataState, state => state.romeo);
  userConnectedObs: Observable<User> = MemoizedUtil.current(this._store, AppUserState);
  metadataDifferencesObs: Observable<MappingObject<string, MappingObject<string, MetadataDifference[]>>> = MemoizedUtil.select(this._store, DepositState, state => state.metadataDifferences);

  @HostBinding("class.is-edit")
  resId: string | undefined;

  depositMode: DepositModeEnum | undefined;

  get depositModeEnum(): typeof DepositModeEnum {
    return DepositModeEnum;
  }

  isValidationMode: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: DepositFormPresentational;

  defaultContributorRole: Enums.Deposit.RoleContributorEnum = Enums.Deposit.RoleContributorEnum.author;
  backTranslatePipe: BackTranslatePipe;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _router: Router,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super();
    this.backTranslatePipe = new BackTranslatePipe(this._store, environment);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.computeIsValidationMode();
    this.subscribe(this.retrieveCurrentStepObs());
    this.retrieveResIdFromUrl();
    this.depositMode = isNotNullNorUndefined(this.resId) ? DepositModeEnum.edit : DepositModeEnum.create;
    this._store.dispatch(new DepositAction.GetMyDefaultProfileData());
    if (this.depositMode === DepositModeEnum.create) {
      this._store.dispatch(new DepositAction.LoadResource(true));
    }
    if (this.depositMode === DepositModeEnum.edit) {
      this._store.dispatch(new DepositAction.GetById(this.resId));
    }
    this._changeDetector.detectChanges();
  }

  scrollToFieldIfDefined(form: FormGroup): void {
    if (UrlQueryParamHelper.currentUrlContainsQueryParam(environment.fieldPathQueryParam)) {
      const mapping = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
      const fieldPath = MappingObjectUtil.get(mapping, environment.fieldPathQueryParam);
      const fc = form.get(fieldPath);
      if (isNullOrUndefined(fc) || isNullOrUndefined(fc[FORM_CONTROL_ELEMENT_REF])) {
        return;
      }
      const elementRef = fc[FORM_CONTROL_ELEMENT_REF] as ElementRef;
      elementRef.nativeElement.scrollIntoView({behavior: "smooth", block: "center"});
    }
  }

  private computeIsValidationMode(): boolean {
    this.isValidationMode = this._router.url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + AppRoutesEnum.depositToValidate + SOLIDIFY_CONSTANTS.URL_SEPARATOR);
    return this.isValidationMode;
  }

  protected retrieveResIdFromUrl(): void {
    this.resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this.resId)) {
      this.resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  private _computeDefaultRole(currentType: Enums.Deposit.DepositFriendlyNameSubTypeEnum): void {
    const defaultContributorRoles: MappingObject<Enums.Deposit.DepositFriendlyNameSubTypeEnum, Enums.Deposit.RoleContributorEnum> = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current.defaultContributorRoles);
    this.defaultContributorRole = MappingObjectUtil.get(defaultContributorRoles, currentType) ?? Enums.Deposit.RoleContributorEnum.author;
  }

  subtypeChange(subtypeId: string | undefined): void {
    if (isNullOrUndefined((subtypeId))) {
      this._store.dispatch(new SharedDepositSubSubtypeAction.Clean());
    } else {
      this._updateBanner();
      this._store.dispatch(new SharedDepositSubSubtypeAction.GetAll(subtypeId));
      this._store.dispatch(new DepositAction.UpdatePublicationSubTypeDocumentId(subtypeId));
      this._computeDefaultRole(subtypeId as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
    }
  }

  titleChange(title: string | undefined): void {
    this._updateBanner();
  }

  private _updateBanner(): void {
    if (this.depositMode === DepositModeEnum.create || isNullOrUndefined(this.formPresentational)) {
      return;
    }
    const subtypeId = this.formPresentational.form.get(DepositFormRuleHelper.pathSubType)?.value;
    const title = this.formPresentational.form.get(DepositFormRuleHelper.pathTitleText)?.value;

    const listSubType = MemoizedUtil.listSnapshot(this._store, SharedDepositSubtypeState);
    const subtype = listSubType?.find(s => s.resId === subtypeId);
    if (isNotNullNorUndefined(subtype)) {
      const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
      const message = isNullOrUndefinedOrWhiteString(deposit?.importSource) ? LabelTranslateEnum.youAreInEditModeOnADepositOfTypeX : LabelTranslateEnum.youAreInEditModeOnADepositOfTypeXViaIdentifier;
      const name = this.backTranslatePipe.transform(subtype?.labels);
      this._store.dispatch(new AppBannerAction.Show(message, BannerColorEnum.warn, {
        name: name,
        identifier: deposit?.importSource,
        title: cleanHtml(title),
      }));
    }
  }

  getMetadataFromServiceChange(getMetadataFromServiceInfo: GetMetadataFromServiceInfo): void {
    const action = new SharedExternalDataAction.GetMetadataFromService(getMetadataFromServiceInfo);
    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(SharedExternalDataAction.GetMetadataFromServiceSuccess),
      filter(result => result.result.successful && (result.action as SharedExternalDataAction.GetMetadataFromServiceSuccess).parentAction === action),
      take(1),
      tap(result => {
        const depositFormModel = (result.action as SharedExternalDataAction.GetMetadataFromServiceSuccess).depositFormModel;
        this.formPresentational.applyMetadataFromService(depositFormModel, getMetadataFromServiceInfo.importSource);
      }),
    ));

    this._store.dispatch(action);
  }

  backToDetail(): void {
    let base = RoutesEnum.depositDetail;
    if (this.isValidationMode) {
      base = RoutesEnum.depositToValidateDetail;
    }
    this._store.dispatch(new Navigate([base, this.resId]));
  }

  backToList(): void {
    this._store.dispatch(new Navigate(this.isValidationMode ? [RoutesEnum.depositToValidateDetail, this.resId] : [RoutesEnum.deposit]));
  }

  create($event: ModelFormControlEventDeposit): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new DepositAction.Create($event),
      DepositAction.CreateSuccess, action => {},
      DepositAction.CreateFail, action => {
        this.formPresentational.isLoadingOnCreate = false;
        this.formPresentational.externalDetectChanges();
      }));
  }

  update($event: ModelFormControlEventDeposit): void {
    const targetRoutingAfterSave = $event.model.targetRoutingAfterSave;
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositAction.Update($event),
      DepositAction.UpdateSuccess,
      () => {
        const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
        if ($event.model.formStep === Enums.Deposit.StepEnum.DESCRIPTION
          && targetRoutingAfterSave === Enums.Deposit.TargetRoutingAfterSaveEnum.NEXT
          && (deposit?.status === Enums.Deposit.StatusEnum.IN_EDITION || deposit?.status === Enums.Deposit.StatusEnum.UPDATES_VALIDATION)
        ) {
          // it is going to navigate to summary
          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
            new DepositAction.CheckMetadataDifferences($event.model.resId),
            DepositAction.CheckMetadataDifferencesSuccess,
            () => {
              this._changeDetector.detectChanges();
            }));
        } else if (targetRoutingAfterSave === Enums.Deposit.TargetRoutingAfterSaveEnum.SAVE_AND_SUBMIT) {
          this.formPresentational.submit();
        }
      }));
  }

  submitForValidation($event: ModelFormControlEventDeposit): void {
    const validationStructureId = $event.model.validationStructureId;
    delete $event.model.validationStructureId;
    this._store.dispatch(new DepositAction.SubmitForValidation($event.model.resId, validationStructureId));
  }

  protected retrieveCurrentStepObs(): Observable<Enums.Deposit.StepEnum> {
    return this.urlStateObs.pipe(
      map(url => url.url),
      distinctUntilChanged(),
      map(urlRaw => {
        const urlSplitted = urlRaw.split("?");
        const url = urlSplitted[0];
        this.currentStep = undefined;

        if (DepositCreateEditRoutable._isEndWithEditRoute(url)) {
          this.currentStep = Enums.Deposit.StepEnum.FILES;
        } else {
          const indexLastSeparator = url.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
          const stepName = url.substring(indexLastSeparator + 1, url.length);
          this.currentStep = Enums.Deposit.StepEnum[stepName];
        }
        this._changeDetector.detectChanges();
        return this.currentStep;
      }),
    );
  }

  private static _isEndWithEditRoute(url: string): boolean {
    return url.endsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + environment.routeSegmentEdit);
  }

  navigateToStep(targetStep: Enums.Deposit.StepEnum): void {
    this._store.dispatch(new Navigate([this.isValidationMode ? RoutesEnum.depositToValidateDetail : RoutesEnum.depositDetail, this.resId, DepositRoutesEnum.edit, targetStep]));
  }

  retrieveRomeo(issn: string): void {
    this._store.dispatch(new SharedExternalDataAction.GetRomeoByIssn(issn));
  }

  navigate($event: Navigate): void {
    this._store.dispatch($event);
  }

  refreshValidationError(): void {
    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
    this._store.dispatch(new DepositAction.ValidateMetadata(deposit.resId, this.isValidationMode && deposit.status === Enums.Deposit.StatusEnum.IN_VALIDATION));
  }
}

export enum DepositModeEnum {
  create = "create",
  edit = "edit",
}
