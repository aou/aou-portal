/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  AbstractControlOptions,
  FormControl,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {DepositBulkResearchGroupDialog} from "@app/features/deposit/components/dialogs/deposit-bulk-research-group/deposit-bulk-research-group.dialog";
import {DepositBulkStructureDialog} from "@app/features/deposit/components/dialogs/deposit-bulk-structure/deposit-bulk-structure.dialog";
import {DepositCommentDialog} from "@app/features/deposit/components/dialogs/deposit-comment/deposit-comment.dialog";
import {
  importIdentifierQueryParam,
  ImportMethodEnum,
  importMethodQueryParam,
} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {DepositRedirectTargetEnum} from "@app/features/deposit/enums/deposit-redirect-target.enum";
import {DepositHelper} from "@app/features/deposit/helpers/deposit.helper";
import {DepositCommentValidatorAction} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.action";
import {DepositCommentValidatorState} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.state";
import {DepositCommentAction} from "@app/features/deposit/stores/comment/deposit-comment.action";
import {DepositCommentState} from "@app/features/deposit/stores/comment/deposit-comment.state";
import {
  DepositAction,
  depositActionNameSpace,
} from "@app/features/deposit/stores/deposit.action";
import {
  defaultDepositStateQueryParameters,
  DepositState,
  DepositStateModel,
} from "@app/features/deposit/stores/deposit.state";
import {DepositStatusHistoryState} from "@app/features/deposit/stores/status-history/deposit-status-history.state";
import {AppState} from "@app/stores/app.state";
import {AppDepositSubtypeState} from "@app/stores/deposit-subtype/app-deposit-subtype.state";
import {DepositBulkLanguageDialog} from "@deposit/components/dialogs/deposit-bulk-language/deposit-bulk-language.dialog";
import {depositStatusHistoryNamespace} from "@deposit/stores/status-history/deposit-status-history.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Comment,
  Deposit,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {DepositListHelper} from "@shared/helpers/deposit-list.helper";
import {SecurityService} from "@shared/services/security.service";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {DoiCleaner} from "@shared/validators/doi.validator";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  ActionSubActionCompletionsWrapper,
  AppBannerAction,
  BannerColorEnum,
  ButtonColorEnum,
  ButtonThemeEnum,
  ConfirmDialog,
  ConfirmDialogInputEnum,
  CookieConsentUtil,
  CookieType,
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  DialogUtil,
  isFalse,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isObservable,
  isTrue,
  KeyValue,
  LocalStorageHelper,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ObjectUtil,
  ObservableOrPromiseOrValue,
  ofSolidifyActionCompleted,
  OrderEnum,
  PollingHelper,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
  StoreUtil,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-list-routable",
  templateUrl: "./deposit-list.routable.html",
  styleUrls: ["./deposit-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositListRoutable extends AbstractListRoutable<Deposit, DepositStateModel> implements OnInit, OnDestroy {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.createDeposit;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof Deposit & string = "title";

  private readonly _STATUS_KEY: keyof Deposit = "status";
  private readonly _STATUS_LIST_KEY: string = "statusList";
  protected _allDeposits: boolean = false;

  listTabs: Tab[];
  showAsContributor: boolean = false;

  tabListCountersObs: Observable<MappingObject<DepositListTabEnum, number>> = MemoizedUtil.select(this._store, DepositState, state => state.tabListCounters);
  currentTabObs: Observable<DepositListTabEnum> = MemoizedUtil.select(this._store, DepositState, state => state.currentTab);

  formControlIdentifier: FormControl = new FormControl(undefined, {
    updateOn: "change",
    validators: [DoiCleaner],
  } as AbstractControlOptions);

  override skipInitialQuery: boolean = true;

  override columnsSkippedToClear: string[] = [this._STATUS_LIST_KEY, DepositState._KEY_SHOW_ALL_DEPOSITS, DepositState._KEY_SHOW_AS_CONTRIBUTOR];

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<Deposit>;

  private readonly _INTERVAL_REFRESH_IN_SECOND: number = environment.refreshDepositListTabStatusCounterIntervalInSecond;

  private _columnStatus: DataTableColumns<Deposit> = {
    field: this._STATUS_KEY,
    header: LabelTranslateEnum.status,
    type: DataTableFieldTypeEnum.singleSelect,
    order: OrderEnum.none,
    isFilterable: true,
    isSortable: true,
    translate: true,
    filterEnum: Enums.Deposit.StatusEnumTranslate.filter(s => isFalse(s.hideInFilter) || isNullOrUndefined(s.hideInFilter)) as KeyValue[],
    component: DataTableComponentHelper.get(DataTableComponentEnum.status),
    maxWidth: "160px",
  };

  listDepositWithMetadataObs: Observable<Deposit[]> = MemoizedUtil.list(this._store, DepositState).pipe(
    filter(listDeposit => isNotNullNorUndefined(listDeposit)),
    map((listDeposit: Deposit[]) => DepositListHelper.improveListDepositWithMetadata(listDeposit)),
  );

  isValidationMode: boolean = false;

  private _displayConditionBulkEdit: (list: Deposit[]) => ObservableOrPromiseOrValue<boolean> = (list: Deposit[]) => isNonEmptyArray(list) && list.findIndex(d => (isFalse(this.isValidationMode) && d.status !== Enums.Deposit.StatusEnum.IN_PROGRESS && d.status !== Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED)
    // tslint:disable-next-line:semicolon
    || (isTrue(this.isValidationMode) && d.status !== Enums.Deposit.StatusEnum.IN_VALIDATION)) === -1;

  bulkActions: DataTableBulkActions<Deposit>[] = [
    {
      icon: IconNameEnum.structures,
      callback: (list: Deposit[]) => this._bulkAddStructures(list),
      labelToTranslate: () => LabelTranslateEnum.addStructure,
      displayCondition: this._displayConditionBulkEdit,
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.researchGroup,
      callback: (list: Deposit[]) => this._bulkAddResearchGroup(list),
      labelToTranslate: () => LabelTranslateEnum.addResearchGroup,
      displayCondition: this._displayConditionBulkEdit,
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.languages,
      callback: (list: Deposit[]) => this._bulkAddLanguage(list),
      labelToTranslate: () => LabelTranslateEnum.addLanguage,
      displayCondition: this._displayConditionBulkEdit,
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.exportBibliography,
      callback: (list: Deposit[]) => this._bulkExportDepositsInProgress(list),
      labelToTranslate: () => LabelTranslateEnum.exportDepositsInProgressInTsv,
      displayCondition: (list: Deposit[]) => (isTrue(this.isValidationMode) && ((isNonEmptyArray(list) && list.length <= 50) || this._securityService.isRootOrAdmin())),
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.askFeedback,
      callback: (list: Deposit[]) => this._bulkAskFeedback(list),
      labelToTranslate: () => LabelTranslateEnum.askFeedbackSelection,
      displayCondition: (list: Deposit[]) => isTrue(this.isValidationMode) && isNonEmptyArray(list) && list.findIndex(d => d.status !== Enums.Deposit.StatusEnum.IN_VALIDATION) === -1,
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.delete,
      callback: (list: Deposit[]) => this._bulkDeleteDeposit(list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.deleteDocuments,
      displayCondition: (list: Deposit[]) => isFalse(this.isValidationMode) && isNonEmptyArray(list) && list.findIndex(d => d.status !== Enums.Deposit.StatusEnum.IN_PROGRESS && d.status !== Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED && d.status !== Enums.Deposit.StatusEnum.IN_ERROR) === -1,
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.submit,
      callback: (list: Deposit[]) => this._bulkSubmitForApprovalDeposit(list),
      labelToTranslate: () => LabelTranslateEnum.sendToValidation,
      displayCondition: (list: Deposit[]) => isFalse(this.isValidationMode) && isNonEmptyArray(list) && list.findIndex(d => (d.status !== Enums.Deposit.StatusEnum.IN_PROGRESS && d.status !== Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED)) === -1,
      typeButton: ButtonThemeEnum.flatButton,
      color: ButtonColorEnum.success,
    },
    {
      icon: IconNameEnum.approve,
      callback: (list: Deposit[]) => this._bulkValidateDeposit(list),
      labelToTranslate: () => LabelTranslateEnum.approveSelection,
      displayCondition: (list: Deposit[]) => isTrue(this.isValidationMode) && isNonEmptyArray(list) && list.findIndex(d => d.status !== Enums.Deposit.StatusEnum.IN_VALIDATION) === -1 && this._securityService.isRootOrAdmin(),
      color: ButtonColorEnum.success,
      typeButton: ButtonThemeEnum.flatButton,
    },
  ];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  computeIsMultiSelectable(currentTab: DepositListTabEnum): void {
    if (this.isValidationMode) {
      this.isMultiSelectable = true;
    } else {
      this.isMultiSelectable = [DepositListTabEnum.in_progress, DepositListTabEnum.all].includes(currentTab);
    }
  }

  override getAll(queryParameters?: QueryParameters): void {
    super.getAll(queryParameters);
    this.refreshCounter();
  }

  private _bulkDeleteDeposit(listId: string[]): Observable<boolean> {
    return DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.bulkAction.delete.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.bulkAction.delete.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      if (isTrue(isConfirmed)) {
        this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new DepositAction.DeleteList(listId), DepositAction.DeleteListSuccess, action => {
          this.getAll();
        }));
      }
    }) as Observable<boolean>;
  }

  private _bulkAddResearchGroup(listDeposit: Deposit[]): void {
    const listDepositId = listDeposit.map(d => d.resId);
    this.subscribe(DialogUtil.open(this._dialog, DepositBulkResearchGroupDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.addResearchGroup.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.addResearchGroup.message"),
      numberDeposit: listDepositId.length,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
    }, researchGroupId => {
      if (isNonEmptyString(researchGroupId)) {
        this._store.dispatch(new DepositAction.UpdateResearchGroup(listDepositId, researchGroupId));
      }
    }));
  }

  private _bulkAddLanguage(listDeposit: Deposit[]): void {
    const listDepositId = listDeposit.map(d => d.resId);
    this.subscribe(DialogUtil.open(this._dialog, DepositBulkLanguageDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.addLanguage.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.addLanguage.message"),
      numberDeposit: listDepositId.length,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
    }, languageId => {
      if (isNonEmptyString(languageId)) {
        this._store.dispatch(new DepositAction.UpdateLanguage(listDepositId, languageId));
      }
    }));
  }

  private _bulkAddStructures(listDeposit: Deposit[]): void {
    const listDepositId = listDeposit.map(d => d.resId);
    this.subscribe(DialogUtil.open(this._dialog, DepositBulkStructureDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.addStructure.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.addStructure.message"),
      numberDeposit: listDepositId.length,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
    }, structureId => {
      if (isNonEmptyString(structureId)) {
        this._store.dispatch(new DepositAction.UpdateStructure(listDepositId, structureId));
      }
    }));
  }

  private _bulkExportDepositsInProgress(listDeposit: Deposit[]): void {
    const url = `${ApiEnum.adminPublications}/${ApiActionNameEnum.EXPORT_DEPOSITS}`;
    const idsParams = listDeposit.map(d => d.resId);
    const action = new DepositAction.ExportDepositsInProgress(url, idsParams);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
      DepositAction.ExportDepositsInProgressSuccess,
      result => {
        this._changeDetector.detectChanges();
      }));
  }

  private _bulkAskFeedback(listDeposit: Deposit[]): Observable<boolean> {
    const listDepositId = listDeposit.map(d => d.resId);
    return DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.bulkAction.askFeedback.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.bulkAction.askFeedback.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      withInput: ConfirmDialogInputEnum.TEXTAREA,
      inputRequired: true,
      inputLabelToTranslate: LabelTranslateEnum.reason,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (message: string) => {
      if (isNullOrUndefinedOrWhiteString(message)) {
        return;
      }
      this._store.dispatch(new DepositAction.AskFeedbackList(listDepositId, message));
    }) as Observable<boolean>;
  }

  private _bulkSubmitForApprovalDeposit(list: Deposit[]): void {
    const listDepositWithoutStructure: Deposit[] = [];
    const listActionWrapper: ActionSubActionCompletionsWrapper[] = [];
    list.forEach(deposit => {
      const structures = deposit?.depositFormModel?.contributors?.academicStructures;
      if (isNonEmptyArray(structures)) {
        listActionWrapper.push({
          action: new DepositAction.SubmitForValidation(deposit.resId, undefined, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.SubmitForValidationSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.SubmitForValidationFail)),
          ],
        });
      } else {
        listDepositWithoutStructure.push(deposit);
      }
    });

    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, listActionWrapper).pipe(
      tap(result => {
        this.getAll();

        if (isNonEmptyArray(listDepositWithoutStructure)) {
          const names = "\n\n" + listDepositWithoutStructure.map(d => "- " + d.title).join("\n");
          DialogUtil.open(this._dialog, ConfirmDialog, {
            titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.bulkAction.submitForApproval.title"),
            // eslint-disable-next-line id-blacklist
            paramTitle: {number: listDepositWithoutStructure.length},
            messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.bulkAction.submitForApproval.message"),
            paramMessage: {names: names},
            cancelButtonToTranslate: LabelTranslateEnum.close,
            colorCancel: ButtonColorEnum.primary,
          }, {
            panelClass: "white-space-pre-line",
          });
        }
      }),
    ));
  }

  private _bulkValidateDeposit(list: Deposit[]): void {
    const listActionWrapper: ActionSubActionCompletionsWrapper[] = [];

    list.forEach(deposit => {
      listActionWrapper.push({
        action: new DepositAction.Submit(deposit.resId, DepositRedirectTargetEnum.NOTHING),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.SubmitSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.SubmitFail)),
        ],
      });
    });

    this.subscribe(StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, listActionWrapper).pipe(
      tap(result => {
        this.getAll();
      }),
    ));
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _injector: Injector,
              protected readonly _dialog: MatDialog,
              private readonly _router: Router,
              private readonly _securityService: SecurityService,
              private readonly _translateService: TranslateService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.deposit, depositActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      listExtraButtons: [],
      historyState: DepositStatusHistoryState,
      historyStateAction: depositStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Deposit.StatusEnumTranslate,
    });
    this.cleanIsNeeded = true;

    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.seeMyDepositAsContributor)) {
      this.showAsContributor = LocalStorageHelper.getItem(LocalStorageEnum.seeMyDepositAsContributor) === SOLIDIFY_CONSTANTS.STRING_TRUE;
    }

    if (this._securityService.isRootOrAdmin()) {
      this.options.listExtraButtons.push({
        color: ButtonColorEnum.primary,
        icon: null,
        labelToTranslate: (current) => LabelTranslateEnum.seeAllDeposits,
        callback: (model, buttonElementRef, checked) => this._showAllDeposits(checked),
        order: 40,
        displayCondition: () => this.isValidationMode,
        isToggleButton: true,
        isToggleChecked: CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.seeAllDepositsLastSelection) && LocalStorageHelper.getItem(LocalStorageEnum.seeAllDepositsLastSelection) === SOLIDIFY_CONSTANTS.STRING_TRUE,
      });
    }

    this.bulkActions.forEach(bulkAction => {
      this.options.listExtraButtons.push({
        color: bulkAction.color,
        typeButton: bulkAction.typeButton,
        icon: bulkAction.icon,
        labelToTranslate: bulkAction.labelToTranslate,
        order: bulkAction.order ?? 30,
        displayCondition: () => isNonEmptyArray(this.dataTablePresentational?.multiSelectionValueOnAllPages) && bulkAction.displayCondition(this.dataTablePresentational?.multiSelectionValueOnAllPages),
        callback: () => {
          const result = bulkAction.callback(this.dataTablePresentational?.multiSelectionValueOnAllPages);
          if (isObservable(result)) {
            this.subscribe(result as Observable<boolean>);
          }
        },
      });
    });
  }

  changeShowAsContributor(showAllDeposits: boolean): void {
    let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, DepositState);
    queryParameter = QueryParametersUtil.clone(queryParameter);
    if (showAllDeposits) {
      this._updateQueryParameterWithAsContributor(queryParameter, true);
      this.showAsContributor = true;
    } else {
      this._updateQueryParameterWithAsContributor(queryParameter, false);
      this.showAsContributor = false;
    }
    this._store.dispatch(new DepositAction.ChangeQueryParameters(queryParameter, true));
    this.refreshCounter();
    this._changeDetector.detectChanges();
  }

  private _showAllDeposits(showAllDeposits: boolean): void {
    let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, DepositState);
    queryParameter = QueryParametersUtil.clone(queryParameter);
    if (showAllDeposits) {
      this._updateQueryParameterWithOnlyValidationRight(queryParameter, true);
      this._allDeposits = true;
    } else {
      this._updateQueryParameterWithOnlyValidationRight(queryParameter, false);
      this._allDeposits = false;
    }
    this._store.dispatch(new DepositAction.ChangeQueryParameters(queryParameter, true));
    this.refreshCounter();
    this._changeDetector.detectChanges();
  }

  private _updateQueryParameterWithOnlyValidationRight(queryParameters: QueryParameters, showAllDeposits: boolean): QueryParameters | undefined {
    queryParameters = ObjectUtil.clone(queryParameters);
    queryParameters.search = ObjectUtil.clone(queryParameters.search);

    if (showAllDeposits) {
      MappingObjectUtil.set(queryParameters.search.searchItems, DepositState._KEY_SHOW_ALL_DEPOSITS, SOLIDIFY_CONSTANTS.STRING_TRUE);
    } else {
      MappingObjectUtil.delete(queryParameters.search.searchItems, DepositState._KEY_SHOW_ALL_DEPOSITS);
    }
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.seeAllDepositsLastSelection)) {
      LocalStorageHelper.setItem(LocalStorageEnum.seeAllDepositsLastSelection, showAllDeposits ? SOLIDIFY_CONSTANTS.STRING_TRUE : SOLIDIFY_CONSTANTS.STRING_FALSE);
    }
    return queryParameters;
  }

  private _updateQueryParameterWithAsContributor(queryParameters: QueryParameters, showAsContributor: boolean): QueryParameters | undefined {
    queryParameters = ObjectUtil.clone(queryParameters);
    queryParameters.search = ObjectUtil.clone(queryParameters.search);

    if (showAsContributor) {
      MappingObjectUtil.set(queryParameters.search.searchItems, DepositState._KEY_SHOW_AS_CONTRIBUTOR, SOLIDIFY_CONSTANTS.STRING_TRUE);
    } else {
      MappingObjectUtil.delete(queryParameters.search.searchItems, DepositState._KEY_SHOW_AS_CONTRIBUTOR);
    }
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.seeMyDepositAsContributor)) {
      LocalStorageHelper.setItem(LocalStorageEnum.seeMyDepositAsContributor, showAsContributor ? SOLIDIFY_CONSTANTS.STRING_TRUE : SOLIDIFY_CONSTANTS.STRING_FALSE);
    }
    return queryParameters;
  }

  create(): void {
    const queryParam = {};

    let identifier: string = this.formControlIdentifier.value;

    if (isNotNullNorUndefinedNorWhiteString(identifier)) {
      identifier = identifier.trim();
      MappingObjectUtil.set(queryParam, importIdentifierQueryParam, identifier);

      let importMethod: ImportMethodEnum = undefined;
      if (AouRegexUtil.isDoi.test(identifier)) {
        importMethod = ImportMethodEnum.doi;
      } else if (AouRegexUtil.isIsbn.test(identifier)) {
        importMethod = ImportMethodEnum.isbn;
      } else if (AouRegexUtil.isPmcid.test(identifier)) {
        importMethod = ImportMethodEnum.pubmed;
      }
      if (isNotNullNorUndefined(importMethod)) {
        MappingObjectUtil.set(queryParam, importMethodQueryParam, importMethod);
      }
    }

    this.navigateNavigate(new Navigate([this._storeRouteService.getCreateRoute(this._state)], queryParam, {skipLocationChange: false}));
  }

  ngOnInit(): void {
    // Allow to preserve query parameters (pagination, sort, filter) when come from detail page
    if (isNotNullNorUndefined(this._routerExt.getPreviousUrl())) {
      if (this._routerExt.getPreviousUrl().includes(DepositRoutesEnum.detail)) {
        this.cleanIsNeeded = false;
      } else {
        this.clean();
      }
    }

    super.ngOnInit();

    if (this._computeIsValidationMode()) {
      this.options.canCreate = false;
    }

    this.computeListTabs();

    if (this.isValidationMode) {
      this._store.dispatch(new AppBannerAction.Show(LabelTranslateEnum.depositValidationInterface, BannerColorEnum.success));
    }

    this.defineColumns();
    this.actions = this.defineActions();

    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: this._INTERVAL_REFRESH_IN_SECOND,
      resetIntervalWhenUserMouseEvent: true,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60 * 10,
      stopRefreshAfterMaximumIntervalReached: true,
      actionToDo: () => this.refreshCounter(),
    }));

    this.subscribe(this.observeCurrentTabToConditionalDisplayStatus());
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    this._store.dispatch(new AppBannerAction.Hide());
  }

  private _computeIsValidationMode(): boolean {
    this.isValidationMode = this._route?.parent?.snapshot?.url.length > 0 && this._route.parent.snapshot.url[0].path === AppRoutesEnum.depositToValidate;
    return this.isValidationMode;
  }

  conditionDisplayEditButton(deposit: Deposit | undefined): boolean {
    return DepositHelper.isEditAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), deposit?.status);
  }

  conditionDisplayDeleteButton(deposit: Deposit | undefined): boolean {
    return DepositHelper.isDeleteAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), deposit?.status);
  }

  override goToEdit(deposit: Deposit): void {
    let base = RoutesEnum.depositDetail;
    if (this.isValidationMode) {
      base = RoutesEnum.depositToValidateDetail;
    }
    this._store.dispatch(new Navigate([base, deposit.resId, DepositRoutesEnum.edit, Enums.Deposit.StepEnum.FILES]));
  }

  private _generateTooltipComment(list: Comment[]): string {
    return list.map(c => c.text).join("\n--\n");
  }

  private _getActionComment(isValidatorComment: boolean): DataTableActions<Deposit> {
    return {
      logo: isValidatorComment ? IconNameEnum.validatorComments : IconNameEnum.comments,
      callback: (deposit: Deposit) => this._openComment(deposit, isValidatorComment),
      placeholder: current => MemoizedUtil.select(this._store, isValidatorComment ? DepositCommentValidatorState : DepositCommentState,
        state => isNonEmptyArray(state.list) ? this._generateTooltipComment(state.list) : (isValidatorComment ? LabelTranslateEnum.validatorsComments : LabelTranslateEnum.comments)),
      displayOnCondition: (deposit: Deposit) => isValidatorComment ? this.isValidationMode && deposit.numberOfValidatorComments > 0 : deposit.numberOfComments > 0,
      mouseoverCallback: ($event, deposit) => {
        const list: Comment[] = isValidatorComment ?
          MemoizedUtil.listSnapshot(this._store, DepositCommentValidatorState) as Comment[]
          : MemoizedUtil.listSnapshot(this._store, DepositCommentState) as Comment[];
        if (isNotNullNorUndefined(list) && list.length > 0 && list[0]?.publication?.resId === deposit.resId) {
          return;
        }
        const queryParameters = new QueryParameters(environment.numberCommentToDisplayInDepositList, {
          field: "creation.when",
          order: OrderEnum.descending,
        });
        this._store.dispatch(isValidatorComment ?
          new DepositCommentValidatorAction.GetAll(deposit.resId, queryParameters)
          : new DepositCommentAction.GetAll(deposit.resId, queryParameters));
      },
      isWrapped: false,
      isVisible: true,
      displayNumberItems: (deposit: Deposit) => isValidatorComment ? deposit.numberOfValidatorComments : deposit.numberOfComments,
    };
  }

  protected defineActions(): DataTableActions<Deposit>[] {
    const actions = [
      ...super._defineActions(),
      this._getActionComment(false),
      this._getActionComment(true),
      {
        logo: IconNameEnum.cloneDeposit,
        callback: (deposit: Deposit) => this._cloneDeposit(deposit),
        placeholder: current => LabelTranslateEnum.cloneDeposit,
        displayOnCondition: (deposit: Deposit) => true,
        isWrapped: true,
        isVisible: true,
      },
      {
        logo: IconNameEnum.internalLink,
        callback: (deposit: Deposit) => this._seeArchive(deposit.archiveId),
        placeholder: (deposit: Deposit) => this._translateService.instant(LabelTranslateEnum.seeArchive) + ": " + SsrUtil.window.location.origin + "/" + deposit.archiveId,
        displayOnCondition: (deposit: Deposit) => isNotNullNorUndefinedNorWhiteString(deposit.archiveId),
        isWrapped: false,
        isVisible: true,
      },
    ];
    actions.find(a => a.logo === IconNameEnum.edit).isWrapped = true;
    return actions;
  }

  private observeCurrentTabToConditionalDisplayStatus(): Observable<DepositListTabEnum> {
    return this.currentTabObs.pipe(
      distinctUntilChanged(),
      filter(currentTab => isNotNullNorUndefined(currentTab)),
      tap(currentTab => {
        this.computeIsMultiSelectable(currentTab);
        const indexOf = this.columns.findIndex(c => c.field === this._STATUS_KEY);
        if ([DepositListTabEnum.all].includes(currentTab)) {
          if (indexOf === -1) {
            this.columns.push(this._columnStatus);
            this.columns = [...this.columns];
          }
        } else {
          if (indexOf !== -1) {
            this.columns.splice(indexOf, 1);
            this.columns = [...this.columns];
          }
        }
        this.dataTablePresentational?.clearFilters();
      }),
    );
  }

  suffixUrlMatcher: (route: ActivatedRoute) => string = (route: ActivatedRoute) => route.snapshot.paramMap.get(AppRoutesEnum.paramIdStatusWithoutPrefixParam);

  private refreshCounter(): void {
    this._store.dispatch(new DepositAction.RefreshAllCounterStatusTab());
  }

  defineColumns(): void {
    this.columns = [...DepositListHelper.columns(MemoizedUtil.listSnapshot(this._store, AppDepositSubtypeState))];
  }

  private computeListTabs(): void {
    this.listTabs = [
      {
        id: DepositListTabEnum.in_progress,
        suffixUrl: DepositListTabEnum.in_progress,
        titleToTranslate: LabelTranslateEnum.pendingSubmission,
        route: () => [...this.rootUrl, DepositListTabEnum.in_progress],
        numberNew: () => this.isValidationMode ? 0 : this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.in_progress))),
        conditionDisplay: () => {
          const isCurrentPage = this._router.url.endsWith(DepositListTabEnum.in_progress);
          return isFalse(this.isValidationMode) || this._securityService.isRootOrAdmin() || isCurrentPage;
        },
      },
      {
        id: DepositListTabEnum.feedback_required,
        suffixUrl: DepositListTabEnum.feedback_required,
        titleToTranslate: LabelTranslateEnum.feedbackRequired,
        route: () => [...this.rootUrl, DepositListTabEnum.feedback_required],
        numberNew: () => this.isValidationMode ? 0 : this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.feedback_required))),
        conditionDisplay: () => this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.feedback_required) > 0)),
      },
      {
        id: DepositListTabEnum.in_validation,
        suffixUrl: DepositListTabEnum.in_validation,
        titleToTranslate: LabelTranslateEnum.inValidationAtTheLibrary,
        route: () => [...this.rootUrl, DepositListTabEnum.in_validation],
        numberNew: () => this.isValidationMode ? this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.in_validation))) : 0,
      },
      {
        id: DepositListTabEnum.in_edition,
        suffixUrl: DepositListTabEnum.in_edition,
        titleToTranslate: LabelTranslateEnum.inEdition,
        route: () => [...this.rootUrl, DepositListTabEnum.in_edition],
        numberNew: () => this.isValidationMode ? 0 : this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.in_edition))),
        conditionDisplay: () => this.tabListCountersObs.pipe(map(m => {
          const isCurrentPage = this._router.url.endsWith(DepositListTabEnum.in_edition);
          return (isFalse(this.isValidationMode) || this._securityService.isRootOrAdmin() || isCurrentPage) && MappingObjectUtil.get(m, DepositListTabEnum.in_edition) > 0;
        })),
      },
      {
        id: DepositListTabEnum.updates_validation,
        suffixUrl: DepositListTabEnum.updates_validation,
        titleToTranslate: LabelTranslateEnum.updatesValidation,
        route: () => [...this.rootUrl, DepositListTabEnum.updates_validation],
        numberNew: () => this.isValidationMode ? this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.updates_validation))) : 0,
        conditionDisplay: () => this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.updates_validation) > 0)),
      },
      {
        id: DepositListTabEnum.in_error,
        suffixUrl: DepositListTabEnum.in_error,
        titleToTranslate: Enums.Deposit.StatusEnumTranslate.find(s => s.key === Enums.Deposit.StatusEnum.IN_ERROR).value,
        route: () => [...this.rootUrl, DepositListTabEnum.in_error],
        numberNew: () => this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.in_error))),
        conditionDisplay: () => this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.in_error) > 0)),
      },
      {
        id: DepositListTabEnum.completed,
        suffixUrl: DepositListTabEnum.completed,
        titleToTranslate: Enums.Deposit.StatusEnumTranslate.find(s => s.key === Enums.Deposit.StatusEnum.COMPLETED).value,
        route: () => [...this.rootUrl, DepositListTabEnum.completed],
      },
      {
        id: DepositListTabEnum.canonical,
        suffixUrl: DepositListTabEnum.canonical,
        titleToTranslate: Enums.Deposit.StatusEnumTranslate.find(s => s.key === Enums.Deposit.StatusEnum.CANONICAL).value,
        route: () => [...this.rootUrl, DepositListTabEnum.canonical],
        conditionDisplay: () => this.tabListCountersObs.pipe(map(m => MappingObjectUtil.get(m, DepositListTabEnum.canonical) > 0)),
      },
      {
        id: DepositListTabEnum.all,
        suffixUrl: DepositListTabEnum.all,
        titleToTranslate: LabelTranslateEnum.all,
        route: () => [...this.rootUrl, DepositListTabEnum.all],
      },
    ];
  }

  private get rootUrl(): string[] {
    if (this.isValidationMode) {
      return [AppRoutesEnum.depositToValidate];
    }
    return [AppRoutesEnum.deposit];
  }

  setCurrentTab($event: Tab): void {
    this._setLastSettingAllDeposit();
    this._setLastSettingAsContributor();
    const currentTab = $event.id as DepositListTabEnum;
    this._store.dispatch(new DepositAction.ChangeCurrentTab(currentTab));
    this.computeIsMultiSelectable(currentTab);
  }

  private _setLastSettingAllDeposit(): void {
    if (this._securityService.isRootOrAdmin()
      && CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.seeAllDepositsLastSelection)
      && LocalStorageHelper.getItem(LocalStorageEnum.seeAllDepositsLastSelection) === SOLIDIFY_CONSTANTS.STRING_TRUE) {
      let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, DepositState);
      queryParameter = QueryParametersUtil.clone(queryParameter);
      this._updateQueryParameterWithOnlyValidationRight(queryParameter, true);
      this._allDeposits = true;
      this._store.dispatch(new DepositAction.ChangeQueryParameters(queryParameter, true, false));
    }
  }

  private _setLastSettingAsContributor(): void {
    if (!this.isValidationMode &&
      CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.seeMyDepositAsContributor)
      && LocalStorageHelper.getItem(LocalStorageEnum.seeMyDepositAsContributor) === SOLIDIFY_CONSTANTS.STRING_TRUE) {
      let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, DepositState);
      queryParameter = QueryParametersUtil.clone(queryParameter);
      this._updateQueryParameterWithAsContributor(queryParameter, true);
      this.showAsContributor = true;
      this._store.dispatch(new DepositAction.ChangeQueryParameters(queryParameter, true, false));
    }
  }

  override showDetail(deposit: Deposit): void {
    if (this.isValidationMode) {
      this._store.dispatch(new Navigate([RoutesEnum.depositToValidateDetail, deposit.resId]));
    } else {
      this._store.dispatch(new Navigate([RoutesEnum.depositDetail, deposit.resId]));
    }
  }

  private _openComment(deposit: Deposit, isValidatorComments: boolean): void {
    DialogUtil.open(this._dialog, DepositCommentDialog, {
      deposit: deposit,
      isValidatorComments: isValidatorComments,
    }, {
      minWidth: "600px",
      maxWidth: "90vw",
    });
  }

  trimWhenPasteIdentifier($event: ClipboardEvent): void {
    const pastedString: string = ($event.clipboardData || SsrUtil.window?.["clipboardData"]).getData("text");
    if (!isNonEmptyString(pastedString)) {
      return;
    }
    if (isNotNullNorUndefinedNorWhiteString(pastedString)) {
      this.formControlIdentifier.setValue(pastedString.trim());
    }

    $event.preventDefault();
    $event.stopPropagation();
  }

  override clean(): void {
    this._store.dispatch(new DepositAction.Clean(false, defaultDepositStateQueryParameters()));
  }

  private _cloneDeposit(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.clone.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.clone.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      this._store.dispatch(new DepositAction.Clone(deposit));
    }));
  }

  private _seeArchive(archiveId: string): void {
    const language = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage);
    this._store.dispatch(new Navigate([archiveId], {
      lang: language,
    }));
  }
}

export type DepositListTabEnum =
  "in_progress"
  | "in_edition"
  | "in_validation"
  | "updates_validation"
  | "feedback_required"
  | "in_error"
  | "completed"
  | "canonical"
  | "all";
export const DepositListTabEnum = {
  in_progress: "in_progress" as DepositListTabEnum,
  in_edition: "in_edition" as DepositListTabEnum,
  in_validation: "in_validation" as DepositListTabEnum,
  updates_validation: "updates_validation" as DepositListTabEnum,
  feedback_required: "feedback_required" as DepositListTabEnum,
  in_error: "in_error" as DepositListTabEnum,
  completed: "completed" as DepositListTabEnum,
  canonical: "canonical" as DepositListTabEnum,
  all: "all" as DepositListTabEnum,
};
