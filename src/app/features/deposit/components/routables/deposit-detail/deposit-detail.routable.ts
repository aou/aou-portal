/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  RouterStateSnapshot,
} from "@angular/router";
import {DepositValidationStructureDialog} from "@app/features/deposit/components/dialogs/deposit-validation-structure/deposit-validation-structure.dialog";
import {DepositSummaryPresentational} from "@app/features/deposit/components/presentationals/deposit-summary/deposit-summary.presentational";
import {DepositHelper} from "@app/features/deposit/helpers/deposit.helper";
import {DocumentFileUploadHelper} from "@app/features/deposit/helpers/document-file-upload.helper";
import {
  DepositFormDate,
  DepositFormModel,
} from "@app/features/deposit/models/deposit-form-definition.model";
import {DepositCommentValidatorAction} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.action";
import {defaultDepositCommentValidatorStateQueryParameters} from "@app/features/deposit/stores/comment-validator/deposit-comment-validator.state";
import {DepositCommentAction} from "@app/features/deposit/stores/comment/deposit-comment.action";
import {defaultDepositCommentStateQueryParameters} from "@app/features/deposit/stores/comment/deposit-comment.state";
import {DepositAction} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {DepositDocumentFileAction} from "@app/features/deposit/stores/document-file/deposit-document-file.action";
import {
  defaultDepositDocumentFileStateQueryParameters,
  DepositDocumentFileState,
} from "@app/features/deposit/stores/document-file/deposit-document-file.state";
import {DepositStatusHistoryAction} from "@app/features/deposit/stores/status-history/deposit-status-history.action";
import {DepositStatusHistoryState} from "@app/features/deposit/stores/status-history/deposit-status-history.state";
import {DepositStructureAction} from "@app/features/deposit/stores/structure/deposit-structure.action";
import {DepositStructureState} from "@app/features/deposit/stores/structure/deposit-structure.state";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositCommentsContainer} from "@deposit/components/containers/deposit-comments/deposit-comments.container";
import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DepositStructure,
  MetadataDifference,
  Romeo,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DepositRoleHelper} from "@shared/helpers/deposit-role.helper";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {SharedDepositTypeSubtypeAction} from "@shared/stores/deposit-type/subtype/shared-deposit-type-subtype.action";
import {SharedDepositSubSubtypeAction} from "@shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.action";
import {SharedExternalDataAction} from "@shared/stores/external-data/shared-external-data.action";
import {SharedExternalDataState} from "@shared/stores/external-data/shared-external-data.state";
import {
  combineLatest,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AppBannerAction,
  BannerColorEnum,
  ButtonColorEnum,
  ButtonThemeEnum,
  cleanHtml,
  ConfirmDialog,
  ConfirmDialogInputEnum,
  DeleteDialog,
  DialogUtil,
  ErrorDtoMessage,
  ExtraButtonToolbar,
  isEmptyString,
  isFalse,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isString,
  isTrue,
  isWhiteString,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ObjectUtil,
  PollingHelper,
  QueryParameters,
  ScrollService,
  SOLIDIFY_CONSTANTS,
  StateModel,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
  StringUtil,
  UrlQueryParamHelper,
  ValidationErrorDto,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-detail-routable",
  templateUrl: "./deposit-detail.routable.html",
  styleUrls: ["./deposit-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositDetailRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, DepositStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositStatusHistoryState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, DepositStatusHistoryState, state => state.queryParameters);
  listCurrentStatusObs: Observable<MappingObject<Enums.DocumentFile.StatusEnum, number>> = MemoizedUtil.select(this._store, DepositDocumentFileState, state => state.listCurrentStatus);
  romeoObs: Observable<Romeo> = MemoizedUtil.select(this._store, SharedExternalDataState, state => state.romeo);
  listUserRoleEnumObs: Observable<Enums.Deposit.UserRoleEnum[]> = MemoizedUtil.select(this._store, DepositState, state => state.listUserRoleEnum);
  @Select(DepositState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(DepositState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  validationErrorObs: Observable<ValidationErrorDto[]> = MemoizedUtil.select(this._store, DepositState, state => state.errorValidation);
  errorDuplicateObs: Observable<ErrorDtoMessage> = MemoizedUtil.select(this._store, DepositState, state => state.errorDuplicate);
  metadataDifferencesObs: Observable<MappingObject<string, MappingObject<string, MetadataDifference[]>>> = MemoizedUtil.select(this._store, DepositState, state => state.metadataDifferences);

  @ViewChild("depositSummaryPresentational")
  depositSummaryPresentational: DepositSummaryPresentational;

  @ViewChild("depositCommentsContainer")
  depositCommentsContainer: DepositCommentsContainer;

  readonly KEY_PARAM_NAME: keyof Deposit & string = "title";

  @HostBinding("class.is-edit")
  isEdit: boolean;

  isEditObs: Observable<boolean>;

  isValidationMode: boolean = false;

  get isCreatorOrContributorOrRootOrAdmin(): boolean {
    if (isTrue(this.isValidationMode) || this._securityService.isRootOrAdmin()) {
      return true;
    }
    const listUserRoleEnum = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.listUserRoleEnum);
    return DepositRoleHelper.isCreatorOrContributor(listUserRoleEnum);
  }

  currentObs: Observable<Deposit> = MemoizedUtil.current(this._store, DepositState);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositState);

  currentUserContributorOfDeposit: boolean = false;

  deleteButtonType: ButtonThemeEnum = ButtonThemeEnum.flatButton;
  deleteButtonColor: ButtonColorEnum = ButtonColorEnum.warn;

  @Select((state: StateModel) => state.router.state) urlStateObs: Observable<RouterStateSnapshot>;

  get depositId(): string {
    return this._resId;
  }

  isDeleteAvailable(deposit: Deposit): boolean {
    return isTrue(this.isCreatorOrContributorOrRootOrAdmin) && DepositHelper.isDeleteAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), deposit?.status);
  }

  isEditAvailable(deposit: Deposit): boolean {
    return isTrue(this.isCreatorOrContributorOrRootOrAdmin) && DepositHelper.isEditAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), deposit?.status);
  }

  isReEditAvailable(deposit: Deposit): boolean {
    return isTrue(this.isCreatorOrContributorOrRootOrAdmin) && DepositHelper.isReEditAvailable(this.isValidationMode, this._securityService.isRootOrAdmin(), deposit?.status);
  }

  _state: StateEnum = StateEnum.deposit;
  _resId: string | undefined;

  isImported: boolean = false;

  private _isPendingApproval: boolean = false;

  depositFormModelObs: Observable<DepositFormModel> = MemoizedUtil.current(this._store, DepositState).pipe(
    distinctUntilChanged(),
    map(deposit => {
      this.isImported = isNotNullNorUndefined(deposit?.importSource);
      const formData: DepositFormModel = isNullOrUndefined(deposit) ? undefined : JSON.parse(deposit.formData);
      this.retrieveRomeo(formData?.description?.identifiers?.issn);
      return formData;
    }),
  );
  depositStatusObs: Observable<Enums.Deposit.StatusEnum> = MemoizedUtil.select(this._store, DepositState, state => state?.current?.status);
  urlQueryParameters: MappingObject<string, string | undefined>;

  isValidationErrorObs: Observable<boolean> = this.validationErrorObs.pipe(
    map(validationError =>
      isNotNullNorUndefined(validationError) && isNonEmptyArray(validationError)),
  );
  isMetadataDifferencesObs: Observable<boolean> = this.metadataDifferencesObs.pipe(
    map(metadataUpdates => isNotNullNorUndefined(metadataUpdates) && MappingObjectUtil.size(metadataUpdates) > 0),
  );

  isFilePendingObs: Observable<boolean> = this.listCurrentStatusObs.pipe(
    map(listCurrentStatus =>
      DocumentFileUploadHelper.numberFilesPending(listCurrentStatus) > 0
      || DocumentFileUploadHelper.numberFilesInError(listCurrentStatus) > 0
      || DocumentFileUploadHelper.numberFilesToBeConfirmed(listCurrentStatus) > 0),
  );

  listExtraButtons: ExtraButtonToolbar<Deposit>[] = [
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.comments,
      callback: (current) => this._addAComment(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.addAComment,
      typeButton: ButtonThemeEnum.button,
      order: 21,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.askFeedback,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && current?.status === Enums.Deposit.StatusEnum.IN_VALIDATION,
      callback: (current) => this._askFeedback(current),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.askFeedback,
      typeButton: ButtonThemeEnum.button,
      order: 23,
    },
    {
      color: ButtonColorEnum.warn,
      icon: IconNameEnum.thisIsNotMyDeposit,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && isNotNullNorUndefined(current) && !this.isValidationMode && this.currentUserContributorOfDeposit && current.status !== Enums.Deposit.StatusEnum.COMPLETED && current.status !== Enums.Deposit.StatusEnum.CANONICAL && current.status !== Enums.Deposit.StatusEnum.DELETED && current.status !== Enums.Deposit.StatusEnum.IN_ERROR,
      callback: (current) => this._isNotMyDeposit(current),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.thisIsNotMyDeposit,
      typeButton: ButtonThemeEnum.button,
      order: 41, // just after the delete button
    },
    {
      color: ButtonColorEnum.warn,
      icon: IconNameEnum.unapprove,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && (current?.status === Enums.Deposit.StatusEnum.IN_VALIDATION),
      callback: (current) => this._reject(current, false),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.reject,
      typeButton: ButtonThemeEnum.flatButton,
      order: 50,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.transfer,
      displayCondition: current => MemoizedUtil.selected(this._store, DepositStructureState).pipe(
        map(list => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && current?.status === Enums.Deposit.StatusEnum.IN_VALIDATION && isNotNullNorUndefined(list) && list?.findIndex(s => (s["joinResource"] as DepositStructure).linkType === Enums.DepositStructure.LinkTypesEnum.VALIDATION) !== -1),
      ),
      callback: (current) => this._transfer(current),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.transfer,
      typeButton: ButtonThemeEnum.button,
      order: 51,
    },
    {
      color: ButtonColorEnum.warn,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.close,
      displayCondition: current => (isTrue(this.isCreatorOrContributorOrRootOrAdmin) || this.isValidationMode) && (current?.status === Enums.Deposit.StatusEnum.IN_EDITION),
      labelToTranslate: (current) => LabelTranslateEnum.cancelUpdate,
      order: 52,
      callback: (current) => this.cancelUpdate(current),
    },
    {
      color: ButtonColorEnum.warn,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.delete,
      displayCondition: current => isTrue(this._securityService.isRootOrAdmin()) && (current?.status === Enums.Deposit.StatusEnum.COMPLETED
        || current?.status === Enums.Deposit.StatusEnum.IN_ERROR),
      labelToTranslate: (current) => LabelTranslateEnum.changeToDeleted,
      order: 53,
      callback: (current) => this._changeToDeleted(current),
    },
    {
      color: ButtonColorEnum.warn,
      icon: IconNameEnum.unapprove,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && (current?.status === Enums.Deposit.StatusEnum.UPDATES_VALIDATION),
      callback: (current) => this._reject(current, true),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.rejectUpdatePublication,
      typeButton: ButtonThemeEnum.flatButton,
      order: 54,
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.cloneDeposit,
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.cloneDeposit,
      order: 55,
      callback: (current) => this._cloneDeposit(current),
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.edit,
      displayCondition: current => this.isEditAvailable(current) || this.isReEditAvailable(current),
      labelToTranslate: (current) => LabelTranslateEnum.edit,
      order: 56,
      callback: (current) => this.goToEdit(current),
    },
    {
      color: ButtonColorEnum.success,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.submit,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && !this.isValidationMode && (current?.status === Enums.Deposit.StatusEnum.IN_PROGRESS || current?.status === Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED || current?.status === Enums.Deposit.StatusEnum.IN_EDITION),
      labelToTranslate: (current) => LabelTranslateEnum.sendToValidation,
      disableCondition: current => combineLatest([this.isValidationErrorObs, this.isFilePendingObs, this.isMetadataDifferencesObs])
        .pipe(map(([isValidationError, isFilePending, hasMetadataUpdate]) => current.numberOfFiles === 0 || isValidationError || isFilePending || (current.status === Enums.Deposit.StatusEnum.IN_EDITION && !hasMetadataUpdate) || DepositHelper.isThesisAndScienceFacultyAndMissingFiles(this._store, current, current.metadataStructures.map(s => s.resId), isTrue((JSON.parse(current.formData) as DepositFormModel)?.contributors?.isBeforeUnige)))),
      order: 57,
      callback: (current) => this._submitForValidation(current),
    },
    {
      color: ButtonColorEnum.success,
      icon: IconNameEnum.transfer,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && current?.status === Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED,
      callback: (current) => this._sendBackForValidation(current),
      disableCondition: current => combineLatest([this.isValidationErrorObs, this.isFilePendingObs])
        .pipe(map(([isValidationError, isFilePending]) => isValidationError || isFilePending)),
      labelToTranslate: (current) => LabelTranslateEnum.sendBackForValidation,
      typeButton: ButtonThemeEnum.flatButton,
      order: 58,
    },
    {
      color: ButtonColorEnum.success,
      icon: IconNameEnum.approve,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && (current?.status === Enums.Deposit.StatusEnum.IN_VALIDATION || current?.status === Enums.Deposit.StatusEnum.UPDATES_VALIDATION) && isNotNullNorUndefined(this.depositSummaryPresentational?.depositFormModelCleanToSubmit),
      callback: (current) => this._approve(current, this.depositSummaryPresentational.depositFormModelCleanToSubmit),
      disableCondition: current => combineLatest([this.isValidationErrorObs, this.isFilePendingObs])
        .pipe(map(([isValidationError, isFilePending]) => isValidationError || isFilePending || this._isPendingApproval)),
      labelToTranslate: (current) => LabelTranslateEnum.approve,
      typeButton: ButtonThemeEnum.flatButton,
      order: 60,
    },
    {
      color: ButtonColorEnum.success,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.resume,
      displayCondition: current => isTrue(this.isCreatorOrContributorOrRootOrAdmin) && this.isValidationMode && current?.status === Enums.Deposit.StatusEnum.IN_ERROR,
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      order: 61,
      callback: (current) => this._resume(current),
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.changeToCanonical,
      displayCondition: current => isTrue(this._securityService.isRootOrAdmin()) && current?.status === Enums.Deposit.StatusEnum.COMPLETED,
      labelToTranslate: (current) => LabelTranslateEnum.changeToCanonical,
      order: 62,
      callback: (current) => this._changeToCanonical(current),
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.changeToCompleted,
      displayCondition: current => isTrue(this._securityService.isRootOrAdmin()) && current?.status === Enums.Deposit.StatusEnum.CANONICAL,
      labelToTranslate: (current) => LabelTranslateEnum.changeToCompleted,
      order: 63,
      callback: (current) => this._changeToCompleted(current),
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.button,
      icon: IconNameEnum.externalLink,
      displayCondition: current => isNonEmptyString(current?.archiveId),
      labelToTranslate: (current) => this._translateService.instant(LabelTranslateEnum.seeArchive) + ` ${current.archiveId}`,
      order: 64,
      navigate: current => this._seeArchive(current.archiveId),
    },
  ];

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _dialog: MatDialog,
              private readonly _scrollService: ScrollService,
              private readonly _zone: NgZone,
              private readonly _storeDialogService: StoreDialogService,
              private readonly _translateService: TranslateService,
              private readonly _securityService: SecurityService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.computeIsValidationMode();
    this.initEditMode();
    this.retrieveResIdFromUrl();
    this._getSubResourceWithParentId(this._resId);
    this._showMetadataDifferences();
    this._computeBannerDetailMode();
    this._createPollingListenerCompletedStatus();

    this._store.dispatch(new DepositDocumentFileAction.GetAll(this._resId, defaultDepositDocumentFileStateQueryParameters()));
    this._store.dispatch(new DepositDocumentFileAction.GetListCurrentStatus(this._resId));
    this._store.dispatch(new DepositAction.ListPublicationUserRoles(this._resId));
    this._store.dispatch(new DepositAction.LoadResource(false));
    const currentDepositInState = MemoizedUtil.currentSnapshot(this._store, DepositState);

    if (!this.isEdit) {
      if (isNonEmptyString(currentDepositInState?.subtype?.type?.resId)) {
        this._store.dispatch(new SharedDepositTypeSubtypeAction.GetAll(currentDepositInState.subtype?.type?.resId));
      }
      if (isNonEmptyString(currentDepositInState?.subtype?.resId)) {
        this._store.dispatch(new SharedDepositSubSubtypeAction.GetAll(currentDepositInState.subtype?.resId));
      }
    }

    if (!this.isEdit && currentDepositInState?.resId !== this._resId) {
      this._store.dispatch(new DepositAction.GetById(this._resId));
    }

    this.subscribe(this.currentObs.pipe(
      filter(deposit => isNotNullNorUndefined(deposit) && deposit.resId === this._resId),
      distinctUntilChanged(),
      tap(deposit => {
        if (!this.isEdit) {
          const formData: DepositFormModel = JSON.parse(deposit.formData);
          const externalUidCurrentUser = MemoizedUtil.selectSnapshot(this._store, AppUserState, state => state.current.externalUid);
          if (isNullOrUndefined(formData) || isNullOrUndefined(externalUidCurrentUser) || isWhiteString(externalUidCurrentUser)) {
            this.currentUserContributorOfDeposit = false;
            return;
          }
          const currentUserCnIndividu = externalUidCurrentUser?.substring(0, externalUidCurrentUser.indexOf("@"));
          this.currentUserContributorOfDeposit = isNotNullNorUndefined(formData.contributors?.contributors?.find(c => c.contributor?.cnIndividu === currentUserCnIndividu));
        }
      }),
    ));

    this.subscribe(this.currentObs.pipe(
      filter(deposit => isNotNullNorUndefined(deposit) && deposit.resId === this._resId && this.isEdit),
      tap(deposit => {
        this._displayBannerEditModeWithSubType(deposit);
      }),
    ));

    this.subscribe(this.depositStatusObs.pipe(
      distinctUntilChanged(),
      tap(status => {
        this._computeBannerDetailMode();
      }),
    ));
  }

  navigateToStep(targetStep: Enums.Deposit.StepEnum): void {
    this._store.dispatch(new Navigate([this.isValidationMode ? RoutesEnum.depositToValidateDetail : RoutesEnum.depositDetail, this._resId, DepositRoutesEnum.edit, targetStep]));
  }

  navigate($event: Navigate): void {
    this._store.dispatch($event);
  }

  private _displayBannerEditModeWithSubType(deposit: Deposit): void {
    if (isNotNullNorUndefined(deposit) && deposit.resId === this._resId) {
      const message = isNullOrUndefinedOrWhiteString(deposit?.importSource) ? LabelTranslateEnum.youAreInEditModeOnADepositOfTypeX : LabelTranslateEnum.youAreInEditModeOnADepositOfTypeXViaIdentifier;
      const title = deposit.title;
      this._store.dispatch(new AppBannerAction.Show(message, BannerColorEnum.warn, {
        name: deposit?.subtype?.name,
        identifier: deposit?.importSource,
        title: cleanHtml(title),
      }));
    } else {
      this._store.dispatch(new AppBannerAction.ShowInEditMode());
    }
  }

  private _computeBannerDetailMode(): void {
    if (this.isEdit) {
      return;
    }
    const bannerInfo = this._getBannerDisplayInfo();
    if (isNotNullNorUndefined(bannerInfo)) {
      this._store.dispatch(new AppBannerAction.Show(bannerInfo.message, bannerInfo.color));
    } else {
      this._store.dispatch(new AppBannerAction.Hide());
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.cleanState();
    this._store.dispatch(new AppBannerAction.Hide());
  }

  private _getBannerDisplayInfo(): BannerDisplayInfo | undefined {
    const depositStatus = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.current?.status);

    if (depositStatus === Enums.Deposit.StatusEnum.IN_PROGRESS) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.inprogress"),
        color: BannerColorEnum.warn,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.IN_ERROR) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.error"),
        color: BannerColorEnum.error,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.REJECTED) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.rejected"),
        color: BannerColorEnum.error,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.COMPLETED) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.completed"),
        color: BannerColorEnum.success,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.IN_VALIDATION) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.invalidation"),
        color: BannerColorEnum.warn,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.feedbackRequired"),
        color: BannerColorEnum.warn,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.SUBMITTED) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.submitted"),
        color: BannerColorEnum.success,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.IN_EDITION) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.inedition"),
        color: BannerColorEnum.warn,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.UPDATES_VALIDATION) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.updatesvalidation"),
        color: BannerColorEnum.warn,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.DELETED) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.deleted"),
        color: BannerColorEnum.warn,
      };
    }

    if (depositStatus === Enums.Deposit.StatusEnum.CANONICAL) {
      return {
        message: MARK_AS_TRANSLATABLE("deposit.alert.status.canonical"),
        color: BannerColorEnum.warn,
      };
    }

    return undefined;
  }

  private computeIsValidationMode(): boolean {
    this.isValidationMode = this._route?.parent?.snapshot?.url.length > 0 && this._route.parent.snapshot.url[0].path === AppRoutesEnum.depositToValidate;
    return this.isValidationMode;
  }

  protected cleanState(): void {
    this._store.dispatch(new DepositAction.Clean(true));
  }

  protected retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this._resId)) {
      this._resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new DepositCommentAction.GetAll(id, defaultDepositCommentStateQueryParameters()));
    if (this.isValidationMode) {
      this._store.dispatch(new DepositCommentValidatorAction.GetAll(id, defaultDepositCommentValidatorStateQueryParameters()));
    }
    this._store.dispatch(new DepositStructureAction.GetAll(id));
  }

  typeChange(typeId: string): void {
    this._store.dispatch(new SharedDepositTypeSubtypeAction.GetAll(typeId));
  }

  goToEdit(deposit: Deposit): void {
    if (deposit.status === Enums.Deposit.StatusEnum.IN_ERROR) {
      this._store.dispatch(new DepositAction.EnableRevision(deposit.resId));
      return;
    }
    let base = RoutesEnum.depositDetail;
    if (this.isValidationMode) {
      base = RoutesEnum.depositToValidateDetail;
    }
    const navigate = new Navigate([base, deposit.resId, DepositRoutesEnum.edit, Enums.Deposit.StepEnum.FILES]);

    if (this.isReEditAvailable(deposit)) {
      this.subscribe(
        StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
          new DepositAction.StartEditingPublication(deposit.resId),
          DepositAction.StartEditingPublicationSuccess,
          resultAction => {
            this._store.dispatch(navigate);
          }),
      );
      return;
    }
    this._store.dispatch(navigate);
  }

  cancelUpdate(deposit: Deposit): void {
    this._keepPolling = true;
    this._createPollingListenerCompletedStatus();
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.cancelUpdate.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.cancelUpdate.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      this.subscribe(
        StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
          new DepositAction.CancelEditingPublication(deposit.resId),
          DepositAction.CancelEditingPublicationSuccess,
          resultAction => {
            this._store.dispatch(new DepositAction.CheckMetadataDifferencesSuccess(resultAction as any, undefined));
            let base = RoutesEnum.depositDetail;
            if (this.isValidationMode) {
              base = RoutesEnum.depositToValidateDetail;
            }
            this._store.dispatch(new Navigate([base, deposit.resId]));
          }),
      );
    }));

  }

  backToList(): void {
    this._store.dispatch(this.backToListNavigate());
  }

  backToListNavigate(): Navigate {
    if (this.isValidationMode) {
      return new Navigate([RoutesEnum.depositToValidate]);
    } else {
      return new Navigate([RoutesEnum.deposit]);
    }
  }

  delete(): void {
    const data = this._storeDialogService.deleteData(this._state);
    this.subscribe(
      this.currentObs.pipe(take(1)),
      (model: Deposit) => {
        data.name = model[this.KEY_PARAM_NAME]?.toString();
        data.resId = model.resId;
      });
    DialogUtil.open(this._dialog, DeleteDialog, data, {
      width: "400px",
    });
  }

  retrieveRomeo(issn: string): void {
    this._store.dispatch(new SharedExternalDataAction.GetRomeoByIssn(issn));
  }

  protected initEditMode(): void {
    this.isEditMode(this._store.selectSnapshot((state: StateModel) => state.router.state).url);
    this.isEditObs = this.urlStateObs.pipe(
      map(url => url.url),
      distinctUntilChanged(),
      map(urlRaw => this.isEditMode(urlRaw)),
      tap(isEdit => {
        if (isTrue(isEdit)) {
          const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
          this._displayBannerEditModeWithSubType(deposit);
        } else {
          this.refreshValidationError();
          this._computeBannerDetailMode();
        }
        this.subscribe(this._zone.onStable.pipe(take(1)), s => setTimeout(() => this._scrollService.scrollToTop(), 0));
      }),
    );
  }

  private isEditMode(urlRaw: string): boolean {
    const urlSplitted = urlRaw.split("?");
    const url = urlSplitted[0];
    this._extractQueryParamsBis(urlSplitted.length > 1 ? urlSplitted[1] : undefined);

    if (this._isEndWithEditRoute(url)) {
      this.isEdit = true;
    } else {
      const indexLastSeparator = url.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
      this.isEdit = this._isEndWithEditRoute(url.substring(0, indexLastSeparator));
    }
    this._changeDetector.detectChanges();
    return this.isEdit;
  }

  private _isEndWithEditRoute(url: string): boolean {
    return url.endsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + environment.routeSegmentEdit);
  }

  private _extractQueryParamsBis(rawQueryParameters: string): void {
    this.urlQueryParameters = UrlQueryParamHelper.getQueryParamMappingObjectFromQueryParamString(rawQueryParameters);
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersObs,
      state: DepositStatusHistoryAction,
      statusEnums: Enums.Deposit.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  private _transfer(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositValidationStructureDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.transfer.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.transfer.message"),
      colorConfirm: ButtonColorEnum.warn,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }), structureId => {
      if (isNullOrUndefinedOrWhiteString(structureId)) {
        return;
      }
      this._store.dispatch(new DepositAction.Transfer(deposit.resId, structureId));
    });
  }

  private _reject(deposit: Deposit, isUpdate: boolean): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: isUpdate ? MARK_AS_TRANSLATABLE("deposit.action.rejectUpdate.title") : MARK_AS_TRANSLATABLE("deposit.action.reject.title"),
      messageToTranslate: isUpdate ? MARK_AS_TRANSLATABLE("deposit.action.rejectUpdate.message") : MARK_AS_TRANSLATABLE("deposit.action.reject.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      withInput: ConfirmDialogInputEnum.TEXTAREA,
      inputRequired: true,
      inputLabelToTranslate: LabelTranslateEnum.reason,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, message => {
      if (!isString(message) || isEmptyString(message)) {
        return;
      }
      this._store.dispatch(new DepositAction.Reject(deposit.resId, message));
    }));
  }

  private _askFeedback(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.askFeedback.title"),
      confirmButtonToTranslate: LabelTranslateEnum.confirm,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      withInput: ConfirmDialogInputEnum.TEXTAREA,
      inputRequired: true,
      inputLabelToTranslate: LabelTranslateEnum.reason,
      colorConfirm: ButtonColorEnum.primary,
      colorCancel: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, message => {
      if (!isString(message) || isEmptyString(message)) {
        return;
      }
      this._store.dispatch(new DepositAction.AskFeedback(deposit.resId, message));
    }));
  }

  private _resume(deposit: Deposit): void {
    this._store.dispatch(new DepositAction.Resume(deposit.resId));
  }

  private _approve(deposit: Deposit, depositFormModel: DepositFormModel): void {
    this._isPendingApproval = true;
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.approve.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.approve.message"),
      confirmButtonToTranslate: LabelTranslateEnum.confirm,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.success,
      colorCancel: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, message => {
      if (isFalse(message)) {
        return;
      }
      deposit = ObjectUtil.clone(deposit);
      deposit.formData = JSON.stringify(depositFormModel);
      this._store.dispatch(new DepositAction.CleanAndSubmit(deposit));
    }, () => {
      this._isPendingApproval = false;
      this._changeDetector.detectChanges();
    }));
  }

  private _sendBackForValidation(deposit: Deposit): void {
    this._store.dispatch(new DepositAction.SendBackForValidation(deposit.resId));
  }

  private _submitForValidation(deposit: Deposit): void {
    const formData: DepositFormModel = isNullOrUndefined(deposit) ? undefined : JSON.parse(deposit.formData);
    const listStructure: string[] = formData?.contributors?.academicStructures;

    if (isNonEmptyArray(listStructure)) {
      this.subscribe(DepositHelper.confirmSubmitDialogObs(this._dialog, () => {
        this._store.dispatch(new DepositAction.SubmitForValidation(deposit.resId));
      }));
    } else {
      this.subscribe(DepositHelper.askValidationStructureForSubmitDialogObs(this._dialog, structureId => {
        const isThesisBefore2010: boolean = formData?.description?.dates
          .filter((d: DepositFormDate) => DepositFormRuleHelper.isDateBefore2010(d.date)).length > 0;
        const isThesisBefore2010OrAuthorBeforeUnige = formData?.contributors?.isBeforeUnige || isThesisBefore2010;
        if (DepositHelper.isThesisAndScienceFacultyAndMissingFiles(this._store, deposit, [structureId], isThesisBefore2010OrAuthorBeforeUnige)) {
          this._displayThesisFilesNeededDialog(false);
          return;
        }
        this._store.dispatch(new DepositAction.SubmitForValidation(deposit.resId, structureId));
      }));
    }
  }

  refreshValidationError(): void {
    if (this.isEdit) {
      return;
    }
    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
    if (isNullOrUndefined(deposit) || deposit.status === Enums.Deposit.StatusEnum.COMPLETED) {
      return;
    }
    this._store.dispatch([
      new DepositAction.ValidateMetadata(deposit.resId, this.isValidationMode
        && [Enums.Deposit.StatusEnum.IN_VALIDATION, Enums.Deposit.StatusEnum.UPDATES_VALIDATION].includes(deposit.status)),
      new DepositAction.CheckDuplicates(deposit.resId),
    ]);
  }

  private _isNotMyDeposit(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.action.isNotMyDeposit.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.action.isNotMyDeposit.message"),
      confirmButtonToTranslate: LabelTranslateEnum.confirm,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, isConfirmed => {
      if (isTrue(isConfirmed)) {
        this._store.dispatch(new DepositAction.NotMyDeposit(deposit.resId));
      }
    }));
  }

  private _addAComment(): void {
    this._scrollService.scrollToBottom();
    if (this.isValidationMode) {
      this.depositCommentsContainer?.depositCommentValidatorFormPresentational?.openCommentForm();
      this.depositCommentsContainer?.depositCommentValidatorFormPresentational?.externalChangeDetector();
    } else {
      this.depositCommentsContainer?.depositCommentFormPresentational?.openCommentForm();
      this.depositCommentsContainer?.depositCommentFormPresentational?.externalChangeDetector();
    }
  }

  private _displayThesisFilesNeededDialog(allowToContinue: boolean): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.thesisNeededFiles.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.warning.thesisNeededFiles.depositor.message"),
      confirmButtonToTranslate: LabelTranslateEnum.submitAFile,
      colorCancel: ButtonColorEnum.primary,
    }, {
      maxWidth: "450px",
    }, isConfirmed => {
      if (!allowToContinue) {
        this.navigateToStep(Enums.Deposit.StepEnum.FILES);
      }
    }));
  }

  private _seeArchive(archiveId: string): string | Navigate {
    const language = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage);
    return new Navigate([archiveId], {
      lang: language,
    });
  }

  private _cloneDeposit(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.clone.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.clone.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      this._store.dispatch(new DepositAction.Clone(deposit));
    }));
  }

  private _changeToCanonical(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.changeToCanonical.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.changeToCanonical.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      this._store.dispatch(new DepositAction.ChangeToCanonical(deposit));
    }));
  }

  private _changeToCompleted(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.changeToCompleted.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.changeToCompleted.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      this._store.dispatch(new DepositAction.ChangeToCompleted(deposit));
    }));
  }

  private _changeToDeleted(deposit: Deposit): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.list.changeToDeleted.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.list.changeToDeleted.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      width: "400px",
      takeOne: true,
    }, (isConfirmed: boolean) => {
      this._store.dispatch(new DepositAction.ChangeToDeleted(deposit));
    }));
  }

  private _createPollingListenerCompletedStatus(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => this._statusShouldContinuePolling(),
      actionToDo: () => {
        let keepCurrentContext = true;
        const status = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.current?.status);
        if (status === Enums.Deposit.StatusEnum.CANCEL_EDITION) {
          keepCurrentContext = false;
        }
        if (status === Enums.Deposit.StatusEnum.COMPLETED) {
          this._keepPolling = false;
        }
        this._store.dispatch(new DepositAction.GetById(this._resId, keepCurrentContext));
      },
    }));
  }

  private _keepPolling: boolean = false;

  private _statusShouldContinuePolling(): boolean {
    const status = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.current?.status);
    return isNotNullNorUndefined(status) && (status === Enums.Deposit.StatusEnum.SUBMITTED || status === Enums.Deposit.StatusEnum.CHECKED || status === Enums.Deposit.StatusEnum.IN_PREPARATION
      || status === Enums.Deposit.StatusEnum.CANCEL_EDITION || status === Enums.Deposit.StatusEnum.IN_EDITION) && this._keepPolling;
  }

  private _showMetadataDifferences(): void {
    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
    if (isNotNullNorUndefined(deposit) && isNotNullNorUndefined(deposit?.status) && (deposit.status === Enums.Deposit.StatusEnum.IN_EDITION
      || deposit.status === Enums.Deposit.StatusEnum.UPDATES_VALIDATION || deposit.status === Enums.Deposit.StatusEnum.FEEDBACK_REQUIRED)) {
      this._store.dispatch(new DepositAction.CheckMetadataDifferences(deposit.resId));
    }
  }
}

interface BannerDisplayInfo {
  color: BannerColorEnum | undefined;
  message: string | undefined;
}
