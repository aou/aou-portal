/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-multiple-import.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {DepositMultipleImportPresentational} from "@app/features/deposit/components/presentationals/deposit-multiple-import/deposit-multiple-import.presentational";
import {DepositImportResultModel} from "@app/features/deposit/models/deposit-import-result.model";
import {DepositAction} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {
  Deposit,
  PublicationImportDTO,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {ErrorDtoUtil} from "@shared/utils/error-dto.util";
import {Observable} from "rxjs";
import {
  filter,
  map,
  take,
} from "rxjs/operators";
import {
  ButtonColorEnum,
  ButtonThemeEnum,
  ClipboardUtil,
  ConfirmDialog,
  DialogUtil,
  ExtraButtonToolbar,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "aou-deposit-multiple-import-routable",
  templateUrl: "./deposit-multiple-import.routable.html",
  styleUrls: ["./deposit-multiple-import.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositMultipleImportRoutable extends SharedAbstractRoutable {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositState);

  listExtraButtons: ExtraButtonToolbar<Deposit>[] = [
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      callback: current => this.importList(this.depositMultipleImportPresentational.generateListPublicationImport()),
      disableCondition: current => isNullOrUndefined(this.depositMultipleImportPresentational?.formArray) || this.depositMultipleImportPresentational.formArray.invalid || this.depositMultipleImportPresentational.formArray.length === 0,
      displayCondition: current => !this.isImportLaunched,
      icon: IconNameEnum.import,
      labelToTranslate: current => LabelTranslateEnum.import,
      order: 30,
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      callback: current => this._continue(),
      displayCondition: current => this.isImportFinished,
      icon: IconNameEnum.next,
      labelToTranslate: current => LabelTranslateEnum.continue,
      order: 30,
    },
  ];

  listResultObs: Observable<MappingObject<string, DepositImportResultModel>> = MemoizedUtil.select(this._store, DepositState, state => state.bulkImportsResult);

  isImportLaunched: boolean = false;
  isImportFinished: boolean = false;

  atLeastOneImportWork: boolean = false;

  mode: ModeEnum = ModeEnum.create;

  @ViewChild("depositMultipleImportPresentational")
  depositMultipleImportPresentational: DepositMultipleImportPresentational;

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _router: Router,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _injector: Injector,
              private readonly _dialog: MatDialog,
              private readonly _notificationService: NotificationService) {
    super();
  }

  private _continue(): void {
    this.backToList();

    if (this.atLeastOneImportWork) {
      DialogUtil.open(this._dialog, ConfirmDialog, {
        titleToTranslate: MARK_AS_TRANSLATABLE("deposit.multiImport.finishedDialog.title"),
        messageToTranslate: MARK_AS_TRANSLATABLE("deposit.multiImport.finishedDialog.message"),
        cancelButtonToTranslate: LabelTranslateEnum.okayIUnderstand,
        colorCancel: ButtonColorEnum.primary,
      }, {
        maxWidth: "90vw",
      });
    }
  }

  backToList(): void {
    this._store.dispatch(this.backToListNavigate());
  }

  backToListNavigate(): Navigate {
    return new Navigate([RoutesEnum.deposit]);
  }

  importList(listPublicationImport: PublicationImportDTO[]): void {
    if (listPublicationImport.length === 0 || this.isImportLaunched) {
      return;
    }
    this.isImportLaunched = true;

    const action = new DepositAction.CreateBulkFromIdentifier(listPublicationImport);
    let index = 0;

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(DepositAction.CreateFromIdentifierFail),
      filter(result => result.result.successful),
      take(listPublicationImport.length),
      map(result => {
        this.isImportFinished = true;
        index++;
        if (index === listPublicationImport.length) {
          this._displayMessageWhenAtLeastOneImportFailed();
        }
        this._changeDetector.detectChanges();
      }),
    ));

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(DepositAction.CreateFromIdentifierSuccess),
      filter(result => result.result.successful),
      take(listPublicationImport.length),
      map(result => {
        this.isImportFinished = true;
        index++;
        if (index === listPublicationImport.length) {
          this._displayMessageWhenAtLeastOneImportFailed();
        }
        this._changeDetector.detectChanges();
      }),
    ));

    this._store.dispatch(action);
  }

  private _displayMessageWhenAtLeastOneImportFailed(): void {
    const bulkImportsResult = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.bulkImportsResult);
    const listIdentifierInError: string[] = [];
    MappingObjectUtil.forEach(bulkImportsResult, (value, key) => {
      if (isNotNullNorUndefinedNorWhiteString(value.error) || isNullOrUndefined(value.deposit)) {
        const errorDtoMessage = ErrorDtoUtil.buildMessageFromAdditionalParameters(value.error);
        if (errorDtoMessage !== null) {
          const isInternalLink = errorDtoMessage.url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
          if (isInternalLink) {
            listIdentifierInError.push(`- ${errorDtoMessage.text} : <a href="${errorDtoMessage.url}">${errorDtoMessage.url}</a>`);
          } else {
            listIdentifierInError.push(`- ${errorDtoMessage.text} : <a href="${errorDtoMessage.url}" target="_blank">${errorDtoMessage.url}</a>`);
          }
        } else {
          listIdentifierInError.push(`- ${value.error.message}`);
        }
      } else {
        this.atLeastOneImportWork = true;
      }
    });

    if (listIdentifierInError.length > 0) {
      this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
          titleToTranslate: MARK_AS_TRANSLATABLE("deposit.multiImport.finishedInErrorDialog.title"),
          messageToTranslate: MARK_AS_TRANSLATABLE("deposit.multiImport.finishedInErrorDialog.message"),
          cancelButtonToTranslate: LabelTranslateEnum.copyToClipboardAndClose,
          paramMessage: {
            listIdentifiers: listIdentifierInError.join("\n"),
          },
          confirmButtonToTranslate: LabelTranslateEnum.close,
          innerHtmlOnMessage: true,
          displayLineReturnOnMessage: true,
        }, {
          maxWidth: "90vw",
        },
        () => {},
        () => {
          ClipboardUtil.copyStringToClipboard(listIdentifierInError.join("\n"));
          this._notificationService.showInformation(LabelTranslateEnum.notificationCopyToClipboard);
        }));
    }
  }

  openDeposit(deposit: Deposit): void {
    this._store.dispatch(new Navigate([RoutesEnum.depositDetail, deposit.resId]));
  }
}

enum ModeEnum {
  create = "create",
  edit = "edit",
}
