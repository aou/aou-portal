/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - redirect-to-deposit-tab-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from "@angular/router";
import {DepositListTabEnum} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {DepositState} from "@deposit/stores/deposit.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {
  AbstractBaseService,
  isNotNullNorUndefinedNorWhiteString,
  MemoizedUtil,
  RouterExtensionService,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class RedirectToDepositTabGuardService extends AbstractBaseService implements CanActivate {
  constructor(private readonly _store: Store,
              private readonly _routerExt: RouterExtensionService) {
    super();
  }

  canActivate(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): boolean {
    const newPath = routerState.url;
    const oldPath = this._oldPath();
    const isDepositToValidate = newPath === `/${AppRoutesEnum.depositToValidate}`;
    const baseUrlList = isDepositToValidate ? AppRoutesEnum.depositToValidate : AppRoutesEnum.deposit;
    let tab = isDepositToValidate ? DepositListTabEnum.in_validation : DepositListTabEnum.in_progress;
    const lastTabVisited = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.currentTab);
    if (newPath === oldPath && isNotNullNorUndefinedNorWhiteString(lastTabVisited)) {
      tab = lastTabVisited;
    }
    this._store.dispatch(new Navigate([baseUrlList, tab]));
    return false;
  }

  private _oldPath(): string {
    let oldPath = this._routerExt.getCurrentUrl();
    const indexOf = oldPath.substring(1).indexOf("/");
    if (indexOf >= 0) {
      oldPath = oldPath.substring(0, indexOf + 1);
    }
    return oldPath;
  }
}
