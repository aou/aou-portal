/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-license.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminLicenseActionNameSpace} from "@admin/license/stores/admin-license.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {License} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  StoreUtil,
} from "solidify-frontend";

export interface AdminLicenseStateModel extends ResourceStateModel<License> {
}

@Injectable()
@State<AdminLicenseStateModel>({
  name: StateEnum.admin_license,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminLicenseState extends ResourceState<AdminLicenseStateModel, License> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminLicenseActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminLicenseDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminLicenseDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminLicense,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.license.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.license.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.license.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminLicenses;
  }

  @Selector()
  static isLoading(state: AdminLicenseStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminLicenseStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminLicenseStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminLicenseStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminLicenseStateModel): boolean {
    return true;
  }
}
