/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-license-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {License} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  PropertyName,
  RegexUtil,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-license-form",
  templateUrl: "./admin-license-form.presentational.html",
  styleUrls: ["./admin-license-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminLicenseFormPresentational extends AbstractFormPresentational<License> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              private readonly _fb: FormBuilder,
              protected readonly _injector: Injector) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.title]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.openLicenseId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.visible]: [true, [SolidifyValidator]],
      [this.formDefinition.sortValue]: ["", [SolidifyValidator]],
      [this.formDefinition.version]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(license: License): void {
    this.form = this._fb.group({
      [this.formDefinition.title]: [license.title, [Validators.required, SolidifyValidator]],
      [this.formDefinition.openLicenseId]: [license.openLicenseId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: [license.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.visible]: [license.visible, [SolidifyValidator]],
      [this.formDefinition.sortValue]: [license.sortValue, [SolidifyValidator]],
      [this.formDefinition.version]: [license.version, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(license: License): License {
    return license;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() licenseGroupId: string;
  @PropertyName() labels: string;
  @PropertyName() title: string;
  @PropertyName() openLicenseId: string;
  @PropertyName() url: string;
  @PropertyName() sortValue: string;
  @PropertyName() version: string;
  @PropertyName() visible: string;
}
