/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-license-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminLicenseCreateRoutable} from "@admin/license/components/routables/admin-license-create/admin-license-create.routable";
import {AdminLicenseDetailEditRoutable} from "@admin/license/components/routables/admin-license-detail-edit/admin-license-detail-edit.routable";
import {AdminLicenseListRoutable} from "@admin/license/components/routables/admin-license-list/admin-license-list.routable";
import {adminLicenseActionNameSpace} from "@admin/license/stores/admin-license.action";
import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
  ResourceExistGuardService,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminLicenseListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.licenseCreate,
    component: AdminLicenseCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AdminRoutesEnum.licenseDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminLicenseDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminLicenseState.currentTitle,
      nameSpace: adminLicenseActionNameSpace,
      resourceState: AdminLicenseState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: AdminRoutesEnum.licenseEdit,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
        component: EmptyContainer,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminLicenseRoutingModule {
}
