/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
} from "@angular/core";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {PermissionUtil} from "@shared/utils/permission.util";
import {
  AbstractHomeRoutable,
  HomeTileModel,
  isEmptyString,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-home-routable",
  templateUrl: "./admin-home.routable.html",
  styleUrls: ["./admin-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminHomeRoutable extends AbstractHomeRoutable {
  protected _userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[] = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.userRoles);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  _tiles: HomeTileModel[] = [
    {
      avatarIcon: IconNameEnum.researchGroup,
      titleToTranslate: LabelTranslateEnum.researchGroups,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.researchGroup.home.subtitle"),
      path: RoutesEnum.adminResearchGroup,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.licenses,
      titleToTranslate: LabelTranslateEnum.licenses,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.license.home.subtitle"),
      path: RoutesEnum.adminLicense,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.structures,
      titleToTranslate: LabelTranslateEnum.structures,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.structure.home.subtitle"),
      path: RoutesEnum.adminStructure,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.users,
      titleToTranslate: LabelTranslateEnum.users,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.user.home.subtitle"),
      path: RoutesEnum.adminUser,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.roles,
      titleToTranslate: LabelTranslateEnum.roles,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.roles.home.subtitle"),
      path: RoutesEnum.adminRole,
      permission: ApplicationRolePermissionEnum.rootPermission,
    },
    {
      avatarIcon: IconNameEnum.events,
      titleToTranslate: LabelTranslateEnum.events,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.events.home.subtitle"),
      path: RoutesEnum.adminApplicationEvent,
      permission: ApplicationRolePermissionEnum.rootPermission,
    },
    {
      avatarIcon: IconNameEnum.globalBanners,
      titleToTranslate: LabelTranslateEnum.globalBanners,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.globalBanner.home.subtitle"),
      path: RoutesEnum.adminGlobalBanner,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.indexFieldAliases,
      titleToTranslate: LabelTranslateEnum.indexFieldAliases,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.indexFieldAlias.home.subtitle"),
      path: RoutesEnum.adminIndexFieldAlias,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.oaiMetadataPrefixes,
      titleToTranslate: this.labelTranslateInterface.oaiPmhOaiMetadataPrefixTileTitle,
      subtitleToTranslate: this.labelTranslateInterface.oaiPmhOaiMetadataPrefixTileSubtitle,
      path: environment.routeAdminOaiMetadataPrefix,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.oaiSets,
      titleToTranslate: this.labelTranslateInterface.oaiPmhOaiSetTileTitle,
      subtitleToTranslate: this.labelTranslateInterface.oaiPmhOaiSetTileSubtitle,
      path: environment.routeAdminOaiSet,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _notificationService: NotificationService,
              protected readonly _injector: Injector) {
    super(_store, _translate, _injector);
  }

  protected _displayTile(tile: HomeTileModel): boolean {
    return PermissionUtil.isUserHavePermission(true, tile.permission, this._userRolesObs)
      && (isEmptyString(this.searchValueInUrl) || this._translate.instant(tile.titleToTranslate).toLowerCase().startsWith(this.searchValueInUrl));
  }
}

