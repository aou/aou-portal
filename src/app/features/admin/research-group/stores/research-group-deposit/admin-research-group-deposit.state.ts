/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-research-group-deposit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminResearchGroupDepositActionNameSpace} from "@admin/research-group/stores/research-group-deposit/admin-research-group-deposit.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {Deposit} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AssociationState,
  AssociationStateModel,
  defaultAssociationStateInitValue,
  NotificationService,
} from "solidify-frontend";

export interface AdminResearchGroupDepositStateModel extends AssociationStateModel<Deposit> {
}

@Injectable()
@State<AdminResearchGroupDepositStateModel>({
  name: StateEnum.admin_researchGroup_deposit,
  defaults: {
    ...defaultAssociationStateInitValue(),
  },
})
export class AdminResearchGroupDepositState extends AssociationState<AdminResearchGroupDepositStateModel, Deposit> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminResearchGroupDepositActionNameSpace,
      resourceName: ApiResourceNameEnum.PUBLICATIONS,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminResearchGroups;
  }
}
