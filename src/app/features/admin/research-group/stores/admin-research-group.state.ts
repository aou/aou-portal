/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-research-group.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminResearchGroupAction,
  adminResearchGroupActionNameSpace,
} from "@admin/research-group/stores/admin-research-group.action";
import {
  AdminResearchGroupDepositState,
  AdminResearchGroupDepositStateModel,
} from "@admin/research-group/stores/research-group-deposit/admin-research-group-deposit.state";
import {Injectable} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {ResearchGroup} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedResearchGroupAction} from "@shared/stores/research-group/shared-research-group.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  Result,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface AdminResearchGroupStateModel extends ResourceStateModel<ResearchGroup> {
  [StateEnum.admin_researchGroup_deposit]: AdminResearchGroupDepositStateModel;
}

@Injectable()
@State<AdminResearchGroupStateModel>({
  name: StateEnum.admin_researchGroup,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.admin_researchGroup_deposit]: {...defaultAssociationStateInitValue()},
  },
  children: [
    AdminResearchGroupDepositState,
  ],
})
export class AdminResearchGroupState extends ResourceState<AdminResearchGroupStateModel, ResearchGroup> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminResearchGroupActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminResearchGroupDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminResearchGroupDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminResearchGroup,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.researchGroup.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.researchGroup.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.researchGroup.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminResearchGroups;
  }

  @Selector()
  static isLoading(state: AdminResearchGroupStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminResearchGroupStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentName(state: AdminResearchGroupStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminResearchGroupStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminResearchGroupStateModel): boolean {
    return true;
  }

  @Action(AdminResearchGroupAction.Synchronize)
  synchronize(ctx: SolidifyStateContext<AdminResearchGroupStateModel>, action: AdminResearchGroupAction.Synchronize): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.SYNCHRONIZE).pipe(
      tap(result => {
        if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
          ctx.dispatch(new AdminResearchGroupAction.SynchronizeSuccess(action, result));
        } else {
          ctx.dispatch(new AdminResearchGroupAction.SynchronizeFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AdminResearchGroupAction.SynchronizeFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(AdminResearchGroupAction.SynchronizeSuccess)
  synchronizeSuccess(ctx: SolidifyStateContext<AdminResearchGroupStateModel>, action: AdminResearchGroupAction.SynchronizeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    ctx.dispatch(new AdminResearchGroupAction.GetAll());
  }

  @Action(AdminResearchGroupAction.SynchronizeFail)
  synchronizeFail(ctx: SolidifyStateContext<AdminResearchGroupStateModel>, action: AdminResearchGroupAction.SynchronizeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.researchGroup.notification.synchronize.fail"));
  }

  @Action(AdminResearchGroupAction.Validate)
  validate(ctx: SolidifyStateContext<AdminResearchGroupStateModel>, action: AdminResearchGroupAction.Validate): Observable<ResearchGroup> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<void, ResearchGroup>(this._urlResource + urlSeparator + action.researchGroupId + urlSeparator + ApiActionNameEnum.VALIDATE)
      .pipe(
        tap(researchGroup => {
          ctx.dispatch(new AdminResearchGroupAction.ValidateSuccess(action, researchGroup));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminResearchGroupAction.ValidateFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminResearchGroupAction.ValidateSuccess)
  validateSuccess(ctx: SolidifyStateContext<AdminResearchGroupStateModel>, action: AdminResearchGroupAction.ValidateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.researchGroup,
    });
    ctx.dispatch(new SharedResearchGroupAction.Clean(false));
    this._notificationService.showSuccess(LabelTranslateEnum.researchGroupValidated);
  }

  @Action(AdminResearchGroupAction.ValidateFail)
  validateFail(ctx: SolidifyStateContext<AdminResearchGroupStateModel>, action: AdminResearchGroupAction.ValidateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToValidateTheResearchGroup);
  }
}
