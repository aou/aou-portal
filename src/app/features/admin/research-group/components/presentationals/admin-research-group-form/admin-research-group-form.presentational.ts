/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-research-group-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {ResearchGroup} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-research-group-form",
  templateUrl: "./admin-research-group-form.presentational.html",
  styleUrls: ["./admin-research-group-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminResearchGroupFormPresentational extends AbstractFormPresentational<ResearchGroup> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.code]: ["", [SolidifyValidator]],
      [this.formDefinition.acronym]: ["", [SolidifyValidator]],
      [this.formDefinition.active]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(researchGroup: ResearchGroup): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [researchGroup.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.code]: [researchGroup.code, [SolidifyValidator]],
      [this.formDefinition.acronym]: [researchGroup.acronym, [SolidifyValidator]],
      [this.formDefinition.active]: [researchGroup.active, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(researchGroup: ResearchGroup): ResearchGroup {
    return researchGroup;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() code: string;
  @PropertyName() acronym: string;
  @PropertyName() active: string;
}
