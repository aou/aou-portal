/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-research-group-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminResearchGroupAction,
  adminResearchGroupActionNameSpace,
} from "@admin/research-group/stores/admin-research-group.action";
import {
  AdminResearchGroupState,
  AdminResearchGroupStateModel,
} from "@admin/research-group/stores/admin-research-group.state";
import {AdminResearchGroupDepositAction} from "@admin/research-group/stores/research-group-deposit/admin-research-group-deposit.action";
import {AdminResearchGroupDepositState} from "@admin/research-group/stores/research-group-deposit/admin-research-group-deposit.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {AppDepositSubtypeState} from "@app/stores/deposit-subtype/app-deposit-subtype.state";
import {Enums} from "@enums";
import {
  Deposit,
  ResearchGroup,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {DepositListHelper} from "@shared/helpers/deposit-list.helper";
import {sharedResearchGroupActionNameSpace} from "@shared/stores/research-group/shared-research-group.action";
import {Observable} from "rxjs";
import {
  filter,
  map,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  ButtonColorEnum,
  ButtonThemeEnum,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  ExtraButtonToolbar,
  isFalse,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  KeyValue,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-research-group-detail-edit-routable",
  templateUrl: "./admin-research-group-detail-edit.routable.html",
  styleUrls: ["./admin-research-group-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminResearchGroupDetailEditRoutable extends AbstractDetailEditCommonRoutable<ResearchGroup, AdminResearchGroupStateModel> implements OnInit {
  @Select(AdminResearchGroupState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminResearchGroupState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override getByIdIfAlreadyInState: boolean = true;
  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedResearchGroupActionNameSpace;
  actions: DataTableActions<Deposit>[];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  listExtraButtons: ExtraButtonToolbar<ResearchGroup>[] = [
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.approve,
      displayCondition: current => isNotNullNorUndefined(current) && (isNullOrUndefined(current.validated) || isFalse(current.validated)),
      callback: (current) => this._validate(current),
      labelToTranslate: (current) => LabelTranslateEnum.validate,
      order: 40,
    },
  ];

  readonly KEY_PARAM_NAME: keyof ResearchGroup & string = "name";

  listDepositObs: Observable<Deposit[]> = MemoizedUtil.selected(this._store, AdminResearchGroupDepositState).pipe(
    filter(listDeposit => isNotNullNorUndefined(listDeposit)),
    map((listDeposit: Deposit[]) => DepositListHelper.improveListDepositWithMetadata(listDeposit)),
  );
  queryParametersDepositObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, AdminResearchGroupDepositState);

  columns: DataTableColumns<Deposit>[] = [
    ...DepositListHelper.columns(MemoizedUtil.listSnapshot(this._store, AppDepositSubtypeState)),
    {
      field: "status",
      header: LabelTranslateEnum.status,
      type: DataTableFieldTypeEnum.singleSelect,
      order: OrderEnum.none,
      isFilterable: true,
      isSortable: true,
      translate: true,
      filterEnum: Enums.Deposit.StatusEnumTranslate.filter(s => isFalse(s.hideInFilter) || isNullOrUndefined(s.hideInFilter)) as KeyValue[],
      component: DataTableComponentHelper.get(DataTableComponentEnum.status),
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              protected readonly _dialog: MatDialog) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_researchGroup, _injector, adminResearchGroupActionNameSpace, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.actions = this.defineActions();
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new AdminResearchGroupDepositAction.GetAll(id));
  }

  private _validate(researchGroup: ResearchGroup): void {
    this._store.dispatch(new AdminResearchGroupAction.Validate(researchGroup.resId));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new AdminResearchGroupDepositAction.GetAll(this._resId, queryParameters));
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  showDetail(deposit: Deposit): void {
    this._store.dispatch(new Navigate([RoutesEnum.depositToValidateDetail, deposit.resId]));
  }

  protected defineActions(): DataTableActions<Deposit>[] {
    return [
      {
        logo: IconNameEnum.internalLink,
        callback: (deposit: Deposit) => this._seeArchive(deposit.archiveId),

        placeholder: current => LabelTranslateEnum.seeArchive,
        displayOnCondition: (deposit: Deposit) => isNotNullNorUndefinedNorWhiteString(deposit.archiveId),
        isWrapped: false,
        isVisible: true,
      },
    ];
  }

  private _seeArchive(archiveId: string): void {
    const language = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage);
    this._store.dispatch(new Navigate([archiveId], {
      lang: language,
    }));
  }
}
