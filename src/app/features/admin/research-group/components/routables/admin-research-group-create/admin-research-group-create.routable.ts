/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-research-group-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminResearchGroupActionNameSpace} from "@admin/research-group/stores/admin-research-group.action";
import {
  AdminResearchGroupState,
  AdminResearchGroupStateModel,
} from "@admin/research-group/stores/admin-research-group.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {ResearchGroup} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedResearchGroupActionNameSpace} from "@shared/stores/research-group/shared-research-group.action";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AbstractFormPresentational,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-research-group-create-routable",
  templateUrl: "./admin-research-group-create.routable.html",
  styleUrls: ["./admin-research-group-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminResearchGroupCreateRoutable extends AbstractCreateRoutable<ResearchGroup, AdminResearchGroupStateModel> {
  @Select(AdminResearchGroupState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminResearchGroupState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedResearchGroupActionNameSpace;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<ResearchGroup>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_researchGroup, _injector, adminResearchGroupActionNameSpace, StateEnum.admin);
  }
}
