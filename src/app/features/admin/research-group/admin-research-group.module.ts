/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-research-group.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminResearchGroupRoutingModule} from "@admin/research-group/admin-research-group-routing.module";
import {AdminResearchGroupFormPresentational} from "@admin/research-group/components/presentationals/admin-research-group-form/admin-research-group-form.presentational";
import {AdminResearchGroupCreateRoutable} from "@admin/research-group/components/routables/admin-research-group-create/admin-research-group-create.routable";
import {AdminResearchGroupDetailEditRoutable} from "@admin/research-group/components/routables/admin-research-group-detail-edit/admin-research-group-detail-edit.routable";
import {AdminResearchGroupListRoutable} from "@admin/research-group/components/routables/admin-research-group-list/admin-research-group-list.routable";
import {AdminResearchGroupState} from "@admin/research-group/stores/admin-research-group.state";
import {AdminResearchGroupDepositState} from "@admin/research-group/stores/research-group-deposit/admin-research-group-deposit.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminResearchGroupCreateRoutable,
  AdminResearchGroupDetailEditRoutable,
  AdminResearchGroupListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminResearchGroupFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminResearchGroupRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminResearchGroupState,
      AdminResearchGroupDepositState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminResearchGroupModule {
}
