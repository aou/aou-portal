/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-user-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminUserMergeUserDialog} from "@admin/user/components/dialogs/admin-user-merge-user/admin-user-merge-user.dialog";
import {
  AdminUserAction,
  adminUserActionNameSpace,
} from "@admin/user/stores/admin-user.action";
import {
  AdminUserState,
  AdminUserStateModel,
} from "@admin/user/stores/admin-user.state";
import {AdminPersonState} from "@admin/user/stores/person/admin-person.state";
import {AdminPersonResearchGroupAction} from "@admin/user/stores/person/person-research-group/admin-person-research-group.action";
import {AdminPersonResearchGroupState} from "@admin/user/stores/person/person-research-group/admin-person-research-group.state";
import {AdminPersonRoleAction} from "@admin/user/stores/person/person-role/admin-person-role.action";
import {AdminPersonRoleState} from "@admin/user/stores/person/person-role/admin-person-role.state";
import {AdminPersonStructureAction} from "@admin/user/stores/person/person-structure/admin-person-structure.action";
import {AdminPersonStructureState} from "@admin/user/stores/person/person-structure/admin-person-structure.state";
import {AdminPersonValidationRightStructureAction} from "@admin/user/stores/person/person-validation-right-structure/admin-person-validation-right-structure.action";
import {AdminPersonValidationRightStructureState} from "@admin/user/stores/person/person-validation-right-structure/admin-person-validation-right-structure.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {
  Person,
  ResearchGroup,
  Role,
  Structure,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {SecurityService} from "@shared/services/security.service";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {SharedRoleAction} from "@shared/stores/role/shared-role.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {Observable} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  ButtonColorEnum,
  ButtonThemeEnum,
  DialogUtil,
  ExtraButtonToolbar,
  isFalse,
  isNotNullNorUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  OrderEnum,
  QueryParameters,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-user-detail-edit-routable",
  templateUrl: "./admin-user-detail-edit.routable.html",
  styleUrls: ["./admin-user-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminUserDetailEditRoutable extends AbstractDetailEditCommonRoutable<User, AdminUserStateModel> implements OnInit {
  @Select(AdminUserState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminUserState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  listPersonObs: Observable<Person[]> = MemoizedUtil.list(this._store, SharedPersonState);
  currentUserObs: Observable<User> = MemoizedUtil.current(this._store, AppUserState);
  personObs: Observable<Person> = MemoizedUtil.current(this._store, AdminPersonState);
  selectedStructureObs: Observable<Structure[]> = MemoizedUtil.selected(this._store, AdminPersonStructureState);
  selectedResearchGroupObs: Observable<ResearchGroup[]> = MemoizedUtil.selected(this._store, AdminPersonResearchGroupState);
  selectedRoleObs: Observable<Role[]> = MemoizedUtil.selected(this._store, AdminPersonRoleState);
  selectedValidationRightObs: Observable<ValidationRight[]> = MemoizedUtil.selected(this._store, AdminPersonValidationRightStructureState);
  businessRolesObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedUserActionNameSpace;

  readonly KEY_PARAM_NAME: keyof User & string = "externalUid";

  listExtraButtons: ExtraButtonToolbar<User>[] = [
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.button,
      icon: IconNameEnum.mergeUser,
      displayCondition: current => isNotNullNorUndefined(current) && isFalse(this.isEdit) && this._securityService.isRootOrAdmin(),
      callback: (current) => this._openDialogMergeUser(current),
      labelToTranslate: (current) => LabelTranslateEnum.mergeUser,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService,
  ) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_user, _injector, adminUserActionNameSpace, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
      field: "sortValue",
      order: OrderEnum.ascending,
    });
    this._store.dispatch(new SharedRoleAction.GetAll(queryParameters));
    this.subscribe(this.currentUserObs.pipe(
      filter(user => isNotNullNorUndefined(user)),
      take(1),
      tap(currentUser => {
        this._refreshCurrentUserWhenUpdated(currentUser);
      }),
    ));
  }

  _getSubResourceWithParentId(id: string): void {
    this.subscribe(this.currentObs.pipe(
      filter(user => isNotNullNorUndefined(user) && user.resId === id),
      tap(user => {
        if (isNotNullNorUndefined(user?.person?.resId)) {
          this._store.dispatch(new AdminPersonStructureAction.GetAll(user.person.resId));
          this._store.dispatch(new AdminPersonResearchGroupAction.GetAll(user.person.resId));
          this._store.dispatch(new AdminPersonRoleAction.GetAll(user.person.resId));
          this._store.dispatch(new AdminPersonValidationRightStructureAction.GetAll(user.person.resId));
        }
      }),
    ));
  }

  private _refreshCurrentUserWhenUpdated(currentUser: User): void {
    if (currentUser?.resId === this._resId) {
      this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(AdminUserAction.UploadFileSuccess))
        .pipe(
          tap(result => {
            this._store.dispatch(new AppUserAction.GetCurrentUser());
          }),
        ));
      this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(AdminUserAction.DeleteFileSuccess))
        .pipe(
          tap(result => {
            this._store.dispatch(new AppUserAction.GetCurrentUser());
          }),
        ));
    }
  }

  navigate($event: string[]): void {
    this._store.dispatch(new Navigate($event));
  }

  private _openDialogMergeUser(user: User): void {
    this.subscribe(DialogUtil.open(this._dialog, AdminUserMergeUserDialog, {
      currentUser: user,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
    }, userIdToMerge => {
      this._store.dispatch(new AdminUserAction.MergeUser(user.resId, userIdToMerge));
    }));
  }
}
