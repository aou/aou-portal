/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-user-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminUserAction} from "@admin/user/stores/admin-user.action";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {
  Person,
  ResearchGroup,
  Role,
  Structure,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {SecurityService} from "@shared/services/security.service";
import {sharedResearchGroupActionNameSpace} from "@shared/stores/research-group/shared-research-group.action";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {
  AbstractApplicationFormPresentational,
  BaseFormDefinition,
  DateUtil,
  isEmptyString,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  ObjectUtil,
  OrcidService,
  OrderEnum,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-user-form",
  templateUrl: "./admin-user-form.presentational.html",
  styleUrls: ["./admin-user-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminUserFormPresentational extends AbstractApplicationFormPresentational<User> {
  formDefinition: AdminUserFormFormComponentFormDefinition = new AdminUserFormFormComponentFormDefinition();
  applicationRolesNames: KeyValue[] = Enums.UserApplicationRole.UserApplicationRoleEnumTranslate;

  userAvatarActionNameSpace: ResourceFileNameSpace = AdminUserAction;
  userState: typeof AdminUserState = AdminUserState;

  @Input()
  listPersons: Person[];

  @Input()
  currentUser: User;

  @Input()
  selectedStructure: Structure[];

  @Input()
  selectedResearchGroup: ResearchGroup[];

  @Input()
  selectedRole: Role[];

  @Input()
  businessRoles: Role[];

  @Input()
  selectedValidationRight: ValidationRight[];

  sharedResearchGroupSort: Sort<ResearchGroup> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedResearchGroupActionNameSpace: ResourceNameSpace = sharedResearchGroupActionNameSpace;
  sharedResearchGroupState: typeof SharedResearchGroupState = SharedResearchGroupState;
  researchGroupLabel: (value: ResearchGroup) => string = value => SharedResearchGroupState.labelCallback(value, this._translateService);
  researchGroupPreTreatmentHighlightText: (result: string) => string = SharedResearchGroupState.preTreatmentHighlightText;
  researchGroupPostTreatmentHighlightText: (result: string, resultBeforePreTreatment: string) => string = SharedResearchGroupState.postTreatmentHighlightText;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _orcidService: OrcidService,
              private readonly _translateService: TranslateService,
              private readonly _securityService: SecurityService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(user: User): void {
    const formArrayValidationRights = this._fb.array([]);

    this.form = this._fb.group({
      [this.formDefinition.externalUid]: [user.externalUid, [Validators.required, SolidifyValidator]],
      [this.formDefinition.firstName]: [user.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [user.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.homeOrganization]: [user.homeOrganization, [Validators.required, SolidifyValidator]],
      [this.formDefinition.email]: [user.email, [Validators.required, SolidifyValidator]],
      [this.formDefinition.person]: [this.person?.resId],
      [this.formDefinition.applicationRole]: [user.applicationRole.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastLoginIpAddress]: [user.lastLoginIpAddress, [SolidifyValidator]],
      [this.formDefinition.lastLoginTime]: [DateUtil.convertDateToDateTimeString(new Date(user.lastLoginTime)), [SolidifyValidator]],
      [this.formDefinition.orcid]: [this.person?.orcid, [SolidifyValidator]],
      [this.formDefinition.researchGroups]: [this.selectedResearchGroup?.map(r => r.resId)],
      [this.formDefinition.roles]: [this.selectedRole?.map(r => r.resId)],
      [this.formDefinition.structures]: [this.selectedStructure?.map(r => r.resId)],
      [this.formDefinition.validationRight]: formArrayValidationRights,
    });

    if (isNonEmptyArray(this.selectedValidationRight)) {
      this.selectedValidationRight.forEach(validationRight => {
        const listId = validationRight.publicationSubtypes.map(r => r.resId);
        const formGroupValidationRight = this._fb.group({
          id: [validationRight.resId, Validators.required],
          listId: [listId],
        });
        formArrayValidationRights.push(formGroupValidationRight as any);
      });
    }
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.externalUid]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.homeOrganization]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.email]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.person]: [""],
      [this.formDefinition.applicationRole]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: ["", [SolidifyValidator]],
      [this.formDefinition.researchGroups]: [[]],
      [this.formDefinition.roles]: [[]],
      [this.formDefinition.structures]: [[]],
      [this.formDefinition.validationRight]: this._fb.array([]),
    });
  }

  protected _treatmentBeforeSubmit(user: User): User {
    delete user.lastLoginTime;
    delete user.lastLoginIpAddress;
    user.applicationRole = {resId: this.form.get(this.formDefinition.applicationRole).value};
    // create a new person with the corresponding resId, if needed.
    const personResId: string = user.person as any;
    if (isNullOrUndefined(personResId) || isEmptyString(personResId)) {
      user.person = null;
    } else {
      user.person = {resId: personResId};
    }
    return user;
  }

  getUser(): User {
    if (isNullOrUndefined(this.form)) {
      return this.model;
    }
    const formValue = this.form.value;
    // Hack to avoid weird error on avatar due to invalid form value
    const keys = ObjectUtil.keys(formValue);
    if (keys.length === 1 && keys[0] === this.formDefinition.validationRight) {
      return this.model;
    }
    return formValue as User;
  }

  get person(): Person | undefined {
    if (isNullOrUndefined(this.model?.person)) {
      return undefined;
    }
    return this.model.person;
  }

  get personResId(): string {
    return this.person?.resId;
  }

  get isAvatarPresent(): boolean {
    return isNotNullNorUndefined(this.model?.person?.avatar);
  }

  hasRole(roleId: string): boolean {
    if (!isNullOrUndefined(this.selectedRole)) {
      const roleIds: string[] = this.selectedRole.map(r => r.resId);
      return roleIds.indexOf(roleId) !== -1;
    }
    return false;
  }

  updateSelectedRoles(roleId: string, checked: boolean): void {
    let fieldValues: string[] = this.form.get(this.formDefinition.roles).value;
    if (isNullOrUndefined(fieldValues)) {
      fieldValues = [];
    }
    const index = fieldValues.indexOf(roleId);
    if (checked && index === -1) {
      fieldValues.push(roleId);
    } else if (!checked && index !== -1) {
      fieldValues.splice(index, 1);
    }
    this.form.get(this.formDefinition.roles).setValue(fieldValues);
    this.form.markAsDirty();
  }

  goToOrcid(orcid: string): void {
    this._orcidService.openOrcidPage(orcid);
  }

  navigateToResearchGroup(researchGroup: ResearchGroup): void {
    this.navigate([RoutesEnum.adminResearchGroupDetail, researchGroup.resId]);
  }
}

export class AdminUserFormFormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() externalUid: string;
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() homeOrganization: string;
  @PropertyName() email: string;
  @PropertyName() person: string;
  @PropertyName() accessToken: string;
  @PropertyName() refreshToken: string;
  @PropertyName() applicationRole: string;
  @PropertyName() lastLoginIpAddress: string;
  @PropertyName() lastLoginTime: string;
  @PropertyName() orcid: string;
  @PropertyName() structures: string;
  @PropertyName() validationRight: string;
  @PropertyName() researchGroups: string;
  @PropertyName() roles: string;
}
