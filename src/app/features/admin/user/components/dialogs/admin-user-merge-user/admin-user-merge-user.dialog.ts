/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-user-merge-user.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  FormControl,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {User} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  FormValidationHelper,
  MARK_AS_TRANSLATABLE,
  OrderEnum,
  ResourceNameSpace,
  SolidifyForbiddenValuesValidator,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-user-merge-user-dialog",
  templateUrl: "./admin-user-merge-user.dialog.html",
  styleUrls: ["./admin-user-merge-user.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminUserMergeUserDialog extends SharedAbstractDialog<AdminUserMergeUserDialogData, string> {
  formControl: FormControl = new FormControl(undefined, [Validators.required, SolidifyValidator]);

  sharedUserSort: Sort<User> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedUserNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  sharedUserState: typeof SharedUserState = SharedUserState;
  userLabel: (value: User) => string = user => user.lastName + ", " + user.firstName;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(private readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<AdminUserMergeUserDialog>,
              private readonly _translateService: TranslateService,
              @Inject(MAT_DIALOG_DATA) public readonly data: AdminUserMergeUserDialogData) {
    super(_dialogRef, data);

    this.formControl.addValidators(SolidifyForbiddenValuesValidator(_translateService, [data.currentUser.resId], MARK_AS_TRANSLATABLE("admin.user.dialog.mergeUser.formError.notThisUser")));
  }

  confirm(): void {
    this.submit(this.formControl.value);
  }
}

export interface AdminUserMergeUserDialogData {
  currentUser?: User;
}

