/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminPersonActionNameSpace} from "@admin/user/stores/person/admin-person.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {Person} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  NotificationService,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
} from "solidify-frontend";

export const defaultAdminPersonInitValue: () => AdminPersonStateModel = () =>
  ({
    ...defaultResourceFileStateInitValue(),
    // [StateEnum.admin_person_structure]: {...defaultAssociationStateInitValue()},
    // [StateEnum.admin_person_researchGroup]: {...defaultAssociationStateInitValue()},
  });

export interface AdminPersonStateModel extends ResourceFileStateModel<Person> {
  // [StateEnum.admin_person_structure]: AdminPersonStructureStateModel;
  // [StateEnum.admin_person_researchGroup]: AdminPersonResearchGroupStateModel;
}

@Injectable()
@State<AdminPersonStateModel>({
  name: StateEnum.admin_person,
  defaults: {
    ...defaultAdminPersonInitValue(),
  },
  children: [
    // AdminPersonStructureState,
    // AdminPersonResearchGroupState,
  ],
})
export class AdminPersonState extends ResourceFileState<AdminPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminPersonActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }
}
