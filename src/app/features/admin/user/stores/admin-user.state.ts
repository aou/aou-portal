/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-user.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminUserFormFormComponentFormDefinition} from "@admin/user/components/presentationals/admin-user-form/admin-user-form.presentational";
import {
  AdminUserAction,
  adminUserActionNameSpace,
} from "@admin/user/stores/admin-user.action";
import {AdminPersonAction} from "@admin/user/stores/person/admin-person.action";
import {
  AdminPersonState,
  AdminPersonStateModel,
  defaultAdminPersonInitValue,
} from "@admin/user/stores/person/admin-person.state";
import {AdminPersonResearchGroupAction} from "@admin/user/stores/person/person-research-group/admin-person-research-group.action";
import {AdminPersonResearchGroupState} from "@admin/user/stores/person/person-research-group/admin-person-research-group.state";
import {AdminPersonRoleAction} from "@admin/user/stores/person/person-role/admin-person-role.action";
import {AdminPersonRoleState} from "@admin/user/stores/person/person-role/admin-person-role.state";
import {AdminPersonStructureAction} from "@admin/user/stores/person/person-structure/admin-person-structure.action";
import {
  AdminPersonStructureState,
  AdminPersonStructureStateModel,
} from "@admin/user/stores/person/person-structure/admin-person-structure.state";
import {AdminPersonValidationRightStructureAction} from "@admin/user/stores/person/person-validation-right-structure/admin-person-validation-right-structure.action";
import {
  AdminPersonValidationRightStructureState,
  AdminPersonValidationRightStructureStateModel,
} from "@admin/user/stores/person/person-validation-right-structure/admin-person-validation-right-structure.state";
import {Injectable} from "@angular/core";
import {Enums} from "@app/enums";
import {ApiActionNameEnum} from "@app/shared/enums/api-action-name.enum";
import {AppPersonValidationRightStructureAction} from "@app/stores/person/validation-right/app-person-validation-right-structure.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {
  Person,
  User,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {SharedPersonAction} from "@shared/stores/person/shared-person.action";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultRelation3TiersStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  Result,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultAdminUserInitValue: () => AdminUserStateModel = () =>
  ({
    ...defaultResourceFileStateInitValue(),
    [StateEnum.admin_person]: {...defaultAdminPersonInitValue()},
    [StateEnum.admin_person_structure]: {...defaultAssociationStateInitValue()},
    [StateEnum.admin_person_validationRightStructure]: {...defaultRelation3TiersStateInitValue()},
  });

export interface AdminUserStateModel extends ResourceFileStateModel<User> {
  [StateEnum.admin_person]: AdminPersonStateModel;
  [StateEnum.admin_person_structure]: AdminPersonStructureStateModel;
  [StateEnum.admin_person_validationRightStructure]: AdminPersonValidationRightStructureStateModel;
}

@Injectable()
@State<AdminUserStateModel>({
  name: StateEnum.admin_user,
  defaults: {
    ...defaultAdminUserInitValue(),
  },
  children: [
    AdminPersonState,
    AdminPersonStructureState,
    AdminPersonResearchGroupState,
    AdminPersonRoleState,
    AdminPersonValidationRightStructureState,
  ],
})
export class AdminUserState extends ResourceFileState<AdminUserStateModel, User> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminUserActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminUserDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminUserDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminUser,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.user.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.user.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.user.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.PARALLEL,
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminUsers;
  }

  protected get _urlFileResource(): string {
    return ApiEnum.adminPeople;
  }

  @Selector()
  static isLoading(state: AdminUserStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminUserStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state?.[StateEnum.admin_person_structure])
      || StoreUtil.isLoadingState(state?.[StateEnum.admin_person_researchGroup])
      || StoreUtil.isLoadingState(state?.[StateEnum.admin_person_validationRightStructure]);
  }

  @Selector()
  static currentTitle(state: AdminUserStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.externalUid;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminUserStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !this.isLoadingWithDependency(state);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminUserStateModel): boolean {
    return true;
  }

  @Action(AdminUserAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedPersonAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.GetAllFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AdminUserAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new AdminUserAction.LoadResourceFail(action));
        }
        return result.success;
      }),
    );
  }

  @OverrideDefaultAction()
  @Action(AdminUserAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);

    if (isNotNullNorUndefined(action.model.person?.avatar)) {
      ctx.dispatch(new AdminUserAction.GetFile(action.model.person.resId));
    }
  }

  protected override _getListActionsUpdateSubResource(model: User, action: AdminUserAction.Create | AdminUserAction.Update, ctx: SolidifyStateContext<AdminUserStateModel>): ActionSubActionCompletionsWrapper[] {
    const user = ctx.getState().current;
    const actions = super._getActionUpdateFile(user.person, action, ctx);
    const personId = action.modelFormControlEvent?.model?.person?.resId;
    if (isNonEmptyString(personId)) {
      const formDefinition = new AdminUserFormFormComponentFormDefinition();
      const formControl = action.modelFormControlEvent.formControl;
      const personInfo: Person = {
        resId: personId,
        orcid: formControl.get(formDefinition.orcid).value,
        firstName: formControl.get(formDefinition.firstName).value,
        lastName: formControl.get(formDefinition.lastName).value,
      };

      actions.push({
        action: new AdminPersonAction.Update({
          formControl: formControl,
          model: personInfo,
          changeDetectorRef: action.modelFormControlEvent.changeDetectorRef,
        }),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonAction.UpdateFail)),
        ],
      });

      const listResearchGroup = action.modelFormControlEvent.formControl.get(formDefinition.researchGroups).value as string[];
      if (isNotNullNorUndefined(listResearchGroup)) {
        actions.push({
          action: new AdminPersonResearchGroupAction.Update(personId, listResearchGroup),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonResearchGroupAction.UpdateSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonResearchGroupAction.UpdateFail)),
          ],
        });
      }

      const listStructure = action.modelFormControlEvent.formControl.get(formDefinition.structures).value as string[];
      if (isNotNullNorUndefined(listStructure)) {
        actions.push({
          action: new AdminPersonStructureAction.Update(personId, listStructure),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonStructureAction.UpdateSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonStructureAction.UpdateFail)),
          ],
        });
      }

      const listValidationRights: ValidationRight[] = action.modelFormControlEvent.formControl.get(formDefinition.validationRight).value as ValidationRight[];
      actions.push({
        action: new AdminPersonValidationRightStructureAction.Update(personId, listValidationRights as any),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonValidationRightStructureAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonValidationRightStructureAction.UpdateFail)),
        ],
      });

      const listRoleIds: string[] = action.modelFormControlEvent.formControl.get(formDefinition.roles).value as string[];
      actions.push({
        action: new AdminPersonRoleAction.Update(personId, listRoleIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonRoleAction.UpdateFail)),
        ],
      });
    }
    return actions;
  }

  @OverrideDefaultAction()
  @Action(AdminUserAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.UpdateSuccess): void {
    super.updateSuccess(ctx, action);
    const isCurrentUserUpdated = action.model.resId === MemoizedUtil.currentSnapshot(this._store, AppUserState)?.resId;
    if (isCurrentUserUpdated) {
      ctx.dispatch(new AppPersonValidationRightStructureAction.GetAll(action.model.person.resId, new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo)));
    }
  }

  @Action(AdminUserAction.Synchronize)
  synchronize(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.Synchronize): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.SYNCHRONIZE).pipe(
      tap(result => {
        if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
          ctx.dispatch(new AdminUserAction.SynchronizeSuccess(action, result));
        } else {
          ctx.dispatch(new AdminUserAction.SynchronizeFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AdminUserAction.SynchronizeFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(AdminUserAction.SynchronizeSuccess)
  synchronizeSuccess(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.SynchronizeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    ctx.dispatch(new AdminUserAction.GetAll());
  }

  @Action(AdminUserAction.SynchronizeFail)
  synchronizeFail(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.SynchronizeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.user.notification.synchronize.fail"));
  }

  @Action(AdminUserAction.MergeUser)
  mergeUser(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.MergeUser): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.userId + urlSeparator + ApiActionNameEnum.MERGE, action.userIdToMerge)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new AdminUserAction.MergeUserSuccess(action, result));
          } else {
            ctx.dispatch(new AdminUserAction.MergeUserFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminUserAction.MergeUserFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminUserAction.MergeUserSuccess)
  mergeUserSuccess(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.MergeUserSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    ctx.dispatch(new AdminUserAction.GetAll());
  }

  @Action(AdminUserAction.MergeUserFail)
  mergeUserFail(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.MergeUserFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.user.notification.mergeUser.fail"));
  }
}
