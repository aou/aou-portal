/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-user-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminUserCreateRoutable} from "@admin/user/components/routables/admin-user-create/admin-user-create.routable";
import {AdminUserDetailEditRoutable} from "@admin/user/components/routables/admin-user-detail-edit/admin-user-detail-edit.routable";
import {AdminUserListRoutable} from "@admin/user/components/routables/admin-user-list/admin-user-list.routable";
import {adminUserActionNameSpace} from "@admin/user/stores/admin-user.action";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
  ResourceExistGuardService,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminUserListRoutable,
    data: {
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.userDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminUserDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminUserState.currentTitle,
      nameSpace: adminUserActionNameSpace,
      resourceState: AdminUserState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: AdminRoutesEnum.userEdit,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
        component: EmptyContainer,
      },
    ],
  },
  {
    path: AdminRoutesEnum.userCreate,
    component: AdminUserCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminUserRoutingModule {
}
