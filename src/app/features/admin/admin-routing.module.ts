/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {AdminHomeRoutable} from "@app/features/admin/components/routables/admin-home/admin-home.routable";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CombinedGuardService,
  labelSolidifyOaiPmh,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminHomeRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.license,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./license/admin-license.module").then(m => m.AdminLicenseModule),
    data: {
      breadcrumb: LabelTranslateEnum.licenses,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.globalBanner,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./global-banner/admin-global-banner.module").then(m => m.AdminGlobalBannerModule),
    data: {
      breadcrumb: LabelTranslateEnum.globalBanners,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.applicationEvent,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./application-event/admin-application-event.module").then(m => m.AdminApplicationEventModule),
    data: {
      breadcrumb: LabelTranslateEnum.events,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.structure,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./structure/admin-structure.module").then(m => m.AdminStructureModule),
    data: {
      breadcrumb: LabelTranslateEnum.structures,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.researchGroup,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./research-group/admin-research-group.module").then(m => m.AdminResearchGroupModule),
    data: {
      breadcrumb: LabelTranslateEnum.researchGroups,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.user,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./user/admin-user.module").then(m => m.AdminUserModule),
    data: {
      breadcrumb: LabelTranslateEnum.user,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.role,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./role/admin-role.module").then(m => m.AdminRoleModule),
    data: {
      breadcrumb: LabelTranslateEnum.role,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.indexFieldAlias,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./index-field-alias/admin-index-field-alias.module").then(m => m.AdminIndexFieldAliasModule),
    data: {
      breadcrumb: LabelTranslateEnum.indexFieldAliases,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.oaiMetadataPrefix,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminOaiMetadataPrefixModule),
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhOaiMetadataPrefixBreadcrumb,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.oaiSet,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminOaiSetModule),
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhOaiSetBreadcrumb,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
