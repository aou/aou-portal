/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-global-banner-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminGlobalBannerCreateRoutable} from "@admin/global-banner/components/routables/admin-global-banner-create/admin-global-banner-create.routable";
import {AdminGlobalBannerDetailEditRoutable} from "@admin/global-banner/components/routables/admin-global-banner-detail-edit/admin-global-banner-detail-edit.routable";
import {AdminGlobalBannerListRoutable} from "@admin/global-banner/components/routables/admin-global-banner-list/admin-global-banner-list.routable";
import {adminGlobalBannerActionNameSpace} from "@admin/global-banner/stores/admin-global-banner.action";
import {AdminGlobalBannerState} from "@admin/global-banner/stores/admin-global-banner.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
  ResourceExistGuardService,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminGlobalBannerListRoutable,
    data: {},
  },
  {
    path: AppRoutesEnum.globalBannerCreate,
    component: AdminGlobalBannerCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AppRoutesEnum.globalBannerDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminGlobalBannerDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminGlobalBannerState.currentTitle,
      nameSpace: adminGlobalBannerActionNameSpace,
      resourceState: AdminGlobalBannerState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: AppRoutesEnum.globalBannerEdit,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
        component: EmptyContainer,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminGlobalBannerRoutingModule {
}
