/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-global-banner.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminGlobalBannerRoutingModule} from "@admin/global-banner/admin-global-banner-routing.module";
import {AdminGlobalBannerFormPresentational} from "@admin/global-banner/components/presentationals/admin-global-banner-form/admin-global-banner-form.presentational";
import {AdminGlobalBannerCreateRoutable} from "@admin/global-banner/components/routables/admin-global-banner-create/admin-global-banner-create.routable";
import {AdminGlobalBannerDetailEditRoutable} from "@admin/global-banner/components/routables/admin-global-banner-detail-edit/admin-global-banner-detail-edit.routable";
import {AdminGlobalBannerListRoutable} from "@admin/global-banner/components/routables/admin-global-banner-list/admin-global-banner-list.routable";
import {AdminGlobalBannerState} from "@admin/global-banner/stores/admin-global-banner.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminGlobalBannerCreateRoutable,
  AdminGlobalBannerDetailEditRoutable,
  AdminGlobalBannerListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminGlobalBannerFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminGlobalBannerRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminGlobalBannerState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminGlobalBannerModule {
}
