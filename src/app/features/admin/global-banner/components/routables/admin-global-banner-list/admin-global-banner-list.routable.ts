/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-global-banner-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminGlobalBannerActionNameSpace} from "@admin/global-banner/stores/admin-global-banner.action";
import {AdminGlobalBannerStateModel} from "@admin/global-banner/stores/admin-global-banner.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  GlobalBanner,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-global-banner-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-global-banner-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminGlobalBannerListRoutable extends AbstractListRoutable<GlobalBanner, AdminGlobalBannerStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof GlobalBanner & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_globalBanner, adminGlobalBannerActionNameSpace, _injector, {}, StateEnum.admin);
  }

  conditionDisplayEditButton(model: GlobalBanner | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: GlobalBanner | undefined): boolean {
    return true;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "type",
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        filterableField: "type",
        sortableField: "type",
        translate: true,
        filterEnum: Enums.GlobalBanner.TypeEnumTranslate,
      },
      {
        field: "enabled",
        header: LabelTranslateEnum.enabled,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "startDate",
        header: LabelTranslateEnum.startDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "endDate",
        header: LabelTranslateEnum.endDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
