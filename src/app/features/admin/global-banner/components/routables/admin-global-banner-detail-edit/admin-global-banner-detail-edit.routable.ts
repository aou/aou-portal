/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-global-banner-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminGlobalBannerActionNameSpace} from "@admin/global-banner/stores/admin-global-banner.action";
import {
  AdminGlobalBannerState,
  AdminGlobalBannerStateModel,
} from "@admin/global-banner/stores/admin-global-banner.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {sharedGlobalBannerActionNameSpace} from "@app/shared/stores/global-banner/shared-global-banner.action";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  GlobalBanner,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-global-banner-detail-edit-routable",
  templateUrl: "./admin-global-banner-detail-edit.routable.html",
  styleUrls: ["./admin-global-banner-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminGlobalBannerDetailEditRoutable extends AbstractDetailEditCommonRoutable<GlobalBanner, AdminGlobalBannerStateModel> {
  @Select(AdminGlobalBannerState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminGlobalBannerState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override getByIdIfAlreadyInState: boolean = false;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedGlobalBannerActionNameSpace;

  readonly KEY_PARAM_NAME: keyof GlobalBanner & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_globalBanner, _injector, adminGlobalBannerActionNameSpace, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
