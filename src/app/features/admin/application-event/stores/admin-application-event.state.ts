/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-application-event.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminApplicationEventActionNameSpace} from "@admin/application-event/stores/admin-application-event.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {ApplicationEvent} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  StoreUtil,
} from "solidify-frontend";

export interface AdminApplicationEventStateModel extends ResourceStateModel<ApplicationEvent> {
}

@Injectable()
@State<AdminApplicationEventStateModel>({
  name: StateEnum.admin_applicationEvent,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminApplicationEventState extends ResourceState<AdminApplicationEventStateModel, ApplicationEvent> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminApplicationEventActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminApplicationEvents;
  }

  @Selector()
  static isLoading(state: AdminApplicationEventStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminApplicationEventStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminApplicationEventStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current?.creation?.when;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminApplicationEventStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminApplicationEventStateModel): boolean {
    return true;
  }
}
