/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-application-event-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminApplicationEventActionNameSpace} from "@admin/application-event/stores/admin-application-event.action";
import {AdminApplicationEventStateModel} from "@admin/application-event/stores/admin-application-event.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {sharedPersonActionNameSpace} from "@app/shared/stores/person/shared-person.action";
import {Enums} from "@enums";
import {
  ApplicationEvent,
  Deposit,
  Person,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedDepositActionNameSpace} from "@shared/stores/deposit/shared-deposit.action";
import {SharedDepositState} from "@shared/stores/deposit/shared-deposit.state";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-application-event-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-application-event-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminApplicationEventListRoutable extends AbstractListRoutable<ApplicationEvent, AdminApplicationEventStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof ApplicationEvent & string = "resId";

  sharedDepositSort: Sort<Deposit> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedDepositActionNameSpace: ResourceNameSpace = sharedDepositActionNameSpace;
  sharedDepositState: typeof SharedDepositState = SharedDepositState;

  sharedPersonSort: Sort<Person> = {
    field: "fullName",
    order: OrderEnum.ascending,
  };
  sharedPersonActionNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;
  sharedPersonState: typeof SharedPersonState = SharedPersonState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_applicationEvent, adminApplicationEventActionNameSpace, _injector, {
      canCreate: false,
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: ApplicationEvent | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: ApplicationEvent | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "eventType.resId" as any,
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        filterableField: "eventType.resId" as any,
        sortableField: "eventType.name" as any,
        order: OrderEnum.none,
        filterEnum: Enums.EventType.EventTypeEnumTranslate,
        isFilterable: true,
        isSortable: true,
        translate: true,
      },
      {
        field: "publication.title" as any,
        header: LabelTranslateEnum.deposit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        filterableField: "publication.resId" as any,
        resourceNameSpace: this.sharedDepositActionNameSpace,
        resourceState: this.sharedDepositState as any,
        searchableSingleSelectSort: this.sharedDepositSort,
        resourceLabelKey: "title",
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "triggerBy.fullName" as any,
        header: LabelTranslateEnum.emitter,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        filterableField: "triggerBy.resId" as any,
        sortableField: "triggerBy.lastName" as any,
        resourceNameSpace: this.sharedPersonActionNameSpace,
        resourceState: this.sharedPersonState as any,
        searchableSingleSelectSort: this.sharedPersonSort,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
