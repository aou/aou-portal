/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-application-event-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminApplicationEventDetailEditRoutable} from "@admin/application-event/components/routables/admin-application-event-detail-edit/admin-application-event-detail-edit.routable";
import {AdminApplicationEventListRoutable} from "@admin/application-event/components/routables/admin-application-event-list/admin-application-event-list.routable";
import {adminApplicationEventActionNameSpace} from "@admin/application-event/stores/admin-application-event.action";
import {AdminApplicationEventState} from "@admin/application-event/stores/admin-application-event.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CombinedGuardService,
  ResourceExistGuardService,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminApplicationEventListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.applicationEventDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminApplicationEventDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminApplicationEventState.currentTitle,
      nameSpace: adminApplicationEventActionNameSpace,
      resourceState: AdminApplicationEventState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminApplicationEventRoutingModule {
}
