/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-role-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminRoleCreateRoutable} from "@admin/role/components/routables/admin-role-create/admin-role-create.routable";
import {AdminRoleDetailEditRoutable} from "@admin/role/components/routables/admin-role-detail-edit/admin-role-detail-edit.routable";
import {AdminRoleListRoutable} from "@admin/role/components/routables/admin-role-list/admin-role-list.routable";
import {adminRoleActionNameSpace} from "@admin/role/stores/admin-role.action";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
  ResourceExistGuardService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const separator: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminRoleListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.roleCreate,
    component: AdminRoleCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AdminRoutesEnum.roleDetail + separator + AppRoutesEnum.paramId,
    component: AdminRoleDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminRoleState.currentTitle,
      nameSpace: adminRoleActionNameSpace,
      resourceState: AdminRoleState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: AdminRoutesEnum.roleEdit,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
        component: EmptyContainer,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoleRoutingModule {
}
