/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-role-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminRoleActionNameSpace} from "@admin/role/stores/admin-role.action";
import {
  AdminRoleState,
  AdminRoleStateModel,
} from "@admin/role/stores/admin-role.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {Role} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedRoleActionNameSpace} from "@shared/stores/role/shared-role.action";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AbstractFormPresentational,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-role-create-routable",
  templateUrl: "./admin-role-create.routable.html",
  styleUrls: ["./admin-role-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminRoleCreateRoutable extends AbstractCreateRoutable<Role, AdminRoleStateModel> {
  @Select(AdminRoleState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminRoleState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedRoleActionNameSpace;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<Role>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_license, _injector, adminRoleActionNameSpace, StateEnum.admin);
  }
}
