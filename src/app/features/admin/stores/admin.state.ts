/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminApplicationEventState} from "@admin/application-event/stores/admin-application-event.state";
import {AdminGlobalBannerState} from "@admin/global-banner/stores/admin-global-banner.state";
import {AdminIndexFieldAliasState} from "@admin/index-field-alias/stores/admin-index-field-alias.state";
import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {AdminResearchGroupState} from "@admin/research-group/stores/admin-research-group.state";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {AdminStructureState} from "@admin/structure/stores/admin-structure.state";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {Injectable} from "@angular/core";
import {
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AdminOaiMetadataPrefixState,
  AdminOaiSetState,
  BaseStateModel,
} from "solidify-frontend";

export interface AdminStateModel extends BaseStateModel {
}

@Injectable()
@State<AdminStateModel>({
  name: StateEnum.admin,
  defaults: {
    isLoadingCounter: 0,
  },
  children: [
    AdminLicenseState,
    AdminIndexFieldAliasState,
    AdminStructureState,
    AdminUserState,
    AdminRoleState,
    AdminResearchGroupState,
    AdminGlobalBannerState,
    AdminApplicationEventState,
    AdminOaiSetState,
    AdminOaiMetadataPrefixState,
  ],
})
export class AdminState {
  constructor(protected readonly _store: Store) {
  }
}
