/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminApplicationEventState} from "@admin/application-event/stores/admin-application-event.state";
import {AdminGlobalBannerState} from "@admin/global-banner/stores/admin-global-banner.state";
import {AdminIndexFieldAliasState} from "@admin/index-field-alias/stores/admin-index-field-alias.state";
import {AdminResearchGroupState} from "@admin/research-group/stores/admin-research-group.state";
import {AdminResearchGroupDepositState} from "@admin/research-group/stores/research-group-deposit/admin-research-group-deposit.state";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {AdminStructureState} from "@admin/structure/stores/admin-structure.state";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {AdminPersonState} from "@admin/user/stores/person/admin-person.state";
import {AdminPersonResearchGroupState} from "@admin/user/stores/person/person-research-group/admin-person-research-group.state";
import {AdminPersonRoleState} from "@admin/user/stores/person/person-role/admin-person-role.state";
import {AdminPersonStructureState} from "@admin/user/stores/person/person-structure/admin-person-structure.state";
import {AdminPersonValidationRightStructureState} from "@admin/user/stores/person/person-validation-right-structure/admin-person-validation-right-structure.state";
import {NgModule} from "@angular/core";
import {AdminRoutingModule} from "@app/features/admin/admin-routing.module";
import {AdminHomeRoutable} from "@app/features/admin/components/routables/admin-home/admin-home.routable";
import {AdminLicenseState} from "@app/features/admin/license/stores/admin-license.state";
import {AdminState} from "@app/features/admin/stores/admin.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {
  AdminOaiMetadataPrefixState,
  AdminOaiSetState,
  SsrUtil,
} from "solidify-frontend";

const routables = [
  AdminHomeRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminState,
      AdminLicenseState,
      AdminIndexFieldAliasState,
      AdminGlobalBannerState,
      AdminApplicationEventState,
      AdminStructureState,
      AdminResearchGroupState,
      AdminResearchGroupDepositState,
      AdminUserState,
      AdminRoleState,
      AdminPersonState,
      AdminPersonStructureState,
      AdminPersonResearchGroupState,
      AdminPersonRoleState,
      AdminPersonValidationRightStructureState,
      AdminOaiSetState,
      AdminOaiMetadataPrefixState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.adminModuleLoaded] = true;
    }
  }
}
