/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-index-field-alias.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminIndexFieldAliasRoutingModule} from "@admin/index-field-alias/admin-index-field-alias-routing.module";
import {AdminIndexFieldAliasFormPresentational} from "@admin/index-field-alias/components/presentationals/admin-index-field-alias-form/admin-index-field-alias-form.presentational";
import {AdminIndexFieldAliasCreateRoutable} from "@admin/index-field-alias/components/routables/admin-index-field-alias-create/admin-index-field-alias-create.routable";
import {AdminIndexFieldAliasDetailEditRoutable} from "@admin/index-field-alias/components/routables/admin-index-field-alias-detail-edit/admin-index-field-alias-detail-edit.routable";
import {AdminIndexFieldAliasListRoutable} from "@admin/index-field-alias/components/routables/admin-index-field-alias-list/admin-index-field-alias-list.routable";
import {AdminIndexFieldAliasState} from "@admin/index-field-alias/stores/admin-index-field-alias.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminIndexFieldAliasListRoutable,
  AdminIndexFieldAliasDetailEditRoutable,
  AdminIndexFieldAliasCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminIndexFieldAliasFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminIndexFieldAliasRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminIndexFieldAliasState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminIndexFieldAliasModule {
}
