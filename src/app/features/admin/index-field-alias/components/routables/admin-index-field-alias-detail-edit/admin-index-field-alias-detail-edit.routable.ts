/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-index-field-alias-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminIndexFieldAliasActionNameSpace} from "@admin/index-field-alias/stores/admin-index-field-alias.action";
import {
  AdminIndexFieldAliasState,
  AdminIndexFieldAliasStateModel,
} from "@admin/index-field-alias/stores/admin-index-field-alias.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IndexFieldAlias} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedIndexFieldAliasActionNameSpace} from "@shared/stores/index-field-alias/shared-index-field-alias.action";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-index-field-alias-detail-edit-routable",
  templateUrl: "./admin-index-field-alias-detail-edit.routable.html",
  styleUrls: ["./admin-index-field-alias-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIndexFieldAliasDetailEditRoutable extends AbstractDetailEditCommonRoutable<IndexFieldAlias, AdminIndexFieldAliasStateModel> {
  @Select(AdminIndexFieldAliasState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminIndexFieldAliasState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedIndexFieldAliasActionNameSpace;

  readonly KEY_PARAM_NAME: keyof IndexFieldAlias & string = "alias";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_indexFieldAlias, _injector, adminIndexFieldAliasActionNameSpace, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
