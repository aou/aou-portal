/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-index-field-alias-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  IndexFieldAlias,
  Label,
  Language,
  LargeLabel,
} from "@models";
import {Store} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedLanguageAction} from "@shared/stores/language/shared-language.action";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  isNotNullNorUndefined,
  isNullOrUndefined,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-index-field-alias-form",
  templateUrl: "./admin-index-field-alias-form.presentational.html",
  styleUrls: ["./admin-index-field-alias-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIndexFieldAliasFormPresentational extends AbstractFormPresentational<IndexFieldAlias> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  languages: Language[] = [];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              protected readonly _store: Store) {
    super(_changeDetectorRef, _elementRef, _injector);
    this._initLanguages();
  }

  private _initLanguages(): void {
    this.languages = [];
    environment.appLanguages.forEach(language => {
      this.languages.push({code: language, resId: language});
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.indexName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.alias]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.field]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.facet]: [false, [SolidifyValidator]],
      [this.formDefinition.system]: [false, [SolidifyValidator]],
      [this.formDefinition.facetMinCount]: [1, []],
      [this.formDefinition.facetLimit]: ["", []],
      [this.formDefinition.facetOrder]: ["", []],
      [this.formDefinition.facetDefaultVisibleValues]: ["", []],
      [this.formDefinition.withDescription]: [false, [SolidifyValidator]],
    });
  }

  protected _bindFormTo(indexFieldAlias: IndexFieldAlias): void {
    this.form = this._fb.group({
      [this.formDefinition.indexName]: [indexFieldAlias.indexName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.alias]: [indexFieldAlias.alias, [Validators.required, SolidifyValidator]],
      [this.formDefinition.field]: [indexFieldAlias.field, [Validators.required, SolidifyValidator]],
      [this.formDefinition.facet]: [indexFieldAlias.facet, [SolidifyValidator]],
      [this.formDefinition.system]: [indexFieldAlias.system, [SolidifyValidator]],
      [this.formDefinition.facetMinCount]: [indexFieldAlias.facetMinCount, []],
      [this.formDefinition.facetLimit]: [indexFieldAlias.facetLimit, []],
      [this.formDefinition.facetOrder]: [indexFieldAlias.facetOrder, []],
      [this.formDefinition.facetDefaultVisibleValues]: [indexFieldAlias.facetDefaultVisibleValues, []],
      [this.formDefinition.withDescription]: [indexFieldAlias.withDescription, [SolidifyValidator]],
    });
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedLanguageAction.GetAll());
    this._initLanguageForm();
    this._addValidators();
  }

  getLabelTranslate(languageCode: Enums.Language.LanguageEnum): string {
    return Enums.Language.LanguageEnumTranslate.find(l => l.key === languageCode)?.value;
  }

  private _addValidators(): void {
    const facetFormControl = this.form.get(this.formDefinition.facet);
    this.subscribe(facetFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap((isFacet: boolean) => {
        this._setValidator(isFacet);
      }),
    ));
    this._setValidator(facetFormControl.value);
  }

  private _setValidator(isFacet: boolean): void {
    const facetMinCountFormControl = this.form.get(this.formDefinition.facetMinCount);
    const facetLimitFormControl = this.form.get(this.formDefinition.facetLimit);
    const facetOrderControl = this.form.get(this.formDefinition.facetOrder);

    if (isFacet) {
      facetMinCountFormControl.setValidators([Validators.required, Validators.min(1), SolidifyValidator]);
      facetLimitFormControl.setValidators([Validators.required, Validators.min(1), SolidifyValidator]);
      facetOrderControl.setValidators([Validators.required, Validators.min(0), SolidifyValidator]);
    } else {
      facetMinCountFormControl.setValidators([]);
      facetMinCountFormControl.setValue(undefined);
      facetLimitFormControl.setValidators([]);
      facetLimitFormControl.setValue(undefined);
      facetOrderControl.setValidators([]);
      facetOrderControl.setValue(undefined);
    }
    this.form.updateValueAndValidity();
    this._changeDetectorRef.detectChanges();
  }

  protected _treatmentBeforeSubmit(indexFieldAlias: IndexFieldAlias): IndexFieldAlias {
    const newLabels: Label[] = [];
    const descriptionLabels: LargeLabel[] = [];
    this.languages.forEach(language => {
      if (isNotNullNorUndefined(indexFieldAlias["label_" + language.resId])) {
        newLabels.push({
          text: indexFieldAlias["label_" + language.resId],
          language: language,
        });

      }
      if (indexFieldAlias.withDescription && isNotNullNorUndefined(indexFieldAlias["description_" + language.resId])) {
        descriptionLabels.push({
          text: indexFieldAlias["description_" + language.resId],
          language: language,
        });
      }
    });
    indexFieldAlias.labels = newLabels;
    indexFieldAlias.descriptionLabels = descriptionLabels;

    return indexFieldAlias;
  }

  private _initLanguageForm(): void {
    if (isNullOrUndefined(this.languages)) {
      return;
    }
    this.languages.forEach(language => {
      this._initLabelFields(language);
      this._initDescriptionFields(language);
    });
    this._changeDetectorRef.detectChanges();
  }

  private _initLabelFields(language: Language): void {
    if (isNullOrUndefined(this.model) || isNullOrUndefined(this.model.labels)) {
      this._addControl("label", language.resId);
      return;
    }
    const existingValue = this.model.labels.find(label => label.language.code === language.code);
    if (isNullOrUndefined(existingValue)) {
      this._addControl("label", language.resId);
      return;
    }
    this._addControl("label", language.resId, existingValue.text);
  }

  private _initDescriptionFields(language: Language): void {
    if (isNullOrUndefined(this.model) || isNullOrUndefined(this.model.descriptionLabels)) {
      this._addControl("description", language.resId);
      return;
    }
    const existingValue = this.model.descriptionLabels.find(label => label.language.code === language.code);
    if (isNullOrUndefined(existingValue)) {
      this._addControl("description", language.resId);
      return;
    }
    this._addControl("description", language.resId, existingValue.text);
  }

  private _addControl(fieldType: string, languageResId: string, value: string | undefined = undefined): void {
    if (!isNullOrUndefined(this.form)) {
      this.form.addControl(fieldType + "_" + languageResId, this._fb.control(value));
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() indexName: string;
  @PropertyName() alias: string;
  @PropertyName() field: string;
  @PropertyName() facet: string;
  @PropertyName() system: string;
  @PropertyName() facetMinCount: string;
  @PropertyName() facetLimit: string;
  @PropertyName() facetOrder: string;
  @PropertyName() facetDefaultVisibleValues: string;
  @PropertyName() withDescription: string;
}
