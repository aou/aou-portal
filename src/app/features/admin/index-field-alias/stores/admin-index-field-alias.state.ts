/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-index-field-alias.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminIndexFieldAliasAction,
  adminIndexFieldAliasActionNameSpace,
} from "@admin/index-field-alias/stores/admin-index-field-alias.action";
import {Injectable} from "@angular/core";
import {appSystemPropertyActionNameSpace} from "@app/stores/system-property/app-system-property.action";
import {AppSystemPropertyStateModel} from "@app/stores/system-property/app-system-property.state";
import {environment} from "@environments/environment";
import {IndexFieldAlias} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminIndexFieldAliasStateModel extends ResourceStateModel<IndexFieldAlias> {
}

@Injectable()
@State<AdminIndexFieldAliasStateModel>({
  name: StateEnum.admin_indexFieldAlias,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
  children: [],
})
export class AdminIndexFieldAliasState extends ResourceState<AdminIndexFieldAliasStateModel, IndexFieldAlias> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminIndexFieldAliasActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminIndexFieldAliasDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminIndexFieldAliasDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminIndexFieldAlias,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.indexFieldAlias.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.indexFieldAlias.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.indexFieldAlias.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.indexResourceIndexFieldAliases;
  }

  @Selector()
  static isLoading(state: AdminIndexFieldAliasStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminIndexFieldAliasStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminIndexFieldAliasStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.alias;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminIndexFieldAliasStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminIndexFieldAliasStateModel): boolean {
    return true;
  }

  @Action([AdminIndexFieldAliasAction.UpdateSuccess, AdminIndexFieldAliasAction.CreateSuccess, AdminIndexFieldAliasAction.DeleteSuccess])
  refreshSystemProperties(ctx: SolidifyStateContext<AppSystemPropertyStateModel>): Observable<any> {
    return ctx.dispatch(new appSystemPropertyActionNameSpace.GetSystemProperties());
  }
}
