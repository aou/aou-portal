/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-index-field-alias-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminIndexFieldAliasCreateRoutable} from "@admin/index-field-alias/components/routables/admin-index-field-alias-create/admin-index-field-alias-create.routable";
import {AdminIndexFieldAliasDetailEditRoutable} from "@admin/index-field-alias/components/routables/admin-index-field-alias-detail-edit/admin-index-field-alias-detail-edit.routable";
import {AdminIndexFieldAliasListRoutable} from "@admin/index-field-alias/components/routables/admin-index-field-alias-list/admin-index-field-alias-list.routable";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";
import {AdminIndexFieldAliasState} from "./stores/admin-index-field-alias.state";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminIndexFieldAliasListRoutable,
    data: {},
  },
  {
    path: AppRoutesEnum.indexFieldAliasDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminIndexFieldAliasDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminIndexFieldAliasState.currentTitle,
    },
    children: [
      {
        path: AppRoutesEnum.indexFieldAliasEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    component: AdminIndexFieldAliasCreateRoutable,
    path: AppRoutesEnum.indexFieldAliasCreate,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminIndexFieldAliasRoutingModule {
}
