/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-structure.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminStructureRoutingModule} from "@admin/structure/admin-structure-routing.module";
import {AdminStructureFormPresentational} from "@admin/structure/components/presentationals/admin-structure-form/admin-structure-form.presentational";
import {AdminStructureCreateRoutable} from "@admin/structure/components/routables/admin-structure-create/admin-structure-create.routable";
import {AdminStructureDetailEditRoutable} from "@admin/structure/components/routables/admin-structure-detail-edit/admin-structure-detail-edit.routable";
import {AdminStructureListRoutable} from "@admin/structure/components/routables/admin-structure-list/admin-structure-list.routable";
import {AdminStructureState} from "@admin/structure/stores/admin-structure.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminStructureCreateRoutable,
  AdminStructureDetailEditRoutable,
  AdminStructureListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminStructureFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminStructureRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminStructureState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminStructureModule {
}
