/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-structure-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminStructureActionNameSpace} from "@admin/structure/stores/admin-structure.action";
import {
  AdminStructureState,
  AdminStructureStateModel,
} from "@admin/structure/stores/admin-structure.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Structure} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {AbstractDetailEditCommonRoutable} from "solidify-frontend";

@Component({
  selector: "aou-admin-structure-detail-edit-routable",
  templateUrl: "./admin-structure-detail-edit.routable.html",
  styleUrls: ["./admin-structure-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminStructureDetailEditRoutable extends AbstractDetailEditCommonRoutable<Structure, AdminStructureStateModel> {
  @Select(AdminStructureState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminStructureState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override getByIdIfAlreadyInState: boolean = false;

  // override checkAvailableResourceNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;

  readonly KEY_PARAM_NAME: keyof Structure & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_structure, _injector, adminStructureActionNameSpace, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
