/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-structure-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminStructureAction,
  adminStructureActionNameSpace,
} from "@admin/structure/stores/admin-structure.action";
import {AdminStructureStateModel} from "@admin/structure/stores/admin-structure.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Structure} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-structure-list-routable",
  templateUrl: "./admin-structure-list.routable.html",
  styleUrls: ["./admin-structure-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminStructureListRoutable extends AbstractListRoutable<Structure, AdminStructureStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof Structure & string = "name";

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_structure, adminStructureActionNameSpace, _injector, {
      listExtraButtons: [
        {
          color: ButtonColorEnum.primary,
          icon: IconNameEnum.init,
          labelToTranslate: (current) => LabelTranslateEnum.init,
          callback: () => this.init(),
          displayCondition: () => this._securityService.isRoot(),
          order: 40,
        },
      ],
    }, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new AdminStructureAction.GetAll());
  }

  conditionDisplayEditButton(model: Structure | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: Structure | undefined): boolean {
    return true;
  }

  init(): void {
    this._store.dispatch(new AdminStructureAction.Init());
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name" as any,
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "openLicenseId" as any,
        header: LabelTranslateEnum.openLicenseId,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  openDetail(structure: Structure): void {
    this.showDetail(structure);
  }
}
