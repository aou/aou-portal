/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-structure-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {Structure} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {sharedStructureActionNameSpace} from "@shared/stores/structure/shared-structure.action";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  BaseResource,
  BreakpointService,
  DateUtil,
  isNonEmptyString,
  KeyValue,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-admin-structure-form",
  templateUrl: "./admin-structure-form.presentational.html",
  styleUrls: ["./admin-structure-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminStructureFormPresentational extends AbstractFormPresentational<Structure> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  sharedStructureSort: Sort<Structure> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedStructureNameSpace: ResourceNameSpace = sharedStructureActionNameSpace;
  sharedStructureState: typeof SharedStructureState = SharedStructureState;
  structureLabel: (value: Structure) => string = value => SharedStructureState.labelCallback(value, this._translateService);

  structureStatusEnum: KeyValue[] = Enums.Structure.StatusEnumTranslate;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService,
              private readonly _translateService: TranslateService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.status]: [Enums.Structure.StatusEnum.ACTIVE, [SolidifyValidator]],
      [this.formDefinition.cnStructC]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.cStruct]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.dewey]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.validUntil]: ["", [SolidifyValidator]],
      [this.formDefinition.parentStructure]: ["", [SolidifyValidator]],
      [this.formDefinition.sortValue]: ["", [SolidifyValidator]],
      [this.formDefinition.acronym]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(structure: Structure): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [structure.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.status]: [structure.status, [Validators.required, SolidifyValidator]],
      [this.formDefinition.cnStructC]: [structure.cnStructC, [Validators.required, SolidifyValidator]],
      [this.formDefinition.cStruct]: [structure.cStruct, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dewey]: [structure.dewey, [Validators.required, SolidifyValidator]],
      [this.formDefinition.validUntil]: [structure.validUntil, [SolidifyValidator]],
      [this.formDefinition.parentStructure]: [structure.parentStructure?.resId, [SolidifyValidator]],
      [this.formDefinition.sortValue]: [structure.sortValue, [SolidifyValidator]],
      [this.formDefinition.acronym]: [structure.acronym, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(structure: Structure): Structure {
    structure.validUntil = DateUtil.convertToLocalDateDateSimple(structure.validUntil);
    const parentStructureId = this.form.get(this.formDefinition.parentStructure).value;
    if (isNonEmptyString(parentStructureId)) {
      structure.parentStructure = {resId: parentStructureId} as BaseResource | any;
    } else {
      structure.parentStructure = undefined;
    }
    return structure;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() status: string;
  @PropertyName() cnStructC: string;
  @PropertyName() cStruct: string;
  @PropertyName() dewey: string;
  @PropertyName() validUntil: string;
  @PropertyName() sortValue: string;
  @PropertyName() parentStructure: string;
  @PropertyName() acronym: string;
}
