/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-structure-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminStructureCreateRoutable} from "@admin/structure/components/routables/admin-structure-create/admin-structure-create.routable";
import {AdminStructureDetailEditRoutable} from "@admin/structure/components/routables/admin-structure-detail-edit/admin-structure-detail-edit.routable";
import {AdminStructureListRoutable} from "@admin/structure/components/routables/admin-structure-list/admin-structure-list.routable";
import {adminStructureActionNameSpace} from "@admin/structure/stores/admin-structure.action";
import {AdminStructureState} from "@admin/structure/stores/admin-structure.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
  ResourceExistGuardService,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminStructureListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.structureCreate,
    component: AdminStructureCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AdminRoutesEnum.structureDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminStructureDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminStructureState.currentTitle,
      nameSpace: adminStructureActionNameSpace,
      resourceState: AdminStructureState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: AdminRoutesEnum.structureEdit,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
        component: EmptyContainer,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminStructureRoutingModule {
}
