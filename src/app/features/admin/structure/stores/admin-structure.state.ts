/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - admin-structure.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminStructureAction,
  adminStructureActionNameSpace,
} from "@admin/structure/stores/admin-structure.action";
import {Injectable} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Structure} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  Result,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface AdminStructureStateModel extends ResourceStateModel<Structure> {
}

@Injectable()
@State<AdminStructureStateModel>({
  name: StateEnum.admin_structure,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {
      field: "sortValue",
      order: OrderEnum.ascending,
    }),
  },
})
export class AdminStructureState extends ResourceState<AdminStructureStateModel, Structure> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminStructureActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminStructureDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminStructureDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminStructure,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.structure.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.structure.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.structure.notification.resource.update"),
      notificationResourceDeleteFailTextToTranslate: MARK_AS_TRANSLATABLE("admin.structure.notification.resource.deleteFail"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminStructures;
  }

  @Selector()
  static isLoading(state: AdminStructureStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminStructureStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminStructureStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminStructureStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminStructureStateModel): boolean {
    return true;
  }

  @Action(AdminStructureAction.Init)
  init(ctx: SolidifyStateContext<AdminStructureStateModel>, action: AdminStructureAction.Init): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.INITIALIZE).pipe(
      tap(result => {
        if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
          ctx.dispatch(new AdminStructureAction.InitSuccess(action));
        } else {
          ctx.dispatch(new AdminStructureAction.InitFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AdminStructureAction.InitFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(AdminStructureAction.InitSuccess)
  initSuccess(ctx: SolidifyStateContext<AdminStructureStateModel>, action: AdminStructureAction.InitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("admin.structure.notification.init.success"));
    ctx.dispatch(new AdminStructureAction.GetAll());
  }

  @Action(AdminStructureAction.InitFail)
  initFail(ctx: SolidifyStateContext<AdminStructureStateModel>, action: AdminStructureAction.InitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.structure.notification.init.fail"));
  }

  @OverrideDefaultAction()
  @Action(AdminStructureAction.Clean)
  clean(ctx: SolidifyStateContext<AdminStructureStateModel>, action: AdminStructureAction.Clean): void {
    super.clean(ctx, new AdminStructureAction.Clean(action.preserveList, ctx.getState().queryParameters));
  }
}
