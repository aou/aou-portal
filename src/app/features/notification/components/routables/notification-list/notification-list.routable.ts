/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - notification-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  NotificationAction,
  notificationActionNameSpace,
} from "@app/features/notification/stores/notification.action";
import {
  NotificationState,
  NotificationStateModel,
} from "@app/features/notification/stores/notification.state";
import {sharedDepositActionNameSpace} from "@app/shared/stores/deposit/shared-deposit.action";
import {sharedPersonActionNameSpace} from "@app/shared/stores/person/shared-person.action";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {
  Deposit,
  Notification,
  Person,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {SharedDepositState} from "@shared/stores/deposit/shared-deposit.state";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  DataTableActions,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObjectUtil,
  MemoizedUtil,
  ObjectUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-notification-list-routable",
  templateUrl: "../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./notification-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationListRoutable extends AbstractListRoutable<Notification, NotificationStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof Notification & string = "resId";
  private readonly _KEY_NOTIFICATION_TYPE: string = "notificationType.resId";

  sharedDepositSort: Sort<Deposit> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedDepositActionNameSpace: ResourceNameSpace = sharedDepositActionNameSpace;
  sharedDepositState: typeof SharedDepositState = SharedDepositState;

  sharedPersonSort: Sort<Person> = {
    field: "fullName",
    order: OrderEnum.ascending,
  };
  sharedPersonActionNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;
  sharedPersonState: typeof SharedPersonState = SharedPersonState;

  columnsSkippedToClear: string[] = [this._KEY_NOTIFICATION_TYPE, AppState.KEY_ONLY_WITH_NULL_READ_TIME];

  protected _onlyUnreadNotification: boolean = false;

  isHighlightCondition: (notification: Notification) => boolean = notification => isNullOrUndefined(notification.readTime);

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.notification, notificationActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      listExtraButtons: [
        {
          color: ButtonColorEnum.primary,
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyUnreadNotification,
          callback: (model, buttonElementRef, checked) => this.showOnlyUnreadNotification(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: false,
        },
      ],
    });
  }

  conditionDisplayEditButton(model: Notification | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: Notification | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "notificationType.resId" as any,
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        filterableField: "notificationType.resId" as any,
        sortableField: "notificationType.resId" as any,
        order: OrderEnum.none,
        filterEnum: Enums.NotificationType.NotificationTypeShortEnumTranslate,
        isFilterable: true,
        isSortable: false,
        translate: true,
      },
      {
        field: "event.publication.title" as any,
        header: LabelTranslateEnum.deposit,
        type: DataTableFieldTypeEnum.innerHtml,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        breakWord: true,
      },
      {
        field: "event.triggerBy.fullName" as any,
        header: LabelTranslateEnum.emitter,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        filterableField: "event.triggerBy.resId" as any,
        sortableField: "event.triggerBy.lastName" as any,
        resourceNameSpace: this.sharedPersonActionNameSpace,
        resourceState: this.sharedPersonState as any,
        searchableSingleSelectSort: this.sharedPersonSort,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "readTime" as any,
        header: LabelTranslateEnum.read,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  defineActions(): DataTableActions<Notification>[] {
    return [
      {
        logo: IconNameEnum.markAsRead,
        callback: model => this._setRead(model),
        placeholder: current => isNotNullNorUndefined(current) && LabelTranslateEnum.markAsRead,
        displayOnCondition: model => isNullOrUndefined(model.readTime),
        isWrapped: false,
      },
    ];
  }

  private _setRead(notification: Notification): void {
    this._store.dispatch(new NotificationAction.SetRead(notification.resId, ViewModeEnum.list));
  }

  showOnlyUnreadNotification(onlyUnreadNotification: boolean): void {
    let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, NotificationState);
    queryParameter = QueryParametersUtil.clone(queryParameter);
    if (onlyUnreadNotification) {
      this.updateQueryParameterWithOnlyUnreadNotification(queryParameter, true);
      this._onlyUnreadNotification = true;
    } else {
      this.updateQueryParameterWithOnlyUnreadNotification(queryParameter, false);
      this._onlyUnreadNotification = false;
    }
    this._store.dispatch(new NotificationAction.ChangeQueryParameters(queryParameter, true));
    this._changeDetector.detectChanges();
  }

  private updateQueryParameterWithOnlyUnreadNotification(queryParameters: QueryParameters, onlyWithValidationRights: boolean): QueryParameters | undefined {
    queryParameters = ObjectUtil.clone(queryParameters);
    queryParameters.search = ObjectUtil.clone(queryParameters.search);

    if (onlyWithValidationRights) {
      MappingObjectUtil.set(queryParameters.search.searchItems, AppState.KEY_ONLY_WITH_NULL_READ_TIME, SOLIDIFY_CONSTANTS.STRING_TRUE);
    } else {
      MappingObjectUtil.delete(queryParameters.search.searchItems, AppState.KEY_ONLY_WITH_NULL_READ_TIME);
    }
    return queryParameters;
  }
}
