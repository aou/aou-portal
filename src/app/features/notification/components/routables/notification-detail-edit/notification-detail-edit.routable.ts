/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - notification-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  NotificationAction,
  notificationActionNameSpace,
} from "@app/features/notification/stores/notification.action";
import {
  NotificationState,
  NotificationStateModel,
} from "@app/features/notification/stores/notification.state";
import {Notification} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ButtonColorEnum,
  ButtonThemeEnum,
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-notification-detail-edit-routable",
  templateUrl: "./notification-detail-edit.routable.html",
  styleUrls: ["./notification-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationDetailEditRoutable extends AbstractDetailEditCommonRoutable<Notification, NotificationStateModel> implements OnInit {
  @Select(NotificationState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(NotificationState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override editAvailable: boolean = false;

  override deleteAvailable: boolean = false;

  override getByIdIfAlreadyInState: boolean = false;

  readonly KEY_PARAM_NAME: keyof Notification & string = "resId";

  listExtraButtons: ExtraButtonToolbar<Notification>[] = [
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.markAsRead,
      displayCondition: current => isNullOrUndefined(current.readTime),
      callback: (current) => this._setRead(current),
      labelToTranslate: (current) => LabelTranslateEnum.markAsRead,
      order: 40,
    },
    {
      color: ButtonColorEnum.primary,
      typeButton: ButtonThemeEnum.flatButton,
      icon: IconNameEnum.markAsUnread,
      displayCondition: current => isNotNullNorUndefined(current.readTime),
      callback: (current) => this._setUnread(current),
      labelToTranslate: (current) => LabelTranslateEnum.markAsUnread,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.notification, _injector, notificationActionNameSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();
    const current: Notification = MemoizedUtil.currentSnapshot(this._store, NotificationState);
    if (isNullOrUndefined(current.readTime)) {
      this._setRead(current);
    }
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _setRead(notification: Notification): void {
    this._store.dispatch(new NotificationAction.SetRead(notification.resId, ViewModeEnum.detail));
  }

  private _setUnread(notification: Notification): void {
    this._store.dispatch(new NotificationAction.SetUnread(notification.resId, ViewModeEnum.detail));
  }
}
