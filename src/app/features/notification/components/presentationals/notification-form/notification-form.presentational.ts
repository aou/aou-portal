/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - notification-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {Notification} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "aou-notification-form",
  templateUrl: "./notification-form.presentational.html",
  styleUrls: ["./notification-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationFormPresentational extends AbstractFormPresentational<Notification> {
  readonly LIST_NOTIFICATION_TYPE_DEPOSIT_VALIDATION: Enums.NotificationType.NotificationTypeEnum[] = [
    Enums.NotificationType.NotificationTypeEnum.PUBLICATION_TO_VALIDATE,
    Enums.NotificationType.NotificationTypeEnum.PUBLICATION_TO_VALIDATE_COMMENTED,
  ];

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  notificationTypeEnumTranslate: KeyValue[] = Enums.NotificationType.NotificationTypeShortEnumTranslate;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              private readonly _fb: FormBuilder,
              protected readonly _injector: Injector) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.notificationType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.deposit]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.emitter]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(notification: Notification): void {
    this.form = this._fb.group({
      [this.formDefinition.notificationType]: [notification.notificationType?.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.deposit]: [notification.event?.publication?.title, [Validators.required, SolidifyValidator]],
      [this.formDefinition.emitter]: [notification.event?.triggerBy?.fullName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.message]: [notification.message, [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(notification: Notification): Notification {
    return notification;
  }

  goToDeposit(): void {
    let baseUrl = RoutesEnum.depositDetail;
    if (this.LIST_NOTIFICATION_TYPE_DEPOSIT_VALIDATION.indexOf(this.model.notificationType.resId as Enums.NotificationType.NotificationTypeEnum) !== -1) {
      baseUrl = RoutesEnum.depositToValidateDetail;
    }
    this._navigateBS.next([baseUrl, this.model.event.publication.resId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() notificationType: string;
  @PropertyName() deposit: string;
  @PropertyName() emitter: string;
  @PropertyName() message: string;
}
