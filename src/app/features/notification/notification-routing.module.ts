/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - notification-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {NotificationDetailEditRoutable} from "@app/features/notification/components/routables/notification-detail-edit/notification-detail-edit.routable";
import {NotificationListRoutable} from "@app/features/notification/components/routables/notification-list/notification-list.routable";
import {notificationActionNameSpace} from "@app/features/notification/stores/notification.action";
import {NotificationState} from "@app/features/notification/stores/notification.state";
import {
  AppRoutesEnum,
  NotificationPageRoutesEnum,
} from "@shared/enums/routes.enum";
import {AouRoutes} from "@shared/models/aou-route.model";
import {
  CombinedGuardService,
  ResourceExistGuardService,
} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: AppRoutesEnum.root,
    component: NotificationListRoutable,
    data: {},
  },
  {
    path: NotificationPageRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: NotificationDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: NotificationState.currentTitle,
      nameSpace: notificationActionNameSpace,
      resourceState: NotificationState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationRoutingModule {
}
