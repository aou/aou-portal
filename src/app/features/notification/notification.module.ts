/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - notification.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {NotificationFormPresentational} from "@app/features/notification/components/presentationals/notification-form/notification-form.presentational";
import {NotificationDetailEditRoutable} from "@app/features/notification/components/routables/notification-detail-edit/notification-detail-edit.routable";
import {NotificationListRoutable} from "@app/features/notification/components/routables/notification-list/notification-list.routable";
import {NotificationRoutingModule} from "@app/features/notification/notification-routing.module";
import {NotificationState} from "@app/features/notification/stores/notification.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  NotificationDetailEditRoutable,
  NotificationListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  NotificationFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    NotificationRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      NotificationState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class NotificationModule {
}
