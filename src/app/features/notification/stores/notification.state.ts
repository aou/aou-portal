/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - notification.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  NotificationAction,
  notificationActionNameSpace,
} from "@app/features/notification/stores/notification.action";
import {appActionNameSpace} from "@app/stores/app.action";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {environment} from "@environments/environment";
import {Notification} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {SessionStorageEnum} from "@shared/enums/session-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {pipe} from "rxjs";
import {Observable} from "rxjs/index";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  OverrideDefaultAction,
  ResourceState,
  ResourceStateModel,
  Result,
  SessionStorageHelper,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface NotificationStateModel extends ResourceStateModel<Notification> {
}

@Injectable()
@State<NotificationStateModel>({
  name: StateEnum.notification,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class NotificationState extends ResourceState<NotificationStateModel, Notification> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: notificationActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    if (isNullOrUndefined(this._store)) {
      return undefined;
    }
    const currentPerson = MemoizedUtil.currentSnapshot(this._store, AppPersonState);
    return ApiEnum.adminPeople + urlSeparator + currentPerson?.resId + urlSeparator + ApiResourceNameEnum.NOTIFICATIONS;
  }

  @Selector()
  static isLoading(state: NotificationStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: NotificationStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: NotificationStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.resId;
  }

  @Selector()
  static isReadyToBeDisplayed(state: NotificationStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: NotificationStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(NotificationAction.GetAll)
  getAll(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.GetAll): Observable<CollectionTyped<Notification>> {
    return this._apiService.getCollection<Notification>(this._urlResource, ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<Notification>) => {
          ctx.dispatch(new NotificationAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new NotificationAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(NotificationAction.GetById)
  getById(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.GetById): Observable<Notification> {
    return this._apiService.getById<Notification>(this._urlResource, action.id)
      .pipe(
        tap((notification: Notification) => {
          ctx.dispatch(new NotificationAction.GetByIdSuccess(action, notification));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new NotificationAction.GetByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(NotificationAction.SetRead)
  setRead(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.SetRead): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_READ)
      .pipe(
        tap(result => ctx.dispatch(new NotificationAction.SetReadSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new NotificationAction.SetReadFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(NotificationAction.SetReadSuccess)
  setReadSuccess(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.SetReadSuccess): void {
    SessionStorageHelper.removeItemInList(SessionStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    this._refreshNotificationPage(ctx, action.parentAction.notificationId, action.parentAction.mode);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notifications.setRead.success"));
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
  }

  @Action(NotificationAction.SetReadFail)
  setReadFail(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.SetReadFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notifications.setRead.fail"));
  }

  @Action(NotificationAction.SetUnread)
  setUnread(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.SetUnread): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_UNREAD)
      .pipe(
        tap(result => ctx.dispatch(new NotificationAction.SetUnreadSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new NotificationAction.SetUnreadFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(NotificationAction.SetUnreadSuccess)
  setUnreadSuccess(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.SetUnreadSuccess): void {
    SessionStorageHelper.addItemInList(SessionStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    this._refreshNotificationPage(ctx, action.parentAction.notificationId, action.parentAction.mode);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notifications.setUnread.success"));
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
  }

  @Action(NotificationAction.SetUnreadFail)
  setUnreadFail(ctx: SolidifyStateContext<NotificationStateModel>, action: NotificationAction.SetUnreadFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notifications.setUnread.fail"));
  }

  private _refreshNotificationPage(ctx: SolidifyStateContext<NotificationStateModel>,
                                   notificationId: string,
                                   mode: ViewModeEnum): void {
    if (mode === ViewModeEnum.detail) {
      ctx.dispatch(new NotificationAction.GetById(notificationId));
    } else if (mode === ViewModeEnum.list) {
      ctx.dispatch(new NotificationAction.GetAll());
    }
  }
}
