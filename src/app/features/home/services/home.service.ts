/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {HomeContactContributorDialog} from "@home/components/dialogs/home-contact-contributor/home-contact-contributor.dialog";
import {HomeAction} from "@home/stores/home.action";
import {ContactableContributor} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractBaseService,
  DialogUtil,
  isEmptyArray,
  NotificationService,
  ObservableUtil,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class HomeService extends AbstractBaseService {
  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _dialog: MatDialog,
              private readonly _notificationService: NotificationService) {
    super();
  }

  contactContributorAboutPublication(publicationId: string): Observable<boolean> {
    const bs = new BehaviorSubject<boolean>(undefined);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new HomeAction.GetContactableContributor(publicationId),
      HomeAction.GetContactableContributorSuccess, result => {
        const listContactableContributor = result.list;
        if (isEmptyArray(listContactableContributor)) {
          this._notificationService.showInformation(LabelTranslateEnum.notificationThereIsNoContactableContributorForThisPublication);
          bs.next(false);
          return;
        }

        this.subscribe(this.contactContributors(listContactableContributor, publicationId),
          result2 => bs.next(result2),
          () => bs.next(false),
        );
      }, HomeAction.GetContactableContributorFail, result => {
        this._notificationService.showInformation(LabelTranslateEnum.notificationThereIsNoContactableContributorForThisPublication);
        bs.next(false);
      }));
    return ObservableUtil.asObservable(bs);
  }

  contactContributors(listContactableContributor: ContactableContributor[], publicationId?: string): Observable<boolean> {
    const bs = new BehaviorSubject<boolean>(undefined);
    this.subscribe(DialogUtil.open(this._dialog, HomeContactContributorDialog, {
        listContactableContributor: listContactableContributor,
        publicationId: publicationId,
      }, {
        width: "90%",
      }, result => bs.next(true),
      () => bs.next(false),
    ));
    return ObservableUtil.asObservable(bs);
  }
}
