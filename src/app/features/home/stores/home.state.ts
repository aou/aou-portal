/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
} from "@angular/common/http";
import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {AOU_CONSTANTS} from "@app/constants";
import {HomeAction} from "@app/features/home/stores/home.action";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SearchContextEnum} from "@home/enums/search-context.enum";
import {ViewModeTableEnum} from "@home/enums/view-mode-table.enum";
import {IndexedDepositHelper} from "@home/helpers/indexed-deposit.helper";
import {SearchHelper} from "@home/helpers/search.helper";
import {
  IndexedDepositEntry,
  PublishedDeposit,
} from "@home/models/published-deposit.model";
import {StatisticsInfo} from "@home/models/statistics-info.model";
import {
  AskCorrectionMessage,
  BibliographyFormat,
  ContactableContributor,
  Deposit,
  PublicationContactMessage,
  StoredSearchCriteria,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {CookieEnum} from "@shared/enums/cookie.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DepositRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AouCookieConsentHelper} from "@shared/helpers/aou-cookie-consent.helper";
import {DepositRoleHelper} from "@shared/helpers/deposit-role.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  Citation,
  CollectionTyped,
  CookieHelper,
  defaultResourceStateInitValue,
  DownloadService,
  Facet,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  ObjectUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  SsrUtil,
  StoreUtil,
  StringUtil,
  TransferStateService,
} from "solidify-frontend";

const DEFAULT_VIEW_MODE_TABLE: ViewModeTableEnum = environment.defaultHomeViewModeTableEnum;

export interface HomeStateModel extends ResourceStateModel<PublishedDeposit> {
  search: string;
  searchWithFulltext: boolean;
  withRestrictedAccessMasters: boolean;
  facets: Facet[];
  viewModeTableEnum: ViewModeTableEnum;
  listRelativePublishedDeposit: PublishedDeposit[];
  facetsSelected: MappingObject<Enums.Facet.Name, string[]>;
  advancedSearch: MappingObject<string, string[]>;
  statistics: StatisticsInfo;
  bibliographies: Citation[];
  citations: Citation[];
  listBibliographyFormat: BibliographyFormat[];
  index: number;
  queryParametersDetail: QueryParameters;
  listContactableContributor: ContactableContributor[];
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[];
}

@Injectable()
@State<HomeStateModel>({
  name: StateEnum.home,
  defaults: {
    ...defaultResourceStateInitValue(),
    search: StringUtil.stringEmpty,
    searchWithFulltext: false,
    withRestrictedAccessMasters: false,
    viewModeTableEnum: DEFAULT_VIEW_MODE_TABLE,
    queryParameters: new QueryParameters(environment.defaultPageSizeHomePage, {
      field: environment.defaultPageSortHomePage,
      order: OrderEnum.descending,
    }),
    listRelativePublishedDeposit: undefined,
    facets: undefined,
    facetsSelected: {} as MappingObject<Enums.Facet.Name, string[]>,
    advancedSearch: {} as MappingObject<string, string[]>,
    statistics: undefined,
    bibliographies: [],
    citations: [],
    listBibliographyFormat: [],
    index: undefined,
    queryParametersDetail: undefined,
    listContactableContributor: [],
    listUserRoleEnum: [],
  },
  children: [],
})
export class HomeState extends BasicState<HomeStateModel> {
  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _httpClient: HttpClient,
              private readonly _notificationService: NotificationService,
              private readonly _downloadService: DownloadService,
              private readonly _actions$: Actions,
              private readonly _securityService: SecurityService,
              private readonly _transferState: TransferStateService) {
    super();
  }

  @Selector()
  static current(state: HomeStateModel): PublishedDeposit {
    return state.current;
  }

  @Selector()
  static search(state: HomeStateModel): string {
    return state.search;
  }

  @Selector()
  static isLoading(state: HomeStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: HomeStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Action(HomeAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(new HomeAction.Search(false, {
      search: ctx.getState().search,
      searchWithFulltext: ctx.getState().searchWithFulltext,
      withRestrictedAccessMasters: ctx.getState().withRestrictedAccessMasters,
      facetsSelected: ctx.getState().facetsSelected,
      advancedSearch: ctx.getState().advancedSearch,
    }, undefined, SearchContextEnum.LIST));
  }

  @Action(HomeAction.Search)
  search(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): Observable<CollectionTyped<IndexedDepositEntry>> {
    let keyTransferState = "HomeState-Search";

    let queryParametersToUse: QueryParameters;
    const viewMode = this._getViewModeToApply(ctx, action);
    const patchState = {
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      search: action.portalSearch?.search,
      searchWithFulltext: action.portalSearch?.searchWithFulltext,
      withRestrictedAccessMasters: action.portalSearch?.withRestrictedAccessMasters,
      facetsSelected: action.portalSearch?.facetsSelected,
      advancedSearch: action.portalSearch?.advancedSearch,
      viewModeTableEnum: viewMode,
    } as HomeStateModel;

    if (action.searchContext === SearchContextEnum.DETAIL) {
      keyTransferState += "-DetailPage";
      patchState.queryParametersDetail = action.queryParameters;
      queryParametersToUse = patchState.queryParametersDetail;
      queryParametersToUse.sort = ObjectUtil.clone(ctx.getState().queryParameters.sort);
    } else /* LIST | EXECUTE_ONLY */ {
      queryParametersToUse = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
      if (action.resetPagination) {
        queryParametersToUse = QueryParametersUtil.resetToFirstPage(queryParametersToUse);
      }
      if (action.searchContext === SearchContextEnum.LIST) {
        patchState.queryParameters = queryParametersToUse;
      }
    }

    ctx.patchState(patchState);

    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    const listAdvancedSearchCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.listAdvancedSearchCriteria);
    const storedSearchCriteriaList = SearchHelper.adaptPortalSearchToStoredSearchCriteriaList(portalSearch, listAdvancedSearchCriteria);

    let withRestrictedAccessMasters = action.portalSearch?.withRestrictedAccessMasters;
    if (isNullOrUndefined(withRestrictedAccessMasters)) {
      const withRestrictedAccessMasterValue = CookieHelper.getItem(CookieEnum.publicationWithRestrictedAccessMasters);
      withRestrictedAccessMasters = withRestrictedAccessMasterValue === SOLIDIFY_CONSTANTS.STRING_TRUE || isNullOrUndefined(withRestrictedAccessMasterValue);
    }

    queryParametersToUse = QueryParametersUtil.clone(queryParametersToUse);
    const search = QueryParametersUtil.getSearchItems(queryParametersToUse);

    if (withRestrictedAccessMasters) {
      keyTransferState += "-" + SearchHelper.WITH_RESTRICTED_ACCESS_MASTERS_QUERY_PARAM;
      MappingObjectUtil.set(search, SearchHelper.WITH_RESTRICTED_ACCESS_MASTERS_QUERY_PARAM, SOLIDIFY_CONSTANTS.STRING_TRUE);
    }
    if (isNonEmptyArray(action.portalSearch?.withFacets)) {
      MappingObjectUtil.set(search, SearchHelper.SEARCH_FACETS, action.portalSearch.withFacets.join(SOLIDIFY_CONSTANTS.COMMA));
    }

    const STATE_KEY = makeStateKey<CollectionTyped<IndexedDepositEntry>>(keyTransferState);
    if (this._transferState.hasKey(STATE_KEY)) {
      const collectionTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.SearchSuccess(action, collectionTransferState));
      return of(collectionTransferState);
    }

    return this._apiService.postQueryParameters<StoredSearchCriteria[], CollectionTyped<IndexedDepositEntry>>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, storedSearchCriteriaList, queryParametersToUse)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, action.cancelUncompleted ? [HomeAction.Search] : []),
        tap((collection: CollectionTyped<IndexedDepositEntry>) => {
          collection._data = collection._data.map(c => this._adaptIndexedDepositEntry(c).metadata);
          this._transferState.set(STATE_KEY, collection);
          ctx.dispatch(new HomeAction.SearchSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchFail(action));
          throw error;
        }),
      );
  }

  private _adaptIndexedDepositEntry(indexedDepositEntryToUpdate: IndexedDepositEntry): IndexedDepositEntry {
    const indexedDepositEntry = ObjectUtil.clone(indexedDepositEntryToUpdate);
    indexedDepositEntry.metadata.resId = indexedDepositEntry.resId;
    if (isNotNullNorUndefinedNorWhiteString(indexedDepositEntry.metadata.container?.title)) {
      indexedDepositEntry.metadata.container.shortTitle = indexedDepositEntry.metadata.container.title;
      if (indexedDepositEntry.metadata.container.shortTitle.length > 50) {
        indexedDepositEntry.metadata.container.shortTitle = indexedDepositEntry.metadata.container.title.substring(0, 50) + "...";
      }
    }
    return indexedDepositEntry;
  }

  @Action(HomeAction.SearchSuccess)
  searchSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchSuccess): void {
    if (action.parentAction.searchContext === SearchContextEnum.DETAIL) {
      const queryParameters = QueryParametersUtil.clone(ctx.getState().queryParametersDetail);
      const paging = queryParameters.paging;
      paging.length = isNullOrUndefined(action.collection) ? 0 : action.collection._page.totalItems;
      queryParameters.paging = paging;
      const publishedDeposit = action.collection._data[0];
      ctx.patchState({
        current: publishedDeposit,
        isLoadingCounter: ctx.getState().isLoadingCounter - 1,
        listUserRoleEnum: [],
        queryParametersDetail: queryParameters,
      });
      if (isNotNullNorUndefined(publishedDeposit)) {
        ctx.dispatch([
          new HomeAction.ListPublicationUserRoles(publishedDeposit.resId),
          new HomeAction.GetStatistics(publishedDeposit.resId),
          new HomeAction.GetCitations(publishedDeposit.resId),
        ]);
      }
    } else if (action.parentAction.searchContext === SearchContextEnum.LIST) {
      const queryParameters = StoreUtil.updateQueryParameters(ctx, action.collection);
      ctx.patchState({
        list: isNullOrUndefined(action.collection) ? [] : action.collection._data,
        total: isNullOrUndefined(action.collection) ? 0 : action.collection._page.totalItems,
        facets: isNullOrUndefined(action.collection) ? [] : action.collection._facets,
        isLoadingCounter: ctx.getState().isLoadingCounter - 1,
        queryParameters: queryParameters,
        queryParametersDetail: undefined,
      });
    } else if (action.parentAction.searchContext === SearchContextEnum.EXECUTE_ONLY) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      });
    }
  }

  @Action(HomeAction.SearchFail)
  searchFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchFail): void {
    if (action.parentAction.searchContext === SearchContextEnum.DETAIL) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      });
    } else if (action.parentAction.searchContext === SearchContextEnum.LIST) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter - 1,
        list: [],
        total: 0,
      });
    } else if (action.parentAction.searchContext === SearchContextEnum.EXECUTE_ONLY) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      });
    }
  }

  @Action(HomeAction.RunStoredSearch)
  runStoredSearch(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.RunStoredSearch): Observable<CollectionTyped<IndexedDepositEntry>> {
    const queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: queryParameters,
    });

    return this._apiService.getCollection<IndexedDepositEntry>(this._urlResource + urlSeparator + ApiActionNameEnum.RUN_STORED_SEARCH + urlSeparator + action.searchId, queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        tap((collection: CollectionTyped<IndexedDepositEntry>) => {
          collection._data = collection._data.map(c => {
            c.metadata.resId = c.resId;
            return c.metadata;
          });
          ctx.dispatch(new HomeAction.RunStoredSearchSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.RunSearchFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.RunStoredSearchSuccess)
  runStoredSearchSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.RunStoredSearchSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: action.collection._data,
    });
  }

  @Action(HomeAction.RunStoredSearch)
  runStoredSearchFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.RunStoredSearch): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.CombineStoredSearches)
  combineStoredSearches(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CombineStoredSearches): Observable<CollectionTyped<IndexedDepositEntry>> {
    const queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
    const search = {};
    if (isNonEmptyArray(action.listIdAdvancedSearch)) {
      search[AOU_CONSTANTS.SEARCHES] = action.listIdAdvancedSearch.join(SOLIDIFY_CONSTANTS.COMMA);
    }
    if (isNonEmptyArray(action.listIdPinned)) {
      search[AOU_CONSTANTS.PUBLICATION_IDS] = action.listIdPinned.join(SOLIDIFY_CONSTANTS.COMMA);
    }
    if (isNotNullNorUndefinedNorWhiteString(action.contributorCnIndividu)) {
      search[AOU_CONSTANTS.CONTRIBUTOR] = action.contributorCnIndividu;
    }
    if (isNotNullNorUndefinedNorWhiteString(action.combinationType)) {
      search[AOU_CONSTANTS.COMBINATION_TYPE] = action.combinationType;
    }

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: queryParameters,
    });

    return this._apiService.getCollection<IndexedDepositEntry>(this._urlResource + urlSeparator + ApiActionNameEnum.COMBINE_STORED_SEARCHES, queryParameters, search)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        tap((collection: CollectionTyped<IndexedDepositEntry>) => {
          collection._data = collection._data.map(c => {
            c.metadata.resId = c.resId;
            return c.metadata;
          });
          ctx.dispatch(new HomeAction.CombineStoredSearchesSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.CombineStoredSearchesFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.CombineStoredSearchesSuccess)
  combineStoredSearchesSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CombineStoredSearchesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: action.collection._data,
    });
  }

  @Action(HomeAction.CombineStoredSearchesFail)
  combineStoredSearchesFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CombineStoredSearchesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.GetBibliographyJavascript)
  getBibliographyJavascript(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetBibliographyJavascript): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    let headers = new HttpHeaders();
    headers = headers.set("Accept", ["application/xml"]);

    return this._httpClient.get<string>(action.url,
      {
        headers,
        observe: "response",
        responseType: "text",
      } as any)
      .pipe(
        map((response: HttpResponse<string>) => response.body),
        tap((javascript: string) => {
          ctx.dispatch(new HomeAction.GetBibliographyJavascriptSuccess(action, javascript));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetBibliographyJavascriptFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.GetBibliographyJavascriptSuccess)
  getBibliographyJavascriptSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetBibliographyJavascriptSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.GetBibliographyJavascriptFail)
  getBibliographyJavascriptFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetBibliographyJavascriptFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.SearchRelativePublishedDeposit)
  searchRelativePublishedDeposit(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchRelativePublishedDeposit): Observable<CollectionTyped<any/*Metadata*/>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParameters = new QueryParameters(5);

    const storedSearchCriteria: StoredSearchCriteria[] = [];
    storedSearchCriteria.push({
      field: "cnIndividu",
      value: "TODO", // action.archive.creator.lastName + ", " + action.archive.creator.firstName,
      type: Enums.StoredSearch.Type.TERM,
      source: Enums.StoredSearch.CriteriaSource.ADVANCED_SEARCH,
      booleanClauseType: Enums.StoredSearch.BooleanClauseType.MUST,
    });

    return this._apiService.postQueryParameters<StoredSearchCriteria[], CollectionTyped<IndexedDepositEntry>>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, storedSearchCriteria, queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        tap((collection: CollectionTyped<IndexedDepositEntry>) => {
          collection._data = collection._data.map(indexDepositEntry => IndexedDepositHelper.adaptArchiveMetadataInArchive(indexDepositEntry));
          ctx.dispatch(new HomeAction.SearchRelativePublishedDepositSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchRelativePublishedDepositFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.SearchRelativePublishedDepositSuccess)
  searchRelativePublishedDepositSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchRelativePublishedDepositSuccess): void {
    ctx.patchState({
      listRelativePublishedDeposit: isNullOrUndefined(action.list) ? [] : action.list._data.filter(archive => archive.resId !== action.parentAction.publishedDeposit.resId),
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.SearchRelativePublishedDepositFail)
  searchRelativePublishedDepositFail(ctx: SolidifyStateContext<HomeStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.GetById)
  getById(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetById): Observable<IndexedDepositEntry> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
      listRelativePublishedDeposit: undefined,
    });

    const STATE_KEY = makeStateKey<PublishedDeposit>(`HomeState-GetById-${action.resId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const publishedDepositTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.GetByIdSuccess(action, publishedDepositTransferState));
      return of(publishedDepositTransferState);
    }

    return this._apiService.get<IndexedDepositEntry>(this._urlResource, {
      [action.type]: action.resId,
    })
      .pipe(
        tap(model => {
          const publishedDeposit = IndexedDepositHelper.adaptArchiveMetadataInArchive(model);
          this._transferState.set(STATE_KEY, publishedDeposit);
          ctx.dispatch(new HomeAction.GetByIdSuccess(action, publishedDeposit));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetByIdFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetByIdSuccess): void {
    ctx.patchState({
      current: action.publishedDeposit,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch([
      new HomeAction.ListPublicationUserRoles(action.publishedDeposit.resId),
      new HomeAction.SearchRelativePublishedDeposit(action.publishedDeposit),
      new HomeAction.GetStatistics(action.publishedDeposit.resId),
      new HomeAction.GetCitations(action.publishedDeposit.resId),
    ]);
  }

  @Action(HomeAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CleanCurrent): void {
    ctx.patchState({
      current: undefined,
      listRelativePublishedDeposit: undefined,
      listUserRoleEnum: [],
      statistics: undefined,
      citations: [],
      bibliographies: [],
    });
  }

  @Action(HomeAction.CleanSearch)
  cleanSearch(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CleanSearch): void {
    ctx.patchState({
      list: [],
      search: StringUtil.stringEmpty,
      searchWithFulltext: false,
      withRestrictedAccessMasters: AouCookieConsentHelper.isWithRestrictedAccessMasters(),
      viewModeTableEnum: DEFAULT_VIEW_MODE_TABLE,
      queryParameters: new QueryParameters(environment.defaultPageSizeHomePage),
      facets: undefined,
      facetsSelected: {} as MappingObject<Enums.Facet.Name, string[]>,
      advancedSearch: {} as MappingObject<string, string[]>,
      index: undefined,
      queryParametersDetail: undefined,
    });
  }

  private _getViewModeToApply(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): ViewModeTableEnum {
    const currentViewModeInState = ctx.getState().viewModeTableEnum;
    const currentViewToUse = isNullOrUndefined(currentViewModeInState) ? DEFAULT_VIEW_MODE_TABLE : currentViewModeInState;
    return isNullOrUndefined(action.portalSearch?.viewTabMode) ? currentViewToUse : action.portalSearch.viewTabMode;
  }

  @Action(HomeAction.NavigateFromListToDetail)
  navigateFromListToDetail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.NavigateFromListToDetail): void {
    const list = ctx.getState().list;
    const paging = ctx.getState().queryParameters.paging;
    const baseIndex = paging.pageIndex * paging.pageSize;
    const index = baseIndex + list.findIndex(p => p.archiveId === action.publishedDepositId);
    const queryParameters = new QueryParameters(1);
    const total = ctx.getState().total;
    queryParameters.paging.pageIndex = index;
    queryParameters.paging.length = total;
    ctx.patchState({
      index: index,
      queryParametersDetail: queryParameters,
    });
    ctx.dispatch(new Navigate([RoutesEnum.homeDetail, action.publishedDepositId]));
  }

  @Action(HomeAction.GetStatistics)
  getStatistics(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatistics): Observable<StatisticsInfo> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      statistics: undefined,
    });

    const STATE_KEY = makeStateKey<StatisticsInfo>(`HomeState-Statistics-${action.publishedDepositId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const statisticsInfoTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.GetStatisticsSuccess(action, statisticsInfoTransferState));
      return of(statisticsInfoTransferState);
    }

    return this._apiService.get<StatisticsInfo>(`${this._urlResource}/${action.publishedDepositId}/${ApiActionNameEnum.STATISTICS}`)
      .pipe(
        tap((model: StatisticsInfo) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new HomeAction.GetStatisticsSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetStatisticsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.GetStatisticsSuccess)
  getStatisticsSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatisticsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      statistics: action.statistics,
    });
  }

  @Action(HomeAction.GetStatisticsFail)
  getStatisticsFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatisticsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.GetAllBibliographyFormats)
  getAllBibliographyFormats(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetAllBibliographyFormats): Observable<BibliographyFormat[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.getList<BibliographyFormat>(ApiEnum.accessSettings + urlSeparator + ApiActionNameEnum.LIST_BIBLIOGRAPHY_FORMATS, undefined)
      .pipe(
        tap((list: BibliographyFormat[]) => {
          ctx.dispatch(new HomeAction.GetAllBibliographyFormatsSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetAllBibliographyFormatsFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.GetAllBibliographyFormatsSuccess)
  getAllBibliographyFormatsSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetAllBibliographyFormatsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listBibliographyFormat: action.list,
    });
  }

  @Action(HomeAction.GetAllBibliographyFormatsFail)
  getAllBibliographyFormatsFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetAllBibliographyFormatsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.AskCorrection)
  askCorrection(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.AskCorrection): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<AskCorrectionMessage, Result>(ApiEnum.adminPublications + urlSeparator + action.publicationId + urlSeparator + ApiActionNameEnum.ASK_CORRECTION, action.askCorrectionMessage)
      .pipe(
        tap((result: Result) => {
          if (result.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new HomeAction.AskCorrectionSuccess(action, result));
          } else {
            ctx.dispatch(new HomeAction.AskCorrectionFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.AskCorrectionFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.AskCorrectionSuccess)
  askCorrectionSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.AskCorrectionSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.notificationRequestForCorrectionSent);
  }

  @Action(HomeAction.AskCorrectionFail)
  askCorrectionFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.AskCorrectionFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.notificationUnableToRequestForCorrection);
  }

  @Action(HomeAction.ExportToOrcid)
  exportToOrcid(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportToOrcid): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(ApiEnum.adminPublications + urlSeparator + action.publishedDeposit.resId + urlSeparator + ApiActionNameEnum.EXPORT_TO_ORCID)
      .pipe(
        tap((result: Result) => {
          if (result.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new HomeAction.ExportToOrcidSuccess(action, result));
          } else {
            ctx.dispatch(new HomeAction.ExportToOrcidFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.ExportToOrcidFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.ExportToOrcidSuccess)
  exportToOrcidSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportToOrcidSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
  }

  @Action(HomeAction.ExportToOrcidFail)
  exportToOrcidFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportToOrcidFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (isNotNullNorUndefined((action.result))) {
      this._notificationService.showWarning(action.result.message);
    } else {
      this._notificationService.showError(LabelTranslateEnum.notificationUnableToExportPublicationToOrcid);
    }
  }

  @Action(HomeAction.GetContactableContributor)
  getContactableContributor(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetContactableContributor): Observable<ContactableContributor[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = `${ApiEnum.adminPublications}/${action.publicationId}/${ApiActionNameEnum.GET_CONTACTABLE_CONTRIBUTORS}`;
    return this._apiService.post<ContactableContributor[]>(url)
      .pipe(
        tap((list: ContactableContributor[]) => {
          ctx.dispatch(new HomeAction.GetContactableContributorSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetContactableContributorFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.GetContactableContributorSuccess)
  getContactableContributorSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetContactableContributorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listContactableContributor: action.list,
    });
  }

  @Action(HomeAction.GetContactableContributorFail)
  getContactableContributorFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetContactableContributorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.ContactContributor)
  contactContributor(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ContactContributor): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    let url = `${ApiEnum.adminPublications}/${action.publicationId}/${ApiActionNameEnum.CONTACT_CONTRIBUTOR}`;
    if (isNullOrUndefinedOrWhiteString(action.publicationId)) {
      url = `${ApiEnum.adminContributors}/${ApiActionNameEnum.CONTACT_CONTRIBUTOR}`;
    }
    return this._apiService.post<PublicationContactMessage, Result>(url, action.publicationContactMessage)
      .pipe(
        tap((result: Result) => {
          if (result.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new HomeAction.ContactContributorSuccess(action, result));
          } else {
            ctx.dispatch(new HomeAction.ContactContributorFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.ContactContributorFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.ContactContributorSuccess)
  contactContributorSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ContactContributorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.notificationMessageSent);
  }

  @Action(HomeAction.ContactContributorFail)
  contactContributorFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ContactContributorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.notificationUnableToSendMessage);
  }

  @Action(HomeAction.ListPublicationUserRoles)
  listPublicationUserRoles(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ListPublicationUserRoles): Observable<Enums.Deposit.UserRoleEnum[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    if (!this._securityService.isLoggedIn()) {
      ctx.dispatch(new HomeAction.ListPublicationUserRolesFail(action));
      return;
    }

    return this._apiService.get<Enums.Deposit.UserRoleEnum[]>(ApiEnum.adminPublications + urlSeparator + action.depositId + urlSeparator + ApiActionNameEnum.LIST_PUBLICATION_USER_ROLES)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.ListPublicationUserRoles]),
        tap(listUserRoleEnum => {
          ctx.dispatch(new HomeAction.ListPublicationUserRolesSuccess(action, listUserRoleEnum));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.ListPublicationUserRolesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.ListPublicationUserRolesSuccess)
  listPublicationUserRolesSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ListPublicationUserRolesSuccess): void {
    const state: Partial<HomeStateModel> = {
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    };
    if (ctx.getState().current?.resId === action.parentAction.depositId) {
      state.listUserRoleEnum = action.listUserRoleEnum;
    }
    ctx.patchState(state);
  }

  @Action(HomeAction.ListPublicationUserRolesFail)
  listPublicationUserRolesFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ListPublicationUserRolesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.IsAllowedToUpdate)
  isAllowedToUpdate(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.IsAllowedToUpdate): Observable<Deposit> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.getById<Deposit>(ApiEnum.adminPublications, action.publishedDeposit.resId)
      .pipe(
        tap(deposit => {
          const isAllowedToUpdate = deposit?.status === Enums.Deposit.StatusEnum.COMPLETED;
          const isAlreadyReadyToUpdate = deposit?.status === Enums.Deposit.StatusEnum.IN_EDITION;
          if (isAllowedToUpdate) {
            ctx.dispatch(new HomeAction.IsAllowedToUpdateSuccess(action, "ALLOWED_TO_UPDATE"));
          } else if (isAlreadyReadyToUpdate) {
            ctx.dispatch(new HomeAction.IsAllowedToUpdateSuccess(action, "ALREADY_IN_UPDATE"));
          } else {
            ctx.dispatch(new HomeAction.IsAllowedToUpdateFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.IsAllowedToUpdateFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.IsAllowedToUpdateSuccess)
  isAllowedToUpdateSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.IsAllowedToUpdateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (action.mode === "ALLOWED_TO_UPDATE") {
      ctx.dispatch(new HomeAction.StartEditingPublication(action.parentAction.publishedDeposit));
    } else if (action.mode === "ALREADY_IN_UPDATE") {
      this._redirectToPublicationDetail(ctx, action.parentAction.publishedDeposit.resId);
    }
  }

  @Action(HomeAction.IsAllowedToUpdateFail)
  isAllowedToUpdateFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.IsAllowedToUpdateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(LabelTranslateEnum.notificationTheStatusOfThePublicationDoesNotAllowItsUpdate);
  }

  @Action(HomeAction.StartEditingPublication)
  startEditingMetadata(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.StartEditingPublication): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(ApiEnum.adminPublications + urlSeparator + action.publishedDeposit.resId + urlSeparator + ApiActionNameEnum.START_METADATA_EDITING)
      .pipe(
        tap(result => {
          if (result?.status === Enums.Result.ActionStatusEnum.EXECUTED) {
            ctx.dispatch(new HomeAction.StartEditingPublicationSuccess(action));
          } else {
            ctx.dispatch(new HomeAction.StartEditingPublicationFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.StartEditingPublicationFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.StartEditingPublicationSuccess)
  startEditingPublicationSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.StartEditingPublicationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._redirectToPublicationDetail(ctx, action.parentAction.publishedDeposit.resId);
  }

  @Action(HomeAction.StartEditingPublicationFail)
  startEditingPublicationFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.StartEditingPublicationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.unableToStartEditingMetadataOnDeposit);
  }

  private _redirectToPublicationDetail(ctx: SolidifyStateContext<HomeStateModel>, publishedDepositId: string): void {
    let depositPath = RoutesEnum.depositDetail;
    if (this._securityService.isRootOrAdmin() || DepositRoleHelper.isValidator(ctx.getState().listUserRoleEnum)) {
      depositPath = RoutesEnum.depositToValidateDetail;
    }
    ctx.dispatch(new Navigate([depositPath, publishedDepositId, DepositRoutesEnum.edit, Enums.Deposit.StepEnum.FILES]));
  }

  @Action(HomeAction.GetCitations)
  getCitation(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetCitations): Observable<Citation[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      citations: undefined,
    });

    return this._apiService.get<Citation[]>(ApiEnum.accessBibliography + urlSeparator + action.publicationId)
      .pipe(
        tap((citations: Citation[]) => {
          ctx.dispatch(new HomeAction.GetCitationsSuccess(action, citations));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetCitationsFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.GetCitationsSuccess)
  getCitationSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetCitationsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      citations: action.citations,
    });
  }

  @Action(HomeAction.GetCitationsFail)
  getCitationFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetCitationsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.ExportBibliography)
  generateBibliography(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportBibliography): void {
    SsrUtil.window.open(action.url, "_blank");
  }
}
