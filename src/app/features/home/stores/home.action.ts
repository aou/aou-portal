/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {SearchContextEnum} from "@home/enums/search-context.enum";
import {PortalSearch} from "@home/models/published-deposit-search.model";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {StatisticsInfo} from "@home/models/statistics-info.model";
import {
  AskCorrectionMessage,
  BibliographyFormat,
  ContactableContributor,
  PublicationContactMessage,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  Citation,
  CollectionTyped,
  QueryParameters,
  Result,
} from "solidify-frontend";

const state = StateEnum.home;

export namespace HomeAction {
  export class ChangeQueryParameters {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public queryParameters?: QueryParameters) {
    }
  }

  export class Search extends BaseAction {
    static readonly type: string = `[${state}] Search`;

    constructor(public resetPagination: boolean, public portalSearch: PortalSearch, public queryParameters?: QueryParameters, public searchContext: SearchContextEnum = SearchContextEnum.LIST, public cancelUncompleted = true) {
      super();
    }
  }

  export class SearchSuccess extends BaseSubActionSuccess<Search> {
    static readonly type: string = `[${state}] Search Success`;

    constructor(public parentAction: Search, public collection?: CollectionTyped<PublishedDeposit> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchFail extends BaseSubActionFail<Search> {
    static readonly type: string = `[${state}] Search Fail`;
  }

  export class RunStoredSearch extends BaseAction {
    static readonly type: string = `[${state}] Run Stored Search`;

    constructor(public searchId: string, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class RunStoredSearchSuccess extends BaseSubActionSuccess<RunStoredSearch> {
    static readonly type: string = `[${state}] Run Stored Search Success`;

    constructor(public parentAction: RunStoredSearch, public collection?: CollectionTyped<PublishedDeposit> | null | undefined) {
      super(parentAction);
    }
  }

  export class RunSearchFail extends BaseSubActionFail<RunStoredSearch> {
    static readonly type: string = `[${state}] Run Stored Search Fail`;
  }

  export class CombineStoredSearches extends BaseAction {
    static readonly type: string = `[${state}] Combine Stored Searches`;

    constructor(public listIdAdvancedSearch: string[], public listIdPinned: string[], public contributorCnIndividu: string, public combinationType?: "or" | "and", public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class CombineStoredSearchesSuccess extends BaseSubActionSuccess<CombineStoredSearches> {
    static readonly type: string = `[${state}] Combine Stored Searches Success`;

    constructor(public parentAction: CombineStoredSearches, public collection: CollectionTyped<PublishedDeposit> | null | undefined) {
      super(parentAction);
    }
  }

  export class CombineStoredSearchesFail extends BaseSubActionFail<CombineStoredSearches> {
    static readonly type: string = `[${state}] Combine Stored Searches Fail`;
  }

  export class GetBibliographyJavascript extends BaseAction {
    static readonly type: string = `[${state}] Get Bibliography Javascript`;

    constructor(public url?: string) {
      super();
    }
  }

  export class GetBibliographyJavascriptSuccess extends BaseSubActionSuccess<GetBibliographyJavascript> {
    static readonly type: string = `[${state}] Get Bibliography Javascript Success`;

    constructor(public parentAction: GetBibliographyJavascript, public javascript: string) {
      super(parentAction);
    }
  }

  export class GetBibliographyJavascriptFail extends BaseSubActionFail<GetBibliographyJavascript> {
    static readonly type: string = `[${state}] Get Bibliography Javascript Fail`;
  }

  export class ExportBibliography extends BaseAction {
    static readonly type: string = `[${state}] Export Bibliography`;

    constructor(public url?: string) {
      super();
    }
  }

  export class SearchRelativePublishedDeposit extends BaseAction {
    static readonly type: string = `[${state}] Search Relative Published Deposit`;

    constructor(public publishedDeposit: PublishedDeposit) {
      super();
    }
  }

  export class SearchRelativePublishedDepositSuccess extends BaseSubActionSuccess<SearchRelativePublishedDeposit> {
    static readonly type: string = `[${state}] Search Relative Published Deposit Success`;

    constructor(public parentAction: SearchRelativePublishedDeposit, public list: CollectionTyped<PublishedDeposit> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchRelativePublishedDepositFail extends BaseSubActionFail<SearchRelativePublishedDeposit> {
    static readonly type: string = `[${state}] Search Relative Published Deposit Fail`;
  }

  export class GetById extends BaseAction {
    static readonly type: string = `[${state}] Get By Id`;

    constructor(public resId: string, public type: "resId" | "archiveId") {
      super();
    }
  }

  export class GetByIdSuccess extends BaseSubActionSuccess<GetById> {
    static readonly type: string = `[${state}] Get By Id Success`;

    constructor(public parentAction: GetById, public publishedDeposit: PublishedDeposit) {
      super(parentAction);
    }
  }

  export class GetByIdFail extends BaseSubActionFail<GetById> {
    static readonly type: string = `[${state}] Get By Id Fail`;
  }

  export class Download extends BaseAction {
    static readonly type: string = `[${state}] Download`;

    constructor(public publishedDeposit: PublishedDeposit) {
      super();
    }
  }

  export class DownloadStart extends BaseSubAction<Download> {
    static readonly type: string = `[${state}] Download Start`;
  }

  export class DownloadSuccess extends BaseSubActionSuccess<DownloadStart> {
    static readonly type: string = `[${state}] Download Success`;
  }

  export class DownloadFail extends BaseSubActionFail<DownloadStart> {
    static readonly type: string = `[${state}] Download Fail`;
  }

  export class CleanCurrent {
    static readonly type: string = `[${state}] Clean Current`;
  }

  export class CleanSearch {
    static readonly type: string = `[${state}] Clean Search`;
  }

  export class NavigateFromListToDetail {
    static readonly type: string = `[${state}] Navigate From List To Detail`;

    constructor(public publishedDepositId: string) {}
  }

  export class GetStatistics extends BaseAction {
    static readonly type: string = `[${state}] Get Statistics`;

    constructor(public publishedDepositId: string) {
      super();
    }
  }

  export class GetStatisticsSuccess extends BaseSubActionSuccess<GetStatistics> {
    static readonly type: string = `[${state}] Get Statistics Success`;

    constructor(public parentAction: GetStatistics, public statistics: StatisticsInfo) {
      super(parentAction);
    }
  }

  export class GetStatisticsFail extends BaseSubActionFail<GetStatistics> {
    static readonly type: string = `[${state}] Get Statistics Fail`;
  }

  export class GetAllBibliographyFormats extends BaseAction {
    static readonly type: string = `[${state}] Get All Bibliography Formats`;
  }

  export class GetAllBibliographyFormatsSuccess extends BaseSubActionSuccess<GetAllBibliographyFormats> {
    static readonly type: string = `[${state}] Get All Bibliography Formats Success`;

    constructor(public parentAction: GetAllBibliographyFormats, public list?: BibliographyFormat[]) {
      super(parentAction);
    }
  }

  export class GetAllBibliographyFormatsFail extends BaseSubActionFail<GetAllBibliographyFormats> {
    static readonly type: string = `[${state}] Get All Bibliography Formats Fail`;
  }

  export class AskCorrection extends BaseAction {
    static readonly type: string = `[${state}] Ask Correction`;

    constructor(public publicationId: string, public askCorrectionMessage: AskCorrectionMessage) {
      super();
    }
  }

  export class AskCorrectionSuccess extends BaseSubActionSuccess<AskCorrection> {
    static readonly type: string = `[${state}] Ask Correction Success`;

    constructor(public parentAction: AskCorrection, public list?: Result) {
      super(parentAction);
    }
  }

  export class AskCorrectionFail extends BaseSubActionFail<AskCorrection> {
    static readonly type: string = `[${state}] Ask Correction Fail`;
  }

  export class ExportToOrcid extends BaseAction {
    static readonly type: string = `[${state}] ExportToOrcid`;

    constructor(public publishedDeposit: PublishedDeposit) {
      super();
    }
  }

  export class ExportToOrcidSuccess extends BaseSubActionFail<ExportToOrcid> {
    static readonly type: string = `[${state}] ExportToOrcid Success`;

    constructor(public parentAction: ExportToOrcid, public result?: Result) {
      super(parentAction);
    }
  }

  export class ExportToOrcidFail extends BaseSubActionFail<ExportToOrcid> {
    static readonly type: string = `[${state}] ExportToOrcid Fail`;

    constructor(public parentAction: ExportToOrcid, public result?: Result) {
      super(parentAction);
    }
  }

  export class GetContactableContributor extends BaseAction {
    static readonly type: string = `[${state}] Get Contactable Contributor`;

    constructor(public publicationId: string) {
      super();
    }
  }

  export class GetContactableContributorSuccess extends BaseSubActionSuccess<GetContactableContributor> {
    static readonly type: string = `[${state}] Get Contactable Contributor Success`;

    constructor(public parentAction: GetContactableContributor, public list: ContactableContributor[]) {
      super(parentAction);
    }
  }

  export class GetContactableContributorFail extends BaseSubActionFail<GetContactableContributor> {
    static readonly type: string = `[${state}] Get Contactable Contributor Fail`;
  }

  export class ContactContributor extends BaseAction {
    static readonly type: string = `[${state}] Contact Contributor`;

    constructor(public publicationContactMessage: PublicationContactMessage, public publicationId?: string) {
      super();
    }
  }

  export class ContactContributorSuccess extends BaseSubActionSuccess<ContactContributor> {
    static readonly type: string = `[${state}] Contact Contributor Success`;

    constructor(public parentAction: ContactContributor, public list?: Result) {
      super(parentAction);
    }
  }

  export class ContactContributorFail extends BaseSubActionFail<ContactContributor> {
    static readonly type: string = `[${state}] Contact Contributor Fail`;
  }

  export class ListPublicationUserRoles extends BaseAction {
    static readonly type: string = `[${state}] List Publication User Roles`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class ListPublicationUserRolesSuccess extends BaseSubActionSuccess<ListPublicationUserRoles> {
    static readonly type: string = `[${state}] List Publication User Roles Success`;

    constructor(public parentAction: ListPublicationUserRoles, public listUserRoleEnum: Enums.Deposit.UserRoleEnum[]) {
      super(parentAction);
    }
  }

  export class ListPublicationUserRolesFail extends BaseSubActionFail<ListPublicationUserRoles> {
    static readonly type: string = `[${state}] List Publication User Roles Fail`;
  }

  export class IsAllowedToUpdate extends BaseAction {
    static readonly type: string = `[${state}] Is Allowed To Update`;

    constructor(public publishedDeposit: PublishedDeposit) {
      super();
    }
  }

  export class IsAllowedToUpdateSuccess extends BaseSubActionSuccess<IsAllowedToUpdate> {
    static readonly type: string = `[${state}] Is Allowed To Update Success`;

    constructor(public parentAction: IsAllowedToUpdate, public mode: "ALREADY_IN_UPDATE" | "ALLOWED_TO_UPDATE") {
      super(parentAction);
    }
  }

  export class IsAllowedToUpdateFail extends BaseSubActionFail<IsAllowedToUpdate> {
    static readonly type: string = `[${state}] Is Allowed To Update Fail`;
  }

  export class StartEditingPublication extends BaseAction {
    static readonly type: string = `[${state}] Start Editing publication`;

    constructor(public publishedDeposit: PublishedDeposit) {
      super();
    }
  }

  export class StartEditingPublicationSuccess extends BaseSubActionSuccess<StartEditingPublication> {
    static readonly type: string = `[${state}] Start Editing publication Success`;
  }

  export class StartEditingPublicationFail extends BaseSubActionFail<StartEditingPublication> {
    static readonly type: string = `[${state}] Start Editing publication Fail`;
  }

  export class GetCitations extends BaseAction {
    static readonly type: string = `[${state}] Get Citations`;

    constructor(public publicationId: string) {
      super();
    }
  }

  export class GetCitationsSuccess extends BaseSubActionSuccess<GetCitations> {
    static readonly type: string = `[${state}] Get Citations Success`;

    constructor(public parentAction: GetCitations, public citations: Citation[]) {
      super(parentAction);
    }
  }

  export class GetCitationsFail extends BaseSubActionFail<GetCitations> {
    static readonly type: string = `[${state}] Get Citations Fail`;
  }
}
