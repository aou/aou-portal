/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {HomeBibliographyRoutable} from "@home/components/routables/home-bibliography/home-bibliography.routable";
import {HomePageRoutable} from "@home/components/routables/home-page/home-page.routable";
import {
  HomePinboardRoutable,
  PinboardListTabEnum,
} from "@home/components/routables/home-pinboard/home-pinboard.routable";
import {HomePublishedDepositDetailRoutable} from "@home/components/routables/home-published-deposit-detail/home-published-deposit-detail.routable";
import {HomePublishedDepositNotFoundRoutable} from "@home/components/routables/home-published-deposit-not-found/home-published-deposit-not-found.routable";
import {HomeSearchRoutable} from "@home/components/routables/home-search/home-search.routable";
import {HomeState} from "@home/stores/home.state";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {ArchiveGuardService} from "@shared/guards/archive-guard.service";
import {AouRoutes} from "@shared/models/aou-route.model";
import {CombinedGuardService} from "solidify-frontend";

const routes: AouRoutes = [
  {
    path: "",
    component: HomePageRoutable,
    data: {},
  },
  {
    path: HomePageRoutesEnum.search,
    component: HomeSearchRoutable,
  },
  {
    path: HomePageRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: HomePublishedDepositDetailRoutable,
    data: {
      breadcrumbMemoizedSelector: HomeState.currentTitle,
      guards: [ArchiveGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: HomePageRoutesEnum.notFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: HomePublishedDepositNotFoundRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.publishedDepositNotFound,
    },
  },
  {
    path: HomePageRoutesEnum.pinboard,
    redirectTo: HomePageRoutesEnum.pinboard + AppRoutesEnum.separator + PinboardListTabEnum.storedSearches,
    pathMatch: "full",
  },
  {
    path: HomePageRoutesEnum.pinboard + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: HomePinboardRoutable,
    data: {
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: HomePageRoutesEnum.bibliography,
    component: HomeBibliographyRoutable,
    data: {
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: HomePageRoutesEnum.contributor,
    // @ts-ignore Dynamic import
    loadChildren: () => import("../contributor/contributor.module").then(m => m.ContributorModule),
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
