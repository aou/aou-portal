/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-search.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  MatSlideToggle,
  MatSlideToggleChange,
} from "@angular/material/slide-toggle";
import {
  ActivatedRoute,
  Params,
} from "@angular/router";
import {HomeAction} from "@app/features/home/stores/home.action";
import {HomeState} from "@app/features/home/stores/home.state";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonDepositState} from "@app/stores/person/deposit/app-person-deposit.state";
import {AppPinboardAction} from "@app/stores/pinboard/app-pinboard.action";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {AppStoredSearchesAction} from "@app/stores/stored-searches/app-stored-searches.action";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeStoredSearchSaveDialog} from "@home/components/dialogs/home-stored-search-save/home-stored-search-save.dialog";
import {SearchContextEnum} from "@home/enums/search-context.enum";
import {ViewModeTableEnum} from "@home/enums/view-mode-table.enum";
import {SearchHelper} from "@home/helpers/search.helper";
import {PortalSearch} from "@home/models/published-deposit-search.model";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {
  AdvancedSearchCriteria,
  StoredSearch,
  StoredSearchCriteria,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  ButtonColorEnum,
  ClipboardUtil,
  ConfirmDialog,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DateUtil,
  DialogUtil,
  Facet,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  LoginMode,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  OAuth2Service,
  OrderEnum,
  Paging,
  QueryParameters,
  QueryParametersUtil,
  ScrollService,
  StoreUtil,
  UrlQueryParamHelper,
  UrlUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-search-routable",
  templateUrl: "./home-search.routable.html",
  styleUrls: ["./home-search.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeSearchRoutable extends SharedAbstractPresentational implements OnInit, OnDestroy {
  searchObs: Observable<string> = MemoizedUtil.select(this._store, HomeState, state => state.search);
  searchWithFulltextObs: Observable<boolean> = MemoizedUtil.select(this._store, HomeState, state => state.searchWithFulltext);
  withRestrictedAccessMastersObs: Observable<boolean> = MemoizedUtil.select(this._store, HomeState, state => state.withRestrictedAccessMasters);
  facetsSelectedObs: Observable<MappingObject<Enums.Facet.Name, string[]>> = MemoizedUtil.select(this._store, HomeState, state => state.facetsSelected);
  advancedSearchObs: Observable<MappingObject<string, string[]>> = MemoizedUtil.select(this._store, HomeState, state => state.advancedSearch);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, HomeState, state => state.queryParameters);
  viewModeTableEnumObs: Observable<ViewModeTableEnum> = MemoizedUtil.select(this._store, HomeState, state => state.viewModeTableEnum);
  totalObs: Observable<number> = MemoizedUtil.select(this._store, HomeState, state => state.total);
  listObs: Observable<PublishedDeposit[]> = MemoizedUtil.select(this._store, HomeState, state => state.list);
  facetsObs: Observable<Facet[]> = MemoizedUtil.select(this._store, HomeState, state => state.facets);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  isInTourModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isInTourMode);
  isLoggedInObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn);
  listAdvancedSearchCriteriaObs: Observable<AdvancedSearchCriteria[]> = MemoizedUtil.select(this._store, AppSearchState, state => state.listAdvancedSearchCriteria);
  isFacetClosed: boolean = true;
  isStoredSearchUpdated: boolean = undefined;

  get viewModeTableEnum(): typeof ViewModeTableEnum {
    return ViewModeTableEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get mappingObjectUtil(): typeof MappingObjectUtil {
    return MappingObjectUtil;
  }

  @ViewChild("withRestrictedAccessMastersToggle")
  withRestrictedAccessMastersToggle: MatSlideToggle;

  columns: DataTableColumns<PublishedDeposit>[] = [];

  actions: DataTableActions<PublishedDeposit>[] = [
    {
      logo: IconNameEnum.addToPinboard,
      callback: current => this._addToPinboard(current),
      placeholder: current => LabelTranslateEnum.addToPinboard,
      displayOnCondition: current => MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn) && !this._isPinned(current.resId),
    },
    {
      logo: IconNameEnum.removeToPinboard,
      callback: current => this._removeToPinboard(current),
      placeholder: current => LabelTranslateEnum.removeToPinboard,
      displayOnCondition: current => MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn) && this._isPinned(current.resId),
    },
  ];

  private _isPinned(publishedDepositId: string): boolean {
    const list = MemoizedUtil.selectedSnapshot(this._store, AppPersonDepositState);
    if (isNullOrUndefinedOrEmptyArray(list)) {
      return false;
    }
    return list.findIndex(d => d.resId === publishedDepositId) !== -1;
  }

  constructor(private readonly _store: Store,
              private readonly _translate: TranslateService,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _oauthService: OAuth2Service,
              protected readonly _dialog: MatDialog,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService,
              private readonly _scrollService: ScrollService) {
    super();
  }

  searchId: string | undefined;
  searchStored: StoredSearch | undefined;

  get isAlreadySavedSearch(): boolean {
    return isNotNullNorUndefinedNorWhiteString(this.searchId);
  }

  get isStoredSearchModified(): boolean {
    if (isNotNullNorUndefined(this.isStoredSearchUpdated)) {
      return this.isStoredSearchUpdated;
    }
    return false;
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this._observeSearchQueryParam());
    this._defineColumns();

    const mapQueryParams = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
    if (MappingObjectUtil.has(mapQueryParams, SearchHelper.SEARCH_ID_QUERY_PARAM) &&
      MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn)) {
      const searchId = MappingObjectUtil.get(mapQueryParams, SearchHelper.SEARCH_ID_QUERY_PARAM);
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new AppStoredSearchesAction.GetById(searchId),
        AppStoredSearchesAction.GetByIdSuccess,
        result => {
          this.searchStored = result.model;
          const currentPerson = MemoizedUtil.currentSnapshot(this._store, AppPersonState);
          if (this.searchStored.creator.resId === currentPerson.resId) {
            this.searchId = this.searchStored.resId;
          }
        }));
    }
  }

  private _observeSearchQueryParam(): Observable<Params> {
    return this._route.queryParams.pipe(tap(params => {
      const searchInfos = SearchHelper.adaptUrlStringToPortalSearch(params);
      const queryParameters = QueryParametersUtil.clone(MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.queryParameters));
      const pageSize = QueryParametersUtil.getPageSize(queryParameters);
      const isQueryParamSettingForHomePage = pageSize === environment.defaultPageSizeHomePage;
      if (isQueryParamSettingForHomePage) {
        queryParameters.paging.pageSize = environment.defaultPageSizeSearchPage;
        queryParameters.sort = {
          field: environment.defaultPageSortSearchPage,
          order: OrderEnum.descending,
        };
      }
      this._store.dispatch(new HomeAction.Search(false, searchInfos, queryParameters, SearchContextEnum.LIST));
      this.isStoredSearchUpdated = !isNullOrUndefined(this.isStoredSearchUpdated);
    }));
  }

  searchChange(search: string): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.search = search;
    this._search(portalSearch);
  }

  searchFulltextChange(event: MatSlideToggleChange): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.searchWithFulltext = event.checked;
    this._search(portalSearch);
  }

  withRestrictedAccessMastersChange(event: MatSlideToggleChange): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.withRestrictedAccessMasters = event.checked;
    this._search(portalSearch);
  }

  facetChange(facetsSelected: MappingObject<Enums.Facet.Name, string[]>): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.facetsSelected = facetsSelected;
    this._scrollService.scrollToTop();
    this._search(portalSearch);
  }

  advancedSearch(advancedSearch: MappingObject<string, string[]>): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.advancedSearch = advancedSearch;
    this._search(portalSearch);
  }

  viewModeChange(viewMode: ViewModeTableEnum): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.viewTabMode = viewMode;
    this._search(portalSearch);
  }

  private _search(portalSearch: PortalSearch): void {
    portalSearch.searchId = this.searchId;
    this._store.dispatch(SearchHelper.generateSearchPageNavigateAction(portalSearch));
  }

  onQueryParametersEvent($event: QueryParameters): void {
    this._store.dispatch(new HomeAction.ChangeQueryParameters($event));
  }

  showDetail(publishedDeposit: PublishedDeposit): void {
    this._store.dispatch(new HomeAction.NavigateFromListToDetail(publishedDeposit.archiveId));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  private _addToPinboard(publishedDeposit: PublishedDeposit): void {
    this._store.dispatch(new AppPinboardAction.Add(publishedDeposit.resId));
  }

  private _removeToPinboard(publishedDeposit: PublishedDeposit): void {
    this._store.dispatch(new AppPinboardAction.Remove(publishedDeposit.resId));
  }

  navigate($event: RoutesEnum): void {
    this._store.dispatch(new Navigate([$event]));
  }

  pageChange(paging: Paging): void {
    const queryParameters = QueryParametersUtil.clone(MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.queryParameters));
    queryParameters.paging = paging;
    this._store.dispatch(new HomeAction.ChangeQueryParameters(queryParameters));
  }

  saveSearch(): void {
    this.subscribe(DialogUtil.open(this._dialog, HomeStoredSearchSaveDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("home.search.dialog.saveSearch.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("home.search.dialog.saveSearch.message"),
      inputLabelToTranslate: LabelTranslateEnum.searchName,
      inputInitialValue: this._translate.instant("home.search.dialog.saveSearch.defaultName", {date: DateUtil.convertToSwissDateTimeLongString(new Date())}),
      checkboxLabel: MARK_AS_TRANSLATABLE("home.search.dialog.saveSearch.usedForBibliography"),
      checkboxValue: undefined,
      confirmButtonToTranslate: LabelTranslateEnum.save,
      cancelButtonToTranslate: LabelTranslateEnum.close,
      colorConfirm: ButtonColorEnum.primary,
      colorCancel: ButtonColorEnum.primary,
    }, undefined, (stored: StoredSearch) => {
      this.searchId = undefined;

      const action = new AppStoredSearchesAction.Create({
        model: {
          name: stored.name,
          criteria: this._generateStoredSearchCriteriaForSearchCreateOrUpdate(),
          withRestrictedAccessMasters: this.withRestrictedAccessMastersToggle.checked,
          usedForBibliography: stored.usedForBibliography,
        },
      });
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action, AppStoredSearchesAction.CreateSuccess,
        result => {
          this.searchId = result.model.resId;
          this.isStoredSearchUpdated = false;
          this._changeDetector.detectChanges();
          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new AppStoredSearchesAction.GetById(this.searchId),
            AppStoredSearchesAction.GetByIdSuccess,
            res => {
              this.searchStored = res.model;
            }));
        }));
    }));

  }

  updateSearch(): void {
    if (isNotNullNorUndefined(this.searchStored.usedForBibliography) && this.searchStored.usedForBibliography) {
      this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
        titleToTranslate: MARK_AS_TRANSLATABLE("pinboard.search.dialog.update.title"),
        messageToTranslate: MARK_AS_TRANSLATABLE("pinboard.search.dialog.update.message"),
        confirmButtonToTranslate: LabelTranslateEnum.save,
        cancelButtonToTranslate: LabelTranslateEnum.close,
        colorConfirm: ButtonColorEnum.primary,
        colorCancel: ButtonColorEnum.primary,
      }, undefined, (title: string) => {
        this.dispatchUpdateSearch();
      }));
    } else {
      this.dispatchUpdateSearch();
    }
  }

  dispatchUpdateSearch(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new AppStoredSearchesAction.Update({
      model: {
        resId: this.searchId,
        criteria: this._generateStoredSearchCriteriaForSearchCreateOrUpdate(),
        withRestrictedAccessMasters: this.withRestrictedAccessMastersToggle.checked,
      },
    }), AppStoredSearchesAction.UpdateSuccess, () => {
      this.isStoredSearchUpdated = false;
      this._changeDetector.detectChanges();
    }));
  }

  navigateToBibliography(): void {
    this._store.dispatch(new AppStoredSearchesAction.Update({
      model: {
        resId: this.searchId,
        usedForBibliography: true,
      },
    }));
    const queryParam = {} as MappingObject<string, string>;
    MappingObjectUtil.set(queryParam, SearchHelper.ADVANCED_SEARCH_QUERY_PARAM, this.searchId);
    this._navigateOrLogin(RoutesEnum.homeBibliography, queryParam);
  }

  private _navigateOrLogin(target: string, queryParams?: Params | undefined): void {
    const isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isLogged) {
      this._navigate(target, queryParams);
    } else {
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, target);
    }
  }

  private _navigate($event: RoutesEnum, queryParams?: Params | undefined): void {
    this._store.dispatch(new Navigate([$event], queryParams));
  }

  private _generateStoredSearchCriteriaForSearchCreateOrUpdate(): StoredSearchCriteria[] {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const searchWithFulltext = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchWithFulltext);
    const withRestrictedAccessMasters = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.withRestrictedAccessMasters);
    const advancedSearch = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.advancedSearch);
    const listAdvancedSearchCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.listAdvancedSearchCriteria);
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    return SearchHelper.adaptPortalSearchToStoredSearchCriteriaList({
      search: search,
      searchWithFulltext: searchWithFulltext,
      withRestrictedAccessMasters: withRestrictedAccessMasters,
      advancedSearch: advancedSearch,
      facetsSelected: facetsSelected,
    }, listAdvancedSearchCriteria);
  }

  share(): void {
    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    portalSearch.searchId = undefined;
    const navigateAction = SearchHelper.generateSearchPageNavigateAction(portalSearch);
    const url = UrlUtil.convertNavigateActionToUrl(navigateAction, environment);
    ClipboardUtil.copyStringToClipboard(url);
    this._notificationService.showInformation(LabelTranslateEnum.searchUrlToShareCopiedToClipboard);
  }

  private _defineColumns(): void {
    const queryParameters = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.queryParameters);
    this.columns = [
      {
        field: "title",
        header: LabelTranslateEnum.title,
        type: DataTableFieldTypeEnum.innerHtml,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        width: "50%",
      },
      {
        field: "contributors",
        header: LabelTranslateEnum.contributors,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.contributors),
      },
      {
        field: environment.defaultPageSortSearchPage as any,
        header: LabelTranslateEnum.year,
        type: DataTableFieldTypeEnum.string,
        order: queryParameters.sort?.field === environment.defaultPageSortSearchPage ? queryParameters.sort?.order : OrderEnum.descending,
        isSortable: true,
        isFilterable: false,
        width: "50px",
      },
      {
        field: "container.shortTitle" as any,
        header: LabelTranslateEnum.publishedIn,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        width: "250px",
      },
    ];
  }

}
