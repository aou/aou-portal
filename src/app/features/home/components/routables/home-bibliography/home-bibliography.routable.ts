/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-bibliography.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  TrackByFunction,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {DomSanitizer} from "@angular/platform-browser";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {AppState} from "@app/stores/app.state";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonDepositAction} from "@app/stores/person/deposit/app-person-deposit.action";
import {AppStoredSearchesAction} from "@app/stores/stored-searches/app-stored-searches.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SearchHelper} from "@home/helpers/search.helper";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {HomeAction} from "@home/stores/home.action";
import {HomeState} from "@home/stores/home.state";
import {
  BibliographyFormat,
  Contributor,
  Deposit,
  StoredSearch,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {SharedContributorAction} from "@shared/stores/contributor/shared-contributor.action";
import {SharedContributorModeEnum} from "@shared/stores/contributor/shared-contributor.state";
import {
  distinctUntilChanged,
  Observable,
} from "rxjs";
import {
  debounceTime,
  tap,
} from "rxjs/operators";
import {
  BaseFormDefinition,
  BreakpointService,
  ClipboardUtil,
  EnumUtil,
  FormValidationHelper,
  Guid,
  isEmptyArray,
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  isNumberReal,
  KeyValue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  PropertyName,
  QueryParameters,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
  StoreUtil,
  StringUtil,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-home-bibliography-routable",
  templateUrl: "./home-bibliography.routable.html",
  styleUrls: ["./home-bibliography.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeBibliographyRoutable extends SharedAbstractPresentational implements OnInit, OnDestroy {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  private readonly _DEFAULT_FORMAT: string = environment.defaultBibliographyFormat;
  readonly _LIST_BOOLEAN_TO_HIDE_WHEN_FALSE: string[] = [this.formDefinition.linkOnAllText, this.formDefinition.withAbstract, this.formDefinition.withThumbnail, this.formDefinition.withAccessLevel];
  readonly LIMIT_MIN_VALUE: number = 1;

  trackByFnAdvancedSearch: TrackByFunction<StoredSearch> = (index: number, item: StoredSearch) => item.resId;
  trackByFnPinned: TrackByFunction<StoredSearch> = (index: number, item: Deposit) => item.resId;

  listObs: Observable<PublishedDeposit[]> = MemoizedUtil.select(this._store, HomeState, state => state.list);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  listBibliographyFormatObs: Observable<BibliographyFormat[]> = MemoizedUtil.select(this._store, HomeState, state => state.listBibliographyFormat);
  listLanguages: Enums.Language.LanguageEnum[] = [
    Enums.Language.LanguageEnum.fr,
    Enums.Language.LanguageEnum.en,
    Enums.Language.LanguageEnum.de,
    Enums.Language.LanguageEnum.es,
  ];

  divUid: string = StringUtil.replaceAll(Guid.MakeNew().guid.toLowerCase(), "-", "");
  divPrefix: string = "bibliography_container_";
  isBibliographyLoading: boolean = false;

  @ViewChild("divScriptInject")
  divScriptInject: ElementRef;

  private _scriptElementRef: ElementRef;
  scriptCode: string;

  listIdPinned: string[] = [];
  listPinned: Deposit[] = [];
  listPinnedUrl: string[] = [];
  listIdAdvancedSearch: string[] = [];
  listAdvancedSearch: StoredSearch[] = [];
  listAdvancedSearchNavigate: Navigate[] = [];
  contributorCnIndividu: string = undefined;

  form: FormGroup;

  contributor: Contributor;

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get anteriorityEnum(): typeof AnteriorityEnum {
    return AnteriorityEnum;
  }

  get sortTypeEnumTranslate(): typeof SortTypeEnumTranslate {
    return SortTypeEnumTranslate;
  }

  get groupEnumTranslate(): typeof GroupEnumTranslate {
    return GroupEnumTranslate;
  }

  get currentPersonId(): string {
    return MemoizedUtil.selectSnapshot(this._store, AppPersonState, state => state.current?.resId);
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get groupEnum(): typeof GroupEnum {
    return GroupEnum;
  }

  get languageEnumTranslate(): typeof Enums.Language.LanguageEnumTranslate {
    return Enums.Language.LanguageEnumTranslate;
  }

  get bibliographyLoading(): boolean {
    return this.isBibliographyLoading;
  }

  get sortTypeEnum(): typeof SortTypeEnum {
    return SortTypeEnum;
  }

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _renderer2: Renderer2,
              private readonly _elementRef: ElementRef,
              private readonly _notificationService: NotificationService,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService,
              private readonly _domSanitizer: DomSanitizer,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    const listBibliographyFormats = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.listBibliographyFormat);
    if (isNullOrUndefinedOrEmptyArray(listBibliographyFormats)) {
      this._store.dispatch(new HomeAction.GetAllBibliographyFormats());
    }

    this._extractUrlParams();

    if (isEmptyArray(this.listIdAdvancedSearch) && isEmptyArray(this.listIdPinned) && isNullOrUndefined(this.contributorCnIndividu)) {
      this.backToPinboard();
      return;
    }

    this._retrieveStoredSearch();
    this._retrievePinnedDeposit();
    this._retrieveContributor();

    this._store.dispatch(new HomeAction.CombineStoredSearches(this.listIdAdvancedSearch, this.listIdPinned, this.contributorCnIndividu));

    this._initForm();
    this._initScript();
  }

  private _initScript(): void {
    this._updateScript(false);
    this.subscribe(this.form.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(500),
      tap(formValue => {
        this._updateScript(true);
      })));
  }

  private _initForm(): void {
    const currentLanguage = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage);
    this.form = this._fb.group({
      [this.formDefinition.uid]: [this.divUid, [SolidifyValidator]],
      [this.formDefinition.searches]: [isNonEmptyArray(this.listIdAdvancedSearch) ? this.listIdAdvancedSearch.join(SOLIDIFY_CONSTANTS.COMMA) : undefined, [SolidifyValidator]],
      [this.formDefinition.publicationIds]: [isNonEmptyArray(this.listIdPinned) ? this.listIdPinned.join(SOLIDIFY_CONSTANTS.COMMA) : undefined, [SolidifyValidator]],
      [this.formDefinition.contributor]: [this.contributorCnIndividu, [SolidifyValidator]],
      [this.formDefinition.lang]: [currentLanguage, [SolidifyValidator]],
      [this.formDefinition.fromYear]: [undefined, [SolidifyValidator]],
      [this.formDefinition.toYear]: [undefined, [SolidifyValidator]],
      [this.formDefinition.sinceYear]: [undefined, [SolidifyValidator]],
      [this.formDefinition.years]: [undefined, [SolidifyValidator]],
      [this.formDefinition.limit]: [1000, [SolidifyValidator, Validators.min(this.LIMIT_MIN_VALUE)]],
      [this.formDefinition.format]: [this._DEFAULT_FORMAT, [SolidifyValidator]],
      [this.formDefinition.sort]: [SortTypeEnum.YEAR, [SolidifyValidator]],
      [this.formDefinition.group]: [GroupEnum.DOCUMENT_TYPE, [SolidifyValidator]],
      [this.formDefinition.groupBySubtype]: [false, [SolidifyValidator]],
      [this.formDefinition.anteriority]: [AnteriorityEnum.all, [SolidifyValidator]],
      [this.formDefinition.linkOnAllText]: [false, [SolidifyValidator]],
      [this.formDefinition.linkTargetBlank]: [false, [SolidifyValidator]],
      [this.formDefinition.withAbstract]: [false, [SolidifyValidator]],
      [this.formDefinition.withThumbnail]: [false, [SolidifyValidator]],
      [this.formDefinition.withAccessLevel]: [false, [SolidifyValidator]],
    });
  }

  private _retrieveStoredSearch(): void {
    if (isEmptyArray(this.listIdAdvancedSearch)) {
      return;
    }
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AppStoredSearchesAction.GetAll(new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo)),
      AppStoredSearchesAction.GetAllSuccess,
      result => {
        const list = result.list._data;
        this.listIdAdvancedSearch.forEach(id => {
          const storedSearch = list.find(s => s.resId === id);
          if (isNotNullNorUndefined(storedSearch)) {
            this.listAdvancedSearch.push(storedSearch);
            this.listAdvancedSearchNavigate.push(this._computeSearchNavigate(storedSearch));
          }
        });
        this._changeDetector.detectChanges();
      }));
  }

  private _retrievePinnedDeposit(): void {
    if (isEmptyArray(this.listIdPinned)) {
      return;
    }
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AppPersonDepositAction.GetAll(this.currentPersonId, new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo)),
      AppPersonDepositAction.GetAllSuccess,
      result => {
        const list = result.list._data;
        this.listIdPinned.forEach(id => {
          const deposit = list.find(r => r.resId === id);
          if (isNotNullNorUndefined(deposit)) {
            this.listPinned.push(deposit);
            this.listPinnedUrl.push(this._computePublicationUrl(deposit));
          }
        });
        this._changeDetector.detectChanges();
      }));
  }

  private _retrieveContributor(): void {
    if (isNullOrUndefined(this.contributorCnIndividu)) {
      return;
    }
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedContributorAction.GetById(this.contributorCnIndividu, false, false, SharedContributorModeEnum.CN_INDIVIDU),
      SharedContributorAction.GetByIdSuccess,
      result => {
        this.contributor = result.model;
        this._changeDetector.detectChanges();
      }));
  }

  private _extractUrlParams(): void {
    this.listIdAdvancedSearch = [];

    const map = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
    if (MappingObjectUtil.has(map, SearchHelper.ADVANCED_SEARCH_QUERY_PARAM)) {
      this.listIdAdvancedSearch = MappingObjectUtil.get(map, SearchHelper.ADVANCED_SEARCH_QUERY_PARAM).split(SearchHelper.VALUES_SEPARATOR);
    }

    if (MappingObjectUtil.has(map, SearchHelper.PINNED_QUERY_PARAM)) {
      this.listIdPinned = MappingObjectUtil.get(map, SearchHelper.PINNED_QUERY_PARAM).split(SearchHelper.VALUES_SEPARATOR);
      if (isNullOrUndefined(this.listIdPinned)) {
        this.listIdPinned = [];
      }
    }

    if (MappingObjectUtil.has(map, SearchHelper.CONTRIBUTOR_QUERY_PARAM)) {
      this.contributorCnIndividu = MappingObjectUtil.get(map, SearchHelper.CONTRIBUTOR_QUERY_PARAM);
    }
  }

  backToPinboard(): void {
    this._store.dispatch(new Navigate([RoutesEnum.homePinboard]));
  }

  private get urlScript(): string {
    if (isNotNullNorUndefinedNorWhiteString(environment.bibliographyProxyUrl)) {
      return environment.bibliographyProxyUrl;
    }
    return ApiEnum.accessBibliography + urlSeparator + ApiActionNameEnum.JAVASCRIPT;
  }

  private _updateScript(scriptChange: boolean): void {
    const search = {} as MappingObject<string, string>;

    const formValue = this.form.value as MappingObject<string>;
    this._manageGroup(formValue);
    this._manageAnteriority(formValue);

    MappingObjectUtil.forEach(formValue, (value, key) => {
      if (isNullOrUndefinedOrWhiteString(value) ||
        (isFalse(value) && this._LIST_BOOLEAN_TO_HIDE_WHEN_FALSE.includes(key))) {
        return;
      }
      MappingObjectUtil.set(search, key, value);
    });

    if (scriptChange) {
      this.divUid = StringUtil.replaceAll(Guid.MakeNew().guid.toLowerCase(), "-", "");
      MappingObjectUtil.set(search, "uid", this.divUid);
    }

    const searchParam = UrlQueryParamHelper.getQueryParamStringFromQueryParamMappingObject(search, false);
    const url = this.urlScript + searchParam;
    const type = "text/javascript";
    this.scriptCode = `<script type="${type}" src="${url}"></script>\n<div id="${this.divPrefix}${this.divUid}"></div>`;

    if (isNotNullNorUndefined(this.divScriptInject)) {
      this.divScriptInject.nativeElement.innerText = "";
      this.isBibliographyLoading = true;
    }

    const action = new HomeAction.GetBibliographyJavascript(url);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
      HomeAction.GetBibliographyJavascriptSuccess,
      result => {
        if (isNotNullNorUndefined(this._scriptElementRef)) {
          this._renderer2.removeChild(this._elementRef.nativeElement, this._scriptElementRef);
        }
        this._scriptElementRef = this._renderer2.createElement("script");
        this._renderer2.setProperty(this._scriptElementRef, "text", result.javascript + "load_bibliography_" + this.divUid + "();");
        this._renderer2.setAttribute(this._scriptElementRef, "type", type);
        this._renderer2.appendChild(this._elementRef.nativeElement, this._scriptElementRef);

        this.isBibliographyLoading = false;
        this._changeDetector.detectChanges();
      }));
  }

  private _manageAnteriority(formValue: MappingObject<string>): void {
    const anteriorityValue = MappingObjectUtil.get(formValue, this.formDefinition.anteriority) as AnteriorityEnum;
    const fromYear = +MappingObjectUtil.get(formValue, this.formDefinition.fromYear);
    const toYear = +MappingObjectUtil.get(formValue, this.formDefinition.toYear);
    const sinceYear = +MappingObjectUtil.get(formValue, this.formDefinition.sinceYear);
    const years = +MappingObjectUtil.get(formValue, this.formDefinition.years);
    MappingObjectUtil.delete(formValue, this.formDefinition.anteriority);
    MappingObjectUtil.delete(formValue, this.formDefinition.fromYear);
    MappingObjectUtil.delete(formValue, this.formDefinition.toYear);
    MappingObjectUtil.delete(formValue, this.formDefinition.sinceYear);
    MappingObjectUtil.delete(formValue, this.formDefinition.years);

    switch (anteriorityValue) {
      case AnteriorityEnum.between:
        if (isNumberReal(fromYear) && fromYear > 0) {
          MappingObjectUtil.set(formValue, this.formDefinition.fromYear, fromYear);
        }
        if (isNumberReal(toYear) && toYear > 0) {
          MappingObjectUtil.set(formValue, this.formDefinition.toYear, toYear);
        }
        break;
      case AnteriorityEnum.since:
        if (isNumberReal(sinceYear) && sinceYear > 0) {
          MappingObjectUtil.set(formValue, this.formDefinition.sinceYear, sinceYear);
        }
        break;
      case AnteriorityEnum.lastYear:
        if (isNumberReal(years) && years > 0) {
          MappingObjectUtil.set(formValue, this.formDefinition.years, years);
        }
        break;
    }
  }

  private _manageGroup(formValue: MappingObject<string>): void {
    const group = MappingObjectUtil.get(formValue, this.formDefinition.group) as GroupEnum;
    const sort = MappingObjectUtil.get(formValue, this.formDefinition.sort) as SortTypeEnum;
    MappingObjectUtil.delete(formValue, this.formDefinition.group);
    if (sort === SortTypeEnum.VALIDATION_DATE) {
      MappingObjectUtil.delete(formValue, this.formDefinition.groupBySubtype);
      return;
    }
    switch (group) {
      case GroupEnum.DOCUMENT_TYPE:
        MappingObjectUtil.set(formValue, this.formDefinition.groupByType, true);
        MappingObjectUtil.set(formValue, this.formDefinition.groupByYear, false);
        break;
      case GroupEnum.YEAR:
        MappingObjectUtil.set(formValue, this.formDefinition.groupByType, false);
        MappingObjectUtil.set(formValue, this.formDefinition.groupByYear, true);
        MappingObjectUtil.delete(formValue, this.formDefinition.groupBySubtype);
        break;
      case GroupEnum.YEAR_AND_DOCUMENT_TYPE:
        MappingObjectUtil.set(formValue, this.formDefinition.groupByType, true);
        MappingObjectUtil.set(formValue, this.formDefinition.groupByYear, true);
        break;
    }
  }

  copyToClipboard(): void {
    ClipboardUtil.copyStringToClipboard(this.scriptCode);
    this._notificationService.showInformation(LabelTranslateEnum.bibliographyScriptCopiedToClipboard);
  }

  selectAnteriorityIfNot(anteriorityEnum: AnteriorityEnum): void {
    const anteriorityFc = this.form.get(this.formDefinition.anteriority);
    if (anteriorityFc.value !== anteriorityEnum) {
      anteriorityFc.setValue(anteriorityEnum);
    }
  }

  manageGroupBySubtypeValue(): void {
    const groupByTypeFc = this.form.get(this.formDefinition.group);
    const groupBySubtypeFc = this.form.get(this.formDefinition.groupBySubtype);
    if (groupByTypeFc.value !== GroupEnum.DOCUMENT_TYPE && groupByTypeFc.value !== GroupEnum.YEAR_AND_DOCUMENT_TYPE) {
      groupBySubtypeFc.setValue(false);
    }
  }

  private _computePublicationUrl(deposit: Deposit): string {
    return RoutesEnum.homeDetail + urlSeparator + deposit.archiveId;
  }

  private _computeSearchNavigate(search: StoredSearch): Navigate {
    const portalSearch = SearchHelper.adaptSearchToPortalSearch(search);
    portalSearch.searchId = search.resId;
    return SearchHelper.generateSearchPageNavigateAction(portalSearch);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() uid: string;
  @PropertyName() searches: string;
  @PropertyName() publicationIds: string;
  @PropertyName() contributor: string;
  @PropertyName() lang: string;
  @PropertyName() fromYear: string;
  @PropertyName() toYear: string;
  @PropertyName() sinceYear: string;
  @PropertyName() years: string;
  @PropertyName() limit: string;
  @PropertyName() format: string;
  @PropertyName() sort: string;
  @PropertyName() group: string;
  @PropertyName() groupByType: string;
  @PropertyName() groupBySubtype: string;
  @PropertyName() groupByYear: string;
  @PropertyName() anteriority: string;
  @PropertyName() linkOnAllText: string;
  @PropertyName() linkTargetBlank: string;
  @PropertyName() withAbstract: string;
  @PropertyName() withThumbnail: string;
  @PropertyName() withAccessLevel: string;
}

export type AnteriorityEnum =
  "all"
  | "between"
  | "since"
  | "last-year";
export const AnteriorityEnum = {
  all: "all" as AnteriorityEnum,
  between: "between" as AnteriorityEnum,
  since: "since" as AnteriorityEnum,
  lastYear: "last-year" as AnteriorityEnum,
};

export type SortTypeEnum =
  "YEAR"
  | "VALIDATION_DATE"
  | "AUTHORS"
  | "TITLE";
export const SortTypeEnum = {
  YEAR: "YEAR" as SortTypeEnum,
  VALIDATION_DATE: "VALIDATION_DATE" as SortTypeEnum,
  AUTHORS: "AUTHORS" as SortTypeEnum,
  TITLE: "TITLE" as SortTypeEnum,
};
export const SortTypeEnumTranslate: KeyValue[] = [
  {
    key: SortTypeEnum.YEAR,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.sortType.year"),
  },
  {
    key: SortTypeEnum.VALIDATION_DATE,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.sortType.entryDate"),
  },
  {
    key: SortTypeEnum.AUTHORS,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.sortType.authors"),
  },
  {
    key: SortTypeEnum.TITLE,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.sortType.title"),
  },
];

export type GroupEnum =
  "DOCUMENT_TYPE"
  | "YEAR"
  | "YEAR_AND_DOCUMENT_TYPE";
export const GroupEnum = {
  DOCUMENT_TYPE: "DOCUMENT_TYPE" as GroupEnum,
  YEAR: "YEAR" as GroupEnum,
  YEAR_AND_DOCUMENT_TYPE: "YEAR_AND_DOCUMENT_TYPE" as GroupEnum,
};
export const GroupEnumTranslate: KeyValue[] = [
  {
    key: GroupEnum.DOCUMENT_TYPE,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.groupEnum.documentType"),
  },
  {
    key: GroupEnum.YEAR,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.groupEnum.year"),
  },
  {
    key: GroupEnum.YEAR_AND_DOCUMENT_TYPE,
    value: MARK_AS_TRANSLATABLE("enum.bibliography.groupEnum.yearAndDocumentType"),
  },
];
