/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-pinboard.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonDepositAction} from "@app/stores/person/deposit/app-person-deposit.action";
import {AppPersonDepositState} from "@app/stores/person/deposit/app-person-deposit.state";
import {AppStoredSearchesAction} from "@app/stores/stored-searches/app-stored-searches.action";
import {AppStoredSearchesState} from "@app/stores/stored-searches/app-stored-searches.state";
import {environment} from "@environments/environment";
import {SearchHelper} from "@home/helpers/search.helper";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {HomeAction} from "@home/stores/home.action";
import {StoredSearch} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  ButtonColorEnum,
  ClipboardUtil,
  ConfirmDialog,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  OrderEnum,
  QueryParameters,
  StoreUtil,
  Tab,
  TabsContainer,
  UrlUtil,
} from "solidify-frontend";
import {HomeStoredSearchSaveDialog} from "@home/components/dialogs/home-stored-search-save/home-stored-search-save.dialog";

@Component({
  selector: "aou-home-pinboard-routable",
  templateUrl: "./home-pinboard.routable.html",
  styleUrls: ["./home-pinboard.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePinboardRoutable extends SharedAbstractPresentational implements OnInit, OnDestroy {
  @ViewChild("tabsContainer")
  tabsContainer: TabsContainer;

  isLoadingStoredSearchesObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppStoredSearchesState);
  isLoadingPinObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppPersonDepositState);

  queryParameters: QueryParameters = new QueryParameters();

  listPinnedObs: Observable<PublishedDeposit[]> = MemoizedUtil.selected(this._store, AppPersonDepositState);
  listStoredSearchObs: Observable<StoredSearch[]> = MemoizedUtil.list(this._store, AppStoredSearchesState);

  columns: DataTableColumns<PublishedDeposit>[] = [
    {
      field: "title",
      header: LabelTranslateEnum.title,
      type: DataTableFieldTypeEnum.innerHtml,
      order: OrderEnum.none,
      isSortable: false,
      isFilterable: false,
    },
  ];

  actions: DataTableActions<PublishedDeposit>[] = [
    {
      logo: IconNameEnum.removeToPinboard,
      callback: (deposit: PublishedDeposit) => this._removePinnedElement(deposit),
      placeholder: current => LabelTranslateEnum.removeToPinboard,
      isWrapped: false,
    },
  ];

  listPinnedSelected: PublishedDeposit[] = [];
  listStoredSearchSelected: StoredSearch[] = [];

  get numberSelectedElements(): number {
    return (this.listPinnedSelected?.length ?? 0) + (this.listStoredSearchSelected?.length ?? 0);
  }

  suffixUrlMatcher: (route: ActivatedRoute) => string = (route: ActivatedRoute) => route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);

  get pinboardListTabEnum(): typeof PinboardListTabEnum {
    return PinboardListTabEnum;
  }

  get currentPersonId(): string {
    return MemoizedUtil.selectSnapshot(this._store, AppPersonState, state => state.current?.resId);
  }

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _dialog: MatDialog,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._store.dispatch(new AppStoredSearchesAction.GetAll(new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo)));
    if (isNotNullNorUndefined(this.currentPersonId)) {
      this._store.dispatch(new AppPersonDepositAction.GetAll(this.currentPersonId, new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo)));
    }
  }

  trackByFn(index: number, item: any): string {
    return item.title;
  }

  showDetail(deposit: PublishedDeposit): void {
    this._store.dispatch([
      new HomeAction.CleanSearch(),
      new Navigate([RoutesEnum.homeDetail, deposit.archiveId]),
    ]);
  }

  onQueryParametersEvent($event: QueryParameters): void {
  }

  navigateToSearchPage(search: StoredSearch): void {
    const portalSearch = SearchHelper.adaptSearchToPortalSearch(search);
    portalSearch.searchId = search.resId;
    this._store.dispatch(SearchHelper.generateSearchPageNavigateAction(portalSearch));
  }

  share(search: StoredSearch, $event: MouseEvent): void {
    if (isNotNullNorUndefined($event)) {
      this.preventClick($event);
    }

    const portalSearch = SearchHelper.adaptSearchToPortalSearch(search);
    const navigateAction = SearchHelper.generateSearchPageNavigateAction(portalSearch);
    const url = UrlUtil.convertNavigateActionToUrl(navigateAction, environment);
    ClipboardUtil.copyStringToClipboard(url);
    this._notificationService.showInformation(LabelTranslateEnum.searchUrlToShareCopiedToClipboard);
  }

  preventClick($event: MouseEvent): void {
    $event.stopPropagation();
  }

  editSearchName(storedSearch: StoredSearch, $event?: MouseEvent): void {
    if (isNotNullNorUndefined($event)) {
      this.preventClick($event);
    }

    this.subscribe(DialogUtil.open(this._dialog, HomeStoredSearchSaveDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("pinboard.search.dialog.edit.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("pinboard.search.dialog.edit.message"),
      inputLabelToTranslate: LabelTranslateEnum.searchName,
      inputInitialValue: storedSearch.name,
      checkboxLabel: MARK_AS_TRANSLATABLE("pinboard.search.dialog.edit.usedForBibliography"),
      checkboxValue: storedSearch.usedForBibliography,
      confirmButtonToTranslate: LabelTranslateEnum.save,
      cancelButtonToTranslate: LabelTranslateEnum.close,
      colorConfirm: ButtonColorEnum.primary,
      colorCancel: ButtonColorEnum.primary,
    }, undefined, (stored: StoredSearch) => {
      const action = new AppStoredSearchesAction.Update({
        model: {
          resId: storedSearch.resId,
          name: stored.name,
          usedForBibliography: stored.usedForBibliography,
        },
      });
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
          AppStoredSearchesAction.UpdateSuccess, success => {
            this._store.dispatch(new AppStoredSearchesAction.GetAll());
          },
          AppStoredSearchesAction.UpdateFail, fail => {
            storedSearch.name = stored.name;
            this.editSearchName(storedSearch);
          },
        ),
      );
    }));
  }

  deleteSearch(storedSearch: StoredSearch, $event?: MouseEvent): void {
    if (isNotNullNorUndefined($event)) {
      this.preventClick($event);
    }

    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("pinboard.search.dialog.delete.title"),
      messageToTranslate: storedSearch.usedForBibliography ? MARK_AS_TRANSLATABLE("pinboard.search.dialog.delete.messageWhenUsedForBibliography") : MARK_AS_TRANSLATABLE("pinboard.search.dialog.delete.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, undefined, isConfirmed => {
      this.listStoredSearchSelected = this.removeSearchFromList(storedSearch, this.listStoredSearchSelected);
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new AppStoredSearchesAction.Delete(storedSearch.resId),
        AppStoredSearchesAction.DeleteSuccess, success => {
          this._store.dispatch(new AppStoredSearchesAction.GetAll());
        }));

      this._changeDetector.detectChanges();
    }));
  }

  private removeSearchFromList(search: any, listSearch: any[]): any[] {
    const index = listSearch.findIndex(p => p === search);
    if (index !== -1) {
      const list = [...listSearch];
      list.splice(index, 1);
      return list;
    }
    return listSearch;
  }

  multiSelectionPinboard(listPinnedElements: PublishedDeposit[]): void {
    this.listPinnedSelected = [...listPinnedElements];
    this.tabsContainer?.externalDetectChanges();
  }

  selectSearch(search: any, $event: MatCheckboxChange): void {
    const index = this.listStoredSearchSelected.indexOf(search);
    if (index === -1) {
      this.listStoredSearchSelected = [...this.listStoredSearchSelected, search];
    } else {
      this.listStoredSearchSelected.splice(index, 1);
      this.listStoredSearchSelected = [...this.listStoredSearchSelected];
    }
    this.tabsContainer?.externalDetectChanges();
  }

  backToHome(): void {
    this._store.dispatch(new Navigate([RoutesEnum.homePage]));
  }

  generateBibliography(): void {
    const queryParam = {} as MappingObject<string, string>;
    if (isNonEmptyArray(this.listStoredSearchSelected)) {
      MappingObjectUtil.set(queryParam, SearchHelper.ADVANCED_SEARCH_QUERY_PARAM,
        this.listStoredSearchSelected.map(s => s.resId).join(SearchHelper.VALUES_SEPARATOR));
    }
    if (isNonEmptyArray(this.listPinnedSelected)) {
      MappingObjectUtil.set(queryParam, SearchHelper.PINNED_QUERY_PARAM,
        this.listPinnedSelected.map(s => s.resId).join(SearchHelper.VALUES_SEPARATOR));
    }
    //update the field usedForBibliography for each selected stored search if needed
    this.listStoredSearchSelected.forEach(search => {
      if (isFalse(search.usedForBibliography)) {
        this._store.dispatch(new AppStoredSearchesAction.Update({
          model: {
            resId: search.resId,
            usedForBibliography: true,
          },
        }));
      }
    });
    this._store.dispatch(new Navigate([RoutesEnum.homeBibliography], queryParam));
  }

  exportBibliography(): void {

  }

  private _removePinnedElement(deposit: PublishedDeposit): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AppPersonDepositAction.Delete(this.currentPersonId, deposit.resId),
      AppPersonDepositAction.DeleteSuccess,
      result => {
        this._store.dispatch(new AppPersonDepositAction.GetAll(this.currentPersonId));
      }));
    this.listPinnedSelected = this.removePinDepositFromList(deposit, this.listPinnedSelected);
    this.tabsContainer?.externalDetectChanges();
  }

  private removePinDepositFromList(deposit: PublishedDeposit, originalList: PublishedDeposit[]): PublishedDeposit[] {
    const index = originalList.findIndex(p => p.resId === deposit.resId);
    if (index !== -1) {
      const list = [...originalList];
      list.splice(index, 1);
      return list;
    }
    return originalList;
  }

  listTabs: Tab[] = [
    {
      id: PinboardListTabEnum.storedSearches,
      suffixUrl: PinboardListTabEnum.storedSearches,
      titleToTranslate: LabelTranslateEnum.savedSearch,
      route: () => [...this.rootUrl, PinboardListTabEnum.storedSearches],
      numberNew: () => this.listStoredSearchSelected.length,
      icon: IconNameEnum.search,
    },
    {
      id: PinboardListTabEnum.pinnedPublications,
      suffixUrl: PinboardListTabEnum.pinnedPublications,
      titleToTranslate: LabelTranslateEnum.publicationsPinned,
      route: () => [...this.rootUrl, PinboardListTabEnum.pinnedPublications],
      numberNew: () => this.listPinnedSelected.length,
      icon: IconNameEnum.pinboard,
    },
  ];

  private get rootUrl(): string[] {
    return [RoutesEnum.homePinboard];
  }
}

export type PinboardListTabEnum = "publications-pinned" | "stored-searches";
export const PinboardListTabEnum = {
  pinnedPublications: "publications-pinned" as PinboardListTabEnum,
  storedSearches: "stored-searches" as PinboardListTabEnum,
};
