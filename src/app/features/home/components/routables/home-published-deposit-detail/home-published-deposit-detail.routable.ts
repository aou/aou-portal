/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-published-deposit-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Meta} from "@angular/platform-browser";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {HomeAction} from "@app/features/home/stores/home.action";
import {HomeState} from "@app/features/home/stores/home.state";
import {
  metaInfoPublication,
  REGEXP_CITATION,
} from "@app/meta-info-list";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeAskCorrectionDialog} from "@home/components/dialogs/home-ask-correction/home-ask-correction.dialog";
import {SearchContextEnum} from "@home/enums/search-context.enum";
import {SearchHelper} from "@home/helpers/search.helper";
import {
  PublishedDeposit,
  PublishedFile,
} from "@home/models/published-deposit.model";
import {StatisticsInfo} from "@home/models/statistics-info.model";
import {HomeService} from "@home/services/home.service";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedArchiveAction} from "@shared/stores/archive/shared-archive.action";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  Citation,
  DialogUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LoginMode,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  MetaService,
  MetaUtil,
  NotificationService,
  OAuth2Service,
  QueryParameters,
  QueryParametersUtil,
  ScrollService,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-published-deposit-detail-routable",
  templateUrl: "./home-published-deposit-detail.routable.html",
  styleUrls: ["./home-published-deposit-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePublishedDepositDetailRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  isLoadingPrepareDownloadObs: Observable<boolean>;
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  currentObs: Observable<PublishedDeposit> = MemoizedUtil.select(this._store, HomeState, state => state.current);
  listObs: Observable<PublishedDeposit[]> = MemoizedUtil.select(this._store, HomeState, state => state.list);
  queryParametersDetailObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, HomeState, state => state.queryParametersDetail);
  statisticsObs: Observable<StatisticsInfo> = MemoizedUtil.select(this._store, HomeState, state => state.statistics);
  listRelativePublishedDepositObs: Observable<PublishedDeposit[]> = MemoizedUtil.select(this._store, HomeState, state => state.listRelativePublishedDeposit);
  isLoggedInObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn);
  listUserRoleEnumObs: Observable<Enums.Deposit.UserRoleEnum[]> = MemoizedUtil.select(this._store, HomeState, state => state.listUserRoleEnum);
  isUserUnigeOrHugObs: Observable<boolean> = MemoizedUtil.select(this._store, AppUserState, state => state.userAuthorizedForDepositActions);
  citationsObs: Observable<Citation[]> = MemoizedUtil.select(this._store, HomeState, state => state.citations);

  // bibliographiesObs: Observable<Citation[]> = MemoizedUtil.select(this._store, HomeState, state => state.bibliographies);

  private _resId: string;

  private _navigateWithKeys: boolean = true;

  @HostListener("window:keydown", ["$event"])
  private _handleKeyboardEvent(event: KeyboardEvent): void {
    if (this._navigateWithKeys) {
      if (event.key === "ArrowLeft") {
        this.previousPublication();
      } else if (event.key === "ArrowRight") {
        this.nextPublication();
      } else if (event.key === "Escape") {
        this.back();
      }
    }
  }

  getCurrentPageIndex(queryParameters: QueryParameters): number {
    return queryParameters.paging.length + queryParameters.paging.pageIndex;
  }

  constructor(private readonly _store: Store,
              private readonly _dialog: MatDialog,
              private readonly _actions$: Actions,
              private readonly _router: Router,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _route: ActivatedRoute,
              private readonly _scrollService: ScrollService,
              private readonly _httpClient: HttpClient,
              private readonly _notificationService: NotificationService,
              private readonly _securityService: SecurityService,
              private readonly _oauthService: OAuth2Service,
              private readonly _metaService: MetaService,
              private readonly _meta: Meta,
              private readonly _translateService: TranslateService,
              private readonly _homeService: HomeService,
  ) {
    super();
  }

  private _rewriteUrl(archiveId: string): void {
    if (SsrUtil.window?.history.replaceState && isNotNullNorUndefined(archiveId)) {
      SsrUtil.window?.history.replaceState({}, null, `${environment.baseHref}${archiveId}${SsrUtil.window.location.search}`);
    }
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._scrollService.scrollToTop();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._translateService.onLangChange.pipe(
        tap(() => {
          const archive = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
          this._metaService.setMetaFromInfo(metaInfoPublication, archive);
        }),
      ));
    }

    this.subscribe(this.currentObs.pipe(
      filter(archive => isNotNullNorUndefined(archive) && archive.resId !== this._resId),
      distinctUntilChanged((previous, current) => previous?.resId === current?.resId),
      SsrUtil.isServer ? take(1) : pipe(),
      tap(archive => {
        this._resId = archive.resId;
        this._rewriteUrl(archive.archiveId);
        this._initIsLoadingPrepareDownloadObs();
        this._metaService.setMetaFromInfo(metaInfoPublication, archive);
      }),
    ));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new HomeAction.CleanCurrent());
    MetaUtil.cleanMatchingMetaByName(this._meta, REGEXP_CITATION);
  }

  private _initIsLoadingPrepareDownloadObs(): void {
    // this.isLoadingPrepareDownloadObs = SharedArchiveState.isDownloadPreparation(this._store, this._resId);
  }

  download(file: PublishedFile): void {
    this._store.dispatch(new SharedArchiveAction.Download(file));
  }

  showPreview(file: PublishedFile): void {
    this._store.dispatch(new SharedArchiveAction.Preview(file));
  }

  back(): void {
    this._store.dispatch(SearchHelper.generateSearchPageNavigateAction(SearchHelper.generatePortalSearchFromStore(this._store)));
  }

  getMessageBackButtonToTranslate(): string {
    return MARK_AS_TRANSLATABLE("home.publishedDeposit.navigation.backToSearch");
  }

  showPublishedDeposit(archive: PublishedDeposit): void {
    this._store.dispatch([
      new HomeAction.CleanCurrent(),
      new Navigate([RoutesEnum.homeDetail, archive.archiveId]),
    ]);
  }

  navigate(navigate: Navigate): void {
    this._store.dispatch(navigate);
  }

  previousPublication(): void {
    this.searchChange(-1);
  }

  nextPublication(): void {
    this.searchChange(1);
  }

  searchChange(index: -1 | 1): void {
    let queryParametersDetail = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.queryParametersDetail);

    if (isNullOrUndefined(queryParametersDetail)) {
      return;
    }

    queryParametersDetail = QueryParametersUtil.clone(queryParametersDetail);
    queryParametersDetail.paging.pageIndex += index;

    if (queryParametersDetail.paging.pageIndex < 0 || (queryParametersDetail.paging.pageIndex + 1) > queryParametersDetail.paging.length) {
      return;
    }

    const portalSearch = SearchHelper.generatePortalSearchFromStore(this._store);
    this._store.dispatch(new HomeAction.Search(true, portalSearch, queryParametersDetail, SearchContextEnum.DETAIL));
  }

  contactContributor(publishedDeposit: PublishedDeposit): void {
    this._navigateWithKeys = false;
    this.subscribe(this._homeService.contactContributorAboutPublication(publishedDeposit.resId),
      result => {
        this._navigateWithKeys = true;
        this._changeDetector.detectChanges();
      });
  }

  login($event: PublishedDeposit): void {
    this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, $event.archiveId);
  }

  update(publishedDeposit: PublishedDeposit): void {
    if (this._securityService.isLoggedIn()) {
      this._store.dispatch(new HomeAction.IsAllowedToUpdate(publishedDeposit));
    }
  }

  sendToOrcid(publishedDeposit: PublishedDeposit): void {
    if (this._securityService.isLoggedIn()) {
      this._store.dispatch(new HomeAction.ExportToOrcid(publishedDeposit));
    }
  }

  askCorrection(publishedDeposit: PublishedDeposit): void {
    this._navigateWithKeys = false;
    this.subscribe(DialogUtil.open(this._dialog, HomeAskCorrectionDialog, {
      publishedDeposit: publishedDeposit,
    }, {
      width: "90%",
    }, success => {
      this._navigateWithKeys = true;
    }, () => {
      this._navigateWithKeys = true;
    }));
  }
}
