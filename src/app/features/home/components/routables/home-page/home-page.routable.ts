/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-page.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {SearchContextEnum} from "@home/enums/search-context.enum";
import {SearchHelper} from "@home/helpers/search.helper";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {HomeAction} from "@home/stores/home.action";
import {HomeState} from "@home/stores/home.state";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {TourEnum} from "@shared/enums/tour.enum";
import {Observable} from "rxjs";
import {
  isArray,
  isNullOrUndefinedOrWhiteString,
  LoginMode,
  MemoizedUtil,
  NotificationService,
  OAuth2Service,
  OrderEnum,
  QueryParameters,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-page-routable",
  templateUrl: "./home-page.routable.html",
  styleUrls: ["./home-page.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageRoutable extends SharedAbstractPresentational implements OnInit {
  listObs: Observable<PublishedDeposit[]> = MemoizedUtil.select(this._store, HomeState, state => state.list);
  searchValueInUrl: string = StringUtil.stringEmpty;

  publicationsNumberObs: Observable<number> = MemoizedUtil.select(this._store, HomeState, state => state.total);
  userAuthorizedForDepositActionsObs: Observable<boolean> = MemoizedUtil.select(this._store, AppUserState, state => state.userAuthorizedForDepositActions);
  isLoggedObs: Observable<boolean> = MemoizedUtil.select(this._store, environment.appState, state => state.isLoggedIn);

  trackByFnPublishedDeposit(index: number, publishedDeposit: PublishedDeposit): string {
    return publishedDeposit.resId;
  }

  constructor(private readonly _store: Store,
              private readonly _translate: TranslateService,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _oauthService: OAuth2Service,
              private readonly _notificationService: NotificationService) {
    super();
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
    const queryParameters = new QueryParameters(environment.defaultPageSizeHomePage, {
      field: environment.defaultPageSortHomePage,
      order: OrderEnum.descending,
    });
    this._store.dispatch(new HomeAction.Search(true, undefined, queryParameters, SearchContextEnum.LIST));
  }

  search(searchTerm: string): void {
    this._store.dispatch(SearchHelper.generateSearchPageNavigateAction({
      search: searchTerm,
    }));
  }

  navigateToSearch(): void {
    this._store.dispatch(SearchHelper.generateSearchPageNavigateAction({}));
  }

  navigateToPublishedDeposit(publishedDeposit: PublishedDeposit): void {
    if (isNullOrUndefinedOrWhiteString(publishedDeposit.archiveId)) {
      return null;
    }
    this._store.dispatch([
      new HomeAction.CleanSearch(),
      new Navigate([RoutesEnum.homeDetail, publishedDeposit.archiveId]),
    ]);
  }

  navigateToDepositOrLogin(): void {
    this._navigateOrLogin(RoutesEnum.deposit);
  }

  navigateToPinboardOrLogin(): void {
    this._navigateOrLogin(RoutesEnum.homePinboard);
  }

  private _navigateOrLogin(target: string): void {
    const isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isLogged) {
      this._navigate(target);
    } else {
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, target);
    }
  }

  private _navigate($event: RoutesEnum | RoutesEnum[]): void {
    this._store.dispatch(new Navigate(isArray($event) ? $event : [$event]));
  }
}
