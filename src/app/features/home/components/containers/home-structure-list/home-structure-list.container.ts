/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-structure-list.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {SearchHelper} from "@home/helpers/search.helper";
import {
  AdvancedSearchCriteria,
  Structure,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-structure-list-container",
  templateUrl: "./home-structure-list.container.html",
  styleUrls: ["./home-structure-list.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeStructureListContainer extends SharedAbstractContainer {
  @Input()
  displayTitle: boolean = true;

  private _structures: Structure[][];

  @Input()
  set structures(value: Structure[]) {
    this._structures = [];
    this.structureLinks = [];
    value.forEach(s => {
      const listArbo = this._getStructureGroupFromParentToChild(s);
      this._structures.push(listArbo);
      this.structureLinks.push(this._computeStructureLinks(listArbo));
    });
  }

  get structures(): Structure[][] {
    return this._structures;
  }

  structureLinks: Navigate[][];

  constructor(private readonly _store: Store) {
    super();
  }

  trackByFnWithResId(index: number, item: Structure): string {
    return item.resId;
  }

  private _computeStructureLinks(structureGroup: Structure[]): Navigate[] {
    const structureLinks = [];
    const structureCriteria: AdvancedSearchCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.structureCriteria);

    if (isNullOrUndefined(structureLinks) || isNullOrUndefinedOrEmptyArray(structureGroup) || isNullOrUndefined(structureCriteria)) {
      return structureLinks;
    }
    structureGroup.forEach(structure => {
      if (isNullOrUndefinedOrWhiteString(structure.resId)) {
        this.structureLinks.push(undefined);
        return;
      }
      const navigate = SearchHelper.generateSearchPageNavigateAction({
        advancedSearch: {[structureCriteria.alias]: [structure.resId]},
      });
      structureLinks.push(navigate);
    });
    return structureLinks;
  }

  private _getStructureGroupFromParentToChild(structure: Structure, structureGroup: Structure[] = []): Structure[] {
    if (isNullOrUndefined(structure)) {
      return structureGroup.reverse();
    }
    structureGroup.push(structure);
    return this._getStructureGroupFromParentToChild(structure.parentStructure, structureGroup);
  }
}
