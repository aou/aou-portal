/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-generate-bibliography-button.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {AOU_CONSTANTS} from "@app/constants";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {HomeAction} from "@home/stores/home.action";
import {StoredSearch} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  isNonEmptyArray,
  isNotNullNorUndefinedNorEmpty,
  MappingObject,
  MappingObjectUtil,
  StoreUtil,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-home-generate-bibliography-button-container",
  templateUrl: "./home-generate-bibliography-button.container.html",
  styleUrls: ["./home-generate-bibliography-button.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeGenerateBibliographyButtonContainer extends SharedAbstractPresentational {
  @Input()
  pinnedPublications: PublishedDeposit[] = [];

  @Input()
  storedSearches: StoredSearch[] = [];

  @Input()
  iconMode: boolean = false;

  get exportFormat(): typeof ExportFormat {
    return ExportFormat;
  }

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  get numberSelectedElements(): number {
    return (this.pinnedPublications?.length ?? 0) + (this.storedSearches?.length ?? 0);
  }

  exportBibliography(format: ExportFormat): void {
    const url = this._constructUrlParams(`${ApiEnum.adminPublications}/${ApiActionNameEnum.EXPORT}`, format);
    const action = new HomeAction.ExportBibliography(url);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
      HomeAction.ExportBibliography,
      result => {
        this._changeDetector.detectChanges();
      }));
  }

  private _constructUrlParams(url: string, format: ExportFormat): string {
    const queryParam = {} as MappingObject<string>;
    if (isNonEmptyArray(this.storedSearches)) {
      MappingObjectUtil.set(queryParam, AOU_CONSTANTS.SEARCHES, this.storedSearches.map(obj => obj.resId).join(","));
    }
    if (isNonEmptyArray(this.pinnedPublications)) {
      MappingObjectUtil.set(queryParam, AOU_CONSTANTS.PUBLICATION_IDS, this.pinnedPublications.map(obj => obj.resId).join(","));
    }
    if (isNotNullNorUndefinedNorEmpty(format)) {
      MappingObjectUtil.set(queryParam, AOU_CONSTANTS.FORMAT, format);
    }
    return url + UrlQueryParamHelper.getQueryParamStringFromQueryParamMappingObject(queryParam);
  }

  preventExportClick($event: MouseEvent): void {
    $event?.stopPropagation();
  }
}

export enum ExportFormat {
  TSV = "tsv",
  JSON = "json",
  XML = "xml",
  BIBTEX = "bibtex"
}
