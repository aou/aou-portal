/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-research-group-list.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {SearchHelper} from "@home/helpers/search.helper";
import {
  AdvancedSearchCriteria,
  ResearchGroup,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-research-group-list-container",
  templateUrl: "./home-research-group-list.container.html",
  styleUrls: ["./home-research-group-list.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeResearchGroupListContainer extends SharedAbstractContainer {
  private _researchGroups: ResearchGroup[];

  @Input()
  set researchGroups(value: ResearchGroup[]) {
    this._researchGroups = value;
    this._computeResearchGroupLinks();
  }

  get researchGroups(): ResearchGroup[] {
    return this._researchGroups;
  }

  researchGroupLinks: Navigate[];

  constructor(private readonly _store: Store) {
    super();
  }

  trackByFnWithResId(index: number, item: ResearchGroup): string {
    return item.resId;
  }

  private _computeResearchGroupLinks(): Navigate[] {
    this.researchGroupLinks = [];
    const researchGroupCriteria: AdvancedSearchCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.researchGroupCriteria);

    if (isNullOrUndefined(this.researchGroupLinks) || isNullOrUndefinedOrEmptyArray(this.researchGroups) || isNullOrUndefined(researchGroupCriteria)) {
      return;
    }
    this.researchGroups.forEach(researchGroup => {
      if (isNullOrUndefinedOrWhiteString(researchGroup.resId)) {
        this.researchGroupLinks.push(undefined);
        return;
      }
      const navigate = SearchHelper.generateSearchPageNavigateAction({
        advancedSearch: {[researchGroupCriteria.alias]: [researchGroup.resId]},
      });
      this.researchGroupLinks.push(navigate);
    });
  }
}
