/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-pin-button.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {AppPersonDepositState} from "@app/stores/person/deposit/app-person-deposit.state";
import {AppPinboardAction} from "@app/stores/pinboard/app-pinboard.action";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-pin-button-container",
  templateUrl: "./home-pin-button.container.html",
  styleUrls: ["./home-pin-button.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePinButtonContainer extends SharedAbstractPresentational implements OnInit {
  listPinnedDepositObs: Observable<PublishedDeposit[]> = MemoizedUtil.selected(this._store, AppPersonDepositState);

  @Input()
  publishedDepositId: string;

  isPinned: boolean = false;

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.listPinnedDepositObs.pipe(
      filter(list => isNotNullNorUndefined(list)),
      tap(list => {
        this.isPinned = list.findIndex(d => d.resId === this.publishedDepositId) !== -1;
        this._changeDetector.detectChanges();
      }),
    ));
  }

  addToPinboard(event?: MouseEvent): void {
    this._stopPropagation(event);
    if (this.isPinned) {
      return;
    }
    this._store.dispatch(new AppPinboardAction.Add(this.publishedDepositId));
  }

  removeToPinboard(event?: MouseEvent): void {
    this._stopPropagation(event);
    if (!this.isPinned) {
      return;
    }
    this._store.dispatch(new AppPinboardAction.Remove(this.publishedDepositId));
  }

  private _stopPropagation(event: MouseEvent): void {
    event?.stopPropagation();
  }
}
