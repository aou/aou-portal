/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-publication-structure-list.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {DepositFormAcademicStructure} from "@deposit/models/deposit-form-definition.model";
import {SearchHelper} from "@home/helpers/search.helper";
import {AdvancedSearchCriteria} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-publication-structure-list-container",
  templateUrl: "./home-publication-structure-list.container.html",
  styleUrls: ["./home-publication-structure-list.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePublicationStructureListContainer extends SharedAbstractContainer {
  private _structures: DepositFormAcademicStructure[];

  @Input()
  set structures(value: DepositFormAcademicStructure[]) {
    this._structures = value;
    this._computeStructureLinks();
  }

  get structures(): DepositFormAcademicStructure[] {
    return this._structures;
  }

  private _structuresWithParentsIds: string[][];

  @Input()
  set structuresWithParentsIds(value: string[][]) {
    this._structuresWithParentsIds = value;
    this._computeStructureLinks();
  }

  get structuresWithParentsIds(): string[][] {
    return this._structuresWithParentsIds;
  }

  private _structuresWithParentsNames: string[][];

  @Input()
  set structuresWithParentsNames(value: string[][]) {
    this._structuresWithParentsNames = value;
    this._computeStructureLinks();
  }

  get structuresWithParentsNames(): string[][] {
    return this._structuresWithParentsNames;
  }

  structureLinks: Navigate[][];

  constructor(private readonly _store: Store) {
    super();
  }

  private _computeStructureLinks(): void {
    this.structureLinks = [];
    const structureCriteria: AdvancedSearchCriteria = MemoizedUtil.selectSnapshot(this._store, AppSearchState, state => state.structureCriteria);
    if (isNullOrUndefined(structureCriteria) || isNullOrUndefinedOrEmptyArray(this.structuresWithParentsIds)) {
      return;
    }
    this.structuresWithParentsIds.forEach(structureGroup => {
      const structureGroupLink = [];
      [...structureGroup].reverse().forEach(structureId => {
        const navigate = SearchHelper.generateSearchPageNavigateAction({
          advancedSearch: {[structureCriteria.alias]: [structureId]},
        });
        structureGroupLink.push(navigate);
      });
      this.structureLinks.push(structureGroupLink);
    });
  }
}
