/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-special-info.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {Enums} from "@enums";
import {SearchHelper} from "@home/helpers/search.helper";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "solidify-frontend";

@Component({
  selector: "aou-home-special-info",
  templateUrl: "./home-special-info.presentational.html",
  styleUrls: ["./home-special-info.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeSpecialInfoPresentational extends SharedAbstractPresentational {
  private _publishedDeposit: PublishedDeposit;

  @Input()
  set publishedDeposit(value: PublishedDeposit) {
    this._publishedDeposit = value;

    this._resetInfos();

    if (isNullOrUndefined(this._publishedDeposit)) {
      return;
    }

    this._computeInfos();
  }

  get publishedDeposit(): PublishedDeposit {
    return this._publishedDeposit;
  }

  typeIntoText(value: any): Text[] {
    return value;
  }

  publishedInInfo: Text[];
  presentedAtInfo: Text[];
  pagesInfo: string;
  rawPagesInfo: string;

  constructor(private readonly _translateService: TranslateService) {
    super();
  }

  private _resetInfos(): void {
    this.publishedInInfo = undefined;
    this.presentedAtInfo = undefined;
    this.pagesInfo = undefined;
    this.rawPagesInfo = undefined;
  }

  private _computeInfos(): void {
    this._computePagesInfo();

    if (this.isPublishedIn) {
      this._computePublishedInInfo();
    }
    if (this.isPresentedAt) {
      const isChapterOfProceeding = this.publishedDeposit.subtypeCode === Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS;
      this._computePresentedAtInfo(!isChapterOfProceeding, !isChapterOfProceeding);
    }
  }

  private _getContainerTitleWithLink(): Text | undefined {
    if (isNotNullNorUndefinedNorWhiteString(this.publishedDeposit?.container?.title)) {
      return {
        value: this.publishedDeposit.container.title,
        link: SearchHelper.generateSearchPageNavigateAction({
          advancedSearch: {[Enums.Facet.Name.PUBLISHED_IN]: [this.publishedDeposit.container.title]},
        }),
      };
    }
  }

  get isPublishedIn(): boolean {
    return [
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.SCIENTIFIC_ARTICLE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_ARTICLE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.OTHER_ARTICLE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS,
    ].includes(this.publishedDeposit.subtypeCode as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
  }

  get isPresentedAt(): boolean {
    return [
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRESENTATION_SPEECH,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.POSTER,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS,
    ].includes(this.publishedDeposit.subtypeCode as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
  }

  get isPagination(): boolean {
    return [
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.COLLECTIVE_WORK,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONFERENCE_PROCEEDINGS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.THESIS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_OF_ADVANCED_STUDIES,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.MASTER_DEGREE,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PRIVATE_DOCTOR_THESIS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PROFESSIONAL_THESIS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.RESEARCH_REPORT,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.TECHNICAL_REPORT,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.WORKING_PAPER,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.PREPRINT,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE,
    ].includes(this.publishedDeposit.subtypeCode as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
  }

  /**
   * If return true, start with editor
   * if return false, start with container
   */
  get publishInStartWithEditor(): boolean {
    return [
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.BOOK_CHAPTER,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS,
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CONTRIBUTION_TO_A_DICTIONARY_ENCYCLOPAEDIA,
    ].includes(this.publishedDeposit.subtypeCode as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
  }

  /**
   * If return true, start with 'presented at' section then 'publish in'
   * if return false, start with 'publish in' section then 'presented at'
   */
  get startWithPresentedAt(): boolean {
    return [
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.CHAPTER_OF_PROCEEDINGS,
    ].includes(this.publishedDeposit.subtypeCode as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
  }

  get isJournalIssue(): Boolean {
    return [
      Enums.Deposit.DepositFriendlyNameSubTypeEnum.JOURNAL_ISSUE,
    ].includes(this.publishedDeposit.subtypeCode as Enums.Deposit.DepositFriendlyNameSubTypeEnum);
  }

  private _computePublishedInInfo(): void {
    this.publishedInInfo = [];
    const container = this.publishedDeposit?.container;
    const containerTitleWithLink = this._getContainerTitleWithLink();
    if (isNotNullNorUndefined(containerTitleWithLink)) {
      this.publishedInInfo.push(containerTitleWithLink);
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.editor)) {
      this.publishedInInfo.push({value: `${container.editor} ${this._translateService.instant(LabelTranslateEnum.editorShort)}`});
    }
    if (this.publishInStartWithEditor) {
      this.publishedInInfo.reverse();
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.volume)) {
      this.publishedInInfo.push({value: `${this._translateService.instant(LabelTranslateEnum.volumeShort)} ${container.volume}`});
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.issue)) {
      this.publishedInInfo.push({value: `${this._translateService.instant(LabelTranslateEnum.issueShort)} ${container.issue}`});
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.specialIssue)) {
      this.publishedInInfo.push({value: `${this._translateService.instant(LabelTranslateEnum.specialIssueShort)} ${container.specialIssue}`});
    }
    if (isNotNullNorUndefinedNorWhiteString(this.pagesInfo) && !this.isJournalIssue) {
      this.publishedInInfo.push({value: this.pagesInfo});
    }
  }

  private _computePresentedAtInfo(withContainerTitleInfo: boolean, withPageInfo: boolean): void {
    this.presentedAtInfo = [];
    const container = this.publishedDeposit?.container;
    if (withContainerTitleInfo) {
      const containerTitleWithLink = this._getContainerTitleWithLink();
      if (isNotNullNorUndefined(containerTitleWithLink)) {
        this.presentedAtInfo.push(containerTitleWithLink);
      }
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.conferenceSubtitle)) {
      this.presentedAtInfo.push({value: container.conferenceSubtitle});
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.conferencePlace)) {
      this.presentedAtInfo.push({value: container.conferencePlace});
    }
    if (isNotNullNorUndefinedNorWhiteString(container?.conferenceDate)) {
      this.presentedAtInfo.push({value: container.conferenceDate});
    }
    if (withPageInfo && isNotNullNorUndefinedNorWhiteString(this.pagesInfo)) {
      this.presentedAtInfo.push({value: this.pagesInfo});
    }
  }

  private _computePagesInfo(): void {
    const pages = [];
    const rawPages = [];
    if (isNullOrUndefined(this.publishedDeposit?.pages)) {
      return undefined;
    }
    if (isNotNullNorUndefinedNorWhiteString(this.publishedDeposit?.pages?.paging)) {
      const paging = this.publishedDeposit?.pages?.paging;
      pages.push(`${this._translateService.instant(LabelTranslateEnum.pagesShort)} ${paging}`);
      rawPages.push(paging);
    }
    if (isNotNullNorUndefinedNorWhiteString(this.publishedDeposit?.pages?.other)) {
      pages.push(this.publishedDeposit.pages.other);
      rawPages.push(this.publishedDeposit.pages.other);
    }
    this.pagesInfo = pages.join("; ");
    this.rawPagesInfo = rawPages.join("; ");
  }
}

interface Text {
  value: string;
  link?: Navigate;
}
