/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-citation.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnInit,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  Citation,
  isNonEmptyArray,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  NotificationService,
} from "solidify-frontend";
import {FormControl} from "@angular/forms";

@Component({
  selector: "aou-home-citation",
  templateUrl: "./home-citation.presentational.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeCitationPresentational extends SharedAbstractPresentational implements OnInit {
  formControl = new FormControl();

  private _listCitations: Citation[];

  selectedCitation: Citation = undefined;

  @Input()
  set listCitations(value: Citation[]) {
    this._listCitations = value;

    if (isNonEmptyArray(this.listCitations)) {
      this.selectedCitation = this.listCitations[0];
    } else {
      this.selectedCitation = undefined;
    }
    this.formControl.setValue(this.selectedCitation);
  }

  get listCitations(): Citation[] {
    return this._listCitations;
  }

  constructor(private readonly _notificationService: NotificationService,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super();
  }
}
