/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-file-info.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {PublishedFile} from "@home/models/published-deposit.model";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DepositRoleHelper} from "@shared/helpers/deposit-role.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  ButtonThemeEnum,
  DateUtil,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  ObjectUtil,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-file-info",
  templateUrl: "./home-file-info.presentational.html",
  styleUrls: ["./home-file-info.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeFileInfoPresentational extends SharedAbstractPresentational implements OnInit {
  get downloadMode(): DownloadMode {
    if (this.isFilePublic) {
      return DownloadMode.allowed;
    } else if (this.isFileRestricted) {
      if (this.isUserUnigeOrHug || this.isCreatorOrContributorOrValidator || this._securityService.isRootOrAdmin()) {
        return DownloadMode.allowed;
      }
    } else if (this.isFilePrivate) {
      if (this.isCreatorOrContributorOrValidator || this._securityService.isRootOrAdmin()) {
        return DownloadMode.allowed;
      }
    }

    // Default case
    if (this.isLoggedIn) {
      return DownloadMode.forbidden;
    }
    return DownloadMode.loginRequired;
  }

  get tooltip(): string | undefined {
    const downloadMode = this.downloadMode;
    if (downloadMode === DownloadMode.allowed) {
      return undefined;
    }
    if (this.isFileRestricted) {
      return LabelTranslateEnum.accessLimitedToMembersOfTheUnigeOrHugCommunity;
    } else if (this.isFilePrivate) {
      return LabelTranslateEnum.accessLimitedToTheAuthorsOfTheDocument;
    }
  }

  get downloadModeEnum(): typeof DownloadMode {
    return DownloadMode;
  }

  get buttonThemeEnum(): typeof ButtonThemeEnum {
    return ButtonThemeEnum;
  }

  get isCreatorOrContributorOrValidator(): boolean {
    return DepositRoleHelper.isCreatorOrContributorOrValidator(this.listUserRoleEnum);
  }

  private _file: PublishedFile;

  @Input()
  set file(value: PublishedFile) {
    this._file = value;

    if (isNullOrUndefined(this._file)) {
      return;
    }

    this._cleanEmbargoInfosIfOver();

    const extension = FileVisualizerHelper.getFileExtension(this._file.name).toLowerCase();
    this.icon = this._defineIcon(extension);

    this.canPreview = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
      dataFile: {
        fileName: this._file.name,
        fileSize: this._file.size,
        mimetype: this._file.mimeType,
        status: FileStatusEnum.READY,
      },
      fileExtension: extension,
    });
  }

  get file(): PublishedFile {
    return this._file;
  }

  @Input()
  isLoggedIn: boolean = false;

  @Input()
  isUserUnigeOrHug: boolean = false;

  @Input()
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[] = [];

  @Input()
  isLoadingPrepareDownload: boolean = false;

  canPreview: boolean = false;
  icon: IconNameEnum;

  private readonly _downloadBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _loginBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("loginChange")
  readonly loginObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._loginBS);

  private readonly _previewBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("previewChange")
  readonly previewObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._previewBS);

  get dateUtil(): typeof DateUtil {
    return DateUtil;
  }

  constructor(private readonly _fileVisualizerService: FileVisualizerService,
              private readonly _securityService: SecurityService) {
    super();
  }

  download(): void {
    this._downloadBS.next(this.file);
  }

  login(): void {
    this._loginBS.next(this.file);
  }

  preview(): void {
    this._previewBS.next(this.file);
  }

  private _defineIcon(extension: string): IconNameEnum {
    if (["pdf"].includes(extension)) {
      return IconNameEnum.filePdf;
    }
    if (["doc", "docx"].includes(extension)) {
      return IconNameEnum.fileDoc;
    }
    if (["zip", "rar"].includes(extension)) {
      return IconNameEnum.fileZip;
    }
    if (["txt", "json"].includes(extension)) {
      return IconNameEnum.fileLines;
    }
    if (["jpg", "jpeg", "png"].includes(extension)) {
      return IconNameEnum.fileImage;
    }
    if (["xls", "xlsx"].includes(extension)) {
      return IconNameEnum.fileExcel;
    }
    if (["csv"].includes(extension)) {
      return IconNameEnum.fileCsv;
    }
    return IconNameEnum.file;
  }

  get isFilePublic(): boolean {
    return this._isAccessLevel(Enums.Deposit.AccessLevelEnum.PUBLIC);
  }

  get isFileRestricted(): boolean {
    return this._isAccessLevel(Enums.Deposit.AccessLevelEnum.RESTRICTED);
  }

  get isFilePrivate(): boolean {
    return this._isAccessLevel(Enums.Deposit.AccessLevelEnum.PRIVATE);
  }

  private _isAccessLevel(accessLevel: Enums.Deposit.AccessLevelEnum): boolean {
    return this.isEmbargo ? this.file?.embargoAccessLevel === accessLevel : this.file?.accessLevel === accessLevel;
  }

  get isEmbargo(): boolean {
    return isNotNullNorUndefinedNorWhiteString(this.file?.embargoAccessLevel);
  }

  private _cleanEmbargoInfosIfOver(): void {
    if (this.isEmbargo && !this._file.isUnderEmbargo) {
      // File has an embargo, but it is over --> clean embargo infos to display only final access
      const file = ObjectUtil.clone(this._file);
      file.embargoAccessLevel = null;
      file.embargoEndDate = null;
      this._file = file;
    }
  }
}

export type DownloadMode =
  "allowed"
  | "forbidden"
  | "login-required";
export const DownloadMode = {
  allowed: "allowed" as DownloadMode,
  forbidden: "forbidden" as DownloadMode,
  loginRequired: "login-required" as DownloadMode,
};
