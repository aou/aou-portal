/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-advanced-search.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
} from "@angular/forms";
import {
  SharedContributorAction,
  sharedContributorActionNameSpace,
} from "@app/shared/stores/contributor/shared-contributor.action";
import {
  ContributorSearchTypeEnum,
  SharedContributorModeEnum,
  SharedContributorState,
} from "@app/shared/stores/contributor/shared-contributor.state";
import {sharedResearchGroupActionNameSpace} from "@app/shared/stores/research-group/shared-research-group.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeContributorOverlayPresentational} from "@home/components/presentationals/home-contributor-overlay/home-contributor-overlay.presentational";
import {AdvancedSearchHelper} from "@home/helpers/advanced-search.helper";
import {
  AdvancedSearchCriteria,
  Contributor,
  ContributorRockSolid,
  EnumValueTranslated,
  ResearchGroup,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {
  BehaviorSubject,
  Observable,
  Subscription,
  tap,
} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
} from "rxjs/operators";
import {
  BackTranslatePipe,
  BaseFormDefinition,
  FormValidationHelper,
  isArray,
  isEmptyArray,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  isString,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  ObjectUtil,
  ObservableUtil,
  OrderEnum,
  OverlayPositionEnum,
  PropertyName,
  ResourceNameSpace,
  SOLIDIFY_CONSTANTS,
  Sort,
  SsrUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "aou-advanced-search",
  templateUrl: "./home-advanced-search.presentational.html",
  styleUrls: ["./home-advanced-search.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeAdvancedSearchPresentational extends SharedAbstractPresentational implements OnInit {
  contributorOverlayComponent: Type<HomeContributorOverlayPresentational> = HomeContributorOverlayPresentational;

  private _isInitialized: boolean = false;

  private readonly _LIST_TYPE_WITH_MULTI_VALUES: Enums.Criteria.TypeEnum[] = [
    Enums.Criteria.TypeEnum.ENUM_MULTIPLE,
    Enums.Criteria.TypeEnum.STRUCTURE,
    Enums.Criteria.TypeEnum.STRUCTURE_SUB_STRUCTURES,
    Enums.Criteria.TypeEnum.RESEARCH_GROUP,
    Enums.Criteria.TypeEnum.CONTRIBUTOR_ALL,
    Enums.Criteria.TypeEnum.CONTRIBUTOR_UNIGE,
  ];
  private _backTranslatePipe: BackTranslatePipe;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  private _listCriterias: AdvancedSearchCriteria[] = [];

  @Input()
  set listCriterias(value: AdvancedSearchCriteria[]) {
    this._listCriterias = value;
    this._extractAndCombinedSpecificsCriterias();
  }

  get listCriterias(): AdvancedSearchCriteria[] {
    return this._listCriterias;
  };

  get listCriteriasWithoutCombined(): AdvancedSearchCriteria[] {
    return this._listCriterias.filter(c => isNullOrUndefined(c.isChildOf));
  };

  private _advancedSearchInit: MappingObject<string, string[]>;

  @Input()
  set advancedSearchInit(value: MappingObject<string, string[]>) {
    this._advancedSearchInit = value;

    if (this._isInitialized) {
      const isExternalChange = JSON.stringify(this._advancedSearchInit) !== JSON.stringify(this._searchBS.value);
      // Allow to refresh form according to external update
      if (isExternalChange) {
        this.formArray.clear({emitEvent: false});
        this.displayAdvancedSearch = true;
        this._changeDetector.detectChanges();
        this._bindExternalAdvancedSearchToForm(false);
      }
    }
  }

  get advancedSearchInit(): MappingObject<string, string[]> {
    return this._advancedSearchInit;
  }

  private readonly _searchBS: BehaviorSubject<MappingObject<string, string[]> | undefined> = new BehaviorSubject<MappingObject<string, string[]> | undefined>(undefined);
  @Output("searchChange")
  readonly searchObs: Observable<MappingObject<string, string[]> | undefined> = ObservableUtil.asObservable(this._searchBS);

  get overlayPositionEnum(): typeof OverlayPositionEnum {
    return OverlayPositionEnum;
  }

  get typeCriteria(): typeof Enums.Criteria.TypeEnum {
    return Enums.Criteria.TypeEnum;
  }

  get getFormArrayControl(): FormGroup[] {
    return this.formArray.controls as FormGroup[];
  }

  getFormControl(formGroup: FormGroup, key: string): FormControl {
    return FormValidationHelper.getFormControl(formGroup, key);
  }

  hasRequiredField(formControl: FormControl): boolean {
    return FormValidationHelper.hasRequiredField(formControl);
  }

  formArray: FormArray;

  displayAdvancedSearch: boolean = false;

  readonly KEY_RESEARCH_GROUP_SEARCH_NAME: string = SharedResearchGroupState.FIELD_SEARCH_NAME;
  sharedResearchGroupSort: Sort<ResearchGroup> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  researchGroupLabel: (value: ResearchGroup) => string = value => SharedResearchGroupState.labelCallback(value, this._translateService);
  sharedResearchGroupActionNameSpace: ResourceNameSpace = sharedResearchGroupActionNameSpace;
  sharedResearchGroupState: typeof SharedResearchGroupState = SharedResearchGroupState;
  researchGroupPreTreatmentHighlightText: (result: string) => string = SharedResearchGroupState.preTreatmentHighlightText;
  researchGroupPostTreatmentHighlightText: (result: string, resultBeforePreTreatment: string) => string = SharedResearchGroupState.postTreatmentHighlightText;

  readonly KEY_CONTRIBUTOR_SEARCH_FIELD: string = SharedContributorState.SEARCH_FIELD;
  readonly KEY_CONTRIBUTOR_ATTRIBUTE_FULL_NAME: string = SharedContributorState.ATTRIBUTE_FULL_NAME;
  readonly KEY_CONTRIBUTOR_ATTRIBUTE_CN_INDIVIDU: string = SharedContributorState.ATTRIBUTE_CN_INDIVIDU;
  sharedContributorSort: Sort<ContributorRockSolid> = {
    field: SharedContributorState.SORT_FIELD,
    order: OrderEnum.ascending,
  };
  contributorLabel: (value: Contributor) => string = value => SharedContributorState.labelCallback(value);
  contributorExtraInfoSecondLineLabelCallback: (value: Contributor) => string = value => value.orcid;
  allContributorColorHexaForLineCallback: (value: Contributor) => string = value => {
    if (isNullOrUndefinedOrWhiteString(value.cnIndividu)) {
      return;
    }
    return "#cf00634f";
  };

  sharedContributorActionNameSpace: ResourceNameSpace = sharedContributorActionNameSpace;
  sharedContributorState: typeof SharedContributorState = SharedContributorState;
  getByListIdCnIndividu: (listResId: string[]) => void = listResId => new SharedContributorAction.GetByListId(listResId, false, true, SharedContributorModeEnum.CN_INDIVIDU);
  getByListIdFullName: (listResId: string[]) => void = listResId => new SharedContributorAction.GetByListId(listResId, false, true, SharedContributorModeEnum.FULL_NAME);

  contributorUnigeExtraSearchParameterSource: MappingObject<string, string> = {
    [SharedContributorState.TYPE_FIELD]: ContributorSearchTypeEnum.UNIGE,
  };

  contributorNonUnigeExtraSearchParameterSource: MappingObject<string, string> = {
    [SharedContributorState.TYPE_FIELD]: ContributorSearchTypeEnum.ALL,
  };

  researchGroupQueryParam: MappingObject<string, string> = {
    validated: SOLIDIFY_CONSTANTS.STRING_TRUE,
  };

  enumLabelCallback: (enumValue: EnumValueTranslated) => string = enumValue => this._backTranslatePipe.transform(enumValue.labels);

  constructor(private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _translateService: TranslateService,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _elementRef: ElementRef) {
    super();
    this._backTranslatePipe = new BackTranslatePipe(this._store, environment);
    this.formArray = this._fb.array([]);
    this.addCriteria();

    this.subscribe(this.formArray.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(600),
      tap(v => {
        this._search();
      }),
    ));
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._removeEmptyCriteriaIfPresent();
    this._bindExternalAdvancedSearchToForm();
    this._isInitialized = true;
  }

  private _removeEmptyCriteriaIfPresent(): void {
    if (MappingObjectUtil.size(this.advancedSearchInit) > 0) {
      const indexInitialEmptyCriteria = this.formArray.controls.findIndex(c => c.get(this.formDefinition.alias).value === SOLIDIFY_CONSTANTS.STRING_EMPTY);
      if (indexInitialEmptyCriteria !== -1) {
        this.formArray.removeAt(indexInitialEmptyCriteria);
      }
      this.displayAdvancedSearch = true;
    }
  }

  private _bindExternalAdvancedSearchToForm(emitEvent: boolean = true): void {
    MappingObjectUtil.forEach(this.advancedSearchInit, (values: string[], key) => {
      this.bindCriteria(key, values, emitEvent);
    });
  }

  private _extractAndCombinedSpecificsCriterias(): void {
    if (isNullOrUndefinedOrEmptyArray(this._listCriterias)) {
      return;
    }
    this._listCriterias = [...this._listCriterias];

    this._criteriaToCombined(Enums.Criteria.TypeEnum.CONTRIBUTOR_ALL, Enums.Criteria.TypeEnum.CONTRIBUTOR_UNIGE, Enums.Criteria.TypeEnum.CONTRIBUTOR_COMBINED);
    this._criteriaToCombined(Enums.Criteria.TypeEnum.STRUCTURE, Enums.Criteria.TypeEnum.STRUCTURE_SUB_STRUCTURES, Enums.Criteria.TypeEnum.STRUCTURE_COMBINED);
  }

  private _criteriaToCombined(uncheckedType: Enums.Criteria.TypeEnum, checkedType: Enums.Criteria.TypeEnum, combinedType: Enums.Criteria.TypeEnum): void {
    const indexUncheckedCriteria = this._getIndexOfSpecificCriteria(uncheckedType);
    const uncheckedCriteria = this._extractSpecificCriteria(indexUncheckedCriteria, uncheckedType);
    const indexCheckedCriteria = this._getIndexOfSpecificCriteria(checkedType);
    const checkedCriteria = this._extractSpecificCriteria(indexCheckedCriteria, checkedType);
    if (isNotNullNorUndefined(uncheckedCriteria) && isNotNullNorUndefined(checkedCriteria)) {
      const criteriaContributorCombination = {
        type: combinedType,
        labels: uncheckedCriteria.labels,
        checkedCriteria: checkedCriteria,
        uncheckedCriteria: uncheckedCriteria,
        isCombination: true,
      } as AdvancedSearchCriteria;
      const smallestIndex = indexUncheckedCriteria < indexCheckedCriteria ? indexUncheckedCriteria : indexCheckedCriteria;
      this._listCriterias.splice(smallestIndex, 0, criteriaContributorCombination);
      uncheckedCriteria.isChildOf = criteriaContributorCombination;
      checkedCriteria.isChildOf = criteriaContributorCombination;
    }
    if (isNotNullNorUndefined(uncheckedCriteria)) {
      this._listCriterias.splice(indexUncheckedCriteria, 0, uncheckedCriteria);
    }
    if (isNotNullNorUndefined(checkedCriteria)) {
      this._listCriterias.splice(indexCheckedCriteria, 0, checkedCriteria);
    }
  }

  private _getIndexOfSpecificCriteria(type: Enums.Criteria.TypeEnum): number {
    return this._listCriterias.findIndex(v => v.type === type);
  }

  private _extractSpecificCriteria(indexOfCriteria: number, type: Enums.Criteria.TypeEnum): AdvancedSearchCriteria | undefined {
    if (indexOfCriteria !== -1) {
      return ObjectUtil.clone(this._listCriterias.splice(indexOfCriteria, 1)[0]);
    }
  }

  toggleAdvancedSearch(): void {
    this.displayAdvancedSearch = !this.displayAdvancedSearch;
  }

  addCriteria(): void {
    const formGroup = this._fb.group({
      [this.formDefinition.alias]: [""],
      [this.formDefinition.values]: [],
      [this.formDefinition.valueFromRange]: [],
      [this.formDefinition.valueToRange]: [],
      [this.formDefinition.match]: [true],
      [this.formDefinition.checked]: [false],
    });
    this._createSubscriptionToResetValueOnTypeChange(formGroup);
    this._createSubscriptionToResetValuesWhenCheckChange(formGroup);
    this.formArray.push(formGroup);
  }

  bindCriteria(alias: string, values: string[], emitEvent: boolean = true): void {
    const adaptedAliasWithoutIndex = AdvancedSearchHelper.cleanIndexedKey(alias);
    const adaptedAliasWithoutExcluded = AdvancedSearchHelper.cleanExcludedKey(adaptedAliasWithoutIndex);
    const criteria = this.getCriteriaFromAlias(adaptedAliasWithoutExcluded);
    const adaptedValues = this._adaptValuesFromType(criteria?.type, values);
    const formGroup = this._fb.group({
      [this.formDefinition.alias]: [adaptedAliasWithoutExcluded],
      [this.formDefinition.values]: [adaptedValues],
      [this.formDefinition.valueFromRange]: [],
      [this.formDefinition.valueToRange]: [],
      [this.formDefinition.match]: [!AdvancedSearchHelper.isExcludedKey(adaptedAliasWithoutIndex)],
      [this.formDefinition.checked]: [isTrue(criteria.isChildOf?.isCombination) ? adaptedAliasWithoutExcluded === criteria.isChildOf?.checkedCriteria?.alias : undefined],
    });

    if (criteria?.type === Enums.Criteria.TypeEnum.INTERVAL_YEAR && isString(adaptedValues) && adaptedValues.includes(SOLIDIFY_CONSTANTS.DASH)) {
      const res = adaptedValues.trim().split(SOLIDIFY_CONSTANTS.DASH);
      if (res.length === 2) {
        formGroup.get(this.formDefinition.valueFromRange).setValue(res[0]);
        formGroup.get(this.formDefinition.valueToRange).setValue(res[1]);
      }
    }
    this._createSubscriptionToResetValueOnTypeChange(formGroup);
    this._createSubscriptionToResetValuesWhenCheckChange(formGroup);
    this.formArray.push(formGroup, {emitEvent: emitEvent});
  }

  private _createSubscriptionToResetValueOnTypeChange(formGroup: FormGroup): Subscription {
    return this.subscribe(formGroup.get(this.formDefinition.alias).valueChanges.pipe(
      tap(alias => {
        formGroup.get(this.formDefinition.values).reset();
        formGroup.get(this.formDefinition.valueFromRange).reset();
        formGroup.get(this.formDefinition.valueToRange).reset();
        this._resetCheck(formGroup, alias);
      }),
    ));
  }

  private _resetCheck(formGroup: FormGroup, alias: string): void {
    const criteria = this.listCriterias.find(c => c.alias === alias);
    let shouldCheck = false;
    if (isNotNullNorUndefined(criteria?.isChildOf)) {
      shouldCheck = (criteria?.isChildOf?.type === Enums.Criteria.TypeEnum.STRUCTURE_COMBINED
        || criteria?.isChildOf?.type === Enums.Criteria.TypeEnum.CONTRIBUTOR_COMBINED);
    }
    setTimeout(() => {
      formGroup.get(this.formDefinition.checked).setValue(shouldCheck);
    }, 0);
  }

  private _createSubscriptionToResetValuesWhenCheckChange(formGroup: FormGroup): Subscription {
    const fcChecked = formGroup.get(this.formDefinition.checked) as FormControl;
    return this.subscribe(fcChecked.valueChanges.pipe(
      tap(isChecked => {
        const criteria = this.getRootCriteriaFromFormGroup(formGroup);
        if (isNullOrUndefined(criteria) || criteria.type !== Enums.Criteria.TypeEnum.CONTRIBUTOR_COMBINED) {
          return;
        }

        const fcValues = formGroup.get(this.formDefinition.values);
        const previousValues = fcValues.value;
        fcValues.setValue([]);
        if (isNonEmptyArray(previousValues)) {
          this._refreshContributor(fcChecked);
        }
      }),
    ));
  }

  getCriteriaFromAlias(alias: string): AdvancedSearchCriteria | undefined {
    return this.listCriterias.find(c => c.alias === alias);
  }

  get numberCriteria(): number {
    return this.formArray.controls.filter(c => {
      const values = c.get(this.formDefinition.values).value;
      if (isArray(values)) {
        return values.length > 0;
      } else {
        return isNotNullNorUndefinedNorWhiteString(values);
      }
    }).length;
  }

  get canDelete(): boolean {
    if (this.formArray.length > 1) {
      return true;
    }
    const criteria = this.formArray.at(0);
    if (criteria) {
      return isNotNullNorUndefinedNorWhiteString(criteria.get(this.formDefinition.alias).value);
    }
  }

  trackByFn(index: number, item: FormGroup): string {
    const formDefinition = new FormComponentFormDefinition();
    return item?.get(formDefinition.match)?.value + "_" + item?.get(formDefinition.alias)?.value;
  }

  toggleMatchMode(formControl: FormControl): void {
    formControl.setValue(!formControl.value);
  }

  deleteCriteria(i: number): void {
    this.formArray.removeAt(i);
    if (this.formArray.length === 0) {
      this._search();
      this.addCriteria();
    }
  }

  private _search(): void {
    this._searchBS.next(this._generateMappingObject());
  }

  private _generateMappingObject(): MappingObject<string, string[]> {
    const mappingObject = {} as MappingObject<string, string[]>;
    this.formArray.controls.forEach((formGroup: FormGroup, index: number) => {
      let alias = formGroup.get(this.formDefinition.alias).value;
      if (isNullOrUndefinedOrWhiteString(alias)) {
        return;
      }

      const rootCriteria = this.getRootCriteriaFromFormGroup(formGroup);
      if (rootCriteria.isCombination) {
        const isChecked = formGroup.get(this.formDefinition.checked).value;
        alias = isChecked ? rootCriteria.checkedCriteria.alias : rootCriteria.uncheckedCriteria.alias;
      }

      try {
        const valuesNotNormalized = this._getFormValue(formGroup, alias);
        const values = this._generateNormalizedValuesArray(valuesNotNormalized, alias);
        const match = formGroup.get(this.formDefinition.match).value;
        if (!match) {
          alias = AdvancedSearchHelper.createExcludedKey(alias);
        }
        alias = AdvancedSearchHelper.createIndexedKey(alias, index);
        MappingObjectUtil.set(mappingObject, alias, values);
      } catch (e) {
        // eslint-disable-next-line no-console
        console.warn("Ignore criteria", e);
        return;
      }
    });
    return mappingObject;
  }

  private _getFormValue(formGroup: FormGroup, name: string): string {
    const criteria = this.getCriteriaFromAlias(name);
    if (criteria.type === Enums.Criteria.TypeEnum.INTERVAL_YEAR) {
      let from = formGroup.get(this.formDefinition.valueFromRange).value;
      let to = formGroup.get(this.formDefinition.valueToRange).value;
      if (isNullOrUndefinedOrWhiteString(from) && isNullOrUndefinedOrWhiteString(to)) {
        throw Error("Missing from or to range value: " + from + "-" + to);
      }
      if (isNullOrUndefinedOrWhiteString(from)) {
        from = SOLIDIFY_CONSTANTS.STRING_EMPTY;
      }
      if (isNullOrUndefinedOrWhiteString(to)) {
        to = SOLIDIFY_CONSTANTS.STRING_EMPTY;
      }
      return from + SOLIDIFY_CONSTANTS.DASH + to;
    }
    return formGroup.get(this.formDefinition.values).value;
  }

  private _generateNormalizedValuesArray(values: string | string[], name: string): string[] {
    if (isString(values) && isNullOrUndefinedOrWhiteString(values)) {
      throw Error("Empty or undefined value from criteria " + name);
    }
    if (!isArray(values)) {
      if (isNullOrUndefinedOrWhiteString(values)) {
        values = [];
      } else {
        values = [values.trim()];
      }
    }
    if (isEmptyArray((values))) {
      throw Error("Empty values for criteria " + name);
    }
    return values;
  }

  private _adaptValuesFromType(type: Enums.Criteria.TypeEnum, values: string | string[]): string | string[] {
    if (isNullOrUndefined(type)) {
      return values;
    }
    if (this._LIST_TYPE_WITH_MULTI_VALUES.includes(type)) {
      if (!isArray(values)) {
        if (isNullOrUndefinedOrWhiteString(values)) {
          return [];
        }
        return [values];
      }
    } else {
      if (isArray(values)) {
        return values[0];
      }
    }
    return values;
  }

  getAliasOfCriteriaOrChildCriteria(criteria: AdvancedSearchCriteria, formGroup: FormGroup): string {
    if (criteria.isCombination) {
      return formGroup.get(this.formDefinition.checked).value ? criteria.checkedCriteria.alias : criteria.uncheckedCriteria.alias;
    }
    return criteria.alias;
  }

  getRootCriteriaFromFormGroup(formGroup: FormGroup): AdvancedSearchCriteria {
    const alias = formGroup.get(this.formDefinition.alias).value;
    if (isNullOrUndefinedOrWhiteString(alias)) {
      return;
    }
    const criteria = this.listCriterias.find(c => c.alias === alias);
    return isNullOrUndefined(criteria?.isChildOf) ? criteria : criteria.isChildOf;
  }

  private _refreshContributor(formControl: FormControl): void {
    const previousValue = formControl.value;
    formControl.setValue(undefined);
    this._changeDetector.detectChanges();
    setTimeout(() => {
      formControl.setValue(previousValue);
      this._changeDetector.detectChanges();
    }, 0);
  }

  extraInfoCallback(contributor: Contributor): void {
    SsrUtil.window?.open(environment.orcidUrl + SOLIDIFY_CONSTANTS.URL_SEPARATOR + contributor.orcid, "_blank");
  }
}

export class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() alias: string;
  @PropertyName() values: string;
  @PropertyName() valueFromRange: string;
  @PropertyName() valueToRange: string;
  @PropertyName() match: string;
  @PropertyName() checked: string;
}
