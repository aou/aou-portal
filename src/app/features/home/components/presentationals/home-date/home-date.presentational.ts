/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-date.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {DepositFormDate} from "@deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {PublicationHelper} from "@shared/helpers/publication.helper";
import {
  EnumUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
} from "solidify-frontend";

@Component({
  selector: "aou-home-date",
  templateUrl: "./home-date.presentational.html",
  styleUrls: ["./home-date.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeDatePresentational extends SharedAbstractPresentational {
  @Input()
  dates?: DepositFormDate[];

  @Input()
  type: Enums.Deposit.DateTypesEnum;

  @Input()
  icon: IconNameEnum = IconNameEnum.creationDate;

  labelToTranslate: string = LabelTranslateEnum.date;

  @Input()
  publishedDeposit: PublishedDeposit;

  get date(): DepositFormDate {
    if (isNullOrUndefinedOrEmptyArray(this.dates) || isNullOrUndefined(this.type)) {
      return undefined;
    }

    if (isNotNullNorUndefined(this.publishedDeposit)) {
      this.labelToTranslate = PublicationHelper.getDateLabel(this.type, this.publishedDeposit.subtypeCode);
    } else {
      const label = EnumUtil.getLabel(Enums.Deposit.DateTypesEnumTranslate, this.type);
      if (isNotNullNorUndefined(label)) {
        this.labelToTranslate = label;
      }
    }

    return this.dates.find(c => c.type === this.type);
  }
}
