/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-contributor-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
} from "@angular/core";
import {HomeHelper} from "@home/helpers/home.helper";
import {
  PublicContributorDto,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {overlayAnimation} from "@shared/components/presentationals/shared-resource-logo-overlay/shared-resource-logo-overlay.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {ContributorIdentifierTypeEnum} from "@shared/enums/contributor-identifier-type.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedPublicContributorAction} from "@shared/stores/public-contributor/shared-public-contributor.action";
import {
  AbstractOverlayPresentational,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-contributor-overlay",
  templateUrl: "./home-contributor-overlay.presentational.html",
  styleUrls: ["./home-contributor-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class HomeContributorOverlayPresentational extends AbstractOverlayPresentational<SharedContributorOverlayData, SharedContributorOverlayExtra> implements OnInit {
  avatarUrl: string;
  contributor: PublicContributorDto;
  extra: SharedContributorOverlayExtra;

  searchLink: Navigate;
  contributorPageLink: Navigate;

  get firstName(): string {
    return this.contributor?.firstName ?? this.data.firstName;
  }

  get lastName(): string {
    return this.contributor?.lastName ?? this.data.lastName;
  }

  get orcid(): string {
    return this.contributor?.orcid ?? this.data.orcid;
  }

  protected override _postUpdateData(): void {
  }

  get user(): User {
    if (isNullOrUndefined(this.data)) {
      return this.data;
    }
    return this.data as User;
  }

  get isAvatarPresent(): boolean {
    return false;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _elementRef: ElementRef,
              protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef) {
    super(_elementRef);
  }

  ngOnInit(): void {
    super.ngOnInit();
    const cnIndividu = this.data.cnIndividu;

    if (isNullOrUndefinedOrWhiteString(cnIndividu)) {
      return;
    }
    this.searchLink = HomeHelper.getContributorLink(this._store, false, this.data.cnIndividu, this.data.firstName, this.data.lastName);
    this.contributorPageLink = HomeHelper.getContributorPersonalPageLink(this.data.cnIndividu);

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedPublicContributorAction.GetContributor(cnIndividu, ContributorIdentifierTypeEnum.cnIndividu),
      SharedPublicContributorAction.GetContributorSuccess,
      result => {
        this.contributor = result.contributor;
        if (this.contributor.hasAvatar) {
          this.avatarUrl = `${ApiEnum.accessContributor}/${cnIndividu}/${ApiActionNameEnum.DOWNLOAD_AVATAR}`;
        }
        this._changeDetector.detectChanges();
      }));
  }
}

export interface SharedContributorOverlayData {
  firstName?: string;
  lastName?: string;
  orcid?: string;
  cnIndividu?: string;
}

export interface SharedContributorOverlayExtra {
}
