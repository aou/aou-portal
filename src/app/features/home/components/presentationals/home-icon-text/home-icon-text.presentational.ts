/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-icon-text.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  OnInit,
  TemplateRef,
} from "@angular/core";
import {
  DomSanitizer,
  SafeHtml,
} from "@angular/platform-browser";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  isFalse,
  isString,
  isTrue,
} from "solidify-frontend";

@Component({
  selector: "aou-home-icon-text",
  templateUrl: "./home-icon-text.presentational.html",
  styleUrls: ["./home-icon-text.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeIconTextPresentational extends SharedAbstractPresentational implements OnInit, AfterViewInit {
  @Input()
  icon: IconNameEnum;

  @Input()
  labelToTranslate: string;

  @Input()
  value: string | string[] | TemplateRef<any>;

  valueSafeHtml: SafeHtml;

  @Input()
  isInnerHtml: boolean = false;

  @Input()
  isLink: boolean = false;

  @Input()
  isLabelAboveValue: boolean = false;

  @Input()
  limitHeightShowPx: number = 26;

  @Input()
  limitHeightMinusShowPx: number = 1;

  @Input()
  displayButtonShowMore: boolean = false;

  @Input()
  showMoreButtonLabelWithNumberToTranslate: string;

  @Input()
  truncateValue: boolean = false;

  showMore: boolean = false;

  @HostListener("window:resize", ["$event"])
  private _onResize(event: Event): void {
    if (isTrue(this.displayButtonShowMore) && isFalse(this.showMore)) {
      this._changeDetector.detectChanges();
    }
  }

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              private readonly _domSanitizer: DomSanitizer) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (isString(this.value) && this.isInnerHtml) {
      this.valueSafeHtml = this._domSanitizer.bypassSecurityTrustHtml(this.value);
    }
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    this._changeDetector.detectChanges();
  }

  toggleShowMore(): void {
    this.showMore = !this.showMore;
  }
}
