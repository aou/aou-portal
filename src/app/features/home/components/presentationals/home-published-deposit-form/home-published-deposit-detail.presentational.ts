/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-published-deposit-detail.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  Output,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ImageDisplayModeEnum} from "@app/shared/enums/image-display-mode.enum";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {
  DepositFormAcademicStructure,
  DepositFormClassification,
  DepositFormCollection,
  DepositFormFunding,
  DepositFormGroup,
} from "@deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {AouEnvironment} from "@environments/environment.defaults.model";
import {SearchHelper} from "@home/helpers/search.helper";
import {
  PublishedDeposit,
  PublishedDepositText,
  PublishedFile,
} from "@home/models/published-deposit.model";
import {StatisticsInfo} from "@home/models/statistics-info.model";
import {PublicationSubtypeDocumentFileTypeGroup} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DepositRoutesEnum} from "@shared/enums/routes.enum";
import {DepositRoleHelper} from "@shared/helpers/deposit-role.helper";
import {SecurityService} from "@shared/services/security.service";
import {UserUtil} from "@shared/utils/user.util";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  Citation,
  EnumUtil,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  KeyValue,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  SsrUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-published-deposit-detail",
  templateUrl: "./home-published-deposit-detail.presentational.html",
  styleUrls: ["./home-published-deposit-detail.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePublishedDepositDetailPresentational extends SharedAbstractPresentational implements OnInit, OnChanges {
  private readonly _MINIMAL_YEAR_TO_SEND_TO_OCRID: number = 1900;

  classificationsWithoutDewey: string[];
  collectionInfo: string[];

  keywordLinks: Navigate[];

  @Input()
  isUserUnigeOrHug: boolean = false;

  @Input()
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[] = [];

  @Input()
  isLoggedIn: boolean = false;

  @Input()
  publicationStatistics: StatisticsInfo;

  @Input()
  relativePublishedDeposit: PublishedDeposit[];

  @Input()
  listCitations: Citation[];

  @Input()
  listBibliographies: Citation[];

  @Input()
  isLoadingPrepareDownload: boolean;

  @Input()
  isLoadingArchiveRating: boolean;

  @Input()
  listPublicationSubtypeDocFile: PublicationSubtypeDocumentFileTypeGroup[];

  listInfo: Info[];

  imageArchive: string | undefined;

  subtypeToTranslate: string;

  subSubtypeToTranslate: string;

  localNumberToTranslate: string;

  private readonly _askCorrectionBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("askCorrectionChange")
  readonly askCorrectionObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._askCorrectionBS);

  private _publishedDeposit: PublishedDeposit;

  private _currentUserContributorRole: Enums.Deposit.RoleContributorEnum | undefined;

  @Input()
  set publishedDeposit(value: PublishedDeposit) {
    this._publishedDeposit = undefined;
    this._changeDetector.detectChanges();

    this._publishedDeposit = value;

    if (isNullOrUndefined(this._publishedDeposit)) {
      this._cleanAttributes();
      return;
    }

    this._retrieveCurrentUserContributorRole();

    this.classificationsWithoutDewey = undefined;
    if (isNonEmptyArray(this._publishedDeposit.classifications)) {
      this.classificationsWithoutDewey = this._publishedDeposit.classifications
        .filter(c => c.code !== "Dewey")
        .map(c => this._classificationInfo(c));
    }

    this.collectionInfo = undefined;
    if (isNonEmptyArray(this._publishedDeposit.collections)) {
      this.collectionInfo = this._publishedDeposit.collections
        .map(c => this._collectionInfo(c));
    }

    this._computeSubtypeLabel(this._publishedDeposit.subtypeCode);
    this._computeSubSubtypeLabel(this.publishedDeposit.subsubtype);
    this._computeLocalNumberLabel();
    this._computeIsOpenAccess();

    this._computeKeywordLinks();
  }

  get publishedDeposit(): PublishedDeposit {
    return this._publishedDeposit;
  }

  private readonly _downloadBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _loginBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("loginChange")
  readonly loginObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._loginBS);

  private readonly _previewBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("previewChange")
  readonly previewObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._previewBS);

  private readonly _showArchiveBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("showArchiveChange")
  readonly showArchiveObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._showArchiveBS);

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _contactContributorBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("contactContributorChange")
  readonly contactContributorObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._contactContributorBS);

  private readonly _updateBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("updateChange")
  readonly updateObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._updateBS);

  private readonly _sendToOrcidBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("sendToOrcidChange")
  readonly sendToOrcidObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._sendToOrcidBS);

  // personOverlayComponent: Type<SharedPersonOverlayPresentational> = SharedPersonOverlayPresentational;

  get typeInfoEnum(): typeof TypeInfoEnum {
    return TypeInfoEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get environment(): AouEnvironment {
    return environment;
  }

  get fileTypeLevelEnum(): typeof Enums.DocumentFileType.FileTypeLevelEnum {
    return Enums.DocumentFileType.FileTypeLevelEnum;
  }

  get roleContributorEnum(): typeof Enums.Deposit.RoleContributorEnum {
    return Enums.Deposit.RoleContributorEnum;
  }

  get dateTypesEnum(): typeof Enums.Deposit.DateTypesEnum {
    return Enums.Deposit.DateTypesEnum;
  }

  get diffusionEnum(): typeof Enums.Facet.FacetValue.Diffusion {
    return Enums.Facet.FacetValue.Diffusion;
  }

  trackByFnWithResId(index: number, item: DepositFormGroup | DepositFormAcademicStructure): string {
    return item.resId;
  }

  typeIntoDepositFormFunding(value: any): DepositFormFunding {
    return value;
  }

  get isCreatorOrContributorOrValidator(): boolean {
    return DepositRoleHelper.isCreatorOrContributorOrValidator(this.listUserRoleEnum);
  }

  get isCreatorOrContributor(): boolean {
    return DepositRoleHelper.isCreatorOrContributor(this.listUserRoleEnum);
  }

  get isValidator(): boolean {
    return DepositRoleHelper.isValidator(this.listUserRoleEnum);
  }

  get displaySendToOrcid(): boolean {
    return this.isLoggedIn
      && this.isContributor
      && this.hasOrcidToken
      && UserUtil.isUnigeOrHugUser(MemoizedUtil.currentSnapshot(this._store, AppUserState))
      && isNotNullNorUndefined(this._currentUserContributorRole)
      && (isNullOrUndefined(this.publishedDeposit?.year) || this.publishedDeposit.year >= this._MINIMAL_YEAR_TO_SEND_TO_OCRID);
  }

  get displaySeeDeposit(): boolean {
    return this.isLoggedIn
      && (
        this.isAdminOrRoot
        ||
        this.isValidator
        ||
        this.isCreatorOrContributor
      );
  }

  private _retrieveCurrentUserContributorRole(): void {
    this._currentUserContributorRole = undefined;
    const currentUser = MemoizedUtil.currentSnapshot(this._store, AppUserState);
    let cnIndividu = undefined;
    if (UserUtil.isUnigeUser(currentUser)) {
      try {
        cnIndividu = UserUtil.getCnIndividu(currentUser);
      } catch (e) {
        return;
      }
    }
    const contributor = this.publishedDeposit.contributors.find(c => (isNotNullNorUndefinedNorWhiteString(currentUser?.person?.orcid) && c.orcid === currentUser.person.orcid) || (isNotNullNorUndefinedNorWhiteString(cnIndividu) && c.cnIndividu === cnIndividu));
    this._currentUserContributorRole = contributor?.role;
  }

  get isContributor(): boolean {
    return DepositRoleHelper.isContributor(this.listUserRoleEnum);
  }

  get hasOrcidToken(): boolean {
    return this._securityService.hasOrcidToken();
  }

  get isAdminOrRoot(): boolean {
    return this._securityService.isRootOrAdmin();
  }

  get depositPath(): string | undefined {
    let baseRoute = RoutesEnum.deposit;
    if (this.isAdminOrRoot || this.isValidator) {
      baseRoute = RoutesEnum.depositToValidate;
    }
    return `/${baseRoute}/${DepositRoutesEnum.detail}/${this.publishedDeposit.resId}`;
  }

  constructor(protected readonly _notificationService: NotificationService,
              private readonly _fb: FormBuilder,
              private readonly _cd: ChangeDetectorRef,
              private readonly _store: Store,
              private readonly _securityService: SecurityService,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.imageArchive = `${ApiEnum.accessPublicMetadata}/${this.publishedDeposit.resId}/${ApiActionNameEnum.THUMBNAIL}`;
    this._defineListInfo();

    this._initAbstractCurrentLanguage();
  }

  private _initAbstractCurrentLanguage(): void {
    this.currentAbstract = undefined;
    if (isNullOrUndefined(this.publishedDeposit.abstracts)) {
      return;
    }
    const currentLanguage = Enums.Language.LanguageIso639V1ToV2(MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage));
    let indexCurrentLanguage = this.publishedDeposit.abstracts.filter(t => isNotNullNorUndefined(t))
      .findIndex(t => t?.language?.toLowerCase() === currentLanguage);
    if (indexCurrentLanguage === -1) {
      const defaultLanguage = Enums.Language.LanguageIso639V1ToV2(environment.defaultLanguage);
      indexCurrentLanguage = this.publishedDeposit.abstracts.filter(t => isNotNullNorUndefined(t))
        .findIndex(t => t?.language?.toLowerCase() === defaultLanguage);
      if (indexCurrentLanguage === -1) {
        indexCurrentLanguage = 0;
      }
    }

    this.currentAbstract = this.publishedDeposit.abstracts[indexCurrentLanguage];
    this._initListLanguage(this.currentAbstract);
  }

  translateLanguage(languageIso3: Enums.Language.LanguageV2Enum): string {
    const languageIso2 = Enums.Language.LanguageIso639V2ToV1(languageIso3);
    if (languageIso2 === null) {
      return languageIso3;
    }
    return EnumUtil.getLabel(Enums.Language.LanguageEnumTranslate, languageIso2);
  }

  private _cleanAttributes(): void {
    this.keywordLinks = [];
    this._currentUserContributorRole = undefined;
  }

  private _computeKeywordLinks(): void {
    this.keywordLinks = [];
    if (isNullOrUndefinedOrEmptyArray(this.publishedDeposit.keywords)) {
      return;
    }
    this.publishedDeposit.keywords.forEach(keyword => {
      const navigate = SearchHelper.generateSearchPageNavigateAction({
        advancedSearch: {[Enums.Facet.Name.KEYWORDS]: [keyword]},
      });
      this.keywordLinks.push(navigate);
    });
  }

  listAllLanguages: string[] = [];
  listOtherLanguages: string[] = [];
  currentAbstract: PublishedDepositText;
  isOpenAccess: boolean = false;

  private _computeIsOpenAccess(): void {
    this.isOpenAccess = this.publishedDeposit.openAccess === Enums.Facet.FacetValue.OpenAccessValue.OPEN_ACCESS;
  }

  private _computeSubtypeLabel(subtypeCode: Enums.Deposit.DepositSubTypeEnum): void {
    this.subtypeToTranslate = EnumUtil.getLabel(Enums.Deposit.DepositSubTypeEnumTranslate, subtypeCode);
  }

  private _computeSubSubtypeLabel(subSubtypeName: string): void {
    this.subSubtypeToTranslate = null;
    if (!isNullOrUndefinedOrWhiteString(subSubtypeName) && subSubtypeName !== Enums.Deposit.DepositSubSubTypeNameEnum.ARTICLE) {
      this.subSubtypeToTranslate = EnumUtil.getLabel(Enums.Deposit.DepositSubSubTypeNameEnumTranslate, subSubtypeName);
    }
  }

  private _computeLocalNumberLabel(): void {
    if (isNotNullNorUndefinedNorWhiteString(this._publishedDeposit.localNumber)) {
      this.localNumberToTranslate = DepositFormRuleHelper.getLabelDependingOfSubType(this._publishedDeposit.subtypeCode,
        DepositFormRuleHelper.pathIdentifierLocalNumber);
    }
  }

  switchToLanguage(language: string): void {
    this.currentAbstract = this.publishedDeposit.abstracts.find(t => t.language === language);
    this._initListLanguage(this.currentAbstract);
  }

  arrayMap(list: any[], key: string): string[] {
    return list.map(i => i[key]);
  }

  private _initListLanguage(currentLang: PublishedDepositText | undefined): void {
    this.listAllLanguages = this.publishedDeposit.abstracts.filter(t => isNotNullNorUndefined(t) && isNonEmptyString(t.language))
      .map(t => t?.language);
    this.listOtherLanguages = this.listAllLanguages.filter(t => t !== currentLang);
  }

  trackByFn(index: number, info: Info): string {
    return index + "";
  }

  private _defineListInfo(): void {
    this.listInfo = [];
    this.listInfo = this.listInfo.filter(i => isNonEmptyString(i.value));
  }

  download(file: PublishedFile): void {
    this._downloadBS.next(file);
  }

  login(file: PublishedFile): void {
    this._loginBS.next(this.publishedDeposit);
  }

  preview(file: PublishedFile): void {
    this._previewBS.next(file);
  }

  contactAuthor(): void {
    this._contactContributorBS.next(this.publishedDeposit);
  }

  askCorrection(): void {
    this._askCorrectionBS.next(this.publishedDeposit);
  }

  update(): void {
    this._updateBS.next(this.publishedDeposit);
  }

  sendToOrcid(): void {
    this._sendToOrcidBS.next(this.publishedDeposit);
  }

  get publisherInfo(): string {
    const publisher = [];
    if (isNotNullNorUndefinedNorWhiteString(this.publishedDeposit.publisherPlace)) {
      publisher.push(this.publishedDeposit.publisherPlace);
    }
    if (isNotNullNorUndefinedNorWhiteString(this.publishedDeposit.publisherName)) {
      publisher.push(this.publishedDeposit.publisherName);
    }
    return publisher.join(" : ");
  }

  private _collectionInfo(collection: DepositFormCollection): string {
    const collectionInfos = [];
    if (isNotNullNorUndefinedNorWhiteString(collection.name)) {
      collectionInfos.push(collection.name);
    }
    if (isNotNullNorUndefinedNorWhiteString(collection.number)) {
      collectionInfos.push(collection.number);
    }
    return collectionInfos.join("; ");
  }

  private _classificationInfo(classification: DepositFormClassification): string {
    const classificationInfos = [];
    if (isNotNullNorUndefinedNorWhiteString(classification.code)) {
      classificationInfos.push(classification.code);
    }
    if (isNotNullNorUndefinedNorWhiteString(classification.item)) {
      classificationInfos.push(classification.item);
    }
    return classificationInfos.join(" : ");
  }

  shareFacebook(): void {
    this._share(environment.shareFacebookLink);
  }

  shareTwitter(): void {
    this._share(environment.shareTwitterLink);
  }

  shareLinkedin(): void {
    this._share(environment.shareLinkedinLink);
  }

  shareEmail(): void {
    this._share(`mailto:?subject={title}&body={title} ({url})`, false);
  }

  private _share(baseUrl: string, openWindow: boolean = true): void {
    const url = SsrUtil.window?.encodeURIComponent(SsrUtil.window?.location.href);
    const title = SsrUtil.window?.encodeURIComponent(this.publishedDeposit.title);
    const keyValue: KeyValue[] = [
      {
        key: "url",
        value: url,
      },
      {
        key: "title",
        value: title,
      },
    ];
    const shareLink = StringUtil.formatKeyValue(baseUrl, ...keyValue);
    if (openWindow) {
      SsrUtil.window?.open(shareLink, "_blank");
    } else if (SsrUtil?.window?.document?.location) {
      SsrUtil.window.document.location.href = shareLink;
    }
  }
}

class Info {
  labelToTranslate: string;
  value?: string;
  code?: boolean = false;
  type?: TypeInfoEnum;
}

enum TypeInfoEnum {
  translate = 1,
}
