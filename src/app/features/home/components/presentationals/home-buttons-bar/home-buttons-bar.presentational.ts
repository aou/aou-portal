/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-buttons-bar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {RippleAnimationConfig} from "@angular/material/core";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {PermissionUtil} from "@shared/utils/permission.util";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {ObservableUtil} from "solidify-frontend";

@Component({
  selector: "aou-home-buttons-bar",
  templateUrl: "./home-buttons-bar.presentational.html",
  styleUrls: ["./home-buttons-bar.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeButtonsBarPresentational extends SharedAbstractPresentational implements OnInit {
  @Input()
  logged: boolean;

  @Input()
  userAuthorizedForDepositActions: boolean;

  rippleAnimation: RippleAnimationConfig = {enterDuration: 500, exitDuration: 500};

  private readonly _navigateToSearchBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("navigateToSearch")
  readonly navigateToSearchObs: Observable<void> = ObservableUtil.asObservable(this._navigateToSearchBS);

  private readonly _navigateToDepositOrLoginBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("navigateToDepositOrLogin")
  readonly navigateToDepositOrLoginObs: Observable<void> = ObservableUtil.asObservable(this._navigateToDepositOrLoginBS);

  private readonly _navigateToBibliographieOrLoginBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("navigateToBibliographieOrLogin")
  readonly navigateToBibliographieOrLoginObs: Observable<void> = ObservableUtil.asObservable(this._navigateToBibliographieOrLoginBS);

  get isUserMemberAuthorizedInstitutions(): boolean {
    return PermissionUtil.isUserMemberAuthorizedInstitutions(this.logged, this.userAuthorizedForDepositActions);
  }

  constructor(private readonly _store: Store) {
    super();
  }

  goToDeposit(): void {
    this._navigateToDepositOrLoginBS.next();
  }

  navigateToSearch(): void {
    this._navigateToSearchBS.next();
  }

  navigateToBibliographie(): void {
    this._navigateToBibliographieOrLoginBS.next();
  }
}
