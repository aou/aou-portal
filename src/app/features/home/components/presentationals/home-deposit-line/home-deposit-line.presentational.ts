/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-deposit-line.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {ImageDisplayModeEnum} from "@app/shared/enums/image-display-mode.enum";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNonEmptyArray,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-deposit-line",
  templateUrl: "./home-deposit-line.presentational.html",
  styleUrls: ["./home-deposit-line.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeDepositLinePresentational extends SharedAbstractPresentational implements OnInit {
  private _publishedDeposit: PublishedDeposit;

  @Input()
  set publishedDeposit(value: PublishedDeposit) {
    this._publishedDeposit = value;
    if (isNonEmptyArray(this._publishedDeposit.structures)) {
      this.structures = this._publishedDeposit.structures.map(s => s.name).join(", ");
    } else {
      this.structures = undefined;
    }
  }

  get publishedDeposit(): PublishedDeposit {
    return this._publishedDeposit;
  }

  structures: string | undefined;

  private readonly _downloadBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _deleteBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._deleteBS);

  private readonly _selectBS: BehaviorSubject<PublishedDeposit | undefined> = new BehaviorSubject<PublishedDeposit | undefined>(undefined);
  @Output("selectChange")
  readonly selectObs: Observable<PublishedDeposit | undefined> = ObservableUtil.asObservable(this._selectBS);

  @HostListener("click", ["$event"]) click(mouseEvent: MouseEvent): void {
    this._selectBS.next(this.publishedDeposit);
  }

  imageArchive: string | undefined;

  get publicationDate(): string | undefined {
    if (isNonEmptyArray(this.publishedDeposit.dates)) {
      return this.publishedDeposit.dates[0].date;
    }
    return undefined;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.imageArchive = `${ApiEnum.accessPublicMetadata}/${this.publishedDeposit.resId}/${ApiActionNameEnum.THUMBNAIL}`;
  }

  download(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    this._downloadBS.next(this.publishedDeposit);
  }

  delete(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    this._deleteBS.next(this.publishedDeposit);
  }
}

export type SharedArchiveTileMode = "home-tiles" | "order-draft" | "order";
export const SharedArchiveTileMode = {
  HOME_TILES: "home-tiles" as SharedArchiveTileMode,
  ORDER_DRAFT: "order-draft" as SharedArchiveTileMode,
  ORDER: "order" as SharedArchiveTileMode,
};
