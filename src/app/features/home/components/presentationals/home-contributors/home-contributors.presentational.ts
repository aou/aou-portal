/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-contributors.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {DepositTableContributorTypeEnum} from "@deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {Enums} from "@enums";
import {HomeContributorOverlayPresentational} from "@home/components/presentationals/home-contributor-overlay/home-contributor-overlay.presentational";
import {HomeHelper} from "@home/helpers/home.helper";
import {PublishedDepositContributor} from "@home/models/published-deposit.model";
import {Contributor} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  isNullOrUndefinedOrEmptyArray,
  Type,
} from "solidify-frontend";

@Component({
  selector: "aou-home-contributors",
  templateUrl: "./home-contributors.presentational.html",
  styleUrls: ["./home-contributors.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeContributorsPresentational extends SharedAbstractPresentational {
  @Input()
  contributors?: PublishedDepositContributor[];

  @Input()
  roles: Enums.Deposit.RoleContributorEnum[];

  @Input()
  includeCollaborations: boolean = false;

  @Input()
  icon: IconNameEnum;

  @Input()
  labelToTranslate: string;

  @Input()
  limitHeightShowPx: number = 65;

  @Input()
  limitHeightMinusShowPx: number = 4;

  @Input()
  showMoreButtonLabelWithNumberToTranslate: string;

  contributorOverlayComponent: Type<HomeContributorOverlayPresentational> = HomeContributorOverlayPresentational;

  constructor(private readonly _store: Store) {
    super();
  }

  typeIntoPublishedDepositContributor(contributor: any): PublishedDepositContributor {
    return contributor;
  }

  getLink(contributor: PublishedDepositContributor): Navigate {
    return HomeHelper.getContributorLink(this._store, true, contributor.cnIndividu, contributor.firstName, contributor.lastName);
  }

  get listContributors(): PublishedDepositContributor[] {
    if (isNullOrUndefinedOrEmptyArray(this.contributors) || (!this.includeCollaborations && isNullOrUndefinedOrEmptyArray(this.roles))) {
      return [];
    }
    if (this.includeCollaborations) {
      return this.contributors.filter(c => this.roles.includes(c.role) || c.type === DepositTableContributorTypeEnum.collaboration);
    } else {
      return this.contributors.filter(c => this.roles.includes(c.role));
    }
  }

  getContributorOverlayData(contributor: PublishedDepositContributor): Contributor {
    const fullName = contributor.lastName + ", " + contributor.firstName;
    return {
      firstName: contributor.firstName,
      lastName: contributor.lastName,
      fullName: fullName,
      orcid: contributor.orcid,
      cnIndividu: contributor.cnIndividu,
    };
  }
}
