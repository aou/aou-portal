/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-files.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Enums} from "@enums";
import {HomeAccessRightHelpDialog} from "@home/components/dialogs/home-access-right-help/home-access-right-help.dialog";
import {PublishedFile} from "@home/models/published-deposit.model";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  DialogUtil,
  EnumUtil,
  isNullOrUndefinedOrEmptyArray,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-files",
  templateUrl: "./home-files.presentational.html",
  styleUrls: ["./home-files.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeFilesPresentational extends SharedAbstractPresentational {
  @Input()
  files: PublishedFile[];

  @Input()
  fileTypeLevel: Enums.DocumentFileType.FileTypeLevelEnum;

  @Input()
  isOpen: boolean = true;

  @Input()
  displayHelp: boolean = false;

  @Input()
  isUserUnigeOrHug: boolean = false;

  @Input()
  listUserRoleEnum: Enums.Deposit.UserRoleEnum[] = [];

  @Input()
  isLoggedIn: boolean = false;

  private readonly _downloadBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _loginBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("loginChange")
  readonly loginObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._loginBS);

  private readonly _previewBS: BehaviorSubject<PublishedFile | undefined> = new BehaviorSubject<PublishedFile | undefined>(undefined);
  @Output("previewChange")
  readonly previewObs: Observable<PublishedFile | undefined> = ObservableUtil.asObservable(this._previewBS);

  labelToTranslate: string;

  get listFiles(): PublishedFile[] {
    if (isNullOrUndefinedOrEmptyArray(this.fileTypeLevel) || isNullOrUndefinedOrEmptyArray(this.files)) {
      return [];
    }
    this.labelToTranslate = EnumUtil.getLabel(Enums.DocumentFileType.DocumentFileTypeEnumTranslate, this.fileTypeLevel);
    return this.files.filter(c => this.fileTypeLevel === c.typeLevel);
  }

  constructor(private readonly _dialog: MatDialog) {
    super();
  }

  toggleIsOpen(): void {
    this.isOpen = !this.isOpen;
  }

  download(file: PublishedFile): void {
    this._downloadBS.next(file);
  }

  login(file: PublishedFile): void {
    this._loginBS.next(file);
  }

  preview(file: PublishedFile): void {
    this._previewBS.next(file);
  }

  trackByFn(index: number, item: PublishedFile): string {
    return item.resId;
  }

  openHelp(mouseEvent: Event): void {
    mouseEvent.stopPropagation();
    DialogUtil.open(this._dialog, HomeAccessRightHelpDialog);
  }
}
