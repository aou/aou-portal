/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-access-right-help.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Enums} from "@enums";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {MARK_AS_TRANSLATABLE} from "solidify-frontend";

@Component({
  selector: "aou-home-access-right-help-dialog",
  templateUrl: "./home-access-right-help.dialog.html",
  styleUrls: ["./home-access-right-help.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeAccessRightHelpDialog extends SharedAbstractDialog<HomeAccessRightHelpDialogData, string> {
  form: FormGroup;

  helpInfo: HelpInfo[] = [
    {
      accessLevel: Enums.DocumentFile.AccessLevelEnum.PUBLIC,
      explain: MARK_AS_TRANSLATABLE("home.detail.help.public.explain"),
    },
    {
      accessLevel: Enums.DocumentFile.AccessLevelEnum.RESTRICTED,
      explain: MARK_AS_TRANSLATABLE("home.detail.help.restricted.explain"),
    },
    {
      accessLevel: Enums.DocumentFile.AccessLevelEnum.PRIVATE,
      explain: MARK_AS_TRANSLATABLE("home.detail.help.private.explain"),
    },
  ];

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<HomeAccessRightHelpDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: HomeAccessRightHelpDialogData) {
    super(_dialogRef, data);
  }
}

export interface HomeAccessRightHelpDialogData {
}

interface HelpInfo {
  accessLevel: Enums.DocumentFile.AccessLevelEnum;
  explain: string;
}
