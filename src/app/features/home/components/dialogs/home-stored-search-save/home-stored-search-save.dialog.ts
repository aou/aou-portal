/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-stored-search-save.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  BaseFormDefinition,
  ButtonColorEnum,
  FormValidationHelper,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  PropertyName,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";
import {StoredSearch} from "@models";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";

@Component({
  selector: "aou-home-stored-search-save-dialog",
  templateUrl: "./home-stored-search-save.dialog.html",
  styleUrls: ["./home-stored-search-save.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeStoredSearchSaveDialog extends SharedAbstractDialog<HomeStoredSearchSaveData, StoredSearch | string> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(protected readonly _dialogRef: MatDialogRef<HomeStoredSearchSaveDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: HomeStoredSearchSaveData,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.form = this._fb.group({
      [this.formDefinition.text]: [isNotNullNorUndefinedNorWhiteString(this.data.inputInitialValue) ? this.data.inputInitialValue : SOLIDIFY_CONSTANTS.STRING_EMPTY, [Validators.required]],
      [this.formDefinition.usedForBibliography]: [isNotNullNorUndefined(this.data.checkboxValue) ? this.data.checkboxValue : false, []],
    });
  }

  get colorConfirm(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.primary : this.data.colorConfirm;
  }

  get colorCancel(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorCancel) ? ButtonColorEnum.primary : this.data.colorCancel;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  confirm(): void {
    const storedSearch = {
      name: this.form.get(this.formDefinition.text).value,
      usedForBibliography: this.form.get(this.formDefinition.usedForBibliography).value,
    } as StoredSearch;
    this.submit(storedSearch);
  }
}

export interface HomeStoredSearchSaveData {
  titleToTranslate: string;
  messageToTranslate?: string | undefined;
  confirmButtonToTranslate?: string | undefined;
  cancelButtonToTranslate?: string | undefined;
  colorConfirm?: ButtonColorEnum;
  colorCancel?: ButtonColorEnum;
  inputRequired?: boolean;
  inputLabelToTranslate?: string | undefined;
  inputInitialValue?: string;
  checkboxLabel?: string;
  checkboxValue?: boolean;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() text: string;
  @PropertyName() usedForBibliography: string;
}
