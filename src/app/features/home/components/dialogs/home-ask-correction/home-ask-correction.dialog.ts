/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-ask-correction.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {AppUserState} from "@app/stores/user/app-user.state";
import {PublishedDeposit} from "@home/models/published-deposit.model";
import {HomeAction} from "@home/stores/home.action";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {
  BaseFormDefinition,
  BreakpointService,
  FormValidationHelper,
  isNotNullNorUndefined,
  MemoizedUtil,
  NotificationService,
  PropertyName,
  SolidifyEmailValidator,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-home-ask-correction-dialog",
  templateUrl: "./home-ask-correction.dialog.html",
  styleUrls: ["./home-ask-correction.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeAskCorrectionDialog extends SharedAbstractDialog<HomeAskCorrectionDialogData> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  form: FormGroup;

  private _isLoadingCounter: number = 0;

  get isLoading(): boolean {
    return this._isLoadingCounter > 0;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<HomeAskCorrectionDialog>,
              protected readonly _fb: FormBuilder,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService,
              public readonly breakpointService: BreakpointService,
              @Inject(MAT_DIALOG_DATA) public readonly data: HomeAskCorrectionDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const currentUser = MemoizedUtil.selectSnapshot(this._store, AppUserState, state => state?.current);

    this.form = this._fb.group({
      [this.formDefinition.senderName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.senderEmail]: ["", [Validators.required, SolidifyEmailValidator, SolidifyValidator]],
      [this.formDefinition.messageContent]: ["", [Validators.required, SolidifyValidator]],
    });

    if (isNotNullNorUndefined(currentUser)) {
      this.form.get(this.formDefinition.senderEmail).setValue(currentUser.email);
      this.form.get(this.formDefinition.senderName).setValue(currentUser.fullName);
    }
  }

  override submit(): void {
    this._isLoadingCounter++;
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new HomeAction.AskCorrection(this.data.publishedDeposit.resId, this.form.value),
      HomeAction.AskCorrectionSuccess, result => {
        this._isLoadingCounter--;
        this._dialogRef.close(result);
      }, HomeAction.AskCorrectionFail, result => {
        this._isLoadingCounter--;
        this.close();
      }));
  }
}

export interface HomeAskCorrectionDialogData {
  publishedDeposit: PublishedDeposit;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() senderName: string;
  @PropertyName() senderEmail: string;
  @PropertyName() messageContent: string;
}

