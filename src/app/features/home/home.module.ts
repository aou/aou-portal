/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {HomeRoutingModule} from "@app/features/home/home-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {HomeGenerateBibliographyButtonContainer} from "@home/components/containers/home-generate-bibliography-button/home-generate-bibliography-button.container";
import {HomePinButtonContainer} from "@home/components/containers/home-pin-button/home-pin-button.container";
import {HomePublicationStructureListContainer} from "@home/components/containers/home-publication-structure-list/home-publication-structure-list.container";
import {HomeResearchGroupListContainer} from "@home/components/containers/home-research-group-list/home-research-group-list.container";
import {HomeStructureListContainer} from "@home/components/containers/home-structure-list/home-structure-list.container";
import {HomeAccessRightHelpDialog} from "@home/components/dialogs/home-access-right-help/home-access-right-help.dialog";
import {HomeAskCorrectionDialog} from "@home/components/dialogs/home-ask-correction/home-ask-correction.dialog";
import {HomeContactContributorDialog} from "@home/components/dialogs/home-contact-contributor/home-contact-contributor.dialog";
import {HomeStoredSearchSaveDialog} from "@home/components/dialogs/home-stored-search-save/home-stored-search-save.dialog";
import {HomeAddOrExcludeButtonPresentational} from "@home/components/presentationals/home-add-or-exclude-button/home-add-or-exclude-button.presentational";
import {HomeAdvancedSearchPresentational} from "@home/components/presentationals/home-advanced-search/home-advanced-search.presentational";
import {HomeButtonsBarPresentational} from "@home/components/presentationals/home-buttons-bar/home-buttons-bar.presentational";
import {HomeCitationPresentational} from "@home/components/presentationals/home-citation/home-citation.presentational";
import {HomeContributorOverlayPresentational} from "@home/components/presentationals/home-contributor-overlay/home-contributor-overlay.presentational";
import {HomeContributorsPresentational} from "@home/components/presentationals/home-contributors/home-contributors.presentational";
import {HomeDatePresentational} from "@home/components/presentationals/home-date/home-date.presentational";
import {HomeDepositLinePresentational} from "@home/components/presentationals/home-deposit-line/home-deposit-line.presentational";
import {HomeFileInfoPresentational} from "@home/components/presentationals/home-file-info/home-file-info.presentational";
import {HomeFilesPresentational} from "@home/components/presentationals/home-files/home-files.presentational";
import {HomeIconTextPresentational} from "@home/components/presentationals/home-icon-text/home-icon-text.presentational";
import {HomeMainPageContentPresentational} from "@home/components/presentationals/home-main-page-content/home-main-page-content.presentational";
import {HomePublishedDepositDetailPresentational} from "@home/components/presentationals/home-published-deposit-form/home-published-deposit-detail.presentational";
import {HomeSearchBarPresentational} from "@home/components/presentationals/home-search-bar/home-search-bar.presentational";
import {HomeSpecialInfoPresentational} from "@home/components/presentationals/home-special-info/home-special-info.presentational";
import {HomeBibliographyRoutable} from "@home/components/routables/home-bibliography/home-bibliography.routable";
import {HomePageRoutable} from "@home/components/routables/home-page/home-page.routable";
import {HomePinboardRoutable} from "@home/components/routables/home-pinboard/home-pinboard.routable";
import {HomePublishedDepositDetailRoutable} from "@home/components/routables/home-published-deposit-detail/home-published-deposit-detail.routable";
import {HomePublishedDepositNotFoundRoutable} from "@home/components/routables/home-published-deposit-not-found/home-published-deposit-not-found.routable";
import {HomeSearchRoutable} from "@home/components/routables/home-search/home-search.routable";
import {HomeState} from "@home/stores/home.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SolidifyFrontendSearchModule} from "solidify-frontend";
import {HomeDepositTilePresentational} from "./components/presentationals/home-deposit-tile/home-deposit-tile.presentational";

const routables = [
  HomePageRoutable,
  HomeSearchRoutable,
  HomePublishedDepositDetailRoutable,
  HomePublishedDepositNotFoundRoutable,
  HomePinboardRoutable,
  HomeBibliographyRoutable,
];
const dialogs = [
  HomeAccessRightHelpDialog,
  HomeAskCorrectionDialog,
  HomeContactContributorDialog,
  HomeStoredSearchSaveDialog,
];
const containers = [
  HomePinButtonContainer,
  HomeResearchGroupListContainer,
  HomeStructureListContainer,
  HomePublicationStructureListContainer,
  HomeGenerateBibliographyButtonContainer,
];
const presentationals = [
  HomePublishedDepositDetailPresentational,
  HomeMainPageContentPresentational,
  HomeSearchBarPresentational,
  HomeFileInfoPresentational,
  HomeButtonsBarPresentational,
  HomeAdvancedSearchPresentational,
  HomeAddOrExcludeButtonPresentational,
  HomeDepositTilePresentational,
  HomeDepositLinePresentational,
  HomeContributorsPresentational,
  HomeDatePresentational,
  HomeIconTextPresentational,
  HomeSpecialInfoPresentational,
  HomeFilesPresentational,
  HomeCitationPresentational,
  HomeContributorOverlayPresentational,
];
const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...presentationals,
    ...dialogs,
  ],
  imports: [
    SharedModule,
    HomeRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      HomeState,
    ]),
    SolidifyFrontendSearchModule,
  ],
  exports: [
    ...routables,
    HomePinButtonContainer,
    HomeResearchGroupListContainer,
    HomeStructureListContainer,
    HomePublicationStructureListContainer,
  ],
  providers: [
    ...services,
  ],
})
export class HomeModule {
}
