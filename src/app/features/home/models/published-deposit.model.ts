/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - published-deposit.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositTableContributorTypeEnum} from "@deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {
  DepositFormAcademicStructure,
  DepositFormClassification,
  DepositFormCollection,
  DepositFormContainer,
  DepositFormCorrection,
  DepositFormDate,
  DepositFormFunding,
  DepositFormGroup,
  DepositFormPage,
} from "@deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {
  BaseResource,
  MappingObject,
} from "solidify-frontend";

export interface IndexedDepositEntry {
  index?: string;
  metadata?: PublishedDeposit;
  resId?: string;
  _links?: any;
}

export interface PublishedDeposit {
  resId?: string;
  abstracts?: PublishedDepositText[];
  accessLevelByTypes?: AccessLevelByType;
  archiveId?: string;
  classifications?: DepositFormClassification[];
  collections?: DepositFormCollection[];
  container?: DepositFormContainer;
  contributors?: PublishedDepositContributor[];
  creator?: PublishedDepositCreator;
  datasets?: string[];
  dates?: DepositFormDate[];
  dblp?: string;
  diffusion?: string;
  openAccess?: string;
  principalFileAccessLevel?: string;
  doi?: string;
  embargoEndDates?: string;
  files?: PublishedFile[];
  fulltexts?: string[];
  fulltextVersions?: string[];
  fundings?: DepositFormFunding[];
  isBeforeUnige?: boolean;
  issn?: string;
  isbn?: string;
  arxiv?: string;
  keywords?: string[];
  languages?: Enums.Language.LanguageV2Enum[];
  licenses?: string[];
  pages?: DepositFormPage;
  mandator?: string;
  pmcid?: string;
  pmid?: string;
  publisherName?: string;
  publisherPlace?: string;
  publisherVersionUrl?: string;
  researchGroups?: DepositFormGroup[];
  status?: Enums.Deposit.StatusEnum;
  structures?: DepositFormAcademicStructure[];
  structuresWithParentsIds?: string[][];
  structuresWithParentsNames?: string[][];
  subsubtype?: string;
  subtype?: string;
  subtypeCode?: Enums.Deposit.DepositSubTypeEnum;
  title?: string;
  titleLang?: Enums.Language.LanguageV2Enum;
  type?: string;
  technicalInfos?: TechnicalInfos;
  award?: string;
  localNumber?: string;
  note?: string;
  originalTitle?: string;
  originalTitleLang?: Enums.Language.LanguageV2Enum;
  unigeDirectors?: string[];
  years?: number[];
  thumbnail?: PublishedFile;
  corrections?: DepositFormCorrection[];
  discipline?: string;
  edition?: string;
  urn?: string;
  year?: number;
  htmlMetaHeaders?: MappingObject<string, string | string[]>;
}

export interface TechnicalInfos {
  creationTime?: string;
  firstValidation?: string;
  lastIndexationDate?: string;
  lastStatusUpdate?: string;
  updateTime?: string;
  status?: Enums.Deposit.StatusEnum;
}

export interface AccessLevelByType {
  publishedVersion?: string[]; // Restricted / Public
  acceptedVersion?: string[]; // Restricted / Public
  submittedVersion?: string[]; // Restricted / Public
}

export interface PublishedDepositCreator {
  firstName?: string;
  fullName?: string;
  lastName?: string;
  resId?: string;
}

export interface PublishedDepositText {
  content?: string;
  language?: Enums.Language.LanguageV2Enum;
}

export interface PublishedDepositContributor {
  guid?: string;
  role?: Enums.Deposit.RoleContributorEnum;
  firstName?: string;
  lastName?: string;
  orcid?: string;
  email?: string;
  institution?: string;
  cnIndividu?: string;
  otherNames?: any;
  structure?: string;
  type?: DepositTableContributorTypeEnum;
  name?: string;
}

export interface PublishedFile extends BaseResource {
  resId?: string;
  name?: string;
  size?: number;
  type?: Enums.Deposit.FileTypeStringEnum;
  typeLevel?: Enums.DocumentFileType.FileTypeLevelEnum;
  mimeType?: string;
  accessLevel?: Enums.Deposit.AccessLevelEnum;
  embargoAccessLevel?: Enums.Deposit.AccessLevelEnum;
  embargoEndDate?: string;
  finalAccessLevel?: Enums.Deposit.AccessLevelEnum;
  isUnderEmbargo?: boolean;
  license?: string;
  licenseUrl?: string;
  licenseTitle?: string;
  label: string;
}
