/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - advanced-search.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isNumberReal,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export class AdvancedSearchHelper {
  static createExcludedKey(key: string): string {
    return key + "!";
  }

  static isExcludedKey(key: string): boolean {
    return key.endsWith("!");
  }

  static cleanExcludedKey(value: string): string {
    return this.isExcludedKey(value) ? value.substring(0, value.length - 1) : value;
  }

  static createIndexedKey(key: string, index: number): string {
    return index + SOLIDIFY_CONSTANTS.UNDERSCORE + key;
  }

  static isIndexedKey(key: string): boolean {
    const indexOfUnderscore = key.indexOf(SOLIDIFY_CONSTANTS.UNDERSCORE);
    if (indexOfUnderscore === -1) {
      return false;
    }
    const prefix = key.substring(0, indexOfUnderscore);
    const prefixNumber: number = +prefix;
    return isNumberReal(prefixNumber);
  }

  static cleanIndexedKey(value: string): string {
    if (this.isIndexedKey(value)) {
      const indexOfUnderscore = value.indexOf(SOLIDIFY_CONSTANTS.UNDERSCORE);
      return value.substring(indexOfUnderscore + 1, value.length);
    }
    return value;
  }
}
