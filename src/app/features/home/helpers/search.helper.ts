/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - search.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  NavigationExtras,
  Params,
} from "@angular/router";
import {Enums} from "@enums";
import {ViewModeTableEnum} from "@home/enums/view-mode-table.enum";
import {AdvancedSearchHelper} from "@home/helpers/advanced-search.helper";
import {PortalSearch} from "@home/models/published-deposit-search.model";
import {HomeState} from "@home/stores/home.state";
import {
  AdvancedSearchCriteria,
  StoredSearch,
  StoredSearchCriteria,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {CookieEnum} from "@shared/enums/cookie.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  CookieHelper,
  FacetHelper,
  isEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
  StringUtil,
} from "solidify-frontend";

export class SearchHelper {
  static SEARCH_ID_QUERY_PARAM: string = "searchId";
  static SEARCH_QUERY_PARAM: string = "search";
  static SEARCH_WITH_FULLTEXT_QUERY_PARAM: string = "fulltext";
  static WITH_RESTRICTED_ACCESS_MASTERS_QUERY_PARAM: string = "with-restricted-access-masters";
  static SEARCH_FACETS: string = "facets";
  static FACETS_QUERY_PARAM: string = "facets";
  static ADVANCED_SEARCH_QUERY_PARAM: string = "advanced-search";
  static PINNED_QUERY_PARAM: string = "pinned";
  static CONTRIBUTOR_QUERY_PARAM: string = "contributor";
  static VIEW_QUERY_PARAM: string = "view";
  static VALUES_SEPARATOR: string = ";";
  static QUERY_PARAM_SEPARATOR: string = "&";
  static QUERY_PARAM_VALUE_AFFECTION: string = "=";
  static GLOBAL_SEARCH: string = "globalSearch";
  static GLOBAL_SEARCH_WITH_FULLTEXT: string = "globalSearchWithFulltext";

  static generatePortalSearchFromStore(store: Store): PortalSearch {
    return {
      search: MemoizedUtil.selectSnapshot(store, HomeState, state => state.search),
      searchWithFulltext: MemoizedUtil.selectSnapshot(store, HomeState, state => state.searchWithFulltext),
      withRestrictedAccessMasters: MemoizedUtil.selectSnapshot(store, HomeState, state => state.withRestrictedAccessMasters),
      facetsSelected: MemoizedUtil.selectSnapshot(store, HomeState, state => state.facetsSelected),
      advancedSearch: MemoizedUtil.selectSnapshot(store, HomeState, state => state.advancedSearch),
      viewTabMode: MemoizedUtil.selectSnapshot(store, HomeState, state => state.viewModeTableEnum),
    };
  }

  static generateSearchPageNavigateAction(portalSearch: PortalSearch): Navigate {
    const completeAdvancedSearchString = this._adaptMappingObjectToUrlQueryString(portalSearch?.advancedSearch);
    const completeFacetsString = this._adaptMappingObjectToUrlQueryString(portalSearch?.facetsSelected);

    const navigationExtras: NavigationExtras = {
      [this.VIEW_QUERY_PARAM]: isNullOrUndefined(portalSearch?.viewTabMode) ? ViewModeTableEnum.list : portalSearch.viewTabMode,
    };
    if (isNotNullNorUndefinedNorWhiteString(portalSearch?.search)) {
      navigationExtras[this.SEARCH_QUERY_PARAM] = SsrUtil.window?.encodeURIComponent(portalSearch.search);
    }
    if (isTrue(portalSearch?.searchWithFulltext)) {
      navigationExtras[this.SEARCH_WITH_FULLTEXT_QUERY_PARAM] = SOLIDIFY_CONSTANTS.STRING_TRUE;
    }
    const withAccessMasters = isTrue(portalSearch?.withRestrictedAccessMasters) ||
      (isNullOrUndefined(portalSearch?.withRestrictedAccessMasters) && CookieHelper.getItem(CookieEnum.publicationWithRestrictedAccessMasters) === SOLIDIFY_CONSTANTS.STRING_TRUE);
    navigationExtras[this.WITH_RESTRICTED_ACCESS_MASTERS_QUERY_PARAM] = withAccessMasters ? SOLIDIFY_CONSTANTS.STRING_TRUE : SOLIDIFY_CONSTANTS.STRING_FALSE;
    if (isNotNullNorUndefined(completeAdvancedSearchString)) {
      navigationExtras[this.ADVANCED_SEARCH_QUERY_PARAM] = completeAdvancedSearchString;
    }
    if (isNotNullNorUndefined(completeFacetsString)) {
      navigationExtras[this.FACETS_QUERY_PARAM] = completeFacetsString;
    }
    if (isNotNullNorUndefined(portalSearch.searchId)) {
      navigationExtras[this.SEARCH_ID_QUERY_PARAM] = portalSearch.searchId;
    }
    return new Navigate([RoutesEnum.homeSearch], navigationExtras);
  }

  private static _adaptMappingObjectToUrlQueryString(mappingObject: MappingObject<string | Enums.Facet.Name, string[]>): string | undefined {
    const queryParams = {} as MappingObject<string, string>;
    if (isNotNullNorUndefined(mappingObject)) {
      MappingObjectUtil.forEach(mappingObject, ((value, key) => {
        const valueString = value.map(v => SsrUtil.window?.encodeURIComponent(v)).join(this.VALUES_SEPARATOR);
        MappingObjectUtil.set(queryParams, key as string, valueString);
      }));
    }
    let completeAdvancedSearchString = undefined;
    MappingObjectUtil.forEach(queryParams, ((value: string, key: string) => {
      if (isNotNullNorUndefined(completeAdvancedSearchString)) {
        completeAdvancedSearchString = completeAdvancedSearchString + this.QUERY_PARAM_SEPARATOR;
      } else {
        completeAdvancedSearchString = StringUtil.stringEmpty;
      }
      completeAdvancedSearchString = completeAdvancedSearchString + key + this.QUERY_PARAM_VALUE_AFFECTION + value;
    }));
    return completeAdvancedSearchString;
  }

  /**
   * Allow to convert url string to portal search
   *
   * @param params of url
   * @return PortalSearch
   */
  static adaptUrlStringToPortalSearch(params: Params): PortalSearch {
    const search = isNullOrUndefinedOrWhiteString(params[this.SEARCH_QUERY_PARAM]) ? StringUtil.stringEmpty : SsrUtil.window?.decodeURIComponent(params[this.SEARCH_QUERY_PARAM]);
    const searchWithFulltext = params[this.SEARCH_WITH_FULLTEXT_QUERY_PARAM] === SOLIDIFY_CONSTANTS.STRING_TRUE;

    const withRestrictedAccessMasters = params[this.WITH_RESTRICTED_ACCESS_MASTERS_QUERY_PARAM] === SOLIDIFY_CONSTANTS.STRING_TRUE;
    const facetsSelected: MappingObject<Enums.Facet.Name, string[]> = this._adaptQueryParamStringToMappingObject(params[this.FACETS_QUERY_PARAM]) as MappingObject<Enums.Facet.Name, string[]>;
    const advancedSearch: MappingObject<string, string[]> = this._adaptQueryParamStringToMappingObject(params[this.ADVANCED_SEARCH_QUERY_PARAM]);
    const viewTabMode = params[this.VIEW_QUERY_PARAM];
    const searchId = params[this.SEARCH_ID_QUERY_PARAM];

    return {
      search: search,
      searchWithFulltext: searchWithFulltext,
      withRestrictedAccessMasters: withRestrictedAccessMasters,
      facetsSelected: facetsSelected,
      advancedSearch: advancedSearch,
      viewTabMode: viewTabMode,
      searchId: searchId,
    };
  }

  private static _adaptQueryParamStringToMappingObject(queryParam: string): MappingObject<Enums.Facet.Name | string, string[]> {
    const mappingObject = {} as MappingObject<Enums.Facet.Name | string, string[]>;

    if (isNotNullNorUndefined(queryParam)) {
      const listParams = queryParam.split(this.QUERY_PARAM_SEPARATOR);
      listParams.forEach(p => {
        const splitParam = p.split(this.QUERY_PARAM_VALUE_AFFECTION);
        const decodedValue = SsrUtil.window?.decodeURIComponent(splitParam[1]);
        if (splitParam.length > 1) {
          MappingObjectUtil.set(mappingObject, splitParam[0], decodedValue.split(this.VALUES_SEPARATOR));
        }
      });
    }

    return mappingObject;
  }

  private static _adaptAdvancedSearchFacet(criteria: AdvancedSearchCriteria, facet: StoredSearchCriteria, value: string[]): void {
    if (criteria?.type === Enums.Criteria.TypeEnum.INTERVAL_YEAR) {
      const years = value[0];
      const res = years.split(SOLIDIFY_CONSTANTS.DASH);
      if (isNotNullNorUndefinedNorWhiteString(res[0])) {
        facet.lowerValue = res[0];
      }
      if (isNotNullNorUndefinedNorWhiteString(res[1])) {
        facet.upperValue = res[1];
      }
      facet.type = Enums.StoredSearch.Type.RANGE;
      return;
    } else if (criteria?.type === Enums.Criteria.TypeEnum.FREE_TEXT_MATCH) {
      facet.type = Enums.StoredSearch.Type.MATCH;
    } else if (criteria?.type === Enums.Criteria.TypeEnum.FREE_TEXT_WILDCARD) {
      facet.type = Enums.StoredSearch.Type.WILDCARD;
    }

    if (value.length === 1) {
      facet.value = value[0];
    } else {
      facet.terms = value;
    }
  }

  /**
   * Allow to convert portal search to backend search object
   *
   * @param portalSearch
   * @param listAdvancedSearchCriteria
   * @return StoredSearchCriteria list
   */
  static adaptPortalSearchToStoredSearchCriteriaList(portalSearch: PortalSearch, listAdvancedSearchCriteria: AdvancedSearchCriteria[]): StoredSearchCriteria[] {
    const StoredSearchCriteriaList: StoredSearchCriteria[] = [];
    StoredSearchCriteriaList.push(...SearchHelper._adaptSearchToStoredSearchCriteria(portalSearch.search, portalSearch.searchWithFulltext));
    StoredSearchCriteriaList.push(...SearchHelper._adaptAdvancedSearchToStoredSearchCriteria(portalSearch.advancedSearch, listAdvancedSearchCriteria));
    StoredSearchCriteriaList.push(...SearchHelper._adaptFacetsToStoredSearchCriteria(portalSearch.facetsSelected));
    return StoredSearchCriteriaList;
  }

  private static _adaptSearchToStoredSearchCriteria(search: string, withFulltext: boolean): StoredSearchCriteria[] {
    if (isNotNullNorUndefinedNorWhiteString(search)) {
      return [{
        value: search,
        field: withFulltext ? this.GLOBAL_SEARCH_WITH_FULLTEXT : this.GLOBAL_SEARCH,
        source: Enums.StoredSearch.CriteriaSource.SEARCH,
      }];
    }
    return [];
  }

  private static _adaptAdvancedSearchToStoredSearchCriteria(advancedSearch: MappingObject<string, string[]>, listCriteria: AdvancedSearchCriteria[]): StoredSearchCriteria[] {
    const facetsSelectedForBackend: StoredSearchCriteria[] = [];

    MappingObjectUtil.forEach(advancedSearch, (value, key) => {
      if (isNullOrUndefined(value) || isEmptyArray(value)) {
        return;
      }
      const keyWithoutNumber = AdvancedSearchHelper.cleanIndexedKey(key);
      const facet = {
        field: AdvancedSearchHelper.cleanExcludedKey(keyWithoutNumber),
        type: Enums.StoredSearch.Type.TERM,
        booleanClauseType: AdvancedSearchHelper.isExcludedKey(keyWithoutNumber) ? Enums.StoredSearch.BooleanClauseType.MUST_NOT : Enums.StoredSearch.BooleanClauseType.MUST,
        source: Enums.StoredSearch.CriteriaSource.ADVANCED_SEARCH,
      } as StoredSearchCriteria;
      const criteria = listCriteria.find(c => c.alias === facet.field);
      this._adaptAdvancedSearchFacet(criteria, facet, value);
      facetsSelectedForBackend.push(facet);
    });
    return facetsSelectedForBackend;
  }

  private static _adaptFacetsToStoredSearchCriteria(facetsSelected: MappingObject<Enums.Facet.Name, string[]>): StoredSearchCriteria[] {
    const facetsSelectedForBackend: StoredSearchCriteria[] = [];

    MappingObjectUtil.forEach(facetsSelected, (value, key) => {
      if (isNullOrUndefined(value) || isEmptyArray(value)) {
        return;
      }
      value.forEach(v => {
        facetsSelectedForBackend.push({
          field: key,
          type: Enums.StoredSearch.Type.TERM,
          value: FacetHelper.cleanExcludedValue(v),
          booleanClauseType: FacetHelper.isExcludedValue(v) ? Enums.StoredSearch.BooleanClauseType.MUST_NOT : Enums.StoredSearch.BooleanClauseType.MUST,
          source: Enums.StoredSearch.CriteriaSource.FACET,
        });
      });
    });
    return facetsSelectedForBackend;
  }

  /**
   * Allow to convert backend search to portal search object
   *
   * @param backendSearch
   * @return portalSearch
   */
  static adaptSearchToPortalSearch(backendSearch: StoredSearch): PortalSearch {
    const portalSearch = {} as PortalSearch;

    const mainStoredSearchCriteria = backendSearch.criteria.find(c => c.source === Enums.StoredSearch.CriteriaSource.SEARCH);
    if (isNotNullNorUndefined(mainStoredSearchCriteria)) {
      portalSearch.search = mainStoredSearchCriteria.value;
    }

    const advancedStoredSearchCriteria = backendSearch.criteria.filter(c => c.source === Enums.StoredSearch.CriteriaSource.ADVANCED_SEARCH);
    portalSearch.advancedSearch = SearchHelper._adaptStoredSearchCriteriaToQuerySearch(advancedStoredSearchCriteria);

    const facetsStoredSearchCriteria = backendSearch.criteria.filter(c => c.source === Enums.StoredSearch.CriteriaSource.FACET);
    portalSearch.facetsSelected = SearchHelper._adaptStoredSearchCriteriaToQuerySearch(facetsStoredSearchCriteria) as MappingObject<Enums.Facet.Name, string[]>;

    portalSearch.withRestrictedAccessMasters = backendSearch.withRestrictedAccessMasters;

    return portalSearch;
  }

  /**
   * Allow to convert list of search conditions into a mapping object for portal search
   *
   * @param listStoredSearchCriteria
   * @return mappingObject for portal search and url queryable
   */
  private static _adaptStoredSearchCriteriaToQuerySearch(listStoredSearchCriteria: StoredSearchCriteria[]): MappingObject<string | Enums.Facet.Name, string[]> {
    const map = {} as MappingObject<string | Enums.Facet.Name, string[]>;

    listStoredSearchCriteria.forEach((storedSearchCriteria, index) => {
      const mustNot = storedSearchCriteria.booleanClauseType === Enums.StoredSearch.BooleanClauseType.MUST_NOT;
      const field = mustNot && storedSearchCriteria.source === Enums.StoredSearch.CriteriaSource.ADVANCED_SEARCH ? AdvancedSearchHelper.createExcludedKey(storedSearchCriteria.field) : storedSearchCriteria.field;
      const value = mustNot && storedSearchCriteria.source === Enums.StoredSearch.CriteriaSource.FACET ? FacetHelper.createExcludedValue(storedSearchCriteria.value) : storedSearchCriteria.value;
      let values = [];

      if (storedSearchCriteria.source === Enums.StoredSearch.CriteriaSource.FACET) {
        if (MappingObjectUtil.has(map, field)) {
          values = MappingObjectUtil.get(map, field);
        }
        values.push(value);
        MappingObjectUtil.set(map, field, values);
      } else if (storedSearchCriteria.source === Enums.StoredSearch.CriteriaSource.ADVANCED_SEARCH) {
        values = isNotNullNorUndefined(value) ? [value] : storedSearchCriteria.terms;
        if (storedSearchCriteria.type === Enums.StoredSearch.Type.RANGE) {
          values = [storedSearchCriteria.lowerValue + SOLIDIFY_CONSTANTS.DASH + storedSearchCriteria.upperValue];
        }
        MappingObjectUtil.set(map, AdvancedSearchHelper.createIndexedKey(field, index), values);
      }
    });

    return map;
  }
}
