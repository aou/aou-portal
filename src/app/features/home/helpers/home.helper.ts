/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AppSearchState} from "@app/stores/search/app-search.state";
import {AdvancedSearchHelper} from "@home/helpers/advanced-search.helper";
import {SearchHelper} from "@home/helpers/search.helper";
import {PortalSearch} from "@home/models/published-deposit-search.model";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
} from "solidify-frontend";

export class HomeHelper {
  static getContributorLink(store: Store, preferContributorPage: boolean, cnIndividu: string, firstName: string, lastName: string): Navigate {
    if (isNotNullNorUndefinedNorWhiteString(cnIndividu)) {
      if (preferContributorPage) {
        return this.getContributorPersonalPageLink(cnIndividu);
      }
      const navigate = this.getUnigeContributorsPublicationsSearchPageLink(store, cnIndividu);
      if (isNotNullNorUndefined(navigate)) {
        return navigate;
      }
    }
    return this.getContributorsPublicationsSearchPageLink(store, firstName, lastName);
  }

  static getContributorPersonalPageLink(cnIndividu: string): Navigate {
    if (isNullOrUndefinedOrWhiteString(cnIndividu)) {
      return;
    }
    return new Navigate([RoutesEnum.contributor + "/" + cnIndividu]);
  }

  static getUnigeContributorsPublicationsSearchPageLink(store: Store, cnIndividu: string): Navigate {
    const advancedSearch = {} as MappingObject<string, string[]>;
    const unigeContributorCriteria = MemoizedUtil.selectSnapshot(store, AppSearchState, state => state.unigeContributorCriteria);
    const unigeDirectorCriteria = MemoizedUtil.selectSnapshot(store, AppSearchState, state => state.unigeDirectorCriteria);
    if (isNullOrUndefinedOrWhiteString(unigeContributorCriteria?.alias)) {
      // eslint-disable-next-line no-console
      console.warn("Unable to get UNIGE contributor's publication search page link because 'unigeContributorCriteria' is missing form 'AppSearchState'");
      return;
    }
    if (isNullOrUndefinedOrWhiteString(cnIndividu)) {
      return;
    }
    MappingObjectUtil.set(advancedSearch, AdvancedSearchHelper.createIndexedKey(unigeContributorCriteria.alias, 0), [cnIndividu]);
    if (isNotNullNorUndefined(unigeDirectorCriteria)) {
      MappingObjectUtil.set(advancedSearch, AdvancedSearchHelper.createExcludedKey(AdvancedSearchHelper.createIndexedKey(unigeDirectorCriteria.alias, 1)), [cnIndividu]);
    } else {
      // eslint-disable-next-line no-console
      console.warn("UNIGE contributor's publication search page link partially generated because 'unigeDirectorCriteria' is missing form 'AppSearchState'");
    }
    const portalSearch = {
      advancedSearch: advancedSearch,
    } as PortalSearch;
    return SearchHelper.generateSearchPageNavigateAction(portalSearch);
  }

  static getUnigeDirectorPublicationsSearchPageLink(store: Store, cnIndividu: string): Navigate {
    const advancedSearch = {} as MappingObject<string, string[]>;
    const unigeDirectorCriteria = MemoizedUtil.selectSnapshot(store, AppSearchState, state => state.unigeDirectorCriteria);
    if (isNullOrUndefinedOrWhiteString(cnIndividu)) {
      return;
    }
    if (isNotNullNorUndefined(unigeDirectorCriteria)) {
      MappingObjectUtil.set(advancedSearch, AdvancedSearchHelper.createIndexedKey(unigeDirectorCriteria.alias, 0), [cnIndividu]);
    } else {
      // eslint-disable-next-line no-console
      console.warn("Unable to get UNIGE director's publication search page link because 'unigeDirectorCriteria' is missing form 'AppSearchState'");
      return;
    }
    const portalSearch = {
      advancedSearch: advancedSearch,
      searchWithFulltext: true,
      withRestrictedAccessMasters: true,
    } as PortalSearch;
    return SearchHelper.generateSearchPageNavigateAction(portalSearch);
  }

  static getContributorsPublicationsSearchPageLink(store: Store, firstName: string, lastName: string): Navigate {
    const advancedSearch = {} as MappingObject<string, string[]>;
    const allContributorCriteria = MemoizedUtil.selectSnapshot(store, AppSearchState, state => state.allContributorCriteria);
    if (isNullOrUndefinedOrWhiteString(allContributorCriteria?.alias)) {
      // eslint-disable-next-line no-console
      console.warn("Unable to get contributor's publication search page link because 'allContributorCriteria' is missing form 'AppSearchState'");
      return;
    }
    if (isNullOrUndefinedOrWhiteString(firstName)
      || isNullOrUndefinedOrWhiteString(lastName)) {
      return;
    }
    MappingObjectUtil.set(advancedSearch, AdvancedSearchHelper.createIndexedKey(allContributorCriteria.alias, 0), [lastName + ", " + firstName]);
    const portalSearch = {
      advancedSearch: advancedSearch,
    } as PortalSearch;
    return SearchHelper.generateSearchPageNavigateAction(portalSearch);
  }
}
