/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - archive-metadata-packages.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export interface ArchiveMetadataPackages {
  resId?: string;
  index: string;
  type: string;
  metadata: ArchiveMetadataPackages.Metadata;
}

export enum MetadataPackagesEnum {
  aipDispositionApproval = "aip-disposition-approval",
  aipOrganizationalUnit = "aip-organizational-unit",
  aipOrganizationalUnitName = "aip-organizational-unit-name",
  aipThumbnail = "aip-thumbnail",
  aipDataTag = "aip-data-tag",
  aipRetention = "aip-retention",
  aipRetentionEnd = "aip-retention-end",
  aipSize = "aip-size",
  aipFileNumber = "aip-file-number",
  aipContainer = "aip-container",
  metadataVersion = "metadata-version",
  aipAccessLevel = "aip-access-level",
  aipUnit = "aip-unit",
  creation = "creation",
}

export namespace ArchiveMetadataPackages {

  export interface Metadata {
    [MetadataPackagesEnum.aipThumbnail]: string;
    [MetadataPackagesEnum.aipRetention]: number;
    [MetadataPackagesEnum.aipRetentionEnd]: string;
    [MetadataPackagesEnum.metadataVersion]: string;
    [MetadataPackagesEnum.aipContainer]: string;
    [MetadataPackagesEnum.aipSize]: number;
    // [MetadataPackagesEnum.aipDataTag]: Enums.Deposit.DataSensitivityEnum;
    type: string;
    packages: Packages;
    agents: Agent[];
    [MetadataPackagesEnum.aipDispositionApproval]: string;
    [MetadataPackagesEnum.aipOrganizationalUnit]: string;
    [MetadataPackagesEnum.aipFileNumber]: number;
    [MetadataPackagesEnum.aipUnit]: string;
    [MetadataPackagesEnum.aipOrganizationalUnitName]: string;
    // [MetadataPackagesEnum.aipAccessLevel]: Enums.Deposit.AccessEnum;
    [MetadataPackagesEnum.creation]: string;
  }

  export interface Packages {
    aip: Package;
    sip: Package;
    deposits: Deposit;
  }

  export interface Package {
    name: string;
    id: string;
  }

  export interface Deposit extends Package {
    events: Events;
  }

  export interface Events {
    appraisal: EventsDate;
    creation: EventsDate;
  }

  export interface EventsDate {
    date: string;
  }

  export interface Agent {
    name: string;
    type: string;
    version: string;
  }
}

