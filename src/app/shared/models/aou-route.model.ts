/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - aou-route.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Type} from "@angular/core";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {
  CombinedGuardService,
  SolidifyRoute,
  SolidifyRouteData,
} from "solidify-frontend";

export declare type AouRoutes = AouRoute[];

export class AouRoute extends SolidifyRoute {
  data?: AouRouteData;
  children?: AouRoutes;
  canActivate?: Type<CombinedGuardService>[];
}

export class AouRouteData extends SolidifyRouteData {
  permission?: ApplicationRolePermissionEnum;
}
