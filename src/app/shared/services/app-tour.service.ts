/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app-tour.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import {StepTourSectionNameEnum} from "@shared/enums/step-tour-section-name.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {
  IStepOption,
  TourService,
} from "ngx-ui-tour-md-menu";
import {
  AbstractAppTourService,
  DefaultSolidifyEnvironment,
  ENVIRONMENT,
  ITourSection,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class AppTourService extends AbstractAppTourService {
  constructor(protected readonly _store: Store,
              protected readonly _matDialog: MatDialog,
              protected readonly _tourService: TourService,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_store, _matDialog, _tourService, tourSteps, _labelTranslate, _environment);
  }
}

const mainTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.mainMenuDeposit,
    content: MARK_AS_TRANSLATABLE("tour.main.menuDeposit.content"),
  },
  {
    anchorId: TourEnum.mainMenuDepositToValidate,
    content: MARK_AS_TRANSLATABLE("tour.main.menuDepositToValidate.content"),
  },
];

const tourSteps: ITourSection[] = [
  {
    key: StepTourSectionNameEnum.main,
    stepTour: mainTourSteps,
  },
];
