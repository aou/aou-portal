/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - store-route.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {AbstractStoreRouteService} from "solidify-frontend";
import {StateEnum} from "../enums/state.enum";

@Injectable({
  providedIn: "root",
})
export class StoreRouteService extends AbstractStoreRouteService {
  constructor() {
    super(environment);
  }

  protected _getCreateRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.deposit) {
      return RoutesEnum.depositCreate;
    }
    if (state === StateEnum.admin_researchGroup) {
      return RoutesEnum.adminResearchGroupCreate;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicenseCreate;
    }
    if (state === StateEnum.admin_globalBanner) {
      return RoutesEnum.adminGlobalBannerCreate;
    }
    if (state === StateEnum.admin_structure) {
      return RoutesEnum.adminStructureCreate;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUserCreate;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRoleCreate;
    }
    if (state === StateEnum.admin_indexFieldAlias) {
      return RoutesEnum.adminIndexFieldAliasCreate;
    }
    return undefined;
  }

  protected _getDetailRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.deposit) {
      return RoutesEnum.depositDetail;
    }
    if (state === StateEnum.notification) {
      return RoutesEnum.notificationDetail;
    }
    if (state === StateEnum.admin_researchGroup) {
      return RoutesEnum.adminResearchGroupDetail;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicenseDetail;
    }
    if (state === StateEnum.admin_globalBanner) {
      return RoutesEnum.adminGlobalBannerDetail;
    }
    if (state === StateEnum.admin_applicationEvent) {
      return RoutesEnum.adminApplicationEventDetail;
    }
    if (state === StateEnum.admin_structure) {
      return RoutesEnum.adminStructureDetail;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUserDetail;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRoleDetail;
    }
    if (state === StateEnum.admin_indexFieldAlias) {
      return RoutesEnum.adminIndexFieldAliasDetail;
    }
    return undefined;
  }

  protected _getRootRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.deposit) {
      return RoutesEnum.deposit;
    }
    if (state === StateEnum.notification) {
      return RoutesEnum.notification;
    }
    if (state === StateEnum.admin_researchGroup) {
      return RoutesEnum.adminResearchGroup;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicense;
    }
    if (state === StateEnum.admin_globalBanner) {
      return RoutesEnum.adminGlobalBanner;
    }
    if (state === StateEnum.admin_applicationEvent) {
      return RoutesEnum.adminApplicationEvent;
    }
    if (state === StateEnum.admin_structure) {
      return RoutesEnum.adminStructure;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUser;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRole;
    }
    if (state === StateEnum.admin_indexFieldAlias) {
      return RoutesEnum.adminIndexFieldAlias;
    }
    return undefined;
  }
}
