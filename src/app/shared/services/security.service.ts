/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - security.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {AppPersonValidationRightStructureState} from "@app/stores/person/validation-right/app-person-validation-right-structure.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositAction} from "@deposit/stores/deposit.action";
import {environment} from "@environments/environment";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DepositRoleHelper} from "@shared/helpers/deposit-role.helper";
import {
  Observable,
  of,
} from "rxjs";
import {
  filter,
  map,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  SolidifySecurityService,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class SecurityService extends SolidifySecurityService {
  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions) {
    super(_store, _actions$, environment);
  }

  canAccessToValidateDepositMenuObs(): Observable<boolean> {
    if (this.isRootOrAdmin()) {
      return of(true);
    }
    return MemoizedUtil.select(this._store, AppPersonValidationRightStructureState, state => state.selected).pipe(
      filter(selected => isNotNullNorUndefined(selected)),
      map(selected => selected.length > 0),
    );
  }

  canSubmitDepositObs(depositId: string): Observable<boolean> {
    if (this.isRootOrAdmin()) {
      return of(true);
    }
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new DepositAction.ListPublicationUserRoles(depositId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.ListPublicationUserRolesSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.ListPublicationUserRolesFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          const listUserRoleEnum = (result.listActionSuccess[0] as DepositAction.ListPublicationUserRolesSuccess)?.listUserRoleEnum;
          return DepositRoleHelper.isCreatorOrContributorOrValidator(listUserRoleEnum);
        }
        return false;
      }),
    );
  }

  hasOrcidToken(): boolean {
    const currentUser = MemoizedUtil.currentSnapshot(this._store, AppUserState);
    return isNotNullNorUndefined(currentUser?.person?.orcidToken);
  }
}
