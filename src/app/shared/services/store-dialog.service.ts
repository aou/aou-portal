/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - store-dialog.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminGlobalBannerActionNameSpace} from "@admin/global-banner/stores/admin-global-banner.action";
import {adminIndexFieldAliasActionNameSpace} from "@admin/index-field-alias/stores/admin-index-field-alias.action";
import {adminLicenseActionNameSpace} from "@admin/license/stores/admin-license.action";
import {adminResearchGroupActionNameSpace} from "@admin/research-group/stores/admin-research-group.action";
import {adminRoleActionNameSpace} from "@admin/role/stores/admin-role.action";
import {adminStructureActionNameSpace} from "@admin/structure/stores/admin-structure.action";
import {adminUserActionNameSpace} from "@admin/user/stores/admin-user.action";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {depositActionNameSpace} from "@app/features/deposit/stores/deposit.action";
import {environment} from "@environments/environment";
import {
  AbstractStoreDialogService,
  DeleteDialogData,
  isNotNullNorUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";
import {StateEnum} from "../enums/state.enum";

@Injectable({
  providedIn: "root",
})
export class StoreDialogService extends AbstractStoreDialogService {
  constructor(@Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface) {
    super(environment, _labelTranslate);
  }

  protected _deleteDataInternal(state: StateEnum): DeleteDialogData | undefined {
    const sharedDeleteDialogData = {} as DeleteDialogData;

    if (state === StateEnum.deposit) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("publication.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = depositActionNameSpace;
    }
    if (state === StateEnum.admin_researchGroup) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.researchGroup.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminResearchGroupActionNameSpace;
    }
    if (state === StateEnum.admin_license) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.license.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminLicenseActionNameSpace;
    }
    if (state === StateEnum.admin_globalBanner) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.globalBanner.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminGlobalBannerActionNameSpace;
    }
    if (state === StateEnum.admin_structure) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.structure.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminStructureActionNameSpace;
    }
    if (state === StateEnum.admin_user) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.user.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminUserActionNameSpace;
    }
    if (state === StateEnum.admin_role) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.roles.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminRoleActionNameSpace;
    }
    if (state === StateEnum.admin_indexFieldAlias) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.indexFieldAlias.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminIndexFieldAliasActionNameSpace;
    }
    if (isNotNullNorUndefined(sharedDeleteDialogData.message)) {
      return sharedDeleteDialogData;
    }
    return undefined;
  }

}
