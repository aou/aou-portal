/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DragDropModule} from "@angular/cdk/drag-drop";
import {LayoutModule} from "@angular/cdk/layout";
import {TextFieldModule} from "@angular/cdk/text-field";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogConfig,
} from "@angular/material/dialog";
import {RouterModule} from "@angular/router";
import {MaterialModule} from "@app/material.module";
import {SharedPersonState} from "@app/shared/stores/person/shared-person.state";
import {SharedState} from "@app/shared/stores/shared.state";
import {
  FaIconLibrary,
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";
import {fab} from "@fortawesome/free-brands-svg-icons";
import {fas} from "@fortawesome/free-solid-svg-icons";
import {FormlyModule} from "@ngx-formly/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedAdditionalInformationPanelContainer} from "@shared/components/containers/shared-additional-information-panel/shared-additional-information-panel.container";
import {SharedTablePersonStructureSubTypeContainer} from "@shared/components/containers/shared-table-person-structure-sub-type/shared-table-person-structure-sub-type.container";
import {SharedUserGuideSidebarContainer} from "@shared/components/containers/shared-user-guide-sidebar/shared-user-guide-sidebar.container";
import {SharedValidatorGuideSidebarContainer} from "@shared/components/containers/shared-validator-guide-sidebar/shared-validator-guide-sidebar.container";
import {SharedAccessLevelWithEmbargoPresentational} from "@shared/components/presentationals/shared-access-level-with-embargo/shared-access-level-with-embargo.presentational";
import {SharedAccessLevelPresentational} from "@shared/components/presentationals/shared-access-level/shared-access-level.presentational";
import {SharedContactPresentational} from "@shared/components/presentationals/shared-contact/shared-contact.presentational";
import {SharedContributorsListPresentational} from "@shared/components/presentationals/shared-contributors-list/shared-contributors-list.presentational";
import {SharedFileTypePresentational} from "@shared/components/presentationals/shared-file-type/shared-file-type.presentational";
import {SharedFileVersionPresentational} from "@shared/components/presentationals/shared-file-version/shared-file-version.presentational";
import {SharedLanguageSelectPresentational} from "@shared/components/presentationals/shared-language-select/shared-language-select.presentational";
import {SharedNotificationInputPresentational} from "@shared/components/presentationals/shared-notification-input/shared-notification-input.presentational";
import {SharedOpenAccessPresentational} from "@shared/components/presentationals/shared-open-access/shared-open-access.presentational";
import {SharedPrincipalFileAccessLevelPresentational} from "@shared/components/presentationals/shared-principal-file-access-level/shared-principal-file-access-level.presentational";
import {SharedStepperPresentational} from "@shared/components/presentationals/shared-stepper/shared-stepper.presentational";
import {SharedStructureInputPresentational} from "@shared/components/presentationals/shared-structure-input/shared-structure-input.presentational";
import {SharedTocPresentational} from "@shared/components/presentationals/shared-toc/shared-toc.presentational";
import {SharedAutocompleteInfiniteScrollDirective} from "@shared/directives/shared-autocomplete-infinite-scroll/shared-autocomplete-infinite-scroll.directive";
import {SafeHtmlPipe} from "@shared/pipes/safe-html.pipe";
import {SharedArchiveState} from "@shared/stores/archive/shared-archive.state";
import {SharedDepositSubtypeState} from "@shared/stores/deposit-subtype/shared-deposit-subtype.state";
import {SharedDepositTypeState} from "@shared/stores/deposit-type/shared-deposit-type.state";
import {SharedDepositTypeSubtypeState} from "@shared/stores/deposit-type/subtype/shared-deposit-type-subtype.state";
import {SharedDepositSubSubtypeState} from "@shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.state";
import {SharedDepositState} from "@shared/stores/deposit/shared-deposit.state";
import {SharedDocumentFileTypeState} from "@shared/stores/document-file-type/shared-document-file-type.state";
import {SharedExternalDataJournalTitleState} from "@shared/stores/external-data/external-data-journal/shared-external-data-journal-title.state";
import {SharedExternalDataOpenAireState} from "@shared/stores/external-data/external-data-open-aire/shared-external-data-open-aire.state";
import {SharedExternalDataPersonState} from "@shared/stores/external-data/external-data-person/shared-external-data-person.state";
import {SharedExternalDataState} from "@shared/stores/external-data/shared-external-data.state";
import {SharedGlobalBannerState} from "@shared/stores/global-banner/shared-global-banner.state";
import {SharedIndexFieldAliasState} from "@shared/stores/index-field-alias/shared-index-field-alias.state";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {SharedLicenseGroupState} from "@shared/stores/license-group/shared-license-group.state";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {SharedPublicContributorState} from "@shared/stores/public-contributor/shared-public-contributor.state";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {HighlightModule} from "ngx-highlightjs";
import {ImageCropperComponent} from "ngx-image-cropper";
import {TourMatMenuModule} from "ngx-ui-tour-md-menu";
import {
  SharedOaiMetadataPrefixState,
  SharedOaiSetState,
  SolidifyFrontendApplicationModule,
} from "solidify-frontend";
import {SharedStructureTreePresentational} from "./components/presentationals/shared-structure-tree/shared-structure-tree.presentational";
import {SharedContributorState} from "./stores/contributor/shared-contributor.state";

const routables = [];
const containers = [
  SharedUserGuideSidebarContainer,
  SharedValidatorGuideSidebarContainer,
  SharedAdditionalInformationPanelContainer,
  SharedTablePersonStructureSubTypeContainer,
];
const dialogs = [];
const contents = [];
const presentationals = [
  SharedPrincipalFileAccessLevelPresentational,
  SharedOpenAccessPresentational,
  SharedAccessLevelPresentational,
  SharedAccessLevelWithEmbargoPresentational,
  SharedTocPresentational,
  SharedStructureTreePresentational,
  SharedStructureInputPresentational,
  SharedStepperPresentational,
  SharedFileVersionPresentational,
  SharedFileTypePresentational,
  SharedNotificationInputPresentational,
  SharedContactPresentational,
  SharedLanguageSelectPresentational,
  SharedContributorsListPresentational,
];
const directives = [
  SharedAutocompleteInfiniteScrollDirective,
];
const pipes = [
  SafeHtmlPipe,
];
const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  FormlyModule,
  MaterialModule,
  FontAwesomeModule,
  TranslateModule,
  SolidifyFrontendApplicationModule,
  TextFieldModule,
  LayoutModule,
  DragDropModule,
  HighlightModule,
  TourMatMenuModule,
  RouterModule,
];
const states = [
  SharedState,
  SharedPersonState,
  SharedPublicContributorState,
  SharedRoleState,
  SharedUserState,
  SharedLanguageState,
  SharedLicenseState,
  SharedLicenseGroupState,
  SharedGlobalBannerState,
  SharedStructureState,
  SharedContributorState,
  SharedDepositState,
  SharedDepositTypeState,
  SharedDepositSubtypeState,
  SharedResearchGroupState,
  SharedDepositTypeSubtypeState,
  SharedDepositSubSubtypeState,
  SharedDocumentFileTypeState,
  SharedExternalDataState,
  SharedExternalDataPersonState,
  SharedExternalDataJournalTitleState,
  SharedExternalDataOpenAireState,
  SharedArchiveState,
  SharedIndexFieldAliasState,
  SharedOaiSetState,
  SharedOaiMetadataPrefixState,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...contents,
    ...presentationals,
    ...directives,
    ...pipes,
  ],
  imports: [
    ...modules,
    ImageCropperComponent,
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    ...modules,
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
    ...contents,
    ...directives,
    ...pipes,
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {panelClass: "solidify-dialogs", hasBackdrop: true} as MatDialogConfig},
  ],
})
export class SharedModule {
  constructor(private readonly _library: FaIconLibrary) {
    // Fontawesome library
    _library.addIconPacks(fab, fas);
  }
}
