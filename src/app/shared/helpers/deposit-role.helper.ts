/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-role.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {isNullOrUndefinedOrEmptyArray} from "solidify-frontend";

export class DepositRoleHelper {
  static isCreator(listUserRoleEnum: Enums.Deposit.UserRoleEnum[]): boolean {
    return this.isRoles(listUserRoleEnum, Enums.Deposit.UserRoleEnum.CREATOR);
  }

  static isContributor(listUserRoleEnum: Enums.Deposit.UserRoleEnum[]): boolean {
    return this.isRoles(listUserRoleEnum, Enums.Deposit.UserRoleEnum.CONTRIBUTOR);
  }

  static isValidator(listUserRoleEnum: Enums.Deposit.UserRoleEnum[]): boolean {
    return this.isRoles(listUserRoleEnum, Enums.Deposit.UserRoleEnum.VALIDATOR);
  }

  static isCreatorOrContributor(listUserRoleEnum: Enums.Deposit.UserRoleEnum[]): boolean {
    return this.isRoles(listUserRoleEnum, Enums.Deposit.UserRoleEnum.CREATOR, Enums.Deposit.UserRoleEnum.CONTRIBUTOR);
  }

  static isCreatorOrContributorOrValidator(listUserRoleEnum: Enums.Deposit.UserRoleEnum[]): boolean {
    return this.isRoles(listUserRoleEnum, Enums.Deposit.UserRoleEnum.CREATOR, Enums.Deposit.UserRoleEnum.CONTRIBUTOR, Enums.Deposit.UserRoleEnum.VALIDATOR);
  }

  static isRoles(listUserRoleEnum: Enums.Deposit.UserRoleEnum[], ...roleToCheck: Enums.Deposit.UserRoleEnum[]): boolean {
    if (isNullOrUndefinedOrEmptyArray(listUserRoleEnum)) {
      return false;
    }
    return listUserRoleEnum.findIndex(r => roleToCheck.includes(r)) !== -1;
  }
}
