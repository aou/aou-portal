/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-list.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositFormRuleHelper} from "@deposit/helpers/deposit-form-rule.helper";
import {DepositFormAcademicStructure} from "@deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DepositSubtype,
} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DataTableColumns,
  DataTableFieldTypeEnum,
  isEmptyString,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  ObjectUtil,
  OrderEnum,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
} from "solidify-frontend";

export class DepositListHelper {
  static columns(listSubType: DepositSubtype[]): DataTableColumns<Deposit>[] {
    return [
      {
        field: "subtype.resId" as any,
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        filterEnum: listSubType,
        order: OrderEnum.none,
        filterableField: "subtype",
        translate: true,
        isFilterable: true,
        isSortable: false,
        width: "40px",
      },
      {
        field: "title",
        header: LabelTranslateEnum.titleOfTheDocument,
        type: DataTableFieldTypeEnum.innerHtml,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        breakWord: true,
        maxWidth: "20%",
      },
      {
        field: "firstAuthor",
        header: LabelTranslateEnum.authors,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
        width: "140px",
      },
      {
        field: "publicationYear",
        header: LabelTranslateEnum.year,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
      {
        field: "creator.fullName" as any,
        header: LabelTranslateEnum.createdBy,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        filterableField: "fullName" as any,
        isFilterable: true,
        isSortable: false,
        width: "140px",
      },
      {
        field: "structures",
        header: LabelTranslateEnum.structures,
        type: DataTableFieldTypeEnum.list,
        order: OrderEnum.none,
        listCallback: (structure: DepositFormAcademicStructure) => structure.name,
        isFilterable: false,
        isSortable: false,
        width: "180px",
        isHighlightCell: deposit => isNullOrUndefinedOrEmptyArray(deposit.metadataStructures), // TODO SOLIDIFY 3
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastStatusUpdate",
        header: LabelTranslateEnum.lastStatusUpdate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "numberOfFiles",
        header: LabelTranslateEnum.file,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
      {
        field: "doi",
        header: LabelTranslateEnum.doi,
        type: DataTableFieldTypeEnum.actionButton,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "90px",
        action: {
          logo: IconNameEnum.doi,
          callback: (deposit: Deposit) => this._openDoi(deposit),
          placeholder: current => current.doi,
          displayOnCondition: (deposit: Deposit) => isNonEmptyString(deposit.doi),
          isWrapped: false,
          isVisible: true,
        },
        centerContent: true,
      },
      {
        field: "pmid",
        header: LabelTranslateEnum.pmid,
        type: DataTableFieldTypeEnum.actionButton,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "90px",
        action: {
          logo: IconNameEnum.pmid,
          callback: (deposit: Deposit) => this._openPmid(deposit),
          placeholder: current => current.pmid,
          displayOnCondition: (deposit: Deposit) => isNonEmptyString(deposit.pmid),
          isWrapped: false,
          isVisible: true,
        },
        centerContent: true,
      },
    ];
  };

  private static _openPmid(deposit: Deposit): void {
    SsrUtil.window?.open(environment.pmidWebsite + deposit.pmid, "_blank");
  }

  private static _openDoi(deposit: Deposit): void {
    SsrUtil.window?.open(environment.doiWebsite + deposit.doi, "_blank");
  }

  static improveListDepositWithMetadata(listDeposit: Deposit[]): Deposit[] {
    const newList: Deposit[] = [];
    listDeposit.forEach(deposit => {
      const cloneDeposit = ObjectUtil.clone(deposit);
      cloneDeposit.structures = isNonEmptyArray(cloneDeposit.metadataStructures) ? cloneDeposit.metadataStructures : cloneDeposit.validationStructures;
      if (isNonEmptyString(cloneDeposit.formData)) {
        cloneDeposit.depositFormModel = DepositFormRuleHelper.applyRuleToCleanUndesiredDataDependingOfType(JSON.parse(deposit.formData), []);
        const numberContributors = cloneDeposit?.depositFormModel?.contributors?.contributors?.length;
        if (numberContributors > 0) {
          const firstAuthor = cloneDeposit.depositFormModel.contributors.contributors.find(c => c.contributor?.role === Enums.Deposit.RoleContributorEnum.author);
          if (isNotNullNorUndefined(firstAuthor)) {
            cloneDeposit.firstAuthor = firstAuthor.contributor.lastname + " " + firstAuthor.contributor.firstname + (numberContributors > 1 ? ` (+${numberContributors - 1})` : "");
          } else {
            //if there is no authors, checks collaborators or editors
            const firstCollaborator = cloneDeposit.depositFormModel.contributors.contributors.find(c => c.contributor?.role === Enums.Deposit.RoleContributorEnum.collaborator);
            if (isNotNullNorUndefined(firstCollaborator)) {
              cloneDeposit.firstAuthor = firstCollaborator.contributor.lastname + " " + firstCollaborator.contributor.firstname + (numberContributors > 1 ? ` (+${numberContributors - 1})` : "");
            } else {
              const firstEditor = cloneDeposit.depositFormModel.contributors.contributors.find(c => c.contributor?.role === Enums.Deposit.RoleContributorEnum.editor);
              if (isNotNullNorUndefined(firstEditor)) {
                cloneDeposit.firstAuthor = firstEditor.contributor.lastname + " " + firstEditor.contributor.firstname + (numberContributors > 1 ? ` (+${numberContributors - 1})` : "");
              }
            }
          }
        }
        const datePublication = cloneDeposit?.depositFormModel?.description?.dates?.filter(d => isNonEmptyString(d.date)).map(d => d.date);
        if (isNonEmptyArray(datePublication)) {
          cloneDeposit.publicationYear = datePublication.map(date => this._extractYearFormDepositDate(date)).join(SOLIDIFY_CONSTANTS.COMMA + " ");
        }
        if (isNullOrUndefined(cloneDeposit.pmid)) {
          cloneDeposit.pmid = cloneDeposit?.depositFormModel?.description?.identifiers?.pmid;
        }
        if (isNullOrUndefined(cloneDeposit.doi)) {
          cloneDeposit.doi = cloneDeposit?.depositFormModel?.description?.identifiers?.doi;
        }
        newList.push(cloneDeposit);
      } else {
        newList.push(cloneDeposit);
      }
    });
    return newList;
  }

  private static _extractYearFormDepositDate(depositDate: string): string {
    if (isNullOrUndefined(depositDate) || isEmptyString(depositDate)) {
      return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
    if (depositDate.indexOf(SOLIDIFY_CONSTANTS.DOT) === -1) {
      return depositDate;
    }
    const depositDateSplit = depositDate.split(SOLIDIFY_CONSTANTS.DOT);
    return depositDateSplit[depositDateSplit.length - 1];
  }
}
