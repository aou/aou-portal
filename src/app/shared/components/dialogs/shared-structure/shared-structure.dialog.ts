/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-structure.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {environment} from "@environments/environment";
import {Structure} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedStructureAction} from "@shared/stores/structure/shared-structure.action";
import {
  isArray,
  isNotNullNorUndefined,
  OrderEnum,
  QueryParameters,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-structure-dialog",
  templateUrl: "./shared-structure.dialog.html",
  styleUrls: ["./shared-structure.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedStructureDialog extends SharedAbstractDialog<SharedPersonStructureDialogData, Structure[] | Structure> implements OnInit {
  structuresSelected: Structure[] | Structure;

  isLoading: boolean = true;
  listStructure: Structure[] = [];

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<SharedStructureDialog>,
              @Inject(MAT_DIALOG_DATA) readonly data: SharedPersonStructureDialogData,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    this.subscribe(
      StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedStructureAction.GetAll(new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
          field: "sortValue",
          order: OrderEnum.ascending,
        })),
        SharedStructureAction.GetAllSuccess,
        resultAction => {
          this.listStructure = resultAction.list._data;
          this.isLoading = false;
          this._computeStructureSelected();
          this._changeDetector.detectChanges();
        }),
    );
  }

  private _computeStructureSelected(): void {
    if (this.data.isMultiSelectable) {
      this.structuresSelected = [];
      if (isArray(this.data.value)) {
        this.data.value.forEach(structureId => {
          const result = this.listStructure.find(s => s.resId === structureId);
          if (isNotNullNorUndefined(result)) {
            (this.structuresSelected as Structure[]).push(result);
          }
        });
      }
    } else {
      const result = this.listStructure.find(s => s.resId === this.data.value);
      if (isNotNullNorUndefined(result)) {
        this.structuresSelected = result;
      }
    }
  }

  multiSelectChange($event: Structure[]): void {
    this.structuresSelected = $event;
  }

  singleSelectChange($event: Structure): void {
    this.structuresSelected = $event;
  }

  confirm(): void {
    this.submit(this.structuresSelected);
  }
}

export interface SharedPersonStructureDialogData {
  value: string[] | string;
  isMultiSelectable: boolean;
}
