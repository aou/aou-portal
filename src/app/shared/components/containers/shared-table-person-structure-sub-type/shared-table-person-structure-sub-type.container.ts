/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-table-person-structure-sub-type.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {sharedStructureActionNameSpace} from "@app/shared/stores/structure/shared-structure.action";
import {environment} from "@environments/environment";
import {
  DepositSubtype,
  Structure,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedStructureDialog} from "@shared/components/dialogs/shared-structure/shared-structure.dialog";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {SharedDepositSubtypeAction} from "@shared/stores/deposit-subtype/shared-deposit-subtype.action";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  BaseFormDefinition,
  BaseResource,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  FormValidationHelper,
  isNonEmptyArray,
  isNullOrUndefined,
  isTrue,
  isUndefined,
  ObservableUtil,
  OrderEnum,
  PropertyName,
  QueryParameters,
  ResourceNameSpace,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-table-person-structure-sub-type-container",
  templateUrl: "./shared-table-person-structure-sub-type.container.html",
  styleUrls: ["./shared-table-person-structure-sub-type.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedTablePersonStructureSubTypeContainer,
    },
  ],
})
export class SharedTablePersonStructureSubTypeContainer extends SharedAbstractContainer implements ControlValueAccessor, OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  columns: DataTableColumns<BaseResource>[] = [
    {
      field: "structure" as any,
      header: LabelTranslateEnum.structure,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.descending,
      isFilterable: false,
      isSortable: true,
    },
    {
      field: "subType" as any,
      header: LabelTranslateEnum.subtype,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isFilterable: false,
      isSortable: false,
    },
  ];

  @Input()
  isWithLinkToAdmin: boolean = true;

  listSubtype: DepositSubtype[];

  sharedStructureActionNameSpace: ResourceNameSpace = sharedStructureActionNameSpace;
  sharedStructureState: typeof SharedStructureState = SharedStructureState;

  @Input()
  formArray: FormArray;

  _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    if (isTrue(value)) {
      this.formArray.disable();
    } else {
      this.formArray.enable();
    }
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private readonly _valueBS: BehaviorSubject<any[] | undefined> = new BehaviorSubject<any[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<any[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(abstractControl: AbstractControl, key: string): FormControl {
    return FormValidationHelper.getFormControl(abstractControl as FormGroup, key);
  }

  isRequired(formControl: AbstractControl, key: string): boolean {
    const errors = formControl.get(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _dialog: MatDialog,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _elementRef: ElementRef,
              private readonly _actions$: Actions) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
      field: "name",
      order: OrderEnum.ascending,
    });
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store,
      this._actions$,
      new SharedDepositSubtypeAction.GetAll(queryParameters),
      SharedDepositSubtypeAction.GetAllSuccess,
      result => {
        this.listSubtype = result?.list?._data;
        this._changeDetector.detectChanges();
      }));
  }

  createStructureSubtype(structurePersonPublicationSubtype: ValidationRight | undefined = undefined): FormGroup {
    if (isUndefined(structurePersonPublicationSubtype)) {
      return this._fb.group({
        [this.formDefinition.id]: ["", [Validators.required]],
        [this.formDefinition.listId]: [[], [Validators.required]],
      });
    } else {
      const listId = isNonEmptyArray(structurePersonPublicationSubtype?.publicationSubtypes) ?
        structurePersonPublicationSubtype?.publicationSubtypes?.map(t => t.resId)
        : [];
      return this._fb.group({
        [this.formDefinition.id]: [structurePersonPublicationSubtype?.resId, [Validators.required]],
        [this.formDefinition.listId]: [listId, [Validators.required]],
      });
    }
  }

  openStructure(): void {
    this.subscribe(DialogUtil.open(this._dialog, SharedStructureDialog, {
      isMultiSelectable: false,
    }, {
      width: "90%",
    }, (structure: Structure) => {
      this.formArray.push(this.createStructureSubtype(structure));
      this.formArray.markAsDirty();
      this._changeDetector.detectChanges();
    }));
  }

  add(tryLater: boolean = true): void {
    if (!this.formArray.disabled) {
      this.openStructure();
    } else {
      if (tryLater) {
        setTimeout(() => this.add(false), 10);
      }
    }
  }

  delete(index: number): void {
    this.formArray.removeAt(index);
    this.formArray.markAsDirty();
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string[]): void {
  }

  goToStructure(structureId: string): void {
    this._navigateBS.next([RoutesEnum.adminStructureDetail, structureId]);
  }

  addAll(index: number): void {
    const formGroup: FormGroup = this.formArray.controls[index] as FormGroup;
    // Remove form control to force reload component with new list
    formGroup.removeControl(this.formDefinition.listId);
    this._changeDetector.detectChanges();
    formGroup.addControl(this.formDefinition.listId, this._fb.control(this.listSubtype?.map(t => t.resId)));
    formGroup.markAsDirty();
    this._changeDetector.detectChanges();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() id: string;
  @PropertyName() listId: string;
}

interface Model {
  id: string;
  listId: string;
}
