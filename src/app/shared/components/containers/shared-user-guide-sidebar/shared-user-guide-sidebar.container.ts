/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-user-guide-sidebar.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Renderer2,
} from "@angular/core";
import {appActionNameSpace} from "@app/stores/app.action";
import {AppState} from "@app/stores/app.state";
import {AppTocState} from "@app/stores/toc/app-toc.state";
import {environment} from "@environments/environment";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  guideSidebarAnimations,
  SharedAbstractGuideSidebarContainer,
} from "@shared/components/containers/shared-abstract-guide-sidebar/shared-abstract-guide-sidebar.container";
import {Observable} from "rxjs";
import {
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-user-guide-sidebar-container",
  templateUrl: "../shared-abstract-guide-sidebar/shared-abstract-guide-sidebar.container.html",
  styleUrls: ["./shared-user-guide-sidebar.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: guideSidebarAnimations,
})
export class SharedUserGuideSidebarContainer extends SharedAbstractGuideSidebarContainer {
  documentationObs: Observable<string> = MemoizedUtil.select(this._store, AppTocState, s => s.userDocumentation);
  isOpenedGuideSidebarObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, s => s.isOpenedSidebarUserGuide);
  actionChangeDisplayGuide: any = appActionNameSpace.ChangeDisplaySidebarUserGuide;
  tocPath: string = environment.documentationTocUserGuidePath;

  title: string = MARK_AS_TRANSLATABLE("userGuideSidebar.title");
  tooltipClose: string = MARK_AS_TRANSLATABLE("userGuideSidebar.tooltipClose");

  constructor(protected readonly _store: Store,
              protected readonly _renderer: Renderer2,
              protected readonly _actions$: Actions) {
    super(_store, _renderer, _actions$);
  }

  closeGuideSidebar(): void {
    this._store.dispatch(new appActionNameSpace.ChangeDisplaySidebarUserGuide(false));
  }
}
