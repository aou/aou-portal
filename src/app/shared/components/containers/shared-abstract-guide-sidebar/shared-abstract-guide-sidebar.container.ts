/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-abstract-guide-sidebar.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import {
  Directive,
  HostBinding,
  OnInit,
  Renderer2,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {TocHelper} from "@shared/helpers/toc.helper";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  ofSolidifyActionCompleted,
  SsrUtil,
} from "solidify-frontend";

export const guideSidebarAnimations: any[] = [
  trigger("sidebarAnimation", [
    state("void", style({opacity: "0" /*right: "-400px"*/})),
    transition(":enter", animate("300ms ease")),
    transition(":leave", animate("300ms ease")),
  ]),
  trigger("backdropAnimation", [
    state("void", style({opacity: "0"})),
    transition(":enter", animate("300ms ease")),
    transition(":leave", animate("300ms ease")),
  ]),
];

@Directive()
export abstract class SharedAbstractGuideSidebarContainer extends SharedAbstractContainer implements OnInit {
  abstract documentationObs: Observable<string>;
  abstract isOpenedGuideSidebarObs: Observable<boolean>;
  abstract actionChangeDisplayGuide: any;

  abstract title: string;
  abstract tooltipClose: string;
  abstract tocPath: string;

  @HostBinding("class.hide")
  hide: boolean = true;

  constructor(protected readonly _store: Store,
              protected readonly _renderer: Renderer2,
              protected readonly _actions$: Actions) {
    super();
  }

  ngOnInit(): void {
    this.subscribe(this.documentationObs.pipe(
        distinctUntilChanged(),
        filter(doc => isNotNullNorUndefined(doc)),
        take(1),
      ),
      () => this.hide = false);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(this.actionChangeDisplayGuide),
      filter(action => action.action["isOpen"]),
      tap(action => this._updateLinkGuide()),
    ));
  }

  private _updateLinkGuide(): void {
    setTimeout(() => {
      TocHelper.updateLinkToc(SsrUtil.window?.document, this._renderer, "guide-sidebar", this.tocPath);
    }, 0);
  }

  abstract closeGuideSidebar(): void;
}
