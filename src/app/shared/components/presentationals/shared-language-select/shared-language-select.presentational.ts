/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-language-select.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {ObservableUtil} from "solidify-frontend";

@Component({
  selector: "aou-shared-language-select",
  templateUrl: "./shared-language-select.presentational.html",
  styleUrls: ["./shared-language-select.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedLanguageSelectPresentational extends SharedAbstractPresentational {
  @Input()
  listAllLanguages: string[] = [];

  @Input()
  listOtherLanguages: string[] = [];

  @Input()
  currentLanguage: string;

  private readonly _languageBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("languageChange")
  readonly languageObs: Observable<string | undefined> = ObservableUtil.asObservable(this._languageBS);

  stopPropagation($event: MouseEvent): void {
    $event.stopPropagation();
  }

  switchToLanguage(language: string): void {
    this._languageBS.next(language);
  }
}
