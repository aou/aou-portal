/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-file-version.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  EnumUtil,
  isNonEmptyString,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-file-version",
  templateUrl: "./shared-file-version.presentational.html",
  styleUrls: ["./shared-file-version.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedFileVersionPresentational extends SharedAbstractPresentational {
  private readonly _VALUE_PUBLISHED_VERSION: string = "(published version)";
  private readonly _VALUE_ACCEPTED_VERSION: string = "(accepted version)";
  private readonly _VALUE_SUBMITTED_VERSION: string = "(submitted version)";
  private readonly _VALUE_AGREEMENT: string = "agreement";

  private readonly _VALUE_SECONDARY: Enums.Deposit.FileTypeStringEnum[] = [
    Enums.Deposit.FileTypeStringEnum.APPENDIX,
    Enums.Deposit.FileTypeStringEnum.SUPPLEMENTAL_DATA,
    Enums.Deposit.FileTypeStringEnum.RECORDING,
    Enums.Deposit.FileTypeStringEnum.EXTRACT,
    Enums.Deposit.FileTypeStringEnum.SUMMARY,
    Enums.Deposit.FileTypeStringEnum.TRANSLATION,
  ];

  private readonly _VALUE_THESIS_OR_MASTER: Enums.Deposit.FileTypeStringEnum[] = [
    Enums.Deposit.FileTypeStringEnum.THESIS,
    Enums.Deposit.FileTypeStringEnum.MASTER,
  ];

  private readonly _VALUE_PRESENTATION_OR_POSTER_OR_PREPRINT: Enums.Deposit.FileTypeStringEnum[] = [
    Enums.Deposit.FileTypeStringEnum.PRESENTATION,
    Enums.Deposit.FileTypeStringEnum.POSTER,
    Enums.Deposit.FileTypeStringEnum.PREPRINT,
  ];

  private readonly _VALUE_CORRECTION: Enums.Deposit.FileTypeStringEnum[] = [
    Enums.Deposit.FileTypeStringEnum.ADDENDUM,
    Enums.Deposit.FileTypeStringEnum.CORRIGENDUM,
    Enums.Deposit.FileTypeStringEnum.ERRATUM,
    Enums.Deposit.FileTypeStringEnum.RETRACTION,
  ];

  _fileVersion: Enums.Deposit.FileTypeStringEnum;
  fileVersionIconName: IconNameEnum;
  labelToTranslate: string;

  @Input()
  set fileVersion(fileVersion: Enums.Deposit.FileTypeStringEnum) {
    this._fileVersion = fileVersion;
    this._computeFileVersionIcon(fileVersion);
    this._computeLabel(fileVersion);
  }

  get fileVersion(): Enums.Deposit.FileTypeStringEnum {
    return this._fileVersion;
  }

  @Input()
  size: string = "size-24";

  @Input()
  withTooltip: boolean = false;

  constructor(private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _computeFileVersionIcon(fileVersion: Enums.Deposit.FileTypeStringEnum): void {
    if (!isNonEmptyString(fileVersion)) {
      return;
    }
    const fileVersionLower = fileVersion.toLowerCase();
    if (fileVersionLower.indexOf(this._VALUE_PUBLISHED_VERSION) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileVersionPublished;
      return;
    }
    if (fileVersionLower.indexOf(this._VALUE_ACCEPTED_VERSION) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileVersionAccepted;
      return;
    }
    if (fileVersionLower.indexOf(this._VALUE_SUBMITTED_VERSION) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileVersionSubmitted;
      return;
    }
    if (fileVersionLower.indexOf(this._VALUE_SUBMITTED_VERSION) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileVersionSubmitted;
      return;
    }
    if (this._VALUE_THESIS_OR_MASTER.indexOf(fileVersion) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileThesisMaster;
      return;
    }
    if (this._VALUE_SECONDARY.indexOf(fileVersion) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileSecondary;
      return;
    }
    if (this._VALUE_CORRECTION.indexOf(fileVersion) !== -1) {
      this.fileVersionIconName = IconNameEnum.fileCorrection;
      return;
    }
    if (this._VALUE_PRESENTATION_OR_POSTER_OR_PREPRINT.indexOf(fileVersion) !== -1) {
      this.fileVersionIconName = IconNameEnum.filePresentationPosterPreprint;
      return;
    }
    if (fileVersionLower === this._VALUE_AGREEMENT) {
      this.fileVersionIconName = IconNameEnum.fileAgreements;
      return;
    }
    this.fileVersionIconName = IconNameEnum.fileOther;
  }

  private _computeLabel(fileVersion: Enums.Deposit.FileTypeStringEnum): void {
    this.labelToTranslate = EnumUtil.getLabel(Enums.Deposit.FileTypeEnumTranslate, fileVersion);
  }
}
