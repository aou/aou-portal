/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-file-type.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {
  DocumentFile,
  DocumentFileType,
} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-file-type",
  templateUrl: "./shared-file-type.presentational.html",
  styleUrls: ["./shared-file-type.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedFileTypePresentational extends SharedAbstractPresentational {
  label: string;

  private _documentFile: DocumentFile;

  @Input()
  set documentFile(value: DocumentFile) {
    this._documentFile = value;
    this.label = this._getLabelDocumentType(value.documentFileType);
  }

  get documentFile(): DocumentFile {
    return this._documentFile;
  }

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _getLabelDocumentType(value: DocumentFileType): string | undefined {
    if (isNullOrUndefined(value)) {
      return undefined;
    }
    const currentLanguage = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage);
    const languageV2 = Enums.Language.LanguageIso639V1ToV2(currentLanguage);
    let label = value.labels?.find(l => l.language === languageV2);
    if (isNullOrUndefined(label)) {
      label = value.labels?.find(l => l.language === Enums.Language.LanguageV2Enum.eng);
    }

    if (isNullOrUndefined(label)) {
      return value.value;
    }

    return label.text;
  }
}
