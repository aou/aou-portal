/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-principal-file-access-level.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  ColorUtil,
  EnumUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-principal-file-access-level",
  templateUrl: "./shared-principal-file-access-level.presentational.html",
  styleUrls: ["./shared-principal-file-access-level.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedPrincipalFileAccessLevelPresentational extends SharedAbstractPresentational {
  _principalFileAccessLevel: Enums.Facet.FacetValue.PrincipalFilesAccessLevel;
  accessLevelIconName: IconNameEnum;
  displayCircle: boolean;
  labelToTranslate: string;
  backgroundColor: string;
  borderColor: string;

  @Input()
  set principalFileAccessLevel(value: Enums.Facet.FacetValue.PrincipalFilesAccessLevel) {
    this._principalFileAccessLevel = value;
    this._computeLabel(value);
    this._computeAccessLevelIcon(value);
    this._computeColor(value);
  }

  get principalFileAccessLevel(): Enums.Facet.FacetValue.PrincipalFilesAccessLevel {
    return this._principalFileAccessLevel;
  }

  @Input()
  size: string = "size-24";

  @Input()
  withTooltip: boolean = false;

  private _computeLabel(accessLevel: Enums.Facet.FacetValue.PrincipalFilesAccessLevel): void {
    this.labelToTranslate = EnumUtil.getLabel(Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate, accessLevel);
  }

  private _computeColor(accessLevel: Enums.Facet.FacetValue.PrincipalFilesAccessLevel): void {
    this.borderColor = Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate.find(d => d.key === accessLevel)?.backgroundColorHexa;
    this.backgroundColor = ColorUtil.addAlpha(this.borderColor, 0.4);
  }

  private _computeAccessLevelIcon(accessLevel: Enums.Facet.FacetValue.PrincipalFilesAccessLevel): void {
    switch (accessLevel) {
      case Enums.Facet.FacetValue.PrincipalFilesAccessLevel.RESTRICTED:
        this.accessLevelIconName = IconNameEnum.accessLevelRestricted;
        this.displayCircle = false;
        break;
      case Enums.Facet.FacetValue.PrincipalFilesAccessLevel.PRIVATE:
        this.accessLevelIconName = IconNameEnum.accessLevelPrivate;
        this.displayCircle = false;
        break;
      case Enums.Facet.FacetValue.PrincipalFilesAccessLevel.PUBLIC:
        this.accessLevelIconName = IconNameEnum.accessLevelPublic;
        this.displayCircle = false;
        break;
      case Enums.Facet.FacetValue.PrincipalFilesAccessLevel.NO_FULLTEXT:
        this.accessLevelIconName = undefined;
        this.displayCircle = true;
        break;
      default:
        this.accessLevelIconName = IconNameEnum.accessLevelUndefined;
        this.displayCircle = false;
        break;
    }
  }
}
