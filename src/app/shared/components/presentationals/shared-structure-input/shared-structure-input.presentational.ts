/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-structure-input.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {SharedStructureAction} from "@app/shared/stores/structure/shared-structure.action";
import {environment} from "@environments/environment";
import {Structure} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {TooltipTranslateEnum} from "@shared/enums/tooltip-translate.enum";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  CookieConsentService,
  FormValidationHelper,
  ObservableUtil,
  OrderEnum,
  OverlayPositionEnum,
  QueryParameters,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-structure-input",
  templateUrl: "./shared-structure-input.presentational.html",
  styleUrls: ["./shared-structure-input.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedStructureInputPresentational,
    },
  ],
})
export class SharedStructureInputPresentational extends SharedAbstractPresentational implements ControlValueAccessor, OnInit {
  @Input()
  formControl: FormControl;

  @Input()
  listDefaultStructure: string[] | string;

  @Input()
  isMulti: boolean = true;

  disableDialog: boolean = false;

  @Input()
  set readonly(value: boolean) {
    if (value) {
      this.formControl.disable();
      this.disableDialog = true;
    } else {
      this.formControl.enable();
      this.disableDialog = false;
    }
  }

  @Input()
  showEmptyRequiredFieldInError: boolean = this.displayEmptyRequiredFieldInError;

  @Input()
  closeAfterValueSelected: boolean = false;

  @Input()
  set mode(value: "profile" | "deposit" | undefined) {
    if (value === "profile") {
      this.tooltip = TooltipTranslateEnum.profileStructure;
    } else if (value === "deposit") {
      this.tooltip = TooltipTranslateEnum.depositStructure;
    } else {
      this.tooltip = undefined;
    }
  }

  @Input()
  hintToTranslate?: string;

  tooltip: string | undefined;

  private readonly _valueChangeBS: BehaviorSubject<Structure[] | Structure> = new BehaviorSubject<Structure[] | Structure>(undefined);
  @Output("valueChange")
  readonly valueChangeObs: Observable<Structure[] | Structure> = ObservableUtil.asObservable(this._valueChangeBS);

  structureLabel: (value: Structure) => string = value => SharedStructureState.labelCallback(value, this._translateService);

  @Input("overlayPosition")
  overlayPosition: OverlayPositionEnum = OverlayPositionEnum.bottom;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  listStructure: Structure[];
  isLoading: boolean;

  constructor(private readonly _dialog: MatDialog,
              private readonly _cd: ChangeDetectorRef,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _cookieConsentService: CookieConsentService,
              private readonly _translateService: TranslateService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.isLoading = true;
    this.subscribe(
      StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedStructureAction.GetAll(new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo, {
          field: "sortValue",
          order: OrderEnum.ascending,
        }), true, false),
        SharedStructureAction.GetAllSuccess,
        resultAction => {
          this.listStructure = resultAction.list._data;
          this.isLoading = false;
          this._changeDetector.detectChanges();
        }),
    );
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
  }

  valueChange($event: Structure[] | Structure): void {
    this._valueChangeBS.next($event);
  }

  preTreatmentHighlightText(result: string): string {
    const endSpan = `</span>`;
    const isHistorical = "is-historical";
    const isObsolete = "is-obsolete";

    if (result.endsWith(endSpan)) {
      const startSpanIsHistorical = `<span class="${isHistorical}">`;
      if (result.startsWith(startSpanIsHistorical)) {
        result = result.substring(startSpanIsHistorical.length, result.length - endSpan.length);
      }

      const startSpanIsObsolete = `<span class="${isObsolete}">`;
      if (result.startsWith(startSpanIsObsolete)) {
        result = result.substring(startSpanIsObsolete.length, result.length - endSpan.length);
      }
    }

    return result;
  }

  postTreatmentHighlightText(result: string, resultBeforePreTreatment: string): string {
    const endSpan = `</span>`;
    const isHistorical = "is-historical";
    const isObsolete = "is-obsolete";

    if (resultBeforePreTreatment.endsWith(endSpan)) {
      const startSpanIsHistorical = `<span class="${isHistorical}">`;
      if (resultBeforePreTreatment.startsWith(startSpanIsHistorical)) {
        return startSpanIsHistorical + result + endSpan;
      }

      const startSpanIsObsolete = `<span class="${isObsolete}">`;
      if (resultBeforePreTreatment.startsWith(startSpanIsObsolete)) {
        return startSpanIsObsolete + result + endSpan;
      }
    }

    return result;
  }
}
