/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-contributors-list.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {DepositTableContributorTypeEnum} from "@deposit/components/containers/deposit-table-contributor/deposit-table-contributor.container";
import {Enums} from "@enums";
import {HomeContributorOverlayPresentational} from "@home/components/presentationals/home-contributor-overlay/home-contributor-overlay.presentational";
import {HomeHelper} from "@home/helpers/home.helper";
import {
  PublishedDeposit,
  PublishedDepositContributor,
} from "@home/models/published-deposit.model";
import {Contributor} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrEmptyArray,
  Type,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-contributors-list",
  templateUrl: "./shared-contributors-list.presentational.html",
  styleUrls: ["./shared-contributors-list.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedContributorsListPresentational extends SharedAbstractPresentational implements OnInit {

  readonly ELLIPSIS_STRING = "...";

  private _deposit: PublishedDeposit;

  contributorsList: ContributorInList[];

  @Input()
  roles: Enums.Deposit.RoleContributorEnum[];

  @Input()
  includeCollaborations: boolean = false;

  @Input()
  set deposit(value: PublishedDeposit) {
    this._deposit = value;
  }

  ngOnInit(): void {
    this.contributorsList = this._buildContributorsList();
  }

  private _buildContributorsList(): ContributorInList[] {
    if (isNullOrUndefinedOrEmptyArray(this._deposit.contributors) || (!this.includeCollaborations && isNullOrUndefinedOrEmptyArray(this.roles))) {
      return [];
    }

    const maxContributors = 5;
    const contributors = this._deposit.contributors;
    let filteredContributors = contributors.filter(c => this.roles.includes(c.role) || (this.includeCollaborations && c.type === DepositTableContributorTypeEnum.collaboration));

    let isTruncated: boolean = false;
    if (filteredContributors.length > maxContributors) {
      filteredContributors = [...filteredContributors.splice(0, maxContributors - 1), filteredContributors[filteredContributors.length - 1]];
      isTruncated = true;
    }

    const contributorsList: ContributorInList[] = [];
    let count: number = 0;
    filteredContributors.forEach(c => {
      count++;
      if (isTruncated && count === maxContributors) {
        const separatorContributorList: ContributorInList = {};
        separatorContributorList.fullName = this.ELLIPSIS_STRING;
        separatorContributorList.isUnige = false;
        contributorsList.push(separatorContributorList);
      }

      const contributorList: ContributorInList = {};
      contributorList.publishedDepositContributor = c;
      contributorList.isUnige = isNotNullNorUndefinedNorWhiteString(c.cnIndividu) ? true : false;
      if (c.type === DepositTableContributorTypeEnum.collaboration) {
        contributorList.fullName = c.name;
      } else {
        contributorList.firstName = c.firstName;
        contributorList.lastName = c.lastName;
        let fullname = c.lastName;
        if (isNotNullNorUndefinedNorWhiteString(c.firstName)) {
          fullname += ", " + c.firstName;
        }
        contributorList.fullName = fullname;
      }
      contributorsList.push(contributorList);
    });

    return contributorsList;
  }

  contributorOverlayComponent: Type<HomeContributorOverlayPresentational> = HomeContributorOverlayPresentational;

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  trackByFn(index: number, item: ContributorInList): string {
    return item.fullName;
  }

  getContributorOverlayData(contributor: PublishedDepositContributor): Contributor {
    const fullName = contributor.lastName + ", " + contributor.firstName;
    return {
      firstName: contributor.firstName,
      lastName: contributor.lastName,
      fullName: fullName,
      orcid: contributor.orcid,
      cnIndividu: contributor.cnIndividu,
    };
  }

  getLink(contributor: ContributorInList): Navigate {
    return HomeHelper.getContributorLink(this._store, true, contributor?.publishedDepositContributor?.cnIndividu, contributor?.firstName, contributor?.lastName);
  }

  preventEvent($event: MouseEvent): void {
    $event.stopPropagation();
  }
}

export interface ContributorInList {
  fullName?: string;
  firstName?: string;
  lastName?: string;
  isUnige?: boolean;
  publishedDepositContributor?: PublishedDepositContributor;
}
