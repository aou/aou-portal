/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-structure-tree.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {FlatTreeControl} from "@angular/cdk/tree";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from "@angular/material/tree";
import {DepositFormAcademicStructure} from "@app/features/deposit/models/deposit-form-definition.model";
import {Enums} from "@enums";
import {Structure} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {SharedStructureState} from "@shared/stores/structure/shared-structure.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-structure-tree",
  templateUrl: "./shared-structure-tree.presentational.html",
  styleUrls: ["./shared-structure-tree.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedStructureTreePresentational extends SharedAbstractPresentational implements AfterViewInit {
  rawStructureList: Structure[];

  @Input()
  set listStructure(value: Structure[]) {
    this.rawStructureList = value;
    this.dataSource.data = this._createDataTree(value);
  }

  get status(): typeof Enums.Structure.StatusEnum {
    return Enums.Structure.StatusEnum;
  }

  @Input()
  canDelete: boolean = false;

  @Input()
  canEdit: boolean = false;

  @Input()
  isMultiSelectable: boolean = false;

  key: string = "resId";

  get valueSelected(): Structure | DepositFormAcademicStructure {
    if (isNullOrUndefined(this._selectBS.value)) {
      return undefined;
    }
    return this._selectBS.value;
  }

  get valuesSelected(): Structure[] | DepositFormAcademicStructure[] | Structure | DepositFormAcademicStructure | undefined {
    if (this.isMultiSelectable) {
      return this.multiValuesSelected;
    } else {
      return this.singleValueSelected;
    }
  }

  get multiValuesSelected(): Structure[] | DepositFormAcademicStructure[] {
    if (this.isMultiSelectable) {
      if (isNullOrUndefined(this._multiSelectBS.value)) {
        return [];
      }
      return this._multiSelectBS.value;
    } else {
      // eslint-disable-next-line no-console
      console.warn("Should never be here");
    }
  }

  get singleValueSelected(): Structure | DepositFormAcademicStructure | undefined {
    if (!this.isMultiSelectable) {
      if (isNullOrUndefined(this._selectBS.value)) {
        return undefined;
      }
      return this._selectBS.value;
    } else {
      // eslint-disable-next-line no-console
      console.warn("Should never be here");
    }
  }

  private readonly _selectBS: BehaviorSubject<Structure | DepositFormAcademicStructure> = new BehaviorSubject<Structure | DepositFormAcademicStructure>(undefined);
  @Output("selectChange")
  readonly selectObs: Observable<Structure | DepositFormAcademicStructure> = ObservableUtil.asObservable(this._selectBS);

  @Input("multiSelect")
  set multiSelect(value: Structure[] | DepositFormAcademicStructure[]) {
    this._multiSelectBS.next(value);
    this.expandSelectedNode();
  }

  private readonly _multiSelectBS: BehaviorSubject<Structure[] | DepositFormAcademicStructure[]> = new BehaviorSubject<Structure[] | DepositFormAcademicStructure[]>(undefined);
  @Output("multiSelectChange")
  readonly multiSelectObs: Observable<Structure[] | DepositFormAcademicStructure[]> = ObservableUtil.asObservable(this._multiSelectBS);

  private readonly _deleteStructureBS: BehaviorSubject<Structure> = new BehaviorSubject<Structure>(undefined);
  @Output("deleteStructure")
  readonly deleteStructureObs: Observable<Structure> = ObservableUtil.asObservable(this._deleteStructureBS);

  private readonly _editStructureBS: BehaviorSubject<Structure> = new BehaviorSubject<Structure>(undefined);
  @Output("editStructure")
  readonly editStructureObs: Observable<Structure> = ObservableUtil.asObservable(this._editStructureBS);

  expandedAll: boolean = false;

  constructor(private readonly _translateService: TranslateService) {
    super();
  }

  private _transformer: (node: StructureNode, level: number) => StructuredFlatNode = (node: StructureNode, level: number) =>
    ({
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      resId: node.resId,
      status: node.status,
      code: node.code,
      // tslint:disable-next-line
    } as StructuredFlatNode);

  treeControl: FlatTreeControl<StructuredFlatNode> = new FlatTreeControl<StructuredFlatNode>(node => node.level, node => node.expandable);
  treeFlattener: MatTreeFlattener<StructureNode, StructuredFlatNode> = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
  dataSource: MatTreeFlatDataSource<StructureNode, StructuredFlatNode> = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  hasChild = (_: number, node: StructuredFlatNode) => node.expandable;

  private _createDataTree(listStructure: Structure[]): StructureNode[] {
    if (isNullOrUndefined(listStructure)) {
      return [];
    }

    const map = {} as MappingObject<string, StructureNode>;

    listStructure.forEach(structure => MappingObjectUtil.set(map, structure.resId, {
      resId: structure.resId,
      name: SharedStructureState.labelCallback(structure, this._translateService),
      status: structure.status,
      code: structure.cStruct,
      children: [],
    }));

    const tree: StructureNode[] = [];

    listStructure.forEach(structure => {
      if (structure.parentStructure?.resId) {
        MappingObjectUtil.get(map, structure.parentStructure.resId)?.children.push(MappingObjectUtil.get(map, structure.resId));
      } else {
        tree.push(MappingObjectUtil.get(map, structure.resId));
      }
    });

    return tree;
  }

  expandAllOrCollapseAll(): void {
    if (this.expandedAll) {
      this.treeControl.collapseAll();
      this.expandedAll = false;
    } else {
      this.treeControl.expandAll();
      this.expandedAll = true;
    }
  }

  expandSelectedNode(): void {
    if (isNullOrUndefined(this.valuesSelected)) {
      return;
    }
    const structureToExpand: Structure[] = [];

    if (this.isMultiSelectable) {

      this.multiValuesSelected.forEach(value => {
        const structure = this.rawStructureList.find(s => s.resId === (value as Structure).resId);
        this._addInStructureRecursive(structureToExpand, structure);
      });

    } else {
      const structure = this.rawStructureList.find(s => s.resId === (this.singleValueSelected as Structure)?.resId);
      this._addInStructureRecursive(structureToExpand, structure);

    }

    structureToExpand.forEach(structure => {
      const nodeToExpand = this.treeControl.dataNodes.find(node => node.resId === structure.resId);
      this.treeControl.expand(nodeToExpand);
    });
  }

  private _addInStructureRecursive(listStructureToExpand: Structure[], structure: Structure): void {
    if (isNotNullNorUndefined(structure?.parentStructure)) {
      listStructureToExpand.push(structure.parentStructure);
      this._addInStructureRecursive(listStructureToExpand, structure.parentStructure);
    }
  }

  select(node: StructuredFlatNode): void {
    if (this.isMultiSelectable) {
      const list: Structure[] | DepositFormAcademicStructure[] = [...this.multiValuesSelected] as Structure[] | DepositFormAcademicStructure[];
      const indexOf = list.findIndex(s => s[this.key] === node[this.key]);
      if (indexOf === -1) {
        const value = node as Structure | any;
        list.push(value as any);
      } else {
        list.splice(indexOf, 1);
      }
      this._multiSelectBS.next(list);
    } else {
      this._selectBS.next(node);
    }
  }

  deleteStructure(node: StructuredFlatNode, $event: MouseEvent): void {
    $event.stopPropagation();
    this._deleteStructureBS.next({
      resId: node.resId,
      name: node.name,
    } as any);
  }

  editStructure(node: StructuredFlatNode, $event: MouseEvent): void {
    $event.stopPropagation();
    this._editStructureBS.next({
      resId: node.resId,
      name: node.name,
    } as any);
  }

  isSelected(node: StructuredFlatNode): boolean {
    if (this.isMultiSelectable) {
      return (this.valuesSelected as Structure[]).findIndex(s => s.resId === node.resId) !== -1;
    } else {
      return (this.valueSelected as Structure)?.resId === node?.resId;
    }
  }
}

interface StructuredFlatNode {
  name: string;
  resId: string;
  level: number;
  status: Enums.Structure.StatusEnum;
  code: string;
  expandable: boolean;
  parent: StructuredFlatNode;
}

interface StructureNode {
  resId: string;
  name: string;
  status: Enums.Structure.StatusEnum;
  code: string;
  children?: StructureNode[];
}
