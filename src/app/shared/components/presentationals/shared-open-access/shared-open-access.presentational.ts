/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-open-access.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {
  Enums,
} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  ColorUtil,
  EnumUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-open-access",
  templateUrl: "./shared-open-access.presentational.html",
  styleUrls: ["./shared-open-access.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedOpenAccessPresentational extends SharedAbstractPresentational {
  _openAccess: Enums.Facet.FacetValue.OpenAccessValue;
  labelToTranslate: string;
  backgroundColor: string;
  borderColor: string;

  @Input()
  set openAccess(value: Enums.Facet.FacetValue.OpenAccessValue) {
    this._openAccess = value;
    this._computeLabel(value);
    this._computeColor(value);
  }

  get openAccess(): Enums.Facet.FacetValue.OpenAccessValue {
    return this._openAccess;
  }

  @Input()
  withTooltip: boolean = false;

  private _computeLabel(diffusion: Enums.Facet.FacetValue.OpenAccessValue): void {
    this.labelToTranslate = EnumUtil.getLabel(Enums.Facet.FacetValue.OpenAccessValueTranslate, diffusion);
  }

  private _computeColor(diffusion: Enums.Facet.FacetValue.OpenAccessValue): void {
    this.borderColor = Enums.Facet.FacetValue.OpenAccessValueTranslate.find(d => d.key === diffusion)?.backgroundColorHexa;
    this.backgroundColor = ColorUtil.addAlpha(this.borderColor, 0.4);
  }
}
