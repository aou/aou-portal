/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-abstract.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Directive} from "@angular/core";
import {
  FloatLabelType,
  MatFormFieldAppearance,
} from "@angular/material/form-field";
import {environment} from "@environments/environment";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HintTranslateEnum} from "@shared/enums/hint-translate.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TooltipTranslateEnum} from "@shared/enums/tooltip-translate.enum";
import {
  AbstractPresentational,
  EnumUtil,
} from "solidify-frontend";

@Directive()
export abstract class SharedAbstractPresentational extends AbstractPresentational {
  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get appearanceInputMaterial(): MatFormFieldAppearance {
    return environment.appearanceInputMaterial;
  }

  get positionLabelInputMaterial(): FloatLabelType {
    return environment.positionLabelInputMaterial;
  }

  get tooltipTranslateEnum(): typeof TooltipTranslateEnum {
    return TooltipTranslateEnum;
  }

  get hintTranslateEnum(): typeof HintTranslateEnum {
    return HintTranslateEnum;
  }
}
