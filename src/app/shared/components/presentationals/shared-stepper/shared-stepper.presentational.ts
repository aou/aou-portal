/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-stepper.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  OnInit,
  Output,
} from "@angular/core";
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  Router,
} from "@angular/router";
import {Enums} from "@enums";
import {Store} from "@ngxs/store";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {Step} from "@shared/models/step.model";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractContainer,
  isNotNullNorUndefined,
  isTrue,
  ObservableUtil,
  ScrollService,
} from "solidify-frontend";

@Component({
  selector: "aou-shared-stepper",
  templateUrl: "./shared-stepper.presentational.html",
  styleUrls: ["./shared-stepper.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedStepperPresentational extends AbstractContainer implements OnInit {
  @Input()
  steps: Step[];

  @Input()
  initialStepActiveId: string | undefined;

  @Input()
  isLoading: boolean;

  @Input()
    // tslint:disable-next-line:semicolon
  suffixUrlMatcher: (route: ActivatedRoute) => string = (route: ActivatedRoute) => route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);

  private readonly _desireGoToStepBS: BehaviorSubject<Enums.Deposit.TargetRoutingAfterSaveEnum> = new BehaviorSubject<Enums.Deposit.TargetRoutingAfterSaveEnum>(undefined);
  @Output("desireGoToStepChange")
  readonly desireGoToStepObs: Observable<Enums.Deposit.TargetRoutingAfterSaveEnum> = ObservableUtil.asObservable(this._desireGoToStepBS);

  currentStep: Step;

  get currentStepIndex(): number {
    return this.steps.findIndex(s => s.id === this.currentStep?.id);
  }

  @Input()
  stepTourAnchor: string;

  get currentStepValid(): boolean {
    const isInvalid = this?.currentStep?.stepFormAbstractControl?.invalid;
    return !isTrue(isInvalid);
  }

  constructor(private readonly _store: Store,
              private readonly _router: Router,
              private readonly _route: ActivatedRoute,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _scrollService: ScrollService,
              private readonly _zone: NgZone) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this._zone.onStable.pipe(take(1)), s => this._scrollService.scrollToTop());
    if (isNotNullNorUndefined(this.initialStepActiveId)) {
      this.currentStep = this.steps.find(t => t.id === this.initialStepActiveId);
    }
    this._computeCurrentStepFromRoute();
    this.subscribe(this._router.events
      .pipe(
        filter(event => event instanceof NavigationCancel || event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this._computeCurrentStepFromRoute();
          this.subscribe(this._zone.onStable.pipe(take(1)), s => setTimeout(() => this._scrollService.scrollToTop(), 0));
        }),
      ),
    );
  }

  trackByFn(index: number, step: Step): string {
    return step.id;
  }

  private _computeCurrentStepFromRoute(): Step | undefined {
    const stepRouteSelected = this.suffixUrlMatcher(this._route);
    const stepIndex = this.steps.findIndex(t => t.suffixUrl === stepRouteSelected);
    if (stepIndex === -1) {
      return undefined;
    }
    this.currentStep = this.steps[stepIndex];
    this._changeDetector.detectChanges();
    return this.currentStep;
  }

  desireGoTo(step: Step): void {
    if (!this.currentStepValid) {
      return;
    }
    if (this.currentStep === step) {
      return;
    }
    const currentStepIndex = this.steps.findIndex(s => s.id === this.currentStep.id);
    const desiredStepIndex = this.steps.findIndex(s => s.id === step.id);
    const diff: Enums.Deposit.TargetRoutingAfterSaveEnum = desiredStepIndex - currentStepIndex;
    if (diff > 1) {
      return;
    }
    this._desireGoToStepBS.next(diff);
  }

  triggerDetectChanges(): void {
    this._changeDetector.detectChanges();
  }
}
