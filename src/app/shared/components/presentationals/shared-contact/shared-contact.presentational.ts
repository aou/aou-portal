/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-contact.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {environment} from "@environments/environment";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";

@Component({
  selector: "aou-shared-contact",
  templateUrl: "./shared-contact.presentational.html",
  styleUrls: ["./shared-contact.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedContactPresentational extends SharedAbstractPresentational implements OnInit {
  private _LOCAL_PREFIX: string = "0";
  private _GLOBAL_PREFIX: string = "+41 ";

  email: string = environment.contactEmail;
  phone: string = environment.contactPhone;

  phoneWithIndicator: string;

  ngOnInit(): void {
    super.ngOnInit();
    this._computePhoneWithIndicator();
  }

  private _computePhoneWithIndicator(): void {
    let phone = this.phone;
    phone = phone.replace("(", "");
    phone = phone.replace(")", "");
    if (phone.startsWith(this._LOCAL_PREFIX)) {
      phone = this._GLOBAL_PREFIX + phone.substring(this._LOCAL_PREFIX.length, phone.length);
    }
    this.phoneWithIndicator = phone;
  }
}
