/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - home-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {AppSearchAction} from "@app/stores/search/app-search.action";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  switchMap,
} from "rxjs";
import {
  filter,
  map,
  take,
} from "rxjs/operators";
import {
  MemoizedUtil,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class HomeGuardService implements CanActivate {
  constructor(public readonly router: Router,
              private readonly _store: Store,
              private readonly _actions$: Actions) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return MemoizedUtil.select(this._store, AppState, state => state.isApplicationInitialized).pipe(
      filter(isApplicationInitialized => isApplicationInitialized),
      take(1),
      switchMap(() => this._shouldNavigate()),
    );
  }

  private _shouldNavigate(): Observable<boolean> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AppSearchAction.GetAllSearchCriteria(),
      AppSearchAction.GetAllSearchCriteriaSuccess,
      () => {},
      AppSearchAction.GetAllSearchCriteriaFail,
      () => {}).pipe(
      map(() => true),
    );
  }
}
