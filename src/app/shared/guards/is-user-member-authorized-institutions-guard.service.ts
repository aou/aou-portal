/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - is-user-member-authorized-institutions-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
  switchMap,
} from "rxjs";
import {
  filter,
  map,
  take,
} from "rxjs/operators";
import {
  isFalse,
  isNotNullNorUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class IsUserMemberAuthorizedInstitutionsGuardService implements CanActivate {
  constructor(private readonly _store: Store,
              private readonly _securityService: SecurityService) {
  }

  canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
    return MemoizedUtil.select(this._store, AppState, state => state.isApplicationInitialized).pipe(
      filter(isApplicationInitialized => isApplicationInitialized),
      take(1),
      switchMap(() => this._shouldNavigate(activatedRoute, routerState)),
    );
  }

  private _shouldNavigate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
    const isLoggedIn = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isFalse(isLoggedIn)) {
      return of(false);
    }
    const userAuthorizedForDepositActions = MemoizedUtil.selectSnapshot(this._store, AppUserState, state => state.userAuthorizedForDepositActions);
    if (isNotNullNorUndefined(userAuthorizedForDepositActions)) {
      return of(this._isUserAuthorizedForDepositActions(userAuthorizedForDepositActions));
    }
    return MemoizedUtil.select(this._store, AppUserState, state => state.userAuthorizedForDepositActions).pipe(
      filter(isUserAuthorizedForDepositActions => isNotNullNorUndefined(isUserAuthorizedForDepositActions)),
      take(1),
      map(isUserAuthorizedForDepositActions => this._isUserAuthorizedForDepositActions(isUserAuthorizedForDepositActions)),
    );
  }

  private _isUserAuthorizedForDepositActions(isUserAuthorizedForDepositActions: boolean): boolean {
    if (isFalse(isUserAuthorizedForDepositActions)) {
      this._store.dispatch(new Navigate([RoutesEnum.homePage]));
    }
    return isUserAuthorizedForDepositActions;
  }
}
