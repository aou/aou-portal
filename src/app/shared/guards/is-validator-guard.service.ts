/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - is-validator-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from "@angular/router";
import {Store} from "@ngxs/store";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {take} from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class IsValidatorGuardService implements CanActivate {
  constructor(private readonly _store: Store,
              private readonly _securityService: SecurityService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state1: RouterStateSnapshot): Observable<boolean> {
    return this._securityService.canAccessToValidateDepositMenuObs().pipe(take(1));
  }
}
