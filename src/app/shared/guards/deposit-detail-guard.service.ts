/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - deposit-detail-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {DepositAction} from "@app/features/deposit/stores/deposit.action";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {AppState} from "@app/stores/app.state";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {TourRouteIdEnum} from "@shared/enums/tour-route-id.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
} from "rxjs";
import {
  map,
  switchMap,
} from "rxjs/operators";
import {
  ApiService,
  isFalse,
  isTrue,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class DepositDetailGuardService implements CanActivate {
  constructor(private readonly _router: Router,
              private readonly _apiService: ApiService,
              private readonly _notificationService: NotificationService,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _securityService: SecurityService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const depositId: string = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    const isInTourMode = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode);
    const isDepositIdForTour = depositId === TourRouteIdEnum.tourDepositId;
    if (isDepositIdForTour || isInTourMode) {
      const isAuthorizedToDisplayTour = isDepositIdForTour && isInTourMode;
      if (!isAuthorizedToDisplayTour) {
        this._store.dispatch(new Navigate([RoutesEnum.deposit]));
      }
      return of(isAuthorizedToDisplayTour);
    }
    const isDepositInValidation = this._isDepositInValidation(route);
    let exists = false;
    let canValidate = false;

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new DepositAction.GetById(depositId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.GetByIdSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.GetByIdFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (isFalse(result.success)) {
          return false;
        }
        const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
        exists = true;
        return deposit?.resId === depositId;
      }),
      switchMap(success => {
        if (isTrue(success) && isDepositInValidation) {
          const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
          return this._securityService.canSubmitDepositObs(deposit.resId).pipe(map(result => {
            canValidate = result;
            return result;
          }));
        } else {
          return of(success);
        }
      }),
      map(success => {
        if (isTrue(success)) {
          return true;
        }
        let redirectRoute: string[];

        if (isDepositInValidation) {
          redirectRoute = [RoutesEnum.depositToValidate];
        } else {
          redirectRoute = [RoutesEnum.deposit];
        }
        this._store.dispatch(new Navigate(redirectRoute));
        if (exists && isDepositInValidation && canValidate === false) {
          this._notificationService.showError(LabelTranslateEnum.youDontHaveAnyValidationRightOnThisDeposit);
        } else {
          this._notificationService.showError(LabelTranslateEnum.thisDepositDoesNotExistOrHasBeenDeleted);
        }
        return false;
      }),
    );
  }

  private _isDepositInValidation(route: ActivatedRouteSnapshot): boolean {
    return route.parent.url.length > 0 && route.parent.url[0].path === AppRoutesEnum.depositToValidate;
  }
}
