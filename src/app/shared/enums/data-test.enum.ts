/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - data-test.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DataTestPartialEnum} from "solidify-frontend";

enum DataTestExtendEnum {
  // Auth
  loginHorizontalInput = "login-horizontal-input",
  loginVerticalInput = "login-vertical-input",
  loginMobileInput = "login-mobile-input",
  logoutMobileInput = "logout-mobile-input",

  // Link Angular Module
  linkMenuDeposit = "link-menu-deposit",
  linkMenuPublication = "link-menu-publication",
  linkMenuPublicationToValidate = "link-menu-publication-to-validate",
  linkMenuAdmin = "link-menu-admin",
  linkMenuPreservationPlanning = "link-menu-preservation-planning",
  linkMenuPreservationSpace = "link-menu-preservation-space",
  linkMenuOrder = "link-menu-order",
  linkMenuHome = "link-menu-home",
  linkMenuPinboard = "link-menu-pinboard",
  linkMenuNotification = "link-menu-notification",

  // Main Button
  back = "back",
  create = "create",
  save = "save",
  delete = "delete",

  // Home
  homeSearchInput = "home-search-input",
  homeDataTableSearch = "home-data-table-search",
  homePinboardDataTablePinned = "home-pinboard-data-table-pinned",
  homeTile = "home-tile",
  homeButtonViewTile = "home-button-view-tile",
  homeButtonViewList = "home-button-view-list",
  homeFacet = "home-facet",

  // Deposit
  depositDataTable = "deposit-data-table",
  depositTitle = "deposit-title",
  depositDescription = "deposit-description",
  depositPublicationDate = "deposit-publicationDate",
  depositAccessLevel = "deposit-accessLevel",
  depositDataSensitivity = "deposit-dataSensitivity",
  depositAddMeAuthor = "deposit-addMeAuthor",
  depositLanguage = "deposit-language",
  depositLicenseId = "deposit-licenseId",
  depositSubmissionPolicy = "deposit-submissionPolicy",
  depositPreservationPolicy = "deposit-preservationPolicy",
  depositDataType = "deposit-data-type",
  depositFileDataTable = "deposit-file-data-table",
  depositDataCategory = "deposit-data-category",

  // Preservation Space
  preservationSpaceTabOrgUnit = "preservationSpace-tab-orgUnit",
  preservationSpaceTabContributor = "preservationSpace-tab-contributor",
  preservationSpaceTabRequestReceived = "preservationSpace-tab-requestReceived",
  preservationSpaceTabRequestSent = "preservationSpace-tab-requestSent",

  // Preservation Space Org Unit
  preservationSpaceOrgUnitName = "preservationSpace-orgUnit-name",
  preservationSpaceOrgUnitListSearchName = "preservationSpace-orgUnit-list-searchName",
  preservationSpaceOrgUnitButtonSeeDeposit = "preservationSpace-orgUnit-button-seeDeposit",
  preservationSpaceOrgUnitButtonSeeReceivedRequest = "preservationSpace-orgUnit-button-seeReceivedRequest",
  preservationSpaceOrgUnitButtonAskJoinOrgUnit = "preservationSpace-orgUnit-button-askJoinOrgUnit",
  preservationSpaceOrgUnitButtonAskCreationOrgUnit = "preservationSpace-orgUnit-button-askCreationOrgUnit",
  preservationSpaceOrgUnitAskCreationName = "preservationSpace-orgUnit-askCreation-name",
  preservationSpaceOrgUnitAskCreationMessage = "preservationSpace-orgUnit-askCreation-message",
  preservationSpaceOrgUnitAskCreationSubmit = "preservationSpace-orgUnit-askCreation-submit",
  preservationSpaceOrgUnitAskJoinRole = "preservationSpace-orgUnit-askJoin-role",
  preservationSpaceOrgUnitAskJoinMessage = "preservationSpace-orgUnit-askJoin-message",
  preservationSpaceOrgUnitAskJoinSubmit = "preservationSpace-orgUnit-askJoin-submit",

  // Admin Home
  adminTileOrganizationalUnit = "admin-tile-organizational-unit",
  adminTileSubmissionPolicy = "admin-tile-submission-policy",
  adminTileOauth2 = "admin-tile-oauth2",
  adminTileUsers = "admin-tile-users",

  // Admin Org Unit
  adminOrgUnitName = "admin-orgUnit-name",
  adminOrgUnitListSearchName = "admin-orgUnit-list-searchName",
  adminOrgUnitSubmissionPolicy = "admin-orgUnit-submissionPolicy",
  adminOrgUnitPreservationPolicy = "admin-orgUnit-preservationPolicy",

  // Shared
  sharedPersonOrgUnitRoleButtonAdd = "shared-personOrgUnitRole-buttonAdd",
  sharedPersonOrgUnitRoleButtonDelete = "shared-personOrgUnitRole-buttonDelete",
  sharedPersonOrgUnitRoleInputRole = "shared-personOrgUnitRole-inputRole",
  sharedPersonOrgUnitRoleInputOrgUnit = "shared-personOrgUnitRole-inputOrgUnit",
  sharedPersonOrgUnitRoleInputPerson = "shared-personOrgUnitRole-inputPerson",
}

// tslint:disable-next-line:variable-name
export const DataTestEnum = {...DataTestPartialEnum, ...DataTestExtendEnum} as Omit<typeof DataTestPartialEnum, keyof typeof DataTestExtendEnum> & typeof DataTestExtendEnum;
export type DataTestEnum = typeof DataTestEnum[keyof typeof DataTestEnum];
