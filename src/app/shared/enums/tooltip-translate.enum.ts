/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - tooltip-translate.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "solidify-frontend";

export class TooltipTranslateEnum {
  static depositType: string = MARK_AS_TRANSLATABLE("tooltip.deposit.type");
  static depositContributorsIsCollaboration: string = MARK_AS_TRANSLATABLE("tooltip.deposit.contributorsIsCollaboration");
  static depositStructure: string = MARK_AS_TRANSLATABLE("tooltip.deposit.structure");
  static depositGroup: string = MARK_AS_TRANSLATABLE("tooltip.deposit.group");
  static depositSubType: string = MARK_AS_TRANSLATABLE("tooltip.deposit.subType");
  static depositSubsubtype: string = MARK_AS_TRANSLATABLE("tooltip.deposit.subsubtype");
  static depositLanguages: string = MARK_AS_TRANSLATABLE("tooltip.deposit.languages");
  static depositLanguagesLanguage: string = MARK_AS_TRANSLATABLE("tooltip.deposit.languagesLanguage");
  static depositTitle: string = MARK_AS_TRANSLATABLE("tooltip.deposit.title");
  static depositTitleText: string = MARK_AS_TRANSLATABLE("tooltip.deposit.titleText");
  static depositTitleLanguage: string = MARK_AS_TRANSLATABLE("tooltip.deposit.titleLanguage");
  static depositOriginalTitle: string = MARK_AS_TRANSLATABLE("tooltip.deposit.originalTitle");
  static depositOriginalTitleText: string = MARK_AS_TRANSLATABLE("tooltip.deposit.originalTitleText");
  static depositOriginalTitleLanguage: string = MARK_AS_TRANSLATABLE("tooltip.deposit.originalTitleLanguage");
  static depositContainerTitle: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerTitle");
  static depositIdentifierIssn: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierIssn");
  static depositPublisher: string = MARK_AS_TRANSLATABLE("tooltip.deposit.publisher");
  static depositPublisherPlacePublication: string = MARK_AS_TRANSLATABLE("tooltip.deposit.publisherPlacePublication");
  static depositPublisherPublishingHouse: string = MARK_AS_TRANSLATABLE("tooltip.deposit.publisherPublishingHouse");
  static depositDates: string = MARK_AS_TRANSLATABLE("tooltip.deposit.dates");
  static depositContainer: string = MARK_AS_TRANSLATABLE("tooltip.deposit.container");
  static depositContainerVolume: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerVolume");
  static depositContainerIssue: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerIssue");
  static depositContainerSpecialIssue: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerSpecialIssue");
  static depositContainerEditor: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerEditor");
  static depositContainerConferenceTitle: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerConferenceTitle");
  static depositContainerConferencePlace: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerConferencePlace");
  static depositContainerConferenceDate: string = MARK_AS_TRANSLATABLE("tooltip.deposit.containerConferenceDate");
  static depositPages: string = MARK_AS_TRANSLATABLE("tooltip.deposit.pages");
  static depositPagesPaging: string = MARK_AS_TRANSLATABLE("tooltip.deposit.pagesPaging");
  static depositPagesNumberPagesArticleNumber: string = MARK_AS_TRANSLATABLE("tooltip.deposit.pagesNumberPagesArticleNumber");
  static depositEdition: string = MARK_AS_TRANSLATABLE("tooltip.deposit.edition");
  static depositIdentifierIsbn: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierIsbn");
  static depositIdentifierDoi: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierDoi");
  static depositIdentifier: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifier");
  static depositIdentifierPmid: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierPmid");
  static depositIdentifierPmcid: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierPmcid");
  static depositIdentifierArXiv: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierArXiv");
  static depositIdentifierDblp: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierDblp");
  static depositIdentifierRepEc: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierRepEc");
  static depositIdentifierMmsid: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierMmsid");
  static depositIdentifierUrn: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierUrn");
  static depositCommercialUrl: string = MARK_AS_TRANSLATABLE("tooltip.deposit.commercialUrl");
  static depositAbstract: string = MARK_AS_TRANSLATABLE("tooltip.deposit.abstract");
  static depositAbstractText: string = MARK_AS_TRANSLATABLE("tooltip.deposit.abstractText");
  static depositAbstractLanguage: string = MARK_AS_TRANSLATABLE("tooltip.deposit.abstractLanguage");
  static depositKeywords: string = MARK_AS_TRANSLATABLE("tooltip.deposit.keywords");
  static depositJel: string = MARK_AS_TRANSLATABLE("tooltip.deposit.jel");
  static depositNote: string = MARK_AS_TRANSLATABLE("tooltip.deposit.note");
  static depositCollection: string = MARK_AS_TRANSLATABLE("tooltip.deposit.collection");
  static depositCollectionName: string = MARK_AS_TRANSLATABLE("tooltip.deposit.collectionName");
  static depositCollectionNumber: string = MARK_AS_TRANSLATABLE("tooltip.deposit.collectionNumber");
  static depositIdentifierLocalNumber: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierLocalNumber");
  static depositDiscipline: string = MARK_AS_TRANSLATABLE("tooltip.deposit.discipline");
  static depositMandatedBy: string = MARK_AS_TRANSLATABLE("tooltip.deposit.mandatedBy");
  static depositFunder: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funder");
  static depositFunderFunder: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funderFunder");
  static depositFunderProjectName: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funderProjectName");
  static depositFunderProjectCode: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funderProjectCode");
  static depositFunderProjectAcronym: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funderProjectAcronym");
  static depositFunderProjectJurisdiction: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funderProjectJurisdiction");
  static depositFunderProjectProgram: string = MARK_AS_TRANSLATABLE("tooltip.deposit.funderProjectProgram");
  static depositDatasets: string = MARK_AS_TRANSLATABLE("tooltip.deposit.datasets");
  static depositDatasetsUrl: string = MARK_AS_TRANSLATABLE("tooltip.deposit.datasetsUrl");
  static depositLinks: string = MARK_AS_TRANSLATABLE("tooltip.deposit.links");
  static depositLinksTarget: string = MARK_AS_TRANSLATABLE("tooltip.deposit.linksTarget");
  static depositLinksDescription: string = MARK_AS_TRANSLATABLE("tooltip.deposit.linksDescription");
  static depositLinksDescriptionText: string = MARK_AS_TRANSLATABLE("tooltip.deposit.linksDescriptionText");
  static depositLinksDescriptionLanguage: string = MARK_AS_TRANSLATABLE("tooltip.deposit.linksDescriptionLanguage");
  static depositLinksType: string = MARK_AS_TRANSLATABLE("tooltip.deposit.linksType");
  static depositAouInternalCollection: string = MARK_AS_TRANSLATABLE("tooltip.deposit.aouInternalCollection");
  static depositCorrections: string = MARK_AS_TRANSLATABLE("tooltip.deposit.corrections");
  static depositCorrectionsNote: string = MARK_AS_TRANSLATABLE("tooltip.deposit.correctionsNote");
  static depositCorrectionsDoi: string = MARK_AS_TRANSLATABLE("tooltip.deposit.correctionsDoi");
  static depositCorrectionsPmid: string = MARK_AS_TRANSLATABLE("tooltip.deposit.correctionsPmid");
  static depositDoctorEmail: string = MARK_AS_TRANSLATABLE("tooltip.deposit.doctorEmail");
  static depositDoctorAddress: string = MARK_AS_TRANSLATABLE("tooltip.deposit.doctorAddress");
  static depositAward: string = MARK_AS_TRANSLATABLE("tooltip.deposit.award");
  static depositClassifications: string = MARK_AS_TRANSLATABLE("tooltip.deposit.classifications");
  static depositClassificationsCode: string = MARK_AS_TRANSLATABLE("tooltip.deposit.classificationsCode");
  static depositClassificationsItem: string = MARK_AS_TRANSLATABLE("tooltip.deposit.classificationsItem");
  static depositIdentifierSelector: string = MARK_AS_TRANSLATABLE("tooltip.deposit.identifierSelector");
  static depositMultiImportInputPasteIdentifiers: string = MARK_AS_TRANSLATABLE("tooltip.deposit.depositMultiImportInputPasteIdentifiers");

  static depositUploadDocumentFileType: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.documentFileType");
  static depositUploadAdditionalDescription: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.additionalDescription");
  static depositUploadAccessLevel: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.accessLevel");
  static depositUploadHasEmbargo: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.hasEmbargo");
  static depositUploadEmbargoLevel: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.embargoLevel");
  static depositUploadEmbargoDuration: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.embargoDuration");
  static depositUploadEmbargoEndDate: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.embargoEndDate");
  static depositUploadLicense: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.license");
  static depositUploadLicenseGroup: string = MARK_AS_TRANSLATABLE("tooltip.deposit.upload.licenseGroup");

  static depositContributorsType: string = MARK_AS_TRANSLATABLE("tooltip.deposit.contributors.type");
  static depositContributorsLastName: string = MARK_AS_TRANSLATABLE("tooltip.deposit.contributors.lastName");
  static depositContributorsCollaborationName: string = MARK_AS_TRANSLATABLE("tooltip.deposit.contributors.collaborationName");
  static depositContributorsFirstName: string = MARK_AS_TRANSLATABLE("tooltip.deposit.contributors.firstName");
  static depositContributorsRole: string = MARK_AS_TRANSLATABLE("tooltip.deposit.contributors.role");

  static depositPanelContributors: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.contributors");
  static depositPanelMembersOfACollaboration: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.membersOfACollaboration");
  static depositPanelMyPublishersPolicy: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.myPublishersPolicy");
  static depositPanelMainInformationOfDeposit: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.mainInformationOfDeposit");
  static depositPanelHostDeposit: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.hostDeposit");
  static depositPanelBibliographicInformation: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.bibliographicInformation");
  static depositPanelIdentifiers: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.identifiers");
  static depositPanelComplementaryDescription: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.complementaryDescription");
  static depositPanelLinks: string = MARK_AS_TRANSLATABLE("tooltip.deposit.panel.links");

  static profileFirstName: string = MARK_AS_TRANSLATABLE("tooltip.profile.firstName");
  static profileLastName: string = MARK_AS_TRANSLATABLE("tooltip.profile.lastName");
  static profileOrcid: string = MARK_AS_TRANSLATABLE("tooltip.profile.orcid");
  static profileEmail: string = MARK_AS_TRANSLATABLE("tooltip.profile.email");
  static profileHomeOrganization: string = MARK_AS_TRANSLATABLE("tooltip.profile.homeOrganization");
  static profileApplicationRole: string = MARK_AS_TRANSLATABLE("tooltip.profile.applicationRole");
  static profileNotificationTypes: string = MARK_AS_TRANSLATABLE("tooltip.profile.notificationTypes");
  static profileResearchGroups: string = MARK_AS_TRANSLATABLE("tooltip.profile.researchGroups");
  static profileStructure: string = MARK_AS_TRANSLATABLE("tooltip.profile.structure");
  static profileValidationRight: string = MARK_AS_TRANSLATABLE("tooltip.profile.validationRight");

  static withFulltextTooltip: string = MARK_AS_TRANSLATABLE("tooltip.search.withFulltextTooltip");
  static advancedSearchGenerateBibliographyDisabled: string = MARK_AS_TRANSLATABLE("tooltip.search.generateBibliographyDisabled");
}
