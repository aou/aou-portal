/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - api-action-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiActionNamePartialEnum} from "solidify-frontend";

export enum ApiActionNameExtendEnum {
  APPROVE = "approve",
  AUTHENTICATED = "authenticated",
  AUTHORIZE = "authorize",
  CHECK = "check",
  CHECK_TOKEN = "check_token",
  CONFIRM = "confirm",
  EXPORT = "export",
  EXPORT_DEPOSITS = "export-deposits",
  TOKEN = "token",
  REVOKE_ALL_TOKENS = "revoke-all-tokens",
  REVOKE_MY_TOKENS = "revoke-my-tokens",
  SEARCH_WITH_USER = "search-with-user",
  USER_ID = "externalUid",
  START_ORCID_AUTH = "start-orcid-auth",
  LANDING = "landing",
  LIST_OAUTH2_GRANT_TYPES = "list-grant-types",

  IMPORT_ALL_PROFILES_FROM_ORCID = "import-all-profiles-from-orcid",
  IMPORT_ALL_FROM_ORCID = "import-all-from-orcid",

  EXPORT_ALL_TO_ORCID = "export-all-to-orcid",
  EXPORT_TO_ORCID = "export-to-orcid",

  INITIALIZE = "initialize",
  SYNCHRONIZE = "synchronize",
  MERGE = "merge",
  REFRESH = "refresh",
  LIST_DOCUMENT_FILE_TYPES = "list-document-file-types",
  LIST_CONTRIBUTOR_ROLES = "list-contributor-roles",
  LIST_VALIDABLE_PUBLICATIONS = "list-validable-publications",
  LIST_MY_PUBLICATIONS = "list-my-publications",
  GET_MY_DEFAULT_PROFILE_DATA = "get-my-default-profile-data",
  SEARCH_UNIGE_PERSON = "search-unige-person",
  GET_ACTIVE = "get-active",
  GET_METADATA_FROM_DOI = "get-metadata-from-doi",
  GET_METADATA_FROM_ARXIV = "get-metadata-from-arxiv",
  GET_METADATA_FROM_PMID = "get-metadata-from-pmid",
  GET_METADATA_FROM_ISBN = "get-metadata-from-isbn",
  GET_ROMEO_INFOS = "get-romeo-infos",
  THUMBNAIL = "thumbnail",
  STATISTICS = "statistics",
  RATINGS = "ratings",
  RATING_BY_USER = "list-for-user",
  BY_RATING = "by-rating",
  INBOX = "inbox",
  SEARCH_INBOX = "search-inbox",
  SENT = "sent",
  SET_READ = "set-read",
  SET_UNREAD = "set-unread",
  SUBMIT_FOR_VALIDATION = "submit-for-validation",
  SEND_BACK_FOR_VALIDATION = "send-back-for-validation",
  SUBMIT = "submit",
  ALLOWED_TO_SUBMIT = "allowed-to-submit",
  REJECT = "reject",
  ASK_FEEDBACK = "ask-feedback",
  ENABLE_REVISION = "enable-revision",
  UNLINK_CONTRIBUTOR = "unlink-contributor",
  LIST_VALIDATOR_COMMENTS = "list-validator-comments",
  START_METADATA_EDITING = "start-metadata-editing",
  CANCEL_METADATA_EDITING = "cancel-metadata-editing",

  CLONE = "clone",
  ADD_VIEW = "add-view",
  ADD_DOWNLOAD = "add-download",
  GET_MY_ACLS = "get-my-acls",
  PURGE_CONTENT_TEMP_FOLDER = "purge-temporary-folder",
  LIST_CURRENT_STATUS = "list-current-status",
  GET_JOURNAL_TITLES = "get-journal-titles",
  GET_OPEN_AIRE_PROJECTS = "get-openaire-projects",
  SCHEDULE_ENABLED_TASKS = "schedule-enabled-tasks",
  DISABLE_TASKS_SCHEDULING = "disable-tasks-scheduling",
  KILL_TASK = "kill-task",
  UPDATE_PUBLICATIONS_RESEARCH_GROUP = "update-publications-researchGroup",
  UPDATE_PUBLICATIONS_STRUCTURE = "update-publications-structure",
  UPDATE_PUBLICATIONS_LANGUAGE = "update-publications-language",
  CREATE_FROM_IDENTIFIER = "create-from-identifier",
  VALIDATE_METADATA = "validate-metadata",
  CHECK_DUPLICATES = "check-duplicates",
  VALIDATE = "validate",
  RATING = "ratings",
  DL = "download",

  BY_EXTERNAL_UID = "by-external-uid",
  UPDATE_SORT_VALUES = "update-sort-values",
  LIST_PUBLICATION_USER_ROLES = "user-roles",
  SEND_TO_CANONICAL = "send-to-canonical",
  SEND_BACK_TO_COMPLETED = "send-back-to-completed",
  LIST_FACET_REQUESTS = "list-facet-requests",
  LIST_INDEX_FIELD_ALIASES = "list-index-field-aliases",
  LIST_ADVANCED_SEARCH_CRITERIA = "list-advanced-search-criteria",
  LIST_BIBLIOGRAPHY_FORMATS = "list-bibliography-formats",
  RUN_STORED_SEARCH = "run-stored-search",
  COMBINE_STORED_SEARCHES = "combine-stored-searches",
  LIST_MY_STORED_SEARCHES = "list-my-stored-searches",
  GET_BY_NAME = "get-by-name",
  GET_BY_CN_INDIVIDU = "get-by-cn-individu",
  DOWNLOAD_STATUS = "download-status",
  PREPARE_DOWNLOAD = "prepare-download",
  JAVASCRIPT = "javascript",
  GET_PENDING_METADATA_UPDATES = "get-pending-metadata-updates",
  IS_USER_MEMBER_OF_AUTHORIZED_INSTITUTION = "is-user-member-of-authorized-institution",
  ASK_CORRECTION = "ask-correction",
  GET_CONTACTABLE_CONTRIBUTORS = "contactable-contributors",
  CONTACT_CONTRIBUTOR = "contact-contributor",
  GET_UNIGE_CONTRIBUTOR_BY_ORCID = "unige-contributor-by-orcid",
}

// tslint:disable-next-line:variable-name
export const ApiActionNameEnum = {...ApiActionNamePartialEnum, ...ApiActionNameExtendEnum} as Omit<typeof ApiActionNamePartialEnum, keyof typeof ApiActionNameExtendEnum> & typeof ApiActionNameExtendEnum;
export type ApiActionNameEnum = typeof ApiActionNameEnum[keyof typeof ApiActionNameEnum];
