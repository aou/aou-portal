/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - routes.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AppRoutesPartialEnum,
  RoutesPartialEnum,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export const urlSeparator: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

enum AppRoutesExtendEnum {
  notFound = "not-found",
  notification = "notification",
  deposit = "deposit",
  depositToValidate = "deposit-to-validate",
  contributor = "contributor",
  admin = "admin",
  paramIdStatus = ":status",
  paramIdStatusWithoutPrefixParam = "status",
  paramIdDataFile = ":idFile",
  paramIdDataFileWithoutPrefixParam = "idFile",
  paramStep = ":step",
  paramStepWithoutPrefixParam = "step",
}

export const AppRoutesEnum = {...AppRoutesPartialEnum, ...AppRoutesExtendEnum} as Omit<typeof AppRoutesPartialEnum, keyof typeof AppRoutesExtendEnum> & typeof AppRoutesExtendEnum;
export type AppRoutesEnum = typeof AppRoutesEnum[keyof typeof AppRoutesEnum];

export enum HomePageRoutesEnum {
  detailUnige = "unige:",
  detailAouTest = "aou-test:",
  detail = "detail",
  notFound = "not-found",
  search = "search",
  pinboard = "pinboard",
  bibliography = "bibliography",
  contributor = "contributor",
}

export enum NotificationPageRoutesEnum {
  detail = "detail",
}

export enum DepositRoutesEnum {
  create = "create",
  detail = "detail",
  edit = "edit",
  importMultiple = "import-multiple",
  metadata = "metadata",
  data = "data",
  files = "files",
  collections = "collections",
  paramTab = ":tab",
  paramTabWithoutPrefix = "tab",
}

export enum NotificationRoutesEnum {
  detail = "detail",
}

export enum AdminRoutesEnum {
  license = "license",
  licenseCreate = "create",
  licenseEdit = "edit",
  licenseDetail = "detail",

  applicationEvent = "application-event",
  applicationEventDetail = "detail",

  researchGroup = "research-group",
  researchGroupCreate = "create",
  researchGroupEdit = "edit",
  researchGroupDetail = "detail",

  structure = "structure",
  structureCreate = "create",
  structureEdit = "edit",
  structureDetail = "detail",

  language = "language",

  user = "user",
  userDetail = "detail",
  userCreate = "create",
  userEdit = "edit",

  person = "person",
  personDetail = "detail",
  personCreate = "create",
  personEdit = "edit",

  role = "role",
  roleDetail = "detail",
  roleCreate = "create",
  roleEdit = "edit",
}

export class RoutesEnum implements RoutesPartialEnum {
  static index: string = AppRoutesEnum.index;
  static login: string = AppRoutesEnum.login;
  static maintenance: string = AppRoutesEnum.maintenance;
  static about: string = AppRoutesEnum.about;
  static releaseNotes: string = AppRoutesEnum.releaseNotes;
  static changelog: string = AppRoutesEnum.changelog;
  static serverOffline: string = AppRoutesEnum.serverOffline;
  static unableToLoadApp: string = AppRoutesEnum.unableToLoadApp;
  static colorCheck: string = AppRoutesEnum.colorCheck;
  static notFound: string = AppRoutesEnum.notFound;

  static homePage: string = AppRoutesEnum.home;
  static homeSearch: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.search;
  static homeDetail: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.detail;
  static homeDetailUnige: string = HomePageRoutesEnum.detailUnige;
  static homeDetailAouTest: string = HomePageRoutesEnum.detailAouTest;
  static homeNotFound: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.notFound;
  static homePinboard: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.pinboard;
  static homeBibliography: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.bibliography;
  static homeContributor: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.contributor;

  static contributor: string = HomePageRoutesEnum.contributor;

  static notification: string = AppRoutesEnum.notification;
  static notificationDetail: string = AppRoutesEnum.notification + urlSeparator + NotificationRoutesEnum.detail;

  static deposit: string = AppRoutesEnum.deposit;
  static depositToValidate: string = AppRoutesEnum.depositToValidate;
  static depositToValidateDetail: string = AppRoutesEnum.depositToValidate + urlSeparator + DepositRoutesEnum.detail;
  static depositCreate: string = AppRoutesEnum.deposit + urlSeparator + DepositRoutesEnum.create;
  static depositDetail: string = AppRoutesEnum.deposit + urlSeparator + DepositRoutesEnum.detail;
  static depositImportMultiple: string = AppRoutesEnum.deposit + urlSeparator + DepositRoutesEnum.importMultiple;

  static admin: string = AppRoutesEnum.admin;

  static adminLicense: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license;
  static adminLicenseCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license + urlSeparator + AdminRoutesEnum.licenseCreate;
  static adminLicenseDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license + urlSeparator + AdminRoutesEnum.licenseDetail;

  static adminIndexFieldAlias: string = AppRoutesEnum.admin + urlSeparator + AppRoutesEnum.indexFieldAlias;
  static adminIndexFieldAliasCreate: string = AppRoutesEnum.admin + urlSeparator + AppRoutesEnum.indexFieldAlias + urlSeparator + AppRoutesEnum.indexFieldAliasCreate;
  static adminIndexFieldAliasDetail: string = AppRoutesEnum.admin + urlSeparator + AppRoutesEnum.indexFieldAlias + urlSeparator + AppRoutesEnum.indexFieldAliasDetail;

  static adminGlobalBanner: string = AppRoutesEnum.admin + urlSeparator + AppRoutesEnum.globalBanner;
  static adminGlobalBannerCreate: string = AppRoutesEnum.admin + urlSeparator + AppRoutesEnum.globalBanner + urlSeparator + AppRoutesEnum.globalBannerCreate;
  static adminGlobalBannerDetail: string = AppRoutesEnum.admin + urlSeparator + AppRoutesEnum.globalBanner + urlSeparator + AppRoutesEnum.globalBannerDetail;

  static adminApplicationEvent: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.applicationEvent;
  static adminApplicationEventDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.applicationEvent + urlSeparator + AdminRoutesEnum.applicationEventDetail;

  static adminResearchGroup: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchGroup;
  static adminResearchGroupCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchGroup + urlSeparator + AdminRoutesEnum.researchGroupCreate;
  static adminResearchGroupDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchGroup + urlSeparator + AdminRoutesEnum.researchGroupDetail;

  static adminStructure: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.structure;
  static adminStructureCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.structure + urlSeparator + AdminRoutesEnum.structureCreate;
  static adminStructureDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.structure + urlSeparator + AdminRoutesEnum.structureDetail;

  static adminUser: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user;
  static adminUserCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user + urlSeparator + AdminRoutesEnum.userCreate;
  static adminUserDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user + urlSeparator + AdminRoutesEnum.userDetail;

  static adminPerson: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person;
  static adminPersonCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person + urlSeparator + AdminRoutesEnum.personCreate;
  static adminPersonDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person + urlSeparator + AdminRoutesEnum.personDetail;

  static adminRole: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role;
  static adminRoleCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role + urlSeparator + AdminRoutesEnum.roleCreate;
  static adminRoleDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role + urlSeparator + AdminRoutesEnum.roleDetail;
}
