/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - api-resource-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiResourceNamePartialEnum} from "solidify-frontend";

enum ApiResourceNameExtendEnum {
  APPLICATION_ROLES = "application-roles",
  COMMENTS = "comments",
  CONTRIBUTORS = "contributors",
  CONTRIBUTOR_ROLES = "contributor-roles",
  DOCUMENT_FILE_TYPES = "document-file-types",
  DOCUMENT_FILES = "document-files",
  DOWNLOAD_TOKEN = "download-token",
  EVENTS = "events",
  EXTERNAL_DATA = "external-data",
  INSTITUTIONS = "institutions",
  LANGUAGES = "languages",
  LICENSES = "licenses",
  LICENSE_GROUPS = "license-groups",
  GLOBAL_BANNERS = "global-banners",
  SHIBLOGIN = "shiblogin",
  MODULES = "modules",
  MONITOR = "monitor",
  PUBLICATION_SUBTYPES_DOCUMENT_FILE_TYPES = "publication-subtypes-document-file-types",
  OAI = "oai",
  OAI_PROVIDER = "oai-provider",
  OAI_SETS = "oai-sets",
  OAUTH_2_CLIENTS = "oauth2-clients",
  ORCID = "orcid",
  PEOPLE = "people",
  PUBLICATIONS = "publications",
  PUBLICATION_TYPES = "publication-types",
  PUBLICATION_SUBTYPES = "publication-subtypes",
  PUBLICATION_SUB_SUBTYPES = "publication-sub-subtypes",
  ROLES = "roles",
  RESEARCH_GROUPS = "research-groups",
  ROLE = "roles",
  STATUS_HISTORY = "status-history",
  NOTIFICATIONS = "notifications",
  NOTIFICATION_TYPES = "notification-types",
  STORED_SEARCHES = "stored-searches",
  STRUCTURES = "structures",
  SYSTEM_PROPERTIES = "system-properties",
  USERS = "users",
  VALIDATION_RIGHTS_STRUCTURES = "validation-rights-structures",
  XSL = "xsl",
  PUBLIC_METADATA = "metadata",
  SETTINGS = "settings",
  BIBLIOGRAPHY = "bibliography",
  PACKAGES = "packages",
  INDEX_FIELD_ALIASES = "index-field-aliases",
}

// tslint:disable-next-line:variable-name
export const ApiResourceNameEnum = {...ApiResourceNamePartialEnum, ...ApiResourceNameExtendEnum} as Omit<typeof ApiResourceNamePartialEnum, keyof typeof ApiResourceNameExtendEnum> & typeof ApiResourceNameExtendEnum;
export type ApiResourceNameEnum = typeof ApiResourceNameEnum[keyof typeof ApiResourceNameEnum];
