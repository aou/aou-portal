/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - icon-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  IconNamePartialEnum,
  PropertyName,
} from "solidify-frontend";

export class IconNameEnum extends IconNamePartialEnum {
  @PropertyName() static readonly researchGroup: string;
  @PropertyName() static readonly deposit: string;
  @PropertyName() static readonly depositToValidate: string;
  @PropertyName() static readonly home: string;
  @PropertyName() static readonly order: string;
  @PropertyName() static readonly preservationSpace: string;
  @PropertyName() static readonly preservationPlanning: string;
  @PropertyName() static readonly administration: string;
  @PropertyName() static readonly profile: string;
  @PropertyName() static readonly token: string;
  @PropertyName() static readonly logout: string;
  @PropertyName() static readonly login: string;
  @PropertyName() static readonly resId: string;
  @PropertyName() static readonly history: string;
  @PropertyName() static readonly docDev: string;
  @PropertyName() static readonly docUser: string;
  @PropertyName() static readonly docValidator: string;
  @PropertyName() static readonly about: string;
  @PropertyName() static readonly cart: string;
  @PropertyName() static readonly search: string;
  @PropertyName() static readonly back: string;
  @PropertyName() static readonly next: string;
  @PropertyName() static readonly refresh: string;
  @PropertyName() static readonly delete: string;
  @PropertyName() static readonly deleteAll: string;
  @PropertyName() static readonly create: string;
  @PropertyName() static readonly add: string;
  @PropertyName() static readonly addPerson: string;
  @PropertyName() static readonly addCollaboration: string;
  @PropertyName() static readonly edit: string;
  @PropertyName() static readonly archiveBrowsing: string;
  @PropertyName() static readonly download: string;
  @PropertyName() static readonly downloadLoginNeeded: string;
  @PropertyName() static readonly downloadNotAllowed: string;
  @PropertyName() static readonly sendRequest: string;
  @PropertyName() static readonly accessControlled: string;
  @PropertyName() static readonly roleValidator: string;
  @PropertyName() static readonly roleUser: string;
  @PropertyName() static readonly myOrder: string;
  @PropertyName() static readonly allOrder: string;
  @PropertyName() static readonly metadata: string;
  @PropertyName() static readonly files: string;
  @PropertyName() static readonly associateArchive: string;
  @PropertyName() static readonly collection: string;
  @PropertyName() static readonly classification: string;
  @PropertyName() static readonly navigate: string;
  @PropertyName() static readonly reserveDoi: string;
  @PropertyName() static readonly submit: string;
  @PropertyName() static readonly approve: string;
  @PropertyName() static readonly unapprove: string;
  @PropertyName() static readonly askFeedback: string;
  @PropertyName() static readonly wait: string;
  @PropertyName() static readonly save: string;
  @PropertyName() static readonly uploadFile: string;
  @PropertyName() static readonly uploadStructuredFiles: string;
  @PropertyName() static readonly filesView: string;
  @PropertyName() static readonly foldersView: string;
  @PropertyName() static readonly preview: string;
  @PropertyName() static readonly star: string;
  @PropertyName() static readonly star_empty: string;
  @PropertyName() static readonly move: string;
  @PropertyName() static readonly information: string;
  @PropertyName() static readonly contributor: string;
  @PropertyName() static readonly requestInbox: string;
  @PropertyName() static readonly requestSent: string;
  @PropertyName() static readonly sip: string;
  @PropertyName() static readonly dip: string;
  @PropertyName() static readonly aip: string;
  @PropertyName() static readonly monitoring: string;
  @PropertyName() static readonly jobs: string;
  @PropertyName() static readonly archivingStatus: string;
  @PropertyName() static readonly aipDownloaded: string;
  @PropertyName() static readonly storagion: string;
  @PropertyName() static readonly simpleChecksum: string;
  @PropertyName() static readonly doubleChecksum: string;
  @PropertyName() static readonly reindex: string;
  @PropertyName() static readonly check: string;
  @PropertyName() static readonly excluded: string;
  @PropertyName() static readonly init: string;
  @PropertyName() static readonly synchronize: string;
  @PropertyName() static readonly run: string;
  @PropertyName() static readonly resume: string;
  @PropertyName() static readonly resumeAll: string;
  @PropertyName() static readonly notIgnore: string;
  @PropertyName() static readonly orderReady: string;
  @PropertyName() static readonly orderInProgress: string;
  @PropertyName() static readonly orderInError: string;
  @PropertyName() static readonly submissionPolicies: string;
  @PropertyName() static readonly preservationPolicies: string;
  @PropertyName() static readonly disseminationPolicies: string;
  @PropertyName() static readonly licenses: string;
  @PropertyName() static readonly globalBanners: string;
  @PropertyName() static readonly institutions: string;
  @PropertyName() static readonly researchDomains: string;
  @PropertyName() static readonly users: string;
  @PropertyName() static readonly roles: string;
  @PropertyName() static readonly oaiSets: string;
  @PropertyName() static readonly oauth2Clients: string;
  @PropertyName() static readonly peoples: string;
  @PropertyName() static readonly fundingAgencies: string;
  @PropertyName() static readonly indexFieldAliases: string;
  @PropertyName() static readonly archiveAcl: string;
  @PropertyName() static readonly languages: string;
  @PropertyName() static readonly metadataTypes: string;
  @PropertyName() static readonly notifications: string;
  @PropertyName() static readonly passwordVisible: string;
  @PropertyName() static readonly passwordHide: string;
  @PropertyName() static readonly testFile: string;
  @PropertyName() static readonly folderOpened: string;
  @PropertyName() static readonly folderClosed: string;
  @PropertyName() static readonly expandAll: string;
  @PropertyName() static readonly collapseAll: string;
  @PropertyName() static readonly send: string;
  @PropertyName() static readonly clear: string;
  @PropertyName() static readonly done: string;
  @PropertyName() static readonly update: string;
  @PropertyName() static readonly emptyCart: string;
  @PropertyName() static readonly copyToClipboard: string;
  @PropertyName() static readonly notFound: string;
  @PropertyName() static readonly up: string;
  @PropertyName() static readonly down: string;
  @PropertyName() static readonly redo: string;
  @PropertyName() static readonly sort: string;
  @PropertyName() static readonly clearCache: string;
  @PropertyName() static readonly warning: string;
  @PropertyName() static readonly autoUpdate: string;
  @PropertyName() static readonly fingerprint: string;
  @PropertyName() static readonly http: string;
  @PropertyName() static readonly success: string;
  @PropertyName() static readonly error: string;
  @PropertyName() static readonly zoomOut: string;
  @PropertyName() static readonly zoomIn: string;
  @PropertyName() static readonly menuButtons: string;
  @PropertyName() static readonly orcid: string;
  @PropertyName() static readonly doi: string;
  @PropertyName() static readonly archive: string;
  @PropertyName() static readonly unigeBlack: string;
  @PropertyName() static readonly unigeWhite: string;
  @PropertyName() static readonly theme: string;
  @PropertyName() static readonly darkMode: string;
  @PropertyName() static readonly lightMode: string;
  @PropertyName() static readonly creationDate: string;
  @PropertyName() static readonly change: string;
  @PropertyName() static readonly trueValue: string;
  @PropertyName() static readonly falseValue: string;
  @PropertyName() static readonly dispose: string;
  @PropertyName() static readonly approveDisposal: string;
  @PropertyName() static readonly approveDisposalByOrgUnit: string;
  @PropertyName() static readonly extendRetention: string;
  @PropertyName() static readonly listView: string;
  @PropertyName() static readonly tilesView: string;
  @PropertyName() static readonly accessLevelRestricted: string;
  @PropertyName() static readonly accessLevelPrivate: string;
  @PropertyName() static readonly accessLevelPublic: string;
  @PropertyName() static readonly accessLevelUndefined: string;
  @PropertyName() static readonly uploadImage: string;
  @PropertyName() static readonly help: string;
  @PropertyName() static readonly maintenance: string;
  @PropertyName() static readonly offline: string;
  @PropertyName() static readonly viewNumber: string;
  @PropertyName() static readonly downloadNumber: string;
  @PropertyName() static readonly sortUndefined: string;
  @PropertyName() static readonly sortAscending: string;
  @PropertyName() static readonly sortDescending: string;
  @PropertyName() static readonly noPreview: string;
  @PropertyName() static readonly fullScreenEnter: string;
  @PropertyName() static readonly fullScreenLeave: string;
  @PropertyName() static readonly guidedTour: string;
  @PropertyName() static readonly dataSensitivity: string;
  @PropertyName() static readonly plus: string;
  @PropertyName() static readonly minus: string;
  @PropertyName() static readonly rate: string;
  @PropertyName() static readonly dataSensitivityPartiallySupportedOrange: string;
  @PropertyName() static readonly dataSensitivityPartiallySupportedRed: string;
  @PropertyName() static readonly dataSensitivityPartiallySupportedCrimson: string;
  @PropertyName() static readonly privacy: string;
  @PropertyName() static readonly structures: string;
  @PropertyName() static readonly draggable: string;
  @PropertyName() static readonly transfer: string;
  @PropertyName() static readonly addAll: string;
  @PropertyName() static readonly comments: string;
  @PropertyName() static readonly validatorComments: string;
  @PropertyName() static readonly cloneDeposit: string;
  @PropertyName() static readonly pmid: string;
  @PropertyName() static readonly events: string;
  @PropertyName() static readonly file: string;
  @PropertyName() static readonly markAsRead: string;
  @PropertyName() static readonly markAsUnread: string;
  @PropertyName() static readonly thisIsNotMyDeposit: string;
  @PropertyName() static readonly fileVersionAccepted: string;
  @PropertyName() static readonly fileVersionPublished: string;
  @PropertyName() static readonly fileVersionSubmitted: string;
  @PropertyName() static readonly fileThesisMaster: string;
  @PropertyName() static readonly filePresentationPosterPreprint: string;
  @PropertyName() static readonly fileAgreements: string;
  @PropertyName() static readonly fileSecondary: string;
  @PropertyName() static readonly fileCorrection: string;
  @PropertyName() static readonly fileOther: string;
  @PropertyName() static readonly question: string;
  @PropertyName() static readonly authorized: string;
  @PropertyName() static readonly notAuthorized: string;
  @PropertyName() static readonly tree: string;
  @PropertyName() static readonly import: string;
  @PropertyName() static readonly autoFix: string;
  @PropertyName() static readonly multiImport: string;
  @PropertyName() static readonly mail: string;
  @PropertyName() static readonly requestCorrection: string;
  @PropertyName() static readonly phone: string;
  @PropertyName() static readonly commentOrQuestion: string;
  @PropertyName() static readonly structureWithoutChild: string;
  @PropertyName() static readonly structureWithChildOpened: string;
  @PropertyName() static readonly structureWithChildClosed: string;
  @PropertyName() static readonly identifiers: string;
  @PropertyName() static readonly keyword: string;
  @PropertyName() static readonly author: string;
  @PropertyName() static readonly mandator: string;
  @PropertyName() static readonly director: string;
  @PropertyName() static readonly editor: string;
  @PropertyName() static readonly guest_editor: string;
  @PropertyName() static readonly photographer: string;
  @PropertyName() static readonly translator: string;
  @PropertyName() static readonly collaboration: string;
  @PropertyName() static readonly filePdf: string;
  @PropertyName() static readonly fileDoc: string;
  @PropertyName() static readonly fileZip: string;
  @PropertyName() static readonly fileLines: string;
  @PropertyName() static readonly fileImage: string;
  @PropertyName() static readonly fileExcel: string;
  @PropertyName() static readonly fileCsv: string;
  @PropertyName() static readonly pinboard: string;
  @PropertyName() static readonly addToPinboard: string;
  @PropertyName() static readonly removeToPinboard: string;
  @PropertyName() static readonly generateBibliography: string;
  @PropertyName() static readonly exportBibliography: string;
  @PropertyName() static readonly changeToCanonical: string;
  @PropertyName() static readonly changeToCompleted: string;
  @PropertyName() static readonly share: string;
  @PropertyName() static readonly mergeUser: string;
  @PropertyName() static readonly note: string;
  @PropertyName() static readonly datasets: string;
  @PropertyName() static readonly correction: string;
  @PropertyName() static readonly award: string;
  @PropertyName() static readonly otherTitle: string;
  @PropertyName() static readonly publisher: string;
  @PropertyName() static readonly pagination: string;
  @PropertyName() static readonly publishedIn: string;
  @PropertyName() static readonly presentedAt: string;
  @PropertyName() static readonly discipline: string;
  @PropertyName() static readonly edition: string;
  @PropertyName() static readonly facebook: string;
  @PropertyName() static readonly twitter: string;
  @PropertyName() static readonly linkedin: string;
  @PropertyName() static readonly menu: string;
  @PropertyName() static readonly excludedFacet: string;
}
