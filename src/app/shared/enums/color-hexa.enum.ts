/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - color-hexa.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ColorHexaEnum as ColorHexaPartialEnum} from "solidify-frontend";

enum ColorHexaExtendEnum {
  pinkUnige = "#CF0063",
  greenOpenAccess = "#00CF6C",
  yellow = "#DBB806",
  orangeOpenAccess = "#F48111",
  orangeEmbargoedOpenAccess = "#FFC897",
  greyClosedAccess = "#545454",
  lightGrey = "#e6e6e6",
}

export const ColorHexaEnum = {...ColorHexaPartialEnum, ...ColorHexaExtendEnum} as Omit<typeof ColorHexaPartialEnum, keyof typeof ColorHexaExtendEnum> & typeof ColorHexaExtendEnum;
export type ColorHexaEnum = typeof ColorHexaEnum[keyof typeof ColorHexaEnum];
