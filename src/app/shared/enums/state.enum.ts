/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - state.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StatePartialEnum} from "solidify-frontend";

enum StateExtendEnum {
  router = "router",

  application = "application",
  application_toc = "application_toc",
  application_contributorRoles = "application_contributorRoles",
  application_depositSubtype = "application_depositSubtype",
  application_user = "application_user",
  application_user_logo = "application_user_logo",
  application_notification = "application_notification",
  application_storedSearches = "application_storedSearches",
  application_banner = "application_banner",
  application_person = "application_person",
  application_person_validationRight = "application_person_validationRight",
  application_person_notificationType = "application_person_notificationType",
  application_person_deposit = "application_person_deposit",
  application_person_researchGroup = "application_person_researchGroup",
  application_person_structure = "application_person_structure",
  application_pinboard = "application_pinboard",
  application_search = "application_search",

  notification = "notification",

  home = "home",
  home_archive_dataFile = "home_archive_dataFile",
  home_archive_package = "home_archive_package",
  home_archive_rating = "home_archive_rating",

  deposit = "deposit",
  deposit_comment = "deposit_comment",
  deposit_commentValidator = "deposit_commentValidator",
  deposit_documentFile = "deposit_documentFile",
  deposit_documentFile_statusHistory = "deposit_documentFile_statusHistory",
  deposit_documentFile_upload = "deposit_documentFile_upload",
  deposit_structure = "deposit_structure",
  deposit_statusHistory = "deposit_statusHistory",
  deposit_person_researchGroup = "deposit_person_researchGroup",
  deposit_person_structure = "deposit_person_structure",
  deposit_researchGroup = "deposit_researchGroup",

  admin = "admin",
  admin_applicationEvent = "admin_applicationEvent",
  admin_license = "admin_license",
  admin_researchGroup = "admin_researchGroup",
  admin_researchGroup_deposit = "admin_researchGroup_deposit",
  admin_structure = "admin_structure",
  admin_user = "admin_user",
  admin_person = "admin_person",
  admin_person_structure = "admin_person_structure",
  admin_person_researchGroup = "admin_person_researchGroup",
  admin_person_role = "admin_person_role",
  admin_person_validationRightStructure = "admin_person_validationRightStructure",
  admin_role = "admin_role",

  shared = "shared",
  shared_role = "shared_role",
  shared_user = "shared_user",
  shared_person = "shared_person",
  shared_externalData = "shared_externalData",
  shared_externalData_person = "shared_externalData_person",
  shared_externalData_journalTitle = "shared_externalData_journalTitle",
  shared_externalData_openAire = "shared_externalData_openAire",
  shared_license = "shared_license",
  shared_licenseGroup = "shared_licenseGroup",
  shared_language = "shared_language",
  shared_researchGroup = "shared_researchGroup",
  shared_contributor = "shared_contributor",
  shared_publicContributor = "shared_publicContributor",
  shared_deposit = "shared_deposit",
  shared_depositType = "shared_depositType",
  shared_depositType_subType = "shared_depositType_subType",
  shared_depositSubtype = "shared_depositSubtype",
  shared_depositSub_subType = "shared_depositSub_subType",
  shared_structure = "shared_structure",
  shared_documentFileType = "shared_documentFileType",
  shared_archive = "shared_archive",
}

// tslint:disable-next-line:variable-name
export const StateEnum = {...StatePartialEnum, ...StateExtendEnum} as Omit<typeof StatePartialEnum, keyof typeof StateExtendEnum> & typeof StateExtendEnum;
export type StateEnum = typeof StateEnum[keyof typeof StateEnum];
