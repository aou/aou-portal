/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - api.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiModuleEnum} from "@app/shared/enums/api-module.enum";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {environment} from "@environments/environment";
import {AouEnvironment} from "@environments/environment.defaults.model";
import {
  isNullOrUndefinedOrWhiteString,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export class ApiKeyword {
  static PARENT_ID: string = "parentId";
}

const SEPARATOR: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;
const parentId: string = `{${ApiKeyword.PARENT_ID}}`;
const parentIdInUrl: string = SEPARATOR + parentId + SEPARATOR;

export class ApiEnum {
  static checkModuleDefine(module: keyof AouEnvironment): void {
    if (isNullOrUndefinedOrWhiteString(environment[module])) {
      // eslint-disable-next-line no-console
      console.error(`The property '${module}' is not defined. Please check that the endpoint '${this.adminModules}' return a value for module '${module}'.`);
    }
  }

  static get access(): string {
    this.checkModuleDefine("access");
    return environment.access;
  }

  static get accessContributor(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.CONTRIBUTORS;
  }

  static get accessPublicMetadata(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.PUBLIC_METADATA;
  }

  static get accessBibliography(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.BIBLIOGRAPHY;
  }

  static get accessSettings(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.SETTINGS;
  }

  static get oaiInfo(): string {
    this.checkModuleDefine("oaiInfo");
    return environment.oaiInfo;
  }

  static get index(): string {
    return environment.index;
  }

  static get admin(): string {
    if (isNullOrUndefinedOrWhiteString(environment.admin)) {
      // eslint-disable-next-line no-console
      console.error(`The property 'admin' is not defined in environment config files.`);
    }
    return environment.admin;
  }

  static get adminModules(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.MODULES;
  }

  static get adminNotifications(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.NOTIFICATIONS;
  }

  static get adminApplicationEvents(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.EVENTS;
  }

  static get adminOrcid(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ORCID;
  }

  static get adminPeople(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PEOPLE;
  }

  static get adminContributors(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.CONTRIBUTORS;
  }

  static get adminDocumentFileTypes(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.DOCUMENT_FILE_TYPES;
  }

  static get adminRoles(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ROLES;
  }

  static get adminUsers(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.USERS;
  }

  static get adminSystemProperties(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SYSTEM_PROPERTIES;
  }

  static get adminModule(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.MODULES;
  }

  static get adminLicenses(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LICENSES;
  }

  static get adminLicenseGroups(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LICENSE_GROUPS;
  }

  static get adminLanguages(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LANGUAGES;
  }

  static get adminGlobalBanners(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.GLOBAL_BANNERS;
  }

  static get adminPublications(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PUBLICATIONS;
  }

  static get adminPublicationsDocumentFile(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PUBLICATIONS + parentIdInUrl + ApiResourceNameEnum.DOCUMENT_FILES;
  }

  static get adminPublicationTypes(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PUBLICATION_TYPES;
  }

  static get adminPublicationSubtypes(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PUBLICATION_SUBTYPES;
  }

  static get adminResearchGroups(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.RESEARCH_GROUPS;
  }

  static get adminStructures(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.STRUCTURES;
  }

  static get adminExternalData(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.EXTERNAL_DATA;
  }

  static get adminContributorRoles(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.CONTRIBUTOR_ROLES;
  }

  static get adminStoredSearches(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.STORED_SEARCHES;
  }

  static get docs(): string {
    const urlAdmin = environment.admin.substring(0, environment.admin.lastIndexOf("/"));
    return urlAdmin + SEPARATOR + ApiModuleEnum.DOCS;
  }

  static get indexResourceIndexFieldAliases(): string {
    return ApiEnum.index + SEPARATOR + ApiResourceNameEnum.INDEX_FIELD_ALIASES;
  }
}
