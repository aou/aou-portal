/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  SharedPersonState,
  SharedPersonStateModel,
} from "@app/shared/stores/person/shared-person.state";
import {State} from "@ngxs/store";
import {
  SharedContributorState,
  SharedContributorStateModel,
} from "@shared/stores/contributor/shared-contributor.state";
import {
  SharedDepositSubtypeState,
  SharedDepositSubtypeStateModel,
} from "@shared/stores/deposit-subtype/shared-deposit-subtype.state";
import {
  SharedDepositTypeState,
  SharedDepositTypeStateModel,
} from "@shared/stores/deposit-type/shared-deposit-type.state";
import {
  SharedDepositState,
  SharedDepositStateModel,
} from "@shared/stores/deposit/shared-deposit.state";
import {
  SharedDocumentFileTypeState,
  SharedDocumentFileTypeStateModel,
} from "@shared/stores/document-file-type/shared-document-file-type.state";
import {
  SharedExternalDataState,
  SharedExternalDataStateModel,
} from "@shared/stores/external-data/shared-external-data.state";
import {
  SharedGlobalBannerState,
  SharedGlobalBannerStateModel,
} from "@shared/stores/global-banner/shared-global-banner.state";
import {
  SharedIndexFieldAliasState,
  SharedIndexFieldAliasStateModel,
} from "@shared/stores/index-field-alias/shared-index-field-alias.state";
import {
  SharedLanguageState,
  SharedLanguageStateModel,
} from "@shared/stores/language/shared-language.state";
import {
  SharedLicenseGroupState,
  SharedLicenseGroupStateModel,
} from "@shared/stores/license-group/shared-license-group.state";
import {
  SharedLicenseState,
  SharedLicenseStateModel,
} from "@shared/stores/license/shared-license.state";
import {
  SharedPublicContributorState,
  SharedPublicContributorStateModel,
} from "@shared/stores/public-contributor/shared-public-contributor.state";
import {
  SharedResearchGroupState,
  SharedResearchGroupStateModel,
} from "@shared/stores/research-group/shared-research-group.state";
import {
  SharedRoleState,
  SharedRoleStateModel,
} from "@shared/stores/role/shared-role.state";
import {
  SharedStructureState,
  SharedStructureStateModel,
} from "@shared/stores/structure/shared-structure.state";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  BaseStateModel,
  SharedOaiMetadataPrefixState,
  SharedOaiMetadataPrefixStateModel,
  SharedOaiSetState,
  SharedOaiSetStateModel,
} from "solidify-frontend";
import {StateEnum} from "../enums/state.enum";

export interface SharedStateModel extends BaseStateModel {
  [StateEnum.shared_externalData]: SharedExternalDataStateModel;
  [StateEnum.shared_person]: SharedPersonStateModel;
  [StateEnum.shared_publicContributor]: SharedPublicContributorStateModel;
  [StateEnum.shared_role]: SharedRoleStateModel;
  [StateEnum.shared_contributor]: SharedContributorStateModel;
  [StateEnum.shared_language]: SharedLanguageStateModel;
  [StateEnum.shared_license]: SharedLicenseStateModel;
  [StateEnum.shared_licenseGroup]: SharedLicenseGroupStateModel;
  [StateEnum.shared_globalBanner]: SharedGlobalBannerStateModel;
  [StateEnum.shared_structure]: SharedStructureStateModel;
  [StateEnum.shared_researchGroup]: SharedResearchGroupStateModel;
  [StateEnum.shared_deposit]: SharedDepositStateModel;
  [StateEnum.shared_depositType]: SharedDepositTypeStateModel;
  [StateEnum.shared_depositSubtype]: SharedDepositSubtypeStateModel;
  [StateEnum.shared_documentFileType]: SharedDocumentFileTypeStateModel;
  [StateEnum.shared_indexFieldAlias]: SharedIndexFieldAliasStateModel;
  [StateEnum.shared_oaiSet]: SharedOaiSetStateModel;
  [StateEnum.shared_oaiMetadataPrefix]: SharedOaiMetadataPrefixStateModel;
}

@Injectable()
@State<SharedStateModel>({
  name: StateEnum.shared,
  defaults: {
    isLoadingCounter: 0,
    [StateEnum.shared_externalData]: undefined,
    [StateEnum.shared_person]: undefined,
    [StateEnum.shared_publicContributor]: undefined,
    [StateEnum.shared_role]: undefined,
    [StateEnum.shared_contributor]: undefined,
    [StateEnum.shared_language]: undefined,
    [StateEnum.shared_license]: undefined,
    [StateEnum.shared_licenseGroup]: undefined,
    [StateEnum.shared_globalBanner]: undefined,
    [StateEnum.shared_structure]: undefined,
    [StateEnum.shared_researchGroup]: undefined,
    [StateEnum.shared_deposit]: undefined,
    [StateEnum.shared_depositType]: undefined,
    [StateEnum.shared_depositSubtype]: undefined,
    [StateEnum.shared_documentFileType]: undefined,
    [StateEnum.shared_indexFieldAlias]: undefined,
    [StateEnum.shared_oaiSet]: undefined,
    [StateEnum.shared_oaiMetadataPrefix]: undefined,
  },
  children: [
    SharedExternalDataState,
    SharedPersonState,
    SharedPublicContributorState,
    SharedRoleState,
    SharedUserState,
    SharedLanguageState,
    SharedLicenseState,
    SharedLicenseGroupState,
    SharedContributorState,
    SharedGlobalBannerState,
    SharedStructureState,
    SharedResearchGroupState,
    SharedDepositState,
    SharedDepositTypeState,
    SharedDepositSubtypeState,
    SharedDocumentFileTypeState,
    SharedIndexFieldAliasState,
    SharedOaiSetState,
    SharedOaiMetadataPrefixState,
  ],
})
export class SharedState {
}
