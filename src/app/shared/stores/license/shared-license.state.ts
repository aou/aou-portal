/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-license.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  SharedLicenseAction,
  sharedLicenseActionNameSpace,
} from "@app/shared/stores/license/shared-license.action";
import {
  License,
  ResearchGroup,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  isWhiteString,
  MappingObjectUtil,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {environment} from "../../../../environments/environment";
import {StateEnum} from "../../enums/state.enum";

export interface SharedLicenseStateModel extends ResourceStateModel<License> {
}

@Injectable()
@State<SharedLicenseStateModel>({
  name: StateEnum.shared_license,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {
      order: OrderEnum.ascending,
      field: SharedLicenseState.FIELD_SORT_VALUE,
    }),
  },
})
export class SharedLicenseState extends ResourceState<SharedLicenseStateModel, License> {

  static readonly FIELD_SORT_VALUE: keyof License = "sortValue";
  static readonly FIELD_SEARCH_TITLE: keyof License = "title";
  static readonly FIELD_SEARCH_ACRONYM: keyof ResearchGroup = "acronym";
  static readonly FIELD_SEARCH_OPEN_LICENSE_ID: keyof License = "openLicenseId";
  static readonly FIELD_SEARCH_RESEARCH_GROUP: keyof License = "licenseGroup";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedLicenseActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminLicenses;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(SharedLicenseAction.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceStateModel<License>>, action: SharedLicenseAction.GetAll): Observable<CollectionTyped<License>> {
    let url = this._urlResource;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const fullName = MappingObjectUtil.get(searchItems, SharedLicenseState.FIELD_SEARCH_TITLE);
    if (!isNullOrUndefined(fullName) && !isWhiteString(fullName)) {
      url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH;

      let searchValue = "";
      fullName.trim().split(" ").forEach(term => {
        if (!isWhiteString(term)) {
          searchValue = this._addTermToSearchValue(searchValue, `i-${SharedLicenseState.FIELD_SEARCH_OPEN_LICENSE_ID}~${term},i-${SharedLicenseState.FIELD_SEARCH_TITLE}~${term},i-${SharedLicenseState.FIELD_SEARCH_ACRONYM}~${term}`);
        }
      });

      MappingObjectUtil.set(searchItems, "search", searchValue);
      MappingObjectUtil.delete(searchItems, SharedLicenseState.FIELD_SEARCH_TITLE);
      MappingObjectUtil.set(searchItems, "match", "any");
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });
    return this._apiService.getCollection<License>(url, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedLicenseAction.GetAll]),
        tap((collection: CollectionTyped<License>) => {
          ctx.dispatch(new SharedLicenseAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedLicenseAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  private _addTermToSearchValue(searchValue: string, term: string): string {
    searchValue = this._prefixWithCommaWhenSearchStringNotNull(searchValue);
    searchValue += term;
    return searchValue;
  }

  private _prefixWithCommaWhenSearchStringNotNull(searchValue: string): string {
    if (searchValue.length > 0) {
      searchValue += SOLIDIFY_CONSTANTS.COMMA;
    }
    return searchValue;
  }
}
