/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-document-file-type.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  PublicationSubtypeDocumentFileTypeDTO,
  PublicationSubtypeDocumentFileTypeGroup,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {
  SharedDocumentFileTypeAction,
  sharedDocumentFileTypeActionNameSpace,
} from "@shared/stores/document-file-type/shared-document-file-type.action";
import {
  ApiService,
  defaultResourceStateInitValue,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {StateEnum} from "../../enums/state.enum";

export interface SharedDocumentFileTypeStateModel extends ResourceStateModel<PublicationSubtypeDocumentFileTypeDTO> {
  listPublicationSubtypeDocumentFileTypeGroup: PublicationSubtypeDocumentFileTypeGroup[];
}

@Injectable()
@State<SharedDocumentFileTypeStateModel>({
  name: StateEnum.shared_documentFileType,
  defaults: {
    ...defaultResourceStateInitValue(),
    listPublicationSubtypeDocumentFileTypeGroup: [],
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "sortValue", order: OrderEnum.ascending}),
  },
})
export class SharedDocumentFileTypeState extends ResourceState<SharedDocumentFileTypeStateModel, PublicationSubtypeDocumentFileTypeDTO> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedDocumentFileTypeActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    const publicationSubTypeId = this._store.selectSnapshot(DepositState.getPublicationSubTypeId);
    return ApiEnum.adminPublicationSubtypes + SOLIDIFY_CONSTANTS.URL_SEPARATOR + publicationSubTypeId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameEnum.LIST_DOCUMENT_FILE_TYPES;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(SharedDocumentFileTypeAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<SharedDocumentFileTypeStateModel>, action: SharedDocumentFileTypeAction.GetAllSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);
    const listPublicationSubtypeDocumentFileTypeGroup = this._generateGroupedList(action.list._data);

    ctx.patchState({
      total: action.list._page?.totalItems,
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listPublicationSubtypeDocumentFileTypeGroup: listPublicationSubtypeDocumentFileTypeGroup,
      queryParameters,
    });
  }

  private _generateGroupedList(list: PublicationSubtypeDocumentFileTypeDTO[]): PublicationSubtypeDocumentFileTypeGroup[] {
    const key: keyof PublicationSubtypeDocumentFileTypeGroup = "level";
    const reducedList = list.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {}) as MappingObject<Enums.DocumentFileType.FileTypeLevelEnum, PublicationSubtypeDocumentFileTypeDTO[]>;
    const listResult: PublicationSubtypeDocumentFileTypeGroup[] = [];
    MappingObjectUtil.forEach(reducedList, (item: PublicationSubtypeDocumentFileTypeDTO[], level: Enums.DocumentFileType.FileTypeLevelEnum) => {
      listResult.push({level: level, publicationSubtypeDocumentFileTypes: item});
    });
    return listResult.sort((a: PublicationSubtypeDocumentFileTypeGroup, b: PublicationSubtypeDocumentFileTypeGroup) =>
      Enums.DocumentFileType.GetFileTypeLevelSortValue(a.level) - Enums.DocumentFileType.GetFileTypeLevelSortValue(b.level));
  }

}
