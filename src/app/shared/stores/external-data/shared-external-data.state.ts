/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-external-data.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {DepositFormModel} from "@app/features/deposit/models/deposit-form-definition.model";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {
  ContributorRockSolid,
  Romeo,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedExternalDataJournalTitleState} from "@shared/stores/external-data/external-data-journal/shared-external-data-journal-title.state";
import {SharedExternalDataOpenAireState} from "@shared/stores/external-data/external-data-open-aire/shared-external-data-open-aire.state";
import {SharedExternalDataPersonState} from "@shared/stores/external-data/external-data-person/shared-external-data-person.state";
import {SharedExternalDataAction} from "@shared/stores/external-data/shared-external-data.action";
import {ErrorDtoUtil} from "@shared/utils/error-dto.util";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  isNonEmptyString,
  isNotNullNorUndefinedNorWhiteString,
  MappingObjectUtil,
  NotificationService,
  ResourceStateModel,
  SnackbarAction,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

const urlSeparator = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

export interface SharedExternalDataStateModel extends BaseStateModel {
  total: number;
  list: ContributorRockSolid[];
  isLoadingCounter: number;
  romeo: Romeo;
}

@Injectable()
@State<SharedExternalDataStateModel>({
  name: StateEnum.shared_externalData,
  defaults: {
    total: 0,
    list: undefined,
    isLoadingCounter: 0,
    romeo: undefined,
  },
  children: [
    SharedExternalDataJournalTitleState,
    SharedExternalDataOpenAireState,
    SharedExternalDataPersonState,
  ],
})
export class SharedExternalDataState extends BasicState<SharedExternalDataStateModel> {
  private readonly _ERROR_CODE_ALREADY_EXISTING_DEPOSIT: number = 1;
  private readonly _ADDITIONAL_PARAMETERS_RES_ID: string = "resId";
  private readonly _ADDITIONAL_PARAMETERS_URL: string = "url";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super();
  }

  protected get _urlResource(): string {
    return ApiEnum.adminExternalData;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedExternalDataAction.GetMetadataFromService)
  getMetadataFromService(ctx: SolidifyStateContext<SharedExternalDataStateModel>, action: SharedExternalDataAction.GetMetadataFromService): Observable<DepositFormModel> {
    const map = new Map<string, string>();
    map.set(action.getMetadataFromServiceInfo.identifierName, action.getMetadataFromServiceInfo.identifier);
    map.set("format", "json");

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<DepositFormModel>(this._urlResource + urlSeparator + action.getMetadataFromServiceInfo.endpoint,
      MappingObjectUtil.toObject(map))
      .pipe(
        tap(depositFormModel => {
          ctx.dispatch(new SharedExternalDataAction.GetMetadataFromServiceSuccess(action, depositFormModel));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedExternalDataAction.GetMetadataFromServiceFail(action, error.error));
          if (ErrorDtoUtil.isErrorCodeOfAlreadyExistingDeposit(error.error)) {
            throw environment.errorToSkipInErrorHandler;
          }
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedExternalDataAction.GetMetadataFromServiceSuccess)
  getMetadataFromServiceSuccess(ctx: SolidifyStateContext<SharedExternalDataStateModel>, action: SharedExternalDataAction.GetMetadataFromServiceSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedExternalDataAction.GetMetadataFromServiceFail)
  getMetadataFromServiceFail(ctx: SolidifyStateContext<SharedExternalDataStateModel>, action: SharedExternalDataAction.GetMetadataFromServiceFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    const errorDto = action.error;
    if (!ErrorDtoUtil.isErrorCodeOfAlreadyExistingDeposit(errorDto)) {
      return;
    }
    const isResIdPresent = ErrorDtoUtil.isResIdPresent(errorDto);
    const isUrlPresent = ErrorDtoUtil.isUrlPresent(errorDto);
    const resId = ErrorDtoUtil.getResId(errorDto);
    const url = ErrorDtoUtil.getUrl(errorDto);

    let snackbarAction: SnackbarAction = undefined;
    if (isResIdPresent && isNotNullNorUndefinedNorWhiteString(resId)) {
      snackbarAction = {
        text: LabelTranslateEnum.seeTheExistingDocument,
        callback: () => ctx.dispatch(new Navigate([RoutesEnum.depositDetail, resId])),
      };
    } else if (isUrlPresent && isNotNullNorUndefinedNorWhiteString(url)) {
      snackbarAction = {
        text: LabelTranslateEnum.seeTheExistingDocument,
        callback: () => SsrUtil.window?.open(url, "_blank"),
      };
    }
    this._notificationService.showInformation(errorDto.message, undefined, snackbarAction);
  }

  @Action(SharedExternalDataAction.GetRomeoByIssn)
  getRomeoByIssn(ctx: SolidifyStateContext<SharedExternalDataStateModel>, action: SharedExternalDataAction.GetRomeoByIssn): Observable<Romeo> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    if (!isNonEmptyString(action.issn)) {
      ctx.dispatch(new SharedExternalDataAction.GetRomeoByIssnFail(action));
      return;
    }
    const map = new Map<string, string>();
    map.set("issn", action.issn);

    return this._apiService.get<Romeo>(this._urlResource + urlSeparator + ApiActionNameEnum.GET_ROMEO_INFOS,
      MappingObjectUtil.toObject(map))
      .pipe(
        tap(romeo => {
          ctx.dispatch(new SharedExternalDataAction.GetRomeoByIssnSuccess(action, romeo));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedExternalDataAction.GetRomeoByIssnFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedExternalDataAction.GetRomeoByIssnSuccess)
  getRomeoByIssnSuccess(ctx: SolidifyStateContext<SharedExternalDataStateModel>, action: SharedExternalDataAction.GetRomeoByIssnSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      romeo: action.romeo,
    });
  }

  @Action(SharedExternalDataAction.GetRomeoByIssnFail)
  getRomeoByIssnFail(ctx: SolidifyStateContext<SharedExternalDataStateModel>, action: SharedExternalDataAction.GetRomeoByIssnFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      romeo: undefined,
    });
  }
}
