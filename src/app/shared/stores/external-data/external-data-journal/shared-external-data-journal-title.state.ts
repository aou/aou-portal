/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-external-data-journal-title.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {JournalTitleDTO} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedExternalDataJournalTitleAction,
  sharedExternalDataJournalTitleActionNameSpace,
} from "@shared/stores/external-data/external-data-journal/shared-external-data-journal-title.action";
import {
  ApiService,
  defaultResourceStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

const urlSeparator = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

export interface SharedExternalDataJournalTitleStateModel extends ResourceStateModel<JournalTitleDTO | any> {
}

@Injectable()
@State<SharedExternalDataJournalTitleStateModel>({
  name: StateEnum.shared_externalData_journalTitle,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedExternalDataJournalTitleState extends ResourceState<SharedExternalDataJournalTitleStateModel, JournalTitleDTO | any> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedExternalDataJournalTitleActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminExternalData + urlSeparator + ApiActionNameEnum.GET_JOURNAL_TITLES;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(SharedExternalDataJournalTitleAction.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<SharedExternalDataJournalTitleStateModel>, action: SharedExternalDataJournalTitleAction.GetAllFail): void {
    super.getAllFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.externalService.unableToFindJournalTitlesData"));
  }
}
