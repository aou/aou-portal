/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-external-data.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {GetMetadataFromServiceInfo} from "@app/features/deposit/components/presentationals/deposit-form-first-step-type/deposit-form-first-step-type.presentational";
import {DepositFormModel} from "@app/features/deposit/models/deposit-form-definition.model";
import {Romeo} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  ErrorDto,
} from "solidify-frontend";

const state = StateEnum.shared_externalData;

export namespace SharedExternalDataAction {
  export class GetMetadataFromService extends BaseAction {
    static readonly type: string = `[${state}] Get Metadata From Service`;

    constructor(public getMetadataFromServiceInfo: GetMetadataFromServiceInfo) {
      super();
    }
  }

  export class GetMetadataFromServiceSuccess extends BaseSubActionSuccess<GetMetadataFromService> {
    static readonly type: string = `[${state}] Get Metadata From Service Success`;

    constructor(public parentAction: GetMetadataFromService, public depositFormModel: DepositFormModel) {
      super(parentAction);
    }
  }

  export class GetMetadataFromServiceFail extends BaseSubActionFail<GetMetadataFromService> {
    static readonly type: string = `[${state}] Get Metadata From Service Fail`;

    constructor(public parentAction: GetMetadataFromService, public error: ErrorDto) {
      super(parentAction);
    }
  }

  export class GetRomeoByIssn extends BaseAction {
    static readonly type: string = `[${state}] Get Romeo By Issn`;

    constructor(public issn: string) {
      super();
    }
  }

  export class GetRomeoByIssnSuccess extends BaseSubActionSuccess<GetRomeoByIssn> {
    static readonly type: string = `[${state}] Get Romeo By Issn Success`;

    constructor(public parentAction: GetRomeoByIssn, public romeo: Romeo) {
      super(parentAction);
    }
  }

  export class GetRomeoByIssnFail extends BaseSubActionFail<GetRomeoByIssn> {
    static readonly type: string = `[${state}] Get Romeo By Issn Fail`;
  }
}

export const sharedExternalDataActionNameSpace = SharedExternalDataAction;
