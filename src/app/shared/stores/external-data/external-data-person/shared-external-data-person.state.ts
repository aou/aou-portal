/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-external-data-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {ContributorRockSolid} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedExternalDataPersonActionNameSpace} from "@shared/stores/external-data/external-data-person/shared-external-data-person.action";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  StoreUtil,
} from "solidify-frontend";

const urlSeparator = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

export interface SharedExternalDataPersonStateModel extends ResourceStateModel<ContributorRockSolid> {
}

@Injectable()
@State<SharedExternalDataPersonStateModel>({
  name: StateEnum.shared_externalData_person,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class SharedExternalDataPersonState extends ResourceState<SharedExternalDataPersonStateModel, ContributorRockSolid> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedExternalDataPersonActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminExternalData + urlSeparator + ApiActionNameEnum.SEARCH_UNIGE_PERSON;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }
}
