/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-archive.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {PublicationDownload} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {CookieEnum} from "@shared/enums/cookie.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedArchiveAction,
  sharedArchiveActionNameSpace,
} from "@shared/stores/archive/shared-archive.action";
import {
  interval,
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  filter,
  map,
  mergeMap,
  startWith,
  takeWhile,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CookieConsentService,
  CookieConsentUtil,
  CookiePartialEnum,
  CookieType,
  defaultResourceStateInitValue,
  DialogUtil,
  DownloadService,
  ErrorDto,
  FilePreviewDialog,
  FileStatusEnum,
  isTrue,
  MemoizedUtil,
  NotificationService,
  NotificationTypeEnum,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../enums/api-action-name.enum";

export interface SharedArchiveStateModel extends ResourceStateModel<PublicationDownload> {
  isPooling: string[];
  isPendingRequest: string[];
  isFirstTimeError: string[];
}

@Injectable()
@State<SharedArchiveStateModel>({
  name: StateEnum.shared_archive,
  defaults: {
    ...defaultResourceStateInitValue(),
    isPooling: [],
    isPendingRequest: [],
    isFirstTimeError: [],
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [],
})
export class SharedArchiveState extends ResourceState<SharedArchiveStateModel, PublicationDownload> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService,
              private readonly _cookieConsentService: CookieConsentService,
              private readonly _dialog: MatDialog) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedArchiveActionNameSpace,
    });
  }

  private readonly _PATH_FILTER: string = "query";

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  private readonly _IS_POOLING_KEY: keyof SharedArchiveStateModel = "isPooling";
  private readonly _IS_PENDING_REQUEST_KEY: keyof SharedArchiveStateModel = "isPendingRequest";
  private readonly _IS_FIRST_TIME_ERROR_KEY: keyof SharedArchiveStateModel = "isFirstTimeError";

  @Selector()
  static isLoading(state: SharedArchiveStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  static isDownloadPreparation(store: Store, archiveResId: string): Observable<boolean> {
    return MemoizedUtil.select(store, SharedArchiveState, state => state.isPooling).pipe(
      map(list => list.includes(archiveResId)),
    );
  }

  @Action(SharedArchiveAction.Download)
  download(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download): any {
    if (isTrue(environment.useDownloadToken) && !CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookieEnum.downloadToken)) {
      this._cookieConsentService.notifyFeatureDisabledBecauseCookieDeclined(CookieType.cookie, CookiePartialEnum.downloadToken);
      return;
    }
    this._notificationService.show({
      durationInSecond: 10,
      data: {
        message: LabelTranslateEnum.fileDownloadInPreparation,
        category: NotificationTypeEnum.information,
      },
      component: environment.defaultNotificationComponent,
    });

    this._setIsPooling(ctx, action, true);
    this._setIsPendingRequest(ctx, action, false);
    this._setIsFirstTimeError(ctx, action, false);

    return interval(2000)
      .pipe(
        takeWhile(() => this._getIsPooling(ctx, action)),
        filter(() => !this._getIsPendingRequest(ctx, action)),
        startWith(0),
        mergeMap(() => {
          this._setIsPendingRequest(ctx, action, true);
          return this._getDownloadStatus(ctx, action.file.resId).pipe(
            tap(() => this._setIsPendingRequest(ctx, action, false)),
          );
        }),
        tap((res: string) => {
          if (res === StringUtil.stringEmpty) {
            this._setIsPendingRequest(ctx, action, true);
            this.subscribe(this._prepareDownload(ctx, action.file.resId).pipe(
              tap(() => {
                this._setIsPendingRequest(ctx, action, false);
              }),
              catchError((e: ErrorDto) => {
                if (e.status === HttpStatusCode.Forbidden) {
                  this._setIsPendingRequest(ctx, action, false);
                  this._setIsPooling(ctx, action, false);
                  ctx.dispatch(new SharedArchiveAction.DownloadFail(action, LabelTranslateEnum.fileDownloadForbidden));
                  return;
                }
                return of(StringUtil.stringEmpty);
              }),
            ));
            return;
          }
          if (res === Enums.PublicationDownload.StatusEnum.READY) {
            this._setIsPooling(ctx, action, false);
            ctx.dispatch(new SharedArchiveAction.DownloadStart(action));
            return;
          }
          if (res === Enums.PublicationDownload.StatusEnum.IN_ERROR) {
            this._setIsPooling(ctx, action, false);
            if (this._getIsFirstTimeError(ctx, action) === false) {
              ctx.dispatch(new SharedArchiveAction.DownloadFail(action));
              this._setIsFirstTimeError(ctx, action, true);
            }
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          this._setIsPooling(ctx, action, false);
          throw error;
        }),
      );
  }

  @Action(SharedArchiveAction.Preview)
  preview(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Preview): any {
    this._notificationService.show({
      durationInSecond: 10,
      data: {
        message: LabelTranslateEnum.filePreviewInPreparation,
        category: NotificationTypeEnum.information,
      },
      component: environment.defaultNotificationComponent,
    });

    this._setIsPooling(ctx, action, true);
    this._setIsPendingRequest(ctx, action, false);
    this._setIsFirstTimeError(ctx, action, false);

    return interval(2000)
      .pipe(
        takeWhile(() => this._getIsPooling(ctx, action)),
        filter(() => !this._getIsPendingRequest(ctx, action)),
        startWith(0),
        mergeMap(() => {
          this._setIsPendingRequest(ctx, action, true);
          return this._getDownloadStatus(ctx, action.file.resId).pipe(
            tap(() => this._setIsPendingRequest(ctx, action, false)),
          );
        }),
        tap((res: string) => {
          if (res === StringUtil.stringEmpty) {
            this._setIsPendingRequest(ctx, action, true);
            this.subscribe(this._prepareDownload(ctx, action.file.resId).pipe(
              tap(() => {
                this._setIsPendingRequest(ctx, action, false);
              }),
              catchError((e: ErrorDto) => {
                if (e.status === HttpStatusCode.Forbidden) {
                  this._setIsPendingRequest(ctx, action, false);
                  this._setIsPooling(ctx, action, false);
                  ctx.dispatch(new SharedArchiveAction.PreviewFail(action, LabelTranslateEnum.filePreviewForbidden));
                  return;
                }
                return of(StringUtil.stringEmpty);
              }),
            ));
            return;
          }
          if (res === Enums.PublicationDownload.StatusEnum.READY) {
            this._setIsPooling(ctx, action, false);
            ctx.dispatch(new SharedArchiveAction.PreviewStart(action));
            return;
          }
          if (res === Enums.PublicationDownload.StatusEnum.IN_ERROR) {
            this._setIsPooling(ctx, action, false);
            if (this._getIsFirstTimeError(ctx, action) === false) {
              ctx.dispatch(new SharedArchiveAction.PreviewFail(action));
              this._setIsFirstTimeError(ctx, action, true);
            }
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          this._setIsPooling(ctx, action, false);
          throw error;
        }),
      );
  }

  private _getDownloadStatus(ctx: SolidifyStateContext<SharedArchiveStateModel>, resId: string): Observable<string> {
    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "text/plain;charset=UTF-8");

    return this._httpClient.get(this._urlResource + urlSeparator + resId
      + urlSeparator + ApiActionNameEnum.DOWNLOAD_STATUS, {
      headers,
      responseType: "text",
    }).pipe(
      catchError((e: ErrorDto) => {
        if (e.status === HttpStatusCode.InternalServerError) {
          return of(Enums.PublicationDownload.StatusEnum.IN_ERROR);
        }
        return of(StringUtil.stringEmpty);
      }),
    );
  }

  private _prepareDownload(ctx: SolidifyStateContext<SharedArchiveStateModel>, resId: string): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set("Content-Type", "text/plain;charset=UTF-8");
    return this._httpClient.post(this._urlResource + urlSeparator + resId
      + urlSeparator + ApiActionNameEnum.PREPARE_DOWNLOAD, {
      headers,
      responseType: "text",
    });
  }

  @Action(SharedArchiveAction.DownloadStart)
  downloadStart(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.DownloadStart): void {
    const archive = action.parentAction.file;
    const fileName = archive.resId + ".zip";
    const fileSize = archive.size;
    this._downloadService.download(`${this._urlResource}/${archive.resId}/${ApiActionNameEnum.DL}`, fileName, fileSize);
  }

  @Action(SharedArchiveAction.DownloadFail)
  downloadFail(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.DownloadFail): void {
    this._notificationService.showError(action.customMessageToTranslate ?? LabelTranslateEnum.fileDownloadFail);
  }

  @Action(SharedArchiveAction.PreviewStart)
  previewStart(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.PreviewStart): void {
    const file = action.parentAction.file;
    DialogUtil.open(this._dialog, FilePreviewDialog, {
      fileInput: {
        dataFile: {
          fileName: file.name,
          fileSize: file.size,
          mimetype: file.mimeType,
          status: FileStatusEnum.READY,
        },
      },
      fileDownloadUrl: `${ApiEnum.accessPublicMetadata}/${file.resId}/${ApiActionNameEnum.DL}`,
    }, {
      width: "max-content",
      maxWidth: "90vw",
      height: "min-content",
    });
  }

  @Action(SharedArchiveAction.PreviewFail)
  previewFail(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.PreviewFail): void {
    this._notificationService.showError(action.customMessageToTranslate ?? LabelTranslateEnum.fileDownloadFail);
  }

  private _getIsPooling(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download): boolean {
    return this._isIncludedInList(ctx, action, this._IS_POOLING_KEY);
  }

  private _setIsPooling(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, toAdd: boolean): void {
    return this._setInList(ctx, action, this._IS_POOLING_KEY, toAdd);
  }

  private _getIsPendingRequest(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download): boolean {
    return this._isIncludedInList(ctx, action, this._IS_PENDING_REQUEST_KEY);
  }

  private _setIsPendingRequest(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, toAdd: boolean): void {
    return this._setInList(ctx, action, this._IS_PENDING_REQUEST_KEY, toAdd);
  }

  private _getIsFirstTimeError(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download): boolean {
    return this._isIncludedInList(ctx, action, this._IS_FIRST_TIME_ERROR_KEY);
  }

  private _setIsFirstTimeError(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, toAdd: boolean): void {
    return this._setInList(ctx, action, this._IS_FIRST_TIME_ERROR_KEY, toAdd);
  }

  private _isIncludedInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, key: keyof SharedArchiveStateModel): boolean {
    return (ctx.getState()[key] as string[]).includes(action.file.resId);
  }

  private _setInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, key: keyof SharedArchiveStateModel, toAdd: boolean): void {
    if (toAdd) {
      this._addInList(ctx, action, key);
    } else {
      this._removeInList(ctx, action, key);
    }
  }

  private _addInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, key: keyof SharedArchiveStateModel): void {
    const list = ctx.getState()[key] as string[];
    if (!list.includes(action.file.resId)) {
      ctx.patchState({
        [key]: [...list, action.file.resId],
      });
    }
  }

  private _removeInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download, key: keyof SharedArchiveStateModel): void {
    const list = ctx.getState()[key] as string[];
    const archiveId = action.file.resId;
    const indexOf = list.indexOf(archiveId);
    if (indexOf !== -1) {
      const listPending = [...list];
      listPending.splice(indexOf, 1);
      ctx.patchState({
        [key]: listPending,
      });
    }
  }
}

