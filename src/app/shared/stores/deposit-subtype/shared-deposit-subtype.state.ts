/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-deposit-subtype.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {
  DepositSubtype,
  DepositType,
} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {sharedDepositSubtypeActionNameSpace} from "@shared/stores/deposit-subtype/shared-deposit-subtype.action";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  OrderEnum,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  StoreUtil,
} from "solidify-frontend";
import {StateEnum} from "../../enums/state.enum";

export interface SharedDepositSubtypeStateModel extends ResourceStateModel<DepositType> {
}

@Injectable()
@State<SharedDepositSubtypeStateModel>({
  name: StateEnum.shared_depositSubtype,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "sortValue", order: OrderEnum.ascending}),
  },
})
export class SharedDepositSubtypeState extends ResourceState<SharedDepositSubtypeStateModel, DepositSubtype> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedDepositSubtypeActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublicationSubtypes;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }
}
