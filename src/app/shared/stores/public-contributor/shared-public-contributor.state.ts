/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-public-contributor.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  PublicationStatistic,
  PublicContributorDto,
} from "@models";
import {
  Action,
  State,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {ContributorIdentifierTypeEnum} from "@shared/enums/contributor-identifier-type.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedPublicContributorAction} from "@shared/stores/public-contributor/shared-public-contributor.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  CollectionTyped,
  defaultBaseStateInitValue,
  isNotNullNorUndefined,
  isNullOrUndefined,
  ObjectUtil,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface SharedPublicContributorStateModel extends BaseStateModel {
  contributor: PublicContributorDto;
  list: PublicationStatistic[] | undefined;
  queryParameters: QueryParameters;
}

@Injectable()
@State<SharedPublicContributorStateModel>({
  name: StateEnum.shared_publicContributor,
  defaults: {
    ...defaultBaseStateInitValue(),
    contributor: undefined,
    list: undefined,
    queryParameters: new QueryParameters(),
  },
  children: [],
})
export class SharedPublicContributorState extends BasicState<SharedPublicContributorStateModel> {
  protected get _urlResource(): string {
    return ApiEnum.accessContributor;
  }

  constructor(private readonly _apiService: ApiService) {
    super();
  }

  @Action(SharedPublicContributorAction.GetContributor)
  getByCnIndividu(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.GetContributor): Observable<PublicContributorDto> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      contributor: undefined,
    });

    let url: string;
    switch (action.identifierType) {
      case ContributorIdentifierTypeEnum.orcid:
        url = `${ApiEnum.accessContributor}/${ApiActionNameEnum.GET_UNIGE_CONTRIBUTOR_BY_ORCID}/${action.identifier}`;
        break;
      case ContributorIdentifierTypeEnum.cnIndividu:
      default:
        url = `${ApiEnum.accessContributor}/${action.identifier}`;
    }
    return this._apiService.get<PublicContributorDto>(url)
      .pipe(
        tap((publicContributorDto: PublicContributorDto) => {
          if (isNotNullNorUndefined(publicContributorDto)) {
            ctx.dispatch(new SharedPublicContributorAction.GetContributorSuccess(action, publicContributorDto));
          } else {
            ctx.dispatch(new SharedPublicContributorAction.GetContributorFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPublicContributorAction.GetContributorFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedPublicContributorAction.GetContributorSuccess)
  getByCnIndividuSuccess(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.GetContributorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      contributor: action.contributor,
    });
  }

  @Action(SharedPublicContributorAction.GetContributorFail)
  getByCnIndividuFail(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.GetContributorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedPublicContributorAction.GetPublicationsByCnIndividu)
  getPublicationsByCnIndividu(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.GetPublicationsByCnIndividu): Observable<CollectionTyped<PublicationStatistic>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: isNullOrUndefined(action.queryParameters) ? ctx.getState().queryParameters : action.queryParameters,
    });

    return this._apiService.getCollection<PublicationStatistic>(`${ApiEnum.accessContributor}/${action.cnIndividu}/${ApiActionNameEnum.STATISTICS}`, action.queryParameters)
      .pipe(
        tap((collection: CollectionTyped<PublicationStatistic>) => {
          ctx.dispatch(new SharedPublicContributorAction.GetPublicationsByCnIndividuSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPublicContributorAction.GetPublicationsByCnIndividuFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedPublicContributorAction.GetPublicationsByCnIndividuSuccess)
  getPublicationsByCnIndividuSuccess(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.GetPublicationsByCnIndividuSuccess): void {
    // Update query Parameters
    const queryParameters = ObjectUtil.clone(ctx.getState().queryParameters);
    const paging = ObjectUtil.clone(queryParameters.paging);
    paging.length = isNullOrUndefined(action.list) ? 0 : action.list._page.totalItems;
    queryParameters.paging = paging;

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: action.list?._data ?? [],
      queryParameters,
    });
  }

  @Action(SharedPublicContributorAction.GetPublicationsByCnIndividuFail)
  getPublicationsByCnIndividuFail(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.GetPublicationsByCnIndividuFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedPublicContributorAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(new SharedPublicContributorAction.GetPublicationsByCnIndividu(action.cnIndividu, action.queryParameters));
  }

  @Action(SharedPublicContributorAction.Clean)
  clean(ctx: SolidifyStateContext<SharedPublicContributorStateModel>, action: SharedPublicContributorAction.Clean): void {
    ctx.patchState({
      contributor: undefined,
      list: [],
    });
  }
}

