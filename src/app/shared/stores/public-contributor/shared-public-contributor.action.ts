/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-public-contributor.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  PublicationStatistic,
  PublicContributorDto,
} from "@models";
import {ContributorIdentifierTypeEnum} from "@shared/enums/contributor-identifier-type.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CollectionTyped,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.shared_publicContributor;

export namespace SharedPublicContributorAction {
  export class ChangeQueryParameters {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public cnIndividu: string, public queryParameters?: QueryParameters) {
    }
  }

  export class GetContributor extends BaseAction {
    static readonly type: string = `[${state}] Get Contributor`;

    constructor(public identifier: string, public identifierType: ContributorIdentifierTypeEnum) {
      super();
    }
  }

  export class GetContributorSuccess extends BaseSubActionSuccess<GetContributor> {
    static readonly type: string = `[${state}] Get Contributor Success`;

    constructor(public parentAction: GetContributor, public contributor?: PublicContributorDto) {
      super(parentAction);
    }
  }

  export class GetContributorFail extends BaseSubActionFail<GetContributor> {
    static readonly type: string = `[${state}] Get Contributor Fail`;
  }

  export class GetPublicationsByCnIndividu extends BaseAction {
    static readonly type: string = `[${state}] Get Publications By Cn Individu`;

    constructor(public cnIndividu: string, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class GetPublicationsByCnIndividuSuccess extends BaseSubActionSuccess<GetPublicationsByCnIndividu> {
    static readonly type: string = `[${state}] Get Publications By Cn Individu Success`;

    constructor(public parentAction: GetPublicationsByCnIndividu, public list?: CollectionTyped<PublicationStatistic>) {
      super(parentAction);
    }
  }

  export class GetPublicationsByCnIndividuFail extends BaseSubActionFail<GetPublicationsByCnIndividu> {
    static readonly type: string = `[${state}] Get Publications By Cn Individu Fail`;
  }

  export class Clean extends BaseAction {
    static readonly type: string = `[${state}] Clean`;
  }

}
