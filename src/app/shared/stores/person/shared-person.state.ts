/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  SharedPersonAction,
  sharedPersonActionNameSpace,
} from "@app/shared/stores/person/shared-person.action";
import {environment} from "@environments/environment";
import {Person} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {AouRegexUtil} from "@shared/utils/aou-regex.util";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  isWhiteString,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {StateEnum} from "../../enums/state.enum";

export interface SharedPersonStateModel extends ResourceStateModel<Person> {
  listPersonMatching: Person[];
}

@Injectable()
@State<SharedPersonStateModel>({
  name: StateEnum.shared_person,
  defaults: {
    ...defaultResourceStateInitValue(),
    listPersonMatching: [],
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedPersonState extends ResourceState<SharedPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedPersonActionNameSpace,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("shared.person.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("shared.person.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("shared.person.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedPersonAction.Search)
  search(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.Search): Observable<boolean> {
    this._incrementLoadingCounter(ctx);
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedPersonAction.SearchPerson(action.person),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchPersonSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchPersonFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedPersonAction.SearchSuccess(action));
        } else {
          ctx.dispatch(new SharedPersonAction.SearchFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedPersonAction.SearchSuccess)
  searchSuccess(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchSuccess): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(SharedPersonAction.SearchFail)
  searchFail(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchFail): void {
    this._decrementLoadingCounter(ctx);
  }

  @Action(SharedPersonAction.SearchPerson)
  searchPerson(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchPerson): Observable<CollectionTyped<Person>> {
    const searchItems = {} as MappingObject<string, string>;
    MappingObjectUtil.set(searchItems, "firstName", action.person.firstName);
    MappingObjectUtil.set(searchItems, "lastName", action.person.lastName);
    const queryParameters = new QueryParameters();
    queryParameters.search = {
      searchItems: searchItems,
    };
    return this._apiService.getCollection<Person>(this._urlResource, queryParameters)
      .pipe(
        tap(collection => {
          ctx.dispatch(new SharedPersonAction.SearchPersonSuccess(action, collection._data));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPersonAction.SearchPersonFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedPersonAction.SearchPersonSuccess)
  searchPersonSuccess(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchPersonSuccess): void {
    ctx.patchState({
      listPersonMatching: action.list,
    });
  }

  @OverrideDefaultAction()
  @Action(SharedPersonAction.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceStateModel<Person>>, action: SharedPersonAction.GetAll): Observable<CollectionTyped<Person>> {
    this._computeQueryParametersSearchItem(action);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    return this._apiService.getCollection<Person>(this._getUrl(action), ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedPersonAction.GetAll]),
        tap((collection: CollectionTyped<Person>) => {
          ctx.dispatch(new SharedPersonAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPersonAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  private _computeQueryParametersSearchItem(action: SharedPersonAction.GetAll): void {
    const onlyWithUser = action.onlyWithUser;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const fullName = MappingObjectUtil.get(searchItems, "fullName");
    if (!isNullOrUndefined(fullName) && !isWhiteString(fullName)) {
      if (onlyWithUser) {
        MappingObjectUtil.set(searchItems, "search", fullName);
      } else {
        let searchValue = "";
        fullName.trim().split(" ").forEach(term => {
          if (!isWhiteString(term)) {
            const partOfORCIDRegex = AouRegexUtil.partOfORCID;
            if (partOfORCIDRegex.test(term)) {
              searchValue = this._addTermToSearchValue(searchValue, `i-orcid~${term}`);
            } else {
              searchValue = this._addTermToSearchValue(searchValue, `i-firstName~${term},i-lastName~${term}`);
            }
          }
        });

        MappingObjectUtil.set(searchItems, "search", searchValue);
        MappingObjectUtil.delete(searchItems, "fullName");
        MappingObjectUtil.set(searchItems, "match", "any");
      }
    }
  }

  private _getUrl(action: SharedPersonAction.GetAll): string {
    const onlyWithUser = action.onlyWithUser;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const fullName = MappingObjectUtil.get(searchItems, "fullName");

    if (onlyWithUser) {
      return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameEnum.SEARCH_WITH_USER;
    }

    if (!isNullOrUndefined(fullName) && !isWhiteString(fullName)) {
      return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameEnum.SEARCH;
    }

    return this._urlResource;
  }

  private _addTermToSearchValue(searchValue: string, term: string): string {
    searchValue = this._prefixWithCommaWhenSearchStringNotNull(searchValue);
    searchValue += term;
    return searchValue;
  }

  private _prefixWithCommaWhenSearchStringNotNull(searchValue: string): string {
    if (searchValue.length > 0) {
      searchValue += SOLIDIFY_CONSTANTS.COMMA;
    }
    return searchValue;
  }
}
