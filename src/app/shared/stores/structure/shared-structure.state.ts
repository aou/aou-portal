/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-structure.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {Enums} from "@app/enums";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  License,
  Structure,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {
  SharedStructureAction,
  sharedStructureActionNameSpace,
} from "@shared/stores/structure/shared-structure.action";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isNonEmptyString,
  isNullOrUndefined,
  isTrue,
  isWhiteString,
  MappingObjectUtil,
  NotificationService,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {environment} from "../../../../environments/environment";
import {StateEnum} from "../../enums/state.enum";

export interface SharedStructureStateModel extends ResourceStateModel<Structure> {
}

@Injectable()
@State<SharedStructureStateModel>({
  name: StateEnum.shared_structure,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "sortValue", order: OrderEnum.ascending}),
  },
})
export class SharedStructureState extends ResourceState<SharedStructureStateModel, Structure> {
  static readonly FIELD_SORT_VALUE: keyof License = "sortValue";

  static readonly FIELD_SEARCH_NAME: keyof Structure = "name";
  static readonly FIELD_SEARCH_ACRONYM: keyof Structure = "acronym";
  static readonly FIELD_SEARCH_CODE: keyof Structure = "cStruct";
  static readonly FIELD_SEARCH_CODE_BACKEND: string = "codeStruct";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedStructureActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminStructures;
  }

  static labelCallback(value: Structure, translateService: TranslateService): string {
    let result = SOLIDIFY_CONSTANTS.STRING_EMPTY;

    if (isTrue(value.status === Enums.Structure.StatusEnum.OBSOLETE)) {
      const inactiveLabel = translateService.instant(LabelTranslateEnum.obsolete);
      result = `<span class="is-obsolete">[${inactiveLabel}] `;
    }

    if (isTrue(value.status === Enums.Structure.StatusEnum.HISTORICAL)) {
      const inactiveLabel = translateService.instant(LabelTranslateEnum.historical);
      result = `<span class="is-historical">[${inactiveLabel}] `;
    }

    result = result + value[SharedStructureState.FIELD_SEARCH_NAME] as string;

    if (isNonEmptyString(value[SharedStructureState.FIELD_SEARCH_ACRONYM])) {
      result += " [" + value[SharedStructureState.FIELD_SEARCH_ACRONYM] + "]";
    }

    if (value.status === Enums.Structure.StatusEnum.OBSOLETE || value.status === Enums.Structure.StatusEnum.HISTORICAL) {
      result = result + "</span>";
    }

    return result;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(SharedStructureAction.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceStateModel<Structure>>, action: SharedStructureAction.GetAll): Observable<CollectionTyped<Structure>> {
    let url = this._urlResource;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const searchTerm = MappingObjectUtil.get(searchItems, SharedStructureState.FIELD_SEARCH_NAME);
    if (!isNullOrUndefined(searchTerm) && !isWhiteString(searchTerm)) {
      url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH;

      let searchValue = "";
      let match = "any";

      if (searchTerm.includes(" ")) {
        // When search term contains a space, the search is done on structure name only (acronym and code may not contains spaces) and the
        // results must contain all words.
        match = "all";
        searchTerm.trim().split(" ").forEach(term => {
          if (!isWhiteString(term)) {
            searchValue = this._addTermToSearchValue(searchValue, `i-${SharedStructureState.FIELD_SEARCH_NAME}~${term}`);
          }
        });
      } else if (!isWhiteString(searchTerm)) {
        // When search term doesn't contain any space, the search is done on name, code and acronym and the result may match any field.
        searchValue = this._addTermToSearchValue(searchValue, `i-${SharedStructureState.FIELD_SEARCH_CODE_BACKEND}~${searchTerm},i-${SharedStructureState.FIELD_SEARCH_NAME}~${searchTerm},i-${SharedStructureState.FIELD_SEARCH_ACRONYM}~${searchTerm}`);
      }

      MappingObjectUtil.set(searchItems, "search", searchValue);
      MappingObjectUtil.delete(searchItems, SharedStructureState.FIELD_SEARCH_NAME);
      MappingObjectUtil.set(searchItems, "match", match);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });
    return this._apiService.getCollection<Structure>(url, ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<Structure>) => {
          ctx.dispatch(new SharedStructureAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedStructureAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      )
      ;
  }

  private _addTermToSearchValue(searchValue: string, term: string): string {
    searchValue = this._prefixWithCommaWhenSearchStringNotNull(searchValue);
    searchValue += term;
    return searchValue;
  }

  private _prefixWithCommaWhenSearchStringNotNull(searchValue: string): string {
    if (searchValue.length > 0) {
      searchValue += SOLIDIFY_CONSTANTS.COMMA;
    }
    return searchValue;
  }
}
