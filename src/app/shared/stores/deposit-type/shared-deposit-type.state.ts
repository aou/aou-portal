/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-deposit-type.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {DepositType} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {sharedDepositTypeActionNameSpace} from "@shared/stores/deposit-type/shared-deposit-type.action";
import {SharedDepositTypeSubtypeState} from "@shared/stores/deposit-type/subtype/shared-deposit-type-subtype.state";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  OrderEnum,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  StoreUtil,
} from "solidify-frontend";
import {StateEnum} from "../../enums/state.enum";

export interface SharedDepositTypeStateModel extends ResourceStateModel<DepositType> {
}

@Injectable()
@State<SharedDepositTypeStateModel>({
  name: StateEnum.shared_depositType,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "sortValue", order: OrderEnum.ascending}),
  },
  children: [
    SharedDepositTypeSubtypeState,
  ],
})
export class SharedDepositTypeState extends ResourceState<SharedDepositTypeStateModel, DepositType> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedDepositTypeActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublicationTypes;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }
}
