/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-deposit-type-subtype.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {environment} from "@environments/environment";
import {DepositSubtype} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {sharedDepositTypeSubtypeActionNameSpace} from "@shared/stores/deposit-type/subtype/shared-deposit-type-subtype.action";
import {SharedDepositSubSubtypeState} from "@shared/stores/deposit-type/subtype/sub-subtype/shared-deposit-sub-subtype.state";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  NotificationService,
  OrderEnum,
  QueryParameters,
} from "solidify-frontend";
import {StateEnum} from "../../../enums/state.enum";

export const defaultSharedDepositTypeSubtypeValue: () => SharedDepositTypeSubtypeStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "sortValue", order: OrderEnum.ascending}),
  });

export interface SharedDepositTypeSubtypeStateModel extends CompositionStateModel<DepositSubtype> {
}

@Injectable()
@State<SharedDepositTypeSubtypeStateModel>({
  name: StateEnum.shared_depositType_subType,
  defaults: {
    ...defaultSharedDepositTypeSubtypeValue(),
  },
  children: [
    SharedDepositSubSubtypeState,
  ],
})
export class SharedDepositTypeSubtypeState extends CompositionState<SharedDepositTypeSubtypeStateModel, DepositSubtype> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedDepositTypeSubtypeActionNameSpace,
      resourceName: ApiResourceNameEnum.PUBLICATION_SUBTYPES,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPublicationTypes;
  }
}
