/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-contributor.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Contributor} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {
  SharedContributorAction,
  sharedContributorActionNameSpace,
} from "@shared/stores/contributor/shared-contributor.action";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isEmptyArray,
  isNullOrUndefined,
  isString,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceActionHelper,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {StateEnum} from "../../enums/state.enum";

export interface SharedContributorStateModel extends ResourceStateModel<Contributor> {
}

@Injectable()
@State<SharedContributorStateModel>({
  name: StateEnum.shared_contributor,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedContributorState extends ResourceState<SharedContributorStateModel, Contributor> {
  static readonly ATTRIBUTE_FIRST_NAME: keyof Contributor = "firstName";
  static readonly ATTRIBUTE_LAST_NAME: keyof Contributor = "lastName";
  static readonly ATTRIBUTE_FULL_NAME: keyof Contributor = "fullName";
  static readonly ATTRIBUTE_CN_INDIVIDU: keyof Contributor = "cnIndividu";

  static readonly SORT_FIELD: keyof Contributor = SharedContributorState.ATTRIBUTE_LAST_NAME;
  static readonly SEARCH_FIELD: string = SharedContributorState.ATTRIBUTE_FULL_NAME;
  static readonly TERM_FIELD: string = "term";
  static readonly TYPE_FIELD: string = "type";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedContributorActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminContributors;
  }

  static labelCallback(value: Contributor): string {
    return value.displayName;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(SharedContributorAction.GetAll)
  getAll(ctx: SolidifyStateContext<SharedContributorStateModel>, action: SharedContributorAction.GetAll): Observable<CollectionTyped<Contributor>> {
    const queryParameters = this._updateQueryParametersToSearchByFullName(action.queryParameters);

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(queryParameters, ctx),
    });
    return this._apiService.getCollection<Contributor>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedContributorAction.GetAll]),
        tap((collection: CollectionTyped<Contributor>) => {
          ctx.dispatch(new SharedContributorAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedContributorAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(SharedContributorAction.LoadNextChunkList)
  loadNextChunkList(ctx: SolidifyStateContext<SharedContributorStateModel>, action: SharedContributorAction.LoadNextChunkList): Observable<CollectionTyped<Contributor>> {
    const queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    if (!StoreUtil.isNextChunkAvailable(queryParameters)) {
      return;
    }

    queryParameters.paging.pageIndex = queryParameters.paging.pageIndex + 1;

    ctx.patchState({
      isLoadingChunk: true,
      queryParameters: queryParameters,
    });

    return this._apiService.getCollection<Contributor>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<Contributor>) => {
          ctx.dispatch(ResourceActionHelper.loadNextChunkListSuccess<Contributor>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceActionHelper.loadNextChunkListFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  private _updateQueryParametersToSearchByFullName(queryParameters: QueryParameters): QueryParameters {
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    let fullName = MappingObjectUtil.get(searchItems, SharedContributorState.SEARCH_FIELD);
    fullName = isString(fullName) ? fullName.trim() : SOLIDIFY_CONSTANTS.STRING_EMPTY;
    MappingObjectUtil.set(searchItems, SharedContributorState.TERM_FIELD, fullName);
    MappingObjectUtil.delete(searchItems, SharedContributorState.SEARCH_FIELD);
    return queryParameters;
  }

  @OverrideDefaultAction()
  @Action(SharedContributorAction.GetByListId)
  getByListId(ctx: SolidifyStateContext<SharedContributorStateModel>, action: SharedContributorAction.GetByListId): void {
    if (isNullOrUndefined(action.listResId) || isEmptyArray(action.listResId)) {
      return;
    }
    let listTemp = undefined;
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        listTemp: undefined,
      };
    } else {
      listTemp = ctx.getState().list;
    }

    const listSubAction = [];
    action.listResId.forEach(resId => {
      if (!isNullOrUndefined(listTemp) && !isEmptyArray(listTemp)) {
        const existingItem = listTemp.find(item => item.resId === resId);
        if (!isNullOrUndefined(existingItem)) {
          return;
        }
      }
      listSubAction.push({
        action: new SharedContributorAction.GetById(resId, true, true, action.modeEnum),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.GetByIdSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.GetByIdFail)),
        ],
      });
    });

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      listTemp: listTemp,
      ...reset,
    });

    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, listSubAction).pipe(
      tap(result => {
        if (result.success) {
          ctx.dispatch(ResourceActionHelper.getByListIdSuccess(this._nameSpace, action));
        } else {
          ctx.dispatch(ResourceActionHelper.getByListIdFail(this._nameSpace, action));
        }
      }),
    ));
  }

  @OverrideDefaultAction()
  @Action(SharedContributorAction.GetById)
  getById(ctx: SolidifyStateContext<SharedContributorStateModel>, action: SharedContributorAction.GetById): Observable<Contributor> {
    let url = this._urlResource;
    const searchItems = {} as MappingObject<string, string>;

    if (action.modeEnum === SharedContributorModeEnum.CN_INDIVIDU) {
      url = url + urlSeparator + ApiActionNameEnum.GET_BY_CN_INDIVIDU;
      MappingObjectUtil.set(searchItems, SharedContributorState.ATTRIBUTE_CN_INDIVIDU, action.id);
    } else if (action.modeEnum === SharedContributorModeEnum.FULL_NAME) {
      url = url + urlSeparator + ApiActionNameEnum.GET_BY_NAME;
      const terms = action.id.split(", ");
      if (terms.length !== 2) {
        ctx.dispatch(new SharedContributorAction.GetByIdFail(action));
        return;
      }
      const lastName = terms[0];
      const firstName = terms[1];
      MappingObjectUtil.set(searchItems, SharedContributorState.ATTRIBUTE_LAST_NAME, lastName);
      MappingObjectUtil.set(searchItems, SharedContributorState.ATTRIBUTE_FIRST_NAME, firstName);
    } else {
      return super.getById(ctx, action);
    }
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    return this._apiService.get<Contributor>(url, searchItems as any)
      .pipe(
        tap((contributor: Contributor) => {
          if (isNullOrUndefined(contributor)) {
            ctx.dispatch(new SharedContributorAction.GetByIdFail(action));
            return;
          }
          ctx.dispatch(new SharedContributorAction.GetByIdSuccess(action, contributor));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedContributorAction.GetByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}

export type SharedContributorModeEnum =
  "resId"
  | "cnIndividu"
  | "fullName";
export const SharedContributorModeEnum = {
  RES_ID: "resId" as SharedContributorModeEnum,
  CN_INDIVIDU: "cnIndividu" as SharedContributorModeEnum,
  FULL_NAME: "fullName" as SharedContributorModeEnum,
};

export type ContributorSearchTypeEnum =
  "UNIGE"
  | "NON_UNIGE"
  | "ALL";
export const ContributorSearchTypeEnum = {
  UNIGE: "UNIGE" as ContributorSearchTypeEnum,
  NON_UNIGE: "NON_UNIGE" as ContributorSearchTypeEnum,
  ALL: "ALL" as ContributorSearchTypeEnum,
};
