/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-research-group.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {ResearchGroup} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {
  SharedResearchGroupAction,
  sharedResearchGroupActionNameSpace,
} from "@shared/stores/research-group/shared-research-group.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isFalse,
  isNonEmptyString,
  isNonWhiteString,
  isNullOrUndefined,
  isWhiteString,
  MappingObjectUtil,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {StateEnum} from "../../enums/state.enum";

export interface SharedResearchGroupStateModel extends ResourceStateModel<ResearchGroup> {
}

@Injectable()
@State<SharedResearchGroupStateModel>({
  name: StateEnum.shared_researchGroup,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedResearchGroupState extends ResourceState<SharedResearchGroupStateModel, ResearchGroup> {
  static readonly FIELD_SORT_VALUE: keyof ResearchGroup = "name";

  static readonly FIELD_SEARCH_NAME: keyof ResearchGroup = "name";
  static readonly FIELD_SEARCH_ACRONYM: keyof ResearchGroup = "acronym";
  static readonly FIELD_SEARCH_CODE: keyof ResearchGroup = "code";
  static readonly FIELD_SEARCH_BY_NAME_STRICT: string = "search-by-name-strict";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedResearchGroupActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminResearchGroups;
  }

  static labelCallback(value: ResearchGroup, translateService: TranslateService): string {
    let result = SOLIDIFY_CONSTANTS.STRING_EMPTY;

    if (isFalse(value.active)) {
      const inactiveLabel = translateService.instant(LabelTranslateEnum.inactive);
      result = `<span class="inactive">[${inactiveLabel}] `;
    }

    result = result + value[SharedResearchGroupState.FIELD_SEARCH_NAME] as string;

    if (isNonEmptyString(value[SharedResearchGroupState.FIELD_SEARCH_ACRONYM])) {
      result += " [" + value[SharedResearchGroupState.FIELD_SEARCH_ACRONYM] + "]";
    }

    if (isNonEmptyString(value[SharedResearchGroupState.FIELD_SEARCH_CODE])) {
      result += " (" + value[SharedResearchGroupState.FIELD_SEARCH_CODE] + ")";
    }

    if (isFalse(value.active)) {
      result = result + "</span>";
    }

    return result;
  }

  static preTreatmentHighlightText(result: string): string {
    const endSpan = `</span>`;
    if (result.endsWith(endSpan)) {
      const startSpanIsHistorical = `<span class="inactive">`;
      if (result.startsWith(startSpanIsHistorical)) {
        result = result.substring(startSpanIsHistorical.length, result.length - endSpan.length);
      }
    }
    return result;
  }

  static postTreatmentHighlightText(result: string, resultBeforePreTreatment: string): string {
    const endSpan = `</span>`;
    if (resultBeforePreTreatment.endsWith(endSpan)) {
      const startSpanIsHistorical = `<span class="inactive">`;
      if (resultBeforePreTreatment.startsWith(startSpanIsHistorical)) {
        return startSpanIsHistorical + result + endSpan;
      }
    }
    return result;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @OverrideDefaultAction()
  @Action(SharedResearchGroupAction.GetAll)
  getAll(ctx: SolidifyStateContext<SharedResearchGroupStateModel>, action: SharedResearchGroupAction.GetAll): Observable<CollectionTyped<ResearchGroup>> {
    let url = this._urlResource;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const fullName = MappingObjectUtil.get(searchItems, SharedResearchGroupState.FIELD_SEARCH_NAME);
    const isStrictSearchByName = MappingObjectUtil.get(searchItems, SharedResearchGroupState.FIELD_SEARCH_BY_NAME_STRICT) === SOLIDIFY_CONSTANTS.STRING_TRUE;
    if (!isNullOrUndefined(fullName) && !isWhiteString(fullName)) {
      url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH;

      const listSearchValue: string[] = [];
      const listTermsToExclude = environment.researchGroupSearchListTermsToExclude;
      let match = "any";

      if (isStrictSearchByName) {
        // Search with different values for each field
        const listTerms = fullName.trim().split(" ")
          .map(term => term.toLowerCase())
          .filter(term => listTermsToExclude.indexOf(term) === -1 && isNonWhiteString(term));
        listTerms.forEach(term => {
          listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_NAME}~${term}`);
        });
        const acronym = MappingObjectUtil.get(searchItems, SharedResearchGroupState.FIELD_SEARCH_ACRONYM);
        if (isNonWhiteString(acronym)) {
          listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_ACRONYM}~${acronym}`);
        }
        const code = MappingObjectUtil.get(searchItems, SharedResearchGroupState.FIELD_SEARCH_CODE);
        if (isNonWhiteString(code)) {
          listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_CODE}~${code}`);
        }
      } else {
        // Search with same value for each field
        if (fullName.includes(" ")) {
          // When search term contains a space, the search is done on group name only (acronym and code may not contains spaces) and the
          // results must contain all words.
          match = "all";
          const listTerms = fullName.trim().split(" ")
            .map(term => term.toLowerCase())
            .filter(term => listTermsToExclude.indexOf(term) === -1 && isNonWhiteString(term));
          listTerms.forEach(term => {
            listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_NAME}~${term}`);
          });
        } else if (!isWhiteString(fullName)) {
          // When search term doesn't contain any space, the search is done on name, code and acronym and the result may match any field
          listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_NAME}~${fullName}`);
          listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_ACRONYM}~${fullName}`);
          listSearchValue.push(`i-${SharedResearchGroupState.FIELD_SEARCH_CODE}~${fullName}`);
        }
      }

      if (listSearchValue.length > 0) {
        MappingObjectUtil.set(searchItems, "search", listSearchValue.join(","));
      }
      MappingObjectUtil.delete(searchItems, SharedResearchGroupState.FIELD_SEARCH_NAME);
      MappingObjectUtil.delete(searchItems, SharedResearchGroupState.FIELD_SEARCH_ACRONYM);
      MappingObjectUtil.delete(searchItems, SharedResearchGroupState.FIELD_SEARCH_CODE);
      MappingObjectUtil.delete(searchItems, SharedResearchGroupState.FIELD_SEARCH_BY_NAME_STRICT);
      MappingObjectUtil.set(searchItems, "match", match);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });
    return this._apiService.getCollection<ResearchGroup>(url, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedResearchGroupAction.GetAll]),
        tap((collection: CollectionTyped<ResearchGroup>) => {
          ctx.dispatch(new SharedResearchGroupAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedResearchGroupAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedResearchGroupAction.ReplaceInList)
  replaceInList(ctx: SolidifyStateContext<SharedResearchGroupStateModel>, action: SharedResearchGroupAction.ReplaceInList): void {
    let list = ctx.getState().list;
    const indexOf = list?.findIndex(r => r.resId === action.researchGroup.resId);
    if (indexOf >= 0) {
      list = [...list];
      list[indexOf] = action.researchGroup;
      ctx.patchState({
        list: list,
      });
    }
  }
}
