/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - corrigendum.validator.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  FormGroup,
  ValidatorFn,
} from "@angular/forms";
import {FormComponentFormDefinitionDepositFormCorrection} from "@deposit/models/deposit-form-definition.model";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
} from "solidify-frontend";

export const CorrigendumValidator = (formGroup: FormGroup): ValidatorFn => {
  const formComponent = new FormComponentFormDefinitionDepositFormCorrection();
  const doiControl = formGroup.controls[formComponent.doi];
  const pmidControl = formGroup.controls[formComponent.pmid];
  const noteControl = formGroup.controls[formComponent.note];
  if ((isNotNullNorUndefinedNorWhiteString(doiControl.value) || isNotNullNorUndefinedNorWhiteString(pmidControl.value)) && isNullOrUndefinedOrWhiteString(noteControl.value)) {
    noteControl.markAllAsTouched();
    noteControl.setErrors({"[attr.required]": true});
  }

  if (!isNotNullNorUndefinedNorWhiteString(doiControl.value) && !isNotNullNorUndefinedNorWhiteString(pmidControl.value)) {
    noteControl.setErrors(null);
  }
  return null;
};
