/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - shared-autocomplete-infinite-scroll.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from "@angular/core";
import {MatAutocomplete} from "@angular/material/autocomplete";
import {SharedAbstractDirective} from "@shared/directives/shared-abstract/shared-abstract.directive";
import {Subject} from "rxjs";
import {
  takeUntil,
  tap,
} from "rxjs/operators";

export interface IAutoCompleteScrollEvent {
  autoComplete: MatAutocomplete;
  scrollEvent: Event;
}

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: "mat-autocomplete[optionsScroll]",
})
export class SharedAutocompleteInfiniteScrollDirective extends SharedAbstractDirective implements OnDestroy {
  @Input()
  thresholdPercent: number = .8;

  @Output("optionsScroll")
    // eslint-disable-next-line @angular-eslint/no-output-native
  scroll: EventEmitter<IAutoCompleteScrollEvent> = new EventEmitter<IAutoCompleteScrollEvent>();

  private _onDestroy: Subject<any> = new Subject();

  constructor(public readonly autoComplete: MatAutocomplete) {
    super();
    this.subscribe(this.autoComplete.opened.pipe(
      tap(() => {
        // Note: When autocomplete raises opened, panel is not yet created (by Overlay)
        // Note: The panel will be available on next tick
        // Note: The panel wil NOT open if there are no options to display
        setTimeout(() => {
          // Note: remove listener just for safety, in case the close event is skipped.
          this.removeScrollEventListener();
          this.autoComplete.panel.nativeElement
            .addEventListener("scroll", this.onScroll.bind(this));
        });
      }),
      takeUntil(this._onDestroy),
    ));

    this.subscribe(this.autoComplete.closed.pipe(
      tap(() => this.removeScrollEventListener()),
      takeUntil(this._onDestroy),
    ));
  }

  private removeScrollEventListener(): void {
    this.autoComplete?.panel?.nativeElement?.removeEventListener("scroll", this.onScroll);
  }

  ngOnDestroy(): void {
    this._onDestroy.next(undefined);
    this._onDestroy.complete();

    this.removeScrollEventListener();
  }

  onScroll(event: Event | any): void {
    if (this.thresholdPercent === undefined) {
      this.scroll.next({autoComplete: this.autoComplete, scrollEvent: event});
    } else {
      const threshold = this.thresholdPercent * 100 * event.target.scrollHeight / 100;
      const current = event.target.scrollTop + event.target.clientHeight;

      if (current > threshold) {
        this.scroll.next({autoComplete: this.autoComplete, scrollEvent: event});
      }
    }
  }
}
