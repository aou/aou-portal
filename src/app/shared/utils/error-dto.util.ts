/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - error-dto.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  ErrorDto,
  ErrorDtoMessage,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
  MappingObjectUtil,
} from "solidify-frontend";

export class ErrorDtoUtil {
  private static readonly _ERROR_CODE_ALREADY_EXISTING_DEPOSIT: number = 1;
  private static readonly _ERROR_CODE_POTENTIAL_PUBLICATION_DUPLICATE: number = 2;

  private static readonly _ADDITIONAL_PARAMETERS_RES_ID: string = "resId";
  private static readonly _ADDITIONAL_PARAMETERS_URL: string = "url";

  static isErrorCodeOfAlreadyExistingDeposit(errorDto: ErrorDto): boolean {
    return errorDto.errorCode === this._ERROR_CODE_ALREADY_EXISTING_DEPOSIT;
  }

  static isErrorCodeOfPotentialPublicationDuplicated(errorDto: ErrorDto): boolean {
    return errorDto.errorCode === this._ERROR_CODE_POTENTIAL_PUBLICATION_DUPLICATE;
  }

  static isUrlPresent(errorDto: ErrorDto): boolean {
    return MappingObjectUtil.has(errorDto.additionalParameters, this._ADDITIONAL_PARAMETERS_URL);
  }

  static isResIdPresent(errorDto: ErrorDto): boolean {
    return MappingObjectUtil.has(errorDto.additionalParameters, this._ADDITIONAL_PARAMETERS_RES_ID);
  }

  static getResId(errorDto: ErrorDto): string {
    return MappingObjectUtil.get(errorDto.additionalParameters, this._ADDITIONAL_PARAMETERS_RES_ID);
  }

  static getUrl(errorDto: ErrorDto): string {
    return MappingObjectUtil.get(errorDto.additionalParameters, this._ADDITIONAL_PARAMETERS_URL);
  }

  static buildLocalUrlFromResId(resId: string): string {
    return `/${RoutesEnum.depositDetail}/${resId}`;
  }

  static buildMessageFromAdditionalParameters(errorDto: ErrorDto): ErrorDtoMessage {
    if (!this.isErrorCodeOfAlreadyExistingDeposit(errorDto) && !this.isErrorCodeOfPotentialPublicationDuplicated(errorDto)) {
      return null;
    }

    let url = this.getUrl(errorDto);

    const resId = this.getResId(errorDto);
    if (isNotNullNorUndefinedNorWhiteString(resId)) {
      url = this.buildLocalUrlFromResId(resId);
    }

    if (isNullOrUndefinedOrWhiteString(url)) {
      return undefined;
    }
    return {
      text: errorDto.error,
      url: url,
    };
  }
}
