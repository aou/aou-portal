/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - user.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {User} from "@models";
import {isNullOrUndefinedOrWhiteString} from "solidify-frontend";

export class UserUtil {
  static isUnigeUser(user: User): boolean {
    return user?.externalUid?.endsWith("@unige.ch");
  }

  static isHugUser(user: User): boolean {
    return user?.externalUid?.endsWith("@hcuge.ch");
  }

  static isUnigeOrHugUser(user: User): boolean {
    return this.isUnigeUser(user) || this.isHugUser(user);
  }

  static getCnIndividu(user: User): string {
    if (isNullOrUndefinedOrWhiteString(user?.externalUid)) {
      throw new Error("No external UID for this user");
    }
    if (!UserUtil.isUnigeUser(user)) {
      throw new Error("Non Unige user can't have cnIndividu");
    }
    if (user.externalUid.indexOf("@") === -1) {
      throw new Error("Unable to retrieve cnIndividu on externalUid");
    }
    return user.externalUid.substring(0, user.externalUid.indexOf("@"));
  }
}
