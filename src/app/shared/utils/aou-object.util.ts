/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - aou-object.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isArray,
  isEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isObject,
  MappingObjectUtil,
} from "solidify-frontend";

// TODO MOVE TO SOLIDIFY
export class AouObjectUtil {
  static cleanEmptyAttributes<T>(object: T | any): T | any | undefined {
    if (isArray(object)) {
      return this._cleanEmptyAttributesOnArray(object);
    } else if (isObject(object)) {
      return this._cleanEmptyAttributesOnObject(object);
    } else {
      if (isNullOrUndefined(object) || isEmptyString(object)) {
        return;
      }
      return object;
    }
  }

  private static _cleanEmptyAttributesOnArray(object: any[]): any[] {
    const array = object;
    const newArray = [];
    array.forEach(item => {
      item = this.cleanEmptyAttributes(item);
      if (isNotNullNorUndefined(item)) {
        newArray.push(item);
      }
    });
    if (newArray.length > 0) {
      return newArray;
    }
  }

  private static _cleanEmptyAttributesOnObject<T>(object: T | any): T {
    const newObj = {};
    MappingObjectUtil.forEach(object as any, (value, key) => {
      const valueCleaned = this.cleanEmptyAttributes(value);
      if (isNotNullNorUndefined(value)) {
        MappingObjectUtil.set(newObj as any, key, valueCleaned);
      }
    });
    if (MappingObjectUtil.size(newObj) === 0) {
      return;
    }
    return newObj as T;
  }

  static groupBy<TResource, UResource = string>(listToGroup: TResource[], callbackTransformKey: undefined | ((key: string) => UResource), ...keys: string[]): KeyValuesObject<TResource, UResource>[] {
    const listResult: KeyValuesObject<TResource, UResource>[] = [];
    const reducedList = listToGroup.reduce((rv, x) => {
      (rv[AouObjectUtil._recursivelyGetKeysOnObject(x, ...keys)] = rv[AouObjectUtil._recursivelyGetKeysOnObject(x, ...keys)] || []).push(x);
      return rv;
    }, {});
    MappingObjectUtil.forEach(reducedList, (value: TResource[], key: string) => {
      if (isNullOrUndefined(callbackTransformKey)) {
        listResult.push({key: key as any, value: value});
      } else {
        listResult.push({key: callbackTransformKey(key), value: value});
      }
    });
    return listResult;
  }

  private static _recursivelyGetKeysOnObject(object: any, ...keys: string[]): any {
    if (keys.length === 0 || isNullOrUndefined(object) || isArray(object)) {
      return object;
    }
    const key = keys.splice(0, 1)[0];
    const newObject = object[key];

    return this._recursivelyGetKeysOnObject(newObject, ...keys);
  }
}

export interface KeyValuesObject<TResource, UResource = string> {
  key: UResource;
  value: TResource[];
}

export interface KeyValueObject<TResource, UResource = string> {
  key: UResource;
  value: TResource;
}
