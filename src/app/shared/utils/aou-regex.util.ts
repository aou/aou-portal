/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - aou-regex.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {RegexUtil} from "solidify-frontend";

export class AouRegexUtil extends RegexUtil {
  static partOfORCID: RegExp = /^\-?(\d{1,4}\-?){1,3}((\d{1,4})|(\d{1,3}(x|X)))?$/;
  static isOrcid: RegExp = /^(\d{4}-){3}\d{3}(\d|X)$/;
  static isDoi: RegExp = /^10\.[0-9]{4,}\/[\S]*$|^10\/[a-zA-Z0-9]+$/;
  static isIsbn: RegExp = /^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/;
  static isPmid: RegExp = /^\d+$/;
  static isPmcid: RegExp = /^PMC\d+$|^\d+$/;
  static onlyDigit: RegExp = /^[0-9]+$/;
}

