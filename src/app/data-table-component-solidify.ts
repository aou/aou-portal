/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - data-table-component-solidify.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SharedAccessLevelWithEmbargoPresentational} from "@shared/components/presentationals/shared-access-level-with-embargo/shared-access-level-with-embargo.presentational";
import {SharedAccessLevelPresentational} from "@shared/components/presentationals/shared-access-level/shared-access-level.presentational";
import {SharedFileTypePresentational} from "@shared/components/presentationals/shared-file-type/shared-file-type.presentational";
import {SharedPrincipalFileAccessLevelPresentational} from "@shared/components/presentationals/shared-principal-file-access-level/shared-principal-file-access-level.presentational";
import {SharedOpenAccessPresentational} from "@shared/components/presentationals/shared-open-access/shared-open-access.presentational";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {UserInfoDataTableColumn} from "@shared/models/user-info-data-table-column.model";
import {
  DataTableComponent,
  DataTableComponentInput,
  EnumUtil,
  KeyValue,
  LabelPresentational,
  LogoWrapperContainer,
  MappingObject,
  StatusPresentational,
  ValueType,
} from "solidify-frontend";
import {SharedContributorsListPresentational} from "@shared/components/presentationals/shared-contributors-list/shared-contributors-list.presentational";
import {Enums} from "@enums";
import RoleContributorEnum = Enums.Deposit.RoleContributorEnum;

export const dataTableComponentSolidify: MappingObject<DataTableComponentEnum, DataTableComponent> = {
  [DataTableComponentEnum.status]: {
    componentType: StatusPresentational,
    inputs: [
      {
        key: "statusModel",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col) => EnumUtil.getKeyValue(col.filterEnum as KeyValue[], DataTableComponentHelper.getData(rowData, col)),
      },
      {
        key: "label",
        valueType: ValueType.isStatic,
        staticValue: undefined,
      },
      {
        key: "takeAllWidth",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  } as DataTableComponent,
  [DataTableComponentEnum.logo]: {
    componentType: LogoWrapperContainer,
    inputs: [
      {
        key: "isUser",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "isUser",
      },
      {
        key: "logoNamespace",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "resourceNameSpace",
      },
      {
        key: "logoState",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "resourceState",
      },
      {
        key: "userInfo",
        valueType: ValueType.isRowData,
      },
      {
        key: "idResource",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col: UserInfoDataTableColumn<any>) => col.idResource(rowData),
      },
      {
        key: "isLogoPresent",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col: UserInfoDataTableColumn<any>) => col.isLogoPresent(rowData),
      },
    ],
  } as DataTableComponent,
  [DataTableComponentEnum.accessLevel]: {
    componentType: SharedAccessLevelPresentational,
    inputs: [
      {
        key: "accessLevel",
        valueType: ValueType.isColData,
      },
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  },
  [DataTableComponentEnum.principalFileAccessLevel]: {
    componentType: SharedPrincipalFileAccessLevelPresentational,
    inputs: [
      {
        key: "principalFileAccessLevel",
        valueType: ValueType.isColData,
      },
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  },
  [DataTableComponentEnum.openAccess]: {
    componentType: SharedOpenAccessPresentational,
    inputs: [
      {
        key: "openAccess",
        valueType: ValueType.isColData,
      },
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  },
  [DataTableComponentEnum.accessLevelWithEmbargo]: {
    componentType: SharedAccessLevelWithEmbargoPresentational,
    inputs: [
      {
        key: "accessLevel",
        valueType: ValueType.isColData,
      },
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
      {
        key: "embargoAccessLevel",
        valueType: ValueType.isOtherColData,
        dataCol: "embargoAccessLevel",
      } as DataTableComponentInput,
      {
        key: "embargoEndDate",
        valueType: ValueType.isOtherColData,
        dataCol: "embargoEndDate",
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.label]: {
    componentType: LabelPresentational,
    inputs: [
      {
        key: "labels",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col) => rowData[col.field].labels,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.fileType]: {
    componentType: SharedFileTypePresentational,
    inputs: [
      {
        key: "documentFile",
        valueType: ValueType.isRowData,
      },
    ],
  },
  [DataTableComponentEnum.contributors]: {
    componentType: SharedContributorsListPresentational,
    inputs: [
      {
        key: "deposit",
        valueType: ValueType.isRowData,
      },
      {
        key: "roles",
        valueType: ValueType.isStatic,
        staticValue: [RoleContributorEnum.author, RoleContributorEnum.collaborator, RoleContributorEnum.director, RoleContributorEnum.editor,
          RoleContributorEnum.guest_editor, RoleContributorEnum.photographer, RoleContributorEnum.translator],
      },
      {
        key: "includeCollaborations",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  },
};
