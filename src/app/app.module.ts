/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {registerLocaleData} from "@angular/common";
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
} from "@angular/common/http";
import localeDe from "@angular/common/locales/de";
import localeEn from "@angular/common/locales/en";
import localeFr from "@angular/common/locales/fr";
import {
  APP_INITIALIZER,
  ErrorHandler,
  NgModule,
} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS,
} from "@angular/material-moment-adapter";
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from "@angular/material/core";
import {MatPaginatorIntl} from "@angular/material/paginator";
import {MatSnackBar} from "@angular/material/snack-bar";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ServiceWorkerModule} from "@angular/service-worker";
import {AppRoutingModule} from "@app/app-routing.module";
import {FooterContainer} from "@app/components/container/footer/footer.container";
import {UserProfileDialog} from "@app/components/dialogs/user-profile/user-profile.dialog";
import {GuidedTourMenuPresentational} from "@app/components/presentationals/guided-tour-menu/guided-tour-menu.presentational";
import {MainToolbarMobilePresentational} from "@app/components/presentationals/main-toolbar/main-toolbar-mobile/main-toolbar-mobile.presentational";
import {MainToolbarPresentational} from "@app/components/presentationals/main-toolbar/main-toolbar/main-toolbar.presentational";
import {UserMenuPresentational} from "@app/components/presentationals/user-menu/user-menu.presentational";
import {UserProfileFormPresentational} from "@app/components/presentationals/user-profile-form/user-profile-form.presentational";
import {dataTableComponentSolidify} from "@app/data-table-component-solidify";
import {icons} from "@app/icons";
import {InMemoryStorage} from "@app/in-memory.storage";
import {labelTranslateSolidify} from "@app/label-translate-solidify";
import {metaInfoList} from "@app/meta-info-list";
import {SharedModule} from "@app/shared/shared.module";
import {
  appActionNameSpace,
  AppExtendAction,
} from "@app/stores/app.action";
import {AppState} from "@app/stores/app.state";
import {AppContributorRolesAction} from "@app/stores/contributor-role/app-contributor-roles.action";
import {AppContributorRolesState} from "@app/stores/contributor-role/app-contributor-roles.state";
import {AppDepositSubtypeAction} from "@app/stores/deposit-subtype/app-deposit-subtype.action";
import {AppDepositSubtypeState} from "@app/stores/deposit-subtype/app-deposit-subtype.state";
import {AppNotificationState} from "@app/stores/notification/app-notification.state";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonDepositState} from "@app/stores/person/deposit/app-person-deposit.state";
import {AppPersonNotificationTypeState} from "@app/stores/person/notification-type/app-person-notification-type.state";
import {AppPersonResearchGroupState} from "@app/stores/person/research-group/app-person-research-group.state";
import {AppPersonStructureState} from "@app/stores/person/structure/app-person-structure.state";
import {AppPersonValidationRightStructureState} from "@app/stores/person/validation-right/app-person-validation-right-structure.state";
import {AppPinboardState} from "@app/stores/pinboard/app-pinboard.state";
import {AppSearchState} from "@app/stores/search/app-search.state";
import {AppStoredSearchesState} from "@app/stores/stored-searches/app-stored-searches.state";
import {appSystemPropertyActionNameSpace} from "@app/stores/system-property/app-system-property.action";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {AppTocAction} from "@app/stores/toc/app-toc.action";
import {AppTocState} from "@app/stores/toc/app-toc.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {environment} from "@environments/environment";
import {FormlyModule} from "@ngx-formly/core";
import {FormlyMaterialModule} from "@ngx-formly/material";
import {
  TranslateLoader,
  TranslateModule,
} from "@ngx-translate/core";
import {NgxsReduxDevtoolsPluginModule} from "@ngxs/devtools-plugin";
import {NgxsLoggerPluginModule} from "@ngxs/logger-plugin";
import {NgxsRouterPluginModule} from "@ngxs/router-plugin";
import {
  Actions,
  NgxsModule,
} from "@ngxs/store";
import {SharedStructureDialog} from "@shared/components/dialogs/shared-structure/shared-structure.dialog";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {StoreRouteService} from "@shared/services/store-route.service";
import {
  HighlightModule,
  provideHighlightOptions,
} from "ngx-highlightjs";
import {
  TourMatMenuModule,
  TourService,
} from "ngx-ui-tour-md-menu";
import {
  APP_OPTIONS,
  AppBannerState,
  AppCarouselState,
  AppConfigService,
  CacheBustingUtil,
  COOKIE_CONSENT_PREFERENCES,
  CustomDateAdapter,
  CustomMatPaginatorIntlService,
  DATA_TABLE_COMPONENT,
  ENVIRONMENT,
  ErrorsSkipperService,
  HttpTranslateLoaderExtra,
  ICONS_LIST,
  isFalse,
  isNullOrUndefined,
  isTrue,
  LABEL_TRANSLATE,
  labelTranslateInterface,
  LanguageInterceptor,
  LocaleIdProvider,
  MatDateLocaleProvider,
  META_INFO_LIST,
  NotificationService,
  NOTIFIER_SERVICE,
  OAuth2Interceptor,
  OAuthStorage,
  ofSolidifyActionCompleted,
  SECURITY_SERVICE,
  SNACK_BAR,
  SOLIDIFY_CONSTANTS,
  SolidifyApplicationAppOptions,
  SolidifyFrontendAbstractAppModule,
  SsrInterceptorService,
  SsrUtil,
  StandardErrorsHandlerService,
  STORE_DIALOG_SERVICE,
  STORE_ROUTE_SERVICE,
  TransferStateService,
  VisualizationState,
} from "solidify-frontend";
import {AppComponent} from "./app.component";
import {MainToolbarDesktopHorizontalPresentational} from "./components/presentationals/main-toolbar/main-toolbar-desktop-horizontal/main-toolbar-desktop-horizontal.presentational";
import {PageNotFoundPresentational} from "./components/presentationals/page-not-found/page-not-found.presentational";
import {cookieConsentPreferences} from "./cookie-consent-preferences";
import {Enums} from "./enums";

const presentationals = [
  AppComponent,
  MainToolbarPresentational,
  MainToolbarDesktopHorizontalPresentational,
  MainToolbarMobilePresentational,
  PageNotFoundPresentational,
  GuidedTourMenuPresentational,
  UserMenuPresentational,
  UserProfileFormPresentational,
];

const containers = [
  FooterContainer,
];

const routables = [];

const dialogs = [
  UserProfileDialog,
  SharedStructureDialog,
];

export const appModuleState = [
  AppState,
  AppPinboardState,
  AppUserState,
  AppUserLogoState,
  AppPersonState,
  AppPersonStructureState,
  AppPersonValidationRightStructureState,
  AppPersonNotificationTypeState,
  AppPersonDepositState,
  AppPersonResearchGroupState,
  AppTocState,
  AppSearchState,
  AppSystemPropertyState,
  AppBannerState,
  AppCarouselState,
  AppNotificationState,
  AppStoredSearchesState,
  VisualizationState,
  AppContributorRolesState,
  AppDepositSubtypeState,
  VisualizationState,
];

registerLocaleData(localeFr);
registerLocaleData(localeDe);
registerLocaleData(localeEn);

const appInitializerFn = (appConfig: AppConfigService) => () => appConfig.mergeConfigAndInitApplication(environment, AppState, AppUserState);

export const createDefaultStorage: () => Storage | null = () => environment.tokenInMemoryStorage ? new InMemoryStorage() : (SsrUtil.window?.sessionStorage ? SsrUtil.window.sessionStorage : null);

// required for AOT compilation
export const HttpLoaderFactory: (http: HttpClient, transferState: TransferStateService) => HttpTranslateLoaderExtra = (http, transferState) => new HttpTranslateLoaderExtra(http, {
  suffix: `.json` + CacheBustingUtil.generateCacheBustingQueryParam(),
}, transferState, environment);

export const appOptionsFactory: (_actions$: Actions) => SolidifyApplicationAppOptions = _actions$ => (
  {
    nameSpace: appActionNameSpace,
    baseAppSate: AppState,
    routeMaintenance: RoutesEnum.maintenance,
    routeServerOffline: RoutesEnum.serverOffline,
    routeUnableToLoadApp: RoutesEnum.unableToLoadApp,
    urlRevokeToken: () => ApiEnum.adminUsers + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameEnum.REVOKE_MY_TOKENS,
    urlAdminModule: () => ApiEnum.adminModule,
    urlActiveGlobalBanner: () => ApiEnum.adminGlobalBanners + urlSeparator + ApiActionNameEnum.GET_ACTIVE,
    initApplicationParallel: [
      {action: new AppExtendAction.StartPollingNotification()},
      {action: new AppTocAction.GetAllDocumentation()},
      {
        action: new appSystemPropertyActionNameSpace.GetSystemProperties(),
        subActionCompletions: [
          _actions$.pipe(ofSolidifyActionCompleted(appSystemPropertyActionNameSpace.GetSystemPropertiesSuccess)),
          _actions$.pipe(ofSolidifyActionCompleted(appSystemPropertyActionNameSpace.GetSystemPropertiesFail)),
        ],
      },
      {
        action: new AppContributorRolesAction.GetAll(),
        subActionCompletions: [
          _actions$.pipe(ofSolidifyActionCompleted(AppContributorRolesAction.GetAllSuccess)),
          _actions$.pipe(ofSolidifyActionCompleted(AppContributorRolesAction.GetAllFail)),
        ],
      },
      {
        action: new AppDepositSubtypeAction.GetAll(),
        subActionCompletions: [
          _actions$.pipe(ofSolidifyActionCompleted(AppDepositSubtypeAction.GetAllSuccess)),
          _actions$.pipe(ofSolidifyActionCompleted(AppDepositSubtypeAction.GetAllFail)),
        ],
      },
    ],
    forceLogin: false,
    loadModule: true,
  }
);

@NgModule({
  declarations: [
    ...presentationals,
    ...containers,
    ...routables,
    ...dialogs,
  ],
  imports: [
    // angular
    BrowserModule.withServerTransition({appId: "serverApp"}),
    BrowserAnimationsModule,
    HttpClientModule,

    // NgXs
    NgxsModule.forRoot([
      ...appModuleState,
    ], {
      developmentMode: !environment.production, // Allow to enable freeze store in dev env
    }),
    NgxsLoggerPluginModule.forRoot({
      logger: console,
      collapsed: false,
      disabled: !SsrUtil.window || isTrue(SsrUtil.isServer) || isFalse(environment.showDebugInformation) || isNullOrUndefined(environment.showDebugInformation),
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: !SsrUtil.window || isTrue(SsrUtil.isServer) || isFalse(environment.showDebugInformation) || isNullOrUndefined(environment.showDebugInformation),
    }),
    NgxsRouterPluginModule.forRoot(),

    // NgTranslate
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient, TransferStateService],
      },
    }),

    // Shared
    SharedModule,

    // App
    AppRoutingModule,
    HighlightModule,
    ServiceWorkerModule.register(environment.serviceWorkerFileName, {
      enabled: environment.production || environment.forceServiceWorker,
      registrationStrategy: "registerImmediately",
      scope: () => environment.baseHref,
    }),
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyMaterialModule,
    TourMatMenuModule,
  ],
  providers: [
    TourService,
    {
      provide: ENVIRONMENT,
      useValue: environment,
    },
    {
      provide: SECURITY_SERVICE,
      useClass: SecurityService,
    },
    {
      provide: LABEL_TRANSLATE,
      useValue: {...labelTranslateInterface, ...labelTranslateSolidify},
    },
    {
      provide: COOKIE_CONSENT_PREFERENCES,
      useValue: cookieConsentPreferences,
    },
    {
      provide: DATA_TABLE_COMPONENT,
      useValue: dataTableComponentSolidify,
    },
    {
      provide: STORE_ROUTE_SERVICE,
      useClass: StoreRouteService,
    },
    {
      provide: STORE_DIALOG_SERVICE,
      useClass: StoreDialogService,
    },
    {
      provide: ICONS_LIST,
      useValue: icons,
    },
    {
      provide: META_INFO_LIST,
      useValue: metaInfoList,
    },
    {
      provide: NOTIFIER_SERVICE,
      useValue: NotificationService,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [AppConfigService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: OAuth2Interceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LanguageInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: SsrInterceptorService,
      multi: true,
    },
    {
      provide: OAuthStorage,
      useFactory: createDefaultStorage,
    },
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntlService,
    },
    {
      provide: ErrorHandler,
      useClass: StandardErrorsHandlerService,
    },
    {
      provide: SNACK_BAR,
      useClass: MatSnackBar,
    },
    LocaleIdProvider,
    {
      provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
      useValue: {
        useUtc: true,
      },
    },
    MatDateLocaleProvider,
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    ErrorsSkipperService,
    provideHighlightOptions({
      coreLibraryLoader: () => import("highlight.js/lib/core"),
      languages: {
        [Enums.HighlightJS.HighlightLanguageEnum.xml]: () => import("highlight.js/lib/languages/xml"),
        [Enums.HighlightJS.HighlightLanguageEnum.json]: () => import("highlight.js/lib/languages/json"),
      },
      themePath: environment.highlightJsThemeLight,
    }),
    {
      provide: APP_OPTIONS,
      useFactory: appOptionsFactory,
      deps: [Actions],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule extends SolidifyFrontendAbstractAppModule {
}
