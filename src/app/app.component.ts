/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - app.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Renderer2,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {UserProfileDialog} from "@app/components/dialogs/user-profile/user-profile.dialog";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {appActionNameSpace} from "@app/stores/app.action";
import {AppState} from "@app/stores/app.state";
import {AppPersonDepositState} from "@app/stores/person/deposit/app-person-deposit.state";
import {AppPersonValidationRightStructureState} from "@app/stores/person/validation-right/app-person-validation-right-structure.state";
import {AppSystemPropertyState} from "@app/stores/system-property/app-system-property.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SearchHelper} from "@home/helpers/search.helper";
import {
  StaticPage,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {SessionStorageEnum} from "@shared/enums/session-storage.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {AouLabelUtil} from "@shared/utils/aou-label.util";
import {PermissionUtil} from "@shared/utils/permission.util";
import {TourService} from "ngx-ui-tour-md-menu";
import {
  combineLatest,
  Observable,
} from "rxjs";
import {
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AppConfigService,
  AppStatusService,
  BreakpointService,
  CookieConsentService,
  DialogUtil,
  GlobalBanner,
  GoogleAnalyticsService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LoggingService,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  MetaService,
  NotificationService,
  ScrollService,
  SessionStorageHelper,
  SOLIDIFY_CONSTANTS,
  SolidifyApplicationAbstractAppComponent,
  SolidifyGlobalBannerState,
  SsrUtil,
  TransferStateService,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "aou-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends SolidifyApplicationAbstractAppComponent {
  currentPhotoUserObs: Observable<string> = MemoizedUtil.select(this._store, AppUserLogoState, state => state.file);
  userRolesObs: Observable<Enums.UserApplicationRole.UserApplicationRoleEnum[]> = MemoizedUtil.select(this._store, AppState, state => state.userRoles);
  userAuthorizedForDepositActionsObs: Observable<boolean> = MemoizedUtil.select(this._store, AppUserState, state => state.userAuthorizedForDepositActions);
  isValidationRightObs: Observable<boolean> = MemoizedUtil.select(this._store, AppPersonValidationRightStructureState, state => state.selected?.length > 0);
  isInTourModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isInTourMode);
  listStaticPagesObs: Observable<StaticPage[]> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current?.staticPages);
  numberPublicationPinnedObs: Observable<number> = MemoizedUtil.selected(this._store, AppPersonDepositState)
    .pipe(map(list => isNullOrUndefined(list) ? 0 : list.length));

  globalBannerObs: Observable<GlobalBanner> = MemoizedUtil.select(this._store, SolidifyGlobalBannerState, state => state.globalBanner);

  currentModule: string;
  isHomePage: boolean;
  showSearchInputInMainToolbar: boolean;

  displayBreadcrumb = environment.displayBreadcrumb;

  get sessionStorageHelper(): typeof SessionStorageHelper {
    return SessionStorageHelper;
  }

  get localStorageEnum(): typeof LocalStorageEnum {
    return LocalStorageEnum;
  }

  get sessionStorageEnum(): typeof SessionStorageEnum {
    return SessionStorageEnum;
  }

  get routesEnum(): typeof RoutesEnum {
    return RoutesEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get labelUtil(): typeof AouLabelUtil {
    return AouLabelUtil;
  }

  get ssrUtil(): typeof SsrUtil {
    return SsrUtil;
  }

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _router: Router,
              protected readonly _translate: TranslateService,
              protected readonly _renderer: Renderer2,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService,
              protected readonly _loggingService: LoggingService,
              public readonly appStatusService: AppStatusService,
              public readonly breakpointService: BreakpointService,
              protected readonly _dialog: MatDialog,
              public readonly tourService: TourService,
              protected readonly _scrollService: ScrollService,
              protected readonly _googleAnalyticsService: GoogleAnalyticsService,
              protected readonly _cookieConsentService: CookieConsentService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _metaService: MetaService,
              protected readonly _transferState: TransferStateService,
              protected readonly _securityService: SecurityService,
  ) {
    super(_store,
      _actions$,
      _router,
      _translate,
      _renderer,
      _injector,
      _notificationService,
      _loggingService,
      appStatusService,
      breakpointService,
      _dialog,
      _scrollService,
      _googleAnalyticsService,
      _cookieConsentService,
      _appConfigService,
      _changeDetector,
      _metaService,
      _transferState,
      _securityService,
    );
    this._listenCurrentModule();
    if (SsrUtil.isBrowser) {
      this.subscribe(this._observeLoggedUserForCart());
      this._checkOrcidQueryParam();
    }
  }

  private _observeLoggedUserForCart(): Observable<boolean> {
    return combineLatest([this.isLoggedObs, this.userRolesObs])
      .pipe(
        map(([isLogged, userRoles]) => this.computeDisplayCart(isLogged, userRoles)),
      );
  }

  private _checkOrcidQueryParam(): void {
    if (UrlQueryParamHelper.currentUrlContainsQueryParam(environment.orcidQueryParam)) {
      let message: string = MARK_AS_TRANSLATABLE("notification.orcid.associated");
      let success: boolean = true;
      let errorDescription: string;
      if (!UrlQueryParamHelper.currentUrlContainsQueryParameterValue(environment.orcidQueryParam, "true")) {
        success = false;
        if (UrlQueryParamHelper.currentUrlContainsQueryParameterValue(environment.orcidErrorQueryParam, "access_denied")) {
          message = MARK_AS_TRANSLATABLE("notification.orcid.association_canceled");
        } else if (UrlQueryParamHelper.currentUrlContainsQueryParam(environment.orcidErrorDescriptionQueryParam)) {
          const mapping = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl(true);
          errorDescription = MappingObjectUtil.get(mapping, environment.orcidErrorDescriptionQueryParam);
          message = MARK_AS_TRANSLATABLE("notification.orcid.association_error");
        }
      }

      this.subscribe(this.currentUserObs.pipe(
        filter(user => isNotNullNorUndefined(user)),
        take(1),
        tap((user: User) => {
          DialogUtil.open(this._dialog, UserProfileDialog, user, {
            width: "90%",
          });
          if (success) {
            this._notificationService.showSuccess(message, {orcid: user?.person?.orcid});
          } else {
            if (isNotNullNorUndefined(errorDescription)) {
              this._notificationService.showError(message, {error_description: errorDescription});
            } else {
              this._notificationService.showWarning(message);
            }
          }
        }),
      ));
    }
  }

  private _listenCurrentModule(): void {
    if (SsrUtil.isServer) {
      this._computeCurrentModule(SsrUtil.window.location.pathname);
    } else {
      this.subscribe(this.urlStateObs.pipe(tap(urlState => this._computeCurrentModule(urlState?.url))));
    }
  }

  private _computeCurrentModule(url: string): void {
    this.ignoreGrid = false;
    this.isHomePage = false;
    this.showSearchInputInMainToolbar = true;
    this.currentModule = undefined;

    if (isNullOrUndefined(url)) {
      return;
    }
    this._setCurrentModuleAndGrid(url);
  }

  private _setCurrentModuleAndGrid(url: string): void {
    if (url === SOLIDIFY_CONSTANTS.URL_SEPARATOR) {
      this.currentModule = undefined;
      this.ignoreGrid = true;
      return;
    }

    const urlHomeDetail = SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeDetail;
    if (url.startsWith(urlHomeDetail)) {
      this.currentModule = undefined;
      if (url.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR) === urlHomeDetail.length) {
        this.ignoreGrid = true;
      }
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeDetailUnige)
      || url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeDetailAouTest)) {
      this.currentModule = undefined;
      this.ignoreGrid = true;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeSearch)) {
      this.currentModule = undefined;
      this.showSearchInputInMainToolbar = false;
      this.ignoreGrid = true;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homePinboard)) {
      this.currentModule = RoutesEnum.homePinboard;
      this.ignoreGrid = true;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeBibliography)) {
      this.currentModule = RoutesEnum.homeBibliography;
      this.ignoreGrid = true;
      return;
    }

    if (url === SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homePage || url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homePage + "?")) {
      this.currentModule = RoutesEnum.homePage;
      this.isHomePage = true;
      this.showSearchInputInMainToolbar = false;
      this.ignoreGrid = true;
      return;
    }

    if (url === SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.colorCheck) {
      this.currentModule = undefined;
      this.isHomePage = true;
      this.ignoreGrid = true;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.notification)) {
      this.currentModule = RoutesEnum.notification;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.deposit + SOLIDIFY_CONSTANTS.URL_SEPARATOR)) {
      this.currentModule = RoutesEnum.deposit;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.depositToValidate + SOLIDIFY_CONSTANTS.URL_SEPARATOR)) {
      this.currentModule = RoutesEnum.depositToValidate;
      return;
    }
  }

  computeDisplayCart(isLogged: boolean, userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    this.displayCart = PermissionUtil.isUserHavePermission(isLogged, ApplicationRolePermissionEnum.userPermission, userRoles);
    return this.displayCart;
  }

  search(searchTerm: string): void {
    this._store.dispatch(SearchHelper.generateSearchPageNavigateAction({
      search: searchTerm,
    }));
  }

  openUserGuideSidebar(): void {
    this._store.dispatch(new appActionNameSpace.ChangeDisplaySidebarUserGuide(true));
  }

  openValidatorGuideSidebar(): void {
    this._store.dispatch(new appActionNameSpace.ChangeDisplaySidebarValidatorGuide(true));
  }
}
