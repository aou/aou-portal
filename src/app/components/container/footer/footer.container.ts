/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - footer.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  FlexibleConnectedPositionStrategy,
  Overlay,
} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  Output,
} from "@angular/core";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {AppTocState} from "@app/stores/toc/app-toc.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedContactPresentational} from "@shared/components/presentationals/shared-contact/shared-contact.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  CookieConsentService,
  MappingObject,
  MemoizedUtil,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-footer-container",
  templateUrl: "./footer.container.html",
  styleUrls: ["./footer.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterContainer extends SharedAbstractContainer {
  isLoadingDocObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppTocState);
  toolsGuideObs: Observable<string> = MemoizedUtil.select(this._store, AppTocState, state => state.toolsGuide);
  integrationGuideObs: Observable<string> = MemoizedUtil.select(this._store, AppTocState, state => state.integrationGuide);
  apisObs: Observable<MappingObject<string, string>> = MemoizedUtil.select(this._store, AppTocState, state => state.apis);
  darkModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.darkMode);

  @Input()
  updateVersion: string | undefined;

  private readonly _updateBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("updateChange")
  readonly updateObs: Observable<void> = ObservableUtil.asObservable(this._updateBS);

  constructor(private readonly _store: Store,
              private readonly _overlay: Overlay,
              private readonly _elementRef: ElementRef,
              private readonly _cookieConsentService: CookieConsentService) {
    super();
  }

  update(): void {
    this._updateBS.next();
  }

  openCommentOrQuestion(): void {
    const overlayRef = this._overlay.create({
      positionStrategy: this._getPosition(this._elementRef),
      scrollStrategy: this._overlay.scrollStrategies.block(),
      hasBackdrop: true,
      backdropClass: "cdk-overlay-transparent-backdrop",
      // width: minWidthField,
    });

    const overlayContentComponent = this._getComponentPortal();
    const componentRef = overlayRef.attach(overlayContentComponent);

    this.subscribe(overlayRef.backdropClick().pipe(
      tap(click => {
        overlayRef.detach();
      }),
    ));
  }

  private _getComponentPortal(): ComponentPortal<SharedContactPresentational> {
    return new ComponentPortal<SharedContactPresentational>(SharedContactPresentational);
  }

  private _getPosition(elementToConnectTo: ElementRef): FlexibleConnectedPositionStrategy {
    return this._overlay.position()
      .flexibleConnectedTo(elementToConnectTo)
      .withFlexibleDimensions(false)
      .withPositions([{
        originX: "start",
        originY: "top",
        overlayX: "start",
        overlayY: "top",
        offsetY: -89,
        offsetX: 10,
      }]);
  }

  openCookieConsentSideBar(): void {
    this._cookieConsentService.openPersonalizationSidebar();
  }

  navigateToAbout(): void {
    this._store.dispatch(new Navigate([RoutesEnum.about]));
  }
}
