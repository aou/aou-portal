/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - abstract-main-toolbar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  Input,
  Output,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {ThemeEnum} from "@app/shared/enums/theme.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  StaticPage,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {PermissionUtil} from "@shared/utils/permission.util";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefinedNorWhiteString,
  LabelUtil,
  LoginMode,
  OAuth2Service,
  ObservableOrPromiseOrValue,
  ObservableUtil,
  SsrUtil,
} from "solidify-frontend";
import LanguageEnum = Enums.Language.LanguageEnum;

@Directive()
export abstract class AbstractMainToolbarPresentational extends SharedAbstractPresentational {
  @Input()
  logged: boolean = false;

  @Input()
  currentPath: string;

  @Input()
  user: User;

  @Input()
  photoUser: string;

  institutionUrl: string = environment.institutionUrl;

  @Input()
  numberPublicationPinned: number = 0;

  @Input()
  numberPendingRequestNotifications: number = 0;

  @Input()
  isValidationRight: boolean;

  @Input()
  isInTourMode: boolean;

  @Input()
  userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  @Input()
  userAuthorizedForDepositActions: boolean;

  // Theme
  private readonly _themeBS: BehaviorSubject<ThemeEnum | undefined> = new BehaviorSubject<ThemeEnum | undefined>(undefined);
  @Output("themeChange")
  readonly themeObs: Observable<ThemeEnum | undefined> = ObservableUtil.asObservable(this._themeBS);

  private readonly _userGuideOpenBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("userGuideOpen")
  readonly userGuideOpenObs: Observable<void | undefined> = ObservableUtil.asObservable(this._userGuideOpenBS);

  // Dark mode
  private readonly _darkModeBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("darkModeChange")
  readonly darkModeObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._darkModeBS);

  isProduction: boolean = environment.production;
  isDemoMode: boolean = environment.isDemoMode;
  isOpen: boolean = false;

  private get _theme(): ThemeEnum | undefined {
    return this._themeBS.getValue();
  }

  private set _theme(value: ThemeEnum | undefined) {
    this._themeBS.next(value);
  }

  get theme(): ThemeEnum | undefined {
    return this._theme;
  }

  @Input()
  set theme(theme: ThemeEnum | undefined) {
    this._theme = theme;
  }

  private get _language(): LanguageEnum | undefined {
    return this._languageBS.getValue();
  }

  private set _language(value: LanguageEnum | undefined) {
    this._languageBS.next(value);
  }

  get language(): LanguageEnum | undefined {
    return this._language;
  }

  @Input()
  set language(language: LanguageEnum | undefined) {
    this._language = language;
  }

  private get _darkMode(): boolean | undefined {
    return this._darkModeBS.getValue();
  }

  private set _darkMode(value: boolean | undefined) {
    this._darkModeBS.next(value);
  }

  get darkMode(): boolean | undefined {
    return this._darkMode;
  }

  @Input()
  set darkMode(darkMode: boolean | undefined) {
    this._darkMode = darkMode;
  }

  // Navigate
  protected readonly _navigateBS: BehaviorSubject<string | Navigate | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<string | Navigate | undefined> = ObservableUtil.asObservable(this._navigateBS);

  // Language
  private readonly _languageBS: BehaviorSubject<Enums.Language.LanguageEnum | undefined> = new BehaviorSubject<Enums.Language.LanguageEnum | undefined>(undefined);
  @Output("languageChange")
  readonly languageObs: Observable<Enums.Language.LanguageEnum | undefined> = ObservableUtil.asObservable(this._languageBS);

  // Logout
  private readonly _logoutBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("logoutChange")
  readonly logoutObs: Observable<void | undefined> = ObservableUtil.asObservable(this._logoutBS);

  listMenuPublicItems: MenuToolbar[] = [
    {
      click: menu => this._clickOnMenu(menu),
      path: () => RoutesEnum.homePage,
      rootModulePath: RoutesEnum.homePage,
      labelToTranslate: LabelTranslateEnum.home,
      isVisible: () => true,
      icon: IconNameEnum.home,
      dataTest: DataTestEnum.linkMenuHome,
    },
  ];

  listMenuLoggedItems: MenuToolbar[] = [
    {
      click: menu => this._clickOnMenu(menu),
      path: () => RoutesEnum.depositToValidate,
      rootModulePath: RoutesEnum.depositToValidate,
      labelToTranslate: LabelTranslateEnum.depositsToValidate,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.userPermission, this.userRoles) && (this._securityService.isRootOrAdmin() || this.isValidationRight || this.isInTourMode) && PermissionUtil.isUserMemberAuthorizedInstitutions(this.logged, this.userAuthorizedForDepositActions),
      icon: IconNameEnum.depositToValidate,
      dataTest: DataTestEnum.linkMenuPublicationToValidate,
      tourAnchor: TourEnum.mainMenuDepositToValidate,
    },
    {
      click: menu => this._clickOnMenu(menu),
      path: () => RoutesEnum.deposit,
      rootModulePath: RoutesEnum.deposit,
      labelToTranslate: LabelTranslateEnum.myDeposits,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.userPermission, this.userRoles) && PermissionUtil.isUserMemberAuthorizedInstitutions(this.logged, this.userAuthorizedForDepositActions),
      icon: IconNameEnum.deposit,
      dataTest: DataTestEnum.linkMenuPublication,
      tourAnchor: TourEnum.mainMenuDeposit,
    },
    {
      click: menu => this._clickOnMenu(menu),
      path: () => RoutesEnum.notification,
      rootModulePath: RoutesEnum.notification,
      labelToTranslate: LabelTranslateEnum.notifications,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.userPermission, this.userRoles) && PermissionUtil.isUserMemberAuthorizedInstitutions(this.logged, this.userAuthorizedForDepositActions),
      icon: IconNameEnum.notifications,
      dataTest: DataTestEnum.linkMenuNotification,
      badgeCounter: () => "" + this.numberPendingRequestNotifications,
      badgeDescription: "Counter of pending request notification in inbox",
      badgeHidden: () => this.numberPendingRequestNotifications === 0,
    },
    {
      click: menu => this._clickOnMenu(menu),
      path: () => RoutesEnum.homePinboard,
      rootModulePath: RoutesEnum.homePinboard,
      labelToTranslate: LabelTranslateEnum.pinboard,
      isVisible: () => this.logged,
      icon: IconNameEnum.pinboard,
      dataTest: DataTestEnum.linkMenuHome,
      badgeCounter: () => "" + this.numberPublicationPinned,
      badgeDescription: "Counter of publication pinned",
      badgeHidden: () => true,
    },
  ];

  listMenusAdmin: MenuToolbar[] = [
    {
      click: menu => this._clickOnMenu(menu),
      path: () => RoutesEnum.admin,
      rootModulePath: RoutesEnum.admin,
      labelToTranslate: LabelTranslateEnum.administration,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.adminPermission, this.userRoles) && PermissionUtil.isUserMemberAuthorizedInstitutions(this.logged, this.userAuthorizedForDepositActions),
      icon: IconNameEnum.administration,
      dataTest: DataTestEnum.linkMenuAdmin,
    },
  ];

  @Input()
  listStaticPages: StaticPage[];

  get routesEnum(): typeof RoutesEnum {
    return RoutesEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  protected constructor(protected readonly _oauthService: OAuth2Service,
                        protected readonly _securityService: SecurityService,
                        protected readonly _store: Store) {
    super();
  }

  getAllMenusMerged(): MenuToolbar[] {
    return [...this.listMenuPublicItems, ...this.listMenuLoggedItems, ...this.listMenusAdmin];
  }

  private _clickOnMenu(menu): void {
    this.navigate(menu.path());
  }

  navigate(path: string | Navigate): void {
    this._navigateBS.next(path);
  }

  useLanguage(language: Enums.Language.LanguageEnum): void {
    this._languageBS.next(language);
  }

  toggleMenu(): void {
    this.isOpen = !this.isOpen;
  }

  openUserGuide(): void {
    this._userGuideOpenBS.next();
  }

  login(): void {
    const currentPath = SsrUtil.window?.location.pathname + SsrUtil.window?.location.search;
    this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, currentPath);
  }

  logout(): void {
    this._logoutBS.next();
  }

  openStaticLink(staticPage: StaticPage): void {
    const href = staticPage.labels.find(label => LabelUtil.isSameLanguage(this.language as any, label, environment.labelEnumConverter))?.url;
    if (isNotNullNorUndefinedNorWhiteString(href)) {
      SsrUtil.window.open(href, "_blank");
    }
  }
}

export interface MenuToolbar {
  path?: () => string;
  rootModulePath: string;
  click: (menu: MenuToolbar) => void;
  labelToTranslate: string;
  icon: IconNameEnum;
  isVisible: () => ObservableOrPromiseOrValue<boolean>;
  badgeCounter?: () => string;
  badgeDescription?: string;
  badgeHidden?: () => boolean;
  dataTest?: DataTestEnum;
  tourAnchor?: TourEnum;
}
