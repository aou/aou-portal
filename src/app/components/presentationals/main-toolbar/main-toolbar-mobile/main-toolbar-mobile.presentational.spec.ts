/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - main-toolbar-mobile.presentational.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClientTestingModule} from "@angular/common/http/testing";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {
  async,
  ComponentFixture,
  TestBed,
} from "@angular/core/testing";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LanguageSelectorPresentational} from "@app/components/presentationals/language-selector/language-selector.presentational";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {
  ENVIRONMENT,
  SNACK_BAR,
} from "solidify-frontend";
import {environment} from "../../../../../environments/environment";
import {MockTranslatePipe} from "../../../../../test-helpers/mock-translate.pipe";
import {MainToolbarMobilePresentational} from "./main-toolbar-mobile.presentational";

describe("MainToolbarMobilePresentational", () => {
  let component: MainToolbarMobilePresentational;
  let fixture: ComponentFixture<MainToolbarMobilePresentational>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainToolbarMobilePresentational, LanguageSelectorPresentational, MockTranslatePipe],
      imports: [HttpClientTestingModule, FontAwesomeModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
        {
          provide: SNACK_BAR,
          useClass: MatSnackBar,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainToolbarMobilePresentational);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit("should create", () => {
    expect(component).toBeTruthy();
  });
});
