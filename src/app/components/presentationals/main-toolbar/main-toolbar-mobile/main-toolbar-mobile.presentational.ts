/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - main-toolbar-mobile.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {UserProfileDialog} from "@app/components/dialogs/user-profile/user-profile.dialog";
import {AbstractMainToolbarPresentational} from "@app/components/presentationals/main-toolbar/abstract-main-toolbar/abstract-main-toolbar.presentational";
import {environment} from "@environments/environment";
import {Store} from "@ngxs/store";
import {SecurityService} from "@shared/services/security.service";
import {
  DialogUtil,
  FrontendVersion,
  OAuth2Service,
} from "solidify-frontend";

@Component({
  selector: "aou-main-toolbar-mobile",
  templateUrl: "./main-toolbar-mobile.presentational.html",
  styleUrls: ["./main-toolbar-mobile.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("menuAnimation", [
      state("void", style({transform: "translateY(-100%)"})),
      transition(":enter", animate("600ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
  ],
})
export class MainToolbarMobilePresentational extends AbstractMainToolbarPresentational {
  @Input()
  frontendVersion: FrontendVersion;

  get numberThemes(): number {
    return environment.appThemes.length;
  }

  constructor(protected readonly _oauthService: OAuth2Service,
              protected readonly _securityService: SecurityService,
              protected readonly _dialog: MatDialog,
              protected readonly _store: Store) {
    super(_oauthService, _securityService, _store);
  }

  override navigate(path: string): void {
    super.navigate(path);
    this.toggleMenu();
  }

  override logout(): void {
    super.logout();
    this.toggleMenu();
  }

  override login(): void {
    super.login();
    this.toggleMenu();
  }

  profileInfo(): void {
    DialogUtil.open(this._dialog, UserProfileDialog, this.user, {
      width: "90%",
    });
  }

  toggleDarkMode(): void {
    this.darkMode = !this.darkMode;
  }

  override openUserGuide(): void {
    super.openUserGuide();
    this.toggleMenu();
  }
}
