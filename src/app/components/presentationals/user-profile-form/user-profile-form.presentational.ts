/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - user-profile-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  NotificationType,
  Person,
  ResearchGroup,
  Structure,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {CookieEnum} from "@shared/enums/cookie.enum";
import {HintTranslateEnum} from "@shared/enums/hint-translate.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {TooltipTranslateEnum} from "@shared/enums/tooltip-translate.enum";
import {AouCookieConsentHelper} from "@shared/helpers/aou-cookie-consent.helper";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {SecurityService} from "@shared/services/security.service";
import {sharedResearchGroupActionNameSpace} from "@shared/stores/research-group/shared-research-group.action";
import {SharedResearchGroupState} from "@shared/stores/research-group/shared-research-group.state";
import {UserUtil} from "@shared/utils/user.util";
import {tap} from "rxjs/operators";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  CookieConsentService,
  CookieConsentUtil,
  CookieHelper,
  CookieType,
  FormValidationHelper,
  isFalse,
  isNonEmptyArray,
  isNullOrUndefined,
  KeyValue,
  MappingObject,
  OrcidService,
  OrderEnum,
  PersonWithEmail,
  PropertyName,
  ResourceNameSpace,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "aou-user-profile-form",
  templateUrl: "./user-profile-form.presentational.html",
  styleUrls: ["./user-profile-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileFormPresentational extends AbstractFormPresentational<Person> {
  displayExtraProfileInformation: boolean = environment.displayExtraProfileInformation;
  allowSyncFromOrcidProfileNow: boolean = false;
  allowSyncToOrcidProfileNow: boolean = false;

  @Input()
  selectedStructure: Structure[];

  @Input()
  userAuthorizedForDepositActions: boolean;

  @Input()
  selectedValidationRight: ValidationRight[];

  @Input()
  selectedNotificationType: NotificationType[];

  @Input()
  selectedResearchGroup: ResearchGroup[];

  isUnigeOrHugUser: boolean;

  @Input()
  set user(user: User) {
    this.formUser = this._fb.group({
      [this.formDefinition.firstName]: [user.person.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [user.person.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinitionUser.homeOrganization]: [user.homeOrganization, [Validators.required, SolidifyValidator]],
      [this.formDefinitionUser.email]: [user.email, [Validators.required, SolidifyValidator]],
      [this.formDefinitionUser.applicationRole]: [user.applicationRole.resId, [Validators.required, SolidifyValidator]],
    });
    this.formUser.disable();
    this.isUnigeOrHugUser = UserUtil.isUnigeOrHugUser(user);
  }

  @Input()
  isStructureMandatory: boolean;

  @ViewChild("slideToggleWithRestrictedAccessMasters")
  slideToggleWithRestrictedAccessMasters: MatSlideToggle;

  formDefinition: PersonFormComponentFormDefinition = new PersonFormComponentFormDefinition();
  formDefinitionUser: UserFormComponentFormDefinition = new UserFormComponentFormDefinition();

  formUser: FormGroup;

  sharedResearchGroupSort: Sort<ResearchGroup> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedResearchGroupActionNameSpace: ResourceNameSpace = sharedResearchGroupActionNameSpace;
  sharedResearchGroupState: typeof SharedResearchGroupState = SharedResearchGroupState;
  researchGroupLabel: (value: ResearchGroup) => string = value => SharedResearchGroupState.labelCallback(value, this._translateService);
  researchGroupPreTreatmentHighlightText: (result: string) => string = SharedResearchGroupState.preTreatmentHighlightText;
  researchGroupPostTreatmentHighlightText: (result: string, resultBeforePreTreatment: string) => string = SharedResearchGroupState.postTreatmentHighlightText;

  roles: KeyValue[] = Enums.UserApplicationRole.UserApplicationRoleEnumTranslate;
  researchGroupQueryParam: MappingObject<string, string> = {
    validated: SOLIDIFY_CONSTANTS.STRING_TRUE,
  };

  formControlNonPublicMaster: FormControl = new FormControl();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get tooltipTranslateEnum(): typeof TooltipTranslateEnum {
    return TooltipTranslateEnum;
  }

  get hintTranslateEnum(): typeof HintTranslateEnum {
    return HintTranslateEnum;
  }

  get personWithEmail(): PersonWithEmail {
    return {
      firstName: this.getFormControlUser(this.formDefinition.firstName).value,
      lastName: this.getFormControlUser(this.formDefinition.lastName).value,
      email: this.getFormControlUser(this.formDefinitionUser.email).value,
    };
  }

  getFormControlUser(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.formUser, key);
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly orcidService: OrcidService,
              private readonly _translateService: TranslateService,
              protected readonly _dialog: MatDialog,
              readonly securityService: SecurityService,
              private readonly _cookieConsentService: CookieConsentService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(person: Person): void {
    const formArrayValidationRight = this._fb.array([]);
    this.form = this._fb.group({
      [this.formDefinition.firstName]: [person.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [person.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: [person.orcid],
      [this.formDefinition.syncFromOrcidProfile]: [person.syncFromOrcidProfile],
      [this.formDefinition.syncFromOrcidProfileNow]: [false],
      [this.formDefinition.syncToOrcidProfile]: [person.syncToOrcidProfile],
      [this.formDefinition.syncToOrcidProfileNow]: [false],
      [this.formDefinition.notificationTypes]: [isNullOrUndefined(this.selectedNotificationType) ? [] : this.selectedNotificationType.map(n => n.resId)],
      [this.formDefinition.structures]: [isNullOrUndefined(this.selectedStructure) ? [] : this.selectedStructure.map(n => n.resId), this.isStructureMandatory ? [Validators.required] : []],
      [this.formDefinition.researchGroups]: [isNullOrUndefined(this.selectedResearchGroup) ? [] : this.selectedResearchGroup.map(n => n.resId)],
      [this.formDefinition.validationRight]: formArrayValidationRight,
    });

    this.allowSyncFromOrcidProfileNow = isNullOrUndefined(person.syncFromOrcidProfile) || isFalse(person.syncFromOrcidProfile);
    this.allowSyncToOrcidProfileNow = isNullOrUndefined(person.syncToOrcidProfile) || isFalse(person.syncToOrcidProfile);

    if (isNonEmptyArray(this.selectedValidationRight)) {
      this.selectedValidationRight.forEach(validationRight => {
        const listId = validationRight.publicationSubtypes.map(r => r.resId);
        const formGroupValidationRight = this._fb.group({
          id: [validationRight.resId, Validators.required],
          listId: [listId],
        });
        formArrayValidationRight.push(formGroupValidationRight as any);
      });
    }
    formArrayValidationRight.disable();

    this._managePublicationWithRestrictedAccessMasters();
  }

  private _managePublicationWithRestrictedAccessMasters(): void {
    const withRestrictedAccessMaster = AouCookieConsentHelper.isWithRestrictedAccessMasters();
    this.formControlNonPublicMaster.setValue(withRestrictedAccessMaster);

    this.subscribe(this.formControlNonPublicMaster.valueChanges.pipe(tap(isChecked => {
      if (!this.isPublicationWithRestrictedAccessMastersFeatureAuthorized) {
        this._cookieConsentService.notifyFeatureDisabledBecauseCookieDeclined(CookieType.cookie, CookieEnum.publicationWithRestrictedAccessMasters);
        this.formControlNonPublicMaster.setValue(false, {
          emitEvent: false,
        });
        this.slideToggleWithRestrictedAccessMasters.checked = false;
        this._changeDetectorRef.detectChanges();
      }
    })));
  }

  get isPublicationWithRestrictedAccessMastersFeatureAuthorized(): boolean {
    return CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookieEnum.publicationWithRestrictedAccessMasters);
  }

  notifyPublicationWithRestrictedAccessMastersFeatureNeedToBeAuthorized(): void {
    if (this.isPublicationWithRestrictedAccessMastersFeatureAuthorized) {
      return;
    }
    this._cookieConsentService.notifyFeatureDisabledBecauseCookieDeclined(CookieType.cookie, CookieEnum.publicationWithRestrictedAccessMasters);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: [""],
      [this.formDefinition.syncFromOrcidProfile]: [""],
      [this.formDefinition.syncFromOrcidProfileNow]: [false],
      [this.formDefinition.syncToOrcidProfile]: [""],
      [this.formDefinition.syncToOrcidProfileNow]: [false],
      [this.formDefinition.structures]: [[]],
      [this.formDefinition.notificationTypes]: [[]],
      [this.formDefinition.researchGroups]: [[]],
      [this.formDefinition.validationRight]: this._fb.array([]),
    });
  }

  protected _treatmentBeforeSubmit(person: Person): Person {
    person.notificationTypes = [];
    const listNotificationTypes = this.form.get(this.formDefinition.notificationTypes).value;
    listNotificationTypes?.forEach(resId => {
      person.notificationTypes.push({resId: resId});
    });

    person.researchGroups = [];
    const listResearchGroups = this.form.get(this.formDefinition.researchGroups).value;
    listResearchGroups?.forEach(resId => {
      person.researchGroups.push({resId: resId});
    });

    person.structures = [];
    const listStructures = this.form.get(this.formDefinition.structures).value;
    listStructures?.forEach(resId => {
      person.structures.push({resId: resId});
    });

    delete person[this.formDefinition.syncToOrcidProfileNow];
    delete person[this.formDefinition.syncFromOrcidProfileNow];
    return person;
  }

  override onSubmit(): void {
    super.onSubmit();
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookieEnum.publicationWithRestrictedAccessMasters)) {
      CookieHelper.setItem(CookieEnum.publicationWithRestrictedAccessMasters, this.formControlNonPublicMaster.value ? SOLIDIFY_CONSTANTS.STRING_TRUE : SOLIDIFY_CONSTANTS.STRING_FALSE);
    }
  }

  navigateToResearchGroup(researchGroup: ResearchGroup): void {
    this.navigate([RoutesEnum.adminResearchGroupDetail, researchGroup.resId]);
  }

  protected override _disableSpecificField(): void {
    this.form.get(this.formDefinition.orcid).disable();
  }
}

class PersonFormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() orcid: string;
  @PropertyName() syncFromOrcidProfile: string;
  @PropertyName() syncFromOrcidProfileNow: string;
  @PropertyName() syncToOrcidProfile: string;
  @PropertyName() syncToOrcidProfileNow: string;
  @PropertyName() structures: string;
  @PropertyName() notificationTypes: string;
  @PropertyName() researchGroups: string;
  @PropertyName() validationRight: string;
}

class UserFormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() homeOrganization: string;
  @PropertyName() email: string;
  @PropertyName() applicationRole: string;
}
