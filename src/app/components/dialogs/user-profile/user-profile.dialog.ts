/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - user-profile.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {UserProfileFormPresentational} from "@app/components/presentationals/user-profile-form/user-profile-form.presentational";
import {AppPersonAction} from "@app/stores/person/app-person.action";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonNotificationTypeAction} from "@app/stores/person/notification-type/app-person-notification-type.action";
import {
  AppPersonNotificationTypeState,
  defaultAppPersonNotificationTypeStateStateQueryParameters,
} from "@app/stores/person/notification-type/app-person-notification-type.state";
import {AppPersonResearchGroupAction} from "@app/stores/person/research-group/app-person-research-group.action";
import {
  AppPersonResearchGroupState,
  defaultAppPersonResearchGroupStateStateQueryParameters,
} from "@app/stores/person/research-group/app-person-research-group.state";
import {AppPersonStructureAction} from "@app/stores/person/structure/app-person-structure.action";
import {
  AppPersonStructureState,
  defaultAppPersonStructureStateStateQueryParameters,
} from "@app/stores/person/structure/app-person-structure.state";
import {AppPersonValidationRightStructureAction} from "@app/stores/person/validation-right/app-person-validation-right-structure.action";
import {
  AppPersonValidationRightStructureState,
  defaultAppPersonValidationRightStructureStateQueryParameters,
} from "@app/stores/person/validation-right/app-person-validation-right-structure.state";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {AppUserLogoAction} from "@app/stores/user/user-logo/app-user-logo.action";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {environment} from "@environments/environment";
import {
  NotificationType,
  Person,
  ResearchGroup,
  Structure,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {ValidationRight} from "@shared/models/business/validation-right.model";
import {SharedPublicContributorAction} from "@shared/stores/public-contributor/shared-public-contributor.action";
import {UserUtil} from "@shared/utils/user.util";
import {Observable} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  ModelFormControlEvent,
  ofSolidifyActionCompleted,
  QueryParameters,
  ResourceFileNameSpace,
  ResourceFileState,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "aou-user-profile-dialog",
  templateUrl: "./user-profile.dialog.html",
  styleUrls: ["./user-profile.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileDialog extends SharedAbstractDialog<User, Person> implements OnInit {
  currentPersonObs: Observable<Person> = MemoizedUtil.current(this._store, AppPersonState);
  isLoadingPersonObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppPersonState);
  selectedValidationRightObs: Observable<ValidationRight[]> = MemoizedUtil.selected(this._store, AppPersonValidationRightStructureState);
  selectedNotificationTypeObs: Observable<NotificationType[]> = MemoizedUtil.selected(this._store, AppPersonNotificationTypeState);
  selectedStructureObs: Observable<Structure[]> = MemoizedUtil.selected(this._store, AppPersonStructureState);
  selectedResearchGroupsObs: Observable<ResearchGroup[]> = MemoizedUtil.selected(this._store, AppPersonResearchGroupState);
  userAuthorizedForDepositActionsObs: Observable<boolean> = MemoizedUtil.select(this._store, AppUserState, state => state.userAuthorizedForDepositActions);
  linkToContributorPage: string;

  @Select(AppPersonState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  userLogoActionNameSpace: ResourceFileNameSpace = AppUserLogoAction;
  userLogoState: typeof AppUserLogoState = AppUserLogoState;

  validateEnable: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: UserProfileFormPresentational;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              readonly dialogRef: MatDialogRef<UserProfileDialog>,
              protected readonly _changeDetector: ChangeDetectorRef,
              @Inject(MAT_DIALOG_DATA) public readonly user: User,
  ) {
    super(dialogRef, user);
    if (isNullOrUndefined(this.user)) {
      throw Error("Missing user");
    }
    if (UserUtil.isUnigeUser(user)) {
      const cnIndividu = UserUtil.getCnIndividu(user);
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedPublicContributorAction.GetPublicationsByCnIndividu(cnIndividu, new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo)),
        SharedPublicContributorAction.GetPublicationsByCnIndividuSuccess,
        result => {
          if (result.list?._page?.totalItems > 0) {
            this.linkToContributorPage = "/" + RoutesEnum.homeContributor + "/" + cnIndividu;
            this._changeDetector.detectChanges();
          }
        },
      ));
    }
  }

  ngOnInit(): void {
    this._store.dispatch(new AppPersonStructureAction.GetAll(this.personResId, defaultAppPersonStructureStateStateQueryParameters()));
    this._store.dispatch(new AppPersonValidationRightStructureAction.GetAll(this.personResId, defaultAppPersonValidationRightStructureStateQueryParameters()));
    this._store.dispatch(new AppPersonNotificationTypeAction.GetAll(this.personResId, defaultAppPersonNotificationTypeStateStateQueryParameters()));
    this._store.dispatch(new AppPersonResearchGroupAction.GetAll(this.personResId, defaultAppPersonResearchGroupStateStateQueryParameters()));
  }

  savePersonInfo($event: ModelFormControlEvent<Person>): void {
    this.subscribe(
      this._actions$.pipe(
        ofSolidifyActionCompleted(AppPersonAction.UpdateSuccess),
        take(1),
        tap(action => {
          if (isFalse(action.result.successful)) {
            return;
          }
          this.formPresentational?.form.markAsPristine();
        }),
      ),
    );
    this._store.dispatch(new AppPersonAction.Update($event));
  }

  getUser(): User {
    if (isNullOrUndefined(this.formPresentational)) {
      return this.user;
    }
    return this.formPresentational.form.value as User;
  }

  get personResId(): string {
    return this.user.person.resId;
  }

  isAvatarPresent(): boolean {
    return isNotNullNorUndefined(this.user.person.avatar);
  }

  newAvatar: Blob | undefined | null = undefined;

  logoChange(blob: Blob): void {
    this.newAvatar = blob;
    this.validateEnable = true;
  }

  validate(): void {
    const syncFromOrcidProfile = this.formPresentational?.form?.get(this.formPresentational?.formDefinition?.syncFromOrcidProfile)?.value;
    const syncFromOrcidProfileNow = this.formPresentational?.form?.get(this.formPresentational?.formDefinition?.syncFromOrcidProfileNow)?.value;
    const syncToOrcidProfile = this.formPresentational?.form?.get(this.formPresentational?.formDefinition?.syncToOrcidProfile)?.value;
    const syncToOrcidProfileNow = this.formPresentational?.form?.get(this.formPresentational?.formDefinition?.syncToOrcidProfileNow)?.value;
    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(AppPersonAction.UpdateSuccess),
      take(1),
      tap(result => {
        const person = MemoizedUtil.currentSnapshot(this._store, AppPersonState);
        const actions = ResourceFileState.getActionUpdateFile(person.resId, this.newAvatar, person.avatar, this.userLogoActionNameSpace, this._actions$);
        this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, actions).pipe(
          tap(resultLogo => {
            if (resultLogo.success) {
              this._store.dispatch(new AppUserAction.GetCurrentUser());
              this.submit(result.action.model);
            }
          }),
        ));
        if (syncFromOrcidProfile && syncFromOrcidProfileNow) {
          this._store.dispatch(new AppPersonAction.ImportAllFromOrcid(person.resId));
        }
        if (syncToOrcidProfile && syncToOrcidProfileNow) {
          this._store.dispatch(new AppPersonAction.ExportAllToOrcid(person.resId));
        }
      }),
    ));
    this.formPresentational?.onSubmit();
  }

  navigate(path: string[]): void {
    this._store.dispatch(new Navigate(path));
    this._dialogRef.close();
  }
}
