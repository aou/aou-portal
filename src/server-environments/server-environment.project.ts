/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - server-environment.project.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {ServerEnvironment} from "./server-environment.defaults.model";

export const serverEnvironmentProject: Partial<ServerEnvironment> = {
  projectName: "aou-portal",
  activeMQResourceCacheEvictMap: {
    publication: "publicationsIndexed",
    contributor: "contributorsUpdated",
  },
  activeMQEnv: "aou",
  ssrRoutes: [
    {
      name: "home",
      path: `/`,
      urlMatcher: `^\/$|^\/${RoutesEnum.homePage}`,
      resource: "home",
      withId: false,
      cacheTimeToLiveInMinutes: 10,
    },
    {
      name: "about",
      path: `/${RoutesEnum.about}`,
      withId: false,
      cacheForbidden: true,
    },
    {
      name: "publication",
      path: `/${RoutesEnum.homeDetailUnige}${AppRoutesEnum.paramId}`,
      pathFromCacheEvict: `/${AppRoutesEnum.paramId}`,
      pathIdKey: AppRoutesEnum.paramId,
      urlMatcher: `^\/${RoutesEnum.homeDetailUnige}[^\/]+$`,
      idMatcher: `^\/(${RoutesEnum.homeDetailUnige}[0-9]+)`,
      resource: "publication",
      withId: true,
      cacheEvictionMessageCustomResourceId: "archiveId",
      cacheEvictionMessageIsDeletedAttributeKey: "deleted",
      cacheEvictionMessageCustomAttributeForMatchingRoute: "archiveId",
      cacheEvictionMessageRouteMatchingRegExp: `^${RoutesEnum.homeDetailUnige}[0-9]+$`,
    },
    {
      name: "publication-test",
      path: `/${RoutesEnum.homeDetailAouTest}${AppRoutesEnum.paramId}`,
      pathFromCacheEvict: `/${AppRoutesEnum.paramId}`,
      pathIdKey: AppRoutesEnum.paramId,
      urlMatcher: `^\/${RoutesEnum.homeDetailAouTest}[^\/]+$`,
      idMatcher: `^\/(${RoutesEnum.homeDetailAouTest}[0-9]+)`,
      resource: "publication",
      withId: true,
      cacheEvictionMessageCustomResourceId: "archiveId",
      cacheEvictionMessageIsDeletedAttributeKey: "deleted",
      cacheEvictionMessageCustomAttributeForMatchingRoute: "archiveId",
      cacheEvictionMessageRouteMatchingRegExp: `^${RoutesEnum.homeDetailAouTest}[0-9]+$`,
    },
    {
      name: "contributor",
      path: `/${RoutesEnum.contributor}/${AppRoutesEnum.paramId}`,
      pathIdKey: AppRoutesEnum.paramId,
      urlMatcher: `^\/${RoutesEnum.contributor}\/[0-9]+$`,
      resource: "contributor",
      withId: true,
      cacheEvictionMessageIsDeletedAttributeKey: "deleted",
    },
  ],
};
