/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - environment.defaults.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ErrorBackendKeyEnum} from "@app/shared/enums/error-backend-key.enum";
import {ThemeEnum} from "@app/shared/enums/theme.enum";
import {Enums} from "@enums";
import {ViewModeTableEnum} from "@home/enums/view-mode-table.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {
  defaultSolidifyApplicationEnvironment,
  isNotNullNorUndefinedNorWhiteString,
} from "solidify-frontend";
import {AouEnvironment} from "./environment.defaults.model";

export const defaultEnvironment: AouEnvironment = {
  ...defaultSolidifyApplicationEnvironment,
  authorization: "http://localhost:4300",

  // === Solidify App Environment ===
  appLanguages: [Enums.Language.LanguageEnum.en, Enums.Language.LanguageEnum.fr],
  appLanguagesTranslate: Enums.Language.LanguageEnumTranslate,
  appThemes: [ThemeEnum.aou],
  appThemesTranslate: [{key: ThemeEnum.aou, value: "Archive ouverte UNIGE"}],
  theme: ThemeEnum.aou,
  themeName: "Archive Ouverte",
  routeHomePage: RoutesEnum.homePage,
  routeSegmentEdit: AppRoutesEnum.edit,
  appTitle: "Archive ouverte UNIGE",
  appDescription: "Digital repository of the scientific heritage of the University of Geneva",
  imageOpenGraph: "themes/aou/toolbar-header-image.png",
  displayBreadcrumb: false,
  fundingSnsfName: "Swiss National Science Foundation",
  pmidWebsite: "https://pubmed.ncbi.nlm.nih.gov/",
  doiWebsite: "https://doi.org/",
  pmidWebsiteId: "https://www.ncbi.nlm.nih.gov/pubmed/",
  pmcidWebsiteId: "https://pmc.ncbi.nlm.nih.gov/articles/",
  arxivWebsiteId: "http://arxiv.org/abs/",
  snsfWebsiteId: "https://data.snf.ch/grants/grant/",
  displayExtraProfileInformation: false,
  listTagsComponentsWhereClickAllowToEnterInEditMode: ["aou-shared-structure-input", "aou-shared-table-person-structure-sub-type-container", "solidify-rich-text-editor"],
  positionLabelInputMaterial: "always",
  contactEmail: "archive-ouverte@unige.ch",
  contactPhone: "(022 37) 99 202",
  displayTour: false,
  listNotificationToHideInProfile: [
    Enums.NotificationType.NotificationTypeEnum.MY_PUBLICATION_FEEDBACK_REQUIRED,
    Enums.NotificationType.NotificationTypeEnum.MY_PUBLICATION_COMMENTED,
  ],
  aboutModulesToIgnore: ["oaiPmh"],
  swaggerModulesToIgnore: ["oaiInfo", "oaiPmh", "authorization"],
  notificationsForgottenToValidateDelaySinceLastCheck: 30,
  textareaMaxSize: 3500,
  labelEnumConverter: Enums.Language.LanguageIso639V1ToV2,
  defaultBibliographyFormat: "unige-long",
  pageSizeOptions: [10, 25, 50, 100],
  defaultPageSize: 25,

  // === Solidify Error Handler Environment ===
  httpErrorKeyToSkipInErrorHandler: [
    ErrorBackendKeyEnum.UPLOAD_DUPLICATE_DATA_FILES,
  ],

  // === Solidify Notification Environment ===
  defaultNotificationErrorDurationInSeconds: 6,
  defaultNotificationSuccessDurationInSeconds: 4,
  defaultNotificationWarningDurationInSeconds: 8,
  defaultNotificationInformationDurationInSeconds: 8,

  // === Local Environment ===
  defaultPageSizeHomePage: 5,
  defaultPageSizeSearchPage: 50,
  defaultPageSortHomePage: "technicalInfos.firstValidation",
  defaultPageSortSearchPage: "year",

  // Base URL For Module Applications (override by AppAction.LoadModules)
  admin: "http://localhost:20200/aou-administration/admin",

  // Documentation
  documentationTocIntegrationGuide: "AoU-IntegrationGuide-toc.html",
  documentationTocToolsGuide: undefined,
  documentationTocUserGuidePath: undefined,
  documentationTocUserGuide: undefined,
  documentationTocValidatorGuidePath: undefined,
  documentationTocValidatorGuide: undefined,

  fieldPathQueryParam: "fieldPath",

  // Access request
  accessRequestRoute: "accessRequest",

  // Others
  modalHeight: "400px",
  institutionUrl: "https://www.unige.ch/",

  // Home
  defaultHomeViewModeTableEnum: ViewModeTableEnum.list,

  // Tweeter
  twitterAccount: "aou_ch",
  twitterTweetToDisplay: 4,

  urlNationalArchivePronom: "https://www.nationalarchives.gov.uk/PRONOM/",

  // Polling
  refreshDepositListTabStatusCounterIntervalInSecond: 60,
  refreshTabStatusCounterIntervalInSecond: 60,
  refreshOrderAvailableIntervalInSecond: 5,
  refreshNotificationsAvailableIntervalInSecond: 60,
  refreshDepositSubmittedIntervalInSecond: 2,
  refreshDepositDataFileIntervalInSecond: 2,

  // Deposit
  depositRequestToValidatorAtLeastOnPdfFile: true,
  depositRequestToDepositorAtLeastOnPdfFileForSendToValidation: true,
  keywordCharLimitForWarning: 100,
  contributorRoleDisplayMode: "dropdown",
  numberCommentToDisplayInDepositList: 5,
  minContributorToDisplayButtonMoveUnigeMemberToCollaborators: 25,

  // Search
  researchGroupSearchListTermsToExclude: ["le", "la", "les", "l'", "de", "du", "d'", "des", "un", "une", "et", "sur", "à", "a", "entre", "dans", "par", "en", "the", "of", "and", "by", "in", "from", "to", "groupe", "group"],
  contributorsSearchPageSize: 10,

  // Share social network
  shareFacebookLink: "https://www.facebook.com/sharer.php?u={url}",
  shareTwitterLink: "https://twitter.com/share?url={url}&text={title}",
  shareLinkedinLink: "https://www.linkedin.com/sharing/share-offsite/?url={url}",

  bibliographyProxyUrl: undefined,
  corrigendumValue: "Corrigendum",

  oaiProviderCustomParam: "smartView=aou_oai2.xsl",

  orcidContributorRolesToSendToProfile: [
    Enums.Deposit.RoleContributorEnum.author,
    Enums.Deposit.RoleContributorEnum.collaborator,
    Enums.Deposit.RoleContributorEnum.editor,
    Enums.Deposit.RoleContributorEnum.guest_editor,
  ],

  avatarSizeDefault: 300,
  avatarSizeMapping: {
    [ImageDisplayModeEnum.MAIN]: 300,
    [ImageDisplayModeEnum.IN_CONTRIBUTOR]: 150,
    [ImageDisplayModeEnum.UPLOAD]: 145,
    [ImageDisplayModeEnum.IN_OVERLAY]: 125,
    [ImageDisplayModeEnum.IN_LIST]: 45,
    [ImageDisplayModeEnum.NEXT_FORM_FIELD]: 45,
  },
  imageSizeDefault: 400,
  imageSizeMapping: {
    [ImageDisplayModeEnum.MAIN]: 400,
    [ImageDisplayModeEnum.UPLOAD]: 200,
    [ImageDisplayModeEnum.IN_PUBLICATION]: 200,
    [ImageDisplayModeEnum.IN_CONTRIBUTOR]: 150,
    [ImageDisplayModeEnum.IN_LINE]: 125,
    [ImageDisplayModeEnum.IN_OVERLAY]: 125,
    [ImageDisplayModeEnum.IN_LIST]: 50,
    [ImageDisplayModeEnum.NEXT_FORM_FIELD]: 50,
  },

  allowedUrlsBuilder: (environment: AouEnvironment) => {
    const list = [];
    if (isNotNullNorUndefinedNorWhiteString(environment.documentationTocIntegrationGuide)) {
      list.push("/" + environment.documentationTocIntegrationGuide);
    }
    if (isNotNullNorUndefinedNorWhiteString(environment.documentationTocToolsGuide)) {
      list.push("/" + environment.documentationTocToolsGuide);
    }
    if (isNotNullNorUndefinedNorWhiteString(environment.documentationTocUserGuide)) {
      list.push("/" + environment.documentationTocUserGuide);
    }
    if (isNotNullNorUndefinedNorWhiteString(environment.documentationTocValidatorGuide)) {
      list.push("/" + environment.documentationTocValidatorGuide);
    }
    return list;
  },

  searchFacetValuesTranslations: [
    ...Enums.Facet.FacetValue.PrincipalFilesAccessLevelTranslate.map(v => ({
      name: Enums.Facet.Name.PRINCIPAL_FILE_ACCESS_LEVEL,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.Facet.FacetValue.OpenAccessValueTranslate.map(v => ({
      name: Enums.Facet.Name.OPEN_ACCESS,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.Facet.FacetValue.DiffusionTranslate.map(v => ({
      name: Enums.Facet.Name.DIFFUSION,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.Facet.FacetValue.FulltextVersionsTranslate.map(v => ({
      name: Enums.Facet.Name.FULLTEXT_VERSION,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.Facet.FacetValue.LanguagesTranslate.map(v => ({
      name: Enums.Facet.Name.LANGUAGE,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.Facet.FacetValue.IsBeforeUnigeTranslate.map(v => ({
      name: Enums.Facet.Name.IS_BEFORE_UNIGE,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
  ],
};
