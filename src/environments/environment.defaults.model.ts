/*-
 * %%----------------------------------------------------------------------------------------------
 * AoU Technology - AoU Portal - environment.defaults.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {ViewModeTableEnum} from "@home/enums/view-mode-table.enum";
import {DefaultSolidifyApplicationEnvironment} from "solidify-frontend";

export interface AouEnvironment extends DefaultSolidifyApplicationEnvironment {
  defaultPageSizeHomePage: number;
  defaultPageSizeSearchPage: number;
  defaultPageSortHomePage: string;
  defaultPageSortSearchPage: string;
  fundingSnsfName: string;
  pmidWebsite: string;
  doiWebsite: string;
  pmidWebsiteId: string;
  pmcidWebsiteId: string;
  arxivWebsiteId: string;
  snsfWebsiteId: string;
  displayExtraProfileInformation: boolean;
  contactEmail: string;
  contactPhone: string;
  displayTour: boolean;
  listNotificationToHideInProfile: Enums.NotificationType.NotificationTypeEnum[];
  swaggerModulesToIgnore: string[];
  displayBreadcrumb: boolean;
  notificationsForgottenToValidateDelaySinceLastCheck: number;
  textareaMaxSize: number;
  defaultBibliographyFormat: string;

  // Base URL For Module Applications (override by AppAction.LoadModules)
  admin: string;
  access?: string;

  // Documentation
  documentationTocIntegrationGuide: string | undefined;
  documentationTocToolsGuide: string | undefined;
  documentationTocUserGuidePath: undefined;
  documentationTocUserGuide: string | undefined;
  documentationTocValidatorGuidePath: undefined;
  documentationTocValidatorGuide: string | undefined;

  fieldPathQueryParam: string;

  // Access request
  accessRequestRoute: string;

  // Others
  modalHeight: string;
  institutionUrl: string;

  // Home
  defaultHomeViewModeTableEnum: ViewModeTableEnum;

  // Tweeter
  twitterAccount: string;
  twitterTweetToDisplay: number;

  urlNationalArchivePronom: string;

  // Polling
  refreshDepositListTabStatusCounterIntervalInSecond: number;
  refreshTabStatusCounterIntervalInSecond: number;
  refreshOrderAvailableIntervalInSecond: number;
  refreshNotificationsAvailableIntervalInSecond: number;
  refreshDepositSubmittedIntervalInSecond: number;
  refreshDepositDataFileIntervalInSecond: number;

  // Deposit
  depositRequestToValidatorAtLeastOnPdfFile: boolean;
  depositRequestToDepositorAtLeastOnPdfFileForSendToValidation: boolean;
  keywordCharLimitForWarning: number;
  contributorRoleDisplayMode: "radio" | "dropdown";
  numberCommentToDisplayInDepositList: number;
  minContributorToDisplayButtonMoveUnigeMemberToCollaborators: number;

  // Search
  researchGroupSearchListTermsToExclude: string[];
  contributorsSearchPageSize: number;

  // Share social network
  shareFacebookLink: string;
  shareTwitterLink: string;
  shareLinkedinLink: string;

  bibliographyProxyUrl: string;
  corrigendumValue: string;

  /**
   * If the current user has one of the roles defined in this list on a publication, they will be able to add it to their orcid profile.
   */
  orcidContributorRolesToSendToProfile: Enums.Deposit.RoleContributorEnum[];
}
