#### [aou-2.1.3](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.1.2...aou-2.1.3) (2024-11-18)

##### Bug Fixes

* **deposit:**
  *  [AOU-2037] avoid to trigger action when press enter in deposit form ([a566b160](https://gitlab.unige.ch/aou/aou-portal/commit/a566b160099beb4ac0937f65e550094beed2ccec))
  *  hide doctor email and address inputs if thesis is before 2010 or author before unige ([01098817](https://gitlab.unige.ch/aou/aou-portal/commit/01098817924e7af5de313e29bcea5e35f49985ec))
  *  [AOU-1802] avoid to create deposit with invalid year or email on thesis ([47befba5](https://gitlab.unige.ch/aou/aou-portal/commit/47befba577235d9ddbfce8fb824646892c41012b))
  *  [AOU-1929] preserve order change on collaboration members ([f4b75748](https://gitlab.unige.ch/aou/aou-portal/commit/f4b75748db7e9eeb0ba92d18b50770bb1a8ca922))
  *  [AOU-1999] hide undesired fields after batch update ([16357fab](https://gitlab.unige.ch/aou/aou-portal/commit/16357fabe30da3a5735fc55fe5705686d041da8a))
* **contributor:**
  *  [AOU-2029] display error on contributor fields ([825cde80](https://gitlab.unige.ch/aou/aou-portal/commit/825cde8055dd1d127d0af46de78209b6307c37f8))
  *  [AOU-2029] display contributor page on profile only if current user have publication ([e6d7e845](https://gitlab.unige.ch/aou/aou-portal/commit/e6d7e8454c3dd5230791ec876e9df6adcfd09383))
  *  [AOU-2021] manage case where contributor have no publications ([e38e15cc](https://gitlab.unige.ch/aou/aou-portal/commit/e38e15cc8f1487085b249ffc3b49a3d4096b4dfd))
*  [AOU-1983] change order of notifications in the profile page ([a53ad88b](https://gitlab.unige.ch/aou/aou-portal/commit/a53ad88b9503d44e554ad4a7d0227816969a095d))
* **SSR:**  replace home path by root path for root SSR page ([6829c162](https://gitlab.unige.ch/aou/aou-portal/commit/6829c162f276a5c94c0be00a80a247e4d37bf037))

##### Code Style Changes

* **deposit-list:**  customize deposit tab for validator ([8616d2dd](https://gitlab.unige.ch/aou/aou-portal/commit/8616d2dd28bf92f6fbfc824e1a23181a04662fd1))
*  fix toggle on search page ([a34b4718](https://gitlab.unige.ch/aou/aou-portal/commit/a34b47184bfe29b219d6d0fa96b61e4d2cacc4b0))

#### [aou-2.1.2](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.1.1...aou-2.1.2) (2024-10-18)

##### New Features

* **i18n:**  add translation for PUBLICATION_UPDATED_IN_BATCH event type ([eb4ff665](https://gitlab.unige.ch/aou/aou-portal/commit/eb4ff6657ad9aac3777098f96337893556813b6b))

##### Bug Fixes

* **orcid:**  allow hug user to use orcid features ([ec174085](https://gitlab.unige.ch/aou/aou-portal/commit/ec174085e6a7212c5fa9733dff532d872618b5d5))
*  avoid to display global banner on SSR ([d66b0099](https://gitlab.unige.ch/aou/aou-portal/commit/d66b00999421d54f57dbd060943301ed1e5d057c))

#### [aou-2.1.1](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.1.0...aou-2.1.1) (2024-10-01)

##### Bug Fixes

*  change enum year on bibliography ([d6431391](https://gitlab.unige.ch/aou/aou-portal/commit/d6431391a40684f77e92b058de2436735efb7f41))

#### [aou-2.1.0](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.7...aou-2.1.0) (2024-09-30)

##### New Features

*  [AOU-1981] avoid to edit or delete system comment and display system comment ([63a956da](https://gitlab.unige.ch/aou/aou-portal/commit/63a956daa957036467c0b99615b28ababd2e1deb))
*  [AOU-1125] improve error message when reaching max number of contributors during import ([cc37063e](https://gitlab.unige.ch/aou/aou-portal/commit/cc37063e558ae9a5a6ce9b6c89aa58f5184186fb))
*  publication validation ([4281fe55](https://gitlab.unige.ch/aou/aou-portal/commit/4281fe5501cbc4980d92ce8e218abd8b047647d2))
* [AOU-1619] accept shortDOI as DOI ([9039c7cd](https://gitlab.unige.ch/aou/aou-portal/commit/9039c7cdfe55c69cab33fd4db7e4b643adeae795))
*  [AOU-1886] add bibtex format to publication export ([58dc6460](https://gitlab.unige.ch/aou/aou-portal/commit/58dc6460a4a96e50c76a7392d29eff41377c4145))
*  [AOU-1923] align advanced search field to the top + add explanation for search with wildcard characters ([e1c50c0d](https://gitlab.unige.ch/aou/aou-portal/commit/e1c50c0de632b872c37c4fc2abe667b384040061))
*  use short form for Editor label on deposit public page ([b771050c](https://gitlab.unige.ch/aou/aou-portal/commit/b771050c1f50f4b0c96e73ac200f5218605f118b))
*  [AOU-1674] move 'Show all' button to the left of contributors ([3db5746e](https://gitlab.unige.ch/aou/aou-portal/commit/3db5746e1fa940d5fc70a53436c6ddbafdfb8232))
*  [AOU-1674] move 'Show all' button to the left ([2b878548](https://gitlab.unige.ch/aou/aou-portal/commit/2b8785481e19cf0039ff617e56b9fe5bbbc3d0ef))
*  [AOU-1674] truncate urls on public deposit page ([53d13b02](https://gitlab.unige.ch/aou/aou-portal/commit/53d13b02c00d140531abb1cfdbf61e4d9c90f447))
*  add translation for miscellaneous language ([9854ea93](https://gitlab.unige.ch/aou/aou-portal/commit/9854ea930e4186aff5670815710c29c7965d43f9))
*  set defaultPageSizeSearchPage to 50 ([6df23b7d](https://gitlab.unige.ch/aou/aou-portal/commit/6df23b7d7b785b58465ab0ddde6c85700a0c27ed))
* [AOU-1897] Rename csv to tsv for publications export ([40644d13](https://gitlab.unige.ch/aou/aou-portal/commit/40644d13e77b11ddf1cec989a90139cef3850fb2))
*  display link on structure and research group in contributor page and overlay ([fd4d6a37](https://gitlab.unige.ch/aou/aou-portal/commit/fd4d6a37dae139e630d87169b001a3a0e613fd4c))
*  display send to ORCID profile button and checkboxes only for some contributor roles and UNIGE members ([de5d4cf5](https://gitlab.unige.ch/aou/aou-portal/commit/de5d4cf550c81663709cb5e10be34576df60f02f))
*  [AOU-1827] add checkbox in user profile dialog to enable synchronization from ORCID profile ([3f856842](https://gitlab.unige.ch/aou/aou-portal/commit/3f8568428609008ec7ac456b9dd607a86fbe80dd))
*  [AOU-1825]  mechanism for detecting potential duplicates in publications ([9944b3bd](https://gitlab.unige.ch/aou/aou-portal/commit/9944b3bda16c7fa22cdc3b724cac1c49c9ffbf55))
*  update to solidify 4.8.39 to improve SSR performance ([02d786f5](https://gitlab.unige.ch/aou/aou-portal/commit/02d786f5cc271dbf9c4480cc1824cbe0ebbb8dd5))
* **home:**
  *  [AOU-1918] scroll to top when facet change ([e534d29d](https://gitlab.unige.ch/aou/aou-portal/commit/e534d29dba80143d3c08539cc116a3b459004432))
  *  [AOU-1939] do not show send to orcid for publication older than 1900 ([38c3086d](https://gitlab.unige.ch/aou/aou-portal/commit/38c3086d93a6b58318657568c17e69193c731d2b))
  *  change order in published in for book chapter, proceedings chapter and contributor in dictionary ([ce39d83c](https://gitlab.unige.ch/aou/aou-portal/commit/ce39d83c5272fc60330fecbacfd47f047ab73199))
  *  use angular number pipe ([5ac85d87](https://gitlab.unige.ch/aou/aou-portal/commit/5ac85d87e17b6eb6b3a7dfe27a2d9020da492833))
  *  [AOU-1869] add new button to export bibliography ([8ab90ff7](https://gitlab.unige.ch/aou/aou-portal/commit/8ab90ff71fcf8829abf5bb1573327021038bd08c))
  *  [AOU-1631] share search should keep restricted access master ([10c75dcc](https://gitlab.unige.ch/aou/aou-portal/commit/10c75dcc1543881e9c3ed67f620be1140dc63923))
* **contributor:**
  *  [AOU-1954] add contact contributor button to contributor dashboard ([842776e9](https://gitlab.unige.ch/aou/aou-portal/commit/842776e9ff0a7ff87cce03292c9813b1451b0d22))
  *  [AOU-1912] add container title in publications list on dashboard ([4f998d22](https://gitlab.unige.ch/aou/aou-portal/commit/4f998d2263dc72bceafbc3892a0da72cae299081))
  *  [AOU-1888] add shortcut link for search contributor's publications ([551b8ce1](https://gitlab.unige.ch/aou/aou-portal/commit/551b8ce1caad832f1bb0ec530f7b39b8bfaf5af4))
  *  [AOU-1886] add new graphic and column ([b4ef4db2](https://gitlab.unige.ch/aou/aou-portal/commit/b4ef4db28b846d246dc53710c784c91d0e97268a))
  *  [AOU-1837] add direct link to bibliography ([0717f8d2](https://gitlab.unige.ch/aou/aou-portal/commit/0717f8d2a41ccac4eb568c24c9e5220414f98190))
  *  display contributor link and overlay on publication list ([5e46e075](https://gitlab.unige.ch/aou/aou-portal/commit/5e46e075ffa499d02e8fc426a2c948d95d561a21))
  *  display avatar on contributor page and overlay ([d1d56b3a](https://gitlab.unige.ch/aou/aou-portal/commit/d1d56b3a93411db71747bb10ed7d7cf783132fb1))
  *  add meta and support SSR for contributor page ([da8829cf](https://gitlab.unige.ch/aou/aou-portal/commit/da8829cf74a772efe5c522766f3191a9920bbd7a))
  *  [AOU-1835] add pie chart and bar chart for diffusion ([d192c942](https://gitlab.unige.ch/aou/aou-portal/commit/d192c942849c59876fb77bfdcdaf9fef5ceb8873))
  *  [AOU-1835-1871] create contributor page ([99a2357a](https://gitlab.unige.ch/aou/aou-portal/commit/99a2357ae2502e13bcb8d83222f30e7e46751295))
* **deposit:**
  *  [AOU-1942] do not allow to enter embargo more than 1200 months ([556a2cab](https://gitlab.unige.ch/aou/aou-portal/commit/556a2cabc4fea0a2f39dbdbaba0cdc7c002c28d5))
  *  [AOU-1617] display duplicate contributor ([7e133d7f](https://gitlab.unige.ch/aou/aou-portal/commit/7e133d7f5d7f61f8633940bfff5bf96fb77b26e7))
  *  [AOU-1903] allow drag and drop on repeatable fields ([825fb387](https://gitlab.unige.ch/aou/aou-portal/commit/825fb387374d87068a5fc9455f4e1d791bbf4918))
  *  [AOU-1924] allow to export in tsv format deposits ([c9437b99](https://gitlab.unige.ch/aou/aou-portal/commit/c9437b994d6deb417b58bd62e6b6483eaf142ab1))
  *  [AOU-1842] raise an alert when two fundings have the same identifier ([0af2d49c](https://gitlab.unige.ch/aou/aou-portal/commit/0af2d49cce1129a23adcaac2f7eccf625e2c6762))
  *  [AOU-1843] remove doi prefix when copy paste on multi import ([d768744d](https://gitlab.unige.ch/aou/aou-portal/commit/d768744de34f45516386158d402841773578676c))
  *  [AOU-1408] manage error when cannot import file automatically ([685a02db](https://gitlab.unige.ch/aou/aou-portal/commit/685a02db2565de1df877e291bdfd7cead1c75aac))
  *  [AOU-1461] make funding fields editable ([6b3901c3](https://gitlab.unige.ch/aou/aou-portal/commit/6b3901c3014a34a863fb0a88e2d25b567fe31e93))
  *  [AOU-1822] support isbn on import input and control format ([9b3d89cb](https://gitlab.unige.ch/aou/aou-portal/commit/9b3d89cb474d62917e650d235b345a3517bc2eba))
  *  [AOU-1794] add button to save and submit in re-validation ([46a33258](https://gitlab.unige.ch/aou/aou-portal/commit/46a33258408962d79a8e151637835a186342f1a5))
  *  [AOU-1812] hide page input on presentation speech ([b9672d93](https://gitlab.unige.ch/aou/aou-portal/commit/b9672d930d36809e1c0ffe28b8bb3def10a7891b))
  *  [AOU-1811] hide tooltip when only one date ([8a533e3a](https://gitlab.unige.ch/aou/aou-portal/commit/8a533e3a111baaa5927d66f937a50cd40b380656))
  *  [AOU-1811] hide publication date on conference and poster type ([9f3bef7a](https://gitlab.unige.ch/aou/aou-portal/commit/9f3bef7ac7a61473d155e2d4ca4f3824f44d553c))
  *  [AOU-1848] hide doctor email and address on re-edition ([4e0bc0f4](https://gitlab.unige.ch/aou/aou-portal/commit/4e0bc0f4a49930df8050b59cbbbe26a6bd67ca3c))
* **facet:**  [AOU-1775] add description on IndexFieldAlias admin form ([cd1607a6](https://gitlab.unige.ch/aou/aou-portal/commit/cd1607a6efac563f4cd33fdcb9ba5b3ef1254cc3))
* **special info:**  display presented at before publish in for chapter of proceedings ([4660cd5f](https://gitlab.unige.ch/aou/aou-portal/commit/4660cd5f040a1bcc3b4daecc769a4d3047ff21de))
* **contributor-page:**  add statistics ([f5d9d542](https://gitlab.unige.ch/aou/aou-portal/commit/f5d9d542a60108204aabca391a5e9f24bf4217ce))
* **search:**
  *  [AOU-1861] trim advanced search values before searching ([33f63ecd](https://gitlab.unige.ch/aou/aou-portal/commit/33f63ecd3b9937cd188e60dd0eced1bedf293e1a))
  *  [AOU-1646] support the new WILDCARD criteria type ([02e0f871](https://gitlab.unige.ch/aou/aou-portal/commit/02e0f871960e113063b89f231808114480174213))
* **orcid:**
  *  [AOU-1914] manage ORCID profile association refusal ([c100ecbb](https://gitlab.unige.ch/aou/aou-portal/commit/c100ecbb6d1d12a74bb8c1456a35d7b3844d70af))
  *  add parameter for ORCID scope ([41edd1af](https://gitlab.unige.ch/aou/aou-portal/commit/41edd1afd75367de1f9b276b64969b19312a4690))
* **i18n:**  [AOU-1818] add translations for isBeforeUnige and languages facet values ([086eb8c8](https://gitlab.unige.ch/aou/aou-portal/commit/086eb8c8ba78fd8541407a30c5b96e626d09ab39))
* **home-publication:**  [AOU-1694] display UNIGE member, overlay on contributor and ORCID in advanced search ([dcd625dc](https://gitlab.unige.ch/aou/aou-portal/commit/dcd625dcbabdb5a25c366cdd51cdfa7f196024aa))
* **ssr:**  manage cache for contributor page ([eb9bd731](https://gitlab.unige.ch/aou/aou-portal/commit/eb9bd731a8a831a80941fc4dafb4c12846d7d57b))
* **bibliographies:**  [AOU-1785] add german translations of citation ([d132d0c8](https://gitlab.unige.ch/aou/aou-portal/commit/d132d0c8e7a2588fafdc4bd7fbd9025258c0f69e))
* **bibliography:**
  *  [AOU-1634] add new sort value by deposit date to generate bibliography ([89de9641](https://gitlab.unige.ch/aou/aou-portal/commit/89de96415d1f19b4b25acc949adcecaafaac9f1f))
  *  [AOU-1864] bibliography JS can be built by using a contributor cnIndividu from the URL ([55661701](https://gitlab.unige.ch/aou/aou-portal/commit/55661701bbd63a7f13d4dbbb93ac7e5e554c7017))
* **profile:**  [AOU-1876] manage orcid synchronisation ([0966d7fd](https://gitlab.unige.ch/aou/aou-portal/commit/0966d7fd715d0f767d80f42b67053bac00c4468a))
* **ORCID:**
  *  [AOU-1877] always display 'Send to ORCID' button if the current user is contributor ([30cede05](https://gitlab.unige.ch/aou/aou-portal/commit/30cede05ed15f9b9f103582919e84d09b479aa8e))
  *  use ORCID config from backend system-properties ([47fb6a6e](https://gitlab.unige.ch/aou/aou-portal/commit/47fb6a6eec638cc6c44e53a330a295324cbe9383))
  *  [AOU-1829] new button to export a publication to ORCID profile ([a8de4791](https://gitlab.unige.ch/aou/aou-portal/commit/a8de47918130f87fd16215bea6b2a7e8e8b570e3))
* **home-detail:**  display contributor overlay ([558bedd9](https://gitlab.unige.ch/aou/aou-portal/commit/558bedd9686918d1e574a7c5b8865cd0afa47768))
* **deposit to validate:**  [AOU-1409] allow to clean abstract ([f9fb5c27](https://gitlab.unige.ch/aou/aou-portal/commit/f9fb5c2793dbd62a186f45b48bb06decd3fac30d))
* **deposit form:**  [AOU-1404] auto trim values on some deposit form fields ([270a8903](https://gitlab.unige.ch/aou/aou-portal/commit/270a89031c951fec520ec427bd14cdea0a4b4a2a))

##### Bug Fixes

*  export publications in tsv only visible in deposits to validate tab ([d4f96789](https://gitlab.unige.ch/aou/aou-portal/commit/d4f9678947ac6d5d90d7550014e8ba2d16f22439))
*  [AOU-1952] improve error message when a validator cannot access a deposit in validation ([1d2f6039](https://gitlab.unige.ch/aou/aou-portal/commit/1d2f603965eb5f13b2af5701ee632da3cb318be8))
*  allow to edit completed deposit ([f9f6e4bb](https://gitlab.unige.ch/aou/aou-portal/commit/f9f6e4bb656bc1df280e57ba5f3b988d3d99554a))
*  display contact contributor link only if email available ([b77c0598](https://gitlab.unige.ch/aou/aou-portal/commit/b77c0598f279b9c4054ab2ee272e7689c55677da))
*  [AoU-1938] change to POST action to download more deposits in format tsv ([907f1a28](https://gitlab.unige.ch/aou/aou-portal/commit/907f1a2811c58ee0d82622f4e726bc500a772779))
*  [AOU-1969] avoid error state undefined after logout ([aac453e3](https://gitlab.unige.ch/aou/aou-portal/commit/aac453e3bbbed9f65a6717d80cc1cd2485ff4247))
*  add missing labels ([2a3f6cce](https://gitlab.unige.ch/aou/aou-portal/commit/2a3f6ccefc9a266a7dce5af3080fafd9d919568d))
*  do not truncate all values on public deposit page ([ecdc4369](https://gitlab.unige.ch/aou/aou-portal/commit/ecdc4369c0346285a92a08130ee289fd5368c517))
*  complete OpenAPI file ([d2e6fbda](https://gitlab.unige.ch/aou/aou-portal/commit/d2e6fbda2f10cabadd87b602eba39f8a1f19164c))
*  lint ([80574e1f](https://gitlab.unige.ch/aou/aou-portal/commit/80574e1f2f7af423adf2c2678013e6cff9dbd4f5))
*  open expandable section in case of error in form ([f3372d8e](https://gitlab.unige.ch/aou/aou-portal/commit/f3372d8efd1b7134877bc7a64133e6e27384dc85))
*  avoid ssr error on LOCALE_ID ([dd510ed5](https://gitlab.unige.ch/aou/aou-portal/commit/dd510ed53a9af5e6e67b8221aa32b21d9ab90579))
*  export to bibliography ([12610182](https://gitlab.unige.ch/aou/aou-portal/commit/12610182cab164f289dbaf454617d98056ae53fd))
*  lint ([ecd8ddb3](https://gitlab.unige.ch/aou/aou-portal/commit/ecd8ddb39bccdacd7b169a057846e0bd41808a4e))
*  use swagger instead of api tocs ([6227989f](https://gitlab.unige.ch/aou/aou-portal/commit/6227989fb58d44551e2bdfa190383fd3efe5ef58))
*  show error under syncFromOrcidProfile checkbox in profile dialog ([426ecd05](https://gitlab.unige.ch/aou/aou-portal/commit/426ecd05ac80528e8f2998a11cc58ad5220cf77c))
*  footer buttons ([02d3a081](https://gitlab.unige.ch/aou/aou-portal/commit/02d3a081477c69a2efb6a1e17063d40db9877a4b))
*  move user guide link on smartphone screen ([431b6335](https://gitlab.unige.ch/aou/aou-portal/commit/431b6335b8f153089dbfe192e118b15c28839bea))
*  color of potential duplicate warn ([59cab0b7](https://gitlab.unige.ch/aou/aou-portal/commit/59cab0b7baf8306cbbb557ec8b5a1f5092c08acb))
*  check publication duplicate to allow internal redirection ([171ef75f](https://gitlab.unige.ch/aou/aou-portal/commit/171ef75f23602b5256123998b50609a253a6fc3d))
*  [AOU-1845] style of home page and main toolbar ([e031c2fb](https://gitlab.unige.ch/aou/aou-portal/commit/e031c2fb680c638539471c99fd4da5b02a2fcec7))
* **index-field-alias:**  remove description when "Has a description" is not toggled ([77d3a58e](https://gitlab.unige.ch/aou/aou-portal/commit/77d3a58e4c5fc88fa24cf3c12936af657b64e050))
* **deposit:**
  *  clean metadata differences when validate or reject update ([2c29f151](https://gitlab.unige.ch/aou/aou-portal/commit/2c29f15174aed92fcf9c53ccd364030f581ef8bd))
  *  [AOU-1961] allow to refresh list from link in header ([25f55eb0](https://gitlab.unige.ch/aou/aou-portal/commit/25f55eb079ec0d43387cd427892472cfde67a331))
  *  [AOU-1946] fix auto import dialog to avoid prompt when should not ([98003838](https://gitlab.unige.ch/aou/aou-portal/commit/9800383880e5b4c145a92efc70c3849c31a7b3ad))
  *  [AOU-1944] clean comment when cloning deposit ([5cef11e7](https://gitlab.unige.ch/aou/aou-portal/commit/5cef11e7115112608a85ac89219c006a97f844c1))
  *  [AOU-1951-1956] remove mask button for rejected deposit and allow always depositor to send for validation a deposit in edition ([e5a52751](https://gitlab.unige.ch/aou/aou-portal/commit/e5a5275161740ce2d1654e38e89771c73b87d499))
  *  [AOU-1933] avoid two edit button ([5aefa4ce](https://gitlab.unige.ch/aou/aou-portal/commit/5aefa4ce04db2af0be3acb77332068e6d76204e6))
  *  [AOU-1949] clear filter when navigation between tabs ([51c52a1a](https://gitlab.unige.ch/aou/aou-portal/commit/51c52a1ae7bd2a812c874d75aa5d97021a8e9d4a))
  *  [AOU-1958] avoid content overflow on deposit list ([cd55f964](https://gitlab.unige.ch/aou/aou-portal/commit/cd55f964d977e937f4aafd216e0e8162960373b5))
  *  [AOU-1948] break word on comment with long line ([1ebe33ad](https://gitlab.unige.ch/aou/aou-portal/commit/1ebe33adf7fb41082be536d7d7d2721b79c10e7e))
  *  [AOU-1968] move add button to the end of the line on repeatable fields ([8449af90](https://gitlab.unige.ch/aou/aou-portal/commit/8449af90b40418d1296243ddc456c3e4fd0271a7))
  *  [AOU-1959] avoid drag problem when select text on repeatable field ([c721b3ed](https://gitlab.unige.ch/aou/aou-portal/commit/c721b3ed9a1bac3f7c683110d0ddfebf849c25a0))
  *  avoid error when close transfer dialog ([c8d3d613](https://gitlab.unige.ch/aou/aou-portal/commit/c8d3d613c1facc4d2367772d9e545b72bfd297d9))
  *  fix doi regex to avoid wrong doi identification during import process ([5049136e](https://gitlab.unige.ch/aou/aou-portal/commit/5049136e7d1f47a9757c152a4adcf0757c3fd6cc))
  *  [AOU-1617] manage case when firstname is missing on duplicate contributor ([04b0d8af](https://gitlab.unige.ch/aou/aou-portal/commit/04b0d8afcd84bcab8012d9d4122138dc739f2cb8))
  *  [AOU-1931] avoid focus problem on repeatable abstract ([c629f7c5](https://gitlab.unige.ch/aou/aou-portal/commit/c629f7c5f2e1dd07654aec340300d660c5857012))
  *  improve display of notification when multiple files are imported automatically ([2daeedfa](https://gitlab.unige.ch/aou/aou-portal/commit/2daeedfa6910b2160484683e35a6c921c0775102))
  *  [AOU-1895] disable save and submit button when form is invalid ([dc77a3e1](https://gitlab.unige.ch/aou/aou-portal/commit/dc77a3e1255989d388f72b917223c6bdc13336db))
  *  avoid error on publication creation due to new save and submit button ([7b12235d](https://gitlab.unige.ch/aou/aou-portal/commit/7b12235da9f31a9410d8fa73a36ea69a92766eb0))
  *  [AOU-1882] avoid collaboration displayed in wrong section on old deposit ([bef84d1c](https://gitlab.unige.ch/aou/aou-portal/commit/bef84d1cd9d1740bb70a582f36da686090241ea3))
  *  avoid error when create a deposit of type thesis ([ee09bf41](https://gitlab.unige.ch/aou/aou-portal/commit/ee09bf4127a20f65a890af3d692c91322e07f6aa))
  *  date section label ([5a1199ba](https://gitlab.unige.ch/aou/aou-portal/commit/5a1199ba120a5e025d4424af78d26aac224b44f5))
  *  [AOU-1847] display date error on correct field ([8addb4c2](https://gitlab.unige.ch/aou/aou-portal/commit/8addb4c2d3e9ae0f08a10a88c10d0f74bbfb2149))
  *  [AOU-1821] avoid to display file automatically imported when should not ([7ab2f17d](https://gitlab.unige.ch/aou/aou-portal/commit/7ab2f17dd7d140db3da66398ba9005a25a6edaa2))
  *  [AOU-1823] fix deposit multiple import to manage import in error ([50e1b61f](https://gitlab.unige.ch/aou/aou-portal/commit/50e1b61fb8d985b2cb7123b19114f509e0705849))
  *  refresh file list after delete ([e586233f](https://gitlab.unige.ch/aou/aou-portal/commit/e586233f62b6602660b1d11235c3ba82da2f5d7b))
  *  avoid to display toast error on validation metadata endpoint ([f28586ed](https://gitlab.unige.ch/aou/aou-portal/commit/f28586ed3eac70ed5167174a3cca45028709b642))
  *  [AOU-1421] submit button is not enabled when datafiles are being treated ([bd52e867](https://gitlab.unige.ch/aou/aou-portal/commit/bd52e8679e112602651de5b2d07f1eb74e7298b2))
  *  [AOU-1424] hide bulk action checkbox when no options available ([6678d138](https://gitlab.unige.ch/aou/aou-portal/commit/6678d13837533774123ca8f7348dac828613e197))
  *  [AOU-1806] select by default last version for CC license ([20405dfa](https://gitlab.unige.ch/aou/aou-portal/commit/20405dfaa7d9173c5c19d6c087b4ff7fd335e159))
  *  [AOU-1625] open comment zone when click on comment button ([ac17e00a](https://gitlab.unige.ch/aou/aou-portal/commit/ac17e00a5bcb8a05f429c8fe0f78063cf88321af))
* **i18n:**
  *  revert wrong text updates ([5afa92e9](https://gitlab.unige.ch/aou/aou-portal/commit/5afa92e9a283bd3147bc158b65b063d1e0cf7988))
  *  modify fr label text to reflect backend update ([764fa27c](https://gitlab.unige.ch/aou/aou-portal/commit/764fa27c90b989c3a679676a858593b7164a6d16))
* **meta:**  clean html tag in title when used in meta title ([85e972e9](https://gitlab.unige.ch/aou/aou-portal/commit/85e972e97be5b52525f282d7e278a18741c5c5a0))
* **home:**
  *  use existing year instead of compute value for display orcid sync button ([ba81a8fb](https://gitlab.unige.ch/aou/aou-portal/commit/ba81a8fbdfea087f697ec5d515c45bea836c8d25))
  *  home routing module blocking ([528154d6](https://gitlab.unige.ch/aou/aou-portal/commit/528154d6e6bc466fba502d73e12f942794bffc94))
  *  [AOU-1846] home guard to not block navigation ([15328fb8](https://gitlab.unige.ch/aou/aou-portal/commit/15328fb8ae791bc18cf66c5d0ca5638cd5779910))
  *  [AOU-1701] wait list of search criteria before display home module ([839ad2e0](https://gitlab.unige.ch/aou/aou-portal/commit/839ad2e0e3e0af1f327d3606f3f1f48f0e109585))
* **contributor:**
  *  contributor error when not supervised work ([36de94df](https://gitlab.unige.ch/aou/aou-portal/commit/36de94df7cfc9951dc205aed61b2d101ce0a2299))
  *  [AOU-1936] improve contributor page to display supervised works ([510e2f27](https://gitlab.unige.ch/aou/aou-portal/commit/510e2f27d9080eca6fd688afe47631a385841d6d))
  *  display missing legend in third chart ([bab4bb03](https://gitlab.unige.ch/aou/aou-portal/commit/bab4bb03debd096708b26a0b6640d10c6db07eee))
  *  default pagination ([9a3ced25](https://gitlab.unige.ch/aou/aou-portal/commit/9a3ced25879e5fd1447d356ea97000a977142b9c))
  *  default sort by publication year ([cdc565f7](https://gitlab.unige.ch/aou/aou-portal/commit/cdc565f7d3f229bbf8728092d32e3dab335312e4))
  *  regenerate charts when change language and use contributor meta ([b2ac47c1](https://gitlab.unige.ch/aou/aou-portal/commit/b2ac47c1229f31daab1d46465aa77ae507afedf3))
  *  use diffusion in publication page ([2e6b6669](https://gitlab.unige.ch/aou/aou-portal/commit/2e6b6669518dd8072cdc2f423694f6f6b81c38b1))
  *  allow to switch between contributor ([d4d37b28](https://gitlab.unige.ch/aou/aou-portal/commit/d4d37b28f4ca031f82c8a6aaed9fc0a75e9e902c))
  *  use override GetById class as parent action to fix typing error ([c04d07bd](https://gitlab.unige.ch/aou/aou-portal/commit/c04d07bd70d94a63621ff48b783a0fb698c01f98))
* **share:**  [AOU-1932] alignment of icon on share menu ([0f27a9dd](https://gitlab.unige.ch/aou/aou-portal/commit/0f27a9ddbc7ab132c920562d27d154dce115781e))
* **orcid:**  manage case when redirect with error ([150c790c](https://gitlab.unige.ch/aou/aou-portal/commit/150c790c8751f84e2785bb8aaf257a9104893160))
* **user profile:**  rollback button to make a new orcid authentication ([a3bf1a04](https://gitlab.unige.ch/aou/aou-portal/commit/a3bf1a044223616bb3357be83fbac7362cbb451d))
* **deposit page:**  fix max height of truncated zones calculation ([28c0b534](https://gitlab.unige.ch/aou/aou-portal/commit/28c0b53406faa839045e16f4973ff1d08fdf0419))
* **AoU-APIs.json:**  remove repeated field ([5c45fa30](https://gitlab.unige.ch/aou/aou-portal/commit/5c45fa3083b7eb1758f1826184514026f5456826))
* **home search:**  allow to refresh advanced search form when change come outside form ([201a1b5c](https://gitlab.unige.ch/aou/aou-portal/commit/201a1b5c994832eb18920a9ae9a0cd8ad73ec596))
* **user-profile:**  [AOU-1892] avoid to sync when user change of ming ([8569b58f](https://gitlab.unige.ch/aou/aou-portal/commit/8569b58f931ce3e8df43ca2173222da9960608b7))
* **ssr:**
  *  remove typo publication path ([ee50a578](https://gitlab.unige.ch/aou/aou-portal/commit/ee50a578646c3642b387ed0420ac2ab1e1851c62))
  *  avoid error "Cannot access 'AppUserState' before initialization" ([53e43038](https://gitlab.unige.ch/aou/aou-portal/commit/53e43038a89e35ee4b2f28ea01d97aabb53be1af))
  *  allow to update dev conf easily for SSR ([65ee7a8e](https://gitlab.unige.ch/aou/aou-portal/commit/65ee7a8e673d36152aa4bba5d4dde40c1bbe7d70))
* **notification:**  [AOU-1449] remove sorting option in notification list ([2ff059aa](https://gitlab.unige.ch/aou/aou-portal/commit/2ff059aa06d5d76e8467030e5f8a28c86a7e923e))
* **contributor overlay:**  display contributor link only if contributor exist ([84cef6d6](https://gitlab.unige.ch/aou/aou-portal/commit/84cef6d695d686b6af5842f83f39ebd513c14f67))
* **admin structure:**  fix display of mat tree chip buttons ([b685e94e](https://gitlab.unige.ch/aou/aou-portal/commit/b685e94e922d13c6b58515f7c27796817c6a9c91))
* **profile:**
  *  use CSS 'white-space' property instead of 'text-wrap' for better browser compatibility ([cc090236](https://gitlab.unige.ch/aou/aou-portal/commit/cc090236c91f86417607a6402572e4ee20d87cb2))
  *  [AOU-1639] wait validate button to save see master non public preferences ([df8a0909](https://gitlab.unige.ch/aou/aou-portal/commit/df8a0909ed489655bc9be12a8cd4fb99257d348b))
* **about:**  add dashboard icon ([4d766f2f](https://gitlab.unige.ch/aou/aou-portal/commit/4d766f2f4c088eaf2c1a9e26d7c84873b4ba5a35))
* **deposit to validate:**  [AOU-1683] hide in edition tab for validators ([47101b8b](https://gitlab.unige.ch/aou/aou-portal/commit/47101b8b41398831276eed779696a423fcf60593))
* **home publication:**
  *  [AOU-1766] pagination for navigation button ([3054905f](https://gitlab.unige.ch/aou/aou-portal/commit/3054905ff0f5bfff6ed1893964da7a2e68f51425))
  *  [AOU-1810] allow to select ISO text to allow copy ([09fdc6c3](https://gitlab.unige.ch/aou/aou-portal/commit/09fdc6c33fd6fe006c9bd4aa45ab4d05eb1020a4))
* **publication:**  fix archive id in url ([fe44b745](https://gitlab.unige.ch/aou/aou-portal/commit/fe44b745ae93061e17115457801294627f875ce2))
* **validation errors summary:**  [AOU-1778] disable links to enter in edition if user cannot edit the deposit ([e6854ddb](https://gitlab.unige.ch/aou/aou-portal/commit/e6854ddb4168648246a94485fd1519942b68da33))
* **StatusEnumTranslate:**  [AOU-1712] do not hide 'DELETED' status in lists ([2f09feb9](https://gitlab.unige.ch/aou/aou-portal/commit/2f09feb989536027f07f9238e61d7f2cdfb9c439))
* **DepositFormPresentational:**  [AOU-1792] do not clean doctor infos when not required ([609464d5](https://gitlab.unige.ch/aou/aou-portal/commit/609464d5080febc4861391182b46ec0a2258fb14))

##### Other Changes

*  reorder action buttons in different steps ([0332feff](https://gitlab.unige.ch/aou/aou-portal/commit/0332feff73bfe859c6c0bdde2818c65e05369b82))
*  list of publications by different filters ([64c4a8f9](https://gitlab.unige.ch/aou/aou-portal/commit/64c4a8f9d62f52af0cdb12499484dd4d1803d4fb))

##### Refactors

* **contributor:**
  *  improve contributor page ([6af12340](https://gitlab.unige.ch/aou/aou-portal/commit/6af12340265f60238e6e7c8d312c36dc3350ebfd))
  *  request needed facet to generate charts ([ee4a14c9](https://gitlab.unige.ch/aou/aou-portal/commit/ee4a14c9e2eb431f693c21af721b8484ff39edce))
*  [AOU-1813] move paging info into new line for journal issue ([7a812ace](https://gitlab.unige.ch/aou/aou-portal/commit/7a812ace0af630993b685ef13e5ff1c4d174504f))
*  deposit ([12df8a1d](https://gitlab.unige.ch/aou/aou-portal/commit/12df8a1d54dcffd469579dc26ec46daa030c29f2))
*  contributor info ([88ff02c6](https://gitlab.unige.ch/aou/aou-portal/commit/88ff02c649a8f3ac919734963282979e24cc9642))
* **home:**  change way to provide truncate class ([a5154291](https://gitlab.unige.ch/aou/aou-portal/commit/a51542914e99623a473b6ba4262d3babc8380205))

##### Code Style Changes

* **dialog:**  update to solidify 5.1.21 and add maxWidth condition to dialog with only minWidth condition ([7349b507](https://gitlab.unige.ch/aou/aou-portal/commit/7349b5070304cb4d7b99cbfb319967d0dae9ccf5))
*  display send button of "something wrong on this page" dialog as main action ([e1e01e18](https://gitlab.unige.ch/aou/aou-portal/commit/e1e01e1841ac1bd373d22bce590b2417807d13ec))
*  dark mode of structure tree ([686067bf](https://gitlab.unige.ch/aou/aou-portal/commit/686067bf818623abea6e865d3e8afd8ded6aa18b))
*  limit max size of dialog ([0568a4a7](https://gitlab.unige.ch/aou/aou-portal/commit/0568a4a710cdd567e470dbe7d06a71ca7b71fbc3))
*  update access level icons ([1cf62993](https://gitlab.unige.ch/aou/aou-portal/commit/1cf6299330eeb338a1b2c440f964d32c3055e862))
*  improve contributor page ([2535fe66](https://gitlab.unige.ch/aou/aou-portal/commit/2535fe66a27499f2c0d883f306cf0762cd062e3e))
*  reduce spaces between radio buttons ([141035fd](https://gitlab.unige.ch/aou/aou-portal/commit/141035fd022efd9c61350d421242b6826c3e9cc1))
*  fix style after angular migration ([545549e1](https://gitlab.unige.ch/aou/aou-portal/commit/545549e106fbe3cd8c01cca1b580eaab6f10f20b))
*  fix form in dark mode ([eda6f506](https://gitlab.unige.ch/aou/aou-portal/commit/eda6f50674c496e067e372bca4cefd9ecd4f2146))
* **deposit:**
  *  fix dark mode on comment form validator ([9c0d37a4](https://gitlab.unige.ch/aou/aou-portal/commit/9c0d37a41452387d693d21248d4350d3d4e07b6a))
  *  [AOU-1947] hide one of the redundant buttons in auto imported file popup ([195dcbee](https://gitlab.unige.ch/aou/aou-portal/commit/195dcbee113d07cb7a2d361666c72b7c4b5e5dd3))
  *  align correctly checkbox embargo with label ([3a120e14](https://gitlab.unige.ch/aou/aou-portal/commit/3a120e14cafb18960d5267b3bb0069f10daad0b7))
  *  [AOU-1597] add url to info bull in the see archive action ([6eaca373](https://gitlab.unige.ch/aou/aou-portal/commit/6eaca3739d5548446ac454fda71c93f18058e99a))
  *  change color and icon for differences panel ([66626de0](https://gitlab.unige.ch/aou/aou-portal/commit/66626de0db89774739be17b3eed20e9f2e97652a))
  *  improve differences panel ([75fbf216](https://gitlab.unige.ch/aou/aou-portal/commit/75fbf216d84313b63689ffeebb70515efc758be5))
  *  [AOU-1850] improve design of alert/error messages ([e312ad7f](https://gitlab.unige.ch/aou/aou-portal/commit/e312ad7f198073d2dfdcc6db7644d36d9e86491d))
  *  fix error detected section in dark mode ([40c51809](https://gitlab.unige.ch/aou/aou-portal/commit/40c51809da496d6d4b2a5a56350cf6e58b1235c6))
* **contributor page:**  [AOU-1917] fine tuning ([19825dd7](https://gitlab.unige.ch/aou/aou-portal/commit/19825dd72e5fb89f24e0f0e54cfc9829e49f6aee))
* **orcid:**  [AOU-1902] adapt to orcid guideline ([140c22aa](https://gitlab.unige.ch/aou/aou-portal/commit/140c22aa36436921aee693ddd097f157938a75ac))
* **contributor:**
  *  break word for download column ([1a72a54b](https://gitlab.unige.ch/aou/aou-portal/commit/1a72a54bfc80a944df65bfedd0f9eba57a9577bc))
  *  display circle when no full text publication ([fca82e72](https://gitlab.unige.ch/aou/aou-portal/commit/fca82e728a748a70ce5a8a3f8d55e8d85c2b26fb))
  *  align number download and view to right ([26193d9a](https://gitlab.unige.ch/aou/aou-portal/commit/26193d9abbf7cd9557088e49cbb0667cebd376b4))
  *  display logo instead of circle color for access level ([d9cce421](https://gitlab.unige.ch/aou/aou-portal/commit/d9cce421fe02e93fa43de374d01d26cb5bf477c6))
  *  remove decimal in chart on axis and percents ([24e99f36](https://gitlab.unige.ch/aou/aou-portal/commit/24e99f360e8a24710149843cd278b6e3cd7766db))
  *  avoid chart zoom when change resolution to smarphone ([6b86686d](https://gitlab.unige.ch/aou/aou-portal/commit/6b86686d19573e842ff9038076be3b57e10e25c6))
  *  [AOU-1896] adapt charts design ([b279fb20](https://gitlab.unige.ch/aou/aou-portal/commit/b279fb20a23b63ec2a67dddf8ce2d87e2e33442a))
  *  change order of enum open access ([8fe09329](https://gitlab.unige.ch/aou/aou-portal/commit/8fe09329afaf6f20317b0be6b2f02aac5bf4c587))
  *  fix link in chart title ([988ee96e](https://gitlab.unige.ch/aou/aou-portal/commit/988ee96e94ff727408503cd58ab20a248059647b))
  *  display last 10 years on diffusion bar chart ([0bd52524](https://gitlab.unige.ch/aou/aou-portal/commit/0bd52524a5f51aa782bd932f7c77832d7f98d031))
  *  improve style of contributor page ([05b8d01a](https://gitlab.unige.ch/aou/aou-portal/commit/05b8d01aa89992834d8c6786ef3f211aef6d6777))
* **publication:**  [AOU-1887] use open access logo ([a85fe71d](https://gitlab.unige.ch/aou/aou-portal/commit/a85fe71d404fe67c675e979934dbacbeb06d72c1))
* **profile:**
  *  update label import / export history ([8f61c5f0](https://gitlab.unige.ch/aou/aou-portal/commit/8f61c5f0441474cf06ffa5a0dae537b70d550314))
  *  improve contributor link ([98331fed](https://gitlab.unige.ch/aou/aou-portal/commit/98331fedb9a0db4257e642622b1fc222a3b9d1a3))
* **home-publication:**  update labels ([a2cf90b5](https://gitlab.unige.ch/aou/aou-portal/commit/a2cf90b56d132c0bdb204f83206b23cc9ed98c46))
* **datatable:**  display only unige member in pink and add underline when clickable ([e4e02bc1](https://gitlab.unige.ch/aou/aou-portal/commit/e4e02bc16eaad63215964e53fc8c32d8e0d1e265))
* **home:**  [AOU-1689] change style of 5 last publications ([604fafa0](https://gitlab.unige.ch/aou/aou-portal/commit/604fafa055fe413e8f263cfd89c4cc7b2b924d10))
* **bibliography:**  fix height of anteriority year input ([ba9e6221](https://gitlab.unige.ch/aou/aou-portal/commit/ba9e6221f68a6948dce8a6b83f7214d3a611ea18))
* **chart:**
  *  improve size of bar chart ([3b9b31f3](https://gitlab.unige.ch/aou/aou-portal/commit/3b9b31f3e805f552d3dbfd01c971147ee0ba31fc))
  *  improve legend ([5669fb08](https://gitlab.unige.ch/aou/aou-portal/commit/5669fb08c8a0ca8379a3b106e20937c1afc6523a))
* **user profile:**  reduce font size and gap between checkbox ([1e9aecda](https://gitlab.unige.ch/aou/aou-portal/commit/1e9aecdaf6465c67bae05a465ca36ba6f8262e52))
* **person structure sub type:**  display label ([0c467b96](https://gitlab.unige.ch/aou/aou-portal/commit/0c467b9620270a02000309a2de887dd0bb559666))


### [2.0.7](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.6...aou-2.0.7) (2023-11-06)


### Bug Fixes

* [AOU-1774] disable button to send into validation if a publication in edition has no update yet ([93e497d](https://gitlab.unige.ch/aou/aou-portal/commit/93e497d419f7052723e1ae8fb6eb44dcfd760058))
* **homepage:** do not call thumbnail endpoint if publication doesn't have any ([b16db96](https://gitlab.unige.ch/aou/aou-portal/commit/b16db961ffb3af50571cb2e34ddc9b882c8e9803))

### [2.0.6](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.5...aou-2.0.6) (2023-10-23)


### Features

* **index:** [AOU-1758] add new facet translations for principalFileAccessLevel ([a1681fd](https://gitlab.unige.ch/aou/aou-portal/commit/a1681fde25086fd678b2642958b7f33d60480882))
* **index:** [AOU-1759] add new openAccess facet values translations ([6bcef52](https://gitlab.unige.ch/aou/aou-portal/commit/6bcef5275db5dbd94134eed5235b74083ad72a4d))
* **published deposit:** [AOU-1759] display Open Access label according to new openAccess index field ([61bf1fc](https://gitlab.unige.ch/aou/aou-portal/commit/61bf1fcef1a2a827ad865bb5d1a816659fae2355))


### Bug Fixes

* enable initialCriticalCss in SSR ([6e519e7](https://gitlab.unige.ch/aou/aou-portal/commit/6e519e70d71e85f54f1c42a5730d5bdf79bb1ec4))

### [2.0.5](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.4...aou-2.0.5) (2023-10-03)


### Bug Fixes

* disable warning in SSR ([675b0bf](https://gitlab.unige.ch/aou/aou-portal/commit/675b0bf38fa29a2a8facd3367f6709bc7382a6ce))
* expose robots.txt at root level ([b58c098](https://gitlab.unige.ch/aou/aou-portal/commit/b58c09822e49b488d69c2364d93c648d4af943c6))
* **i18n:** rename substructures into sub-entities ([d3932ff](https://gitlab.unige.ch/aou/aou-portal/commit/d3932ff7121bc432ff43e8ee8748ee29172b796e))
* **public deposit:** [AOU-1741] update date of presentation label for Presentation and Poster ([d82c90f](https://gitlab.unige.ch/aou/aou-portal/commit/d82c90f4ba5cf4a6d660753225a3492c2d8c48e5))

### [2.0.4](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.3...aou-2.0.4) (2023-09-11)


### Features

* **DepositDetail:** [AOU-1740] hide 'Ask feedback' button when in UPDATES_VALIDATION status ([a819eb7](https://gitlab.unige.ch/aou/aou-portal/commit/a819eb77675c287d1d6b4a0840246bd20c4c130d))


### Bug Fixes

* **DepositDetail:** show eventual metadata differences when in FEEDBACK_REQUIRED status as well ([3996ab5](https://gitlab.unige.ch/aou/aou-portal/commit/3996ab5f6a272a2195c3fe9b67867ac23b1a8e95))

### [2.0.3](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.2...aou-2.0.3) (2023-08-28)


### Features

* [AOU-1331] enable server side rendering ([b9995cd](https://gitlab.unige.ch/aou/aou-portal/commit/b9995cd418b7d8ee195f506ee70505a4f6cb806d))
* **deposit detail:** [AOU-1723] Show delete button for ADMIN and ROOT users ([0d178c1](https://gitlab.unige.ch/aou/aou-portal/commit/0d178c11dcd31faaa01e13d63441fe89fb6cae7d))
* **DepositDetailRoutable:** [AOU-1726] set color of delete button to red ([caf22fa](https://gitlab.unige.ch/aou/aou-portal/commit/caf22fa2bf658141787d1bb913e66ef6e0c7fc77))


### Bug Fixes

* [AOU-1695] display validation errors on publications that are ready to validate updates ([8c2b86d](https://gitlab.unige.ch/aou/aou-portal/commit/8c2b86da481dc58f1073cfc22416c5fe8065a7f1))
* **deposit list:** [AOU-1651] allow to see bulk actions buttons on my deposit list page ([c9c850a](https://gitlab.unige.ch/aou/aou-portal/commit/c9c850a97f2668a6691dba259440d33c3d425600))
* ignore oai pmh module in about page ([4e57eee](https://gitlab.unige.ch/aou/aou-portal/commit/4e57eee93e22113b8fc0f5aeb4a336014c360b86))
* lint error ([cbb9dbc](https://gitlab.unige.ch/aou/aou-portal/commit/cbb9dbc1fb062bbd1a8a057d48a39cfd5dcff46e))
* **publication display:** [AOU-1700] display container title when only the title is present ([4b46d6d](https://gitlab.unige.ch/aou/aou-portal/commit/4b46d6db3734ed34a1ed0234f181c9f5b09a2c6b))
* style of home page in SSR ([879034e](https://gitlab.unige.ch/aou/aou-portal/commit/879034e5b3ec0371cfb159ff08c3bbf16b51fac8))

### [2.0.2](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.1...aou-2.0.2) (2023-06-13)


### Features

* **advanced search:** [AOU-1677] tick 'UNIGE contributors only' checkbox by default ([a795bf4](https://gitlab.unige.ch/aou/aou-portal/commit/a795bf4a828fa8e051b29fb0ef5dd5a6caa1fae4))
* **deposit summary:** [AOU-1667] display name of last editor beside creator name ([a76fbeb](https://gitlab.unige.ch/aou/aou-portal/commit/a76fbeb498bcbd8d32dbcd84b39c673cfaaba952))
* **i18n:** update pinboard explanation texts ([27af6d2](https://gitlab.unige.ch/aou/aou-portal/commit/27af6d2a32df04e65324aae6ce89961433e12796))
* preserve current page when login ([881c2e8](https://gitlab.unige.ch/aou/aou-portal/commit/881c2e8aed4d3a528d7e4411007f34e64d2b020d))
* preserve current page when login ([e6ff33c](https://gitlab.unige.ch/aou/aou-portal/commit/e6ff33cbead8cae5c4d87c9cc1a6aa10abc30eaa))


### Bug Fixes

* **i18n:** update some texts ([8a11ad6](https://gitlab.unige.ch/aou/aou-portal/commit/8a11ad6530ff93e3e03923c6363957a20acd184d))
* proxy for short doi ([de09878](https://gitlab.unige.ch/aou/aou-portal/commit/de098789a8d88943ccad85ef8f28a38ed9a57420))

### [2.0.1](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.0.0...aou-2.0.1) (2023-04-27)


### Features

* **deposit details:** [AOU-1602] display bibliographic reference in ISO format ([06efb37](https://gitlab.unige.ch/aou/aou-portal/commit/06efb37e92e57c6e216e57a18304a8f6d7880c25))


### Bug Fixes

* [AOU-1632] Update publication public display ([cbbd1c1](https://gitlab.unige.ch/aou/aou-portal/commit/cbbd1c1c8f6e4a5b1ed6833c694da004ebe84191))
* **deposit details:** fix PMID never displayed ([0eab590](https://gitlab.unige.ch/aou/aou-portal/commit/0eab590edf3a063e16c78f0485ea69c4e735aea5))
* **details page:** [AOU-1632] show publication sub-subtype ([0ab486a](https://gitlab.unige.ch/aou/aou-portal/commit/0ab486a3e992f30ac0802001f35e6673f519696c))
* **details page:** [AOU-1655] display correct language name (or iso code if translation doesn't exist) ([991d87f](https://gitlab.unige.ch/aou/aou-portal/commit/991d87f62e049879ad82b76499783d2edb4c74ab))
* **details page:** do not show presentedAt when value is empty ([c24a8e5](https://gitlab.unige.ch/aou/aou-portal/commit/c24a8e572c5bb4cd2a4f979e900a7617508e4a81))
* **details page:** force change detection to fix 'Show more' button visibility ([79f0004](https://gitlab.unige.ch/aou/aou-portal/commit/79f0004fa64cbd54a416f5d651c5335d43bbf558))
* **details page:** move errata note at first position ([769e9b2](https://gitlab.unige.ch/aou/aou-portal/commit/769e9b2f000a280379c1bdb76d150b99955ee98e))
* **details page:** print 'arXiv' instead of 'ARXIV' ([dbe277f](https://gitlab.unige.ch/aou/aou-portal/commit/dbe277fce342b477fd431c4f9a13f5d761e808ff))
* **details page:** rename pagination into pages number ([cc92783](https://gitlab.unige.ch/aou/aou-portal/commit/cc92783f42788c30793602f34960c0fae9b1279b))
* **details page:** show DBLP identifier ([894aa72](https://gitlab.unige.ch/aou/aou-portal/commit/894aa72abc2fc1846b0cce9b478033086069c3e4))
* **details page:** show publishedIn and rawPagesInfo only if not empty ([f1c4ce1](https://gitlab.unige.ch/aou/aou-portal/commit/f1c4ce1185626717d92d94651d2c91140c664ff3))
* **details page:** wrap structures and fundings when too large ([f616798](https://gitlab.unige.ch/aou/aou-portal/commit/f616798138f05500b483eb3fc5ac91340acaccbb))
* display meta pdf url only if public ([3244a6a](https://gitlab.unige.ch/aou/aou-portal/commit/3244a6a6ea8a38b14402c74a05bdee75bde0f2dc))
* don't send authorization token on short doi endpoint ([dee7c35](https://gitlab.unige.ch/aou/aou-portal/commit/dee7c353a6aa307fa9c64e699dbe15dcd438d1fe))
* **header:** [AOU-1641] Show searchbox in header when on favorite searches and bibliography ([43e65bf](https://gitlab.unige.ch/aou/aou-portal/commit/43e65bfec9139ae5ae7b9dabe6dab326636d5fad))
* **i18n:** use 'number of pages' instead of 'pages number' ([2572fdd](https://gitlab.unige.ch/aou/aou-portal/commit/2572fddaaf429559cb4a9d768478276db6a1710f))
* **meta:** use subtype instead of type ([4e2de2c](https://gitlab.unige.ch/aou/aou-portal/commit/4e2de2c01b846a7507f8c058abf96d9fe53f7e7a))

## [2.0.0](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.2.0-RC3...aou-2.0.0) (2023-03-21)


### Bug Fixes

* **home search:** sort correctly search criteria ([98359e2](https://gitlab.unige.ch/aou/aou-portal/commit/98359e29c2ad32ac856c3a38150787d082483750))

## [1.2.0-RC2](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.2.0-RC1...aou-1.2.0-RC2) (2023-03-15)


### Features

* [AoU-1581] Add copy/paste button next to the script generated for bibliography ([49aea3e](https://gitlab.unige.ch/aou/aou-portal/commit/49aea3e50ed1384e2fdb207217c66405ad37122a))
* add 'Recording' facet value translation ([0b6bc6f](https://gitlab.unige.ch/aou/aou-portal/commit/0b6bc6f1e5d86d8d9cd85fc7194e681bc212a635))
* add noFulltext facet translation ([9737bdb](https://gitlab.unige.ch/aou/aou-portal/commit/9737bdbff668de11d62602958b3eaf4eca63f628))
* **deposit list:** add radio to choose to display deposit created or as contributor ([d68f9a9](https://gitlab.unige.ch/aou/aou-portal/commit/d68f9a943ae0c1df3acf3d632478805ac753073b))
* **home search:** disable/enable 'Update search' and 'Generate bibliography' buttons according to stored search modified status ([102e9fb](https://gitlab.unige.ch/aou/aou-portal/commit/102e9fb4fe6ac439059bacdc58f01399fa94d5b8))
* **homepage:** [AOU-1567] put last publications beside carrousel ([cdee27d](https://gitlab.unige.ch/aou/aou-portal/commit/cdee27d14a6fc31df2cb82a73c1a221b3297cf23))
* **HomeState:** total publications on homepage includes or excludes restricted access masters according to user profile ([bb8f836](https://gitlab.unige.ch/aou/aou-portal/commit/bb8f836bf918eedfd818d79de9103086e26df548))
* **main menu:** [AOU-1626] hide badge for favorite publications ([6ce2b89](https://gitlab.unige.ch/aou/aou-portal/commit/6ce2b8907601e6117ac11893b297e3e2c1c5d8e3))
* **main toolbar:** [AOU-1626] reorganize menu items ([72447e4](https://gitlab.unige.ch/aou/aou-portal/commit/72447e4d4096b2f24207c39e608ef2d3301769cf))
* **search results:** [AOU-1567] display UNIGE contributors in primary color ([526f942](https://gitlab.unige.ch/aou/aou-portal/commit/526f94294b26c22d5bf8ccd40f9aada988ae1be6))
* **search results:** [AOU-1567] use a short string for container title ([1a1fe0c](https://gitlab.unige.ch/aou/aou-portal/commit/1a1fe0c9aa8000ca662ee6f718764f52317526cf))
* **StoredSearch:** [AoU-1558] warn the user when deleting a stored search used to generate a bibliography ([73254d0](https://gitlab.unige.ch/aou/aou-portal/commit/73254d05bd2bc31debc30381ca89aa176ee5f567))


### Bug Fixes

* [AoU-1599] Design of the bibliography generator page: spare some spaces ([1f05210](https://gitlab.unige.ch/aou/aou-portal/commit/1f052109526d6b4600bf07af7bea9191dfe456c2))
* [AoU-1607] Generates a new UID when changing query parameters ([65c604d](https://gitlab.unige.ch/aou/aou-portal/commit/65c604dcb4f8475a67a3f0f816d7c5c54e808395))
* [AoU-1613] Hide doctor's info when creating a thesis dated before 2010 or published outside unige ([098eea8](https://gitlab.unige.ch/aou/aou-portal/commit/098eea8e583c58692f57ffcc4b04f1e2360d6183))
* [AoU-1615] change endpoint to search contributors in advanced research ([a74dff0](https://gitlab.unige.ch/aou/aou-portal/commit/a74dff0d8999fe9330469ce92d27c1653dff57db))
* [AoU-1624] Bibliography generator page: switch tabs ([2245ad7](https://gitlab.unige.ch/aou/aou-portal/commit/2245ad7c3edd840683777dbd23fdf4c9be5ce7ac))
* **bibliography:** show selected publications titles as innerHTML to support html tags ([499f60e](https://gitlab.unige.ch/aou/aou-portal/commit/499f60e795993b266e1dc27bdadd2758fee6c548))
* **i18n:** label updates ([f9919de](https://gitlab.unige.ch/aou/aou-portal/commit/f9919de2c2781d9dd1cfbe625ca9502a3f17c187))
* **i18n:** typo ([1bfb490](https://gitlab.unige.ch/aou/aou-portal/commit/1bfb490d41893cf4671fddfce6b91ab889f7d16d))
* **i18n:** typo in fr.json ([478e8c9](https://gitlab.unige.ch/aou/aou-portal/commit/478e8c90aadbfa9c0cf14345cfe7972ea900073a))
* lint errors ([6cfa71f](https://gitlab.unige.ch/aou/aou-portal/commit/6cfa71f22d6cc640f4b0be30ac8512155329d969))
* **main menu:** search button navigates to search page with/without restricted access masters parameter according to user profile ([e541861](https://gitlab.unige.ch/aou/aou-portal/commit/e54186148f98287b167ac03ae03de77e809b262b))

## [1.2.0-RC1](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.2.0-alpha1...aou-1.2.0-RC1) (2023-02-08)


### Features

* [AOU-1345] add link to SNSF on deposit edit, deposit summary and public page ([e6c0c14](https://gitlab.unige.ch/aou/aou-portal/commit/e6c0c14dac0fc240784f85493065ae9b7810e7e6))
* [AoU-1352] Add new publication type as Professional Thesis ([5839194](https://gitlab.unige.ch/aou/aou-portal/commit/5839194bec7d375d3f24b3356473ec2997b8ed73))
* [AoU-1379] first version of publications update / cancel ([9c0879f](https://gitlab.unige.ch/aou/aou-portal/commit/9c0879fb5da21f6ade580c7c87d71ea162606d5e))
* [AoU-1380] Change status from COMPLETED to CANONICAL and viceversa ([a1bcd34](https://gitlab.unige.ch/aou/aou-portal/commit/a1bcd343f2d1bab0b3361114ec57a2ed53568561))
* [AOU-1405] synchronize user orcid with authorization server ([98c754c](https://gitlab.unige.ch/aou/aou-portal/commit/98c754ccc25714cf40500258bd002094cda1655a))
* [AOU-1427] (bibliography) create UI to generate a JS bibliography export ([5b7adc9](https://gitlab.unige.ch/aou/aou-portal/commit/5b7adc97131660a2b4c5e9862d9036aac4f44e8c))
* [AoU-1430] Download publications from access module ([f8722ef](https://gitlab.unige.ch/aou/aou-portal/commit/f8722efc14cf81b50983f46e0f756fe2cd3b2443))
* [AOU-1431] add button to create a cookie to show or hide non public masters ([9834916](https://gitlab.unige.ch/aou/aou-portal/commit/98349160b647dfaeec9801a606ee80d4045d075b))
* [AoU-1432] Disable deposit actions for users of non authorized institutions ([e94e6e8](https://gitlab.unige.ch/aou/aou-portal/commit/e94e6e8bea695c70b72d5e7b2aa4f7992b011f73))
* [AOU-1453] combined structures and contributors criterias ([8bfd1c5](https://gitlab.unige.ch/aou/aou-portal/commit/8bfd1c5a03d466152d38887dd752b8ede26add24))
* [AOU-1504] set meta tags in header for public pages and publications ([f0b1b66](https://gitlab.unige.ch/aou/aou-portal/commit/f0b1b66fa314cf9b0e2df9b45580d7f629ac563f))
* [AoU-1509] Create robots.txt file ([46188c7](https://gitlab.unige.ch/aou/aou-portal/commit/46188c7e5e037a60be9ad1c6ca0cca3310643589))
* [AoU-1561] Show differences at summary step when updating a deposit ([98e05f7](https://gitlab.unige.ch/aou/aou-portal/commit/98e05f70144261fa964a8cf63f5d00de78c30085))
* [AoU-1577] Files labels are not displayed on public notice page ([f7701e7](https://gitlab.unige.ch/aou/aou-portal/commit/f7701e715262eda546ff68c48a1bde88d22612d7))
* [AoU-1589] Be able to put in status DELETED publications already completed and rejected ([91a8856](https://gitlab.unige.ch/aou/aou-portal/commit/91a88562e5995d08d646cd093137e25930e5509c))
* add button to shared saved search from pinboard ([d61efa4](https://gitlab.unige.ch/aou/aou-portal/commit/d61efa431917836c31be5517c55ddf0a313806a6))
* **admin:** [AOU-1480] manage index field alias ([9acfd1c](https://gitlab.unige.ch/aou/aou-portal/commit/9acfd1c1eeb749eed98011745876a292074d1e58))
* allow to navigate between publication ([c3a9981](https://gitlab.unige.ch/aou/aou-portal/commit/c3a9981dc217638cc7636b8ce020afbc7d631fca))
* **bibliography:** [AOU-1478] Add parameter allowing to group publications by subtypes ([ef75c02](https://gitlab.unige.ch/aou/aou-portal/commit/ef75c027dec5da146aca88dcc03ab5f9d53147ea))
* **bulk structure:** [AOU-1383] use component searchable tree single select ([b10afd1](https://gitlab.unige.ch/aou/aou-portal/commit/b10afd164618c291a5cb162d71c4e391a2b355a3))
* **deposit and reserach group:** [AOU-1434] add action in publication list to navigate on public publication ([1194edf](https://gitlab.unige.ch/aou/aou-portal/commit/1194edf043e674622ec5961a70001eb9074aea2c))
* **deposit contributors:** [AOU-1281] rise a warning when contributor missing firstname ([0af75b1](https://gitlab.unige.ch/aou/aou-portal/commit/0af75b10b876cca8ccf0fa3b050d44084b6862cf))
* **deposit list:** [AOU-1114] display edit button when only one deposit selected ([3b63223](https://gitlab.unige.ch/aou/aou-portal/commit/3b632235ffd4781e614548744951200e17b9ab8f))
* **deposit list:** [AOU-1392] hide empty deposit tab if no publication to display ([8c23b85](https://gitlab.unige.ch/aou/aou-portal/commit/8c23b858a55c5f8baba7e7196e4c511df6cf4d14))
* **deposit:** [AOU-1267] add history action ([e07d35f](https://gitlab.unige.ch/aou/aou-portal/commit/e07d35f34d5f28de9cdbe0e44edd8bd2271654cc))
* **deposit:** [AOU-1456] handle unauthorized response when delete agreement file ([f94b7da](https://gitlab.unige.ch/aou/aou-portal/commit/f94b7da79891e46a42cc9d9488b0eca5ef4d5f42))
* **favorites:** [AOU-1488] insert div section to explain how to generate bibliography ([dfa5674](https://gitlab.unige.ch/aou/aou-portal/commit/dfa567486a396df4eab6a180b85f175a8a79e515))
* **home page:** display correct number of publication ([01845ec](https://gitlab.unige.ch/aou/aou-portal/commit/01845ec1dfa88a6eb4e93ef47e3287d092fbf620))
* **home page:** sort last publication ([bdeb7c9](https://gitlab.unige.ch/aou/aou-portal/commit/bdeb7c908cc8ddf5c71ca71fb38978305dd87529))
* **home publication detail:** [AOU-1482] manage action contact author ([3e7d3c8](https://gitlab.unige.ch/aou/aou-portal/commit/3e7d3c85bbb4c43292a0509c61ecf9941fcd52c2))
* **home publication detail:** [AOU-1483] manage action to ask correction ([c229132](https://gitlab.unige.ch/aou/aou-portal/commit/c229132732256d6f0487d2fd97b609aa651013bb))
* **home publication detail:** [AOU-1505] manage update action + replace usages of IS_CREATOR_OR_CONTRIBUTOR endpoint by LIST_PUBLICATION_USER_ROLES endpoint ([b359c1e](https://gitlab.unige.ch/aou/aou-portal/commit/b359c1e626fde85ba8075d3d90fc45ff10f8de04))
* **home publication detail:** [AOU-1522] update tyle of download button file ([84b171b](https://gitlab.unige.ch/aou/aou-portal/commit/84b171b916e6d78b081b73c771a3a9fe003f03b6))
* **home publication detail:** add URN identifier ([3fe2e28](https://gitlab.unige.ch/aou/aou-portal/commit/3fe2e28b4514b8186cbce7d57e77603e09286ae0))
* **home publication search:** sort by year descending ([cb9639d](https://gitlab.unige.ch/aou/aou-portal/commit/cb9639d1a6899ce3d730d82ca7a89e9f5d7498dd))
* **home search and detail:** [AOU-1497] add several improvments ([4361547](https://gitlab.unige.ch/aou/aou-portal/commit/4361547ec08910e62cb900acb0ad1eec9cca2801))
* **home search:** add button to share search url ([c2afd1f](https://gitlab.unige.ch/aou/aou-portal/commit/c2afd1fa06dc696e08e393e233635818464baf46))
* **home search:** allow to search by start year or end year only ([44ff6dd](https://gitlab.unige.ch/aou/aou-portal/commit/44ff6ddbe70cd758f742b8c7fc5e691d5eda4254))
* **main toolbar:** [AOU-1481] display menu for static pages ([eabd29b](https://gitlab.unige.ch/aou/aou-portal/commit/eabd29baae25724ff3b7a7ce7f71030671dba727))
* **oai-pmh:** add oai pmh link on footer and menus in admin ([012565c](https://gitlab.unige.ch/aou/aou-portal/commit/012565c7bb07377b230ce95011a9fce7f1859690))
* **public publication page:** [AOU-1419] update according to templates ([a8416e6](https://gitlab.unige.ch/aou/aou-portal/commit/a8416e63beefa80ce00f7bc5945e4b05931cf515))
* **publication detail:** enable preview of files ([7e57424](https://gitlab.unige.ch/aou/aou-portal/commit/7e57424a3249cfa55c55b8ea00a0ddc8b2ef8af6))
* **publication page:** [AOU-1433] add statistics informations on publication public page ([a316c43](https://gitlab.unige.ch/aou/aou-portal/commit/a316c43aa3735b43e646d401380c40ebd70e978f))
* **publication public detail:** use archive id instead of resId ([09a278a](https://gitlab.unige.ch/aou/aou-portal/commit/09a278afd803c9010a95e0e0708f413c47d7f984))
* **search:** [AOU-1381-1382-1384-1385] manage advanced search, except facets, pinned publication and save searches ([077c9c8](https://gitlab.unige.ch/aou/aou-portal/commit/077c9c85b9df0df4d5948653dedb3c9133c0bdb6))
* **search:** manage fulltext search ([e093179](https://gitlab.unige.ch/aou/aou-portal/commit/e093179ba168f3c280801cac0a8b282388115394))
* use displayName when searching contributors in advanced search ([cab4196](https://gitlab.unige.ch/aou/aou-portal/commit/cab41960272cef6578a8581aff3107675c8ed175))


### Bug Fixes

* (i18n) update labels ([8a1786b](https://gitlab.unige.ch/aou/aou-portal/commit/8a1786bd4af9824569ce96a158913e1cbb49a56a))
* [AOU-1040] allow completion of publication bulk validation even when error ([a24d3ed](https://gitlab.unige.ch/aou/aou-portal/commit/a24d3ed00fc677302214dda5f551c4afe84218be))
* [AOU-1147] display deposit identifiers in red when only an integer for validator only ([07f3e7b](https://gitlab.unige.ch/aou/aou-portal/commit/07f3e7b94194c108b9ce1186852b6ae9ad9c7ecd))
* [AOU-1249] isbn translation ([d9cd2ee](https://gitlab.unige.ch/aou/aou-portal/commit/d9cd2ee8c43df6f23a4f63179d6f4a7deeebe844))
* [AOU-1300] use label callback on admin structure tree ([17a9c32](https://gitlab.unige.ch/aou/aou-portal/commit/17a9c328a6f10dbed10d6e3dab2794087a413940))
* [AoU-1310] Be able to add contributors without firstname ([37d7f79](https://gitlab.unige.ch/aou/aou-portal/commit/37d7f7900736c8450ed6f00d2d6b8a454a86aac4))
* [AOU-1341] translate labels for deposit type ([0c57250](https://gitlab.unige.ch/aou/aou-portal/commit/0c572504abc8f872a1e4214247121521a8cebbb6))
* [AOU-1346] update order deposit status filter ([be01216](https://gitlab.unige.ch/aou/aou-portal/commit/be012169f133633206a154c079ba95a39a9b8ba3))
* [AoU-1363] Display email field into profile ([cd7b971](https://gitlab.unige.ch/aou/aou-portal/commit/cd7b971adcb3fb1812b4d55bc73034239d34b817))
* [AOU-1368] no overlay on truncated error messages ([773ed33](https://gitlab.unige.ch/aou/aou-portal/commit/773ed33338b764c33d759eab6f4b766b2454c455))
* [AOU-1370] hide business roles when empty in admin user detail and lock orcid field in profile when verified ([0e3e146](https://gitlab.unige.ch/aou/aou-portal/commit/0e3e146c725f657fe5fc28d20a37e6c386d64a2e))
* [AoU-1492] fix labels when rejecting an updated deposit ([225c4c7](https://gitlab.unige.ch/aou/aou-portal/commit/225c4c71c87d71da65c7f240fd99b8f01d20dd3e))
* [AoU-1499] Simplified metadata differences text for creators and collaborators ([7a59138](https://gitlab.unige.ch/aou/aou-portal/commit/7a591388a89e13eabee826cac5f8c03b0171b543))
* [AOU-1530] public deposit page: prevent navigating with keys when dialogs is open ([cc9de83](https://gitlab.unige.ch/aou/aou-portal/commit/cc9de83447ee1494cda582f89573985863f94b58))
* [AoU-1550] Do not display thumbnail if it does not exits in the last 5 deposit at home page ([3d48718](https://gitlab.unige.ch/aou/aou-portal/commit/3d48718aff481835c9a068387ae9c95e18032744))
* [Aou-1568] Do not search automatically when writing in the search bar ([86e32f5](https://gitlab.unige.ch/aou/aou-portal/commit/86e32f507ab02be4cb6d50b8dd2c1af3a675a6b4))
* [AoU-1579] Change to admin permission the access to facet management ([663d6a9](https://gitlab.unige.ch/aou/aou-portal/commit/663d6a98c6bed58cd1da919701ffce5162613b46))
* adapt proxy for users with multiples words at lastname ([ba50f43](https://gitlab.unige.ch/aou/aou-portal/commit/ba50f437a5d1f646ae8a60073615822e7963ed65))
* add deps for APP_OPTIONS ([f12f8e3](https://gitlab.unige.ch/aou/aou-portal/commit/f12f8e377a3905ef0211f38c70737c95bfccecb8))
* add missing english translate ([af64fba](https://gitlab.unige.ch/aou/aou-portal/commit/af64fbac742d885b6b7420909e6120d6da87ba63))
* add unreferenced state contributor ([7515111](https://gitlab.unige.ch/aou/aou-portal/commit/75151111151ad1c3de6c93177f339929a4168c86))
* added validations rules when updating a thesis publication ([dbfccf8](https://gitlab.unige.ch/aou/aou-portal/commit/dbfccf8728d3d88e3533f82d779d787a338798d6))
* **admin user:** [AOU-1371] allow to merge two users ([aed8d1e](https://gitlab.unige.ch/aou/aou-portal/commit/aed8d1e742fc1c420f86f518e7eaf0cea95c11a1))
* **advanced search contributor:** [AOU-1489] preserve search info on load next chunk ([efd89e9](https://gitlab.unige.ch/aou/aou-portal/commit/efd89e9a4cee0dd193bd9a1798056fb30f8095b1))
* **advanced search:** allow to preserved same criteria and preserved also order of criteria ([3394bf3](https://gitlab.unige.ch/aou/aou-portal/commit/3394bf3861980b30cdeddd9de504a4a836ef82b4))
* **advanced search:** rename enum criteria ([82ee92d](https://gitlab.unige.ch/aou/aou-portal/commit/82ee92dfbfb75b94d32a50821c847826899c84dd))
* app bootstrap ([d4b25f2](https://gitlab.unige.ch/aou/aou-portal/commit/d4b25f2ea0bd307beb038980fa92cdc55e88d37a))
* avatar logo ([1beabce](https://gitlab.unige.ch/aou/aou-portal/commit/1beabce5c29602b148d9d866adf5ae7667678dff))
* bootstrap of application ([f0c5e80](https://gitlab.unige.ch/aou/aou-portal/commit/f0c5e807414a9d4962849b49d5944862357e12c9))
* change way to override endpoint list my saved search ([674ad80](https://gitlab.unige.ch/aou/aou-portal/commit/674ad803ed1f7e0cdf2691b00e1e6743f5cd1a81))
* clean search info when opening the detail from another place than the search result ([f153a37](https://gitlab.unige.ch/aou/aou-portal/commit/f153a3705caa3a385a9c51bd3b14488024d20c93))
* **deposit collection number:** remove input validator to allow to enter letter ([97106e6](https://gitlab.unige.ch/aou/aou-portal/commit/97106e6bb8979358a4dd59f773be4c08269e032c))
* **deposit detail:** [AOU-1492] Specific message in dialog when rejecting publication update ([accee97](https://gitlab.unige.ch/aou/aou-portal/commit/accee97f7238da70b79569222f9b363fd42c8a64))
* **deposit file:** [AOU-1177] drag and drop ([6acd20f](https://gitlab.unige.ch/aou/aou-portal/commit/6acd20f7f413a889b16df4789700ba201a39745a))
* **deposit file:** prevent drag and drop when readonly ([f455db6](https://gitlab.unige.ch/aou/aou-portal/commit/f455db660f05efded5618f99c7967b97865ac8f9))
* **deposit snsf:** [AOU-1345] url to snsf website ([ff7f282](https://gitlab.unige.ch/aou/aou-portal/commit/ff7f282b47795a8085f87b99ab8b9eccabdc2787))
* **deposit:** [AOU-1426] refactor to allow multi validation on deposit navigation ([db946be](https://gitlab.unige.ch/aou/aou-portal/commit/db946be4e88e2b10c242a004d3aaee4f0bae646b))
* **deposit:** [AOU-1435] allow root or admin to edit publication in validation mode ([157af3e](https://gitlab.unige.ch/aou/aou-portal/commit/157af3e37f6de39fb30860560739d2a12f66f9be))
* **deposit:** style of result in bulk add research group ([994d4b5](https://gitlab.unige.ch/aou/aou-portal/commit/994d4b5ec2f29bb059573dda46fef072af283119))
* do not remove collaboration when move unige members to collaborators ([a44bf33](https://gitlab.unige.ch/aou/aou-portal/commit/a44bf33e8a1cd4395144ca89e0b6ccbe41c2cc47))
* extract year from date when checking if date is before 2010 ([c4da2ff](https://gitlab.unige.ch/aou/aou-portal/commit/c4da2ffb3646703d962097c06a8b4f88bc74e4e0))
* first login when user is not unige or hug member ([9946b70](https://gitlab.unige.ch/aou/aou-portal/commit/9946b70654c6708c4b4134c8845c2d976cde0b81))
* fix lint problem ([9f32410](https://gitlab.unige.ch/aou/aou-portal/commit/9f3241084dbbe7f2c2ea4ddc67b99c5a8fd780dd))
* **guard:** [AOU-1472] use combined guard to avoid unwanted navigation cancel on page refresh ([c27a607](https://gitlab.unige.ch/aou/aou-portal/commit/c27a60749ebdd89b442c463c6bb0f6a0085e7fd9))
* **home bibliography:** [AOU-1475] add traditionnal href link for publication and advanced search ([78f7656](https://gitlab.unige.ch/aou/aou-portal/commit/78f765682e30ef874e99bcd168d50fce658dbfb8))
* **home bibliography:** [AOU-1475] enlarge format input and add tooltip on ellipsis ([872a833](https://gitlab.unige.ch/aou/aou-portal/commit/872a8333cbfa2e261be5ffa921eb7bc2c2da0846))
* **home page:** [AOU-1475] hide submit deposit if not member unige ([5bf9dc0](https://gitlab.unige.ch/aou/aou-portal/commit/5bf9dc0b581149d281786ce2409dae26b25c3d1d))
* **home publication detail:** [AOU-1475] add help button to explain access levels ([55d4757](https://gitlab.unige.ch/aou/aou-portal/commit/55d47571713a00f21f60bea984e7615d29a0cd6b))
* **home publication detail:** [AOU-1475] add share to mail option ([b465e89](https://gitlab.unige.ch/aou/aou-portal/commit/b465e89280f68af2bec0e5be1e143caef60ef321))
* **home publication detail:** [AOU-1475] disable by default button to download, fix latence and display preview button when not logged ([5f1519c](https://gitlab.unige.ch/aou/aou-portal/commit/5f1519c5e78cdd2316a38ae69092cd4772ccec95))
* **home publication detail:** [AOU-1475] display only open access tag ([b344e64](https://gitlab.unige.ch/aou/aou-portal/commit/b344e649fa9e5ed867b4297d3000cf03fd680a7a))
* **home publication detail:** [AOU-1475] display pagination on header instead of archive id ([d11f6a9](https://gitlab.unige.ch/aou/aou-portal/commit/d11f6a97ffbde399a725cb8027b5ddfd19fcb90d))
* **home publication detail:** [AOU-1475] expand secondary files section if primary section have no files ([d8c4bec](https://gitlab.unige.ch/aou/aou-portal/commit/d8c4bec78028e1eddee44b35e28eb9efe5854fa2))
* **home publication detail:** [AOU-1475] fix link in abstract ([1ce887a](https://gitlab.unige.ch/aou/aou-portal/commit/1ce887abb7a1888c8654c2659131b6329d1cdf28))
* **home publication detail:** [AOU-1475] remove parenthesis around page number in published in ([782be04](https://gitlab.unige.ch/aou/aou-portal/commit/782be04781ac94311547f214739876d45918cf92))
* **home publication detail:** [AOU-1502] refresh abstract ([62c0c6d](https://gitlab.unige.ch/aou/aou-portal/commit/62c0c6d37358851e346ff993189da9d0741f38a1))
* **home publication detail:** [AOU-1549] Display collaborations and collaborations members on public details page ([637b9f8](https://gitlab.unige.ch/aou/aou-portal/commit/637b9f8dbd6d88baae915f872589878b6d8d7155))
* **home publication detail:** [AoU-1578] [AoU-1576] display button update for admin and validator ([4a8aeca](https://gitlab.unige.ch/aou/aou-portal/commit/4a8aecab8a341f2d9d873186c746465d44e2e61c))
* **home publication detail/list:** [AOU-1475] update label for button back to search and link show more ([e48d5d7](https://gitlab.unige.ch/aou/aou-portal/commit/e48d5d7ca0ba82237a92a2d24b67afd7f7a03b47))
* **home publication detail:** allow to contact author or ask correction when not logged ([050c00d](https://gitlab.unige.ch/aou/aou-portal/commit/050c00d80dbaba5606e1b41362d62130131eb9f2))
* **home publication detail:** error when navigate between publication after use back browser button from list ([f78659c](https://gitlab.unige.ch/aou/aou-portal/commit/f78659cb0c207903ef29b4ebe7bd0e0bd314d263))
* **home publication detail:** hide update button when not logged ([001a635](https://gitlab.unige.ch/aou/aou-portal/commit/001a635462393e45da8585fadbe9cd73e53a4ae0))
* **home publication detail:** preserve sort when navigate between publication ([59e3b12](https://gitlab.unige.ch/aou/aou-portal/commit/59e3b12c2e7b44ec7e43e7898b63496fafc09988))
* **home publication detail:** repair button that allow to contact author ([31d9f77](https://gitlab.unige.ch/aou/aou-portal/commit/31d9f77916ddd0c963c9747b4a401473a4d9da9b))
* **home publication search:** [AOU-1475] align correctly numbers results of facets and add tooltip on ellipsis ([5e661d9](https://gitlab.unige.ch/aou/aou-portal/commit/5e661d91588f691555cf11321e562ce33df75faa))
* **home publication search:** [AOU-1475] check structure criteria by default ([d1d0245](https://gitlab.unige.ch/aou/aou-portal/commit/d1d0245cced6a027f9e41d85f86b9c23cae751a3))
* **home publication search:** [AOU-1475] move with restricted access masters in search toolbar ([15af24c](https://gitlab.unige.ch/aou/aou-portal/commit/15af24ca4f7e3acbb936d60ab9b7e0e8678349d5))
* **home publication search:** [AOU-1475] set default pagination to 25 results and remove option 5 ([5b7d388](https://gitlab.unige.ch/aou/aou-portal/commit/5b7d388259bd8edb08bbd7c4a33cda034e440674))
* **HomeFileInfoPresentational:** display current files access levels (instead of embargo values that are over) ([4913933](https://gitlab.unige.ch/aou/aou-portal/commit/49139336ccfa2ebbe1e8ed5ee9a2d4742da645f8))
* **HomeSearchFacetPresentational:** fix number of facets visible by default ([39c68ff](https://gitlab.unige.ch/aou/aou-portal/commit/39c68ffe3c644fa3aa2700622957437418269fcf))
* **IndexFieldAlias:** use ISO639-1 to store IndexFieldAlias labels in order to match selected portal display language ([3c65a0e](https://gitlab.unige.ch/aou/aou-portal/commit/3c65a0e0fb99cad04ea28c96866eb2742d27c14a))
* jenkins build by removing useless patch ([c07cec8](https://gitlab.unige.ch/aou/aou-portal/commit/c07cec86c8a08a9f7799275a822822ff98edec16))
* **keyword cleaner:** [AOU-1302] manage * at the end of keyword ([2b038d3](https://gitlab.unige.ch/aou/aou-portal/commit/2b038d3e8b512e3f087a067d24f5e94c0da68dfe))
* **keyword:** [AOU-1441] update keyword cleaner ([b8edb08](https://gitlab.unige.ch/aou/aou-portal/commit/b8edb0826afae671142ac95178010f19934679c4))
* lint ([c3893f1](https://gitlab.unige.ch/aou/aou-portal/commit/c3893f11254b32cd9028e00152d840575f96225b))
* lint ([fa7cef1](https://gitlab.unige.ch/aou/aou-portal/commit/fa7cef1916b9e027dc541d484b004e3388290830))
* lint ([ebddf32](https://gitlab.unige.ch/aou/aou-portal/commit/ebddf3249b30938649cf8d5db42566e687d9427a))
* lint ([1c9c462](https://gitlab.unige.ch/aou/aou-portal/commit/1c9c462c2d4066c9c80eee3e4ecadde10f069de8))
* lint ([70d78ab](https://gitlab.unige.ch/aou/aou-portal/commit/70d78abb4c808997fa8634dfb1bddf83101ead52))
* lint ([3d35a6e](https://gitlab.unige.ch/aou/aou-portal/commit/3d35a6e7b906b3313b187aded8b85ffb6b601f62))
* lint ([f3309df](https://gitlab.unige.ch/aou/aou-portal/commit/f3309df889b05e6de95e60d0a86eb2a50c114d9c))
* lint ([f4bd6a9](https://gitlab.unige.ch/aou/aou-portal/commit/f4bd6a936a82005e5d6424a8b55d1f45bac810bc))
* lint ([741c21e](https://gitlab.unige.ch/aou/aou-portal/commit/741c21e72081e63850bea70e0cefc09719c07aed))
* lint error ([519e391](https://gitlab.unige.ch/aou/aou-portal/commit/519e39132b068e75d941720e8dd55c1c84db958d))
* lint error ([4636940](https://gitlab.unige.ch/aou/aou-portal/commit/463694035aca4ae5b0b1f4fcebb9074eb3277c87))
* lint error ([64a2e02](https://gitlab.unige.ch/aou/aou-portal/commit/64a2e02b2f7fe142560ee5b66898fd1c600cf9aa))
* lint error ([c60fb69](https://gitlab.unige.ch/aou/aou-portal/commit/c60fb69a7b58966153a552fea7ca1edcf006fb32))
* lint error unused imports ([0d5a907](https://gitlab.unige.ch/aou/aou-portal/commit/0d5a907e59a54cace8296ebf548361e34b2fc24b))
* lint issues ([d68ce54](https://gitlab.unige.ch/aou/aou-portal/commit/d68ce54d9fc32853bab4953ec061a3b46b20630d))
* loading problem that break design of app ([b3c2181](https://gitlab.unige.ch/aou/aou-portal/commit/b3c21811d84d7c20e97de23173d5759d4d30d368))
* manage toc when string empty in config ([6845ea0](https://gitlab.unige.ch/aou/aou-portal/commit/6845ea0d6d70286d9a483fe2c27fbd4ed00c479e))
* modularity ([462d7d0](https://gitlab.unige.ch/aou/aou-portal/commit/462d7d03f26eb0fb857b7ab34723aaac8bb541f0))
* npm audit vulnerability ([22931f7](https://gitlab.unige.ch/aou/aou-portal/commit/22931f74f82dda53a6d7da876e7c0f9a3f93991b))
* **pinbaord:** display only search of current user ([cceb689](https://gitlab.unige.ch/aou/aou-portal/commit/cceb6899219793a2ecbad2fc21fc3393f0b0bf7e))
* **pinboard:** update default name for storedSearch ([44bdc7e](https://gitlab.unige.ch/aou/aou-portal/commit/44bdc7e3e78a5954e1fdd937becc5fa2760b6b8a))
* preserve advanced search criteria on same alias when open search from pinboard ([034319b](https://gitlab.unige.ch/aou/aou-portal/commit/034319ba5ca2f18c1debb880f7ebe6b568add12c))
* prevent carousel to change of tiles when video is playing ([5c60c42](https://gitlab.unige.ch/aou/aou-portal/commit/5c60c420274ca501802ba41fe872b4a142188490))
* **profile:** [AOU-1465] hide specifics fields for non UNIGE nor HUG members ([496b68b](https://gitlab.unige.ch/aou/aou-portal/commit/496b68b902b9a0feedddc21c87d3e40834c6fe56))
* proxy conf local to copy ([4fd3c89](https://gitlab.unige.ch/aou/aou-portal/commit/4fd3c89ea4f1926d81bbda374970b1c63a1b2d33))
* **publication detail:** preview of file ([cb374aa](https://gitlab.unige.ch/aou/aou-portal/commit/cb374aadf211461a906e2a01f331556eba8191bc))
* **publication search:** add input component for director ([f0fb730](https://gitlab.unige.ch/aou/aou-portal/commit/f0fb730b3adf846479c48c229f90af34eba74910))
* **publication search:** send restricted access master when save or update search ([4efe7f6](https://gitlab.unige.ch/aou/aou-portal/commit/4efe7f63ba946aadb5149a192e2bfbf7bdf0efa2))
* refresh and infinite call on search when year criteria defined ([69e7471](https://gitlab.unige.ch/aou/aou-portal/commit/69e7471725a6f806615971ca2fedb6d03041670e))
* rename index field yearSort into year ([c8b9d8b](https://gitlab.unige.ch/aou/aou-portal/commit/c8b9d8b1dda68fae4c7b9d1d1b3d956dcdaaf91f))
* **research group:** [AOU-1269] hide code in create dialog and fix dark theme in alternative dialog ([f2833ab](https://gitlab.unige.ch/aou/aou-portal/commit/f2833aba80972053be394461ea561647157b1609))
* **research group:** search highlight and hide inactive data on profil and advanced search ([12f1931](https://gitlab.unige.ch/aou/aou-portal/commit/12f193123b9ead6322d54db061694b66f3024e1e))
* routing after angular 14 migration ([6ae14c7](https://gitlab.unige.ch/aou/aou-portal/commit/6ae14c7931e3991d3268af6556c5903d271f2a91))
* scroll to top when open public publication page ([d34a21d](https://gitlab.unige.ch/aou/aou-portal/commit/d34a21df96077c23b128cededbac0b96d18a994e))
* **search:** display all contributor by default when unchecked ([ae9f059](https://gitlab.unige.ch/aou/aou-portal/commit/ae9f0591e8e05e3bdcd68513406f7ddeb266ceac))
* **search:** remove search id when stored search is not created by current user ([122bef3](https://gitlab.unige.ch/aou/aou-portal/commit/122bef3582b74378dfdafdcd119a13ef0e695ac7))
* set correct role when move unige members to collaborators ([7a99271](https://gitlab.unige.ch/aou/aou-portal/commit/7a99271df1bb2ade3382803f468f9b49578f37de))
* set saved value for with restricted access masters when open a stored search ([4d6f9cb](https://gitlab.unige.ch/aou/aou-portal/commit/4d6f9cb0a13e2eda8d0f4d8e089ed66df5b52558))
* show DOI in lists if property exists in database but is ignored from metadata ([3b7dd30](https://gitlab.unige.ch/aou/aou-portal/commit/3b7dd30296919200ac6821316d1fb7abac256788))
* status labels ([ff8b280](https://gitlab.unige.ch/aou/aou-portal/commit/ff8b2806c8cc16db90209fb3fbd8da1983bc7997))
* **structure:** display is obsolete and is historical in searchable tree component ([75bcd77](https://gitlab.unige.ch/aou/aou-portal/commit/75bcd77938aa8243b8f70ff674332de80d30d6f2))
* typing ([e8425c4](https://gitlab.unige.ch/aou/aou-portal/commit/e8425c40de4388bfc46b839f12814648c5e362f9))
* typo ([da4e763](https://gitlab.unige.ch/aou/aou-portal/commit/da4e763b615289aa82416d9030b3034e6ffa042c))
* update copyright date ([c5c327f](https://gitlab.unige.ch/aou/aou-portal/commit/c5c327f2841f7455676ef6ca912372316379e339))
* update labels ([14e2afc](https://gitlab.unige.ch/aou/aou-portal/commit/14e2afc7aed0235c924b129823ab7c104b3c240f))
* upload logo at creation time ([489415d](https://gitlab.unige.ch/aou/aou-portal/commit/489415d9813cb07a9ab262437123bb1ae6fd95b5))
* wrong import ([34ae658](https://gitlab.unige.ch/aou/aou-portal/commit/34ae658b1e13e6b8f54c06c167829949b075d09b))
* wrong nexus link into package lock json ([5c107d3](https://gitlab.unige.ch/aou/aou-portal/commit/5c107d3faadb04b652e357ae244f40096ad69489))

## [1.2.0-alpha1](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.1.0...aou-1.2.0-alpha1) (2022-09-12)


### Features

* [AOU-1116] add an entry in main menu for validation guide ([6cc9c61](https://gitlab.unige.ch/aou/aou-portal/commit/6cc9c61446b6d17bdfb7b2c477e5a421db5a7d51))
* [AOU-847] manage administrator rights ([f270915](https://gitlab.unige.ch/aou/aou-portal/commit/f27091577911c9d55c0af841e873df512eb0efcc))
* [AOU-849] display list of linked deposits on admin research group detail page ([e4d2503](https://gitlab.unige.ch/aou/aou-portal/commit/e4d2503293634739fa50629f45aec8019e179aa0))
* **access:** [AOU-1362] initiate access module with search and published deposit detail ([d876e61](https://gitlab.unige.ch/aou/aou-portal/commit/d876e61fdc15e43829dbfd07905185a0942bfe97))
* **contributor:** [AOU-788] display button to move all unige member into member collaboration ([455bf82](https://gitlab.unige.ch/aou/aou-portal/commit/455bf824d52b2b712f62cab963039578664adbb8))
* **deposit list:** [AOU-1119] display more than one comment when mouse over comment icon ([310d0a0](https://gitlab.unige.ch/aou/aou-portal/commit/310d0a0297260b7219360f7e3c56432a6981db46))
* **deposit list:** [AOU-1314] display number of comment on action button ([3b502f9](https://gitlab.unige.ch/aou/aou-portal/commit/3b502f909c1ed99c3d4e231476c94c7cd7c940b6))
* **document file:** [AOU-867] add not sure for license checkbox ([bbfabea](https://gitlab.unige.ch/aou/aou-portal/commit/bbfabea5f4c74512f27fe1e2462001eb80dce704))
* **home page:** [AOU-1367] update design of home page content ([3d65425](https://gitlab.unige.ch/aou/aou-portal/commit/3d65425de0185ba9183555b4996da68e0817cfa1))
* **home page:** [AOU-1367] update design of home page header ([00ef66c](https://gitlab.unige.ch/aou/aou-portal/commit/00ef66c9060d9c7c87b5ac16c1670128aec0d83a))
* **image:** [AOU-1095] allow non square image ([33eef72](https://gitlab.unige.ch/aou/aou-portal/commit/33eef728d6e30facc663fc244affba2869f4a50e))
* improve maintenance mode ([edc76a8](https://gitlab.unige.ch/aou/aou-portal/commit/edc76a89a0d7f3cd45395f888afffef3b53cbb6b))
* **person search:** [AOU-858] implement pagination ([ef934c0](https://gitlab.unige.ch/aou/aou-portal/commit/ef934c0d2fff3deaa7bb7a072091e514917d3741))


### Bug Fixes

* [AOU-1147] add logical tests in detail page ([dcb8aa9](https://gitlab.unige.ch/aou/aou-portal/commit/dcb8aa9c76ca7b8681d01a8b86394cba9d2fc19c))
* [AOU-1304] fix validators cannot download non public document files ([abf8642](https://gitlab.unige.ch/aou/aou-portal/commit/abf86429eb3461d8408f4d172c072c08dbb548fe))
* [AOU-1321] fix name app tab ([cb8aac5](https://gitlab.unige.ch/aou/aou-portal/commit/cb8aac514a5b9e98c662c39c33cf01ce1d22ac7a))
* [AoU-1322] Delete lang text when cleaning abstract text automatically ([a418cf3](https://gitlab.unige.ch/aou/aou-portal/commit/a418cf30dedec74bb98392ea728d0e228c6679b0))
* [AoU-1332] Bulk import in parallel ([a30ff28](https://gitlab.unige.ch/aou/aou-portal/commit/a30ff287c9e4b40159d49e6101e257f518905503))
* [AoU-1343] Hide Guided tour link in drop-down menu ([06ec4ba](https://gitlab.unige.ch/aou/aou-portal/commit/06ec4ba288d48d513983ac93a52a423edcc1b74f))
* [AoU-1356] Filter undefined document file subtypes ([a353c70](https://gitlab.unige.ch/aou/aou-portal/commit/a353c708c1b8a5876ad698a555841cc9d97bcea9))
* [AoU-1360] Complete error message from import multiple ([37c14d5](https://gitlab.unige.ch/aou/aou-portal/commit/37c14d53ad7ef161f3c6965838e10b0d43d32c55))
* add missing [@import](https://gitlab.unige.ch/import) of mixins ([233c122](https://gitlab.unige.ch/aou/aou-portal/commit/233c122f07fd875cf2acb95cdfb8408e90835fe0))
* add missing translation on home page ([522f795](https://gitlab.unige.ch/aou/aou-portal/commit/522f7953c001802e9a80c636ca8851ed50084f67))
* avoid to send oauth token on toc request ([d75f44d](https://gitlab.unige.ch/aou/aou-portal/commit/d75f44d84c9b2c06edced939abccfadcf42d3f8d))
* detect changes when move unige members to collaborators ([f7f15c4](https://gitlab.unige.ch/aou/aou-portal/commit/f7f15c4452e7ac48fdae15eafc4c858e757f45e1))
* docker entrypoint ([3e98031](https://gitlab.unige.ch/aou/aou-portal/commit/3e98031f0707fc7fc57e5244dd3802a875897890))
* docker entrypoint ([49549b4](https://gitlab.unige.ch/aou/aou-portal/commit/49549b4f5d275e53c7436255b97b707411e9f235))
* email validation ([ef52fcc](https://gitlab.unige.ch/aou/aou-portal/commit/ef52fccf8c8347e2fece26553735eb3069be3dd8))
* icon of validator comment ([f0cb872](https://gitlab.unige.ch/aou/aou-portal/commit/f0cb872711edbac4622bb19bed58e2bebaaa5e74))
* keyword check rule on summary ([2fabbd6](https://gitlab.unige.ch/aou/aou-portal/commit/2fabbd6c54e65a1febff14cbeb46736b15f096cf))
* label ([3a2bda4](https://gitlab.unige.ch/aou/aou-portal/commit/3a2bda4f3e79e8fb69db2ceadf75c17adedfd40e))
* lint errors ([0e3fa0b](https://gitlab.unige.ch/aou/aou-portal/commit/0e3fa0b6129eae4c2d041c2252ada11739c9d396))
* lint issues correction ([441b4a3](https://gitlab.unige.ch/aou/aou-portal/commit/441b4a3514e2074ec48010c168b15a0cf37a2fc0))
* **lint:** remove unused import ([81e7d05](https://gitlab.unige.ch/aou/aou-portal/commit/81e7d053b264237f055a505141b30bf15cc582ab))
* maintenance mode with serveur offline ([aa3c2c8](https://gitlab.unige.ch/aou/aou-portal/commit/aa3c2c88c3023063bb541d2e304ccb9c75023d40))
* nginx conf ([1be2847](https://gitlab.unige.ch/aou/aou-portal/commit/1be28477acb0774aa3359fe706c4077ad678f207))
* **publication:** hide identifier when there is no identifier to displayed ([76d712d](https://gitlab.unige.ch/aou/aou-portal/commit/76d712da2eae29f42ed7b489d5bc31ef36e9607b))
* **service worker:** allow to auto determine basehref and use it to register service worker scope ([abb0b8b](https://gitlab.unige.ch/aou/aou-portal/commit/abb0b8bc8f1652be1d11d36e024ac8008b3afbe4))
* update labels ([4af47f0](https://gitlab.unige.ch/aou/aou-portal/commit/4af47f0aee833803f3125a2550aa51f29c0030fa))

## [1.1.0](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.0.0...aou-1.1.0) (2022-06-10)


### Features

* [AoU-1050] New notification for comments in deposit to the contributors ([b1ae0e8](https://gitlab.unige.ch/aou/aou-portal/commit/b1ae0e8dc178e8874731e9c13c9793f66996cb6a))
* [AOU-1064] display inactive group on summary ([ff860d3](https://gitlab.unige.ch/aou/aou-portal/commit/ff860d3c1afa52b2095d4246138c79735540937a))
* [AOU-1064] treatment of inactive research groups ([1e68b89](https://gitlab.unige.ch/aou/aou-portal/commit/1e68b890d49f9f408a86fd9448864be94ea3b7f0))
* [AOU-1072] allow validators to send back for validation a deposit in status feedback required ([23739ff](https://gitlab.unige.ch/aou/aou-portal/commit/23739ffa7e550c63161d11241646ee6ca89de59a))
* [AoU-1078] New notification by email: deposit_in_progress_for_long_time ([629c0aa](https://gitlab.unige.ch/aou/aou-portal/commit/629c0aa770117b4f8ccaf702bd468ee58a7cdf12))
* [AOU-1080] display error message when duplicate files are detected ([953dcca](https://gitlab.unige.ch/aou/aou-portal/commit/953dccab4cf13711351e4dbeea0b0b45c655dc3c))
* [AOU-1082] add profile in mobile menu ([decc012](https://gitlab.unige.ch/aou/aou-portal/commit/decc012bf73eb17ac79fa7009502404799bd4823))
* [AOU-1112] complete mail to for depositor on deposit detail page ([ebf78f2](https://gitlab.unige.ch/aou/aou-portal/commit/ebf78f268b2905b748bc996d5d0d46ae762bfe2f))
* [AOU-1113] display special banner in case of problem of for a special annoucement ([0de3ccb](https://gitlab.unige.ch/aou/aou-portal/commit/0de3ccb8d1d3e9785aab1951869ee81a674d1afa))
* [AoU-1159] Add comments sections to deposit form steps except the first one ([dc66db8](https://gitlab.unige.ch/aou/aou-portal/commit/dc66db8b9f70be6642252d5c9f36af10c4eb3e00))
* [AoU-1183] Add a size limit for the abstract field ([bfa1c60](https://gitlab.unige.ch/aou/aou-portal/commit/bfa1c6089b995dc1a67172a6353347fb82e11d4a))
* [AOU-1194] migrate to solidify 3.1 and angular 13.2.1 ([19250d0](https://gitlab.unige.ch/aou/aou-portal/commit/19250d048c4c5821ce263be55cdd9758fcbee0f5))
* [AOU-1217] autoscroll to first validation error ([ebe0d5f](https://gitlab.unige.ch/aou/aou-portal/commit/ebe0d5fb4deca9bbf5dd1297f28dbf2262295398))
* [AoU-1229] Clone deposit ([d6c76eb](https://gitlab.unige.ch/aou/aou-portal/commit/d6c76eb03bcfff5b0741f609a130a124701c9080))
* [AOU-1284] allow to navigate to field from error and detail page ([d137542](https://gitlab.unige.ch/aou/aou-portal/commit/d1375422394f740b5fe26ebcc38bcfbd36222df6))
* [AOU-489] use rich text editor for title and abstract ([686d027](https://gitlab.unige.ch/aou/aou-portal/commit/686d0276f0e545d3f7ba122a8cb68950e155c247))
* [AOU-856] display words counter below abstract textarea ([e414eea](https://gitlab.unige.ch/aou/aou-portal/commit/e414eea0290e2b99dcd24413495af28082221f67))
* [AoU-874] add my name button in contributors step ([644f06a](https://gitlab.unige.ch/aou/aou-portal/commit/644f06afd30bca71a179f131ae19890f78954596))
* [AOU-903] create edition shortcut for validators from detail page ([1286ec5](https://gitlab.unige.ch/aou/aou-portal/commit/1286ec52b2c2f078d0d1f4e338d75d5882c6e2de))
* [AOU-956] Add Imprimatur and Diffusion mode document types applying same access level as agreement ([b13ad6c](https://gitlab.unige.ch/aou/aou-portal/commit/b13ad6c5d617f8db95e4f100a28e138121faf178))
* **banner:** [AOU-1052] display title of deposit ([28ea19e](https://gitlab.unige.ch/aou/aou-portal/commit/28ea19eabc58d67ea84818a69cb142290790b625))
* **comment:** [AOU-1120] display differently internal comment ([1d9ca9e](https://gitlab.unige.ch/aou/aou-portal/commit/1d9ca9e46c59bcce29ca5816fa9896046ae04093))
* deposit - contributor ([24a60d1](https://gitlab.unige.ch/aou/aou-portal/commit/24a60d16cde9143b4dfd3106a9722d6b387cb45c))
* **deposit contributor:** [AOU-1014] display dialog when no structure selected ([643d89e](https://gitlab.unige.ch/aou/aou-portal/commit/643d89eb2a39004d11b1d8ae4c09155c2c1f8522))
* **deposit dates:** [AOU-1037] display label section conditionnaly depending of date displayed ([282454b](https://gitlab.unige.ch/aou/aou-portal/commit/282454b386cbc3fa9965a7caeaf28d12de223691))
* **deposit description:** [AOU-1070] expand main info by default for validator ([fd27b18](https://gitlab.unige.ch/aou/aou-portal/commit/fd27b1867f7f7be7d980b6bba67572419a13268f))
* **deposit description:** [AOU-1154] add validation on corrigendum ([6119328](https://gitlab.unige.ch/aou/aou-portal/commit/611932805fdb626136aa48f5eef7dac51116c7fc))
* **deposit detail:** [AOU-1007] make doi, pmid and pmcid clickable ([14db980](https://gitlab.unige.ch/aou/aou-portal/commit/14db98066d1f5678f6ef118fdad28474966d959e))
* **deposit detail:** [AOU-1051] add mailto if create mail defined ([5eba0ec](https://gitlab.unige.ch/aou/aou-portal/commit/5eba0eca5eb29f775b4161c9812ba5070b9c949d))
* **deposit detail:** [AOU-1051] display creator ([38a7e92](https://gitlab.unige.ch/aou/aou-portal/commit/38a7e924613a3cf6adc90e97f523af611058e963))
* **deposit errors:** [AOU-940] avoid to display technical path on validation error ([cd744fe](https://gitlab.unige.ch/aou/aou-portal/commit/cd744feae403902e484d866bb720d9f1cea4b441))
* **deposit file:** [AOU-1097] hide additional description by default ([8223e9d](https://gitlab.unige.ch/aou/aou-portal/commit/8223e9d9564f49a919a528945263afb041774787))
* **deposit identifier:** [AOU-1124] display navigation button on identifier ([189b49f](https://gitlab.unige.ch/aou/aou-portal/commit/189b49f77e699ddc72e2e44ee3ce0bbba4ae4f70))
* **deposit language:** [AOU-1199] use multi select for publication language ([de6220d](https://gitlab.unige.ch/aou/aou-portal/commit/de6220df8920f21c9bc339a8263f6944b6cf47c2))
* **deposit lists:** [AOU-1073] add lastStatusUpdate in deposits lists ([87e41c8](https://gitlab.unige.ch/aou/aou-portal/commit/87e41c88611ee46f0edab553f222c7201191c9d4))
* **deposit thesis:** [AOU-1132] check files present in deposit for type thesis ([5ee5721](https://gitlab.unige.ch/aou/aou-portal/commit/5ee57219a67c25cdb94ec9363d444877d68aeb48))
* **deposit:** [AOU-1086] allow access to any deposit to a user that has a direct link ([4a6cdda](https://gitlab.unige.ch/aou/aou-portal/commit/4a6cdda2e9a3b6910b1275dd7f9e0507e6e3cb65))
* **deposit:** [AOU-1188] bulk update for language ([3e9e24c](https://gitlab.unige.ch/aou/aou-portal/commit/3e9e24c6e32a84b99b9927bc0edb5249d0399b7b))
* **deposit:** [AOU-1202] allow to write original title a creation time ([2150ce4](https://gitlab.unige.ch/aou/aou-portal/commit/2150ce4591f524570fbe090ae2f60211d7fc115c))
* **deposit:** [AOU-1283] define html tags allowed on title and description ([b3c1b6b](https://gitlab.unige.ch/aou/aou-portal/commit/b3c1b6b7dc83a5fa97b43180f60901e737a63034))
* **DepositFormRuleHelper:** [AOU-1122] activate 'Corrigendum' section in form for 'Chapitre de livre' and 'Contribution à un dictionnaire' ([182bef5](https://gitlab.unige.ch/aou/aou-portal/commit/182bef5e6e8f5e641bcb25e47947d37a5617cca5))
* **multi import:** [AOU-1081] improve error message for multiple imports ([fd9ced9](https://gitlab.unige.ch/aou/aou-portal/commit/fd9ced984c2bd71ba3e953da43b3973fb1ac3622))
* new EventType + complete/fix translations ([1a1296a](https://gitlab.unige.ch/aou/aou-portal/commit/1a1296afc569aed0d76e4e3b28e622b320386ebc))
* **notification:** [AoU-907] Add button to mark notification as unread and mark as read when clicking into the notification detail ([87a92b0](https://gitlab.unige.ch/aou/aou-portal/commit/87a92b0f3f12a280e65a11ccf40388419e0d6b6c))
* **notifications:** [AOU-1063] add filter and sort ([48be67f](https://gitlab.unige.ch/aou/aou-portal/commit/48be67f501c277ce05891662838ed45403a28205))
* **profil:** [AOU-784] force user to fill at least one structure at startup if he is from unige ([0aa70f0](https://gitlab.unige.ch/aou/aou-portal/commit/0aa70f06f62b40814b44fea9087beb2b3581ecbf))
* **search:** [AOU-864-976] improve structures and research groups search ([856e600](https://gitlab.unige.ch/aou/aou-portal/commit/856e600ad23ff4eb627b0c9601c974c62a3d26cd))
* **structure:** [AOU-1133] searchable multi select tree component for structure ([ea3064e](https://gitlab.unige.ch/aou/aou-portal/commit/ea3064eed6326282c81129241b319f6a3c2ab4fe))


### Bug Fixes

* [AOU-1027] mandatory fields for thesis not tested ([8e7a83f](https://gitlab.unige.ch/aou/aou-portal/commit/8e7a83f83d5778fc4f3e2ecef5c19a46411d67d4))
* [AOU-1034] move language label to right corner and automatically expand section when no language is selected ([f431d42](https://gitlab.unige.ch/aou/aou-portal/commit/f431d421a5c6700ca2e21db598a92d5878b4b35e))
* [Aou-1048] Report error in user profile when selecting a structure parent... ([49934e6](https://gitlab.unige.ch/aou/aou-portal/commit/49934e69d4f17c1bc0654a50881b57fd6fd166aa))
* [AOU-1065] sticky header on deposit list ([867dc8f](https://gitlab.unige.ch/aou/aou-portal/commit/867dc8fcabd360af48faa89171e774d075ef92a4))
* [AoU-1085] Search by name in openAire ([79d9a99](https://gitlab.unige.ch/aou/aou-portal/commit/79d9a999ac9ecf89636cd0cb0f29b6cd2c9436ad))
* [AOU-1100] button add me as author should set me as unige member if it's the case ([88e876c](https://gitlab.unige.ch/aou/aou-portal/commit/88e876cb54e34a717f365a0a3f39897f7f830fef))
* [AOU-1111] hide profile link on mobile menu if not connected ([5bc2577](https://gitlab.unige.ch/aou/aou-portal/commit/5bc2577cff7f5bb0a234b7e73f5a318a80ff3bd7))
* [AOU-1139] avoid to call notification endpoint without token ([51028db](https://gitlab.unige.ch/aou/aou-portal/commit/51028dbe463534efadf068e0874acf9ce3d99966))
* [AOU-1161] avoid display validation error from an other deposit ([6befebb](https://gitlab.unige.ch/aou/aou-portal/commit/6befebb4adfca3a3f918d1b864635c2ddcd729fb))
* [AOU-1171] avoid to send token on toc resource ([7939e89](https://gitlab.unige.ch/aou/aou-portal/commit/7939e89b02418aa262ad4c8f02687c4ff3cbe653))
* [AOU-1174] disable approve button once it is clicked to avoid multiple submissions ([a87931a](https://gitlab.unige.ch/aou/aou-portal/commit/a87931a55116a235bf3160cfdbbdc485e415aadc))
* [AoU-1212] Add missing notification into profile: forgotten_publications_to_validate ([0f7a00b](https://gitlab.unige.ch/aou/aou-portal/commit/0f7a00b030e7f697ce3c02dff500333bf03fc773))
* [AOU-1233] manage redirect after login to any route ([5830c86](https://gitlab.unige.ch/aou/aou-portal/commit/5830c866b5f686b448f6bd82547f54613d35e57a))
* [AOU-1234] do no trigger popup warning pdf coming from import when create deposit manually ([b383d0e](https://gitlab.unige.ch/aou/aou-portal/commit/b383d0e63edb99b449dcd9becd24b16c5cad8f1a))
* [AOU-1238] update deposit validation guard ([920e9ef](https://gitlab.unige.ch/aou/aou-portal/commit/920e9ef3afea2c12d19b8868530cd6e46b9ae450))
* [AOU-1241] remove debugging code ([b3c2c7b](https://gitlab.unige.ch/aou/aou-portal/commit/b3c2c7b2497998c546fb299442e45aab57db68af))
* [AoU-1248] Improve clone feature ([1425e0d](https://gitlab.unige.ch/aou/aou-portal/commit/1425e0d0199b72a102474754232b7d43d6d5833f))
* [AOU-1256] preserve status isBeforeUnige and allow to send in validation a thesis without imprimatur if it is before unige ([facd4f5](https://gitlab.unige.ch/aou/aou-portal/commit/facd4f5f71d9b5867189a1544b276a47c1d73be4))
* [AoU-1259] Abstract not mandatory for Master's publication types ([c2dd270](https://gitlab.unige.ch/aou/aou-portal/commit/c2dd2708812755b3fa2ddc0dc379ea2160bb0e60))
* [AoU-1268] Allow empty licence for files ([a855ace](https://gitlab.unige.ch/aou/aou-portal/commit/a855ace30f458801c7988ddfa3452c8769c73627))
* [AOU-1274] display backend modules on about page ([137c535](https://gitlab.unige.ch/aou/aou-portal/commit/137c535c2502f1c4082e0571a3536b3699a7e429))
* [AoU-1290] ReOrder notification list in profile dialog ([daccf23](https://gitlab.unige.ch/aou/aou-portal/commit/daccf2391ad3720a0535a4ea58277472c5e28a5f))
* [AOU-968] profile validation rules are not all displayed ([f762eb3](https://gitlab.unige.ch/aou/aou-portal/commit/f762eb3ed1edcd4a047c262109e9a2df25c0857f))
* [AOU-986] do not apply validation rules on completed deposits ([0d2b21c](https://gitlab.unige.ch/aou/aou-portal/commit/0d2b21c514c76b087545f35f2f45053f9df8cf70))
* [AOU-989] hide dewey code for validator ([64bc0ac](https://gitlab.unige.ch/aou/aou-portal/commit/64bc0ac74941096e84aecd0a1c93ea5002707128))
* [AOU-991] hide user guid from menu if not defined ([db5e944](https://gitlab.unige.ch/aou/aou-portal/commit/db5e944e10d1be1fabd7a92853b7337d705af786))
* **about page:** add theme name ([a76d4d6](https://gitlab.unige.ch/aou/aou-portal/commit/a76d4d6e905280de7d036a4a0c25911c93681ecf))
* allow to edit user profile twice time ([f5a64ba](https://gitlab.unige.ch/aou/aou-portal/commit/f5a64ba350a61b8c540e61d5c26047ac4460ec18))
* avatar avoid 404 when we know that there is no avatar ([a9278b2](https://gitlab.unige.ch/aou/aou-portal/commit/a9278b281f7858afda0a11777f32f5a56acf2cb0))
* avoid to display pdf imported on validator deposit interface ([063fb23](https://gitlab.unige.ch/aou/aou-portal/commit/063fb23a9cdbecf4ac855806a4fdea4e65370f20))
* correct the authorization URL in proxy.conf.js ([f34b7b5](https://gitlab.unige.ch/aou/aou-portal/commit/f34b7b5304dd18595d50aafe4c013b2a13d51bb1))
* **deposit and notification:** [AOU-1285] shorten deposit title in list ([423d3f7](https://gitlab.unige.ch/aou/aou-portal/commit/423d3f79e19d886c2416fa859bf38352317ede60))
* **deposit contributor step:** [AOU-982] duplicated academic structure when autocomplete ([71ae256](https://gitlab.unige.ch/aou/aou-portal/commit/71ae25683a7eb526f519e71688e931eb334e8ac4))
* **deposit contributor:** [AOU-1079] remove default value for validator on structure and research group ([454b686](https://gitlab.unige.ch/aou/aou-portal/commit/454b686a2ace20a840818cc876add441b7e05ba4))
* **deposit contributor:** [AoU-1278] sse previous control for collaborations if it was empty ([51c8df0](https://gitlab.unige.ch/aou/aou-portal/commit/51c8df05cd29a32aac1530d4b379a64522dcbbf3))
* **deposit corrigendum:** [AOU-1262] pmid and doi in corrigendum are not clickable ([b0a0f27](https://gitlab.unige.ch/aou/aou-portal/commit/b0a0f27ae9cb614a45a523b2f8c9e8f3de6392ae))
* **deposit description:** [AOU-1033] action button to clean page hidden behind another field ([29d59b5](https://gitlab.unige.ch/aou/aou-portal/commit/29d59b58ba660decb0efae8b45552780f7a4a7e3))
* **deposit description:** [AoU-1141] hide DOI dataset url and commercial url for subtypes related with thesis ([5099ba0](https://gitlab.unige.ch/aou/aou-portal/commit/5099ba04a4518c73afbae4cfa84118e2fcf6e417))
* **deposit detail:** [AOU-985] display research group's code when not empty ([ac77ff7](https://gitlab.unige.ch/aou/aou-portal/commit/ac77ff79f03a34fe83be0b5d9d01a1bb558c09ef))
* **deposit file:** [AOU-1011] avoid to lose license version when edit ([224d35e](https://gitlab.unige.ch/aou/aou-portal/commit/224d35e6597d9a8855927fd1f2aff9b7989b4258))
* **deposit file:** [AOU-1288] imprimatur of thesis must be always public ([8d4aa40](https://gitlab.unige.ch/aou/aou-portal/commit/8d4aa4050b876314e19955525acf9e4cb73570ad))
* **deposit form:** [AOU-1023] rename date for posters and presentation ([11e1e2d](https://gitlab.unige.ch/aou/aou-portal/commit/11e1e2d486582da227c28acb1e34c5fb83a5727d))
* **deposit list:** [AOU-1015] replace update date with create date ([5db5c15](https://gitlab.unige.ch/aou/aou-portal/commit/5db5c1539d8f074b9f8f7401229d0f75d1dd6f41))
* **deposit list:** [AOU-1282] display validation structure in italic ([ef5965f](https://gitlab.unige.ch/aou/aou-portal/commit/ef5965ffe917e0d4963221dbf16ee6ca722a7961))
* **deposit list:** [AOU-983] preserve query paremeters when back to detail and set default pagination to 50 ([2bb3183](https://gitlab.unige.ch/aou/aou-portal/commit/2bb3183c078fcf76c31606e25259cd2b8032f692))
* **deposit list:** [AOU-995] disallow the bulk action button send to validation when one without file ([9624fdf](https://gitlab.unige.ch/aou/aou-portal/commit/9624fdf8d8325c9eafc308f093b138aedfee1dbe))
* **deposit types:** [AOU-1286] use sortValue to order types, subtypes and sub-subtypes ([d1f5b6d](https://gitlab.unige.ch/aou/aou-portal/commit/d1f5b6d375d191b27850996d39e23df3c299f1d1))
* **deposit validation:** [AOU-1055] avoid to allow to enter in edit mode when status is not authorized ([9601973](https://gitlab.unige.ch/aou/aou-portal/commit/96019735c370be912cf8d2c13fd5b8cad9d633a6))
* **deposit validation:** prevent to navigate on edition mode when click on abstract link ([b629e19](https://gitlab.unige.ch/aou/aou-portal/commit/b629e19b968f92b1c6d187572d5842824f7206cf))
* **deposit validator:** [AOU-1091] allow to change language on summary without redirect to edit mode ([09e372a](https://gitlab.unige.ch/aou/aou-portal/commit/09e372aa3fd43738c4c9c0ff47d1c26b5f9cb643))
* **deposit:** [AOU-1261] allow to redirect to duplicated deposit ([67aa154](https://gitlab.unige.ch/aou/aou-portal/commit/67aa154b625dfc843963d45a366a329797f8944b))
* **deposit:** [AOU-1263] avoid popup automatically imported PDF on cloned deposit ([e9efdf2](https://gitlab.unige.ch/aou/aou-portal/commit/e9efdf2f63149c435e2e8fae617074c3656bca12))
* **deposit:** [AOU-993] disallow send in validation when missing file ([3f198ae](https://gitlab.unige.ch/aou/aou-portal/commit/3f198ae417135f785b4b6fd6908c01a616c657e7))
* **DepositDocumentFileUploadDialog:** [AoU-1145] Do not set licence until there is a value ([a1422b6](https://gitlab.unige.ch/aou/aou-portal/commit/a1422b694ecda8c65b89f7f728dfc3350bcf5984))
* **deposit:** language field on deposit creation ([aad77b6](https://gitlab.unige.ch/aou/aou-portal/commit/aad77b6e058b4294173a375d86e0d7645c6e1b4b))
* **DepositStructureState:** send Create before Delete request when updating relation to prevent validators loosing rights too early when transfering a deposit ([e701133](https://gitlab.unige.ch/aou/aou-portal/commit/e701133302cdcd5378597614b5bd7c3eb4449c16))
* display inner html for deposit title on notification list ([fcd0e6f](https://gitlab.unige.ch/aou/aou-portal/commit/fcd0e6fb7f4b873f59f1d9f26be7cef9619ab20a))
* **en.json:** improve translations ([4fd4a00](https://gitlab.unige.ch/aou/aou-portal/commit/4fd4a008136b68eb9317c94a267407f8b75d2f39))
* **en.json:** improve translations ([cf1eca0](https://gitlab.unige.ch/aou/aou-portal/commit/cf1eca0066dfe5a659342e3ad759d47bee1e98fe))
* english corrections ([a9d14ec](https://gitlab.unige.ch/aou/aou-portal/commit/a9d14ec27e2255a3428b50ad5ac6fb779ac09122))
* fix lint errors ([42ff0e9](https://gitlab.unige.ch/aou/aou-portal/commit/42ff0e9e16c7040d77136204fe34bc8aa3255bc5))
* **fr.json:** typo ([0496b83](https://gitlab.unige.ch/aou/aou-portal/commit/0496b8381900165899db7411434af9dba3514c46))
* **fr.json:** typo ([e5549a4](https://gitlab.unige.ch/aou/aou-portal/commit/e5549a4d2a5dc1f3f8ebfe0d94cde6309051c1a5))
* **fr.json:** typo ([d7e4833](https://gitlab.unige.ch/aou/aou-portal/commit/d7e4833339b47ef25efe419e5212c632e19964ae))
* **funding:** [AOU-1012] problem with fundings remove mandatory attribute for a field ([ea9ac4a](https://gitlab.unige.ch/aou/aou-portal/commit/ea9ac4a7103aef343fbd3270887eeff9da9c74dd))
* git ignore ([36b7694](https://gitlab.unige.ch/aou/aou-portal/commit/36b7694cd9a30bbf6be50372013a2cdf1dc11d62))
* git ignore ([fd3c45b](https://gitlab.unige.ch/aou/aou-portal/commit/fd3c45b2a96303a4fe631a4573f5342c6330eed9))
* **home page:** [AOU-990] guid tour is blocking for normal user ([721feda](https://gitlab.unige.ch/aou/aou-portal/commit/721feda833d98c1d10690116361f41672d0f6ee6))
* improve display of structure overlay input ([0b1e26c](https://gitlab.unige.ch/aou/aou-portal/commit/0b1e26c1645d0b97aba781469416ae2c2521fa6a))
* improve rich text toolbar ([40839ff](https://gitlab.unige.ch/aou/aou-portal/commit/40839ffa155d8fa1160e27536052bed0700f83a7))
* intergrity sha for solidify ([89b36b3](https://gitlab.unige.ch/aou/aou-portal/commit/89b36b393a532cfb3e00f89e5001c9029f6f436c))
* **keyword:** [AOU-1069] update to solidify 1.8.11 to fix separator comma with wrong label ([3ba2935](https://gitlab.unige.ch/aou/aou-portal/commit/3ba2935ac21197e649bc2363804152a2f7a6b588))
* label ([1a7f9fb](https://gitlab.unige.ch/aou/aou-portal/commit/1a7f9fb2d9c925a4698ef845cb71d2fd9c96b005))
* label ([66ed4b5](https://gitlab.unige.ch/aou/aou-portal/commit/66ed4b5741e8fab59d805b05f8660988ae02fc33))
* label en ([5188df4](https://gitlab.unige.ch/aou/aou-portal/commit/5188df441111e514c1c475b7efd45d97c268dc41))
* label fr ([e428ad6](https://gitlab.unige.ch/aou/aou-portal/commit/e428ad66696dec51ddea719ef0f8d89497f5cd8c))
* labels ([f1dd775](https://gitlab.unige.ch/aou/aou-portal/commit/f1dd7756cb1a57c901f26e582b91855a86f8e8e8))
* lint error ([6e14a50](https://gitlab.unige.ch/aou/aou-portal/commit/6e14a501778230b181178f3e6b4c5b6f4e15cd75))
* lint error missing copyright ([30b5d0b](https://gitlab.unige.ch/aou/aou-portal/commit/30b5d0b7d4f99ff3eb7556470858ec3958946df6))
* **list:** [AOU-1068] display always more option action button ([6e4e41d](https://gitlab.unige.ch/aou/aou-portal/commit/6e4e41ddd12501d893ca771718c31a6fe6d32449))
* make the rich text clickable on detail mode ([77a108e](https://gitlab.unige.ch/aou/aou-portal/commit/77a108e402f76926b8c49244cdfe3478b09d288a))
* output value of avatar upload component ([f70446a](https://gitlab.unige.ch/aou/aou-portal/commit/f70446ab3ac609b319eae4def574bb0311c13b26))
* **profile:** [AOU-1005] notification hidden when select last value ([fc3cca9](https://gitlab.unige.ch/aou/aou-portal/commit/fc3cca98b1eb9236e7153ee1a5ec3196356bc628))
* remove notification error message already displayed by solidify error handler ([e1e87a1](https://gitlab.unige.ch/aou/aou-portal/commit/e1e87a1e93a940fd136b486828e0b224562ecfd4))
* Remove unused current selector since it is used MemoizedUtil.currentSnapshot instead ([2400f23](https://gitlab.unige.ch/aou/aou-portal/commit/2400f2328b2ab82393afc31d51e006d124202ae7))
* remove useless separator ([9f5962a](https://gitlab.unige.ch/aou/aou-portal/commit/9f5962a56debd2c7ea8eb2b1ac22fe66b970d943))
* **research group:** remove duplicate column ([ecbfbe0](https://gitlab.unige.ch/aou/aou-portal/commit/ecbfbe00f7a98c837fa0c8170a85b07b5c49b6b9))
* **rich text:** prevent link into title input ([8d2c06f](https://gitlab.unige.ch/aou/aou-portal/commit/8d2c06f4eb6e46161fb1f98465978dfbe8134295))
* scroll behavior ([a43bc32](https://gitlab.unige.ch/aou/aou-portal/commit/a43bc32554a175174acc1020ac8a2428d38904bd))
* slight corrections ([bf4e180](https://gitlab.unige.ch/aou/aou-portal/commit/bf4e18036d5b2219d7ca7e43d741a0e545d03e02))
* template with angularCompilerOptions strictTemplates option ([f3bce7c](https://gitlab.unige.ch/aou/aou-portal/commit/f3bce7c96de24ba892fe5967f37ed0103a65bc41))
* typo on output after avatar upload ([1353012](https://gitlab.unige.ch/aou/aou-portal/commit/13530124cf62b228005aee3dc5a56017fa8f6363))
* update labels ([107040e](https://gitlab.unige.ch/aou/aou-portal/commit/107040ef5f3747e6876f62e2e97ba06eb75fb994))
* update translation ([8aa4bba](https://gitlab.unige.ch/aou/aou-portal/commit/8aa4bba38186f12f2d4621fb7ba0f4e0a9004b22))
* **upload file:** hide license version when no license group selected and pre select undefined version when license group selected ([79d02ce](https://gitlab.unige.ch/aou/aou-portal/commit/79d02cee6a5b647bf60ff4884bdbf5a45058936d))
* use 'Publication mode' value instead of 'Diffusion mode' ([339cdb4](https://gitlab.unige.ch/aou/aou-portal/commit/339cdb49b658ff52f6d314ea52859bd1043145d7))
* user menu ([609e0e5](https://gitlab.unige.ch/aou/aou-portal/commit/609e0e57638658c3d71e6fde0abdddcd8184ec2a))
* **user profile:** avoid error when there is no picture and migrate avatar and images components ([8939906](https://gitlab.unige.ch/aou/aou-portal/commit/893990682a84898e7121c9206a510203c4ca0bf7))
* **validation rights:** [AOU-1019] slow interface when lot of validation right in profile or admin person detail ([31c86da](https://gitlab.unige.ch/aou/aou-portal/commit/31c86da6c9bbbaf21f3ffcee2be55c5997d39e5d))
* warning typing for abstract control ([5d98259](https://gitlab.unige.ch/aou/aou-portal/commit/5d982599a368ae712105c63152ae8308a0e4a533))

## [1.0.5](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.0.4...aou-1.0.5) (2022-01-17)

### Features

* [AOU-1064] display inactive group on summary ([c9b6051](https://gitlab.unige.ch/aou/aou-portal/commit/c9b6051fa05070f85ad44d43323f129009dffa49))

### Bug Fixes

* use 'Publication mode' value instead of 'Diffusion mode' ([d696667](https://gitlab.unige.ch/aou/aou-portal/commit/d69666774bdf5fb9ef7a178fd9ff37c6b76019b2))

## [1.0.4](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.0.3...aou-1.0.4) (2022-01-03)

### Features

* [AOU-1064] treatment of inactive research groups ([469ae7a](https://gitlab.unige.ch/aou/aou-portal/commit/469ae7a5f3fe62d3af64647f29b59b8aff158af4))
* [AOU-1072] allow validators to send back for validation a deposit in status feedback
  required ([43f14ed](https://gitlab.unige.ch/aou/aou-portal/commit/43f14ed119698a5fa8cd88f9931e6cdfd835d071))
* [AOU-1112] complete mail to for depositor on deposit detail
  page ([7dc8490](https://gitlab.unige.ch/aou/aou-portal/commit/7dc8490c470f6c9145b246482e71e2df1a35a2c8))
* [AOU-956] Add Imprimatur and Diffusion mode document types applying same access level as
  agreement ([33b759e](https://gitlab.unige.ch/aou/aou-portal/commit/33b759ed29bb06f35b3bc7c9a6db428e8299d167))

### Bug Fixes

* [Aou-1048] Report error in user profile when selecting a structure
  parent... ([19a5e3f](https://gitlab.unige.ch/aou/aou-portal/commit/19a5e3fb527ae41c9d8806f0dede99140b89318e))
* [AOU-1100] button add me as author should set me as unige member if it's the
  case ([c52e0fa](https://gitlab.unige.ch/aou/aou-portal/commit/c52e0fa9dda57941571473064edaafb900f11c29))
* [AOU-1111] hide profile link on mobile menu if not
  connected ([2c0e7bf](https://gitlab.unige.ch/aou/aou-portal/commit/2c0e7bf8c3b71620c1db8fb1ddc8e26871479d74))
* [AOU-986] do not apply validation rules on completed
  deposits ([c136d4e](https://gitlab.unige.ch/aou/aou-portal/commit/c136d4e549cd1175154542ba846431b7bc3e901f))
* [AOU-989] hide dewey code for validator ([e735c7e](https://gitlab.unige.ch/aou/aou-portal/commit/e735c7e2d3f9778bcd43845206213550db57acb9))
* update translation ([c1750d5](https://gitlab.unige.ch/aou/aou-portal/commit/c1750d5844ed516901ad5e0a9d5a227844953353))

## [1.0.3](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.0.2...aou-1.0.3) (2021-11-23)

### Features

* [AOU-1080] display error message when duplicate files are
  detected ([2dd867c](https://gitlab.unige.ch/aou/aou-portal/commit/2dd867c627d432b40a0c211864d155004e831f06))
* [AOU-1082] add profile in mobile menu ([d952b10](https://gitlab.unige.ch/aou/aou-portal/commit/d952b10f0c89ef3bae5f7326a55d65a5cc7b3d45))
* [AoU-874] add my name button in contributors step ([71c378e](https://gitlab.unige.ch/aou/aou-portal/commit/71c378e33c2c2bf822d7249bceb8056c24770f7f))
* **banner:** [AOU-1052] display title of deposit ([3d8505f](https://gitlab.unige.ch/aou/aou-portal/commit/3d8505f92e04ef2471c8d15e9285362764689889))
* **deposit description:** [AOU-1070] expand main info by default for
  validator ([30e206b](https://gitlab.unige.ch/aou/aou-portal/commit/30e206b83c057247dde2fd5f46ab3eead0c4a3b6))
* **deposit detail:** [AOU-1051] add mailto if create mail
  defined ([73875d8](https://gitlab.unige.ch/aou/aou-portal/commit/73875d825459550956bd6a9b121c353a754aa405))
* **deposit detail:** [AOU-1051] display creator ([670a74a](https://gitlab.unige.ch/aou/aou-portal/commit/670a74a0f55671c7aaf113a0c006e2a68153c548))
* **multi import:** [AOU-1081] improve error message for multiple
  imports ([d75245d](https://gitlab.unige.ch/aou/aou-portal/commit/d75245d5621346c1a110ff49205b78d833f381fa))
* **profil:** [AOU-784] force user to fill at least one structure at startup if he is from
  unige ([6f84716](https://gitlab.unige.ch/aou/aou-portal/commit/6f847164c7701f4423cf415bdaafcb5868d0fe58))

### Bug Fixes

* [AOU-1065] sticky header on deposit list ([0495489](https://gitlab.unige.ch/aou/aou-portal/commit/0495489d46cc851aeff779bbb1b66dcafba9a376))
* avatar avoid 404 when we know that there is no avatar ([eb03aa5](https://gitlab.unige.ch/aou/aou-portal/commit/eb03aa5db9c38ac9c4c7357a724f39faec674b92))
* **deposit contributor:** [AOU-1079] remove default value for validator on structure and research
  group ([1fd318c](https://gitlab.unige.ch/aou/aou-portal/commit/1fd318c132d46184e5245aa560f9f176063df54f))
* **deposit validator:** [AOU-1091] allow to change language on summary without redirect to edit
  mode ([49440f4](https://gitlab.unige.ch/aou/aou-portal/commit/49440f4278172ced79c29d6bdb3b1317b2d9b187))
* **keyword:** [AOU-1069] update to solidify 1.8.11 to fix separator comma with wrong
  label ([a7a10ce](https://gitlab.unige.ch/aou/aou-portal/commit/a7a10ce1dded76958b5be0a46d51e08d09b9599a))
* **list:** [AOU-1068] display always more option action
  button ([477bb9a](https://gitlab.unige.ch/aou/aou-portal/commit/477bb9ad49a3bacc35a9a6024c209b81cda1e25f))

## [1.0.2](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.0.1...aou-1.0.2) (2021-10-20)

### Features

* [AOU-903] create edition shortcut for validators from detail
  page ([9a21543](https://gitlab.unige.ch/aou/aou-portal/commit/9a21543e8a14097e6a234b47be508fbe26166102))
* **deposit dates:** [AOU-1037] display label section conditionnaly depending of date
  displayed ([f4ca3f6](https://gitlab.unige.ch/aou/aou-portal/commit/f4ca3f6f4534874652f97c1e3dba7ff6515db99f))
* **deposit list:** [AOU-1035] shorten title ([4d644ce](https://gitlab.unige.ch/aou/aou-portal/commit/4d644ceab35310d38ead34ce5aef7f278bd14cce))

### Bug Fixes

* [AOU-1027] mandatory fields for thesis not tested ([0cf4bf2](https://gitlab.unige.ch/aou/aou-portal/commit/0cf4bf2429bc18677aa22a31768888c95c06f4ed))
* [AOU-1034] move language label to right corner and automatically expand section when no language is
  selected ([b8d6693](https://gitlab.unige.ch/aou/aou-portal/commit/b8d6693422283d2e4b38ee780ba0c4b3ec940fc5))
* **deposit description:** [AOU-1033] action button to clean page hidden behind another
  field ([4e5522a](https://gitlab.unige.ch/aou/aou-portal/commit/4e5522abad7d61c388c80d407a83e265b9f5c650))
* **deposit form:** [AOU-1023] rename date for posters and
  presentation ([63b6a0b](https://gitlab.unige.ch/aou/aou-portal/commit/63b6a0be970c36ca4cfbb0721fd8989450b5d8c9))
* **funding:** [AOU-1012] problem with fundings remove mandatory attribute for a
  field ([1021a90](https://gitlab.unige.ch/aou/aou-portal/commit/1021a907c3c7d09ecdbf03a7fca5fed686e9b158))
* **validation rights:** [AOU-1019] slow interface when lot of validation right in profile or admin person
  detail ([bfe2b41](https://gitlab.unige.ch/aou/aou-portal/commit/bfe2b410f556ab122060f7dfead083144b5ee67c))

## [1.0.1](https://gitlab.unige.ch/aou/aou-portal/compare/aou-1.0.0...aou-1.0.1) (2021-10-05)

### Features

* [AOU-856] display words counter below abstract textarea ([2a1e456](https://gitlab.unige.ch/aou/aou-portal/commit/2a1e456f7b834c9d27506e1662e4cca228605c8b))
* **deposit contributor:** [AOU-1014] display dialog when no structure
  selected ([9f575bf](https://gitlab.unige.ch/aou/aou-portal/commit/9f575bf4d0b4abccdacf0d73e9ab506df465705d))
* **deposit detail:** [AOU-1007] make doi, pmid and pmcid
  clickable ([edf4a5c](https://gitlab.unige.ch/aou/aou-portal/commit/edf4a5ce51c0711e30945541761abe118d5f49a8))
* **search:** [AOU-864-976] improve structures and research groups
  search ([a613ce9](https://gitlab.unige.ch/aou/aou-portal/commit/a613ce9850abffbd05a9641a3632baadbc895225))

### Bug Fixes

* [AOU-968] profile validation rules are not all displayed ([b01481c](https://gitlab.unige.ch/aou/aou-portal/commit/b01481c2cba4f79632637b61c331b160a126a6b1))
* [AOU-991] hide user guid from menu if not defined ([c96125e](https://gitlab.unige.ch/aou/aou-portal/commit/c96125e53b77016f40cfc2d743d32863ba8c8f3c))
* correct the authorization URL in proxy.conf.js ([bed361b](https://gitlab.unige.ch/aou/aou-portal/commit/bed361b7425d525bc2d692570a32ba63364f3e71))
* **deposit contributor step:** [AOU-982] duplicated academic structure when
  autocomplete ([8087d86](https://gitlab.unige.ch/aou/aou-portal/commit/8087d864dbfcaa155237e6d4b13902fbc3858b4c))
* **deposit detail:** [AOU-985] display research group's code when not
  empty ([15fe934](https://gitlab.unige.ch/aou/aou-portal/commit/15fe93429c48c306f3fb04b02bfbc26778dd9ca0))
* **deposit file:** [AOU-1011] avoid to lose license version when
  edit ([67d2761](https://gitlab.unige.ch/aou/aou-portal/commit/67d2761a65759f1e735893a258cce129ace4888e))
* **deposit list:** [AOU-1015] replace update date with create
  date ([709e629](https://gitlab.unige.ch/aou/aou-portal/commit/709e6296d688cf2b6f3f194bb5cbabc221fc830e))
* **deposit list:** [AOU-983] preserve query paremeters when back to detail and set default pagination to
  50 ([97bd2da](https://gitlab.unige.ch/aou/aou-portal/commit/97bd2da404d6723dbf4ed22d42bd1f20fcd478d1))
* **deposit list:** [AOU-995] disallow the bulk action button send to validation when one without
  file ([8378417](https://gitlab.unige.ch/aou/aou-portal/commit/83784174bc0b212f16189559646c54f87f4bb087))
* **home page:** [AOU-990] guid tour is blocking for normal
  user ([e121e91](https://gitlab.unige.ch/aou/aou-portal/commit/e121e9156142741b01d3111da595738e25f818d7))
* **profile:** [AOU-1005] notification hidden when select last
  value ([1438dfe](https://gitlab.unige.ch/aou/aou-portal/commit/1438dfe6f12845b08a85218ab4544fc2873a0d20))
* remove useless separator ([e3fe9cf](https://gitlab.unige.ch/aou/aou-portal/commit/e3fe9cf224065e85aaeba70d053cace0028bddf2))

## [1.0.0](https://gitlab.unige.ch/aou/aou-portal/compare/aou-0.0.5...aou-1.0.0) (2021-09-20)

### Features

* [AOU-342] set accept language header on backend requests ([d11e2fc](https://gitlab.unige.ch/aou/aou-portal/commit/d11e2fc77700a40979147de62f80da588c7419c6))
* [AOU-785] allow depositors to manually enter new research
  groups ([b0dc76e](https://gitlab.unige.ch/aou/aou-portal/commit/b0dc76edc0b43426a30c24a174f097d936680667))
* [AOU-832] integrate endpoint to check if deposit is valid when open it on detail
  view ([557aea7](https://gitlab.unige.ch/aou/aou-portal/commit/557aea79234a8ec72559cf4361c39c2d9e24653e))
* [AOU-863] add feedback tool on footer ([d6cb937](https://gitlab.unige.ch/aou/aou-portal/commit/d6cb93704a9009ae22b7a617ecea57977299f25a))
* [AOU-865] automatic removal of eventual DOI prefixes in
  forms ([2c7c811](https://gitlab.unige.ch/aou/aou-portal/commit/2c7c811daf119f5ee55e453b55a4dd519ac57235))
* [Aou-885] add labels for publication type and publication
  subtype ([3f2db9b](https://gitlab.unige.ch/aou/aou-portal/commit/3f2db9b5fd74663206bc31a989038b687e02c790))
* [AoU-900] allow contributors non-unige without firstname ([1acc315](https://gitlab.unige.ch/aou/aou-portal/commit/1acc315cf70b5c91cf69e521b814b5ba7107bdef))
* add refresh button in user list for ROOT ([78e6072](https://gitlab.unige.ch/aou/aou-portal/commit/78e6072afddbacab0dbd6679f6f8f5dffa485ec8))
* **admin research group :** [AOU-886] replace init by
  synchronize ([2960f14](https://gitlab.unige.ch/aou/aou-portal/commit/2960f14dba7e1d99c157bac0a4545da576e7aa56))
* **carousel:** [AOU-945] dymamic content for the portal home
  page ([baa27e3](https://gitlab.unige.ch/aou/aou-portal/commit/baa27e34ca1e301167b2e69e6886ce85bdb0377d))
* **carousel:** add new tiles ([bc668ea](https://gitlab.unige.ch/aou/aou-portal/commit/bc668eac2a095f0537f813d764251a413aeda683))
* **contact infos:** [AOU-863] add contact infos ([7037df4](https://gitlab.unige.ch/aou/aou-portal/commit/7037df4ec2774e7bf7fd3b89b606227f02195721))
* **deposit commercial url:** [AOU-926] check commercial url and dataset url fields
  contents ([972d3bc](https://gitlab.unige.ch/aou/aou-portal/commit/972d3bc887d4d32aed0621d3732f6ee8ade2a40b))
* **deposit contributor:** [AOU-622] new style on contributor
  list ([fbe0154](https://gitlab.unige.ch/aou/aou-portal/commit/fbe0154e1e4282a59f96e634e9d137e21ca81b46))
* **deposit contributors:** [AOU-831] change default value of contributors for some document
  types ([40e7c5b](https://gitlab.unige.ch/aou/aou-portal/commit/40e7c5b16da4ce674fee4b4000890b2f776b024b))
* **deposit creation workflow:** [AOU-637] new deposit merge the create buttons and adapt the import
  page ([b9f0c0b](https://gitlab.unige.ch/aou/aou-portal/commit/b9f0c0b520e9fe6005e0bdee619ff66a0a475bb6))
* **deposit creation:** [AOU-816] design reorder fields in initialisation page right
  panel ([33792ef](https://gitlab.unige.ch/aou/aou-portal/commit/33792ef6a1716e5a49f84877a24d5052114f20bb))
* **deposit description:** [AOU-651] add button find issn linked to
  this... ([04ca602](https://gitlab.unige.ch/aou/aou-portal/commit/04ca602a43d1a31577b9bf2556337665a49dd99e))
* **deposit detail:** [AOU-951] display button to see archive on archive ouverte
  website ([f685c55](https://gitlab.unige.ch/aou/aou-portal/commit/f685c555b47f091e5b3a4eb3b1231afe8e72bc0f))
* **deposit funding:** [AOU-746] funding possibility to add a funding not in open
  aire ([729889c](https://gitlab.unige.ch/aou/aou-portal/commit/729889c5f400e28cc5112285e3ee61be99207c66))
* **deposit list:** [AOU-896] validation step allow bulk feedback and reorder button and style
  them ([9e3ce71](https://gitlab.unige.ch/aou/aou-portal/commit/9e3ce715f8db4fac156e86ccbf1a43717f9845f5))
* **deposit metadata update:** [AOU-793] form adapt pages fields to the new metadata
  fields ([e3b0c57](https://gitlab.unige.ch/aou/aou-portal/commit/e3b0c57387089d0fe8352232bd506430f708cb2e))
* **deposit style:** [AOU-794-514-880] update general style of deposit
  form ([8e91741](https://gitlab.unige.ch/aou/aou-portal/commit/8e91741d0461134ad90f58d1ca6e5f93553c2afd))
* **deposit thumbnail:** [AOU-595] add thumbnail upload ([b8ea42d](https://gitlab.unige.ch/aou/aou-portal/commit/b8ea42d93acdd4ceb9bb9b9934383e6ff76ef3e8))
* **deposit title:** [AOU-911] convert title into textarea ([4237b3e](https://gitlab.unige.ch/aou/aou-portal/commit/4237b3e904f26b76162a3bb9faa2d8e6b7ffaedd))
* **deposit validator:** [AOU-741] functionalities to help validators correct a
  deposit ([8a35b2d](https://gitlab.unige.ch/aou/aou-portal/commit/8a35b2da57615a04b5a82cd5417ff7b13e3a6051))
* **deposit:** [AOU-707] update the content of the orange
  banner ([cd2970f](https://gitlab.unige.ch/aou/aou-portal/commit/cd2970f722e585499f98e6a8aa3cd546a666bb55))
* **deposits list:** [AOU-604] add searchable DOI and PMID
  fields ([f45e252](https://gitlab.unige.ch/aou/aou-portal/commit/f45e252e8b23a78411c781e4438b13f45c9011a2))
* **design:** [AOU-905] improve look and feel deposit detail page and minor changes on other
  pages ([65db330](https://gitlab.unige.ch/aou/aou-portal/commit/65db330766a412fcaef69ea88021aa5b9be86852))
* display role on token dialog ([08aeea9](https://gitlab.unige.ch/aou/aou-portal/commit/08aeea9d1aebbadd368dc436acd3b9a38f0e87d5))
* **files list:** [AOU-921] add translations for status IMPOSSIBLE_TO_DOWNLOAD + prevent editing files having this
  status ([59c39b0](https://gitlab.unige.ch/aou/aou-portal/commit/59c39b0177856c1fc0493e1ab8bb359123b8ddba))
* **profile:** [AOU-635-636] profile describe structure and research
  group ([3bc968b](https://gitlab.unige.ch/aou/aou-portal/commit/3bc968b64e56ad02c77c9973a617ab2c98fac480))
* **summary:** [AOU-740] functionalities to help validators make the validation
  decision ([68035eb](https://gitlab.unige.ch/aou/aou-portal/commit/68035eba2d9b967d136a694c7c2e30282ef510b4))
* **UNIGE person search:** [AOU-883] add pagination on contributor
  search ([8a6fb4d](https://gitlab.unige.ch/aou/aou-portal/commit/8a6fb4dfa7c034917b586ba76b972086dad9747a))
* **User admin form:** [AOU-860] allow to edit business
  roles ([12454c1](https://gitlab.unige.ch/aou/aou-portal/commit/12454c15825fc54464aaaaf36b94ddad650dfbdc))

### Bug Fixes

* [AOU-624] update drag and drop cursor icon ([91420fe](https://gitlab.unige.ch/aou/aou-portal/commit/91420fe4e88b9b6dd528e84030eb2122cf3f666d))
* [AoU-855] add editors/collaborators when there is no author in deposit
  list ([672b092](https://gitlab.unige.ch/aou/aou-portal/commit/672b092a3702858151c326ca1275b81597197e74))
* [AoU-870] show a prefix message when contributors are imported with
  affiliation ([81ed462](https://gitlab.unige.ch/aou/aou-portal/commit/81ed46284033245ed1a0f0de07d2de7f2fd7355e))
* [AoU-875] remove column type from notification table ([cb4b74a](https://gitlab.unige.ch/aou/aou-portal/commit/cb4b74a0882d2df232630bbb8c871d04671fa2de))
* [AOU-890] publication sub types no longer appear in the deposit create
  step ([88ed900](https://gitlab.unige.ch/aou/aou-portal/commit/88ed90015b7d921ca2a93228c7444bcfce1a277e))
* [AoU-902] change notification order in profile ([68e1df1](https://gitlab.unige.ch/aou/aou-portal/commit/68e1df1cfdac2af353bf721bdc6f5c5ffac930fc))
* [AoU-902] change notification order in profile ([7c87a30](https://gitlab.unige.ch/aou/aou-portal/commit/7c87a305452f3621765df3c5477027386c8ff8b3))
* [AOU-931] multi import change button back to list into
  continue ([57ba7f6](https://gitlab.unige.ch/aou/aou-portal/commit/57ba7f67bd48deb9ccda5665554d28a78262b6aa))
* [AOU-958] trim value pasted on deposit creation field and fix enter press on input
  identifier ([5ac6621](https://gitlab.unige.ch/aou/aou-portal/commit/5ac66210183b63d7ac808f07e37ef4159b888163))
* 944 profile hide the second automatic notification ([a3dd156](https://gitlab.unige.ch/aou/aou-portal/commit/a3dd156e15d2694bdb2e755723a4a1c68d172215))
* add missing orcid in notification success message ([9a96aa9](https://gitlab.unige.ch/aou/aou-portal/commit/9a96aa9d6422557771aebc41564abadf27fa1853))
* add setting to decide to show or hide tour dialog at first
  login ([155567b](https://gitlab.unige.ch/aou/aou-portal/commit/155567b0d7b5d2a00fc9560f57b03c33ccf95034))
* avoid double get metadata from identifier when create ([8e92a6d](https://gitlab.unige.ch/aou/aou-portal/commit/8e92a6d604aedd2dd69de5b39b2a9ff044d7a212))
* carousel ([76f8361](https://gitlab.unige.ch/aou/aou-portal/commit/76f8361dfa4e660faa78a334d94d9ddbeb1ce218))
* **comment:** [AOU-762] list of comments is not sorted by date after a new comment is
  added ([208015f](https://gitlab.unige.ch/aou/aou-portal/commit/208015fc720d1d156c44ca8c2ba3336d57ffa5de))
* **contributor drag:** [AOU-768] hide secondary row during drag and drop
  process ([09d8bab](https://gitlab.unige.ch/aou/aou-portal/commit/09d8bab61caf805ea0316fdb6b141c4fd73acb76))
* **contributor search:** [AOU-728] not send person searh request if firstname or lastname is one
  character ([e2a13a5](https://gitlab.unige.ch/aou/aou-portal/commit/e2a13a581b3f5e1661907840d04b18eb31fb71f5))
* **contributor:** [AOU-877] treatment of multiple contributors generates a strange display of
  names ([6b9207d](https://gitlab.unige.ch/aou/aou-portal/commit/6b9207d87a0d6832c1d4600b538d51b8742a9caf))
* **deposit comment:** [AOU-748] add a comment button on validator summary scroll to wrong comment
  zone ([2ab15b4](https://gitlab.unige.ch/aou/aou-portal/commit/2ab15b4bc6b4c5fa6ccddaa9fbc62885c6496301))
* **deposit contributor import:** [AOU-882] contributor fall in error when freshly
  imported ([0cb35fe](https://gitlab.unige.ch/aou/aou-portal/commit/0cb35fe6fe937e29f56b75929045ab2f0dd0a777))
* **deposit contributor:** [AOU-755] improve autocomplete code
  velocity ([2bde202](https://gitlab.unige.ch/aou/aou-portal/commit/2bde202b05f65bb0992f737b28e7712350b84c85))
* **deposit contributor:** [AOU-789] remove error message on duplicate contributor when other is
  deleted ([f2320d5](https://gitlab.unige.ch/aou/aou-portal/commit/f2320d5cb3ede656bf21ec45f1360c32775853de))
* **deposit contributor:** [AOU-854] collaborators automatically open collaboration member when at least
  one ([7a7e1f3](https://gitlab.unige.ch/aou/aou-portal/commit/7a7e1f333497ef227f40db1edff7cd405a860442))
* **deposit contributor:** [AOU-857] hide button when least than two person and init with one
  person ([7efd094](https://gitlab.unige.ch/aou/aou-portal/commit/7efd094226b05fca729ea5c48f6c9c779b2a8b76))
* **deposit creation:** [AOU-938] when creating manual deposit next button is activated even when form is
  invalid ([f40ef79](https://gitlab.unige.ch/aou/aou-portal/commit/f40ef79ce2310aa8f22365181c15d638b15b220b))
* **deposit date:** [AOU-941] wrong date types for diploma ([caeb1ec](https://gitlab.unige.ch/aou/aou-portal/commit/caeb1ec8db085e11bc07b7f0efe0fc5487a1de2a))
* **deposit description:** [AOU-676] mising up of corrective doi
  pmid ([0e1290f](https://gitlab.unige.ch/aou/aou-portal/commit/0e1290f6c98e175ee70c4d612ff94c32b001e792))
* **deposit description:** [AOU-791] swat title and doc type order in step
  description ([5f33e4b](https://gitlab.unige.ch/aou/aou-portal/commit/5f33e4b32e36a9727b54c32d08d51ae7e7178db6))
* **deposit description:** [AOU-851] DOI of correction not tested for
  validity ([3337116](https://gitlab.unige.ch/aou/aou-portal/commit/33371165c3faa3f7246f99fbce466f4cb3ed27bf))
* **deposit description:** [AOU-898] update container labels and rules depending on
  subtypes ([f0f1c9f](https://gitlab.unige.ch/aou/aou-portal/commit/f0f1c9fad97e094d6d186cd5c3a658426071942f))
* **deposit detail:** [AOU-924] no search into issn org for container title of books chapter and chapter of
  proceed ([9ca9c93](https://gitlab.unige.ch/aou/aou-portal/commit/9ca9c93d2255d347ef87d227850cfd874fa9b9d3))
* **deposit detail:** [AOU-961] avoid container title issn search for
  contributor ([2079d9d](https://gitlab.unige.ch/aou/aou-portal/commit/2079d9d937696f6ad3c9c08e43459dbc507edfaa))
* **deposit detail:** display of keywork on firefox ([58b9ef7](https://gitlab.unige.ch/aou/aou-portal/commit/58b9ef7b1db2633e37180a0932130e10ce90e53e))
* **deposit detail:** do not ignore thesis number ([c6d39d7](https://gitlab.unige.ch/aou/aou-portal/commit/c6d39d78e524cac34bb1f09fe283237e725fc21c))
* **deposit detail:** do not show sections when no identifiers are
  defined ([8a47d95](https://gitlab.unige.ch/aou/aou-portal/commit/8a47d9521453de6cc8c87df2a85f5e46c1902941))
* **deposit errors:** [AOU-934] update box title ([84a4369](https://gitlab.unige.ch/aou/aou-portal/commit/84a4369e0b25751c12d67a895fdd6ac826c46fd8))
* **deposit file:** [AOU-760] unable to go to next step when there is a confirmed file in the
  list ([8646f55](https://gitlab.unige.ch/aou/aou-portal/commit/8646f5590eed209850f040c4ee49298a93e9f61d))
* **deposit file:** [AOU-841] add special rule for agreement file
  type ([589bc74](https://gitlab.unige.ch/aou/aou-portal/commit/589bc74ec6038d61c6486102137e30521f924244))
* **deposit file:** [AOU-954] page doesn't refresh when a duplicate file is
  found ([30c70aa](https://gitlab.unige.ch/aou/aou-portal/commit/30c70aa40cbe946db3f9764b9f54190b0f385d74))
* **deposit files:** [AOU-671] refresh files table when there is an error concerning duplicate
  files ([11c4984](https://gitlab.unige.ch/aou/aou-portal/commit/11c49843ec16973c9d333a6f0a85f16af9f83529))
* **deposit form:** [AOU-852] volume and issue field are now correctly saved as
  string ([2622805](https://gitlab.unige.ch/aou/aou-portal/commit/26228054047117b0fce4f8bf9d09e3fcb6aff950))
* **deposit identifier:** [AOU-925] do not ignore issn for fournal
  issues ([821910b](https://gitlab.unige.ch/aou/aou-portal/commit/821910b77d8a8a22ca71e46b1c14d0ee70472de5))
* **deposit list:** [AOU-778] display bulk add of structure or research group in validation
  mode ([8e87dcb](https://gitlab.unige.ch/aou/aou-portal/commit/8e87dcb4ded5237bd3d71bb0116a8bd080b53336))
* **deposit list:** hide status ([4452314](https://gitlab.unige.ch/aou/aou-portal/commit/44523142bcf5064248141adeb76274c9bc3f8076))
* **deposit multi import:** [AOU-844] shade import button when not
  active ([6988bd3](https://gitlab.unige.ch/aou/aou-portal/commit/6988bd33ac434b66a59674fee7abbd0307b740e5))
* **deposit multi import:** [AOU-845] small fixes with multiple import via doi
  pmid ([1612df9](https://gitlab.unige.ch/aou/aou-portal/commit/1612df9a772609f5517da7269e446735ed04014b))
* **deposit not my publication:** [AOU-960] change management of button its not my publication after
  validation ([f4c9a8c](https://gitlab.unige.ch/aou/aou-portal/commit/f4c9a8cc4bd0de68024721b58a4311f22bb20e1c))
* **deposit submission:** [AOU-743] one deposit validated, redirect to
  list ([e0df017](https://gitlab.unige.ch/aou/aou-portal/commit/e0df017147d0eedb5f84e0df4712b3f1ac14d0ee))
* **deposit summary:** [AOU-853] do not parse 'conferenceDate' as a
  date ([3c54be7](https://gitlab.unige.ch/aou/aou-portal/commit/3c54be7c6c3d82cac89c95ce753711a9c655ed78))
* **deposit summary:** display in red funding without juridisction only for
  validator ([83eadac](https://gitlab.unige.ch/aou/aou-portal/commit/83eadac07415528ff1acd971a76be6947bc285ad))
* **deposit upload:** [AOU-957] hide complementary description for
  Agreements ([472f855](https://gitlab.unige.ch/aou/aou-portal/commit/472f855cd07ea09c6252c6529575c9fbea6cd954))
* **deposit validation:** [AOU-806] replace delete action on validation screen by reject
  action ([bf969a0](https://gitlab.unige.ch/aou/aou-portal/commit/bf969a06cc9f0f8e2810575a6561a726028e8b42))
* **deposit validation:** [AOU-871] hide in progresss tab from the deposits to validate
  screen ([63fbf41](https://gitlab.unige.ch/aou/aou-portal/commit/63fbf4174a982be5b997f47ae4fddcdf39e4a934))
* **deposit validation:** [AOU-916] avoid to call validate metadata on edit mode for better
  performance ([2ca5182](https://gitlab.unige.ch/aou/aou-portal/commit/2ca518286ac1a8113d8b1ec897d4ce1db1f638fe))
* **deposit validation:** [AOU-920] leave validation deposit menu when press
  F5 ([1457ace](https://gitlab.unige.ch/aou/aou-portal/commit/1457ace3b277a69e64927e23f5033be69fafdaec))
* **deposit:** [AOU-914] type of deposit not displayed when
  create ([2051e2c](https://gitlab.unige.ch/aou/aou-portal/commit/2051e2c262d233fef87dfd133387fa55730654ff))
* **deposit:** [AOU-928] submission of a book not possible ([48820c6](https://gitlab.unige.ch/aou/aou-portal/commit/48820c6d61007c0745c56e73375819f043864153))
* **deposit:** prevent error when submit a deposit with structure list
  null ([f1fee48](https://gitlab.unige.ch/aou/aou-portal/commit/f1fee488e3015053a2be2ce2af5905e1e2510c54))
* detect invalid input on every field ([b887d55](https://gitlab.unige.ch/aou/aou-portal/commit/b887d55f31e420bf54035c0f7cd29fedd48cbf72))
* display bulk approve and reject only for admin root and super
  validator ([ab013f3](https://gitlab.unige.ch/aou/aou-portal/commit/ab013f3ca32e7f8af70e3e96dded13c2c5d72711))
* **document file:** [AOU-913] sort deposit document file depending of sort
  value ([2cfb894](https://gitlab.unige.ch/aou/aou-portal/commit/2cfb89481989ed2523a58374efa25c0f557fb056))
* fix regex to detect unige affiliation when importing using a
  service ([8d35d8d](https://gitlab.unige.ch/aou/aou-portal/commit/8d35d8de18ec896700ed2baf283fc344f3c4c31c))
* **font:** [AOU-891] load robot font ([c007597](https://gitlab.unige.ch/aou/aou-portal/commit/c007597ae3a38eff7f3bc7de3f75f5aaec3f934c))
* **form description:** make localNumber field visible again for
  thesis ([ece81d5](https://gitlab.unige.ch/aou/aou-portal/commit/ece81d5203c0ba7da8ec2d8e55f03322be833e53))
* **form:** [AOU-942] hide all fields mapped to 'container' metadata on description step for deposits of Proceedings
  type ([e039d49](https://gitlab.unige.ch/aou/aou-portal/commit/e039d49e2445c1d818ee85ca21830070d00c0ffa))
* **form:** [AOU-946] hide container title for Books and Edited books and remove required rule to allow
  submission ([5bdedb9](https://gitlab.unige.ch/aou/aou-portal/commit/5bdedb9bbd587b84ced1e8b4cec0dcbc4fbb4581))
* **form:** [AOU-950] store conference title in container title + rename conferenceTitle into
  conferenceSubtitle ([cfc6788](https://gitlab.unige.ch/aou/aou-portal/commit/cfc6788ddcc9211e93f8e68c1681b0c2877008e1))
* **general style:** [AOU-872] toolbar lose sticky ([93afa7d](https://gitlab.unige.ch/aou/aou-portal/commit/93afa7d4ae466463e7437a97be6a160ed35d9e0a))
* hide delete button for depositor when status is not in progress, feedback required or in
  error ([d34f0f4](https://gitlab.unige.ch/aou/aou-portal/commit/d34f0f466239f99151ecf13c554a08ab43207c79))
* **import:** remove frontend translation to use backend messages when import not found
  404 ([dc39b1c](https://gitlab.unige.ch/aou/aou-portal/commit/dc39b1c1f6092c316f4e43c188fc3097c3eb0018))
* missing import ([70adc22](https://gitlab.unige.ch/aou/aou-portal/commit/70adc221846ab2819553e38cffba92104a41cc3d))
* **multiple imports:** [AOU-935] send requests sequentially to prevent exceptions on
  backend ([4b5d9b6](https://gitlab.unige.ch/aou/aou-portal/commit/4b5d9b6c8f452b32d9ca86bdb488d1fa8cbf728a))
* override default orcid client id with system property value if
  defined ([e18f2b1](https://gitlab.unige.ch/aou/aou-portal/commit/e18f2b108f046b8c0d57f1b0bfb4994a7401a8c1))
* **profile form:** make form dirty when only notifications checkboxes has been
  modified ([0a9c0a1](https://gitlab.unige.ch/aou/aou-portal/commit/0a9c0a178c491afc78c75fc865a18468ba5d4453))
* **profile:** hide some notification ([09ff1d0](https://gitlab.unige.ch/aou/aou-portal/commit/09ff1d044aecca38dfe5996c6179ebd49bc3cc42))
* redirect to aou website on home page button click ([01eaf94](https://gitlab.unige.ch/aou/aou-portal/commit/01eaf94ac3c9e8b6a10c3976c6c3fd5b977babfa))
* remove setting about token endpoint and login url for non dev
  env ([df8155f](https://gitlab.unige.ch/aou/aou-portal/commit/df8155fa8ca1f3e063e8a2d6a2952882371870d3))
* **rest call:** [AOU-892] calls to undefined person made on
  login ([68781ae](https://gitlab.unige.ch/aou/aou-portal/commit/68781aeaf726b5f5075cd9b8abae622fa912f737))
* swap structure and research group in profile + rename structure to affiliation
  entity ([97fc83d](https://gitlab.unige.ch/aou/aou-portal/commit/97fc83d4b5a19d568d84367a40aec1d77b8292cf))
* transfer current language when open aou website ([27cc6d1](https://gitlab.unige.ch/aou/aou-portal/commit/27cc6d15521a7e7e0cdefc12508aaa3a3ed13c7a))
* update labels ([ea33fb7](https://gitlab.unige.ch/aou/aou-portal/commit/ea33fb785b45598f2e99b7d844c0126e5a3e0dc4))
* update solidify version to 1.5.21 ([1f39278](https://gitlab.unige.ch/aou/aou-portal/commit/1f3927804b07573497c97dd052df6fbc88aedd5c))
* update translations ([b7bba3a](https://gitlab.unige.ch/aou/aou-portal/commit/b7bba3a24e55a2d5d3360af095d6b5ce061b9f47))
* **validation structure:** [AOU-798] list of structures is incomplete on validation structure
  dialog ([f820322](https://gitlab.unige.ch/aou/aou-portal/commit/f82032298abccb33ac9a21e8edb523905b390891))
* **welcome dialog:** [AOU-917] translate missing on welcome
  dialog ([476df45](https://gitlab.unige.ch/aou/aou-portal/commit/476df453e23cbe98d3a66a38c1268c429de41713))
* white button on snackbar ([ecb6d58](https://gitlab.unige.ch/aou/aou-portal/commit/ecb6d58d8503a4770c578c890afaca96386f7bcd))
* width of field publication house ([ba50e66](https://gitlab.unige.ch/aou/aou-portal/commit/ba50e66f255b4dcc8d90d01ec31b05f07a5c43c9))
* z-index mobile ([1ec9417](https://gitlab.unige.ch/aou/aou-portal/commit/1ec9417c76cd2b6dcc11ff7536967c6fe3b899de))

## [0.0.5](https://gitlab.unige.ch/aou/aou-portal/compare/aou-0.0.4...aou-0.0.5) (2021-08-10)

### Features

* [AOU-539] new form to import a list of doi or pmid ([9e681b3](https://gitlab.unige.ch/aou/aou-portal/commit/9e681b3ca16fb4c81f28736628e66fc8d379eaac))
* [AOU-677] finish task do not store empty date value ([65c47cb](https://gitlab.unige.ch/aou/aou-portal/commit/65c47cb9b96ed32a1640b9eb5a5f6d97760adea8))
* aou-708 import metadata from isbn ([1c87a60](https://gitlab.unige.ch/aou/aou-portal/commit/1c87a60169f211373247c66155322e4fd93d728c))
* **button style:** [AOU-623] ensure that the action buttons have the same look throughout the
  app ([1328112](https://gitlab.unige.ch/aou/aou-portal/commit/1328112d1920304b8f2e5ce148b0cc6977041597))
* **deposit contributor:** [AOU-709-710] add button to automatically
  select... ([f6f8ec8](https://gitlab.unige.ch/aou/aou-portal/commit/f6f8ec8c39a59e67ac99bebcca49f2337a0d4cc8))
* **deposit file:** [AOU-590] allow drag drop to upload
  files ([de40f06](https://gitlab.unige.ch/aou/aou-portal/commit/de40f06d0b2a6dc7ef14f6f6a5703efb5f919993))
* **deposit import:** [AOU-434-435] show explicit error messages when
  importing ([788a7ba](https://gitlab.unige.ch/aou/aou-portal/commit/788a7ba7b644c7bee3b3e6de62cffe6ec5e8c3e3))
* **deposit license:** [AOU-704] update file license selection to allow selection of specific
  version ([67452f4](https://gitlab.unige.ch/aou/aou-portal/commit/67452f47235588364bd12cca279cf8b5411cb25b))
* **deposit list:** [AOU-567-711] create popups allowing to add research
  group... ([c2fd170](https://gitlab.unige.ch/aou/aou-portal/commit/c2fd17002696aa81c16dc84059679a157371dbf1))
* **deposit multi import:** display dialog to explain what to do after multiple
  import ([9876ad4](https://gitlab.unige.ch/aou/aou-portal/commit/9876ad488470b65294b77d39e85fba17b25ef651))
* **deposit upload file:** always preselect first version of
  license ([b1597fe](https://gitlab.unige.ch/aou/aou-portal/commit/b1597fe2c052ac8372f1f0c5b4c2234c9f33d129))
* **deposit:** [AOU-414] add button it is not my publication ([0af37df](https://gitlab.unige.ch/aou/aou-portal/commit/0af37df42032d3242876dc754877f2d8e755d79f))
* display its not my deposit button only when needed ([f334d9b](https://gitlab.unige.ch/aou/aou-portal/commit/f334d9b4c2fc698485e34d589feb24a6ffb9a593))
* **file preview:** [AOU-687] add preview for files ([b3b90af](https://gitlab.unige.ch/aou/aou-portal/commit/b3b90af8612cd71fe2191dd4717fb2805260fa93))
* **file upload:** [AOU-647] file uplaod hide license field when not in free access and style
  updates ([9b850db](https://gitlab.unige.ch/aou/aou-portal/commit/9b850dbb0593dc33697cd1126309a284a3e85f28))
* **files:** [AOU-701] update file list display ([b21a8f1](https://gitlab.unige.ch/aou/aou-portal/commit/b21a8f196d95f8481c1f8421918c088f0e545b0c))
* **FileTypeLevelEnum:** [AOU-761] add ADMINISTRATION level ([996e445](https://gitlab.unige.ch/aou/aou-portal/commit/996e445b402050024a90b76196c8b9860b18820e))
* **home:** [AOU-716] homepage fill content with two links consult or manage my
  deposit ([ffcc478](https://gitlab.unige.ch/aou/aou-portal/commit/ffcc4782b11712b3878d0669ce4d85cc904c8d31))
* **structure:** [AOU-682] structure component enable search with tree
  possibility ([3ebb661](https://gitlab.unige.ch/aou/aou-portal/commit/3ebb661a6b5144074247ff48d5c08c2a72a6161f))
* **tooltip:** [AOU-638] tooltips should be added in the
  application ([aa652e6](https://gitlab.unige.ch/aou/aou-portal/commit/aa652e6602efd7ebb4d30ed190135b0b429a0d62))
* use dialog util everywhere ([389a645](https://gitlab.unige.ch/aou/aou-portal/commit/389a64599416288afaa2d805e16d955b004e6b84))

### Bug Fixes

* [AOU-674] send to validation upload file button label is
  unclear ([2c94ee1](https://gitlab.unige.ch/aou/aou-portal/commit/2c94ee170b133de425b36d2ca17ba9a4ee371d68))
* aou-677 do not store empty date values ([c8e7e7e](https://gitlab.unige.ch/aou/aou-portal/commit/c8e7e7e4926a9e8fdade876f689b65df0824eca9))
* aou-677 do not store empty date values ([cd6afc6](https://gitlab.unige.ch/aou/aou-portal/commit/cd6afc6662e1b422e76db57722c07e26ccf8c034))
* autocomplete structure and research group when list null ([4404556](https://gitlab.unige.ch/aou/aou-portal/commit/4404556d5f7a73cee08da70b751f8db817c6b05a))
* avoid to display undefined when there is not license on deposit file
  list ([46bda3d](https://gitlab.unige.ch/aou/aou-portal/commit/46bda3dfbeb57ef0ccc4f356d261eb2374de1680))
* **deposit import:** [AOU-715] hide the dblp import which is not in
  place ([270cd9f](https://gitlab.unige.ch/aou/aou-portal/commit/270cd9f727200d25006cb10159edc4f6582123f8))
* **deposit keyword input:** update treatment of keywords ([1703a9c](https://gitlab.unige.ch/aou/aou-portal/commit/1703a9cad6d7a59a9c143ff151258cd5f7034a4a))
* **deposit list:** display structure on deposit list ([8c99ef1](https://gitlab.unige.ch/aou/aou-portal/commit/8c99ef130eb77db4f518736d19ebaebef25f6d67))
* **deposit multiple import:** prevent user to type text into paste input
  field ([a0e4678](https://gitlab.unige.ch/aou/aou-portal/commit/a0e467803e36d16122ca4f56b23c06a96d4b3350))
* **deposit mutli import:** [AOU-770] import via multiple id display message when error
  500 ([6b27022](https://gitlab.unige.ch/aou/aou-portal/commit/6b27022af4ad48e8884aa973dd76573547335fbe))
* **deposit:** [AOU-703] display notifications type in profile depending on
  role ([77e2f3b](https://gitlab.unige.ch/aou/aou-portal/commit/77e2f3bdd89b921ed429c82153953c4b39d9bca3))
* **deposit:** change target page when exit in validation
  mode ([d6bdfbc](https://gitlab.unige.ch/aou/aou-portal/commit/d6bdfbc9a9062947e7b19829c87b77ccd50351ef))
* labels and undefined version ([13b3a28](https://gitlab.unige.ch/aou/aou-portal/commit/13b3a2888edb7f33674fc4f2f8dce3053248aefb))
* lint error ([6607ea4](https://gitlab.unige.ch/aou/aou-portal/commit/6607ea4817ec499b847e39426033e78f418b0fa3))
* not allow to delete deposit in validation ([cd1e8d2](https://gitlab.unige.ch/aou/aou-portal/commit/cd1e8d274bda69e59aeeb8f840865376e18849ea))
* **notification and event:** [AOU-724] redirect from notifications and
  events ([799e4be](https://gitlab.unige.ch/aou/aou-portal/commit/799e4be1cd5860c080927a76602ca1269779be80))
* remove unused label ([ef27b8f](https://gitlab.unige.ch/aou/aou-portal/commit/ef27b8f298b56589866a34d98328d9a2be18d21f))
* remove useless parameter ([fb62db7](https://gitlab.unige.ch/aou/aou-portal/commit/fb62db7edb63a46b9145cff921586b4d7cf4e5b5))
* routing broken on deposit form ([5f47798](https://gitlab.unige.ch/aou/aou-portal/commit/5f477985cd6e0954e25f5ef0810f1c46714793f6))
* **structure and researchgroup:** uniformizing structure and researchgroup label in searchable
  dropdown ([c2cfe92](https://gitlab.unige.ch/aou/aou-portal/commit/c2cfe92bb4a42f32a2a57d2fe187850f578ec289))
* **upload:** [AOU-713] access level disappear after edit ([54860c8](https://gitlab.unige.ch/aou/aou-portal/commit/54860c89e633dfa53137eeef2fb722241e2a08ee))
* **upload:** [AOU-713] unable to return to file without embargo when change from public to
  restricted ([d8f815e](https://gitlab.unige.ch/aou/aou-portal/commit/d8f815e93433d8c610303bb32f1783ee98c0dc6a))
* **upload:** [AOU-725] fix invalid date imported cause embargo duration input
  broken ([29f458f](https://gitlab.unige.ch/aou/aou-portal/commit/29f458f1006b29811e573afb7d6098613bda748d))
* when invalid set publication date to undefined and hide embargo duration input
  helper ([9cbcb14](https://gitlab.unige.ch/aou/aou-portal/commit/9cbcb14089f6ad687c6a0165ba1c6c20e770dc2a))

## [0.0.4](https://gitlab.unige.ch/aou/aou-portal/compare/aou-0.0.3...aou-0.0.4) (2021-07-06)

### Features

* [AOU-661] extreme programming feedback ([7c0b99f](https://gitlab.unige.ch/aou/aou-portal/commit/7c0b99f48425e26c9fa9a4a3b2772b276e86dfeb))
* **admin events:** [AOU-552] new page to list application
  events ([37ee3c5](https://gitlab.unige.ch/aou/aou-portal/commit/37ee3c5c4bd555880efff4f91fc3c884b6a6e1bd))
* **deposit contributor:** [AOU-449] make the director mandatory in case of thesis
  master ([c17942a](https://gitlab.unige.ch/aou/aou-portal/commit/c17942acc10982c59ef69787ca831331a10ed857))
* **deposit file:** [AOU-504] add a new text field for a deposit
  file ([841a1cf](https://gitlab.unige.ch/aou/aou-portal/commit/841a1cf0d3b4514968d9b5c72666c548e521e1c9))
* **deposit file:** [AOU-588] allow depositor to continue without file on deposit but mandatory for
  validator ([592fbe5](https://gitlab.unige.ch/aou/aou-portal/commit/592fbe5465743975782d6a525467e2041a6e5638))
* **deposit file:** [AOU-592] add icons along to file
  version ([eb08336](https://gitlab.unige.ch/aou/aou-portal/commit/eb08336d2a50f605e9de0495aa853bc5c1a34487))
* **deposit file:** integrate romeo infos on deposit files
  step ([3dac4b8](https://gitlab.unige.ch/aou/aou-portal/commit/3dac4b85712abf4e33bdfb0fe525ac28337565a7))
* **deposit files:** [AOU-496] update the list of deposited
  files ([b4a1edd](https://gitlab.unige.ch/aou/aou-portal/commit/b4a1edd32cb04ca3cb29920f0b106c7272338142))
* **deposit funding:** [AOU-332] deposit funding allow to retrieve project infos from
  openAire ([44ce97c](https://gitlab.unige.ch/aou/aou-portal/commit/44ce97c63ddeb122b2993fe1f463541c436d23f9))
* **deposit import:** [AOU-508] add an information pop-up message to the depositor explaining what the system has
  done ([1da73e1](https://gitlab.unige.ch/aou/aou-portal/commit/1da73e168a20b38dea8f11daa1f44a5d6fef73a2))
* **deposit keyword:** [AOU-569] quality check for keyword ([4a5a366](https://gitlab.unige.ch/aou/aou-portal/commit/4a5a366f27079f848cf66ad30a2a9be0d8de7a55))
* **deposit list:** [AOU-451] add a filter option on the columns of deposit
  list ([79138d2](https://gitlab.unige.ch/aou/aou-portal/commit/79138d245d439ee45c2fb7b4ddf603fe3fde42e7))
* **deposit:** [AOU-550] display new metadata fields ([43eadee](https://gitlab.unige.ch/aou/aou-portal/commit/43eadee1fccd9f3fe9969b3edd013b8f93d8622d))
* **deposit:** [AOU-596] redirect user to the list of deposit after
  submission ([9523e29](https://gitlab.unige.ch/aou/aou-portal/commit/9523e298854ce73920f29e34d6471f023e49947c))
* **deposits list:** [AOU-563] add checkboxes column to execute an action on multiple deposits at
  once ([6c6894c](https://gitlab.unige.ch/aou/aou-portal/commit/6c6894c71e5baaad43aba174f458bebf1ee633e6))
* **journal search:** [AOU-459] allow to search on journal title with extended
  search ([f0baa4a](https://gitlab.unige.ch/aou/aou-portal/commit/f0baa4af2e491b8b4f1703766d18d408ef42b4c5))
* **license:** [AOU-572] add a sort field on admin form and sort in file upload searchable
  select ([1d31eac](https://gitlab.unige.ch/aou/aou-portal/commit/1d31eacb6e59117450a948e440708feda2f5a671))
* **notification:** [AOU-553] new page to list user
  notifications ([6e05875](https://gitlab.unige.ch/aou/aou-portal/commit/6e05875c91ebed674219e00d596fd47cb56c1ee3))
* **research group:** [AOU-559] [AOU-560] add acronym and add advanced
  search ([cc43cf3](https://gitlab.unige.ch/aou/aou-portal/commit/cc43cf369d30d7a6f7b3b0a267a4627d977655f7))

### Bug Fixes

* AOU-627, AOU-628, AOU-630 + other label corrections. EN +
  FR ([3dc8e44](https://gitlab.unige.ch/aou/aou-portal/commit/3dc8e44ca42a95c83fadafc858a84b94503b949d))
* **deposit contributor:** reverse placeholder ([7cdb95d](https://gitlab.unige.ch/aou/aou-portal/commit/7cdb95d331528e8e3d886f738d17da3ebda06ba1))
* **deposit creation:** unable to exit ([531d2a4](https://gitlab.unige.ch/aou/aou-portal/commit/531d2a4996c3339010d8d1796dafcf3cca80ea45))
* **deposit date:** [AOU-600] swap date1 and date2 when importing
  articles ([495f032](https://gitlab.unige.ch/aou/aou-portal/commit/495f03209c3856aa760b1dc0b75f5888d8541622))
* **deposit description:** [AOU-599] [AOU-641] abstract and number thesis mandatory when diploma or
  thesis ([f11592b](https://gitlab.unige.ch/aou/aou-portal/commit/f11592b26993c8dd767dda6d8f6f731cf9b3bd39))
* **deposit file:** [AOU-612] popup about automatic pdf displayed when
  back ([a98415b](https://gitlab.unige.ch/aou/aou-portal/commit/a98415b381ff354bbdc5de2c0642a15e36ae3c94))
* **deposit file:** display message file required when submit to
  validation ([f39e8cf](https://gitlab.unige.ch/aou/aou-portal/commit/f39e8cf2b9a8b2c754189da754228a672ad2ced6))
* **deposit funding:** allow to have funding code with alpha ([8323ac7](https://gitlab.unige.ch/aou/aou-portal/commit/8323ac74ddf9b3c179bbd5c332b55802d41dc90c))
* **deposit import:** [AOU-662] when a doi is not found the message should be
  explicit ([268c377](https://gitlab.unige.ch/aou/aou-portal/commit/268c3772a8e09464d9851eaf58dbeeec3e50dce3))
* **deposit keyword:** hide message limit exceeded when there is no
  keyword ([02e87a1](https://gitlab.unige.ch/aou/aou-portal/commit/02e87a13ce8b1cd358d63ac420bc71e260f22152))
* **deposit:** [AOU-665] disable button once clicked to avoid multiple deposits
  created ([b3a325d](https://gitlab.unige.ch/aou/aou-portal/commit/b3a325dbe5e01bf0a803e53d2c66e895064f117f))
* **DepositDocumentFileState:** [AOU-675] send label on file
  upload ([cff3798](https://gitlab.unige.ch/aou/aou-portal/commit/cff379896e12a7f1a35a6dcd39407a882ee89b77))
* **description:** [AOU-613] doctor adress field should be a textarea on step 4 as
  well ([6b956b9](https://gitlab.unige.ch/aou/aou-portal/commit/6b956b9379ddc5701efde487933b8d873e086ea4))
* **files list:** [AOU-672] duplicate files are downloadable ([f2a1577](https://gitlab.unige.ch/aou/aou-portal/commit/f2a157760f3cac59f082dd167464a8ff70aa6ef1))
* **fundings:** [AOU-655] move 'fundings' metadata property on description
  step ([c153fbd](https://gitlab.unige.ch/aou/aou-portal/commit/c153fbdd7197b5ef0f716203957e39765b20fa12))
* **keyword:** [AOU-598] split long keyword with other
  separators ([6b3ce0c](https://gitlab.unige.ch/aou/aou-portal/commit/6b3ce0c0bcf2bd5ee7d40dbe9c94d28c2bc03a11))
* minor english corrections ([4200ce0](https://gitlab.unige.ch/aou/aou-portal/commit/4200ce097f9845e4d1dfe632b9c9cb54feaa274b))
* **Notifications:** remove obsolete notification types available in
  profile ([ddbda76](https://gitlab.unige.ch/aou/aou-portal/commit/ddbda76adf2e2d3972d1d9cfef2c4c64439ef165))
* **NotificationState:** TSLint use 'const' instead of 'let' ([b955da1](https://gitlab.unige.ch/aou/aou-portal/commit/b955da1e91af66f5c27c68e06a2c857d8978d4dd))
* rename C4 enum value to L4 on deposit sub type enum ([91903d1](https://gitlab.unige.ch/aou/aou-portal/commit/91903d10b4b257da0671f9175d2745a7f3eb2f84))
* **romeo:** [AOU-658] improve romeo display when issn not
  found ([41ee5d5](https://gitlab.unige.ch/aou/aou-portal/commit/41ee5d5bc11c7adbfc7170475d5152f9fc337e7f))
* when deposit file is not ready, the button next should not be
  clickable ([e3939ff](https://gitlab.unige.ch/aou/aou-portal/commit/e3939ff4a0945fbb32fff07ceba99d9609777681))

## [0.0.3](https://gitlab.unige.ch/aou/aou-portal/compare/aou-0.0.2...aou-0.0.3) (2021-06-01)

### Features

* [AOU-366] integrate deposit import service from PMID ([2585e15](https://gitlab.unige.ch/aou/aou-portal/commit/2585e154a1538e28e1fc6040ce4723b3885ddf2b))
* 410 reorder deposit list column ([d5b5299](https://gitlab.unige.ch/aou/aou-portal/commit/d5b5299ef28eec2f14d3b92ee858212dda306565))
* **admin user:** [AOU-429] add toggle in user list to show only user with validation
  right ([cf046df](https://gitlab.unige.ch/aou/aou-portal/commit/cf046df6aeefc9927e391f4433f9e4f257bf73c2))
* AOU-386 add support for undefined container issue and
  volume ([a60e342](https://gitlab.unige.ch/aou/aou-portal/commit/a60e3427d54d5c857ae7f918867a9d17444a70f0))
* **deposit contributor:** [AOU-526] selecting all contributors as authors when
  importing ([7908381](https://gitlab.unige.ch/aou/aou-portal/commit/790838116f7726972d0edfbdaa3f21390e35059c))
* **deposit first step:** [AOU-453] display language in first step and
  hide... ([43d3fe9](https://gitlab.unige.ch/aou/aou-portal/commit/43d3fe91831fe6c370cb8a3dd19c28a6da4ba6db))
* **deposit import:** [AOU-426] Integrate deposit import service
  arXiv ([4e68fb3](https://gitlab.unige.ch/aou/aou-portal/commit/4e68fb33e138430f8a9305e722726b8b80aae9c1))
* **deposit schema:** [AOU-416] update fields form according to pivotal format
  updates ([d0a257f](https://gitlab.unige.ch/aou/aou-portal/commit/d0a257f1a9ae1f36585c0b263d5f7fef0b130926))
* **deposit:** [AOU-275] use research group dropdown into deposit
  step ([526a8b9](https://gitlab.unige.ch/aou/aou-portal/commit/526a8b9d01d771000d0bc141fccedd511cca8c80))
* **deposit:** [AOU-330] change field label depending of
  type ([7aae772](https://gitlab.unige.ch/aou/aou-portal/commit/7aae772ddbf5368516db2b2a2cedb26d1a395f5e))
* **deposit:** [AOU-330] conditionnaly display deposit form input depending of
  type ([753947b](https://gitlab.unige.ch/aou/aou-portal/commit/753947bb94440db2b2b52c49c93d3a422902bda9))
* **deposit:** [AOU-330] display ignored attributes to validator if
  present ([8683c33](https://gitlab.unige.ch/aou/aou-portal/commit/8683c335c88815d694cff4374ee7ccc3aae80f5d))
* **deposit:** [AOU-330] translate ignored fields ([02009e0](https://gitlab.unige.ch/aou/aou-portal/commit/02009e0d105c9848128b9e74dc7d3e033d272db2))
* **deposit:** [AOU-335] all repeatable element should already have one element empty ready to
  fill ([39e157f](https://gitlab.unige.ch/aou/aou-portal/commit/39e157f43126e398d448c2ee2fefb9a1e89bddd4))
* **deposit:** [AOU-351] allow to enter a file embargo end date by giving a period in
  months ([0c165da](https://gitlab.unige.ch/aou/aou-portal/commit/0c165da781bf7f37a0d1f07ed13f30e62a120491))
* **deposit:** [AOU-364] WIP: search journal titles on backend
  endpoint ([db6d387](https://gitlab.unige.ch/aou/aou-portal/commit/db6d387c1053ac96c36f3ffe259436d2de033a1d))
* **deposit:** [AOU-395] use new endpoint deposit file status for
  polling ([cc4ebe1](https://gitlab.unige.ch/aou/aou-portal/commit/cc4ebe1bee366570de62a2ba7c151e33b643e256))
* **deposit:** [AOU-457] send import source to the backend on deposit
  creation ([ce44edd](https://gitlab.unige.ch/aou/aou-portal/commit/ce44edd63e70058b3e4660beacbef7bc887682f8))
* **deposit:** [AOU-476] display message if deposit is imported and contributor limit is
  reached ([2d8702d](https://gitlab.unige.ch/aou/aou-portal/commit/2d8702d870e4cc7c0333eb0ff562c5be61246204))
* **deposit:** [AOU-509] update deposit fields presentation ([2ffeb97](https://gitlab.unige.ch/aou/aou-portal/commit/2ffeb97458cd1f484ebd7af6fb304eb45786efd1))
* **depositContributor:** [AOU-408] when import, contributor type radio should be
  undefined ([1d3a4c4](https://gitlab.unige.ch/aou/aou-portal/commit/1d3a4c4390c8a37124ed7909bb79f70f97dfd2f2))
* **depositContributor:** [AOU-477] display contributor institution on contributor list if present and looks like to
  unige ([7471ece](https://gitlab.unige.ch/aou/aou-portal/commit/7471ece78cd32775d8bd84aea2b28ae1a6b40d7b))
* **depositContributor:** [AOU-478] display contributor unige structure on contributor list when selected from
  oracle ([636de0c](https://gitlab.unige.ch/aou/aou-portal/commit/636de0c78010c7c33daf6ebbd3e4f303e2811682))
* **depositList:** [AOU-452] updates in deposit columns list ([32af589](https://gitlab.unige.ch/aou/aou-portal/commit/32af58948eb777eb9deaa456f349f6bcb2b9a069))
* **depositType:** [AOU-440] select a defaut value in subtype field in deposit
  form ([7eb1db4](https://gitlab.unige.ch/aou/aou-portal/commit/7eb1db41c310c8fe52b1f10e4ad3a141ef667f91))
* **document file:** [AOU-399] allow to download file to confirm or already
  confirmed ([ff6641b](https://gitlab.unige.ch/aou/aou-portal/commit/ff6641b2ec1a78ab37ae212a887e2b0e21c24244))
* **license:** [AOU-218] use advanced search endpoint to search
  licenses ([d022580](https://gitlab.unige.ch/aou/aou-portal/commit/d022580fc33677d30387101b545dcef4ea04779d))
* **profil:** [AOU-469] update list notification type in
  profile ([fff593f](https://gitlab.unige.ch/aou/aou-portal/commit/fff593f54726a60d4d4c576604ef6527ac384a65))
* **research groups:** [AoU-363] Add research groups
  initialization ([071112f](https://gitlab.unige.ch/aou/aou-portal/commit/071112ffb0a6b9403297f40de257051fee3aae0c))

### Bug Fixes

* 397 add research group in admin-users ([57ae94f](https://gitlab.unige.ch/aou/aou-portal/commit/57ae94fe89f79ac0e27840a31717a1b0c0bb5faa))
* after rebase ([a0e71b3](https://gitlab.unige.ch/aou/aou-portal/commit/a0e71b3aeab21c0d6df9322e123dbefa82bed4c9))
* AOU-356 display error when delete structure with children ([dc08e9f](https://gitlab.unige.ch/aou/aou-portal/commit/dc08e9fc4dba0877e83b518a57f7e1d982f7788d))
* AOU-385 document file type in list should be translated ([bcab33d](https://gitlab.unige.ch/aou/aou-portal/commit/bcab33d44e0ac1abb326ef49753c44b79887c206))
* AOU-389 hide deposits to validate link when user has no validation
  rights ([e456b57](https://gitlab.unige.ch/aou/aou-portal/commit/e456b57b58e7ffb680adffccefedfd22f6d1bea5))
* AOU-391 deposit in list author column not show always
  author ([ce0a37c](https://gitlab.unige.ch/aou/aou-portal/commit/ce0a37c98e6cacd3bc9769a8abf4b0c64117337e))
* AOU-401 show contributors validation errors from backend ([720dc5a](https://gitlab.unige.ch/aou/aou-portal/commit/720dc5a7c4c2b1f0122d58dc37876e15638fccb6))
* AOU-406 allow html tags in the abstract field ([007e7e6](https://gitlab.unige.ch/aou/aou-portal/commit/007e7e63a1879282bdcc1a31b8f9d1f21a9bbed8))
* AOU-411 redirect to home page when invalid login ([897b941](https://gitlab.unige.ch/aou/aou-portal/commit/897b9413850af2b3cb52ccc44a2e973dfd9389ca))
* automatically scroll to middle when submit, transmits
  deposit ([ad136c1](https://gitlab.unige.ch/aou/aou-portal/commit/ad136c1f9f68bea2fac3587ab7ea494387412b31))
* avoid error on status check for deletable button ([19818a4](https://gitlab.unige.ch/aou/aou-portal/commit/19818a483d3d4c2c14e79e487557421c1bef7e27))
* avoid to display error on next step after import in creation
  step ([a21daf5](https://gitlab.unige.ch/aou/aou-portal/commit/a21daf5b0905f9853e6e561950580e25e886cbfa))
* **deposit contributor:** [AOU-515] when adding contributors the number of errors is not updated
  directly ([8244b84](https://gitlab.unige.ch/aou/aou-portal/commit/8244b84378148a8d23f60637387000879d510412))
* **deposit contributor:** [AOU-522] fix alignment of collaboration on contributor
  table ([c834f95](https://gitlab.unige.ch/aou/aou-portal/commit/c834f95d4009df5d4cf314e9672173eeb7e397ae))
* **deposit creation:** hide title language when not subtype
  selected ([7d0dc70](https://gitlab.unige.ch/aou/aou-portal/commit/7d0dc707670ac3baee3aec00d310b5633544e350))
* **deposit description:** [AOU-518] container title autocomplete doesn't always
  displayed ([affe765](https://gitlab.unige.ch/aou/aou-portal/commit/affe76510af89fede0eef53aea3fe32c9e12d5ee))
* **deposit description:** [AOU-520] add spinner for the journal search by title and by
  issn ([547567c](https://gitlab.unige.ch/aou/aou-portal/commit/547567c0dfc886519ad6b5de5f2aa3e341998b51))
* **deposit detail:** [AOU-517] show all comments in deposit detail
  view ([4eee6e3](https://gitlab.unige.ch/aou/aou-portal/commit/4eee6e3320952f2b3305096d0052e723b044f60a))
* **deposit file embargo:** [AOU-527] should be able to enter an embargo duration when date is in mm.yyyy format and fix gap of 1
  day ([a5a0faa](https://gitlab.unige.ch/aou/aou-portal/commit/a5a0faafb48e9273aff3e9029673664163367d3a))
* **deposit first step:** [AOU-494] subsubtype not saved ([89474ea](https://gitlab.unige.ch/aou/aou-portal/commit/89474eae2d34173f5610958e78c4efe0b3976b3b))
* **deposit first step:** remove undefined option sub sub
  type ([eb1374f](https://gitlab.unige.ch/aou/aou-portal/commit/eb1374f217ce3e6c39fdf911448496f57249c544))
* **deposit first step:** set title language by default to deposit
  language ([cff1170](https://gitlab.unige.ch/aou/aou-portal/commit/cff117012d67c9f2a07a140047325aaae748f4d2))
* **deposit list:** [AOU-531] when more than one author in deposit list, replace "..." by "et
  al." ([e62713c](https://gitlab.unige.ch/aou/aou-portal/commit/e62713c11b34bfd03034789a2306405dc324e350))
* **Deposit-comments-form:** [AOU-532] hidden edit buttons for comment not
  yours ([feafcf0](https://gitlab.unige.ch/aou/aou-portal/commit/feafcf0c96b69a977df99c55fe1c9d7263740d38))
* **deposit-document-file.container.ts:** use confirm instead of toBeConfirmed for
  action ([0bf490c](https://gitlab.unige.ch/aou/aou-portal/commit/0bf490cb09a6e373e07b58d5cb8035d7f92706e2))
* **deposit:** [AOU-388] remove delete button when a deposit is in
  validation ([85475f9](https://gitlab.unige.ch/aou/aou-portal/commit/85475f91a20fce7f0c52343d9418b956ec17728e))
* **deposit:** [AOU-461] values in repeatable fields are filled from the second
  field ([f43f628](https://gitlab.unige.ch/aou/aou-portal/commit/f43f628ddfc7645d53dd541126e6fb40961f27a9))
* **deposit:** [AOU-462] keywords are not managed correctly ([c00de5d](https://gitlab.unige.ch/aou/aou-portal/commit/c00de5d45e24042001412f0b722a6aeb40da8ece))
* **deposit:** [AOU-465] detect changes on form when error
  happend ([f056d31](https://gitlab.unige.ch/aou/aou-portal/commit/f056d3131090b52bdc3d13469d2807b32051ffe4))
* **DepositSummaryPresentational:** show DOI in the summary ([0007dc5](https://gitlab.unige.ch/aou/aou-portal/commit/0007dc559d11a7e712d4b130413d3a3192d5a953))
* **deposit:** unable to restore the sub sub type ([4337a48](https://gitlab.unige.ch/aou/aou-portal/commit/4337a4840397cb2735a4ff72902139333c35352f))
* display submit button on deposit with status feedback
  required ([cbb72ac](https://gitlab.unige.ch/aou/aou-portal/commit/cbb72ac08f58e8b6656c7a7940a3e6a9b87598cd))
* **en.json:** fix typo ([5da8096](https://gitlab.unige.ch/aou/aou-portal/commit/5da80960e330dfaa1378229fb8a1d13012b70e2f))
* english corrections ([64d6524](https://gitlab.unige.ch/aou/aou-portal/commit/64d652409cafbf5806085e7a4c6a208f7ab24f1c))
* error compilation ([71f6323](https://gitlab.unige.ch/aou/aou-portal/commit/71f6323e29c6bdadd2c9546b4a4d3c5a7e8fc43d))
* **genral:** avoid app to launch first time with invalid
  language ([7c8f844](https://gitlab.unige.ch/aou/aou-portal/commit/7c8f844b131c89ce063b446f8076523f42f71e4a))
* hide title language for sub type other than diploma ([3e3ba52](https://gitlab.unige.ch/aou/aou-portal/commit/3e3ba5287711247bad2c43fbedde21d4b70323b1))
* lint error ([5490605](https://gitlab.unige.ch/aou/aou-portal/commit/5490605a62015c186d960ba9f5ae9d0c937d2a72))
* lint error ([a2b33ad](https://gitlab.unige.ch/aou/aou-portal/commit/a2b33ad39e899f0a814f75d7844c9f0d4fb25699))
* lint error ([3a9058e](https://gitlab.unige.ch/aou/aou-portal/commit/3a9058e8d51ef5d766af9e089849701abdeff813))
* **login:** [AOU-498] when try to access to an url without token info we lose target page after
  login ([682228c](https://gitlab.unige.ch/aou/aou-portal/commit/682228c6776fba33d697dfb68f537b9585b588bd))
* redirection of deposit validation edition part and update deposit with cleaned info before
  submit ([7e9f2ae](https://gitlab.unige.ch/aou/aou-portal/commit/7e9f2aee07289e8d6c5c6992beccc95f526643f0))
* remove language for container title and block issn to 9
  chars ([5af997e](https://gitlab.unige.ch/aou/aou-portal/commit/5af997e88d72026a5a66d1ab18dadb59f027452f))
* script to build solidify ([caa2ba9](https://gitlab.unige.ch/aou/aou-portal/commit/caa2ba987d86f671b5f69849508b306cb602423b))
* **token dialog:** [AOU-495] migrate for new version
  solidify ([e839368](https://gitlab.unige.ch/aou/aou-portal/commit/e839368cf0fa3d2a59460268710f8c275006cc36))
* update to solidify 1.2.15 to fix deposit document file type
  filter ([fc0b4a0](https://gitlab.unige.ch/aou/aou-portal/commit/fc0b4a02443ddcc462e943982b16e22b15f82d50))
* validation util by updating solidify frontend to 1.3.11 ([0990452](https://gitlab.unige.ch/aou/aou-portal/commit/09904520925535f0516e7ebd04efc07fd6c1aa44))

## [0.0.2](https://gitlab.unige.ch/aou/aou-portal/compare/aou-0.0.1...aou-0.0.2) (2021-03-31)

### Features

* 174 define an order property on structures ([7e6a088](https://gitlab.unige.ch/aou/aou-portal/commit/7e6a088d9c8417846eef121f5e7b4ce290d8abf8))
* 1806 remove breadcrumb and display active menu ([d9ca7e5](https://gitlab.unige.ch/aou/aou-portal/commit/d9ca7e57610267fa5effc16529fbee1a3e03d88b))
* 196 allow download document file after upload ([3884d6e](https://gitlab.unige.ch/aou/aou-portal/commit/3884d6eac6d44bdad887ed9320d584b4aba7f744))
* 221 improve comments components ([e4968c6](https://gitlab.unige.ch/aou/aou-portal/commit/e4968c6c1debf69925090e48ccd97e9deabb6d10))
* 227 add modal to request validation structure if no present on deposit and also validation before
  submit ([b042b03](https://gitlab.unige.ch/aou/aou-portal/commit/b042b03e02e2fd7fe1321769c6e4e1cc14b75a25))
* 227 allow validator to transfer deposit to an other
  structure ([e50b93d](https://gitlab.unige.ch/aou/aou-portal/commit/e50b93df98164206b29347891ce48e066368ad0e))
* 231 deposit redirect to deposit lists with an error message if we try to access a deposit we are not allowed to
  see ([61a01c4](https://gitlab.unige.ch/aou/aou-portal/commit/61a01c466524841779c105a6d2fe9356482a259c))
* 233 contributors form ask form confirmation if no contributor is
  unige ([c7c1a04](https://gitlab.unige.ch/aou/aou-portal/commit/c7c1a04339f98b63bb75ea92f13e53568f9b3185))
* 245 filter contributors types by deposit subtype ([663fc29](https://gitlab.unige.ch/aou/aou-portal/commit/663fc2937d3bfebd55c28379a36338af7e99612e))
* 247 deposit lists show more infos in each row ([1d9430d](https://gitlab.unige.ch/aou/aou-portal/commit/1d9430da2b02e9e8947717e5ac81c20fa59b7edc))
* 248 deposit form initialize form with default data ([7ce033f](https://gitlab.unige.ch/aou/aou-portal/commit/7ce033ffb7340990a94e1cdff677b1a034a2d09d))
* 248 move funding from description step to contributor step ([9405237](https://gitlab.unige.ch/aou/aou-portal/commit/9405237044692920034cd21aaa9f6c79f280f8f1))
* 252 separate list comments into 2 lists ([bfaf1d8](https://gitlab.unige.ch/aou/aou-portal/commit/bfaf1d8f38dcb7fb5a8c003b938fdfaa22b03e6f))
* 257 complete UI to give structure validators rules ([7f8bfc2](https://gitlab.unige.ch/aou/aou-portal/commit/7f8bfc2ce588419c30ab6d0f034d3497d26d3b30))
* 259 save when press previous on deposit ([40596a1](https://gitlab.unige.ch/aou/aou-portal/commit/40596a1fe2402ab23528f9124baf0df88102f4e8))
* 261 as validator allow to validate, ask feedback or reject a
  deposit ([bc0c2ff](https://gitlab.unige.ch/aou/aou-portal/commit/bc0c2ff692c34e1c45df54c7c79e3b6286e48f31))
* 262 dedicated component for access level with embargo on deposit document file
  list ([403476a](https://gitlab.unige.ch/aou/aou-portal/commit/403476ab4b4e88f911220cd555ef0de7abc5fa7f))
* 272 display all fields values in deposit summary and
  detail ([188d0a7](https://gitlab.unige.ch/aou/aou-portal/commit/188d0a795bbdfbacf9762e383a2a627e5731c451))
* 273 add dois managment in deposit container identifiers ([ba3eac3](https://gitlab.unige.ch/aou/aou-portal/commit/ba3eac355081caed50ee4ba9bf99d165e9274747))
* 290 change format date on deposit description and remove validation
  rule ([7aeb79f](https://gitlab.unige.ch/aou/aou-portal/commit/7aeb79f6997a74e4d838f86c7d98fff2b77020cb))
* 291 create stepper component for deposit ([bdf5835](https://gitlab.unige.ch/aou/aou-portal/commit/bdf58352c803fbf7419022c5f7d99a6dffbe08c0))
* 292 improve multilanguage field in summary ([3c0534c](https://gitlab.unige.ch/aou/aou-portal/commit/3c0534cbf4c9a9ce30696712fc993b70ef0ce288))
* 294 set material input label in position always open ([ba521a8](https://gitlab.unige.ch/aou/aou-portal/commit/ba521a87eadc169ddb435694af2f98da3ea241af))
* 299 adapt relation person and structure in profile ([21cd86b](https://gitlab.unige.ch/aou/aou-portal/commit/21cd86b3370fd833154d4219472cac1db210aee5))
* 302 adjustment on deposit step file ([1d91918](https://gitlab.unige.ch/aou/aou-portal/commit/1d91918c3e42968ee63f1d8fec72744194c5be82))
* 305 deposit back button on detail or create should open last list tab opened or default tab if null depending of deposit validation or my
  deposit ([0b263b5](https://gitlab.unige.ch/aou/aou-portal/commit/0b263b5850beba52dd1e3aa6b7fb9d7a9ed3b408))
* 306 reorganize user profile for a better display ([9e35967](https://gitlab.unige.ch/aou/aou-portal/commit/9e35967e7dc00da27dff619e1476d4597c78306d))
* 307 and 343 add button to directly create deposit with import option selected and hide import option when
  used ([58238ff](https://gitlab.unige.ch/aou/aou-portal/commit/58238ff0f836ddada94c475835b128843c7984b1))
* 317 allow to submit for validation a deposit directly from
  detail ([a9309e5](https://gitlab.unige.ch/aou/aou-portal/commit/a9309e566cfc20ab44ee60d70eb4f50f03bed976))
* 319 integrate deposit import service with doi ([4d3f74d](https://gitlab.unige.ch/aou/aou-portal/commit/4d3f74da5906b82a85d13fccc3c6d229db1ab746))
* 327 deposit display dialog with error message with try to go next without pdf file
  uploaded ([cfb4b48](https://gitlab.unige.ch/aou/aou-portal/commit/cfb4b48b3d30ab5fa7b1543d5e793d6602323377))
* 329 hide language on deposit link ([e9fec81](https://gitlab.unige.ch/aou/aou-portal/commit/e9fec81fcef742758bdf17a82e49b5cc50763d31))
* 336 display deposit subtype in banner edit mode ([53030c1](https://gitlab.unige.ch/aou/aou-portal/commit/53030c1023a1850e065c991447a607e4921a5961))
* 338 deposit file upload change behavior when click to
  upload ([797a530](https://gitlab.unige.ch/aou/aou-portal/commit/797a5305b1a114a76470a31dd7d023dbcb2ce96c))
* 340 deposit display in summary member of unige in pink ([f9424e4](https://gitlab.unige.ch/aou/aou-portal/commit/f9424e4259561cad26959c3bbd5f2991da3671a5))
* add default sorting on deposit list to show most recently update
  first ([98f86ea](https://gitlab.unige.ch/aou/aou-portal/commit/98f86ea4911593b9888d9c1f1b7893287c312674))
* add deposit banner depending of status ([7fea9a2](https://gitlab.unige.ch/aou/aou-portal/commit/7fea9a2069161e6d467aa125595ea2807f69e4bd))
* add environment variable to conditionnaly display at least one pdf file mandatory
  dialog ([58169f3](https://gitlab.unige.ch/aou/aou-portal/commit/58169f316a65a1b8b3c5e9f5b017c4307a1acfc2))
* add solidify tooltip on ellipsis on all material input ([2ddf336](https://gitlab.unige.ch/aou/aou-portal/commit/2ddf336b715d20b24ccb00da7b8dfb130780ec95))
* allow to enable revision and resume a deposit in error ([c5e5b96](https://gitlab.unige.ch/aou/aou-portal/commit/c5e5b9655e87a40e124de0557028018069f5a23d))
* allow to go to orcid page ([33cf751](https://gitlab.unige.ch/aou/aou-portal/commit/33cf751ba5bbb58b19bccffac265accab152ea2d))
* allow to have html tag into deposit title ([9d49283](https://gitlab.unige.ch/aou/aou-portal/commit/9d49283374c57ee4a74b5d6f10f9816d5ad13840))
* button to confirm a document file ([e720177](https://gitlab.unige.ch/aou/aou-portal/commit/e720177053fdfb17a75d1e894ce6183b4f04d1c9))
* **contributors:** allow searching with empty firstname ([e76f833](https://gitlab.unige.ch/aou/aou-portal/commit/e76f83326e1f893ba43e9a2eb13e418d331abe66))
* display in deposit summary the submitter ([78961d6](https://gitlab.unige.ch/aou/aou-portal/commit/78961d6d7b1d096441da9e2fd1d15d5841c38e7f))
* migrate new confirm dialog feature to solidify and update to solidify
  1.1.23 ([f4413ac](https://gitlab.unige.ch/aou/aou-portal/commit/f4413ac465e233de46c7ea3b698460d82c8d6417))
* show language validation errors in text with languages
  fields ([32315e5](https://gitlab.unige.ch/aou/aou-portal/commit/32315e5f6344189565969636e400647a62b695e3))
* update to solidify 1.2.4 to allow to block when there is no
  contributor ([02f6aac](https://gitlab.unige.ch/aou/aou-portal/commit/02f6aacd0168aaea4ade298d0788d34d0d586718))

### Bug Fixes

* 212 external service identifier example is hidden when field in
  error ([8362daf](https://gitlab.unige.ch/aou/aou-portal/commit/8362dafd2ce7dc837ef0ede634c8b8db57016335))
* 225 update to solidify 1.1.18 to fix textarea does not allow to enter multilines on chrome and
  firefox ([695b18f](https://gitlab.unige.ch/aou/aou-portal/commit/695b18f882ce858b4052c64f4fe423d2781f6255))
* 226 deposit form add more space to enter the title on the first
  step ([fe4906f](https://gitlab.unige.ch/aou/aou-portal/commit/fe4906fd671aef8fc03aa238317155410e5993f1))
* 228 scroll to top of page when landing on a new deposit form
  step ([5ddef99](https://gitlab.unige.ch/aou/aou-portal/commit/5ddef99795352a2e9a5756c2a9f6ffeac2bba880))
* 236 missing translation file status in history dialog ([7e66778](https://gitlab.unige.ch/aou/aou-portal/commit/7e667788f8a08320264948dbd6ea614536bee856))
* 269 unable to unselect a structure in the structure tree on
  deposit ([918871c](https://gitlab.unige.ch/aou/aou-portal/commit/918871c8a59cfb8756633cfe31c3279d0ee98964))
* 284 collection code input field allow letters ([5d920f4](https://gitlab.unige.ch/aou/aou-portal/commit/5d920f4b89231df9571b5d41a80141d8f1b6ff6b))
* 301 set button grey on stepper when form invalid and move is affiliated unige at first step and convert it in
  radio ([b364be3](https://gitlab.unige.ch/aou/aou-portal/commit/b364be35faf9f60aca40db8ef9fef1ab700b409d))
* 310 contributors form: selecting "non UNIGE" on a "UNIGE member" does not update the
  form ([8e57b44](https://gitlab.unige.ch/aou/aou-portal/commit/8e57b44d3ff3488406204684964ac8de2ced12b3))
* 313 selecting validation structure at deposit submission doesn't
  work ([a3f340c](https://gitlab.unige.ch/aou/aou-portal/commit/a3f340cfa748e21fee564c16a95fe3f8173d3674))
* 314 validation error is not displayed under language ([1188881](https://gitlab.unige.ch/aou/aou-portal/commit/1188881a00c8b408b4ba3d3469d116307630de49))
* 316 page doesn't refresh when sending a deposit to
  validation ([024763e](https://gitlab.unige.ch/aou/aou-portal/commit/024763e01ef625e794abc785f1ac26721caeebdd))
* 318 admin structure tree click in tree item cause invalid
  redirection ([dd2f1e3](https://gitlab.unige.ch/aou/aou-portal/commit/dd2f1e397996d84d5dca5f3c61bafdfec3fddeb1))
* 320 removing validation rules from an user does not enable the save
  button ([97fe1e3](https://gitlab.unige.ch/aou/aou-portal/commit/97fe1e3abbaf0d25f38161d3061dba03416f9765))
* 324 group dropdown for select type and subtype using
  optgroup ([3a1f7ec](https://gitlab.unige.ch/aou/aou-portal/commit/3a1f7ec8f47451ae12b1a0b390aeb6aa11cd67a9))
* 331 deposit description remove value by default on date ([1c15bbe](https://gitlab.unige.ch/aou/aou-portal/commit/1c15bbeb17d133630bb6a8b4ccba3efa0291509d))
* 334 deposit stepper should not allow to change step when current form in current step is
  invalid ([eef8f5d](https://gitlab.unige.ch/aou/aou-portal/commit/eef8f5d2aba7f14867ea5df5c5c881cbc8e6211a))
* 339 deposit contributor after save member of collaboration move into contributor list when should
  not ([4ad9c1b](https://gitlab.unige.ch/aou/aou-portal/commit/4ad9c1b2f4d7f0fe63e3ea1fec9442f929974bdd))
* 341 dates fields are not filled on description step when importing metadata with
  DOI ([98ccdc5](https://gitlab.unige.ch/aou/aou-portal/commit/98ccdc558b324b6e40a8642033bc75575bd67b09))
* 353 when validation error fix several bug ([a96c5fe](https://gitlab.unige.ch/aou/aou-portal/commit/a96c5feaa930028c8bc77c9e7b849a5a85fc8474))
* 371 add empty value in subsubtype dropdown ([7efe9a1](https://gitlab.unige.ch/aou/aou-portal/commit/7efe9a1b3561108d94bac61c838122e09553bb27))
* 373 improve display of non existent value on input ([f67db1d](https://gitlab.unige.ch/aou/aou-portal/commit/f67db1df95102764630c98f7138db81604356046))
* 374 update to solidify 1.2.12 to fix tooltip on ellipsis in disable
  input ([059544d](https://gitlab.unige.ch/aou/aou-portal/commit/059544d76050c44a68884036490c1eae888fa051))
* 375 use separate spinner for comment and validator comment ([9229d45](https://gitlab.unige.ch/aou/aou-portal/commit/9229d4570524929ade3517420a9d60ab633dc479))
* add spinner on deposit list ([5884f14](https://gitlab.unige.ch/aou/aou-portal/commit/5884f1449d43e31016a66bcab8c64622ece6990a))
* allow admin to access to user ([0d3544a](https://gitlab.unige.ch/aou/aou-portal/commit/0d3544af9823973c4139f9df74cd50f4f6d57e6c))
* allow to choose two time the same file to upload ([cf40a5e](https://gitlab.unige.ch/aou/aou-portal/commit/cf40a5eb3b8cda8dc4f687ce7bc7b7ffabd201fb))
* allow to edit a deposit in error ([4e16f02](https://gitlab.unige.ch/aou/aou-portal/commit/4e16f02cb88cb0f549152c937a021f7fc7b33743))
* avoid error in summary when no data selected in abstract ([b9cd9b9](https://gitlab.unige.ch/aou/aou-portal/commit/b9cd9b9a58634631c82192593fa359588ca3ef47))
* avoid html tag in title into comment dialog ([fa6f9bf](https://gitlab.unige.ch/aou/aou-portal/commit/fa6f9bfc2789d76a44ad9634160fd121a9cb8672))
* avoid to redirect after deleting a deposit from a list tab ([2db1e41](https://gitlab.unige.ch/aou/aou-portal/commit/2db1e4189ad80c25598e499f36ce26e8bdcbe4d3))
* change date format initialization ([0a5c5c2](https://gitlab.unige.ch/aou/aou-portal/commit/0a5c5c2ff67f1e0a93d2641ca94bc87543925457))
* change label type and subtype ([cca70b7](https://gitlab.unige.ch/aou/aou-portal/commit/cca70b75ffd52b5ba821550f14666961c358dc15))
* confirmed deposit display in green ([460c28e](https://gitlab.unige.ch/aou/aou-portal/commit/460c28e138739d168560728b9c48394155e6066b))
* create deposit structure link on submit with validation structure and when
  redirect ([a4e679f](https://gitlab.unige.ch/aou/aou-portal/commit/a4e679f4f58d9f015f6e441d9346e9e95c5fdddf))
* deposit submission with validation structure ([f43bdef](https://gitlab.unige.ch/aou/aou-portal/commit/f43bdef5d4f41496c56655f26c38c45ba9dd87a9))
* deposit summary in error when there is no doi ([7e3ca7d](https://gitlab.unige.ch/aou/aou-portal/commit/7e3ca7d4e320a354e1463dd1e748e7071e865234))
* deposit summary when data are not provided ([92e9b07](https://gitlab.unige.ch/aou/aou-portal/commit/92e9b07d9f77e8d9396c1fca882bb6eccc91a5a8))
* **DepositAction:** set correct type values in SubmitSuccess and SubmitFail
  classes ([2e51395](https://gitlab.unige.ch/aou/aou-portal/commit/2e5139536519b23b92585fc9980d4d20d7417ae6))
* **DepositDetailRoutable:** correctly manage approval dialog
  cancel ([bb6281b](https://gitlab.unige.ch/aou/aou-portal/commit/bb6281b2ffe6418d0349557ccbcd9c75d9453fda))
* **DepositDetailRoutable:** rename 'item' into 'joinResource' required after backend
  update ([3f1dacd](https://gitlab.unige.ch/aou/aou-portal/commit/3f1dacdf836735a6ff4a448789c89ad9f476172a))
* **DepositFormPresentational:** do not multiplicate contributors if clicking many times on import metadata
  button ([bf1bdd8](https://gitlab.unige.ch/aou/aou-portal/commit/bf1bdd85e608d0381e64235bae14318893abc21f))
* **DepositFormPresentational:** prevent errors when trying to set missing default
  data ([051e0ac](https://gitlab.unige.ch/aou/aou-portal/commit/051e0ac9b07090446d86f779d5c2d97ed53f1b05))
* display doi in deposit list action placeholder ([f5df1f1](https://gitlab.unige.ch/aou/aou-portal/commit/f5df1f18853cbd20aabd5adf7e1e47e6f48d5924))
* display of doi in summary ([450fbe2](https://gitlab.unige.ch/aou/aou-portal/commit/450fbe2919a8ddb48b18a19a6870cd9a8956a64b))
* endpoint for list validation deposit ([a757fa4](https://gitlab.unige.ch/aou/aou-portal/commit/a757fa4f01d31d6d9eaf6998e545921bcab72c83))
* grouping subtime in type diaplay wrong name for type ([6617b5e](https://gitlab.unige.ch/aou/aou-portal/commit/6617b5ecbee4b0c1dd2eedd88f923e342db6d344))
* lint error ([0e315c0](https://gitlab.unige.ch/aou/aou-portal/commit/0e315c0e9421069718167c66a14f7194e32e82c3))
* migrate typo dlcm icon ([f48a03f](https://gitlab.unige.ch/aou/aou-portal/commit/f48a03f4a8beeefbd19a3a199df85938cbced77d))
* move refresh into documentfile confirm action ([4aa317c](https://gitlab.unige.ch/aou/aou-portal/commit/4aa317cfde54057252af16d5b6cab00289bd388e))
* multilanguage on summary ([b9715aa](https://gitlab.unige.ch/aou/aou-portal/commit/b9715aaa0808578c458f214cdcdb6a8dcfe5ae15))
* open confirm dialog when submit deposit in validation without already
  structure ([268c5b4](https://gitlab.unige.ch/aou/aou-portal/commit/268c5b452e0f20943982d43aca5aa4915792c0a7))
* performance issue that crash the browser ([fce8338](https://gitlab.unige.ch/aou/aou-portal/commit/fce8338520aeae5dd0c7a7d8b1ee314b5943cf27))
* publication year displayed in deposit list ([ae79405](https://gitlab.unige.ch/aou/aou-portal/commit/ae7940530a64c2913fb9c1d9ad42ce5752e174a0))
* reduct clickable zone on tree to match with highlighted
  part ([70c3d95](https://gitlab.unige.ch/aou/aou-portal/commit/70c3d952883910f61aceef15f9d55b8b50a62242))
* remove file input automatically clicked at open ([4320770](https://gitlab.unige.ch/aou/aou-portal/commit/43207706fff5fc8603b55fc4aea4db63ec3cfe6a))
* remove translate on banner message ([a430483](https://gitlab.unige.ch/aou/aou-portal/commit/a430483431d1c42cda3d30a745894fc9da868311))
* removing unused files ([bde3f4c](https://gitlab.unige.ch/aou/aou-portal/commit/bde3f4cd59dc0036efc1667f83ce85a07b4ebca6))
* scroll to top when change of step ([72917b0](https://gitlab.unige.ch/aou/aou-portal/commit/72917b018f3cf279fd27e2ee768fa2260af75fbd))
* scroll to top when open a deposit ([2c0044b](https://gitlab.unige.ch/aou/aou-portal/commit/2c0044b65776d9974948269212fa4c97192eb9fb))
* show selected structures in admin user page ([12849e7](https://gitlab.unige.ch/aou/aou-portal/commit/12849e71dee92c830edf025eb6a699e41d1db60c))
* shown error message when importing from service with identifier not
  valid ([213cf5f](https://gitlab.unige.ch/aou/aou-portal/commit/213cf5f5d5dcca359daf468c70859fc547d6d35f))
* sonar duplicate code issues ([afa8776](https://gitlab.unige.ch/aou/aou-portal/commit/afa8776c2c480ffc557b5f254d8df44c05bd07b4))
* spinner on contributor page should only appear on table line not
  global ([6659421](https://gitlab.unige.ch/aou/aou-portal/commit/66594217a973068fde6f717acc08488910d130bd))
* spinner on type on summary ([0bb062a](https://gitlab.unige.ch/aou/aou-portal/commit/0bb062a9b2712374d8dc7e5f7dad97af5d86b5b0))
* submit deposit from detail ask validation structure when should
  not ([47fd22f](https://gitlab.unige.ch/aou/aou-portal/commit/47fd22f9aba7966cc4f3489ee0b587834f8804e8))
* text updates ([5f9f3c5](https://gitlab.unige.ch/aou/aou-portal/commit/5f9f3c5ca056d66cc592f47ba40ae2de4a9ce33e))
* typo on de.json file that break extration of translation ([f9262ec](https://gitlab.unige.ch/aou/aou-portal/commit/f9262ec94037a44c5741d317ca5f367b703b3511))
* typo that break the app ([b8f9bba](https://gitlab.unige.ch/aou/aou-portal/commit/b8f9bba51b3f1437f79c94800b4ce0f4b7ce911a))
* unblock submission of deposit with files confirmed ([cb867a0](https://gitlab.unige.ch/aou/aou-portal/commit/cb867a044dfdb443adf6086b1b0fffbf847e1ab8))
* update to solidify 1.2.11 to fix long string without space char in data
  table ([7fd7990](https://gitlab.unige.ch/aou/aou-portal/commit/7fd7990939d1e30c8cc9b16bcbcbb5d40fee323e))
* update to solidify 1.2.6 and define explicit status error ([727ea82](https://gitlab.unige.ch/aou/aou-portal/commit/727ea827f4a7bf346eab10f0f65c0fbca1427ddd))
* use 'deposit' word in french translations ([4072bbb](https://gitlab.unige.ch/aou/aou-portal/commit/4072bbb08069596fcf52a1e3ccd8c4f81bc0dd69))
* validation error on second date input ([cea9bd7](https://gitlab.unige.ch/aou/aou-portal/commit/cea9bd741a35f79f2b28735265bdd47661f916c8))
* wait validation rigth before displaying profile and hide this section if
  empty ([5062fb6](https://gitlab.unige.ch/aou/aou-portal/commit/5062fb66a6702a6d9355d66b87c1812258222061))
* wrong import that break build ([4b8d046](https://gitlab.unige.ch/aou/aou-portal/commit/4b8d046a5324990a8f32507e26ab4cb886380dc7))

### 0.0.1 (2021-02-01)

### Features

* 118 add structure admin page ([a567d87](https://gitlab.unige.ch/aou/aou-portal/commit/a567d872bd0a9eaa44ebfe9bb159baaf84b58fa8))
* 118 add tree component for manage structure ([7c5f7c6](https://gitlab.unige.ch/aou/aou-portal/commit/7c5f7c60a3a19558ab4164aeeab69bb0919a9127))
* 120 create research groups admin page ([dd28f65](https://gitlab.unige.ch/aou/aou-portal/commit/dd28f654d1dc78fc85e363e4b6933337f623e242))
* 138 update solidify to 1.1.6 to apply generic way to display resource not found
  page ([5c43774](https://gitlab.unige.ch/aou/aou-portal/commit/5c437748f13170dc33d7417a253fd782540aac6e))
* 147 profil a user must be able to select a list of structures he belongs
  to ([35480ea](https://gitlab.unige.ch/aou/aou-portal/commit/35480eaf76be5a338d15fa1a87c8e41808b295bc))
* 151 list only publication created by me ([0d78552](https://gitlab.unige.ch/aou/aou-portal/commit/0d785524403d909077eca3381c37c03d8a8c9ea9))
* 155 allow user to select notification type on user profil
  modal ([125f6e6](https://gitlab.unige.ch/aou/aou-portal/commit/125f6e6929c24dd12829031ef08b2b60825db919))
* 156 build the publication form ([2188460](https://gitlab.unige.ch/aou/aou-portal/commit/2188460399fd6164add41d89debd6d43c24bc0ad))
* 156 drive deposit form by url routing ([5b1cb35](https://gitlab.unige.ch/aou/aou-portal/commit/5b1cb35b4ead5b22096f7fa2fb28713209836923))
* 157 create a component to search for contributors ([b0e0cef](https://gitlab.unige.ch/aou/aou-portal/commit/b0e0cef31b42b0ea05ea8a4de9aa883e6d4b2b04))
* 160 admin person allow to select role for a structure ([c6a21e4](https://gitlab.unige.ch/aou/aou-portal/commit/c6a21e45ef51c2412f001952d53414cec61804c4))
* 161 admin page for person merge to user page ([e055360](https://gitlab.unige.ch/aou/aou-portal/commit/e055360b6109a25bef5a358dc9f4178065ffce4b))
* 166 create component to manage publication files ([127e80d](https://gitlab.unige.ch/aou/aou-portal/commit/127e80d295d60556f22f023253eaa40a45a68544))
* 178 add possibility to indicate that the portal is a test
  version ([bf1dd49](https://gitlab.unige.ch/aou/aou-portal/commit/bf1dd499da8742a6a8ebb02d481b1aadc3431ddf))
* 190 deposit form allow to indicate that deposit is create from manual
  input ([e1baa70](https://gitlab.unige.ch/aou/aou-portal/commit/e1baa7059e2f3c5cf1c1b3fc7fe58802b86d9b23))
* 191 add acadenic structure on deposit description step ([606587c](https://gitlab.unige.ch/aou/aou-portal/commit/606587cf40bb3d5fa171db1fd19a374dfe596213))
* 191 add advanced input on deposit description section ([70122a9](https://gitlab.unige.ch/aou/aou-portal/commit/70122a9a47dbf9846086d3b8ca1c3784d98ebdc3))
* 191 add basic input on deposit description step ([b5140b2](https://gitlab.unige.ch/aou/aou-portal/commit/b5140b2177322f28ee3c4b1fbd060164d69d116d))
* 191 add collection input on deposit description step ([dbd51f3](https://gitlab.unige.ch/aou/aou-portal/commit/dbd51f3599e2211c596b6b8af5252fb08e3813e2))
* 191 add containers input on deposit description step ([e74aada](https://gitlab.unige.ch/aou/aou-portal/commit/e74aadae2102c847eb651801b8b421116c069512))
* 191 add dates input on deposit description step ([f3f9212](https://gitlab.unige.ch/aou/aou-portal/commit/f3f9212269aea1ea8bbc137d5cae42ecb9641852))
* 191 add funding input on deposit description step ([238f1fa](https://gitlab.unige.ch/aou/aou-portal/commit/238f1fa311c10de22956edf1348cd972baefacb3))
* 191 add input classification on deposit description step ([d7ccca4](https://gitlab.unige.ch/aou/aou-portal/commit/d7ccca4a36bd4d5aa15e51f6befc8b2fe5640075))
* 191 add input page for deposit description step ([6e61319](https://gitlab.unige.ch/aou/aou-portal/commit/6e613191a1b0f7dcbbf325a7ea7a0460c44b1847))
* 191 add input publication identifier on deposit description
  step ([6b54be1](https://gitlab.unige.ch/aou/aou-portal/commit/6b54be1a79f6664d55594367593ea3d14f35b911))
* 191 add input right on deposit description step ([6308b1d](https://gitlab.unige.ch/aou/aou-portal/commit/6308b1de6b475935508eafa678e9ff18e4ba8827))
* 191 add link input on deposit definition step ([99340a6](https://gitlab.unige.ch/aou/aou-portal/commit/99340a6bccf7dac31afe7618f2b76093d9fe7ac3))
* 191 allow to cancel language selected on text language input (exept for original
  title) ([d1e2fbf](https://gitlab.unige.ch/aou/aou-portal/commit/d1e2fbf37cf17835ec6347bbde95cd4e9d0e4a22))
* 209 allow to display validation error from backend on deposit section and
  field ([5dd9d9a](https://gitlab.unige.ch/aou/aou-portal/commit/5dd9d9a115607acd55d250bd8f4f33dc65144430))
* 210 list language from back on deposit form description
  step ([8b3b60d](https://gitlab.unige.ch/aou/aou-portal/commit/8b3b60ddf917d5325652b7dbd635165e279b0ff6))
* add a button to init structure when use is root ([177fdd9](https://gitlab.unige.ch/aou/aou-portal/commit/177fdd9296be246251ddaed991d982c81bf2867a))
* add admin module and main page ([aea4031](https://gitlab.unige.ch/aou/aou-portal/commit/aea4031d72e36a46559abfbad934107d5782582a))
* add button that allow to save deposit form ([4fd755c](https://gitlab.unige.ch/aou/aou-portal/commit/4fd755cc7adbc0b89a7728d6f14749aaa57f7c3a))
* add deposit detail info on contributors ([813c472](https://gitlab.unige.ch/aou/aou-portal/commit/813c472a3f7fde379ee126b1aae8df780994ad1d))
* add floating label always on some field to show the
  possibility ([3e5f0cf](https://gitlab.unige.ch/aou/aou-portal/commit/3e5f0cf3daaca1a5caa66c1e2c8417e52fe7d06d))
* add licenses admin pages ([28f3a35](https://gitlab.unige.ch/aou/aou-portal/commit/28f3a359e8913674c90f5672347514977fcb35d6))
* add publication list with tab on admin section ([61b6702](https://gitlab.unige.ch/aou/aou-portal/commit/61b6702945b4ca40c9b1408f78d35292be5d8f95))
* add radio is collaboration and logical arround this input ([e67c447](https://gitlab.unige.ch/aou/aou-portal/commit/e67c4479c20c1528a8eab30bd7d9e2d268677839))
* add repeatable input citation, abstract and group on deposit document
  step ([d6213b7](https://gitlab.unige.ch/aou/aou-portal/commit/d6213b7f970a556fec4b74165764820cbd3d6003))
* add roles admin pages ([ea98eec](https://gitlab.unige.ch/aou/aou-portal/commit/ea98eec6c6bd17794e9842805a311fca0bebc758))
* add status history for document file in publication ([d860130](https://gitlab.unige.ch/aou/aou-portal/commit/d860130c2199056c2860752da62f08ac3fcdb980))
* add status history for document file in publication ([b3b7a67](https://gitlab.unige.ch/aou/aou-portal/commit/b3b7a67d9b37b37470fdcca90b540b052b10343e))
* add status history for document file in publication ([f182bdd](https://gitlab.unige.ch/aou/aou-portal/commit/f182bddc3d228f9e24b8faeb112f0c59c45947b5))
* add sub subtype for deposit first step form ([72672b2](https://gitlab.unige.ch/aou/aou-portal/commit/72672b2f18aa4f03f787f732d95f9d4ce0c4a949))
* add tooltip on input method for deposit ([19d4a4d](https://gitlab.unige.ch/aou/aou-portal/commit/19d4a4d0df796c1782ecf18ed0cf27e9f822a3b1))
* added status history dialog for publication ([dc734a0](https://gitlab.unige.ch/aou/aou-portal/commit/dc734a0f62ea9ba4db2b7303320326f6b7a4b400))
* added user admin pages ([075cd9b](https://gitlab.unige.ch/aou/aou-portal/commit/075cd9bbd8d5cebf53f8e91f79bff9e8215b3992))
* allow to load modules endpoint dynamically ([ab6a9d6](https://gitlab.unige.ch/aou/aou-portal/commit/ab6a9d641874599b3f59f108ac047708608715c7))
* allow to remove repetable element on deposit description
  form ([d5d1849](https://gitlab.unige.ch/aou/aou-portal/commit/d5d18497b66693c7eb0928583bbebf20c6156b90))
* allow to sort contributor ([27ce6d6](https://gitlab.unige.ch/aou/aou-portal/commit/27ce6d69198e66281402b9d75964ba939ae96e0c))
* allow to submit deposit for validation ([c635399](https://gitlab.unige.ch/aou/aou-portal/commit/c63539915012fde941909d1983a7e33247e8fd71))
* an user can add research groups from his profile ([a2bb5be](https://gitlab.unige.ch/aou/aou-portal/commit/a2bb5be623c6343bebf42845635cbfb23672a73a))
* change input deposit description group code as number ([43f1032](https://gitlab.unige.ch/aou/aou-portal/commit/43f1032719ac00cb14c0013cd18442d5b004a249))
* create language shared state ([6ced340](https://gitlab.unige.ch/aou/aou-portal/commit/6ced340013dd2c9ccc2476ff6c0e0d1546f6e70b))
* create page dedicated to list deposit to validate ([ba9f1d2](https://gitlab.unige.ch/aou/aou-portal/commit/ba9f1d28779565a8233bcad1a10e61a1206ebd12))
* deposit contributor add author correspondant with logical and bulk action
  button ([b0c2eb9](https://gitlab.unige.ch/aou/aou-portal/commit/b0c2eb9be28e6fdea4d459ed7c9efbd5b049a608))
* display a message when there is no structure to display ([d529e3d](https://gitlab.unige.ch/aou/aou-portal/commit/d529e3d832f44c1f62917325b9a53b912f69ed34))
* display embargo info on deposit file info ([7986909](https://gitlab.unige.ch/aou/aou-portal/commit/7986909db48abe99b26ec2e7a17b6151c24de00a))
* display some deposit detail on summary part ([6ea2698](https://gitlab.unige.ch/aou/aou-portal/commit/6ea2698c79102ffea0a8b1703192159844e5c0d6))
* enable documentation integration ([9b677ce](https://gitlab.unige.ch/aou/aou-portal/commit/9b677ce8635d96c0132e3882b8aa04184ed206be))
* finish research group admin page ([1eb39b4](https://gitlab.unige.ch/aou/aou-portal/commit/1eb39b406ffb227758f5c5ba3f59403986a3a117))
* implement new features on collaborator deposit step ([1627bf6](https://gitlab.unige.ch/aou/aou-portal/commit/1627bf679be9d1ded602a5aea5f5ce53aebb7fc0))
* initialized skeletton of app from dlcm portal ([b6010f0](https://gitlab.unige.ch/aou/aou-portal/commit/b6010f079c3ac31e25495712f5c0aaaae12ee276))
* let keywords contain spaces ([0d1cb4c](https://gitlab.unige.ch/aou/aou-portal/commit/0d1cb4c6687777a72a16e2f672e7e9387e36f36d))
* migration on solidify 1.0.0 ([c83e4aa](https://gitlab.unige.ch/aou/aou-portal/commit/c83e4aae67cccdadd18a5854e2b6ffad25225280))
* move publication outside admin ([d06d070](https://gitlab.unige.ch/aou/aou-portal/commit/d06d070a36fd8abbabbbc1699210a9d29edc45a2))
* optimized deposit loading ([f3994c4](https://gitlab.unige.ch/aou/aou-portal/commit/f3994c4d217af76af3ab6adcbb9a56db15a3d0e5))
* pipe translate from backend ([f67629d](https://gitlab.unige.ch/aou/aou-portal/commit/f67629dd5f2a758044f6e9f492665001ee99fc87))
* proxy added and use it to login in ([6847132](https://gitlab.unige.ch/aou/aou-portal/commit/6847132a7afb451313b2e5e4f3d10ac33dc0f790))
* send deposit step as string to backend ([0469a75](https://gitlab.unige.ch/aou/aou-portal/commit/0469a75836443a961c875cc154dc27e96f3c5260))
* translate all label from solidify ([e72b015](https://gitlab.unige.ch/aou/aou-portal/commit/e72b0158031c7391623e2d39b9d0a672ac26caa0))
* use checkboxes instead of radio buttons for affiliation and
  collaboration ([30939d3](https://gitlab.unige.ch/aou/aou-portal/commit/30939d3ba13ed5bcf5ef3c1e7e5164a163b37304))
* use patch value on deposit form to allow partial data comming from
  backend ([8738ccf](https://gitlab.unige.ch/aou/aou-portal/commit/8738ccf4ea3af87ed2b87fe7d76095a78a11c17d))
* use structure component for deposit academic structure ([6359111](https://gitlab.unige.ch/aou/aou-portal/commit/6359111d740689631ddfa6b4a3e7432e02ad8ba1))

### Bug Fixes

* 213 on contributor none unige button deselect all radio
  button ([d061fd8](https://gitlab.unige.ch/aou/aou-portal/commit/d061fd801c62c49b58ca6edeb3d5408b20fecc1d))
* 223 sort language by sort value in select box ([a711eba](https://gitlab.unige.ch/aou/aou-portal/commit/a711ebad0dbea9bcacb2e35e357110b2e82482f6))
* 224 languages not loaded on all deposit selectbox ([2fac40c](https://gitlab.unige.ch/aou/aou-portal/commit/2fac40c11d6e522be823cfc14f9d4119e3f09fcc))
* 230 rename french text in error ([c1bf828](https://gitlab.unige.ch/aou/aou-portal/commit/c1bf828a8a78165159458264d913d675ce3546bf))
* **AcademicStructures:** code and oldCode are text ([2554882](https://gitlab.unige.ch/aou/aou-portal/commit/25548823e347a50bdb0fa5a2f0e429e9590851f5))
* adapt to new changes in solidify-frontend ([3002104](https://gitlab.unige.ch/aou/aou-portal/commit/300210415097eb19d3c66bb94c2a87fae393bd87))
* adapt to new changes in solidify-frontend ([a83846f](https://gitlab.unige.ch/aou/aou-portal/commit/a83846f3839a5e7d99f374852584ad3f130424e7))
* add deposit status history as a children in deposit state ([53e54dc](https://gitlab.unige.ch/aou/aou-portal/commit/53e54dcfbfe13b010019a661e903d00214319bc6))
* add different style on structure depending on status and rename french status
  obsolete ([4b35257](https://gitlab.unige.ch/aou/aou-portal/commit/4b35257bb1e9c6d560a25d21e14c13c5c9749d45))
* add missing icon getter on role ([3b09dd2](https://gitlab.unige.ch/aou/aou-portal/commit/3b09dd2b3dfb6a6c3892674c17fb578028a6c952))
* add workarround to allow running app without publication sub subtype model
  generated ([036fee9](https://gitlab.unige.ch/aou/aou-portal/commit/036fee9906c5ae7affe690a67e0b92632c7bcdae))
* admin user list avatar load ([2f27fef](https://gitlab.unige.ch/aou/aou-portal/commit/2f27fef2bb8dc967f8b3520a26b88bd8b4cd7683))
* adpat to work with new solidify ([1707b17](https://gitlab.unige.ch/aou/aou-portal/commit/1707b17d43572dd9e365a7d1ed5b5892203ff8aa))
* all start script in package json ([5a23b1a](https://gitlab.unige.ch/aou/aou-portal/commit/5a23b1ad92f2b3813342d60e8ef74e14ddb4a193))
* allow to skip step contributor directly with next button ([e7bd3b9](https://gitlab.unige.ch/aou/aou-portal/commit/e7bd3b97b37228cc423715f5071820de4a94a3c8))
* **aou-portal:** Dockerfile ([b4cd536](https://gitlab.unige.ch/aou/aou-portal/commit/b4cd536e447e5329f02fc5d225f727abb3e29aa0))
* **aou-portal:** production theme ([96c9ae2](https://gitlab.unige.ch/aou/aou-portal/commit/96c9ae20f749d1823fec84448d44e7300a68727d))
* cancel button on unige contributor not reset with correct
  value ([3001787](https://gitlab.unige.ch/aou/aou-portal/commit/3001787e6e9b86f9655746610d19d5a101b39f14))
* change LocalStateEnum from properties to enum ([4a4e402](https://gitlab.unige.ch/aou/aou-portal/commit/4a4e402d84b08c3393ea3e256aeaa95985de60a9))
* change metadata into formData ([d542062](https://gitlab.unige.ch/aou/aou-portal/commit/d542062afc4636e0990419ede2006facf1915fb1))
* change to use depositAcademicStructureForm to
  createAcademicStructure ([ddc4826](https://gitlab.unige.ch/aou/aou-portal/commit/ddc4826b02ddc360426fdacd4b23ad730ef25108))
* clean list sub sub type when create deposit ([9da1310](https://gitlab.unige.ch/aou/aou-portal/commit/9da13105ff2fab5b012ae239a3d71848fa75bb28))
* clear deposit subtype and subsubtype values accordingly ([a136935](https://gitlab.unige.ch/aou/aou-portal/commit/a1369354330bc738fbdbd335e9f43797206e3c6b))
* console error on date on deposit form ([8fbabad](https://gitlab.unige.ch/aou/aou-portal/commit/8fbabadb7dda7f6341e3a1319d218173670f1ec7))
* contributor firstname lastname change after dragging and select member
  unige ([dc5eada](https://gitlab.unige.ch/aou/aou-portal/commit/dc5eada98501acf137a29bf118d11d63f32fd4d1))
* contributor unige member first name and last name
  disappear ([9f64fda](https://gitlab.unige.ch/aou/aou-portal/commit/9f64fda2138d1915bfc40dbb9e688c390e86ca57))
* create deposit redirection to edit ([a818ba6](https://gitlab.unige.ch/aou/aou-portal/commit/a818ba689ab1b2b06f9b88e96db87c00bf58bad7))
* default order on deposit list ([a2a2785](https://gitlab.unige.ch/aou/aou-portal/commit/a2a2785cd537ef2265d3c107e2fed78d243ffca5))
* delete dialog variable not used ([c305532](https://gitlab.unige.ch/aou/aou-portal/commit/c3055327634f858ffbf37652e4ac21cfb7ca914e))
* deposit description form width size wired due to min size
  input ([2bdbeb0](https://gitlab.unige.ch/aou/aou-portal/commit/2bdbeb086fd45c45313b0e192e7237ca98a5432a))
* deposit description step page input ([872035d](https://gitlab.unige.ch/aou/aou-portal/commit/872035d4cc67ce3d2994c6571a17f51fb7422dec))
* **DepositFormDescriptionRightPresentational:** correct method
  signature ([8f2cd0c](https://gitlab.unige.ch/aou/aou-portal/commit/8f2cd0c3073d8357d3d3f9cbb3dba5fdae81e0c4))
* disable linear mode to allow navigate with url on deposit
  form ([6731966](https://gitlab.unige.ch/aou/aou-portal/commit/6731966cd180ba40b96720b5ad488cfc1e448b14))
* display error on step header only on dirty field ([40ed02e](https://gitlab.unige.ch/aou/aou-portal/commit/40ed02edaf5d02fb455a5ab23e583b8b2156d06d))
* display tree node with long label when hover ([0f3a4d2](https://gitlab.unige.ch/aou/aou-portal/commit/0f3a4d2e2ae1a72e7e8cb4b8678f1845ae4652c9))
* error that avoid display deposit summary ([702c1ec](https://gitlab.unige.ch/aou/aou-portal/commit/702c1ec4c0774388f16fac934d63bbaa2e9293b5))
* error that broke deposit form ([211fc6b](https://gitlab.unige.ch/aou/aou-portal/commit/211fc6b1437994bec399443e680dc727044afaec))
* generate model and enum from backend ([18d7b51](https://gitlab.unige.ch/aou/aou-portal/commit/18d7b51819a6ed2fea72bae3df0a931a57a6fc5e))
* hide in user profile user first name and last name ([099abf0](https://gitlab.unige.ch/aou/aou-portal/commit/099abf0cd10888c139be3527be766468d1b5131e))
* identifiant publication not restore correctly ([c70cbea](https://gitlab.unige.ch/aou/aou-portal/commit/c70cbeaaca881daaab36c50704c951dd61973ae4))
* import generated method ([6a032eb](https://gitlab.unige.ch/aou/aou-portal/commit/6a032eb3b65850af7a7664d5f92e2238053ba2ee))
* in case of user specify the role to delete for user
  profile ([f2448ea](https://gitlab.unige.ch/aou/aou-portal/commit/f2448ea64b0dda26546b6dfe97bfac0256a274f1))
* in case of user specify the role to delete for user
  profile ([9881667](https://gitlab.unige.ch/aou/aou-portal/commit/9881667a1ebbaa33117c20bfcea785b8a543a4fb))
* lint error ([4c4d1bf](https://gitlab.unige.ch/aou/aou-portal/commit/4c4d1bf24127917628d8e2b1622973e513b221d0))
* make orginal title translation not mandatory ([20a3bab](https://gitlab.unige.ch/aou/aou-portal/commit/20a3bababd67a4eaf31b9f29e6f5443bfb75f10f))
* not use create with list of child ids for person structure relation if role
  user ([062ce52](https://gitlab.unige.ch/aou/aou-portal/commit/062ce52915f124252816d81843e5ff624fa3d6f2))
* npm run start ([4a4070c](https://gitlab.unige.ch/aou/aou-portal/commit/4a4070c80089a67a231144d42971d34d6684a89c))
* preserve structure pagination size when return on list ([3cde842](https://gitlab.unige.ch/aou/aou-portal/commit/3cde8423d738cca05964d5acea76d5e66a3636f3))
* prevent navigation with mat stepper header ([1a7664e](https://gitlab.unige.ch/aou/aou-portal/commit/1a7664e6581c468b6b96a96de3c126cf5f157ba8))
* **README.md:** typo ([e1f9448](https://gitlab.unige.ch/aou/aou-portal/commit/e1f9448d268bdc00e0cf66a26a9e9e102167e164))
* remove all redundante code with solidify ([15f4d93](https://gitlab.unige.ch/aou/aou-portal/commit/15f4d9394f345c7e870cdeb5c91d4a506936e29e))
* remove back button on publication list ([fad6e6d](https://gitlab.unige.ch/aou/aou-portal/commit/fad6e6d0d55d46d997d6020a1a60bd6e3eb3eb12))
* remove breakpoint service to use service provided by
  solidify ([8574cdf](https://gitlab.unige.ch/aou/aou-portal/commit/8574cdf083750a05d5c82f1672f7468a8961fed9))
* remove double parsing, and fix load of sub sub type list when
  F5 ([5097440](https://gitlab.unige.ch/aou/aou-portal/commit/50974401c3102bd9c1347127161fa7836b8ecf0c))
* remove form file ([a00d331](https://gitlab.unige.ch/aou/aou-portal/commit/a00d331dda4753fdb12840ae18064e7957c332ca))
* remove obsolete import ([24bfcb7](https://gitlab.unige.ch/aou/aou-portal/commit/24bfcb7647c471e65cac64b6b7847b6ff32e391b))
* remove references to dlcm ([11c2da1](https://gitlab.unige.ch/aou/aou-portal/commit/11c2da1aac77a866c21ae863dde34566c90f0ea9))
* remove rights section in deposit creation ([6e9ae4c](https://gitlab.unige.ch/aou/aou-portal/commit/6e9ae4cb25a8d83350b827605ab185be73da7869))
* remove script build:solidify ([3cfc91b](https://gitlab.unige.ch/aou/aou-portal/commit/3cfc91b19bdaa3b9d5366cffdc74caf2ffe4aa14))
* remove workarround an update api json ([3f0d128](https://gitlab.unige.ch/aou/aou-portal/commit/3f0d128717c83203c43287cbe7c3b2f38761d08e))
* rename element in readme ([86d876a](https://gitlab.unige.ch/aou/aou-portal/commit/86d876a7710b1a40f84a77b835f8efb0af16a2bd))
* rename label 'Authors, affiliations and projects' into 'Contributors and
  affiliations' ([4b888ca](https://gitlab.unige.ch/aou/aou-portal/commit/4b888ca372b294a1dc114163cdfbb94465d4025c))
* rename open archive into archive ouverte ([660e363](https://gitlab.unige.ch/aou/aou-portal/commit/660e3634c82f74c9b1797d1182de74215b5f315c))
* retrieve language and structure when create a new deposit ([e94f66c](https://gitlab.unige.ch/aou/aou-portal/commit/e94f66c97780a1a858bbe2dac6b4afb24cbee9fd))
* rollback change on live reload ([b838465](https://gitlab.unige.ch/aou/aou-portal/commit/b838465a3a52481a47a4184ec98ffe79872c6249))
* service worker to run at the root level ([49b1fa6](https://gitlab.unige.ch/aou/aou-portal/commit/49b1fa6448005054d52c10cbb15673e23e24686d))
* setting to allow to connect local angular env to backend running on
  aoutest ([5f872b8](https://gitlab.unige.ch/aou/aou-portal/commit/5f872b85d8cad4f76973f65d93e12a0363c060f7))
* **SharedPersonStructureRolePresentational:** typo in
  imports ([1344779](https://gitlab.unige.ch/aou/aou-portal/commit/1344779eecf9bf67f4ac2d33be756476ff7dc4cf))
* **SharedPersonStructureRolePresentational:** typo in
  imports ([cb060ae](https://gitlab.unige.ch/aou/aou-portal/commit/cb060ae407f54ddb2b262edf660d8fa3d4b0f70f))
* show profile for user with role user ([84c86a3](https://gitlab.unige.ch/aou/aou-portal/commit/84c86a3161cc5eb82360fad879499f573a7cb6a5))
* show structures already selected for user as role user ([56a1b31](https://gitlab.unige.ch/aou/aou-portal/commit/56a1b313a770f2dcb278d767e2e236aa3fe48342))
* sort correctly document file type group ([c492f3f](https://gitlab.unige.ch/aou/aou-portal/commit/c492f3f6bb4b868e8206672b4344bd969da59b1b))
* translate role in user profile ([d1aaeaa](https://gitlab.unige.ch/aou/aou-portal/commit/d1aaeaa8af4014b96bef497cf5ead128a1135c96))
* unable to edit embargo date on deposit document file ([99c1c23](https://gitlab.unige.ch/aou/aou-portal/commit/99c1c23b6abfa6782fc6bf5d9d6c6b84d6a0d34a))
* uniformize display input on user profil dialog ([6f4f3a0](https://gitlab.unige.ch/aou/aou-portal/commit/6f4f3a064306ced5fdbcbf3ef6a5555ef23770bf))
* update selector prefix ([401e39d](https://gitlab.unige.ch/aou/aou-portal/commit/401e39d76340b1512154aa453d1c529b11cd8fca))
* update solidify to 1.1.1 to take into account runtime override conf in solidify app
  state ([7ecdd7f](https://gitlab.unige.ch/aou/aou-portal/commit/7ecdd7fde6fa3fb4897dcef662a626a3b136d031))
* update solidify to 1.1.3 to fix additionnal information
  panel ([edf8175](https://gitlab.unige.ch/aou/aou-portal/commit/edf8175c068b1fb9d39f1005c316df95d654607e))
* use correct case for project name ([aabe238](https://gitlab.unige.ch/aou/aou-portal/commit/aabe238bbf57c5e303d5f3c59575c963b4aff8db))
* use lower case for valid LinkTypes values ([276437e](https://gitlab.unige.ch/aou/aou-portal/commit/276437eb2a12d557ee68034cc43990acefef494b))
* use publication step enum in the deposit form ([a9d7e55](https://gitlab.unige.ch/aou/aou-portal/commit/a9d7e555d92c1cf90dcf390d5ec525d7eb54e083))
* use the label Deposits instead of Deposit in the menu bar ([e2d87c2](https://gitlab.unige.ch/aou/aou-portal/commit/e2d87c283882a682b03ba12284b67ed00b63aeeb))
* way to load deposit form contributor more simpler ([a6ea609](https://gitlab.unige.ch/aou/aou-portal/commit/a6ea60968205e55f035a4650de09643d62c29397))
* when reopen deposit display correctly subtype selected ([2b17cd8](https://gitlab.unige.ch/aou/aou-portal/commit/2b17cd8cd574f7f8e2f5d5391ec32111dfa434f7))
