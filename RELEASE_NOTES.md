#### [aou-2.1.3](https://gitlab.unige.ch/aou/aou-portal/compare/aou-2.1.2...aou-2.1.3) (2024-11-18)

##### Bug Fixes

* **deposit:**
  *  [AOU-2037] avoid to trigger action when press enter in deposit form ([a566b160](https://gitlab.unige.ch/aou/aou-portal/commit/a566b160099beb4ac0937f65e550094beed2ccec))
  *  hide doctor email and address inputs if thesis is before 2010 or author before unige ([01098817](https://gitlab.unige.ch/aou/aou-portal/commit/01098817924e7af5de313e29bcea5e35f49985ec))
  *  [AOU-1802] avoid to create deposit with invalid year or email on thesis ([47befba5](https://gitlab.unige.ch/aou/aou-portal/commit/47befba577235d9ddbfce8fb824646892c41012b))
  *  [AOU-1929] preserve order change on collaboration members ([f4b75748](https://gitlab.unige.ch/aou/aou-portal/commit/f4b75748db7e9eeb0ba92d18b50770bb1a8ca922))
  *  [AOU-1999] hide undesired fields after batch update ([16357fab](https://gitlab.unige.ch/aou/aou-portal/commit/16357fabe30da3a5735fc55fe5705686d041da8a))
* **contributor:**
  *  [AOU-2029] display error on contributor fields ([825cde80](https://gitlab.unige.ch/aou/aou-portal/commit/825cde8055dd1d127d0af46de78209b6307c37f8))
  *  [AOU-2029] display contributor page on profile only if current user have publication ([e6d7e845](https://gitlab.unige.ch/aou/aou-portal/commit/e6d7e8454c3dd5230791ec876e9df6adcfd09383))
  *  [AOU-2021] manage case where contributor have no publications ([e38e15cc](https://gitlab.unige.ch/aou/aou-portal/commit/e38e15cc8f1487085b249ffc3b49a3d4096b4dfd))
*  [AOU-1983] change order of notifications in the profile page ([a53ad88b](https://gitlab.unige.ch/aou/aou-portal/commit/a53ad88b9503d44e554ad4a7d0227816969a095d))
* **SSR:**  replace home path by root path for root SSR page ([6829c162](https://gitlab.unige.ch/aou/aou-portal/commit/6829c162f276a5c94c0be00a80a247e4d37bf037))

##### Code Style Changes

* **deposit-list:**  customize deposit tab for validator ([8616d2dd](https://gitlab.unige.ch/aou/aou-portal/commit/8616d2dd28bf92f6fbfc824e1a23181a04662fd1))
*  fix toggle on search page ([a34b4718](https://gitlab.unige.ch/aou/aou-portal/commit/a34b47184bfe29b219d6d0fa96b61e4d2cacc4b0))

