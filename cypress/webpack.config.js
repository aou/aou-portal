var path = require('path');

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            '@src': path.resolve(__dirname, '../src'),
            '@shared': path.resolve(__dirname, '../src/app/shared'),
            '@cypress-environments': path.resolve(__dirname, './environments'),
            '@environments': path.resolve(__dirname, '../src/environments'),
            '@app': path.resolve(__dirname, '../src/app'),
            '@admin': path.resolve(__dirname, '../src/app/features/admin'),
            '@deposit': path.resolve(__dirname, '../src/app/features/deposit'),
            '@home': path.resolve(__dirname, '../src/app/features/home'),
            '@models': path.resolve(__dirname, '../src/app/models/index'),
            '@enums': path.resolve(__dirname, '../src/app/enums/index'),
            'solidify-frontend': path.resolve(__dirname, '../node_modules/solidify-frontend')
        }
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: [/node_modules/],
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            // skip typechecking for speed
                            transpileOnly: true
                        }
                    }
                ]
            }
        ]
    }
}
