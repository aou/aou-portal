import {cypressEnvironmentLocal} from "./cypress-environment.local";
import {CypressEnvironmentModel} from "./cypress-environment.model";

export const cypressEnvironment: CypressEnvironmentModel = {
  rootUrl: "http://localhost:4300",
  localAuth: false,
  ...cypressEnvironmentLocal,
};
