export interface CypressEnvironmentModel {
  rootUrl: string;
  localAuth: boolean;
  tokenRoot: string | undefined;
  tokenAdmin: string | undefined;
  tokenUser: string | undefined;
}
