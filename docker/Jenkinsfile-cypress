pipeline {
    agent any

    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        skipStagesAfterUnstable()
        timeout(time: 1, unit: 'HOURS')
        timestamps()
    }

    environment {
        dockerPullEndPoint= "nexus.unige.ch:445"
        dockerPushEndPoint= "nexus.unige.ch:446"
        HTTP_PROXY = "http://proxy.unige.ch:3128"
        HTTPS_PROXY = "http://proxy.unige.ch:3128"
    }

    stages {

        stage('Docker build'){
            steps {
                withCredentials([usernamePassword(credentialsId: "nexusCredentials",
                        usernameVariable: 'NEXUS_USER',
                        passwordVariable: 'NEXUS_PASSWORD')]) {

                    sh "docker login ${dockerPullEndPoint} -u ${NEXUS_USER} -p ${NEXUS_PASSWORD}"
                    sh '''
                     docker build --no-cache --pull -t archive-ouverte-portal-e2e:latest -f docker/Dockerfile . --build-arg IMAGE_SRC=nexus.unige.ch:445/cypress/included:4.5.0
                  '''
                }
            }
        }

        stage('Docker push') {
            steps {
                withCredentials([usernamePassword(credentialsId: "nexusCredentials",
                        usernameVariable: 'NEXUS_USER',
                        passwordVariable: 'NEXUS_PASSWORD')]) {

                    sh "docker login ${dockerPushEndPoint} -u ${NEXUS_USER} -p ${NEXUS_PASSWORD}"
                    sh '''
                        docker tag archive-ouverte-portal-e2e:latest ${dockerPushEndPoint}/archive-ouverte-portal-e2e:latest
                        docker push ${dockerPushEndPoint}/archive-ouverte-portal-e2e:latest
                    '''
                }
            }
        }
    }

    post {
        failure {
            emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                    replyTo: '${DEFAULT_REPLYTO}', subject: '${DEFAULT_SUBJECT}',
                    attachLog: true,
                    to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                            [$class: 'RequesterRecipientProvider']]))
        }
        always {
            retry(3) {
                deleteDir()
            }
        }
    }
}
